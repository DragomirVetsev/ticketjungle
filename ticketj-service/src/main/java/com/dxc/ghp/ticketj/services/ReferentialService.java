/**
 *
 */
package com.dxc.ghp.ticketj.services;

import java.util.List;

import com.dxc.ghp.ticketj.dto.CitiesAndGenresDTO;
import com.dxc.ghp.ticketj.dto.PerformerDTO;

/**
 * Contains all referential services related to all static data.
 */
public interface ReferentialService {

    /**
     * @return City of the Venue and and genres of the events which will be used to
     *         search events.
     */
    CitiesAndGenresDTO getCitiesAndGenres();

    /**
     * Finds and returns all performers.
     *
     * @return List of {@link PerformerDTO}. Cannot be null. Empty list means no
     *         performers.
     */
    List<PerformerDTO> findAllPerformers();
}
