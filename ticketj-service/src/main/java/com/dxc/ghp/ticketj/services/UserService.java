package com.dxc.ghp.ticketj.services;

import java.util.List;

import com.dxc.ghp.ticketj.dto.UserDTO;
import com.dxc.ghp.ticketj.dto.UserInfoDTO;
import com.dxc.ghp.ticketj.misc.exceptions.InvalidCredentialsException;
import com.dxc.ghp.ticketj.misc.types.Credentials;

/**
 * Contains all user related services.
 */
public interface UserService {

    /**
     * Finds and returns list with all Ticket Jungle users.
     *
     * @return List of {@link UserDTO}. Cannot be null. Empty list means no users.
     */
    List<UserDTO> findAllUsers();

    /**
     * Finds user by his/her credentials.
     * 
     * @param credentials
     *            user`s username and password.
     * @return the {@link Credentials} if it exists in the database.
     * @throws InvalidCredentialsException
     *             if the user send wrong username or password.
     * @see InvalidCredentialsException
     */
    UserInfoDTO findUserByCredentials(Credentials credentials);
}
