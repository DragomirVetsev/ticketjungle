package com.dxc.ghp.ticketj.services;

import java.util.List;

import com.dxc.ghp.ticketj.dto.CartItemDTO;
import com.dxc.ghp.ticketj.dto.EventDTO;
import com.dxc.ghp.ticketj.dto.EventSearchDTO;
import com.dxc.ghp.ticketj.dto.StageDTO;
import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.dto.SubEventOrderedPairDTO;
import com.dxc.ghp.ticketj.misc.exceptions.EntityExistsException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.misc.exceptions.ValidationException;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;

/**
 * Contains all event related services.
 */
public interface EventService {
    /**
     * Finds and returns all Ticket Jungle events.
     *
     * @return List of {@link EventDTO}. Cannot be null. Empty list means no events.
     */
    List<EventDTO> findAllEvents();

    /**
     * Creates an event.
     *
     * @param eventDto
     *            is the event data transfer object being passed for persistence.
     * @return the {@link EventDTO} object after being persisted into the
     *         repository.
     */
    EventDTO createEvent(EventDTO eventDto);

    /**
     * Creates a sub-event.
     *
     * @param subEventDto
     *            is the sub-event data transfer object being passed for
     *            persistence.
     * @return the {@link SubEventDTO} object after being persisted into the
     *         repository.
     */
    SubEventDTO createSubEvent(SubEventDTO subEventDto);

    /**
     * @param criteriaDto
     *            Is the information for witch
     * @return The desired Events found by criteria. Cannot be null. Empty list
     *         means no events.
     */
    List<EventDTO> findByCriteria(EventSearchDTO criteriaDto);

    /**
     * Finds and returns a Ticket Jungle event by name.
     *
     * @param name
     *            is the name of the Event we are searching for. Must not be null
     * @return Finds and returns an {@link EventDTO} by name.
     */
    EventDTO findByName(String name);

    /**
     * Finds and returns the current state of the stage and its seats for a given
     * sub-event.
     *
     * @param performance
     *            is the id of the sub-event.
     * @return The condition of the hall for the respective sub-event.
     */
    StageDTO findSubEventStageSeats(PerformanceId performance);

    /**
     * Finds and returns all Ticket Jungle sub-events in an event.
     *
     * @param eventName
     *            is the name of the Event which contains these sub-events. Must not
     *            be null.
     * @return List of {@link SubEventDTO}. Cannot be null. Empty list means no
     *         sub-events.
     */
    List<SubEventDTO> findAllSubEventsInEvent(String eventName);

    /**
     * Finds and returns a pair of lists with Ticket Jungle sub-events ordered by
     * time and popularity.
     *
     * @return Pair of lists ({@link SubEventOrderedPairDTO}) ordered by their time
     *         and popularity. The pair entity cannot be null. Lists cannot be null.
     *         Empty list means no sub-events.
     */
    SubEventOrderedPairDTO findAllSubEventsOrdered();

    /**
     * Modifies an already existing event entity (does not create a new entity if it
     * does not exist).
     *
     * @param eventName
     *            The unique event identifier.
     * @param eventToUpdate
     *            The event data transfer object containing only fields to be
     *            modified. Can be null.
     * @return The {@link EventDTO} object after being modified. Cannot be null.
     * @throws ValidationException
     *             When DTO validation has failed.
     * @throws EntityNotFoundException
     *             when there is no event entity with given unique identifier.
     * @throws EntityExistsException
     *             when there is an existing event with {@code eventName}.
     */
    EventDTO updateEvent(String eventName, EventDTO eventToUpdate);

    /**
     * Modifies an already existing sub-event entity (does not create a new entity
     * if it does not exist).
     *
     * @param performance
     *            The unique sub-event identifier.
     * @param subEventToUpdate
     *            The event data transfer object containing only fields to be
     *            modified. Can be null.
     * @return The {@link SubEventDTO} object after being modified. Cannot be null.
     * @throws ValidationException
     *             When DTO validation has failed.
     * @throws EntityNotFoundException
     *             when there is no sub-event entity with given unique identifier.
     * @throws EntityExistsException
     *             when there is an existing sub-event with {@code performance}.
     */
    SubEventDTO updateSubEvent(PerformanceId performance, SubEventDTO subEventToUpdate);

    /**
     * Deletes an existing sub-event entity (does not delete if there already are
     * relationships containing this entity).
     *
     * @param performance
     *            The unique sub-event identifier.
     * @return The {@link SubEventDTO} object after being modified. Cannot be null.
     * @throws ValidationException
     *             When DTO validation has failed.
     * @throws EntityNotFoundException
     *             when there is no sub-event entity with given unique identifier.
     * @throws EntityExistsException
     *             when there is an existing sub-event with {@code performance}.
     */
    SubEventDTO deleteSubEvent(PerformanceId performance);

    /**
     * @param cartItems
     *            the list of Cart Items DTO
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @throws ValidationException
     *             when the validation within the {@code subEventToCreate} object
     *             has failed.
     * @throws EntityExistsException
     *             when there is an existing cart item with {@code ticket}.
     */
    void createCartItem(List<CartItemDTO> cartItems);
}
