package com.dxc.ghp.ticketj.services;

import java.util.List;

/**
 * Contains all office worker business related services.
 */
public interface OfficeWorkerService {
    /**
     * Finds and returns all Ticket Jungle not assigned to an office workers.
     *
     * @return List of all available workers' usernames. Cannot be null. Empty list
     *         means no workers or no available workers.
     */
    List<String> findAllAvailableWorkers();
}
