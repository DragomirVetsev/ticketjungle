package com.dxc.ghp.ticketj.services;

import java.util.List;

import com.dxc.ghp.ticketj.dto.OfficeDTO;
import com.dxc.ghp.ticketj.misc.exceptions.DuplicateFieldException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityExistsException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.exceptions.ValidationException;

/**
 * Contains all office related services.
 */
public interface OfficeService {
    /**
     * Finds and returns list with all Ticket Jungle offices.
     *
     * @return List of {@link OfficeDTO}. Cannot be null. Empty list means no
     *         events.
     */
    List<OfficeDTO> findAllOffices();

    /**
     * Creates a new office.
     *
     * @param newOffice
     *            The data transfer object containing info for constructing a new
     *            office. Can be null.
     * @return New {@link OfficeDTO} object of the persisted office. Cannot be null.
     * @throws ValidationException
     *             When DTO validation has failed.
     * @throws EntityNotFoundException
     *             When the city or phone code in the {@code newOffice} object do
     *             not exist.
     * @throws EntityExistsException
     *             When there is an existing office with the same primary
     *             identified.
     * @throws DuplicateFieldException
     *             When any of the unique properties of the office excluding the
     *             primary identifier, already exist. Unique properties: email,
     *             phone(code and number) and address(street and city).
     */
    OfficeDTO createOffice(OfficeDTO newOffice);

    /**
     * Modifies an already existing office entity (does not create a new entity if
     * it does not exist).
     *
     * @param officeId
     *            The unique office identifier.
     * @param officeToUpdate
     *            The office data transfer object containing only fields to be
     *            modified. Can be null.
     * @return The {@link OfficeDTO} object after being modified. Cannot be null.
     * @throws ValidationException
     *             When DTO validation has failed.
     * @throws EntityNotFoundException
     *             when there is no office entity with given unique identifier OR
     *             the city, phone code or any worker's username in the
     *             {@code officeToUpdate} object does not exist.
     * @throws DuplicateFieldException
     *             When any of the unique properties of the office excluding the
     *             primary identifier, already exist. Unique properties: email,
     *             phone(code and number) and address(street and city).
     */
    OfficeDTO modifyOffice(String officeId, OfficeDTO officeToUpdate);
}
