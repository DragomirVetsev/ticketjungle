package com.dxc.ghp.ticketj.services;

import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.dto.CustomerDTO;
import com.dxc.ghp.ticketj.dto.CustomerWithCredentialsDTO;
import com.dxc.ghp.ticketj.dto.OrderDTO;
import com.dxc.ghp.ticketj.dto.OrderWithTicketsDTO;
import com.dxc.ghp.ticketj.dto.ReservedTicketDTO;
import com.dxc.ghp.ticketj.dto.ReviewDTO;
import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.dto.TicketDTO;
import com.dxc.ghp.ticketj.misc.exceptions.EntityExistsException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.exceptions.ValidationException;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;

/**
 * Contains all customer related services.
 */
public interface CustomerService {

    /**
     * Finds and returns customer.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return {@link Optional} of CustomerDTO. Cannot be null. Empty Optional means
     *         no customer found.
     */
    Optional<CustomerDTO> findCustomer(String username);

    /**
     * Finds all Customer's tickets.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return list of tickets. Cannot be null. Empty list means this customer has
     *         no tickets.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with the given username does not exist.
     */
    List<TicketDTO> findTicketsOfCustomer(String username);

    /**
     * Find all Customer's reviews.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return list of reviews. Cannot be null. Empty list means this customer has
     *         no reviews.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with the given username does not exist.
     */
    List<ReviewDTO> findReviewsOfCustomer(String username);

    /**
     * Find Customer's Wishlist.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return list of subevents. Cannot be null. Empty list means this customer has
     *         no subevents in his Wishlist.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with the given username does not exist.
     */
    List<SubEventDTO> findWishlistOfCustomer(String username);

    /**
     * Find the cuustomer's shopping cart.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return customer's reserved tickets. Cannot be null. Empty list means this
     *         customer has no reserved tickets.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with the given username does not exist.
     */
    List<ReservedTicketDTO> findCartItemsOfCustomer(String username);

    /**
     * Remove the list of selected cart items of Customer.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @param reservedTicket
     *            the list of selected cart items from the customer.
     * @return updated list of cartItem
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with the given username does not exist.
     */
    List<ReservedTicketDTO> removeCartItemOfCustomer(String username, List<ReservedTicketDTO> reservedTicket);

    /**
     * Creates customer if the customer does not exists, if he exists the return
     * empty Optional. Cannot be null.
     *
     * @param newCustomerDTO
     *            Is a instance of the customer data that will be used to update or
     *            create a customer.
     * @return A instance of the new customer that was created or a empty Optional
     *         if the customer exists. Cannot be null.
     * @throws EntityExistsException
     *             when the customer with the given username exists.
     */
    CustomerDTO registerCustomer(CustomerWithCredentialsDTO newCustomerDTO);

    /**
     * Updates customer information or creates customer if the customer does not
     * exists.
     *
     * @param username
     *            is the unique customer identifier to be patched.
     * @param newCustomerDTO
     *            Is a instance of the customer data that will be used to update or
     *            create a customer.
     * @return A instance of the new customer that was updated or created.
     */
    CustomerDTO patchCustomer(final String username, CustomerWithCredentialsDTO newCustomerDTO);

    /**
     * Creates customer order for the tickets in the list of
     * newOrderWithTicketsDTO.**
     *
     * @param newOrderWithTicketsDTO
     *            The DTO that contains the necessary data for creating the order.*
     *            Can be null.*@return OrderWithTicketsDTO holding data about the
     *            created order.
     * @param loggedUsername
     *            The logged user username.
     * @return {@link OrderDTO}. Cannot be null.
     */
    OrderDTO createOrderWithTickets(OrderWithTicketsDTO newOrderWithTicketsDTO, String loggedUsername);

    /**
     * Adds sub-event to customer's wishlist if the sub-event exists and has not
     * been added to it.
     *
     * @param username
     *            The username of the customer to whom the wishlist belongs.
     * @param performanceId
     *            The ID of the sub-event to add if it exists or has not yet been
     *            added to the client's wish list.
     * @return {@code true} if the {@code SubEvent} has been added. {@code false} if
     *         the {@code SubEvent} hasn't been added.
     */
    boolean addSubEventToWishList(String username, PerformanceId performanceId);

    /**
     * Removes a sub-event from customer's wishlist if the sub-event is added to it
     *
     * @param username
     *            The username of the customer to whom the wishlist belongs.
     * @param performanceId
     *            The ID of the sub-event.
     * @return {@code true} if the {@code SubEvent} has been removed. {@code false}
     *         if the {@code SubEvent} hasn't been removed.
     */
    boolean removeSubEventFromWishList(String username, PerformanceId performanceId);

    /**
     * Removes multiple sub-events from customer's wishlist.
     *
     * @param username
     *            The username of the customer to whom the wishlist belongs.
     * @param selectedSubEvents
     *            The IDs of the selected sub-events.
     * @return {@code true} if one or many of the sub-events are removed.
     *         {@code false} if no sub-events were removed from the wishlist.
     */
    boolean removeSelectedSubEventsFromWishlist(String username, List<PerformanceId> selectedSubEvents);

}
