export const environment = {
  production: false,
  restApi: 'http://localhost:8081/ticketj/rest',
};

// This environment is used during development to run and test
// frontend without deploying it on tomcat.
