/**
 * Variables for web server environment.
 */
export const environment = {
  production: false,
  restApi: 'rest',
};

// This environment is used during development to run
// frontend and backend as one web application on tomcat.
