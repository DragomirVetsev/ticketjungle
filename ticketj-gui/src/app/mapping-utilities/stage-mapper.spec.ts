import {
  mapSeatDTOToSeatModel,
  mapRowDTOToRowModel,
  mapSectorDTOToSectorModel,
  mapAddressDTOToAddressModel,
  mapStageDTOToStageModel,
} from './stage-mapper';
import {
  SeatDTO,
  RowDTO,
  SectorDTO,
  AddressDTO,
  StageDTO,
} from '../models/dto/dto';

describe('Mapper Tests', () => {
  describe('mapSeatDTOToSeatModel', () => {
    it('should map SeatDTO to SeatModel', () => {
      const seatDTO: SeatDTO = {
        number: 1,
        status: 'ACQUIRED',
      };

      const expectedSeatModel = {
        number: 1,
        status: 'ACQUIRED',
      };

      const seatModel = mapSeatDTOToSeatModel(seatDTO);

      expect(seatModel).toEqual(expectedSeatModel);
    });
  });

  describe('mapRowDTOToRowModel', () => {
    it('should map RowDTO to RowModel', () => {
      const rowDTO: RowDTO = {
        number: 1,
        seats: [
          { number: 1, status: 'ACQUIRED' },
          { number: 2, status: 'AVAILABLE' },
        ],
      };

      const expectedRowModel = {
        number: 1,
        seats: [
          { number: 1, status: 'ACQUIRED' },
          { number: 2, status: 'AVAILABLE' },
        ],
      };

      const rowModel = mapRowDTOToRowModel(rowDTO);

      expect(rowModel).toEqual(expectedRowModel);
    });
  });

  describe('mapSectorDTOToSectorModel', () => {
    it('should map SectorDTO to SectorModel', () => {
      const sectorDTO: SectorDTO = {
        name: 'Sector 1',
        rows: [
          {
            number: 1,
            seats: [{ number: 1, status: 'ACQUIRED' }],
          },
        ],
      };

      const expectedSectorModel = {
        name: 'Sector 1',
        rows: [
          {
            number: 1,
            seats: [{ number: 1, status: 'ACQUIRED' }],
          },
        ],
      };

      const sectorModel = mapSectorDTOToSectorModel(sectorDTO);

      expect(sectorModel).toEqual(expectedSectorModel);
    });
  });

  describe('mapAddressDTOToAddressModel', () => {
    it('should map AddressDTO to AddressModel', () => {
      const addressDTO: AddressDTO = {
        street: 'Ulitsa 1',
        city: 'Sofia',
        postCode: '1000',
      };

      const expectedAddressModel = {
        street: 'Ulitsa 1',
        city: 'Sofia',
        postCode: '1000',
      };

      const addressModel = mapAddressDTOToAddressModel(addressDTO);

      expect(addressModel).toEqual(expectedAddressModel);
    });
  });

  describe('mapStageDTOToStageModel', () => {
    it('should map StageDTO to StageModel', () => {
      const stageDTO: StageDTO = {
        venueName: 'NDK',
        address: {
          street: 'Ulitsa 1',
          city: 'Sofia',
          postCode: '1000',
        },
        city: 'Sofia',
        stageName: 'Zala 1',
        sectors: [
          {
            name: 'Sector 1',
            rows: [
              {
                number: 1,
                seats: [{ number: 1, status: 'ACQUIRED' }],
              },
            ],
          },
        ],
      };

      const expectedStageModel = {
        venueName: 'NDK',
        address: {
          street: 'Ulitsa 1',
          city: 'Sofia',
          postCode: '1000',
        },
        city: 'Sofia',
        stageName: 'Zala 1',
        sectors: [
          {
            name: 'Sector 1',
            rows: [
              {
                number: 1,
                seats: [{ number: 1, status: 'ACQUIRED' }],
              },
            ],
          },
        ],
      };

      const stageModel = mapStageDTOToStageModel(stageDTO);

      expect(stageModel).toEqual(expectedStageModel);
    });
  });
});
