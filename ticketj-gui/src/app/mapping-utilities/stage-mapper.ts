import { AddressModel } from '../models/AddressModel';
import {
  RowModel,
  SeatModel,
  SectorModel,
  StageModel,
} from '../models/StageModel';
import {
  AddressDTO,
  RowDTO,
  SeatDTO,
  SectorDTO,
  StageDTO,
} from '../models/dto/dto';

export function mapSeatDTOToSeatModel(seatDTO: SeatDTO): SeatModel {
  return {
    number: seatDTO.number,
    status: seatDTO.status,
  };
}

export function mapRowDTOToRowModel(rowDTO: RowDTO): RowModel {
  return {
    number: rowDTO.number,
    seats: rowDTO.seats.map(mapSeatDTOToSeatModel),
  };
}

// Assuming you have a mapRowDTOToRowModel function to convert RowDTO to RowModel

export function mapSectorDTOToSectorModel(sectorDTO: SectorDTO): SectorModel {
  return {
    name: sectorDTO.name,
    rows: sectorDTO.rows.map(mapRowDTOToRowModel),
  };
}

export function mapAddressDTOToAddressModel(addressDTO: AddressDTO): AddressModel {
  return {
    street: addressDTO.street,
    city: addressDTO.city,
    postCode: addressDTO.postCode,
  };
}

export function mapStageDTOToStageModel(stageDTO: StageDTO): StageModel {
  return {
    venueName: stageDTO.venueName,
    address: mapAddressDTOToAddressModel(stageDTO.address),
    city: stageDTO.city,
    stageName: stageDTO.stageName,
    sectors: stageDTO.sectors.map(mapSectorDTOToSectorModel),
  };
}
