import { Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { NavigationEnd, Router } from '@angular/router';
import { LoginService } from './services/account/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'ticketj-gui';
  windowScrolled = false;
  showMonkey = false;

  constructor(
    @Inject(DOCUMENT) private readonly document: Document,
    private readonly router: Router,
    private readonly loginService: LoginService
  ) {}

  // @HostListener('window:beforeunload', ['$event'])
  // logOutOnBrowserClose(event: Event): void {
  //   if (event.currentTarget === window) {
  //     this.loginService.logout();
  //   }
  // }

  // @HostListener('window:beforeunload', ['$event'])
  // logOutOnBrowserClose() {
  //   let pageReloaded = window.performance
  //     .getEntriesByType('navigation')
  //     .map(nav => (nav as any).type)
  //     .includes('reload');
  //   if (!pageReloaded) {
  //     this.loginService.logout();
  //   }
  // }

  ngOnInit() {
    window.addEventListener('scroll', () => {
      this.windowScrolled = window.scrollY !== 0;
    });

    this.router.events.subscribe(event => {
      const delayBeforeShowMonkeyFalse = 2000;
      if (event instanceof NavigationEnd) {
        this.showMonkey = true;
        setTimeout(() => {
          this.showMonkey = false;
        }, delayBeforeShowMonkeyFalse);
      }
    });
  }
  toTop(): void {
    window.scrollTo(0, 0);
  }
}
