import { TestBed } from '@angular/core/testing';

import { SearchEventService } from './search-event.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { StaticSearchCriteria } from 'src/app/models/StaticSearchCriteria';

describe('SearchEventService', () => {
  let service: SearchEventService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [SearchEventService],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(SearchEventService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get events', () => {
    const expectedQuery =
      '&from=2023-08-28T12:00:00Z&to=2023-08-30T12:00:00Z&city=Sofia&genre=Rock';
    service.searchForEvents(expectedQuery).subscribe({
      next: response => {
        expect(response).toEqual([]);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/events/search?${expectedQuery}`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });

  it('should handle empty event list', () => {
    const expectedQuery =
      '&from=2019-08-28T12:00:00Z&to=2019-08-30T12:00:00Z&city=Sofia&genre=Rock';
    service.searchForEvents(expectedQuery).subscribe({
      next: response => {
        expect(response).toEqual([]);
      },
    });

    const request = httpTestingController.expectOne(
      `${environment.restApi}/events/search?${expectedQuery}`
    );
    request.flush([]);
  });

  it('should get cities and genres', () => {
    let subscribedValue: StaticSearchCriteria;
    service.getSearchStaticCriteria().subscribe(value => {
      subscribedValue = value;
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/referential/cities-and-genres`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });

  it('should handle empty cities and genres list', () => {
    let subscribedValue: StaticSearchCriteria;
    service.getSearchStaticCriteria().subscribe(value => {
      subscribedValue = value;
    });

    const request = httpTestingController.expectOne(
      `${environment.restApi}/referential/cities-and-genres`
    );
    request.flush([]);
  });
});
