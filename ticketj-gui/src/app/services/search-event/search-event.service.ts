import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EventModol } from 'src/app/models/EventModel';
import { StaticSearchCriteria } from 'src/app/models/StaticSearchCriteria';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SearchEventService {
  private readonly eventsUrl = `${environment.restApi}/referential/cities-and-genres`;
  private readonly searchUrl = environment.restApi + '/events/search';
  constructor(private readonly httpClient: HttpClient) {}

  getSearchStaticCriteria(): Observable<StaticSearchCriteria> {
    return this.httpClient.get<StaticSearchCriteria>(this.eventsUrl);
  }

  searchForEvents(query: string): Observable<EventModol[]> {
    const url = `${this.searchUrl}?${query}`;
    return this.httpClient.get<EventModol[]>(url);
  }

  searchForEvents1(
    paramMap: Map<string, string | null>
  ): Observable<EventModol[]> {
    let params = new HttpParams();

    paramMap.forEach((value, key) => {
      if (value != null) {
        params = params.append(key, value);
      }
    });
    return this.httpClient.get<EventModol[]>(this.searchUrl, { params });
  }
}
