import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserCredentials } from 'src/app/models/UserCredentials';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { JwtUserModel } from 'src/app/models/JwtUserModel';
@Injectable({
  providedIn: 'root',
})
export class LoginService {
  clearTimeout: any;
  private readonly loginUrl = environment.restApi + '/login';
  constructor(
    private readonly http: HttpClient,
    private readonly router: Router
  ) {}

  onLogin(user: UserCredentials): Observable<JwtUserModel> {
    return this.http.post<JwtUserModel>(this.loginUrl, user);
  }
  static decodeToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (error) {
      return null;
    }
  }
  autoLogout(expirationDate: number) {
    this.clearTimeout = setTimeout(() => {
      this.logout();
    }, expirationDate);
  }
  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/');
    if (this.clearTimeout) {
      clearTimeout(this.clearTimeout);
    }
  }
}
