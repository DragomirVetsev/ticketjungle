import { TestBed } from '@angular/core/testing';

import { OfficeService } from './office.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { OfficeModel } from 'src/app/models/OfficeModel';

describe('OfficeComponent', () => {
  let service: OfficeService;
  let httpTestingController: HttpTestingController;
  let officeName: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [OfficeService],
    });
    service = TestBed.inject(OfficeService);
    httpTestingController = TestBed.inject(HttpTestingController);
    officeName = '';
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get offices', () => {
    service.getOffices().subscribe({
      next: response => {
        expect(response).toEqual([]);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/offices`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });

  it('should handle empty office list', () => {
    service.getOffices().subscribe(response => {
      expect(response).toEqual([]);
    });

    const request = httpTestingController.expectOne(
      `${environment.restApi}/offices`
    );
    request.flush([]);
  });
  const testNewOffice: OfficeModel = {
    officeName: 'Name Test',
    officeAddress: {
      street: 'Some street',
      city: 'Sofia',
      postCode: '1234',
    },
    email: 'test@email.com',
    phone: { code: '+359', number: '54565434' },
    workingHours: { startTime: '09:00Z', endTime: '18:00Z' },
    workers: ['testWorker'],
  };
  it('should create new office', () => {
    service.createOffice(testNewOffice).subscribe({
      next: response => {
        expect(response).toEqual(testNewOffice);
      },
    });
  });
  it('should update office', () => {
    service.patchOffice(testNewOffice).subscribe({
      next: response => {
        expect(response).toEqual(testNewOffice);
      },
    });
  });
});
