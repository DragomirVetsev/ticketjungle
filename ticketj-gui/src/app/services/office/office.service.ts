import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OfficeModel } from 'src/app/models/OfficeModel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OfficeService {
  private readonly officesUrl = `${environment.restApi}/offices`;
  private readonly availableWorkersUrl = `${environment.restApi}/office-workers/available`;

  constructor(private readonly httpClient: HttpClient) {}

  getOffices(): Observable<OfficeModel[]> {
    return this.httpClient.get<OfficeModel[]>(this.officesUrl);
  }
  getAvailableWorkers(): Observable<string[]> {
    return this.httpClient.get<string[]>(this.availableWorkersUrl);
  }
  createOffice(office: OfficeModel): Observable<OfficeModel> {
    return this.httpClient.post<OfficeModel>(this.officesUrl, office);
  }
  patchOffice(office: OfficeModel): Observable<OfficeModel> {
    const officePatchURL = `${this.officesUrl}/${office.officeName}`;
    return this.httpClient.patch<OfficeModel>(officePatchURL, office);
  }
}
