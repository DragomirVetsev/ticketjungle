import { TestBed } from '@angular/core/testing';

import { ErrorInterceptorInterceptor } from './error-interceptor';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('ErrorInterceptorInterceptor', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule],
      providers: [ErrorInterceptorInterceptor],
    })
  );

  it('should be created', () => {
    const interceptor: ErrorInterceptorInterceptor = TestBed.inject(
      ErrorInterceptorInterceptor
    );
    expect(interceptor).toBeTruthy();
  });
});
