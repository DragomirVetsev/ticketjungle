import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { UserModel } from 'src/app/models/UserModel';

describe('UserComponent', () => {
  let service: UserService;
  let httpTestingController: HttpTestingController;
  let username: string;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService],
    });
    service = TestBed.inject(UserService);
    httpTestingController = TestBed.inject(HttpTestingController);
    username = '';
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get users', () => {
    service.getUsers().subscribe({
      next: (response: UserModel[]) => {
        expect(response).toEqual([]);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/users`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });

  it('should handle empty user list', () => {
    service.getUsers().subscribe(response => {
      expect(response).toEqual([]);
    });

    const request = httpTestingController.expectOne(
      `${environment.restApi}/users`
    );
    request.flush([]);
  });
});
