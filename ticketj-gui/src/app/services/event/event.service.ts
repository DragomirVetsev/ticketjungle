import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { mapStageDTOToStageModel } from 'src/app/mapping-utilities/stage-mapper';
import { CartItemId } from 'src/app/models/CartItemId';
import { EventModol } from 'src/app/models/EventModel';
import { PerformerModel } from 'src/app/models/PerformerModel';
import { StageModel } from 'src/app/models/StageModel';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { SubEventPairModel } from 'src/app/models/SubEventPairModel';
import { StageDTO } from 'src/app/models/dto/dto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  private readonly eventsUrl = environment.restApi + '/events';
  private readonly subEventsUrl = `${environment.restApi}/events`;

  constructor(private readonly httpClient: HttpClient) {}

  getEventsUrl(): string {
    return this.eventsUrl;
  }

  getEventByName(name: string): Observable<EventModol> {
    return this.httpClient.get<EventModol>(`${this.eventsUrl}/${name}`);
  }

  getEvents(): Observable<EventModol[]> {
    return this.httpClient.get<EventModol[]>(this.eventsUrl);
  }

  getPerformers(): Observable<PerformerModel[]> {
    return this.httpClient.get<PerformerModel[]>(
      `${environment.restApi}/referential/performers`
    );
  }

  getSubEvents(eventName: string): Observable<SubEventModel[]> {
    const subEventsUrl = `${environment.restApi}/events/${eventName}/sub-events`;
    return this.httpClient.get<SubEventModel[]>(subEventsUrl);
  }

  getSubEventsOrdered(): Observable<SubEventPairModel> {
    const subEventTimeUrl = `${environment.restApi}/events/sub-events/ordered`;
    return this.httpClient.get<SubEventPairModel>(subEventTimeUrl);
  }

  getSubEventSeats(event: string, subEvent: string): Observable<StageModel> {
    const subEventSeatsUrl = `${environment.restApi}/events/${event}/sub-events/${subEvent}/seats`;

    return this.httpClient
      .get<StageDTO>(subEventSeatsUrl)
      .pipe(map((response: StageDTO) => mapStageDTOToStageModel(response)));
  }

  createEvent(event: EventModol): Observable<EventModol> {
    return this.httpClient.post<EventModol>(this.eventsUrl, event);
  }

  createSubEvent(
    subEvent: SubEventModel,
    eventName: string
  ): Observable<SubEventModel> {
    const subEventsEndpoint = `${this.subEventsUrl}/${eventName}/sub-events`;
    return this.httpClient.post<SubEventModel>(subEventsEndpoint, subEvent);
  }

  updateEvent(event: EventModol): Observable<EventModol> {
    return this.httpClient.patch<EventModol>(
      `${this.eventsUrl}/${event.name}`,
      event
    );
  }

  updateSubEvent(subEvent: SubEventModel): Observable<SubEventModel> {
    return this.httpClient.patch<SubEventModel>(
      `${this.eventsUrl}/${subEvent.performanceId.eventName}/sub-events/${subEvent.performanceId.name}`,
      subEvent
    );
  }

  deleteSubEvent(subEvent: SubEventModel): Observable<SubEventModel> {
    return this.httpClient.delete<SubEventModel>(
      `${this.eventsUrl}/${subEvent.performanceId.eventName}/sub-events/${subEvent.performanceId.name}`
    );
  }

  createCartItem(cartItemId: CartItemId[]) {
    const createCartItemURL = `${this.eventsUrl}/${cartItemId[0].performance.eventName}/sub-events/addcart`; // Assuming cartItemId is an array
    return this.httpClient.post<CartItemId[]>(createCartItemURL, cartItemId); // Remove the 'body' property
  }
}
