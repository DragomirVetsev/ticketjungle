import { TestBed } from '@angular/core/testing';

import { EventService } from './event.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { StageModel } from 'src/app/models/StageModel';
import { EventModol, Genre } from 'src/app/models/EventModel';
import { PerformanceModel } from 'src/app/models/PerformanceModel';
import { AddressModel } from 'src/app/models/AddressModel';
import { ScoreAndCountModel } from 'src/app/models/ScoreAndCountModel';
import { PerformanceId } from 'src/app/models/PerformanceId';

describe('EventService', () => {
  let service: EventService;
  let httpTestingController: HttpTestingController;
  let eventName: string;
  let testGenre: Genre;
  let testEvent: EventModol;
  let performanceId: PerformanceId;
  let performanceDetails: PerformanceModel;
  let venueAddress: AddressModel;
  let reviewScoreAndCount: ScoreAndCountModel;
  let testSubEvent: SubEventModel;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EventService],
    });
    service = TestBed.inject(EventService);
    httpTestingController = TestBed.inject(HttpTestingController);
    eventName = '';
    testGenre = { value: 'pop' };
    testEvent = {
      name: 'testEvent',
      description: 'Test Event Description',
      genre: testGenre,
    };
    venueAddress = {
      city: 'Sofia',
      postCode: '1000',
      street: 'Test Street',
    };
    reviewScoreAndCount = {
      score: 0,
      count: 0,
    };
    performanceId = {
      name: 'Test Sub Event',
      eventName: 'testEvent',
    };
    performanceDetails = {
      venue: 'NDK',
      stage: 'Zala 1',
      startDateTime: new Date(),
    };
    testSubEvent = {
      performanceId: performanceId,
      performanceDetails: performanceDetails,
      description: 'Test sub event for rendering loaded sub events.',
      ticketPrice: 1,
      venueAddress: venueAddress,
      performers: [''],
      reviewScoreAndCount: reviewScoreAndCount,
    };
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should getEventsUrl()', () => {
    expect(service.getEventsUrl()).toEqual(`${environment.restApi}/events`);
  });

  it('should get events', () => {
    service.getEvents().subscribe({
      next: response => {
        expect(response).toEqual([]);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/events`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });

  it('should get sub events', () => {
    service.getSubEvents(eventName).subscribe({
      next: response => {
        expect(response).toEqual([]);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/events/${eventName}/sub-events`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });

  it('should getEventByName()', () => {
    service.getEventByName(eventName).subscribe({
      next: response => {
        expect(response).toBeTruthy;
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/events/${eventName}`
    );
    expect(request.request.method).toEqual('GET');
  });

  it('should get sub-event seat state', () => {
    const stageModel: StageModel = {
      venueName: 'NDK',
      stageName: 'Zala 31',
      city: 'Sofia',
      sectors: [],
      address: { city: '', postCode: '', street: '' },
    };
    const subEvent = '';
    service.getSubEventSeats('', '').subscribe({
      next: response => {
        expect(response).toBeTruthy;
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/events/${eventName}/sub-events/${subEvent}/seats`
    );
  });

  it('should call the API and return the response as StageModel', (done: DoneFn) => {
    const event = 'example_event';
    const subEvent = 'example_sub_event';

    const mockStageDTO = {
      venueName: '',
      stageName: '',
      address: {
        city: '',
        postCode: '',
        street: '',
      },
      city: '',
      sectors: [],
    };

    const expectedStageModel: StageModel = {
      venueName: '',
      stageName: '',
      address: {
        city: '',
        postCode: '',
        street: '',
      },
      city: '',
      sectors: [],
    };

    service.getSubEventSeats(event, subEvent).subscribe(result => {
      expect(result).toEqual(expectedStageModel);
      done();
    });

    const subEventSeatsUrl = `${environment.restApi}/events/${event}/sub-events/${subEvent}/seats`;

    const req = httpTestingController.expectOne(subEventSeatsUrl);
    expect(req.request.method).toEqual('GET');

    req.flush(mockStageDTO);
  });

  it('should createEvent()', () => {
    service.createEvent(testEvent).subscribe({
      next: response => {
        expect(response).toEqual(testEvent);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/events`
    );
    expect(request.request.method).toEqual('POST');
    request.flush(testEvent);
  });

  it('should updateEvent()', () => {
    service.updateEvent(testEvent).subscribe({
      next: response => {
        expect(response).toEqual(testEvent);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/events/${testEvent.name}`
    );
    expect(request.request.method).toEqual('PATCH');
    request.flush(testEvent);
  });

  it('should updateSubEvent()', () => {
    service.updateSubEvent(testSubEvent).subscribe({
      next: response => {
        expect(response).toEqual(testSubEvent);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/events/${testEvent.name}/sub-events/${testSubEvent.performanceId.name}`
    );
    expect(request.request.method).toEqual('PATCH');
    request.flush(testSubEvent);
  });

  it('should deleteSubEvent()', () => {
    service.deleteSubEvent(testSubEvent).subscribe({
      next: response => {
        expect(response).toEqual(testSubEvent);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/events/${testEvent.name}/sub-events/${testSubEvent.performanceId.name}`
    );
    expect(request.request.method).toEqual('DELETE');
    request.flush(testSubEvent);
  });
});
