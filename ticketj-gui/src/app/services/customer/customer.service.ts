import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AddressModel } from 'src/app/models/AddressModel';
import { CartModel } from 'src/app/models/CartModel';
import { Credentials } from 'src/app/models/Credentials';
import { CustomerModel } from 'src/app/models/CustomerModel';
import { CustomerWithCredentials } from 'src/app/models/CustomerWithCredentials';
import { OrderWithTicketsModel } from 'src/app/models/OrderWithTicketsModel';
import { PerformanceId } from 'src/app/models/PerformanceId';
import { ReviewModel } from 'src/app/models/ReviewModel';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { TicketModel } from 'src/app/models/TicketModel';
import { PhoneModel } from 'src/app/models/UserModel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  getExpirationDate: any;
  autoLogout(autoLogout: any) {
    throw new Error('Method not implemented.');
  }
  constructor(private readonly httpClient: HttpClient) {}

  getCustomer(username: string): Observable<CustomerModel> {
    const urlWithUsername = `${environment.restApi}/customers/${username}`;
    return this.httpClient.get<CustomerModel>(urlWithUsername);
  }

  putCustomer(customer: CustomerWithCredentials): Observable<CustomerModel> {
    const urlWithUsername = `${environment.restApi}/customers/register`;
    return this.httpClient.put<CustomerModel>(urlWithUsername, customer);
  }

  updateCustomerAddress(
    customerName: string,
    customer: AddressModel
  ): Observable<CustomerModel> {
    const jsonData = JSON.stringify({
      address: customer,
    });
    const receivedObject = JSON.parse(jsonData);
    const urlWithUsername = `${environment.restApi}/customers/${customerName}`;
    return this.httpClient.patch<CustomerModel>(
      urlWithUsername,
      receivedObject
    );
  }

  updateCustomerPhone(
    customerName: string,
    customer: PhoneModel
  ): Observable<CustomerModel> {
    const jsonData = JSON.stringify({
      phoneNumber: customer,
    });
    const receivedObject = JSON.parse(jsonData);
    const urlWithUsername = `${environment.restApi}/customers/${customerName}`;
    return this.httpClient.patch<CustomerModel>(
      urlWithUsername,
      receivedObject
    );
  }

  updateCustomerPassword(
    customerName: string,
    customer: Credentials
  ): Observable<CustomerModel> {
    const jsonData = JSON.stringify({
      credentials: customer,
    });
    const receivedObject = JSON.parse(jsonData);
    const urlWithUsername = `${environment.restApi}/customers/${customerName}`;
    return this.httpClient.patch<CustomerModel>(
      urlWithUsername,
      receivedObject
    );
  }

  updateCustomer(
    customerName: string,
    customer: CustomerWithCredentials
  ): Observable<CustomerModel> {
    const urlWithUsername = `${environment.restApi}/customers/${customerName}`;
    return this.httpClient.patch<CustomerModel>(urlWithUsername, customer);
  }

  getCustomerTickets(customerName: string): Observable<TicketModel[]> {
    const customerTicketsURL = `${environment.restApi}/customers/${customerName}/tickets`;
    return this.httpClient.get<TicketModel[]>(customerTicketsURL);
  }

  getWishlist(username: string): Observable<SubEventModel[]> {
    const customerWishlistsURL = `${environment.restApi}/customers/${username}/wishlist`;
    return this.httpClient.get<SubEventModel[]>(customerWishlistsURL);
  }

  getReviews(username: string): Observable<ReviewModel[]> {
    const customerReviewsURL = `${environment.restApi}/customers/${username}/reviews`;
    return this.httpClient.get<ReviewModel[]>(customerReviewsURL);
  }

  getCart(username: string): Observable<CartModel[]> {
    const customerCartURL = `${environment.restApi}/customers/${username}/cart`;
    return this.httpClient.get<CartModel[]>(customerCartURL);
  }

  createOrder(
    username: string,
    order: OrderWithTicketsModel
  ): Observable<OrderWithTicketsModel> {
    const customerOrderURL = `${environment.restApi}/customers/${username}/order`;
    return this.httpClient.post<OrderWithTicketsModel>(customerOrderURL, order);
  }

  options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  addToWishlist(
    username: string,
    performanceId: PerformanceId
  ): Observable<null> {
    const customerAddToWishlist = `${environment.restApi}/customers/${username}/wishlist/add`;
    return this.httpClient.patch<null>(
      customerAddToWishlist,
      performanceId,
      this.options
    );
  }

  removeFromWishlist(
    username: string,
    performanceId: PerformanceId
  ): Observable<HttpResponse<ResponseType>> {
    const customerRemoveToWishlist = `${environment.restApi}/customers/${username}/wishlist/remove`;
    return this.httpClient.patch<HttpResponse<ResponseType>>(
      customerRemoveToWishlist,
      performanceId,
      this.options
    );
  }

  removeSelectedFromWishlist(
    username: string,
    performanceIds: PerformanceId[]
  ): Observable<HttpResponse<ResponseType>> {
    const customerRemoveToWishlist = `${environment.restApi}/customers/${username}/wishlist/remove/selected`;

    return this.httpClient.patch<HttpResponse<ResponseType>>(
      customerRemoveToWishlist,
      performanceIds,
      this.options
    );
  }
  removeCartItemOfCustomer(username: string, cartTicketModel: CartModel[]) {
    const customerCartRemoveURL = `${environment.restApi}/customers/${username}/cart/remove`;
    return this.httpClient.delete<CartModel[]>(customerCartRemoveURL, {
      body: cartTicketModel,
    });
  }
}
