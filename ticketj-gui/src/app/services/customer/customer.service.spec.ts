import { TestBed } from '@angular/core/testing';

import { CustomerService } from './customer.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import {
  CustomerModel,
  PersonNameModel,
  PhoneModel,
} from 'src/app/models/CustomerModel';

describe('CustomerService', () => {
  let service: CustomerService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CustomerService],
    });
    service = TestBed.inject(CustomerService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should get all customer tickets', () => {
    const testCustomerName = 'testUsername';
    service.getCustomerTickets(testCustomerName).subscribe({
      next: response => {
        expect(response).toEqual([]);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/customers/${testCustomerName}/tickets`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });
  it('should get all customer wishlist', () => {
    const testCustomerName = 'testUsername';
    service.getWishlist(testCustomerName).subscribe({
      next: response => {
        expect(response).toEqual([]);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/customers/${testCustomerName}/wishlist`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });
  it('should get all customer reviews', () => {
    const testCustomerName = 'testUsername';
    service.getReviews(testCustomerName).subscribe({
      next: response => {
        expect(response).toEqual([]);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/customers/${testCustomerName}/reviews`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });

  it('should get all customer cart items', () => {
    const testCustomerName = 'testUsername';
    service.getCart(testCustomerName).subscribe({
      next: response => {
        expect(response).toEqual([]);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/customers/${testCustomerName}/cart`
    );
    expect(request.request.method).toEqual('GET');
    request.flush([]);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get customer', () => {
    const mockPersonName: PersonNameModel = {
      givenName: 'John',
      familyName: 'Doe',
    };

    const mockPhoneNumber: PhoneModel = {
      code: '123',
      number: '555-1234',
    };
    const mockCustomer: CustomerModel = {
      username: 'bobo',
      personName: mockPersonName,
      email: 'some@abv.bg',
      phone: mockPhoneNumber,
      address: {
        street: '123 Main Street',
        city: 'New York',
        postCode: '10001',
      },
      birthday: new Date('1990-01-01'),
    };

    service.getCustomer('bobo').subscribe({
      next: response => {
        expect(response).toEqual(mockCustomer);
      },
    });
    const request = httpTestingController.expectOne(
      `${environment.restApi}/customers/bobo`
    );
    expect(request.request.method).toEqual('GET');
    request.flush(mockCustomer);
  });
});
