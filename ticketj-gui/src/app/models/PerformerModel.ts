export interface PerformerModel {
  name: string;
  description: string;
}
