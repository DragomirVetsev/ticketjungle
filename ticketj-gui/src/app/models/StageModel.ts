import { AddressModel } from './AddressModel';

export interface StageModel {
  venueName: string;
  address: AddressModel;
  city: string;
  stageName: string;
  sectors: SectorModel[];
}

export interface SectorModel {
  name: string;
  rows: RowModel[];
}

export interface RowModel {
  number: number;
  seats: SeatModel[];
}
export interface SeatModel {
  number: number;
  status: string;
}
