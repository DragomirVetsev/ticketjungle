import { Validators } from '@angular/forms';
import { AddressModel } from './AddressModel';
import { UserCredentials } from './UserCredentials';
import { PersonNameModel, PhoneModel } from './UserModel';
import {
  CITY_MAX_LENGTH,
  CITY_MIN_LENGTH,
  CITY_PATTERN,
  FAMILY_NAME_MAX_LENGTH,
  FAMILY_NAME_MIN_LENGTH,
  GIVEN_NAME_MAX_LENGTH,
  GIVEN_NAME_MIN_LENGTH,
  PASSWORD_MAX_LENGTH,
  PASSWORD_MIN_LENGTH,
  PASSWORD_PATTERN,
  PERSON_NAME_PATTERN,
  USERNAME_MAX_LENGTH,
  USERNAME_MIN_LENGTH,
  USERNAME_PATTERN,
} from './ValidationConstants';

export interface CustomerWithCredentials {
  credentials: UserCredentials;
  personName: PersonNameModel;
  email: string;
  phoneNumber: PhoneModel;
  address: AddressModel;
  birthday: string | null;
}

export interface NullableCustomerWithCredentials {
  credentials: NullableUserCredentials;
  personName: NullablePersonNameModel;
  email: string | null;
  phoneNumber: NullablePhoneModel;
  address: NullableAddressModel;
  birthday: string | null;
}

export interface NullableUserCredentials {
  username: string | null;
  password: string | null;
}

export interface NullablePersonNameModel {
  givenName: string | null;
  familyName: string | null;
}

export interface NullablePhoneModel {
  code: string | null;
  number: string | null;
}

export interface NullableAddressModel {
  city: string | null;
  postCode: string | null;
  street: string | null;
}

export const USERNAME_VALIDATOTORS = [
  Validators.required,
  Validators.minLength(USERNAME_MIN_LENGTH),
  Validators.maxLength(USERNAME_MAX_LENGTH),
  Validators.pattern(USERNAME_PATTERN),
];

export const PASSWORD_VALIDATOTORS = [
  Validators.required,
  Validators.minLength(PASSWORD_MIN_LENGTH),
  Validators.maxLength(PASSWORD_MAX_LENGTH),
  Validators.pattern(PASSWORD_PATTERN),
];

export const GIVEN_NAME_VALIDATOTORS = [
  Validators.required,
  Validators.minLength(GIVEN_NAME_MIN_LENGTH),
  Validators.maxLength(GIVEN_NAME_MAX_LENGTH),
  Validators.pattern(PERSON_NAME_PATTERN),
];

export const FAMILY_NAME_VALIDATOTORS = [
  Validators.required,
  Validators.minLength(FAMILY_NAME_MIN_LENGTH),
  Validators.maxLength(FAMILY_NAME_MAX_LENGTH),
  Validators.pattern(PERSON_NAME_PATTERN),
];

export const UNRESCRICTED_CITY_VALIDATOTORS = [
  Validators.required,
  Validators.minLength(CITY_MIN_LENGTH),
  Validators.maxLength(CITY_MAX_LENGTH),
  Validators.pattern(CITY_PATTERN),
];
