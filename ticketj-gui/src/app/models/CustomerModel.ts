import { AddressModel } from './AddressModel';

export interface CustomerModel {
  username: string;
  personName: PersonNameModel;
  email: string;
  phone: PhoneModel;
  address: AddressModel;
  birthday: Date;
}

export interface PersonNameModel {
  givenName: string;
  familyName: string;
}

export interface PhoneModel {
  code: string;
  number: string;
}
