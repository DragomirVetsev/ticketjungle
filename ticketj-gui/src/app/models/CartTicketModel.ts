import { PerformanceModel } from './PerformanceModel';
import { SeatModel } from './StageModel';

export interface CartTicketModel {
  selected: boolean;
  event: string;
  subevent: string;
  performance: PerformanceModel;
  sector: string;
  row: number;
  seat: SeatModel;
  price: number;
}
