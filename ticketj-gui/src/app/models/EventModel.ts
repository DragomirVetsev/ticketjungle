export interface EventModol {
  name: string;
  description?: string;
  genre?: Genre;
}

export interface Genre {
  value: string;
}

export const Genres: Genre[] = [
  { value: 'Rock' },
  { value: 'Pop' },
  { value: 'Theater' },
  { value: 'Tragedy' },
  { value: 'Tehno' },
];

export const getAllGenres = (): string[] => Genres.map(genre => genre.value);
