import { PerformanceId } from './PerformanceId';
import { SeatDTO } from './dto/dto';

export interface CartItemId {
  username: string;
  performance: PerformanceId;
  venue: string;
  stage: string;
  sector: string;
  row: number;
  seat: SeatDTO;
}
