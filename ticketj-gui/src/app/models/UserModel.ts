import { AddressModel } from './AddressModel';
export interface UserModel {
  username: string;
  personName: PersonNameModel;
  email: string;
  contactInfo: ContactInfoModel;
  role: string;
  [key: string]: string | PersonNameModel | ContactInfoModel;
}

export interface ContactInfoModel {
  address?: AddressModel;
  phone?: PhoneModel;
}

export interface PersonNameModel {
  givenName: string;
  familyName: string;
}

export interface PhoneModel {
  code: string;
  number: string;
}

export const Codes: string[] = ['+359', '+40'];
