import { SubEventModel } from './SubEventModel';

export interface SubEventPairModel {
  orderedByTime: SubEventModel[];
  orderedByPopularity: SubEventModel[];
}
