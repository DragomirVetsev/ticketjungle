export interface StaticSearchCriteria {
  cities: string[];
  genres: string[];
}
