export interface AddressModel {
  city: string;
  postCode: string;
  street: string;
}
