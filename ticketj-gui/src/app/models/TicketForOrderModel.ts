import { PerformanceModel } from './PerformanceModel';

export interface TicketForOrderModel {
  event: string;
  subEvent: string;
  performance: PerformanceModel;
  sector: string;
  row: string;
  seatNumber: string;
  discount: string;
}

export const Discounts: string[] = ['NONE', 'KID', 'STUDENT', 'ELDERLY'];
export const getAllDiscounts = (): string[] => Discounts;
