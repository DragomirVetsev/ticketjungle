export interface ReviewModel {
  subEventName: string;
  eventName: string;
  venue: string;
  stage: string;
  startTime: Date;
  comment: string;
  score: number;
}
