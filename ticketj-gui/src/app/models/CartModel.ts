import { PerformanceModel } from './PerformanceModel';
import { SeatModel } from './StageModel';

export interface CartModel {
  event: string;
  subEvent: string;
  performance: PerformanceModel;
  sector: string;
  row: number;
  seat: SeatModel;
  price: number;
}
