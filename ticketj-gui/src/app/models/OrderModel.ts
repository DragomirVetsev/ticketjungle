import { AddressModel } from './AddressModel';
import { PersonNameModel } from './CustomerModel';
import { TicketModel } from './TicketModel';

export interface OrderModel {
  username: string;
  personName: PersonNameModel;
  creationDate: Date;
  paymentType: string;
  deliveryType: string;
  unrestrictedAddress: AddressModel;
  purchaseStatus: string;
  tickets: TicketModel[];
}
export const DeliveryTypes: string[] = [
  'EMAIL_DELIVERY',
  'HOME_DELIVERY',
  'OFFICE_DELIVERY',
];
export const getAllDeliveryTypes = (): string[] => DeliveryTypes;

export const PaymentTypes: string[] = ['BANK TRANSFER', 'DEBIT/CREDIT CARD'];
export const getAllPaymentTypes = (): string[] => PaymentTypes;
