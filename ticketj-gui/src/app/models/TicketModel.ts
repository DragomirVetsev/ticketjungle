import { PerformanceId } from './PerformanceId';
import { PerformanceModel } from './PerformanceModel';
import { SeatDTO } from './dto/dto';

export interface TicketModel {
  orderId: number;
  performanceId: PerformanceId;
  subEventDescription: string;
  performanceDetails: PerformanceModel;
  sector: string;
  row: number;
  seat: SeatDTO;
  price: number;
  discount: DiscountModel;
}
export interface DiscountModel {
  name: string;
  percentage: number;
}
