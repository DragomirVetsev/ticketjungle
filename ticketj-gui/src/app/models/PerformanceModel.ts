export interface PerformanceModel {
  venue: string;
  stage: string;
  startDateTime: Date;
}
