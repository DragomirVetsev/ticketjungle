/**
 * Maximum allowed length for the given name of a person.
 */
export const GIVEN_NAME_MAX_LENGTH = 30;
/**
 * Minimmum allowed length for the given name of a person.
 */
export const GIVEN_NAME_MIN_LENGTH = 2;
/**
 * Maximum allowed length for the family name of a person.
 */
export const FAMILY_NAME_MAX_LENGTH = 30;
/**
 * Minimum allowed length for the family name of a person.
 */
export const FAMILY_NAME_MIN_LENGTH = 2;
/**
 * The regular expression pattern containing the allowed symbols for a persons
 * name. Allows: Only english letters and blank spaces.
 */
export const PERSON_NAME_PATTERN = '[a-zA-Z ]+';
/**
 * Maximum allowed length for the user-name.
 */
export const USERNAME_MAX_LENGTH = 30;
/**
 * Minimum allowed length for the user-name.
 */
export const USERNAME_MIN_LENGTH = 2;
/**
 * The regular expression pattern containing the allowed symbols for email.
 * Allows:[English Upper and lower case letters, symbols ~!@#$%^&*()_+ and
 * numbers with a @ in the middle and a . something at the end]
 */
export const EMAIL_PATTERN =
  '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$';
/**
 * Maximum allowed length for the email.
 */
export const EMAIL_MAX_LENGTH = 30;
/**
 * Minimum allowed length for the email.
 */
export const EMAIL_MIN_LENGTH = 2;
/**
 * The regular expression pattern containing the allowed symbols for email.
 * Allows:[Matches only positive numbers, but the first digit must not be 0]
 */
export const PHONE_NUMBER_PATTERN = '^[1-9]\\d*$';
/**
 * The regular expression pattern containing the allowed symbols for email.
 * Allows:[Which can be 1 to 3 digits or 1 to 4 digits for the area code an
 * optional separator after the area code "-" for matching 1 to 4 digits for the
 * local exchange code and the number in the code are from 1 to 9]
 */
export const PHONE_CODE_PATTERN = '^\\+\\d*[1-9]\\d*$';

/**
 * Maximum allowed length for the phone number.
 */
export const PHONE_NUMBER_MAX_LENGTH = 9;
/**
 * Minimum allowed length for the phone number.
 */
export const PHONE_NUMBER_MIN_LENGTH = 8;
/**
 * Maximum allowed length for the phone code.
 */
export const PHONE_CODE_MAX_LENGTH = 6;
/**
 * Minimum allowed length for the phone code.
 */
export const PHONE_CODE_MIN_LENGTH = 3;
/**
 * The regular expression pattern containing the allowed symbols for user-name.
 * Allows:[English Upper and lower case letters, symbols ~!@#$%^&*()_+ and
 * numbers]
 */
export const USERNAME_PATTERN = '[a-zA-Z~!@#$%^&*()_+0-9]+';

/**
 * The regular expression pattern containing the allowed symbols for password.
 * The password must have at least: One upper case letter, one lower case
 * letter, one symbol and one number
 */
export const PASSWORD_PATTERN =
  '^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()_+])' +
  '[A-Za-z0-9!@#$%^&*()_+]+$';
/**
 * Maximum allowed length for the email.
 */
export const PASSWORD_MAX_LENGTH = 30;
/**
 * Minimum allowed length for the email.
 */
export const PASSWORD_MIN_LENGTH = 10;
/**
 * Maximum allowed length for city.
 */
export const CITY_MAX_LENGTH = 30;

/**
 * Minimum allowed length for city.
 */
export const CITY_MIN_LENGTH = 2;

/**
 * The regular expression pattern containing the allowed symbols for city's
 * name. Allows: Only english letters and blank spaces.
 */
export const CITY_PATTERN = PERSON_NAME_PATTERN;

/**
 * Maximum allowed length for genre.
 */
export const GENRE_MAX_LENGTH = 50;
/**
 * Minimum allowed length for genre.
 */
export const GENRE_MIN_LENGTH = 2;

/**
 * The regular expression pattern containing the allowed symbols for genre.
 * Allows: Only english letters and blank spaces.
 */
export const GENRE_PATTERN = PERSON_NAME_PATTERN;

/**
 * The maximum size of the Event name.
 */
export const EVENT_MAX_LENGTH = 100;

/**
 * The maximum size of the Event name.
 */
export const EVENT_MIN_LENGTH = 5;

/**
 * The regular expression pattern containing the allowed symbols for event's
 * name.Allows: Different language letters, blank spaces, numbers and
 * symbols("'.!?:-).
 */
export const EVENT_NAME_PATTERN = '^[\\p{L} 0-9"\'\\.!\\?:\\-–]+$';

/**
 * The maximum size of event's description.
 */
export const EVENT_DESCRIPTION_MAX_LENGTH = 500;

/**
 * The minimum size of event's description.
 */
export const EVENT_DESCRIPTION_MIN_LENGTH = 8;

/**
 * The maximum size of sub-event's name.
 */
export const SUB_EVENT_NAME_MAX_LENGTH = 100;

/**
 * The minimum size of sub-event's name.
 */
export const SUB_EVENT_NAME_MIN_LENGTH = 5;

/**
 * The regular expression pattern containing the allowed symbols for event's
 * name. * name. Allows: Different language letters, blank spaces, numbers and
 * symbols("'.!?:-).
 */
export const SUB_EVENT_NAME_PATTERN = EVENT_NAME_PATTERN;

/**
 * The maximum length of venue's name.
 */
export const VENUE_MAX_LENGTH = 50;

/**
 * The minimum length of venue's name.
 */
export const VENUE_MIN_LENGTH = 2;
/**
 * The regular expression pattern containing the allowed symbols for venue's
 * name. * name. Allows: Different language letters, blank spaces, numbers and
 * symbols("'.-).
 */
export const VENUE_PATTERN = EVENT_NAME_PATTERN;

/**
 * The maximum length of stage's name.
 */
export const STAGE_MAX_LENGTH = 30;

/**
 * The minimum length of stage's name.
 */
export const STAGE_MIN_LENGTH = 5;

/**
 * The regular expression pattern containing the allowed symbols for stage's
 * name. Allows: Different language letters, blank spaces, numbers and
 * symbols("'.-).
 */
export const STAGE_PATTERN = EVENT_NAME_PATTERN;

/**
 * The minimum allowed street's name length.
 */
export const STREET_NAME_MIN_LENGTH = 5;
/**
 * The maximum allowed street's name length.
 */
export const STREET_NAME_MAX_LENGTH = 50;

/**
 * The pattern for the street. Allows: Different language letters, blank spaces,
 * numbers and symbols("'.-).
 */
export const STREET_PATTERN = '^[\\p{L} 0-9"\'\\.№,\\-]+$';

/**
 * The minimum allowed postcode length.
 */
export const POSTCODE_MIN_LENGTH = 3;
/**
 * The maximum allowed postcode length.
 */
export const POSTCODE_MAX_LENGTH = 10;

/**
 * The pattern for postcode. Allows: Upper case english letters, numbers and
 * symbol -
 */
export const POSTCODE_PATTERN = '^[A-Z0-9-]+$';

/**
 * The maximum allowed performer's name length.
 */
export const PERFORMER_MAX_LENGTH = 30;

/**
 * The minimum allowed performer's name length.
 */
export const PERFORMER_MIN_LENGTH = 2;

/**
 * The pattern for performer's name. Allows any language letter, numbers, and
 * symbols ('\"\\-~!@№$%€§^&*()_+.:;)
 */
export const PERFORMER_PATTERN =
  '[\\p{L}\\p{M}\\p{N}\\s\'"\\-~!@№$%€§^&*()_+.:;]+';

/**
 * The maximum allowed review score decimal digits.
 */
export const REVIEW_SCORE_ALLOWED_DECIMALS = 2;

/**
 * The minimum allowed review score value.
 */
export const REVIEW_SCORE_MIN = 0.0;

/**
 * The maximum allowed review score value.
 */
export const REVIEW_SCORE_MAX = 5.0;
/**
 * The minimum allowed office's name length.
 */
export const OFFICE_MIN_LENGTH = 5;
/**
 * The maximum allowed office's name length.
 */
export const OFFICE_MAX_LENGTH = 30;
/**
 * The pattern for office name.
 */
export const OFFICE_NAME_PATTERN = '^[\\p{L} 0-9"\'\\.№,\\-]+$';
