export interface StageDTO {
  venueName: string;
  address: AddressDTO;
  city: string;
  stageName: string;
  sectors: SectorDTO[];
}

export interface AddressDTO {
  street: string;
  city: string;
  postCode: string;
}

export interface SectorDTO {
  name: string;
  rows: RowDTO[];
}

export interface RowDTO {
  number: number;
  seats: SeatDTO[];
}

export interface SeatDTO {
  number: number;
  status: string;
}
