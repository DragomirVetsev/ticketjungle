import { AddressModel } from './AddressModel';
import { PhoneModel } from './UserModel';
import { Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpStatusCode } from '@angular/common/http';
import {
  EMAIL_MAX_LENGTH,
  EMAIL_MIN_LENGTH,
  EMAIL_PATTERN,
  OFFICE_MAX_LENGTH,
  OFFICE_MIN_LENGTH,
  PHONE_NUMBER_MAX_LENGTH,
  PHONE_NUMBER_MIN_LENGTH,
  PHONE_NUMBER_PATTERN,
  POSTCODE_MAX_LENGTH,
  POSTCODE_MIN_LENGTH,
  POSTCODE_PATTERN,
  STREET_NAME_MAX_LENGTH,
  STREET_NAME_MIN_LENGTH,
} from './ValidationConstants';

export interface OfficeModel {
  officeName: string;
  officeAddress: AddressModel;
  phone: PhoneModel;
  email: string;
  workingHours: PeriodModel;
  workers: string[];
  [key: string]: string | AddressModel | PhoneModel | PeriodModel | string[];
}
export interface PeriodModel {
  startTime: string;
  endTime: string;
}

export interface City {
  value: string;
}

export const CITIES: string[] = [
  'Sofia',
  'Plovdiv',
  'Burgas',
  'Varna',
  'Montana',
  'Stara Zagora',
  'Veliko Turnovo',
];
export const NAME_VALIDATOTORS = [
  Validators.required,
  Validators.minLength(OFFICE_MIN_LENGTH),
  Validators.maxLength(OFFICE_MAX_LENGTH),
  Validators.pattern('^[a-zA-Z 0-9"\'\\.№,\\-]+$'),
];
export const STREET_VALIDATORS = [
  Validators.required,
  Validators.minLength(STREET_NAME_MIN_LENGTH),
  Validators.maxLength(STREET_NAME_MAX_LENGTH),
  Validators.pattern('^[a-zA-Za-åa-ö-w-я 0-9"\'\\.№,\\-]+$'),
];
export const POST_CODE_VALIDATORS = [
  Validators.required,
  Validators.minLength(POSTCODE_MIN_LENGTH),
  Validators.maxLength(POSTCODE_MAX_LENGTH),
  Validators.pattern(POSTCODE_PATTERN),
];
export const CITY_VALIDATORS = [Validators.required];
export const PHONE_CODE_VALIDATORS = [Validators.required];
export const PHONE_NUMBER_VALIDATORS = [
  Validators.required,
  Validators.minLength(PHONE_NUMBER_MIN_LENGTH),
  Validators.maxLength(PHONE_NUMBER_MAX_LENGTH),
  Validators.pattern(PHONE_NUMBER_PATTERN),
];
export const EMAIL_VALIDATORS = [
  Validators.required,
  Validators.minLength(EMAIL_MIN_LENGTH),
  Validators.maxLength(EMAIL_MAX_LENGTH),
  Validators.pattern(EMAIL_PATTERN),
];
export const START_TIME_VALIDATORS = [Validators.required];
export const END_TIME_VALIDATORS = [Validators.required];

export function officeErrorHandling(error: any, snackBar: MatSnackBar): void {
  const ERROR_TYPE = error.error.type;
  const ERROR_DETAILS = error.error.details;

  if (error.status === HttpStatusCode.Conflict) {
    handleConflict(ERROR_TYPE, ERROR_DETAILS, snackBar);
  } else {
    handleUnexpectedError(snackBar);
  }
}

function handleConflict(error: string, details: any, snackBar: MatSnackBar) {
  if (error === 'ENTITY_EXISTS') {
    openErrorSnackBar('name', details.entityId, snackBar);
  } else if (error === 'DUPLICATE_FIELD') {
    const FIELD_TYPE = details.fieldType;
    if (FIELD_TYPE === 'EMAIL') {
      openErrorSnackBar(FIELD_TYPE, details.fieldValue, snackBar);
    } else if (FIELD_TYPE === 'PHONE') {
      const PHONE_VALUE = `${details.fieldValue.code} ${details.fieldValue.number}`;
      openErrorSnackBar(FIELD_TYPE, PHONE_VALUE, snackBar);
    } else {
      const ADDRESS_VALUE = `${details.fieldValue.street}, ${details.fieldValue.city}`;
      openErrorSnackBar(FIELD_TYPE, ADDRESS_VALUE, snackBar);
    }
  } else {
    snackBar.open(
      `Worker '${details.workerUsername}' is already assigned to office '${details.officeName}'!`,
      'Close',
      { duration: SNACKBAR_DURATION }
    );
  }
}
const SNACKBAR_DURATION = 3000;
function handleUnexpectedError(snackBar: MatSnackBar) {
  snackBar.open('Oops! Something went wrong!', 'Close', {
    duration: SNACKBAR_DURATION,
  });
}

function openErrorSnackBar(
  fieldType: string,
  fieldValue: any,
  snackBar: MatSnackBar
) {
  snackBar.open(
    `Office with ${fieldType.toLocaleLowerCase()}: '${fieldValue}' already exists!`,
    'Close',
    { duration: SNACKBAR_DURATION }
  );
}
