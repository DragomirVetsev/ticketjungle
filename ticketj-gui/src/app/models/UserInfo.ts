import { PersonNameModel } from './CustomerModel';

export interface UserInfo {
  username: string;
  fullName: PersonNameModel;
  role: string;
}
