export interface ScoreAndCountModel {
  score: number;
  count: number;
}
