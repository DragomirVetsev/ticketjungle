import { AddressModel } from './AddressModel';
import { TicketForOrderModel } from './TicketForOrderModel';

export interface OrderWithTicketsModel {
  paymentType: string;
  deliveryType: string;
  deliveryAddress: AddressModel;
  ticketsForOrderDTOs: TicketForOrderModel[];
}

export const DeliveryTypes: string[] = [
  'EMAIL_DELIVERY',
  'HOME_DELIVERY',
  'OFFICE_DELIVERY',
];
export const getAllDeliveryTypes = (): string[] => DeliveryTypes;

export const PaymentTypes: string[] = ['BANK TRANSFER', 'DEBIT/CREDIT CARD'];
export const getAllPaymentTypes = (): string[] => PaymentTypes;
