import { AddressModel } from './AddressModel';
import { PerformanceId } from './PerformanceId';
import { PerformanceModel } from './PerformanceModel';
import { ScoreAndCountModel } from './ScoreAndCountModel';

export interface SubEventModel {
  performanceId: PerformanceId;
  description?: string;
  ticketPrice?: number;
  performanceDetails: PerformanceModel;
  performers?: string[];
  venueAddress?: AddressModel;
  reviewScoreAndCount?: ScoreAndCountModel;
}
