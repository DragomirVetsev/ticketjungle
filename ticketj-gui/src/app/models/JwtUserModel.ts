import { UserInfo } from './UserInfo';

export interface JwtUserModel {
  userInfo: UserInfo;
  jwtToken: string;
}
