export interface PerformanceId {
  name: string;
  eventName: string;
}
