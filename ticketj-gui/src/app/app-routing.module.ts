import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './modules/user/user-list/user-list.component';
import { IndexComponent } from './modules/index/index.component';
import { CustomerProfileComponent } from './modules/customer/customer-profile/customer-profile.component';
import { EventCreateComponent } from './modules/event/event-create/event-create.component';
import { AccountAuthenticationComponent } from './modules/account/account-authentication/account-authentication.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  {
    path: 'events',
    loadChildren: () =>
      import('./modules/event/event.module').then(m => m.EventModule),
  },
  {
    path: 'customers',
    loadChildren: () =>
      import('./modules/customer/customer.module').then(m => m.CustomerModule),
  },
  {
    path: 'account',
    loadChildren: () =>
      import('./modules/account/account.module').then(m => m.AccountModule),
  },
  {
    path: 'offices',
    loadChildren: () =>
      import('./modules/offices/offices.module').then(m => m.OfficesModule),
  },

  {
    path: 'users',
    component: UserListComponent,
  },
  {
    path: 'customer-profile/:customerName',
    component: CustomerProfileComponent,
  },
  {
    path: 'event-create',
    component: EventCreateComponent,
  },
  {
    path: ' account',
    component: AccountAuthenticationComponent,
  },
  { path: '**', redirectTo: '' },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
