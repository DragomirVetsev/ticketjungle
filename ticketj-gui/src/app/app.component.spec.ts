import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { EventModule } from './modules/event/event.module';
import { HeaderModule } from './modules/header/header.module';
import { MaterialModule } from './modules/materia/material.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTabsModule } from '@angular/material/tabs';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FooterModule } from './modules/footer/footer.module';
import { CustomerModule } from './modules/customer/customer.module';

describe('AppComponent', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        EventModule,
        CustomerModule,
        HeaderModule,
        FooterModule,
        MaterialModule,
        MatGridListModule,
        FlexLayoutModule,
        MatTabsModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        HttpClientModule,
        HttpClientTestingModule,
      ],
      declarations: [AppComponent],
    })
  );

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ticketj-gui'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('ticketj-gui');
  });

  it('should scroll toTop()', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;
    fixture.detectChanges();
    component.toTop();
    expect(window.scrollY).toBe(0);
  });
});
