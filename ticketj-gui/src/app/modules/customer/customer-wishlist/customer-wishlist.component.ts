import {
  AfterViewInit,
  Component,
  ElementRef,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Location } from '@angular/common';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { expandingRowAnimation } from './customer-wishlist-animations';
import { PerformanceId } from 'src/app/models/PerformanceId';
import { finalize } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCheckbox, MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'app-customer-wishlist',
  templateUrl: './customer-wishlist.component.html',
  styleUrls: ['./customer-wishlist.component.css'],
  animations: [expandingRowAnimation],
})
export class CustomerWishlistComponent implements AfterViewInit {
  @ViewChildren('checkbox') checkboxes!: QueryList<MatCheckbox>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('gorilla_hand') gorilla!: ElementRef;
  @ViewChildren('banana') bananas!: QueryList<ElementRef>;
  customerWishlist: SubEventModel[] = [];
  username!: string | null;
  dataSource: any;
  isLoading = false;
  message = 'Handle to Bobo to remove.';
  gorillaName = 'Bobo';
  map = new Map<PerformanceId, any>();
  isIndeterminate = true;
  numberOfSubEvents = 0;
  displayedColumns: string[] = [
    'name',
    'place',
    'startTime',
    'performers',
    'ticketPrice',
    'go-to',
    'expand',
    'banana',
    ' ',
  ];
  columnsToDisplayWithExpand = [...this.displayedColumns];
  expandedElement: SubEventModel | null | undefined;

  constructor(
    private readonly service: CustomerService,
    private readonly router: ActivatedRoute,
    private readonly location: Location,
    private readonly snackBar: MatSnackBar,
    private readonly route: Router
  ) {
    this.router.paramMap.subscribe(params => {
      const customerName = params.get('customerName');
      if (customerName) {
        this.getCustomerInfo(customerName);
      }
      this.username = params.get('customerName');
    });
  }

  ngAfterViewInit(): void {
    const TIMEOUT_DELAY = 3000;
    setTimeout(() => {
      this.dragDrop();
    }, TIMEOUT_DELAY);
  }

  getCustomerInfo(customerName: string) {
    this.isLoading = true;
    this.service
      .getWishlist(customerName)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          this.customerWishlist = response;
          this.numberOfSubEvents += this.customerWishlist.length;

          this.dataSource = new MatTableDataSource<SubEventModel>(
            this.customerWishlist
          );
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        error: error => {
          const notFoundStatusCode = 404;
          if (error.status === notFoundStatusCode) {
            this.location.back();
            this.snackBar.open(
              `${error.error.details.entity} with ID: '${error.error.details.entityId}' is not found!`,
              'Close',
              {
                duration: 10000,
              }
            );
          }
        },
      });
  }
  onClickProfile() {
    const customerName = this.router.snapshot.paramMap.get('customerName');
    this.route.navigate([`/customers/${customerName}`]);
  }

  getEventURL(event: SubEventModel): string {
    return `/events/${event.performanceId.eventName}/sub-events`;
  }

  removeFromWishlist(performanceId: PerformanceId): void {
    const delay = 1000;
    if (this.username != null) {
      this.service.removeFromWishlist(this.username, performanceId).subscribe();
      setTimeout(() => {
        window.location.reload();
      }, delay);
    }
  }

  dragDrop() {
    let selected: HTMLDivElement | null = null;

    this.gorilla.nativeElement.addEventListener('drop', (_event: DragEvent) => {
      if (selected) {
        this.gorilla.nativeElement.appendChild(selected);
        const names = selected.textContent?.split('|');
        if (names) {
          const subEventNameTrimmed = names[0].trim();
          const eventNameTrimmed = names[1].trim();
          const performanceId: PerformanceId = {
            name: subEventNameTrimmed,
            eventName: eventNameTrimmed,
          };
          this.message = '';
          if (this.username !== null) {
            this.removeRowByPerformanceId(performanceId);
            this.service
              .removeFromWishlist(this.username, performanceId)
              .subscribe();
          }
          const delay = 500;
          setTimeout(() => {
            if (selected) {
              selected.remove();
            }
          }, delay);
        }
      }
    });

    this.bananas.forEach(banana => {
      banana.nativeElement.addEventListener('dragstart', (event: DragEvent) => {
        selected = event.target as HTMLDivElement;
        this.gorilla.nativeElement.addEventListener(
          'dragover',
          (e: DragEvent) => {
            e.preventDefault();
          }
        );
      });
    });
  }

  checkIfAnythingSelected(): boolean {
    return this.map.size === 0;
  }
  modifySelectedEvents(performanceId: PerformanceId, event: any) {
    if (event.checked) {
      this.map.set(performanceId, event);
    } else {
      this.map.delete(performanceId);
    }
  }

  removeRowByPerformanceId(performanceId: PerformanceId): void {
    this.customerWishlist = this.customerWishlist.filter(
      event => event.performanceId.name !== performanceId.name
    );
    this.dataSource.data = this.customerWishlist;
  }

  removeAllFromMap() {
    const performanceIds = [...this.map.keys()];
    performanceIds.forEach(id => {
      this.removeRowByPerformanceId(id);
      this.modifySelectedEvents(id, false);
    });
    if (this.username) {
      this.service
        .removeSelectedFromWishlist(this.username, performanceIds)
        .subscribe();
    }
  }

  unselectAllFromMap() {
    this.customerWishlist.forEach(event => {
      this.map.set(event.performanceId, false);
    });
    this.checkboxes.forEach(checkbox => {
      checkbox.checked = false;
    });
    this.map.clear();
    this.isIndeterminate = true;
  }

  selectAll(event: any) {
    if (event.checked) {
      this.customerWishlist.forEach(performance => {
        this.map.set(performance.performanceId, true);
      });
      this.checkboxes.forEach(checkbox => {
        checkbox.checked = true;
      });
    } else {
      this.unselectAllFromMap();
    }
  }

  makeInterminate(event: MatCheckboxChange) {
    this.isIndeterminate = event.checked ? false : true;
  }
}
