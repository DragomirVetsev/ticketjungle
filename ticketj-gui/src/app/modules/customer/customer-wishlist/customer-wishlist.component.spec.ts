import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CustomerWishlistComponent } from './customer-wishlist.component';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {
  MatCheckboxChange,
  MatCheckboxModule,
} from '@angular/material/checkbox'; // Add MatCheckboxModule import
import { of } from 'rxjs';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonComponentsModule } from '../../common-components/common-components.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatBadgeModule } from '@angular/material/badge';
import { HttpResponse } from '@angular/common/http';

describe('CustomerWishlistComponent', () => {
  let component: CustomerWishlistComponent;
  let fixture: ComponentFixture<CustomerWishlistComponent>;
  let service: CustomerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerWishlistComponent],
      imports: [
        HttpClientTestingModule,
        MatTableModule,
        MatPaginatorModule,
        RouterTestingModule,
        MatCardModule,
        MatSnackBarModule,
        MatSortModule,
        MatBadgeModule,
        BrowserAnimationsModule,
        CommonComponentsModule,
        MatCheckboxModule,
      ],
      providers: [
        {
          provide: CustomerService,
          useValue: {
            getWishlist: (customerUsername: string) => {
              return of([]);
            },
          },
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    service = TestBed.inject(CustomerService);
    fixture = TestBed.createComponent(CustomerWishlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getCustomerInfo', () => {
    const getWishlistSpy = spyOn(service, 'getWishlist').and.returnValue(
      of([])
    );
    const customerUsername = 'testUsername';
    component.getCustomerInfo(customerUsername);
    expect(getWishlistSpy).toHaveBeenCalledWith(customerUsername);
  });

  it('should call unselectAllFromMap', () => {
    const performanceId = { name: 'testName', eventName: 'testEventName' };
    component.map.set(performanceId, true);
    component.unselectAllFromMap();
    expect(component.map.size).toBe(0);
  });

  it('should call selectAll', () => {
    const initialMapSize = component.map.size;
    component.selectAll({ checked: true });
    expect(component.map.size).toBe(
      initialMapSize + component.customerWishlist.length
    );
  });

  it('should call makeInterminate', () => {
    const fakeMatCheckboxChange: MatCheckboxChange = {
      source: null!,
      checked: false,
    };

    component.makeInterminate(fakeMatCheckboxChange);
    expect(component.isIndeterminate).toBe(true);
  });

  it('should remove a row by performanceId', () => {
    // Create a sample SubEventModel and set it as the dataSource
    const samplePerformanceId = {
      name: 'TestEvent',
      eventName: 'TestEventName',
    };
    const sampleRow = {
      performanceId: samplePerformanceId,
      name: 'TestRow',
      description: '',
      ticketPrice: 0,
      performanceDetails: {
        venue: '',
        stage: '',
        startDateTime: new Date(),
      },
    };
    component.customerWishlist = [sampleRow];
    component.dataSource = { data: component.customerWishlist } as any; // Mock MatTableDataSource

    // Call the method to remove the row
    component.removeRowByPerformanceId(samplePerformanceId);

    // Expect the dataSource data to be empty
    expect(component.dataSource.data.length).toBe(0);
  });

  it('should not remove rows with different performanceId', () => {
    const samplePerformanceId = {
      name: 'TestEvent',
      eventName: 'TestEventName',
    };
    const samplePerformanceId1 = {
      name: 'TestEvent1',
      eventName: 'TestEventName1',
    };
    const sampleRow1 = {
      performanceId: samplePerformanceId,
      name: 'TestRow',
      description: '',
      ticketPrice: 0,
      performanceDetails: {
        venue: '',
        stage: '',
        startDateTime: new Date(),
      },
    };
    const sampleRow2 = {
      performanceId: samplePerformanceId1,
      name: 'TestRow',
      description: '',
      ticketPrice: 0,
      performanceDetails: {
        venue: '',
        stage: '',
        startDateTime: new Date(),
      },
    };
    component.customerWishlist = [sampleRow1, sampleRow2];
    component.dataSource = { data: component.customerWishlist } as any; // Mock MatTableDataSource

    // Call the method to remove a row with a specific performanceId
    component.removeRowByPerformanceId(samplePerformanceId);

    // Expect the dataSource data to have only one row left (sampleRow2)
    expect(component.dataSource.data.length).toBe(1);
    expect(component.dataSource.data[0]).toEqual(sampleRow2);
  });
});
