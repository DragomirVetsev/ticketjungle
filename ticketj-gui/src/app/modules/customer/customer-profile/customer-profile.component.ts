import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs';
import { CustomerModel } from 'src/app/models/CustomerModel';
import { LoginService } from 'src/app/services/account/login.service';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'app-customer-profile',
  templateUrl: './customer-profile.component.html',
  styleUrls: ['./customer-profile.component.css'],
})
export class CustomerProfileComponent implements OnInit {
  customerProfile: CustomerModel | null = null;
  isLoading = false;
  role!: string;
  logdInFlag!: boolean;

  constructor(
    private readonly customerService: CustomerService,
    private readonly route: ActivatedRoute // Inject ActivatedRoute
  ) {}

  ngOnInit() {
    this.isLoading = true;

    this.route.paramMap.subscribe(params => {
      const customerName = params.get('customerName');
      if (customerName) {
        this.getCustomerInfo(customerName);
      }
    });
  }

  getCustomerInfo(customerName: string) {
    this.customerService
      .getCustomer(customerName)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(response => {
        this.customerProfile = response;
      });
  }
  goBack() {
    window.history.back();
  }

  chechForToken() {
    this.logdInFlag = true;

    const token = localStorage.getItem('jwtToken');

    if (token) {
      const decodedToken = LoginService.decodeToken(token);
      if (decodedToken?.ROLE) {
        this.role = decodedToken.ROLE;
      }
      this.logdInFlag = false;
    }
  }
}
