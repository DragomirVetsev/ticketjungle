import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerProfileComponent } from './customer-profile.component';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import {
  CustomerModel,
  PersonNameModel,
  PhoneModel,
} from 'src/app/models/CustomerModel';
import { CommonComponentsModule } from '../../common-components/common-components.module';

describe('CustomerProfileComponent', () => {
  let component: CustomerProfileComponent;
  let fixture: ComponentFixture<CustomerProfileComponent>;
  let customerService: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerProfileComponent],
      imports: [CommonComponentsModule, RouterTestingModule],
      providers: [
        {
          provide: CustomerService,
          useValue: {
            getCustomer: (customerUsername: string) => {
              return of({});
            },
          },
        },
      ],
    });

    fixture = TestBed.createComponent(CustomerProfileComponent);
    component = fixture.componentInstance;
    customerService = TestBed.inject(CustomerService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getCustomer()', () => {
    // const mockPersonName: PersonNameModel = {
    //   givenName: 'Boris',
    //   familyName: 'Spaski',
    // };

    // const mockPhoneNumber: PhoneModel = {
    //   code: '123',
    //   number: '555-1234',
    // };
    // const mockCustomer: CustomerModel = {
    //   username: 'bobo',
    //   personName: mockPersonName,
    //   email: 'some@abv.bg',
    //   phone: mockPhoneNumber,
    //   address: {
    //     street: '123 Main Street',
    //     city: 'New York',
    //     postCode: '10001',
    //   },
    //   birthday: new Date('1990-01-01'),
    // };
    const getCustomerSpy = spyOn(
      customerService,
      'getCustomer'
    ).and.callThrough();
    component.getCustomerInfo('bobo');
    expect(getCustomerSpy).toHaveBeenCalledWith('bobo');
  });
});
