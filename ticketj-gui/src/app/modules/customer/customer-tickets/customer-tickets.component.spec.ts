import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerTicketsComponent } from './customer-tickets.component';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { of } from 'rxjs';

describe('CustomerTicketsComponent', () => {
  let component: CustomerTicketsComponent;
  let fixture: ComponentFixture<CustomerTicketsComponent>;
  let service: CustomerService;
  let snackBar: MatSnackBar;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerTicketsComponent],
      providers: [
        {
          provide: CustomerService,
          useValue: {
            getCustomerTickets: (customerUsername: string) => {
              return of([]);
            },
          },
        },
      ],
      imports: [RouterTestingModule, MatSnackBarModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    service = TestBed.inject(CustomerService);
    snackBar = TestBed.inject(MatSnackBar);
    fixture = TestBed.createComponent(CustomerTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call getCustomerTickets', () => {
    const getCustomerTicketsSpy = spyOn(
      service,
      'getCustomerTickets'
    ).and.callThrough();
    const customerUsername = 'testUsername';
    component.getCustomerInfo(customerUsername);
    expect(getCustomerTicketsSpy).toHaveBeenCalledWith(customerUsername);
  });
});
