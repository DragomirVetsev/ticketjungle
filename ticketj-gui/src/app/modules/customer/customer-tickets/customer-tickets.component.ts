import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs';
import { TicketModel } from 'src/app/models/TicketModel';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'app-customer-tickets',
  templateUrl: './customer-tickets.component.html',
  styleUrls: ['./customer-tickets.component.css'],
})
export class CustomerTicketsComponent {
  customerTickets?: TicketModel[];
  pastTickets: TicketModel[] = [];
  futureTickets: TicketModel[] = [];
  isLoading = false;

  constructor(
    private readonly service: CustomerService,
    private readonly router: ActivatedRoute,
    private readonly snackBar: MatSnackBar,
    private readonly location: Location,
    private readonly route: Router
  ) {
    this.router.paramMap.subscribe(params => {
      const customerName = params.get('customerName');
      if (customerName) {
        this.getCustomerInfo(customerName);
      }
    });
  }

  getCustomerInfo(customerName: string) {
    this.isLoading = true;
    this.service
      .getCustomerTickets(customerName)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          this.customerTickets = response;
          this.mapTickets(this.customerTickets);
        },
        error: error => {
          const notFoundStatusCode = 404;
          if (error.status === notFoundStatusCode) {
            this.location.back();
            this.snackBar.open(
              `${error.error.details.entity} with ID: '${error.error.details.entityId}' is not found!`,
              'Close',
              {
                duration: 10000,
              }
            );
          }
        },
      });
  }
  mapTickets(customerTickets: TicketModel[]): void {
    const currentPastTickets: TicketModel[] = [];
    const currentFutureTickets: TicketModel[] = [];
    const currentDate = new Date();
    customerTickets.forEach(ticket => {
      if (new Date(ticket.performanceDetails.startDateTime) < currentDate) {
        currentPastTickets.push(ticket);
      } else {
        currentFutureTickets.push(ticket);
      }
    });
    this.pastTickets = currentPastTickets;
    this.futureTickets = currentFutureTickets;
  }
  onClickProfile() {
    const customerName = this.router.snapshot.paramMap.get('customerName');
    this.route.navigate([`/customers/${customerName}`]);
  }
}
