import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerProfileComponent } from './customer-profile/customer-profile.component';
import { CustomerTicketsComponent } from './customer-tickets/customer-tickets.component';
import { CustomerReviewsComponent } from './customer-reviews/customer-reviews.component';
import { CustomerWishlistComponent } from './customer-wishlist/customer-wishlist.component';
import { CustomerRoutingModule } from './customer-routing';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../materia/material.module';
import { CustomerChooserComponent } from './customer-chooser/customer-chooser.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { TicketsModule } from '../tickets/tickets.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCardModule } from '@angular/material/card';
import { CommonComponentsModule } from '../common-components/common-components.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CustomerCartComponent } from './customer-cart/customer-cart.component';
import { CustomerOrderComponent } from './customer-order/customer-order.component';
import { MatStepperModule } from '@angular/material/stepper';
import { CustomerProfileSettingsComponent } from './customer-profile-settings/customer-profile-settings.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatBadgeModule } from '@angular/material/badge';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { OrderDialogComponent } from './customer-order/order-dialog/order-dialog.component';
@NgModule({
  declarations: [
    CustomerProfileComponent,
    CustomerTicketsComponent,
    CustomerReviewsComponent,
    CustomerWishlistComponent,
    CustomerChooserComponent,
    CustomerCartComponent,
    CustomerOrderComponent,
    CustomerProfileSettingsComponent,
    OrderDialogComponent,
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    TicketsModule,
    MatPaginatorModule,
    MatSortModule,
    MatBadgeModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    MatCardModule,
    MatCheckboxModule,
    MatSnackBarModule,
    CommonComponentsModule,
    MatStepperModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    CustomerProfileComponent,
    CustomerChooserComponent,
    OrderDialogComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class CustomerModule {}
