import { HttpStatusCode } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import jwtDecode from 'jwt-decode';
import { Moment } from 'moment';
import { AddressModel } from 'src/app/models/AddressModel';
import { Credentials } from 'src/app/models/Credentials';
import {
  CustomerWithCredentials,
  GIVEN_NAME_VALIDATOTORS,
  PASSWORD_VALIDATOTORS,
  UNRESCRICTED_CITY_VALIDATOTORS,
} from 'src/app/models/CustomerWithCredentials';
import {
  NAME_VALIDATOTORS,
  PHONE_CODE_VALIDATORS,
  PHONE_NUMBER_VALIDATORS,
  POST_CODE_VALIDATORS,
  STREET_VALIDATORS,
} from 'src/app/models/OfficeModel';
import { Codes, PhoneModel } from 'src/app/models/UserModel';
import { LoginService } from 'src/app/services/account/login.service';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'app-customer-profile-settings',
  templateUrl: './customer-profile-settings.component.html',
  styleUrls: ['./customer-profile-settings.component.css'],
})
export class CustomerProfileSettingsComponent {
  bday!: Moment;
  comfirmPassword!: string;
  customerForm: FormGroup;
  addressForm: FormGroup;
  phoneForm: FormGroup;
  passwordForm: FormGroup;
  PhoneCodes = Codes;
  errorSnack = 'error-snackbar';
  newClient: CustomerWithCredentials = {
    credentials: {
      username: '',
      password: '',
    },
    personName: { givenName: '', familyName: '' },
    email: '',
    phoneNumber: {
      code: '',
      number: '',
    },
    address: { city: '', postCode: '', street: '' },
    birthday: new Date().toISOString(),
  };

  newAddress: AddressModel = {
    city: '',
    postCode: '',
    street: '',
  };

  newPhone: PhoneModel = {
    code: '',
    number: '',
  };

  newPassword: Credentials = {
    username: '',
    password: '',
  };

  constructor(
    readonly service: CustomerService,
    readonly loginService: LoginService,
    private readonly fb: FormBuilder,
    private readonly snackBar: MatSnackBar
  ) {
    this.customerForm = this.initializeForm();
    this.addressForm = this.initAddressForm();
    this.phoneForm = this.initPhoneForm();
    this.passwordForm = this.iliPaswordForm();
  }
  initializeForm(): FormGroup {
    return this.fb.group({
      password: [this.newClient.credentials.username, NAME_VALIDATOTORS],
      street: [this.newClient.personName.givenName, GIVEN_NAME_VALIDATOTORS],
      city: [this.newClient.address.city, UNRESCRICTED_CITY_VALIDATOTORS],
      postCode: [this.newClient.address.postCode, POST_CODE_VALIDATORS],
      phoneCode: [this.newClient.phoneNumber.code, PHONE_CODE_VALIDATORS],
      number: [this.newClient.phoneNumber.number, PHONE_NUMBER_VALIDATORS],
    });
  }
  initAddressForm(): FormGroup {
    return this.fb.group({
      street: [this.newAddress.street, STREET_VALIDATORS],
      city: [this.newAddress.city, UNRESCRICTED_CITY_VALIDATOTORS],
      postCode: [this.newAddress.postCode, POST_CODE_VALIDATORS],
    });
  }
  initPhoneForm(): FormGroup {
    return this.fb.group({
      code: [this.newPhone.code, PHONE_CODE_VALIDATORS],
      number: [this.newPhone.number, PHONE_NUMBER_VALIDATORS],
    });
  }
  iliPaswordForm(): FormGroup<any> {
    return this.fb.group(
      {
        password: [this.newPassword.password, PASSWORD_VALIDATOTORS],
        confirmPassword: [],
      },
      {
        validator: this.confirmedValidator('password', 'confirmPassword'),
      }
    );
  }

  confirmedValidator(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (
        matchingControl.errors &&
        !matchingControl.errors['confirmedValidator']
      ) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  updateAddress() {
    this.mapCustomerAddressPropertie();
    const token = localStorage.getItem('jwtToken');
    if (token) {
      const decodedToken: any = jwtDecode(token);
      this.service
        .updateCustomerAddress(decodedToken.sub, this.newAddress)
        .subscribe({
          next: _response => {
            this.snackBar.open(`Successfully changed`, 'Close', {
              duration: 15000,
              panelClass: this.errorSnack,
            });
          },
          error: error => {
            if (error.status === HttpStatusCode.Conflict) {
              this.snackBar.open(`Data conflict`, 'Close', {
                duration: 15000,
                panelClass: this.errorSnack,
              });
            }
            if (error.status === HttpStatusCode.BadRequest) {
              this.snackBar.open(`Invalid address details`, 'Close', {
                duration: 15000,
                panelClass: this.errorSnack,
              });
            }
          },
        });
    }
  }

  updatePhone() {
    this.mapCustomerPhonePropertie();
    const token = localStorage.getItem('jwtToken');
    if (token) {
      const decodedToken: any = jwtDecode(token);
      this.service
        .updateCustomerPhone(decodedToken.sub, this.newPhone)
        .subscribe({
          next: _response => {
            this.snackBar.open(`Successfully changed`, 'Close', {
              duration: 15000,
              panelClass: this.errorSnack,
            });
          },
          error: error => {
            if (error.status === HttpStatusCode.Conflict) {
              this.snackBar.open(`Data conflict`, 'Close', {
                duration: 15000,
                panelClass: this.errorSnack,
              });
            }
            if (error.status === HttpStatusCode.BadRequest) {
              this.snackBar.open(`Invalid phone details`, 'Close', {
                duration: 15000,
                panelClass: this.errorSnack,
              });
            }
          },
        });
    }
  }
  updateAll() {
    if (this.passwordForm.valid) {
      this.updatePassword();
    }
    if (this.addressForm.valid) {
      this.updateAddress();
    }
    if (this.phoneForm.valid) {
      this.updatePhone();
    }
  }
  updatePassword() {
    this.mapCustomerPhonePassword();
    const token = localStorage.getItem('jwtToken');
    if (token) {
      const decodedToken: any = jwtDecode(token);
      this.newPassword.username = decodedToken.sub;
      this.newPassword.password = this.passwordForm.value.password;
      this.service
        .updateCustomerPassword(decodedToken.sub, this.newPassword)
        .subscribe(response => {});
    }
    this.loginService.logout();
  }

  mapCustomerPhonePassword() {
    this.newPassword.password = this.passwordForm.value.password;
  }
  mapCustomerPhonePropertie() {
    this.newPhone.code = this.phoneForm.value.code;
    this.newPhone.number = this.phoneForm.value.number;
  }
  mapCustomerAddressPropertie() {
    this.newAddress.city = this.addressForm.value.city;
    this.newAddress.postCode = this.addressForm.value.postCode;
    this.newAddress.street = this.addressForm.value.street;
  }
}
