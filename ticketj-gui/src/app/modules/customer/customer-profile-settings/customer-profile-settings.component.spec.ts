import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerProfileSettingsComponent } from './customer-profile-settings.component';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from '../../materia/material.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';
describe('CustomerProfileSettingsComponent', () => {
  let component: CustomerProfileSettingsComponent;
  let fixture: ComponentFixture<CustomerProfileSettingsComponent>;
  let customerService: CustomerService;
  const token: string =
    'eyJhbGciOiJIUzUxMiJ9.eyJST0xFIjoiQ1VTVE9NRVIiLCJJc3N1ZXIiOiJUaWNrZXQgSnVuZ2xlIiwiVXNlcm5hbWUiOiJUaWNrZXRKIFVzZXIiLCJleHAiOjMyNTIzODczMzUxLCJpYXQiOjE2OTI3MjQ1NTF9.xOWN0h1Y0AVtmNodv8BVAOtgj8hiM_U6xw4ytVpyMtnfhfVbIcGEbKub2-m21BTyJRSk8JeI8C5ORNGd0hIkMA';
  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [CustomerProfileSettingsComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        NoopAnimationsModule,
        MatSnackBarModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    customerService = TestBed.inject(CustomerService);
    fixture = TestBed.createComponent(CustomerProfileSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update address', () => {
    spyOn(localStorage, 'getItem').and.returnValue(token);

    component.updateAddress();

    expect(localStorage.getItem).toHaveBeenCalledWith('jwtToken');
  });

  it('should update phone', () => {
    spyOn(localStorage, 'getItem').and.returnValue(token);

    component.updatePhone();

    expect(localStorage.getItem).toHaveBeenCalledWith('jwtToken');
  });

  it('should update password', () => {
    spyOn(localStorage, 'getItem').and.returnValue(token);
    spyOn(component.loginService, 'logout');

    component.passwordForm.value.password = 'new-password';

    component.updatePassword();

    expect(localStorage.getItem).toHaveBeenCalledWith('jwtToken');

    expect(component.loginService.logout).toHaveBeenCalled();
  });
});
