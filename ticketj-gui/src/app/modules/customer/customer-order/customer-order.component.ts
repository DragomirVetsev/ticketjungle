import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { CartModel } from 'src/app/models/CartModel';
import { CustomerModel } from 'src/app/models/CustomerModel';
import {
  DeliveryTypes,
  OrderWithTicketsModel,
  PaymentTypes,
  getAllDeliveryTypes,
  getAllPaymentTypes,
} from 'src/app/models/OrderWithTicketsModel';
import {
  Discounts,
  TicketForOrderModel,
  getAllDiscounts,
} from 'src/app/models/TicketForOrderModel';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { OrderDialogComponent } from './order-dialog/order-dialog.component';
import { MatDialog } from '@angular/material/dialog';

interface CombinedModel {
  ticket: TicketForOrderModel;
  ticketFromCart: CartModel;
  originalPrice: number;
}
@Component({
  selector: 'app-customer-order',
  templateUrl: './customer-order.component.html',
  styleUrls: ['./customer-order.component.css'],
})
export class CustomerOrderComponent {
  hasTickets = false;
  dataSource: MatTableDataSource<CombinedModel>;

  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    city: ['', [Validators.required, Validators.pattern(/^[A-Za-z\s]+$/)]],
    street: ['', Validators.required],
    postCode: ['', [Validators.required, Validators.pattern(/^\d{4}$/)]],
  });
  combinedFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });
  isLinear = false;
  ticketsForOrder: TicketForOrderModel[] = [];
  ticketsFromCart: CartModel[] = [];
  discount: string = Discounts[0];
  discountOptions: string[] = getAllDiscounts();
  deliveryType: string = DeliveryTypes[0];
  deliveryOptions: string[] = getAllDeliveryTypes();
  paymentType: string = PaymentTypes[0];
  paymentOptions: string[] = getAllPaymentTypes();

  customerProfile: CustomerModel;

  orderWithTickets: OrderWithTicketsModel = {
    paymentType: '',
    deliveryType: '',
    deliveryAddress: {
      city: '',
      postCode: '',
      street: '',
    },
    ticketsForOrderDTOs: this.ticketsForOrder,
  };

  displayedColumns: string[] = [
    'performance',
    'details',
    'seat',
    'discount',
    'price',
  ];
  constructor(
    private readonly customerService: CustomerService,
    private readonly snackBar: MatSnackBar,
    private readonly _formBuilder: FormBuilder,
    private readonly route: Router,
    private readonly router: ActivatedRoute,
    private readonly dialog: MatDialog
  ) {
    this.ticketsFromCart =
      this.route.getCurrentNavigation()?.extras.state?.['ticketsFromCart'];

    this.customerProfile =
      this.route.getCurrentNavigation()?.extras.state?.['customerProfile'];

    if (this.ticketsFromCart) {
      this.ticketsFromCart.forEach((ticket: CartModel) => {
        const ticketForOrder: TicketForOrderModel = {
          event: ticket.event,
          subEvent: ticket.subEvent,
          performance: ticket.performance,
          sector: ticket.sector,
          row: ticket.row.toString(),
          seatNumber: ticket.seat.number.toString(),
          discount: this.discount,
        };

        this.ticketsForOrder.push(ticketForOrder);
      });
    }
    this.hasTickets = this.ticketsForOrder.length > 0;

    const combinedData: CombinedModel[] = this.ticketsForOrder.map(
      (ticket, index) => ({
        ticket,
        ticketFromCart: this.ticketsFromCart[index],
        originalPrice: this.ticketsFromCart[index].price,
      })
    );

    this.dataSource = new MatTableDataSource<CombinedModel>(combinedData);
  }
  updatePrice(combinedModel: CombinedModel) {
    let percentDiscount = 0;
    const KID_DISCOUNT_PERCENTAGE = 20;
    const ELDERLY_DISCOUNT_PERCENTAGE = 15;
    const STUDENT_DISCOUNT_PERCENTAGE = 10;
    if (combinedModel.ticket.discount === 'KID') {
      percentDiscount = KID_DISCOUNT_PERCENTAGE;
    } else if (combinedModel.ticket.discount === 'STUDENT') {
      percentDiscount = STUDENT_DISCOUNT_PERCENTAGE;
    } else if (combinedModel.ticket.discount === 'ELDERLY') {
      percentDiscount = ELDERLY_DISCOUNT_PERCENTAGE;
    } else {
      percentDiscount = 0;
    }
    const PERCENTAGE_DIVISOR = 100;
    combinedModel.ticketFromCart.price =
      combinedModel.originalPrice -
      (combinedModel.originalPrice * percentDiscount) / PERCENTAGE_DIVISOR;
  }
  onClickCart() {
    const customerName = this.router.snapshot.paramMap.get('customerName');
    this.route.navigate([`/customers/${customerName}/cart`]);
  }
  createOrder() {
    const customerName = this.router.snapshot.paramMap.get('customerName');
    if (customerName === null) {
      throw new Error('Customer name not found in the URL');
    }

    this.customerService
      .createOrder(customerName, this.orderWithTickets)
      .subscribe({
        next: (response: any) => {
          const dialogRef = this.dialog.open(OrderDialogComponent, {
            data: { responseMessage: response },
          });

          dialogRef.afterClosed().subscribe(() => {
            const customerName =
              this.router.snapshot.paramMap.get('customerName');
            this.route.navigate([`/customers/${customerName}//tickets`]);
          });
        },
        error: _error => {
          this.snackBar.open('Order NOT created!', 'Close', {
            duration: 3000,
            panelClass: 'error-snackbar',
          });
        },
      });
  }
}
