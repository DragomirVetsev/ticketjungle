import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDialogComponent } from './order-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('OrderDialogComponent', () => {
  let component: OrderDialogComponent;
  let fixture: ComponentFixture<OrderDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderDialogComponent],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            responseMessage: {
              purchaseStatus: 'success',
              personName: {
                givenName: 'John',
                familyName: 'Doe',
              },
              unrestrictedAddress: {
                street: '123 Main Street',
                city: 'Example City',
                postCode: '12345',
              },
            },
          },
        },
        { provide: MatDialogRef, useValue: {} },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(OrderDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
