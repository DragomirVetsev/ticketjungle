import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrderModel } from 'src/app/models/OrderModel';

@Component({
  selector: 'app-order-dialog',
  templateUrl: './order-dialog.component.html',
  styleUrls: ['./order-dialog.component.css'],
})
export class OrderDialogComponent {
  order: OrderModel;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<OrderDialogComponent>
  ) {
    this.order = data.responseMessage as OrderModel;
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}
