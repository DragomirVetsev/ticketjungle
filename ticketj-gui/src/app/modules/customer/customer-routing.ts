import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerProfileComponent } from './customer-profile/customer-profile.component';
import { CustomerTicketsComponent } from './customer-tickets/customer-tickets.component';
import { CustomerWishlistComponent } from './customer-wishlist/customer-wishlist.component';
import { CustomerReviewsComponent } from './customer-reviews/customer-reviews.component';
import { CustomerChooserComponent } from './customer-chooser/customer-chooser.component';
import { CustomerCartComponent } from './customer-cart/customer-cart.component';
import { CustomerOrderComponent } from './customer-order/customer-order.component';
import { CustomerProfileSettingsComponent } from './customer-profile-settings/customer-profile-settings.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerChooserComponent,
    //all customers component to be added
  },
  {
    path: ':customerName',
    component: CustomerProfileComponent,
  },
  {
    path: ':customerName/tickets',
    component: CustomerTicketsComponent,
  },
  {
    path: ':customerName/wishlist',
    component: CustomerWishlistComponent,
  },
  {
    path: ':customerName/reviews',
    component: CustomerReviewsComponent,
  },
  {
    path: ':customerName/cart',
    component: CustomerCartComponent,
  },
  {
    path: ':customerName/order',
    component: CustomerOrderComponent,
},
{
    path: ':customerName/settings',
    component: CustomerProfileSettingsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerRoutingModule {}
