import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerChooserComponent } from './customer-chooser.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';

describe('CustomerChooserComponent', () => {
  let component: CustomerChooserComponent;
  let fixture: ComponentFixture<CustomerChooserComponent>;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerChooserComponent],
      imports: [
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        BrowserAnimationsModule,
      ],
    });
    fixture = TestBed.createComponent(CustomerChooserComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to /customers/username when username is provided', () => {
    component.username = 'bobo';
    const navigateSpy = spyOn(router, 'navigate');

    component.navigateToCustomer();

    expect(navigateSpy).toHaveBeenCalledWith(['/customers', 'bobo']);
  });

  it('should navigate to /customers when username is not provided', () => {
    component.username = '';
    const navigateSpy = spyOn(router, 'navigate');

    component.navigateToCustomer();

    expect(navigateSpy).toHaveBeenCalledWith(['/customers']);
  });
});
