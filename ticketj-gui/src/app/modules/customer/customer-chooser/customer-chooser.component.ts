import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-chooser',
  templateUrl: './customer-chooser.component.html',
})
export class CustomerChooserComponent {
  username = '';
  customersBaseUrl = '/#/customers/';

  constructor(private readonly router: Router) {}

  navigateToCustomer() {
    // Navigate to /customers/username using the Angular Router
    if (this.username) {
      this.router.navigate(['/customers', this.username]);
    } else {
      this.router.navigate(['/customers']);
    }
  }
}
