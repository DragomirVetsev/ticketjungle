import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CustomerReviewsComponent } from './customer-reviews.component';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { of } from 'rxjs';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonComponentsModule } from '../../common-components/common-components.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('CustomerReviewsComponent', () => {
  let component: CustomerReviewsComponent;
  let fixture: ComponentFixture<CustomerReviewsComponent>;
  let service: CustomerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CustomerReviewsComponent],
      imports: [
        HttpClientTestingModule,
        MatTableModule,
        RouterTestingModule,
        MatCardModule,
        MatPaginatorModule,
        MatSnackBarModule,
        MatSortModule,
        BrowserAnimationsModule,
        CommonComponentsModule,
      ],
      providers: [
        {
          provide: CustomerService,
          useValue: {
            getReviews: (customerUsername: string) => {
              return of([]);
            },
          },
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    service = TestBed.inject(CustomerService);
    fixture = TestBed.createComponent(CustomerReviewsComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call getCustomerReviews', () => {
    const getWishlistSpy = spyOn(service, 'getReviews').and.callThrough();
    const customerUsername = 'testUsername';
    component.getCustomerInfo(customerUsername);
    expect(getWishlistSpy).toHaveBeenCalledWith(customerUsername);
  });
});
