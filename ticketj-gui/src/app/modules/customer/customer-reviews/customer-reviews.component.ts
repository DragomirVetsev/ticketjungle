import { Component, ViewChild } from '@angular/core';
import { ReviewModel } from 'src/app/models/ReviewModel';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Location } from '@angular/common';
import { finalize } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-customer-reviews',
  templateUrl: './customer-reviews.component.html',
  styleUrls: ['./customer-reviews.component.css'],
})
export class CustomerReviewsComponent {
  customerReviews: ReviewModel[] = [];
  username?: string;
  dataSource: any;
  hasReviews = false;
  isLoading = false;

  displayedColumns: string[] = [
    'subEventName',
    'place',
    'startTime',
    'comment',
    'score',
  ];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private readonly service: CustomerService,
    private readonly router: ActivatedRoute,
    private readonly route: Router,
    private readonly location: Location,
    private readonly snackBar: MatSnackBar
  ) {
    this.router.paramMap.subscribe(params => {
      const customerName = params.get('customerName');
      if (customerName) {
        this.getCustomerInfo(customerName);
      }
    });
  }

  getCustomerInfo(customerName: string) {
    this.isLoading = true;
    this.service
      .getReviews(customerName)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          this.customerReviews = response;
          this.hasReviews = this.customerReviews.length > 0;
          this.dataSource = new MatTableDataSource<ReviewModel>(
            this.customerReviews
          );
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        error: error => {
          const notFoundStatusCode = 404;
          if (error.status === notFoundStatusCode) {
            this.location.back();
            this.snackBar.open(
              `${error.error.details.entity} with ID: '${error.error.details.entityId}' is not found!`,
              'Close',
              {
                duration: 10000,
              }
            );
          }
        },
      });
  }

  getStarIcons(score: number): string {
    if (score === null) {
      return 'Vote now!';
    }

    const maxScore = 5;
    const filledStars = Math.min(Math.round(score), maxScore);
    const emptyStars = Math.max(maxScore - filledStars, 0);
    return '★'.repeat(filledStars) + '☆'.repeat(emptyStars);
  }

  onClickProfile() {
    const customerName = this.router.snapshot.paramMap.get('customerName');
    this.route.navigate([`/customers/${customerName}`]);
  }
}
