import { Component, ViewChild } from '@angular/core';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Location } from '@angular/common';
import { finalize } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { CartModel } from 'src/app/models/CartModel';
import { CartTicketModel } from 'src/app/models/CartTicketModel';
import { CustomerModel } from 'src/app/models/CustomerModel';

@Component({
  selector: 'app-customer-cart',
  templateUrl: './customer-cart.component.html',
  styleUrls: ['./customer-cart.component.css'],
})
export class CustomerCartComponent {
  customerCart: CartModel[] = [];
  selectedItems: CartModel[] = [];
  forDTO: CartTicketModel[] = [];
  username?: string;
  dataSource: MatTableDataSource<CartModel>;
  hasCart = false;
  isLoading = false;
  allComplete = false;
  customerProfile: CustomerModel | null = null;
  item: CartModel;

  displayedColumns: string[] = [
    'select',
    'performance',
    'details',
    'seat',
    'status',
    'price',
  ];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private readonly service: CustomerService,
    private readonly router: ActivatedRoute,
    private readonly route: Router,
    private readonly location: Location,
    private readonly snackBar: MatSnackBar
  ) {
    this.dataSource = new MatTableDataSource<CartModel>([]);
    this.item = {
      event: '',
      subEvent: '',
      performance: {
        venue: '',
        stage: '',
        startDateTime: new Date(),
      },
      sector: '',
      row: 0,
      seat: {
        number: 0,
        status: '',
      },
      price: 0.0,
    };
    this.router.paramMap.subscribe(params => {
      const customerName = params.get('customerName');
      if (customerName) {
        this.getCustomerInfo(customerName);
      }
    });
  }

  getCustomerInfo(customerName: string) {
    this.isLoading = true;

    this.service.getCustomer(customerName).subscribe(response => {
      this.customerProfile = response;
    });

    this.service
      .getCart(customerName)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          this.customerCart = response;
          this.hasCart = this.customerCart.length > 0;
          this.dataSource = new MatTableDataSource<CartModel>(
            this.customerCart
          );
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        error: error => {
          const notFoundStatusCode = 404;
          if (error.status === notFoundStatusCode) {
            this.location.back();
            this.snackBar.open(
              `${error.error.details.entity} with ID: '${error.error.details.entityId}' is not found!`,
              'Close',
              {
                duration: 10000,
              }
            );
          }
        },
      });
  }

  getSelectedItems(): CartModel[] {
    return this.selectedItems;
  }
  onCheckboxChange(event: any, item: CartModel) {
    const checkboxElement = event.target;
    if (checkboxElement.checked) {
      this.selectedItems.push(item);
    } else {
      const index = this.selectedItems.findIndex(cartItem => cartItem === item);
      if (index !== -1) {
        this.selectedItems.splice(index, 1);
      }
    }
  }

  onDeleteSelectedItems() {
    const customerName = this.router.snapshot.paramMap.get('customerName');
    if (customerName !== null) {
      this.service
        .removeCartItemOfCustomer(customerName, this.selectedItems)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe({
          next: response => {
            this.selectedItems = response;
            this.hasCart = this.customerCart.length > 0;

            this.dataSource = new MatTableDataSource<CartModel>(
              this.customerCart
            );
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            window.location.reload();

            const customerName =
              this.router.snapshot.paramMap.get('customerName');
            if (customerName) {
              this.getCustomerInfo(customerName);
            }
          },
          error: error => {
            const notFoundStatusCode = 404;
            if (error.status === notFoundStatusCode) {
              this.location.back();
              this.snackBar.open(
                `${error.error.details.entity} with ID: '${error.error.details.entityId}' is not found!`,
                'Close',
                {
                  duration: 10000,
                }
              );
            }
          },
        });
    } else {
      console.error('customerName is null');
    }
    this.selectedItems = [];
  }

  onClickProfile() {
    const customerName = this.router.snapshot.paramMap.get('customerName');
    this.route.navigate([`/customers/${customerName}`]);
  }
  onClickOrder() {
    const customerName = this.router.snapshot.paramMap.get('customerName');

    if (this.customerProfile) {
      // Only navigate if customerProfile is loaded
      this.route.navigate([`/customers/${customerName}/order`], {
        state: {
          ticketsFromCart: this.selectedItems,
          customerProfile: this.customerProfile,
        },
      });
    } else {
      // Handle the case where customerProfile is not loaded yet
      console.error('Customer profile is not loaded yet.');
    }
  }
}
