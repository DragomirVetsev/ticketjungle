import { CommonModule, NgFor } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { EventService } from 'src/app/services/event/event.service';
import { EventModule } from '../event/event.module';
import { RouterModule } from '@angular/router';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { MatCardModule } from '@angular/material/card';
import { CommonComponentsModule } from '../common-components/common-components.module';
import { finalize } from 'rxjs';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  containingEvent: string;
}

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  standalone: true,
  imports: [
    MatGridListModule,
    NgFor,
    EventModule,
    RouterModule,
    MatCardModule,
    CommonModule,
    CommonComponentsModule,
  ],
})
export class IndexComponent implements OnInit {
  isLoading = true;
  subEventByTime: SubEventModel[] = [];
  subEventByPopularity: SubEventModel[] = [];
  tiles: Tile[] = [];

  constructor(private readonly eventService: EventService) {}
  ngOnInit() {
    this.eventService
      .getSubEventsOrdered()
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(response => {
        const subEventPair = response;
        this.subEventByTime = subEventPair.orderedByTime;
        this.subEventByPopularity = subEventPair.orderedByPopularity;
        this.initializeTiles(this.tiles, this.subEventByTime);
      });
  }
  initializeTiles(tiles: Tile[], subEventList: SubEventModel[]) {
    let size = 0;
    tiles.push({
      text: subEventList[size]?.performanceId.name,
      cols: 3,
      rows: 1,
      color: 'lightblue',
      containingEvent: subEventList[size]?.performanceId.eventName,
    });
    size++;
    tiles.push({
      text: subEventList[size]?.performanceId.name,
      cols: 1,
      rows: 2,
      color: 'lightpink',
      containingEvent: subEventList[size]?.performanceId.eventName,
    });
    size++;
    tiles.push({
      text: subEventList[size]?.performanceId.name,
      cols: 2,
      rows: 1,
      color: 'lightgreen',
      containingEvent: subEventList[size]?.performanceId.eventName,
    });
    size++;
    tiles.push({
      text: subEventList[size]?.performanceId.name,
      cols: 1,
      rows: 1,
      color: '#DDBDF1',
      containingEvent: subEventList[size]?.performanceId.eventName,
    });
  }
}
