import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexComponent } from './index.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { EventService } from 'src/app/services/event/event.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgFor } from '@angular/common';
import { of } from 'rxjs';

describe('IndexComponent', () => {
  let component: IndexComponent;
  let fixture: ComponentFixture<IndexComponent>;
  let eventService: EventService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatGridListModule, NgFor, HttpClientTestingModule],
      providers: [EventService],
    }).compileComponents();

    eventService = TestBed.inject(EventService);
    fixture = TestBed.createComponent(IndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    eventService = TestBed.inject(EventService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call getSubEventsOrdered()', () => {
    const getSubEventsOrderedSpy = spyOn(eventService, 'getSubEventsOrdered')
      .and.callThrough()
      .and.returnValue(of({ orderedByTime: [], orderedByPopularity: [] }));
    component.ngOnInit();
    expect(getSubEventsOrderedSpy).toHaveBeenCalled();
  });
  it('should call initializeTiles()', () => {
    const initializeTilesSpy = spyOn(
      component,
      'initializeTiles'
    ).and.callThrough();
    component.initializeTiles([], []);
    expect(initializeTilesSpy).toHaveBeenCalled();
  });
});
