import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableTitleComponent } from './table-title/table-title.component';
import { MonkeyDropComponent } from './monkey-drop/monkey-drop.component';
import { LoadingBufferComponent } from './loading-buffer/loading-buffer.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    TableTitleComponent,
    MonkeyDropComponent,
    LoadingBufferComponent,
  ],
  imports: [CommonModule, MatProgressSpinnerModule],
  exports: [TableTitleComponent, MonkeyDropComponent, LoadingBufferComponent],
})
export class CommonComponentsModule {}
