import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingBufferComponent } from './loading-buffer.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('LoadingBufferComponent', () => {
  let component: LoadingBufferComponent;
  let fixture: ComponentFixture<LoadingBufferComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoadingBufferComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(LoadingBufferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
