import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonkeyDropComponent } from './monkey-drop.component';

describe('MonkeyDropComponent', () => {
  let component: MonkeyDropComponent;
  let fixture: ComponentFixture<MonkeyDropComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MonkeyDropComponent]
    });
    fixture = TestBed.createComponent(MonkeyDropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
