import { Component } from '@angular/core';

@Component({
  selector: 'app-monkey-drop',
  templateUrl: './monkey-drop.component.html',
  styleUrls: ['./monkey-drop.component.css'],
})
// export class MonkeyDropComponent {
//   monkeyXposition = '0';
//   maxLeftPosition = 70;
//   ngOnInit() {
//     this.monkeyXposition = `${
//       Math.floor(Math.random() * this.maxLeftPosition) + 1
//     }vw`;
//   }
// }
export class MonkeyDropComponent {
  monkeyXposition = '0';
  ngOnInit(): void {
    const maxLeftPosition = 70;
    const maxUnasignedIntegerValue = 4294967296;

    const randomValue = new Uint32Array(1);
    window.crypto.getRandomValues(randomValue);
    const randomPosition =
      Math.floor(
        (randomValue[0] / maxUnasignedIntegerValue) * maxLeftPosition
      ) + 1;
    this.monkeyXposition = `${randomPosition}vw`;
  }
}
