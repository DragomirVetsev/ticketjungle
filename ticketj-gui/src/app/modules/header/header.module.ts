import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../materia/material.module';
import { MainHeaderComponent } from './main-header/main-header.component';
import { RouterModule } from '@angular/router';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { EventModule } from '../event/event.module';

@NgModule({
  declarations: [MainHeaderComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatListModule,
    EventModule,
  ],

  exports: [MainHeaderComponent],
})
export class HeaderModule {}
