import { Component, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { EventSearchComponent } from '../../event/event-search/event-search.component';

import { EventCreateComponent } from '../../event/event-create/event-create.component';

import { AccountAuthenticationComponent } from '../../account/account-authentication/account-authentication.component';

import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';
import { LoginService } from 'src/app/services/account/login.service';

@Component({
  selector: 'app-main-header',

  templateUrl: './main-header.component.html',

  styleUrls: ['./main-header.component.css'],
})
export class MainHeaderComponent implements OnInit {
  logdInFlag!: boolean;
  username!: string;
  logedUsername!: string;
  role!: string;

  constructor(
    private readonly router: Router,
    public dialog: MatDialog,
    private readonly loginService: LoginService
  ) {}

  ngOnInit() {
    this.chechForToken();
  }

  openDialog() {
    this.dialog.open(EventSearchComponent);
  }

  openCreateDialog() {
    this.dialog.open(EventCreateComponent);
  }

  openAccountAuthDialog() {
    this.chechForToken();

    this.dialog.open(AccountAuthenticationComponent);
  }

  chechForToken() {
    this.logdInFlag = true;

    const token = localStorage.getItem('jwtToken');

    if (token) {
      const decodedToken = LoginService.decodeToken(token);
      if (decodedToken && decodedToken['ROLE']) {
        this.role = decodedToken['ROLE'];
      }
      this.logdInFlag = false;
    }
  }

  ngDoCheck() {
    this.chechForToken();
  }

  logOut() {
    this.loginService.logout();
  }

  getUsername(): string {
    const token = localStorage.getItem('jwtToken');
    if (token) {
      try {
        const decodedToken: any = jwt_decode(token);

        return decodedToken.sub;
      } catch (error) {
        return 'null';
      }
    } else {
      return 'null';
    }
  }

  getGivenName(): string | null {
    const givenName = localStorage.getItem('givenName');
    if (givenName) {
      return givenName;
    } else {
      return null;
    }
  }

  getFamilyName(): string | null {
    const familyName = localStorage.getItem('familyName');
    if (familyName) {
      return familyName;
    } else {
      return null;
    }
  }

  toProfile() {
    this.username = this.getUsername();

    if (this.username) {
      this.router.navigate(['/customers', this.username]);
    }
  }

  toProfileSettings() {
    this.username = this.getUsername();

    if (this.username) {
      this.router.navigate([`/customers/${this.username}/settings`]);
    }
  }
}
