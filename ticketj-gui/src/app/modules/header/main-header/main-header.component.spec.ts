import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainHeaderComponent } from './main-header.component';
import { MatIconModule } from '@angular/material/icon';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from '../../materia/material.module';
import { LoginService } from 'src/app/services/account/login.service';
import { of } from 'rxjs';

describe('MainHeaderComponent', () => {
  let component: MainHeaderComponent;
  let fixture: ComponentFixture<MainHeaderComponent>;
  let router: Router;
  let loginService: LoginService;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [MainHeaderComponent],
      imports: [
        HttpClientTestingModule,
        MatIconModule,
        RouterTestingModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        NoopAnimationsModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    router = TestBed.inject(Router) as any;
    fixture = TestBed.createComponent(MainHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    loginService = TestBed.inject(LoginService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate toProfile()', () => {
    const username = 'bobo';
    const navigateSpy = spyOn(router, 'navigate');
    const getUsernameSpy = spyOn(component, 'getUsername')
      .and.callThrough()
      .and.returnValue(username);

    component.username = username;
    component.toProfile();

    expect(getUsernameSpy).toHaveBeenCalled();
    expect(navigateSpy).toHaveBeenCalledWith(['/customers', username]);
  });

  it('should logOut()', () => {
    const logoutSpy = spyOn(loginService, 'logout').and.callThrough();

    component.logOut();

    expect(logoutSpy).toHaveBeenCalled();
  });

  it('should getUsername() throws error', () => {
    const getUsernameSpy = spyOn(localStorage, 'getItem')
      .and.callThrough()
      .and.returnValue('token');

    component.getUsername();

    expect(getUsernameSpy).toHaveBeenCalled();
  });

  it('should open dialog when openDialog clicked()', () => {
    const openDialogSpy = spyOn(component.dialog, 'open');

    component.openDialog();

    expect(openDialogSpy).toHaveBeenCalled();
  });

  it('should open dialog when openCreateDialog clicked()', () => {
    const openDialogSpy = spyOn(component.dialog, 'open');

    component.openCreateDialog();

    expect(openDialogSpy).toHaveBeenCalled();
  });
});
