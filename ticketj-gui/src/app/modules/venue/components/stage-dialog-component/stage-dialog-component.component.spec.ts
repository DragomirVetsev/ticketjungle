import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';
import { StageDialogComponent } from './stage-dialog-component.component';

describe('StageDialogComponent', () => {
  let component: StageDialogComponent;
  let fixture: ComponentFixture<StageDialogComponent>;

  const data = {
    stageModel: {
      venueName: 'Test Venue',
      stageName: 'Test Stage',
    },
    event: 'Test Event',
    subEvent: 'Test SubEvent',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule, StageDialogComponent], // Import your module here
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: data },
        {
          provide: MatDialogRef,
          useValue: {
            close: jasmine.createSpy('close'),
          },
        },
      ],
    });
    fixture = TestBed.createComponent(StageDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges(); // Trigger change detection
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog', () => {
    component.onNoClick();
    expect(component.dialogRef.close).toHaveBeenCalled();
  });
});
