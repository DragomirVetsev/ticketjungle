import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StageModel } from 'src/app/models/StageModel';
import { SectorComponent } from '../sector/sector.component';
import { CommonModule } from '@angular/common';
import { StageComponent } from '../stage/stage.component';
import { MatButtonModule } from '@angular/material/button';
@Component({
  selector: 'app-stage-dialog-component',
  templateUrl: './stage-dialog-component.component.html',
  styleUrls: ['./stage-dialog-component.component.css'],
  standalone: true,
  imports: [SectorComponent, CommonModule, StageComponent, MatButtonModule],
})
export class StageDialogComponent {
  stageModel: StageModel;
  event: string;
  subEvent: string;

  constructor(
    public dialogRef: MatDialogRef<StageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.stageModel = data.stageModel;
    this.event = data.event;
    this.subEvent = data.subEvent;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
