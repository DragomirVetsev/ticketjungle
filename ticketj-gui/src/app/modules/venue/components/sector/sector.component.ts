import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { RowModel, SeatModel, SectorModel } from 'src/app/models/StageModel';
import { CartItemId } from 'src/app/models/CartItemId';
import { CommonModule, Location } from '@angular/common';
import { EventService } from 'src/app/services/event/event.service';
import jwtDecode from 'jwt-decode';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-sector',
  templateUrl: './sector.component.html',
  styleUrls: ['./sector.component.css'],
  imports: [MatDialogModule, MatButtonModule, CommonModule],
  standalone: true,
})
export class SectorComponent {
  public sector: SectorModel;
  selectedSeat: CartItemId[] = [];
  actualUsername = '';
  public stage: string;
  public venue: string;
  public event: string;
  public subEvent: string;

  constructor(
    private readonly eventService: EventService,
    private readonly snackBar: MatSnackBar,
    private readonly location: Location,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      sectorHolder: SectorModel;
      stage: string;
      venue: string;
      event: string;
      subEvent: string;
    }
  ) {
    this.sector = data.sectorHolder;
    this.stage = data.stage;
    this.venue = data.venue;
    this.event = data.event;
    this.subEvent = data.subEvent;
  }

  getUsername(): string {
    const token = localStorage.getItem('jwtToken');
    if (token) {
      const decodedToken: any = jwtDecode(token);
      console.log(decodedToken.sub);
      return decodedToken.sub;
    }
    return '';
  }

  createCartItemId(
    seat: SeatModel,
    row: RowModel,
    sector: SectorModel
  ): CartItemId {
    const cartItemId: CartItemId = {
      username: this.getUsername(),
      performance: {
        name: this.subEvent,
        eventName: this.event,
      },
      venue: this.venue,
      stage: this.stage,
      sector: sector.name,
      row: row.number,
      seat: seat,
    };
    return cartItemId;
  }

  onSeatClick(
    seat: SeatModel,
    row: RowModel,
    sector: SectorModel,
    stage: string
  ) {
    const cartItemId = this.createCartItemId(seat, row, sector);
    if (cartItemId.seat.status === 'AVAILABLE') {
      cartItemId.seat.status = 'SELECTED';
      this.selectedSeat.push(cartItemId);
      console.log(this.selectedSeat);
    } else if (cartItemId.seat.status === 'SELECTED') {
      cartItemId.seat.status = 'AVAILABLE';
      const index = this.selectedSeat.indexOf(cartItemId);
      if (index !== -1) {
        this.selectedSeat.splice(index, 1);
        console.log(this.selectedSeat);
      }
    } else {
      console.log(`Seat already reserved.`);
    }
  }

  addToCart() {
    for (const cartItemId of this.selectedSeat) {
      cartItemId.seat.status = 'AVAILABLE';
    }

    this.eventService.createCartItem(this.selectedSeat).subscribe(
      response => {},
      error => {
        const notFoundStatusCode = 404;
        if (error.status === notFoundStatusCode) {
          this.snackBar.open(
            `While you wait, some of the tickets you are trying to reserve have already been taken! Please try again!'`,
            'Close',
            {
              duration: 10000,
            }
          );
        }
      }
    );

    this.selectedSeat = [];
  }

  closeDialog() {
    for (const cartItemId of this.selectedSeat) {
      cartItemId.seat.status = 'AVAILABLE';
    }
    console.log(this.selectedSeat);
    this.selectedSeat = [];
  }
}
