import {
  ComponentFixture,
  TestBed,
  ComponentFixtureAutoDetect,
} from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { SectorComponent } from './sector.component';

describe('SectorComponent', () => {
  let component: SectorComponent;
  let fixture: ComponentFixture<SectorComponent>;

  const mockData = {
    sectorHolder: {}, // You can provide meaningful data for testing here
    stage: 'Test Stage',
    venue: 'Test Venue',
    event: 'Test Event',
    subEvent: 'Test SubEvent',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatButtonModule,
        MatDialogModule,
        CommonModule,
        HttpClientTestingModule,
      ],
      declarations: [],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: mockData },
        MatSnackBar,
        { provide: ComponentFixtureAutoDetect, useValue: true },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    fixture = TestBed.createComponent(SectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set input data correctly', () => {
    expect(component.stage).toBe(mockData.stage);
    expect(component.venue).toBe(mockData.venue);
    expect(component.event).toBe(mockData.event);
    expect(component.subEvent).toBe(mockData.subEvent);
  });
});
