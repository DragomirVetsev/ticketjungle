import { Component, Input } from '@angular/core';
import { StageModel } from 'src/app/models/StageModel';
import { SectorComponent } from '../../sector/sector.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
@Component({
  selector: 'app-theatre-stoyan-buchvarov-main-hall',
  templateUrl: './theatre-stoyan-buchvarov-main-hall.component.html',
  styleUrls: [
    './theatre-stoyan-buchvarov-main-hall.component.css',
    '../stage.component.css',
  ],
  standalone: true,
  imports: [CommonModule, SectorComponent, MatButtonModule],
})
export class TheatreStoyanBuchvarovMainHallComponent {
  @Input() stageModel = {} as StageModel;
  @Input() event!: string;
  @Input() subEvent!: string;
}
