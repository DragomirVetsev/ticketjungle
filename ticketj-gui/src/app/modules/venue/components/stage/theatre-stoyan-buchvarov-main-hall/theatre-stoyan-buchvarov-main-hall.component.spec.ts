import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TheatreStoyanBuchvarovMainHallComponent } from './theatre-stoyan-buchvarov-main-hall.component';

describe('TheatreStoyanBuchvarovMainHallComponent', () => {
  let component: TheatreStoyanBuchvarovMainHallComponent;
  let fixture: ComponentFixture<TheatreStoyanBuchvarovMainHallComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TheatreStoyanBuchvarovMainHallComponent],
    });
    fixture = TestBed.createComponent(TheatreStoyanBuchvarovMainHallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
