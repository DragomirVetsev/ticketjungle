import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NdkZala3Component } from './ndk-zala3.component';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { SectorComponent } from '../../sector/sector.component';
import { SectorModel } from 'src/app/models/StageModel';

describe('NdkZala3Component', () => {
  let component: NdkZala3Component;
  let fixture: ComponentFixture<NdkZala3Component>;
  const data = {
    sectorHolder: [],
  };
  const sectorModel: SectorModel = {
    name: '',
    rows: [
      {
        number: 1,
        seats: [
          {
            number: 1,
            status: 'AVAILABLE',
          },
        ],
      },
    ],
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NdkZala3Component,
        CommonModule,
        SectorComponent,
        MatButtonModule,
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: data },
        { provide: MatDialogRef, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(NdkZala3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('open dialog test', () => {
    const openDialogSpy = spyOn(component.dialog, 'open');

    component.openDialog(sectorModel);

    const expectedData = {
      sectorHolder: sectorModel,
      stage: component.stageModel.stageName,
      venue: component.stageModel.venueName,
      event: component.event,
      subEvent: component.subEvent,
    };

    expect(openDialogSpy).toHaveBeenCalledWith(jasmine.anything(), {
      data: expectedData,
    });
  });

  it('should calculate the status', () => {
    const status = component.calculateStatus(sectorModel);
    expect(status).toEqual('sector-available');
  });
});
