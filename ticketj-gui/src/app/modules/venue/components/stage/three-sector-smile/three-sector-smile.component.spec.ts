import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { ThreeSectorSmileComponent } from './three-sector-smile.component';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { SectorComponent } from '../../sector/sector.component';
import { CommonModule } from '@angular/common';
import { RowModel, SeatModel, SectorModel } from 'src/app/models/StageModel';

describe('ThreeSectorSmileComponent', () => {
  let component: ThreeSectorSmileComponent;
  let fixture: ComponentFixture<ThreeSectorSmileComponent>;
  const data = {
    sectorHolder: [],
  };

  const sectorModel: SectorModel = {
    rows: [
      {
        seats: [
          { status: 'AVAILABLE' } as SeatModel,
          { status: 'ACQUIRED' } as SeatModel,
          { status: 'RESERVED' } as SeatModel,
          { status: 'RESERVED' } as SeatModel,
          { status: 'SOME_OTHER_STATUS' } as SeatModel,
        ],
      } as RowModel,
    ],
    name: '',
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, ThreeSectorSmileComponent, SectorComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: data },
        { provide: MatDialogRef, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(ThreeSectorSmileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('open dialog test', () => {
    const openDialogSpy = spyOn(component.dialog, 'open');

    component.openDialog(sectorModel);

    const expectedData = {
      sectorHolder: sectorModel,
      stage: component.stageModel.stageName,
      venue: component.stageModel.venueName,
      event: component.event,
      subEvent: component.subEvent,
    };

    expect(openDialogSpy).toHaveBeenCalledWith(jasmine.anything(), {
      data: expectedData,
    });
  });

  it('should calculate status correctly', () => {
    component.stageModel.sectors = [sectorModel];
    const status = component.calculateStatus(component.stageModel.sectors[0]);
    expect(status).toEqual('sector-available');
  });
});
