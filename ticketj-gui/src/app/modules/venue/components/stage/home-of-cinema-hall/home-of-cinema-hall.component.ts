import { Component, Input } from '@angular/core';
import { StageModel } from 'src/app/models/StageModel';
import { SectorComponent } from '../../sector/sector.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
@Component({
  selector: 'app-home-of-cinema-hall',
  templateUrl: './home-of-cinema-hall.component.html',
  styleUrls: ['./home-of-cinema-hall.component.css', '../stage.component.css'],
  standalone: true,
  imports: [CommonModule, SectorComponent, MatButtonModule],
})
export class HomeOfCinemaHallComponent {
  @Input() stageModel = {} as StageModel;
  @Input() event!: string;
  @Input() subEvent!: string;
}
