import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeOfCinemaHallComponent } from './home-of-cinema-hall.component';

describe('HomeOfCinemaHallComponent', () => {
  let component: HomeOfCinemaHallComponent;
  let fixture: ComponentFixture<HomeOfCinemaHallComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HomeOfCinemaHallComponent]
    });
    fixture = TestBed.createComponent(HomeOfCinemaHallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
