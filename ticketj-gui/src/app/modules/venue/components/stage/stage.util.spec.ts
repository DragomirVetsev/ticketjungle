import { SectorModel } from "src/app/models/StageModel";
import { calculateStatus } from "./stage.util";


describe('calculateStatus', () => {
  it('should return "sector-available" when there are available seats', () => {
    const sectorModel: SectorModel = {
      name: 'Sector 1',
      rows: [
        {
          number: 1,
          seats: [
            { status: 'AVAILABLE', number: 1 },
            { status: 'RESERVED', number: 2 },
            { status: 'AVAILABLE', number: 3 },
            { status: 'ACQUIRED', number: 4 },
          ],
        },
      ],
    };
    const result = calculateStatus(sectorModel);

    expect(result).toEqual('sector-available');
  });

  it('should return "sector-acquired" when all seats are acquired', () => {
    const sectorModel: SectorModel = {
      name: 'Sector 1',
      rows: [
        {
          number: 1,
          seats: [
            { status: 'ACQUIRED', number: 1 },
            { status: 'ACQUIRED', number: 2 },
            { status: 'ACQUIRED', number: 3 },
            { status: 'ACQUIRED', number: 4 },
          ],
        },
      ],
    };

    const result = calculateStatus(sectorModel);

    expect(result).toEqual('sector-acquired');
  });

  it('should return "sector-reserved" when there are reserved seats but not all seats are acquired', () => {
    const sectorModel: SectorModel = {
      name: 'Sector 1',
      rows: [
        {
          number: 1,
          seats: [
            { status: 'ACQUIRED', number: 1 },
            { status: 'RESERVED', number: 2 },
            { status: 'RESERVED', number: 3 },
            { status: 'RESERVED', number: 4 },
          ],
        },
      ],
    };

    const result = calculateStatus(sectorModel);

    expect(result).toEqual('sector-reserved');
  });

  it('should return an empty string when the sector model has no seats', () => {
    const sectorModel: SectorModel = {
      rows: [{number: 1, seats:[{number: 1, status:'Invalid status'}]}],
      name: '',
    };

    const result = calculateStatus(sectorModel);

    expect(result).toEqual('sector-acquired');
  });
});
