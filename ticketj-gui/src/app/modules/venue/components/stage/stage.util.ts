import { SectorModel } from 'src/app/models/StageModel';
export function calculateStatus(secorModel: SectorModel): string {
  let availableSeats = 0;
  let acquiredSeats = 0;
  let reservedSeats = 0;

  let allSeats = 0;
  for (const row of secorModel.rows) {
    for (const seat of row.seats) {
      switch (seat.status) {
        case 'AVAILABLE':
          availableSeats++;
          allSeats++;
          break;
        case 'ACQUIRED':
          acquiredSeats++;
          allSeats++;
          break;
        case 'RESERVED':
          reservedSeats++;
          allSeats++;
          break;
        default:
          break;
      }
    }
  }

  if (availableSeats > 0) {
    return 'sector-available';
  } else if (acquiredSeats === allSeats) {
    return 'sector-acquired';
  } else {
    return 'sector-reserved';
  }
}
