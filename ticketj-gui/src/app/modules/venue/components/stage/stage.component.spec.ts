import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StageComponent } from './stage.component';
import { CommonModule } from '@angular/common';
import { NdkZala1Component } from './ndk-zala1/ndk-zala1.component';
import { NdkZala3Component } from './ndk-zala3/ndk-zala3.component';
import { ThreeSectorSmileComponent } from './three-sector-smile/three-sector-smile.component';
import { IvanVazovNationalTheatreComponent } from './ivan-vazov-national-theatre/ivan-vazov-national-theatre.component';
import { SectorModel, SeatModel, RowModel } from 'src/app/models/StageModel';
import { SectorComponent } from '../sector/sector.component';

describe('StageComponent', () => {
  let component: StageComponent;
  let fixture: ComponentFixture<StageComponent>;

  const sectorModel: SectorModel = {
    rows: [
      {
        seats: [
          { status: 'AVAILABLE' } as SeatModel,
          { status: 'ACQUIRED' } as SeatModel,
          { status: 'RESERVED' } as SeatModel,
          { status: 'RESERVED' } as SeatModel,
          { status: 'SOME_OTHER_STATUS' } as SeatModel,
        ],
      } as RowModel,
    ],
    name: '',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        StageComponent,
        NdkZala1Component,
        NdkZala3Component,
        ThreeSectorSmileComponent,
        IvanVazovNationalTheatreComponent,
      ],
    });
    fixture = TestBed.createComponent(StageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('open dialog test', () => {
    const openDialogSpy = spyOn(component.dialog, 'open');

    component.openDialog(sectorModel);

    expect(openDialogSpy).toHaveBeenCalledWith(SectorComponent, {
      data: { sectorHolder: sectorModel },
    });
  });
});
