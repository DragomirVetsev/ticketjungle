import { Component, Input } from '@angular/core';
import { SectorModel, StageModel } from 'src/app/models/StageModel';
import { CommonModule } from '@angular/common';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { SectorComponent } from '../sector/sector.component';
import { NdkZala1Component } from './ndk-zala1/ndk-zala1.component';
import { NdkZala3Component } from './ndk-zala3/ndk-zala3.component';
import { IvanVazovNationalTheatreComponent } from './ivan-vazov-national-theatre/ivan-vazov-national-theatre.component';
import { ThreeSectorSmileComponent } from './three-sector-smile/three-sector-smile.component';
import { HomeOfCinemaHallComponent } from './home-of-cinema-hall/home-of-cinema-hall.component';
import { TheatreStoyanBuchvarovMainHallComponent } from './theatre-stoyan-buchvarov-main-hall/theatre-stoyan-buchvarov-main-hall.component';
import { FestivalAndCongressCentreHallComponent } from './festival-and-congress-centre-hall/festival-and-congress-centre-hall.component';

@Component({
  selector: 'app-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.css'],
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    NdkZala1Component,
    NdkZala3Component,
    IvanVazovNationalTheatreComponent,
    ThreeSectorSmileComponent,
    HomeOfCinemaHallComponent,
    TheatreStoyanBuchvarovMainHallComponent,
    FestivalAndCongressCentreHallComponent,
  ],
})
export class StageComponent {
  @Input() stageModel = {} as StageModel;
  @Input() event = '';
  @Input() subEvent = '';

  constructor(public dialog: MatDialog) {}

  public openDialog(sector: SectorModel) {
    this.dialog.open(SectorComponent, {
      data: {
        sectorHolder: sector,
      },
    });
  }
}
