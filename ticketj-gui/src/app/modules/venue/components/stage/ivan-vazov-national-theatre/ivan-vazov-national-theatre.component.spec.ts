import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { IvanVazovNationalTheatreComponent } from './ivan-vazov-national-theatre.component';
import { SectorModel } from 'src/app/models/StageModel';
import { SectorComponent } from '../../sector/sector.component';

describe('IvanVazovNationalTheatreComponent', () => {
  let component: IvanVazovNationalTheatreComponent;
  let fixture: ComponentFixture<IvanVazovNationalTheatreComponent>;

  const sectorModel: SectorModel = {
    name: '',
    rows: [
      {
        number: 1,
        seats: [
          {
            number: 1,
            status: 'AVAILABLE',
          },
        ],
      },
    ],
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [], // Rimuovi la dichiarazione del componente da qui
      imports: [MatDialogModule, CommonModule, MatButtonModule],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            sectorHolder: sectorModel,
          },
        },
        { provide: MatDialogRef, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(IvanVazovNationalTheatreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('open dialog test', () => {
    const openDialogSpy = spyOn(component.dialog, 'open');

    component.openDialog(sectorModel);

    const expectedData = {
      sectorHolder: sectorModel,
      stage: component.stageModel.stageName,
      venue: component.stageModel.venueName,
      event: component.event,
      subEvent: component.subEvent,
    };

    expect(openDialogSpy).toHaveBeenCalledWith(jasmine.anything(), {
      data: expectedData,
    });
  });

  it('should calculate the status', () => {
    const status = component.calculateStatus(sectorModel);
    expect(status).toEqual('sector-available');
  });
});
