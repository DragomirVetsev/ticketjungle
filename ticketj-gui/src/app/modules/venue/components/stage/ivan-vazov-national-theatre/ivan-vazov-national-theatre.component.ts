import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { StageModel, SectorModel } from 'src/app/models/StageModel';
import { SectorComponent } from '../../sector/sector.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { calculateStatus } from '../stage.util';
@Component({
  selector: 'app-ivan-vazov-national-theatre',
  templateUrl: './ivan-vazov-national-theatre.component.html',
  styleUrls: [
    './ivan-vazov-national-theatre.component.css',
    '../stage.component.css',
  ],
  standalone: true,
  imports: [CommonModule, SectorComponent, MatButtonModule],
})
export class IvanVazovNationalTheatreComponent {
  @Input() stageModel = {} as StageModel;
  @Input() event!: string;
  @Input() subEvent!: string;

  constructor(public dialog: MatDialog) {}

  openDialog(sector: SectorModel) {
    const dialogData = {
      sectorHolder: sector,
      stage: this.stageModel.stageName,
      venue: this.stageModel.venueName,
      event: this.event,
      subEvent: this.subEvent,
    };

    this.dialog.open(SectorComponent, {
      data: dialogData,
    });
  }

  calculateStatus(secorModel: SectorModel): string {
    return calculateStatus(secorModel);
  }
}
