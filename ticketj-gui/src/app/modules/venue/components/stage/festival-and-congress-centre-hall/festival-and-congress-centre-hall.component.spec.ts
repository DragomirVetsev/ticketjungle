import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FestivalAndCongressCentreHallComponent } from './festival-and-congress-centre-hall.component';

describe('FestivalAndCongressCentreHallComponent', () => {
  let component: FestivalAndCongressCentreHallComponent;
  let fixture: ComponentFixture<FestivalAndCongressCentreHallComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FestivalAndCongressCentreHallComponent],
    });
    fixture = TestBed.createComponent(FestivalAndCongressCentreHallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
