import { Component, Input } from '@angular/core';
import { StageModel } from 'src/app/models/StageModel';
import { SectorComponent } from '../../sector/sector.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-festival-and-congress-centre-hall',
  templateUrl: './festival-and-congress-centre-hall.component.html',
  styleUrls: [
    './festival-and-congress-centre-hall.component.css',
    '../stage.component.css',
  ],
  standalone: true,
  imports: [CommonModule, SectorComponent, MatButtonModule],
})
export class FestivalAndCongressCentreHallComponent {
  @Input() stageModel = {} as StageModel;
  @Input() event!: string;
  @Input() subEvent!: string;
}
