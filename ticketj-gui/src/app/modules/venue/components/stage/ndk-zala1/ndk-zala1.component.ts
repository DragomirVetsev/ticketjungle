import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { StageModel, SectorModel } from 'src/app/models/StageModel';
import { SectorComponent } from '../../sector/sector.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { calculateStatus } from '../stage.util';
@Component({
  selector: 'app-ndk-zala1',
  templateUrl: './ndk-zala1.component.html',
  styleUrls: ['./ndk-zala1.component.css', '../stage.component.css'],
  standalone: true,
  imports: [CommonModule, SectorComponent, MatButtonModule],
})
export class NdkZala1Component {
  @Input() stageModel = {} as StageModel;
  @Input() event!: string;
  @Input() subEvent!: string;

  constructor(public dialog: MatDialog) {}

  openDialog(sector: SectorModel) {
    const dialogData = {
      sectorHolder: sector,
      stage: this.stageModel.stageName,
      venue: this.stageModel.venueName,
      event: this.event,
      subEvent: this.subEvent,
    };

    this.dialog.open(SectorComponent, {
      data: dialogData,
    });
  }

  calculateStatus(secorModel: SectorModel): string {
    return calculateStatus(secorModel);
  }
}
