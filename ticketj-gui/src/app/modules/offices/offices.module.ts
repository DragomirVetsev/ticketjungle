import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateOfficeComponent } from './create-office/create-office.component';
import { OfficeListComponent } from './office-list/office-list.component';
import { MaterialModule } from '../materia/material.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { CommonComponentsModule } from '../common-components/common-components.module';
import { OfficesRoutingModule } from './offices-routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatTimepickerModule } from 'ngx-mat-timepicker';
import { OfficeFormComponent } from './office-form/office-form.component';
import { UpdateOfficeComponent } from './update-office/update-office.component';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [
    OfficeListComponent,
    CreateOfficeComponent,
    OfficeFormComponent,
    UpdateOfficeComponent,
  ],
  imports: [
    CommonModule,
    OfficesRoutingModule,
    MaterialModule,
    MatPaginatorModule,
    MatSortModule,
    MatSnackBarModule,
    MatTableModule,
    FormsModule,
    NgxMatTimepickerModule,
    ReactiveFormsModule,
    CommonComponentsModule,
    MatButtonModule,
    MatTooltipModule,
  ],
})
export class OfficesModule {}
