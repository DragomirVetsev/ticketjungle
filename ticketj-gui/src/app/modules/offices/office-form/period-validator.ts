import { AbstractControl } from '@angular/forms';

export function ValidPeriod(startTime: string, endTime: string) {
  return (group: AbstractControl) => {
    const startTimeControl = group.get(startTime);
    const endTimeControl = group.get(endTime);

    if (!startTimeControl || !endTimeControl) {
      return null;
    }
    if (
      startTimeControl.value.length === 0 ||
      endTimeControl.value.length === 0
    ) {
      return null;
    }

    if (startTimeControl.value > endTimeControl.value) {
      endTimeControl.setErrors({ invalidPeriod: true });
    } else {
      endTimeControl.setErrors(null);
    }
    return null;
  };
}
