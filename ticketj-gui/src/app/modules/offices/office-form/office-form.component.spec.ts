import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeFormComponent } from './office-form.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CITIES } from 'src/app/models/OfficeModel';
import { Codes as PHONE_CODES } from 'src/app/models/UserModel';
import { By } from '@angular/platform-browser';
import { FormBuilder } from '@angular/forms';
import { OfficeService } from 'src/app/services/office/office.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

describe('OfficeFormComponent', () => {
  let component: OfficeFormComponent;
  let fixture: ComponentFixture<OfficeFormComponent>;
  let formBuilder: FormBuilder;
  let service: OfficeService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [OfficeFormComponent],
      providers: [
        {
          provide: OfficeService,
          useValue: {
            getAvailableWorkers: () => {
              return of([]);
            },
          },
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(OfficeFormComponent);
    formBuilder = TestBed.inject(FormBuilder);
    service = TestBed.inject(OfficeService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should call getAvailableWorkers', () => {
    const getAvailableWorkersSpy = spyOn(
      service,
      'getAvailableWorkers'
    ).and.returnValue(of([]));
    component.ngOnInit();
    expect(getAvailableWorkersSpy).toHaveBeenCalled();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have all cities', () => {
    expect(component.Cities).toBe(CITIES);
  });
  it('should have all phone codes', () => {
    expect(component.PhoneCodes).toBe(PHONE_CODES);
  });
  it('should have office name field when creating office', () => {
    expect(component.isOfficeCreation).toBeTruthy();
    const officeName = fixture.debugElement.query(
      By.css('.office-name')
    ).nativeElement;
    expect(officeName.innerHTML).not.toBeNull();
  });

  it('should passedOffice reset the office', () => {
    let officeTest = {
      officeName: 'Test',
      officeAddress: {
        street: '68 street',
        city: 'Sofia',
        postCode: '1839',
      },
      email: 'test@email.com',
      phone: { code: '+359', number: '886546789' },
      workingHours: { startTime: '9:00Z', endTime: '18:00Z' },
      workers: ['EmployeeTest'],
    };
    component.passedOffice = officeTest;
    expect(component.office).toEqual(officeTest);
  });

  it('should mapOfficeProperties when office form changes', () => {
    let officeForm = formBuilder.group({
      officeName: ['Random Name'],
      street: ['street new'],
      city: ['Vratza'],
      postCode: ['7295'],
      code: ['+45'],
      number: ['776876543'],
      email: ['testing@test.com'],
      startTime: ['10:00'],
      endTime: ['15:00'],
      workers: [['EmployeeTest']],
    });
    let officeTest = {
      officeName: 'Random Name',
      officeAddress: {
        street: 'street new',
        city: 'Vratza',
        postCode: '7295',
      },
      email: 'testing@test.com',
      phone: { code: '+45', number: '776876543' },
      workingHours: {
        startTime: '10:00',
        endTime: '15:00',
      },
      workers: ['EmployeeTest'],
    };
    component.officeForm = officeForm;
    component.mapOfficeProperties();
    expect(component.office).toEqual(officeTest);
  });

  it('should reset form values to initial values', () => {
    component.passedOffice = {
      officeName: 'Ivan',
      officeAddress: {
        street: 'street test',
        city: 'Plovdiv',
        postCode: '4000',
      },
      email: 'notIvan@test.com',
      phone: { code: '+45', number: '647856434' },
      workingHours: {
        startTime: '09:00',
        endTime: '18:00',
      },
      workers: ['renter'],
    };
    const initialFormValues = component.officeForm.value;
    component.officeForm = formBuilder.group({
      officeName: ['Random Name'],
      street: ['street new'],
      city: ['Vratza'],
      postCode: ['7295'],
      code: ['+45'],
      number: ['776876543'],
      email: ['testing@test.com'],
      startTime: ['10:00'],
      endTime: ['15:00'],
      workers: [['EmployeeTest']],
    });
    expect(component.officeForm.value).not.toEqual(initialFormValues);
    component.onReset();
    expect(component.officeForm.value).toEqual(initialFormValues);
  });
});
