import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
} from '@angular/forms';

import {
  CITIES,
  CITY_VALIDATORS,
  POST_CODE_VALIDATORS,
  STREET_VALIDATORS,
  EMAIL_VALIDATORS,
  END_TIME_VALIDATORS,
  OfficeModel,
  NAME_VALIDATOTORS,
  PHONE_CODE_VALIDATORS,
  PHONE_NUMBER_VALIDATORS,
  START_TIME_VALIDATORS,
} from 'src/app/models/OfficeModel';
import { Codes } from 'src/app/models/UserModel';
import { ValidPeriod } from './period-validator';
import { OfficeService } from 'src/app/services/office/office.service';
@Component({
  selector: 'app-office-form',
  templateUrl: './office-form.component.html',
  styleUrls: ['./office-form.component.css'],
})
export class OfficeFormComponent implements OnInit {
  office: OfficeModel = {
    officeName: '',
    officeAddress: {
      street: '',
      city: '',
      postCode: '',
    },
    email: '',
    phone: { code: '+359', number: '' },
    workingHours: { startTime: '', endTime: '' },
    workers: [],
  };
  isOfficeCreation = true;
  officeForm: FormGroup;
  workersToChoose: string[] = [];
  initialFormValues: AbstractControl;
  @Input() set passedOffice(office: OfficeModel) {
    this.office = office;
    this.workersToChoose = this.workersToChoose.concat(this.office.workers);
  }
  @Input() set changeOfficeName(flag: boolean) {
    this.isOfficeCreation = flag;
  }
  @Output() newItemEvent = new EventEmitter<OfficeModel>();
  Cities = CITIES;
  PhoneCodes = Codes;
  constructor(
    private readonly fb: FormBuilder,
    private readonly officeService: OfficeService
  ) {
    this.officeForm = new FormGroup({});
    this.initialFormValues = this.officeForm.value;
  }
  ngOnInit(): void {
    this.officeForm = this.initializeForm();
    this.initialFormValues = this.officeForm.getRawValue();
    this.officeService
      .getAvailableWorkers()
      .subscribe(
        response =>
          (this.workersToChoose = this.workersToChoose.concat(response))
      );
  }
  initializeForm(): FormGroup {
    const toTimeIndex = 5;
    return this.fb.group(
      {
        officeName: new FormControl(
          {
            value: this.office.officeName,
            disabled: !this.isOfficeCreation,
          },
          NAME_VALIDATOTORS
        ),
        street: new FormControl(
          {
            value: this.office.officeAddress.street,
            disabled: false,
          },
          STREET_VALIDATORS
        ),
        city: new FormControl(
          {
            value: this.office.officeAddress.city,
            disabled: false,
          },
          CITY_VALIDATORS
        ),
        postCode: new FormControl(
          {
            value: this.office.officeAddress.postCode,
            disabled: false,
          },
          POST_CODE_VALIDATORS
        ),
        code: new FormControl(
          {
            value: this.office.phone.code,
            disabled: false,
          },
          PHONE_CODE_VALIDATORS
        ),
        number: new FormControl(
          {
            value: this.office.phone.number,
            disabled: false,
          },
          PHONE_NUMBER_VALIDATORS
        ),
        email: new FormControl(
          {
            value: this.office.email,
            disabled: false,
          },
          EMAIL_VALIDATORS
        ),
        startTime: new FormControl(
          {
            value: this.office.workingHours.startTime.substring(0, toTimeIndex),
            disabled: false,
          },
          START_TIME_VALIDATORS
        ),
        endTime: new FormControl(
          {
            value: this.office.workingHours.endTime.substring(0, toTimeIndex),
            disabled: false,
          },
          END_TIME_VALIDATORS
        ),
        workers: new FormControl({
          value: this.office.workers,
          disabled: false,
        }),
      },
      { validators: ValidPeriod('startTime', 'endTime') }
    );
  }
  sendOfficeToParent() {
    this.mapOfficeProperties();
    this.newItemEvent.emit(this.office);
  }
  mapOfficeProperties() {
    this.office.officeName = this.officeForm.value.officeName;
    this.office.officeAddress.city = this.officeForm.value.city;
    this.office.officeAddress.postCode = this.officeForm.value.postCode;
    this.office.officeAddress.street = this.officeForm.value.street;
    this.office.phone.code = this.officeForm.value.code;
    this.office.phone.number = this.officeForm.value.number;
    this.office.email = this.officeForm.value.email;
    this.office.workingHours.startTime = this.officeForm.value.startTime;
    this.office.workingHours.endTime = this.officeForm.value.endTime;
    this.office.workers = this.officeForm.value.workers;
  }
  onReset() {
    this.officeForm.reset(this.initialFormValues);
  }
}
