import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOfficeComponent } from './create-office.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { OfficeService } from 'src/app/services/office/office.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OfficeModel } from 'src/app/models/OfficeModel';
import { of } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('CreateOfficeComponent', () => {
  let component: CreateOfficeComponent;
  let fixture: ComponentFixture<CreateOfficeComponent>;
  let service: OfficeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateOfficeComponent],
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {
              return of();
            },
          },
        },
        {
          provide: OfficeService,
          useValue: {
            createOffice: () => {
              return of({});
            },
          },
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    service = TestBed.inject(OfficeService);
    fixture = TestBed.createComponent(CreateOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  const testNewOffice: OfficeModel = {
    officeName: 'Name Test',
    officeAddress: {
      street: 'Some street',
      city: 'Sofia',
      postCode: '1234',
    },
    email: 'test@email.com',
    phone: { code: '+359', number: '54565434' },
    workingHours: { startTime: '09:00Z', endTime: '18:00Z' },
    workers: ['Ivan'],
  };
  it('should call createOffice', () => {
    const createOfficeSpy = spyOn(service, 'createOffice').and.callThrough();
    component.setNewOffice(testNewOffice);
    expect(createOfficeSpy).toHaveBeenCalledWith(testNewOffice);
  });
});
