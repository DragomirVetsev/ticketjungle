import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { OfficeModel, officeErrorHandling } from 'src/app/models/OfficeModel';
import { OfficeService } from 'src/app/services/office/office.service';

@Component({
  selector: 'app-create-office',
  templateUrl: './create-office.component.html',
  styleUrls: ['./create-office.component.css'],
})
export class CreateOfficeComponent {
  constructor(
    private readonly officeService: OfficeService,
    private readonly snackBar: MatSnackBar,
    private readonly dialogRef: MatDialogRef<CreateOfficeComponent>
  ) {}
  setNewOffice(office: OfficeModel) {
    this.officeService.createOffice(office).subscribe({
      next: _response => {
        this.snackBar.open('The new Office was created!', 'Close', {
          duration: 3000,
          panelClass: 'success-snackbar',
        });
        this.dialogRef.close(office);
      },
      error: error => {
        officeErrorHandling(error, this.snackBar);
      },
    });
  }
  closeDialog() {
    this.dialogRef.close();
  }
}
