import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OfficeModel } from 'src/app/models/OfficeModel';
import { OfficeService } from 'src/app/services/office/office.service';
import { CreateOfficeComponent } from '../create-office/create-office.component';
import { finalize } from 'rxjs';
import { UpdateOfficeComponent } from '../update-office/update-office.component';
import { rowsAnimation } from 'src/app/models/animations';

@Component({
  selector: 'app-office-list',
  templateUrl: './office-list.component.html',
  styleUrls: ['./office-list.component.css'],
  animations: [rowsAnimation],
})
export class OfficeListComponent implements OnInit {
  officeList: OfficeModel[] = [];
  dataSource = new MatTableDataSource<OfficeModel>();
  isLoading = true;
  isAdmin: boolean;
  displayedColumns = [
    'officeName',
    'email',
    'phoneNumber',
    'addressCity',
    'addressPostCode',
    'addressStreet',
    'workingHours',
  ];

  @ViewChild(MatSort) set matSort(sort: MatSort) {
    this.dataSource.sort = sort;
  }
  @ViewChild(MatPaginator) set matPaginator(paginator: MatPaginator) {
    this.dataSource.paginator = paginator;
  }
  constructor(
    private readonly officeService: OfficeService,
    public dialog: MatDialog
  ) {
    this.isAdmin = this.isAdminFromStorage();
  }

  isAdminFromStorage(): boolean {
    const role = localStorage.getItem('role');
    return role != null && role === 'ADMIN';
  }

  ngOnInit() {
    this.officeService
      .getOffices()
      .pipe(
        finalize(() => {
          this.isLoading = false;
          if (this.isAdmin) {
            this.displayedColumns.unshift('update');
          }
        })
      )
      .subscribe(response => {
        this.officeList = response;
        this.dataSource = new MatTableDataSource(this.officeList);
        this.setCustomSort();
        this.setCustomFilter();
      });
  }
  setCustomSort() {
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'addressCity':
          return item.officeAddress.city.toLowerCase();
        case 'officeName':
          return item.officeName.toLowerCase();
        case 'workingHours':
          return item.workingHours.startTime.toLocaleLowerCase();
        default:
          return this.getFieldValue(item, property);
      }
    };
  }
  setCustomFilter() {
    this.dataSource.filterPredicate = (data, filter) => {
      const dataStr =
        data.officeName +
        data.email +
        data.officeAddress.postCode +
        data.officeAddress.city +
        data.officeAddress.street +
        (data.phone.code + ' ' + data.phone.number);
      return dataStr.indexOf(filter) !== -1;
    };
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue;
  }
  getFieldValue(item: OfficeModel, field: string): string | number {
    const value = item[field];
    if (typeof value === 'object' && value !== null) {
      if ('city' in value && 'postCode' in value && 'street' in value) {
        return `${value.city} ${value.postCode} ${value.street}`;
      } else if ('code' in value && 'number' in value) {
        const phoneValue = value;
        return `${phoneValue.code}${phoneValue.number}`;
      } else {
        return 'N/A';
      }
    }
    return String(value);
  }
  goBack() {
    window.history.back();
  }
  createOffice() {
    const dialogRef = this.dialog.open(CreateOfficeComponent, {
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const data = this.dataSource.data;
        data.unshift(result);
        this.dataSource.data = data;
      }
    });
  }
  updateOffice(office: OfficeModel) {
    const copyOffice = this.copyOffice(office);
    const dialogRef = this.dialog.open(UpdateOfficeComponent, {
      data: copyOffice,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const index = this.dataSource.data.indexOf(office);
        const data = this.dataSource.data;
        data[index] = result;
        this.dataSource.data = data;
      }
    });
  }
  copyOffice(office: OfficeModel) {
    return {
      officeName: office.officeName,
      officeAddress: {
        street: office.officeAddress.street,
        city: office.officeAddress.city,
        postCode: office.officeAddress.postCode,
      },
      email: office.email,
      phone: { code: office.phone.code, number: office.phone.number },
      workingHours: {
        startTime: office.workingHours.startTime,
        endTime: office.workingHours.endTime,
      },
      workers: office.workers,
    };
  }
  changeDate(date: string) {
    return new Date('2013-07-12T' + date);
  }
}
