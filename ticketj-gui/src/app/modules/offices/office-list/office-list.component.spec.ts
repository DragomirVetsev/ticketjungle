import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OfficeListComponent } from './office-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OfficeService } from 'src/app/services/office/office.service';
import { MatTableModule } from '@angular/material/table';
import { of } from 'rxjs';
import { CommonComponentsModule } from '../../common-components/common-components.module';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OfficeModel } from 'src/app/models/OfficeModel';
import { MatDialogModule } from '@angular/material/dialog';

describe('OfficeListComponent', () => {
  let component: OfficeListComponent;
  let fixture: ComponentFixture<OfficeListComponent>;
  let officeService: OfficeService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OfficeListComponent],
      imports: [
        HttpClientTestingModule,
        MatTableModule,
        MatCardModule,
        MatPaginatorModule,
        CommonComponentsModule,
        MatIconModule,
        FlexLayoutModule,
        MatDialogModule,
        CommonComponentsModule,
      ],
    }).compileComponents();

    officeService = TestBed.inject(OfficeService);
    fixture = TestBed.createComponent(OfficeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getOffices()', () => {
    const getOfficesSpy = spyOn(officeService, 'getOffices').and.returnValue(
      of([])
    );
    component.ngOnInit();
    expect(getOfficesSpy).toHaveBeenCalled();
  });

  it('should return the correct field value for string property', () => {
    const item: OfficeModel = {
      officeName: 'Office1',
      officeAddress: { city: 'City', postCode: '1234', street: 'Main St' },
      phone: { code: '+359', number: '886545632' },
      email: 'email@email.com',
      workingHours: { startTime: '', endTime: '' },
      workers: ['Ivan'],
    };
    const field = 'officeName';
    const result = component.getFieldValue(item, field);
    expect(result).toBe('Office1');
  });

  it('should return the correct field value for address property', () => {
    const item: OfficeModel = {
      officeName: 'Sample Office',
      officeAddress: { city: 'City', postCode: '1234', street: 'Main St' },
      phone: { code: '+359', number: '886545632' },
      email: 'email@email.com',
      workingHours: { startTime: '', endTime: '' },
      workers: ['Ivan'],
    };
    const field = 'officeAddress';
    const result = component.getFieldValue(item, field);
    expect(result).toBe('City 1234 Main St');
  });
  it('should go back in history', () => {
    const windowSpy = spyOn(window.history, 'back');
    component.goBack();
    expect(windowSpy).toHaveBeenCalled();
  });
});
