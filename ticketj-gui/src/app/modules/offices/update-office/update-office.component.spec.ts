import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateOfficeComponent } from './update-office.component';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { OfficeService } from 'src/app/services/office/office.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { OfficeModel } from 'src/app/models/OfficeModel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('UpdateOfficeComponent', () => {
  let component: UpdateOfficeComponent;
  let fixture: ComponentFixture<UpdateOfficeComponent>;
  let service: OfficeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateOfficeComponent],
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        MatDialogModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {
              return of();
            },
          },
        },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        {
          provide: OfficeService,
          useValue: {
            patchOffice: () => {
              return of({});
            },
          },
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(UpdateOfficeComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(OfficeService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  const testUpdateOffice: OfficeModel = {
    officeName: 'Name Test',
    officeAddress: {
      street: 'Some street',
      city: 'Sofia',
      postCode: '1234',
    },
    email: 'test@email.com',
    phone: { code: '+359', number: '54565434' },
    workingHours: { startTime: '09:00Z', endTime: '18:00Z' },
    workers: ['TestWorker'],
  };
  it('should call patchOffice', () => {
    const updateOfficeSpy = spyOn(service, 'patchOffice').and.callThrough();
    component.updateOffice(testUpdateOffice);
    expect(updateOfficeSpy).toHaveBeenCalledWith(testUpdateOffice);
  });
});
