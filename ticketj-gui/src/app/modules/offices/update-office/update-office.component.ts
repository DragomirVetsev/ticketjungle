import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OfficeModel, officeErrorHandling } from 'src/app/models/OfficeModel';
import { OfficeService } from 'src/app/services/office/office.service';

@Component({
  selector: 'app-update-office',
  templateUrl: './update-office.component.html',
  styleUrls: ['./update-office.component.css'],
})
export class UpdateOfficeComponent {
  officeName: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public officeToUpdate: OfficeModel,
    private readonly officeService: OfficeService,
    private readonly snackBar: MatSnackBar,
    private readonly dialogRef: MatDialogRef<UpdateOfficeComponent>
  ) {
    this.officeName = this.officeToUpdate.officeName;
  }
  updateOffice(office: OfficeModel) {
    office.officeName = this.officeName;
    this.officeService.patchOffice(office).subscribe({
      next: _response => {
        this.snackBar.open(
          `Office ${office.officeName} was successfully updated!`,
          'Close',
          {
            duration: 3000,
            panelClass: 'success-snackbar',
          }
        );
        this.dialogRef.close(office);
      },
      error: error => {
        officeErrorHandling(error, this.snackBar);
      },
    });
  }
  closeDialog() {
    this.dialogRef.close();
  }
}
