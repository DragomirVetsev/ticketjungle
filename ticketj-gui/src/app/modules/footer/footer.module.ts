import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainFooterComponent } from './main-footer/main-footer.component';
import { MaterialModule } from '../materia/material.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [MainFooterComponent],
  imports: [CommonModule, MaterialModule, RouterModule],
  exports: [MainFooterComponent],
})
export class FooterModule {}
