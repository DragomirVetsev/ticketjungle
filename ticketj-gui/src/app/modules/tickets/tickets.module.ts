import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketsTableComponent } from './tickets-table/tickets-table.component';
import { MaterialModule } from '../materia/material.module';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { RouterLink } from '@angular/router';

@NgModule({
  declarations: [TicketsTableComponent],
  imports: [
    CommonModule,
    MaterialModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    RouterLink,
  ],
  exports: [TicketsTableComponent],
})
export class TicketsModule {}
