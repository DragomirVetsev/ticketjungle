import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { TicketModel } from 'src/app/models/TicketModel';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { expandingRowAnimation } from './tickets-table-animations';

@Component({
  selector: 'app-tickets-table',
  templateUrl: './tickets-table.component.html',
  styleUrls: ['./tickets-table.component.css'],
  animations: [expandingRowAnimation],
})
export class TicketsTableComponent implements OnChanges, AfterViewInit {
  @Input() tickets: TicketModel[] = [];
  dataSource = new MatTableDataSource<TicketModel>();
  displayedColumns = [
    'orderId',
    'subEventName',
    'startTime',
    'seatStatus',
    'seat',
    'row',
    'sector',
    'stage',
    'venue',
    'price',
  ];
  columnsToDisplayWithExpand = [...this.displayedColumns, 'expand'];
  expandedElement: TicketModel | null | undefined;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnChanges(_changes: SimpleChanges): void {
    this.resetValues();
  }
  ngAfterViewInit(): void {
    this.resetValues();
  }
  resetValues(): void {
    this.dataSource = new MatTableDataSource<TicketModel>(this.tickets);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sortingDataAccessor = (ticket, property) => {
      switch (property) {
        case 'subEventName':
          return ticket.performanceId.name;
        case 'startTime':
          return ticket.performanceDetails.startDateTime.toString();
        case 'venue':
          return ticket.performanceDetails.venue;
        case 'seatStatus':
          return ticket.seat.status;
        default:
          return this.getFieldValue(ticket, property);
      }
    };
  }
  getFieldValue(item: any, field: string): string | number {
    const value = item[field];
    if (typeof value === 'number') {
      return value;
    }
    return String(value);
  }
  getEventURL(ticket: TicketModel): string {
    return `/events/${ticket.performanceId.eventName}/sub-events`;
  }
}
