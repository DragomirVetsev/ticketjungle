import { Location } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs';
import { StaticSearchCriteria } from 'src/app/models/StaticSearchCriteria';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { EventService } from 'src/app/services/event/event.service';
import { SearchEventService } from 'src/app/services/search-event/search-event.service';
import { expandingRowAnimation } from './event-update-animations';
import { SubEventCreateComponent } from '../sub-event-create/sub-event-create.component';
import { SubEventUpdateComponent } from './sub-event-update/sub-event-update.component';
import { EventModol, Genre } from 'src/app/models/EventModel';
import { MatSort } from '@angular/material/sort';

const ERROR_PANEL_CLASS = 'error-snackbar';
const SUCCESS_PANEL_CLASS = 'success-snackbar';
@Component({
  selector: 'app-event-update',
  templateUrl: './event-update.component.html',
  styleUrls: ['./event-update.component.css'],
  animations: [expandingRowAnimation],
})
export class EventUpdateComponent {
  isLoadingEvent = true;
  isLoadingSubEvents = true;
  dataSource = new MatTableDataSource<SubEventModel>();
  filterValue = '';
  event = {} as EventModol;
  originalEventName: string;
  staticSearchData: StaticSearchCriteria = { cities: [''], genres: [''] };
  displayedColumns: string[] = ['name', 'place', 'startTime', 'expand'];
  columnsToDisplayWithExpand = [...this.displayedColumns];
  expandedElement: SubEventModel | null | undefined;
  updatedEventDescription? = '';
  updatedEventGenre?: Genre;

  @ViewChild(MatSort) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }
  get sort() {
    return this.dataSource.sort ?? new MatSort();
  }
  constructor(
    private readonly eventService: EventService,
    private readonly searchService: SearchEventService,
    private readonly route: ActivatedRoute,
    readonly dialog: MatDialog,
    readonly snackBar: MatSnackBar,
    private readonly location: Location
  ) {
    this.checkForAuthorization();

    this.originalEventName = '';
    this.getDataFromServer();
    this.route.paramMap.subscribe(params => {
      this.originalEventName = params.get('event_name') ?? '';
      this.getSubEvents(this.originalEventName);
      this.getEventByName(this.originalEventName);
    });
  }

  getEventByName(name: string) {
    this.eventService
      .getEventByName(name)
      .pipe(
        finalize(() => {
          this.isLoadingEvent = false;
        })
      )
      .subscribe({
        next: response => {
          this.event = response;
          this.updatedEventDescription = response.description;
          this.updatedEventGenre = response.genre;
        },
        error: error => {
          const notFoundStatusCode = 404;
          if (error.status === notFoundStatusCode) {
            this.location.back();
            const errorMessage = `${error.error.details.entity} with ID: '${error.error.details.entityId}' is not found!`;
            this.showSnackBarMessage(errorMessage, ERROR_PANEL_CLASS);
          }
        },
      });
  }

  getSubEvents(eventName: string) {
    this.eventService
      .getSubEvents(eventName)
      .pipe(
        finalize(() => {
          this.isLoadingSubEvents = false;
        })
      )
      .subscribe({
        next: response => {
          this.dataSource = new MatTableDataSource<SubEventModel>(response);
          this.setCustomSorting();
        },
        error: error => {
          const notFoundStatusCode = 404;
          if (error.status === notFoundStatusCode) {
            this.location.back();
            const errorMessage = `${error.error.details.entity} with ID: '${error.error.details.entityId}' is not found!`;
            this.showSnackBarMessage(errorMessage, ERROR_PANEL_CLASS);
          }
        },
      });
  }

  getDataFromServer(): void {
    this.searchService.getSearchStaticCriteria().subscribe(response => {
      this.staticSearchData = response;
    });
  }

  updateEvent() {
    const successMessage = 'Event was updated!';
    const errorMessage = 'Event NOT updated!';
    const minDescriptionLength = 8;
    const updatedEvent: EventModol = {
      name: this.originalEventName,
    };
    if (this.updatedEventDescription) {
      updatedEvent.description = this.updatedEventDescription;
    }
    if (this.updatedEventGenre) {
      updatedEvent.genre = this.updatedEventGenre;
    }
    if (
      updatedEvent.description &&
      updatedEvent.description.length < minDescriptionLength &&
      updatedEvent.description !== ''
    ) {
      this.showSnackBarMessage(errorMessage, ERROR_PANEL_CLASS);
    }
    this.eventService.updateEvent(updatedEvent).subscribe({
      next: _response => {
        this.showSnackBarMessage(successMessage, SUCCESS_PANEL_CLASS);
      },
    });
  }

  discardChanges() {
    this.getEventByName(this.originalEventName);
  }

  addPerformance() {
    const dialogRef = this.dialog.open(SubEventCreateComponent, {
      width: '80%',
      autoFocus: false,
      maxHeight: '90vh',
      data: this.originalEventName,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const data = this.dataSource.data;
        data.unshift(result);
        this.dataSource.data = data;
      }
    });
  }

  updateSubEventDialog(subEvent: SubEventModel, isDuplicate: boolean) {
    const dialogRef = this.dialog.open(SubEventUpdateComponent, {
      width: '80%',
      autoFocus: false,
      maxHeight: '90vh',
      data: [subEvent, isDuplicate],
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (isDuplicate) {
          const data = this.dataSource.data;
          data.unshift(result);
          this.dataSource.data = data;
        } else {
          const index = this.dataSource.data.indexOf(subEvent);
          const data = this.dataSource.data;
          data[index] = result;
          this.dataSource.data = data;
        }
      }
    });
  }

  deleteSubEvent(subEvent: SubEventModel) {
    this.eventService.deleteSubEvent(subEvent).subscribe({
      next: () => {
        this.getSubEvents(this.originalEventName);
        const successMessage = `Performance was successfully deleted.`;
        this.showSnackBarMessage(successMessage, SUCCESS_PANEL_CLASS);
      },
      error: error => {
        const errorStatusCode = 409;
        if (error.status === errorStatusCode) {
          const errorMessage = `You cannot delete this! There already exists a relationship containing this entity!`;
          this.showSnackBarMessage(errorMessage, ERROR_PANEL_CLASS);
        }
      },
    });
  }

  showSnackBarMessage(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 5000,
      panelClass,
    });
  }

  checkForAuthorization() {
    if (localStorage.getItem('role') !== 'ADMIN') {
      this.location.back();
      this.snackBar.open(`Unauthorized User!`, 'Close', {
        duration: 10000,
      });
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setCustomSorting(): void {
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'name':
          return item.performanceId.name;
        case 'place':
          return item.performanceDetails.stage;
        case 'startTime':
          return item.performanceDetails.startDateTime.toString();
        default:
          return 'N/A';
      }
    };
  }
}
