import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventUpdateComponent } from './event-update.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MaterialModule } from '../../materia/material.module';
import { EventRoutingModule } from '../event-routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { EventService } from 'src/app/services/event/event.service';
import { of } from 'rxjs';
import { EventModol, Genre } from 'src/app/models/EventModel';
import { SearchEventService } from 'src/app/services/search-event/search-event.service';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { AddressModel } from 'src/app/models/AddressModel';
import { PerformanceId } from 'src/app/models/PerformanceId';
import { PerformanceModel } from 'src/app/models/PerformanceModel';
import { ScoreAndCountModel } from 'src/app/models/ScoreAndCountModel';
import { CommonComponentsModule } from '../../common-components/common-components.module';

describe('EventUpdateComponent', () => {
  let component: EventUpdateComponent;
  let fixture: ComponentFixture<EventUpdateComponent>;
  let eventService: EventService;
  let searchService: SearchEventService;
  let genre: Genre;
  let event: EventModol;
  let subEvent: SubEventModel;
  let performanceId: PerformanceId;
  let performanceDetails: PerformanceModel;
  let venueAddress: AddressModel;
  let reviewScoreAndCount: ScoreAndCountModel;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EventUpdateComponent],
      imports: [
        CommonModule,
        EventRoutingModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        MatDialogModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        MatStepperModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        MatSnackBarModule,
        CommonComponentsModule,
      ],
    });
    fixture = TestBed.createComponent(EventUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    eventService = TestBed.inject(EventService);
    searchService = TestBed.inject(SearchEventService);
    genre = {
      value: 'Pop',
    };
    event = {
      name: 'Test Event Name',
      description: 'Test Event Description',
      genre: genre,
    };
    venueAddress = {
      city: 'Sofia',
      postCode: '1000',
      street: 'Test Street',
    };
    reviewScoreAndCount = {
      score: 0,
      count: 0,
    };
    performanceId = {
      name: 'Test Sub Event',
      eventName: 'Test Event Name',
    };
    performanceDetails = {
      venue: 'NDK',
      stage: 'Zala 1',
      startDateTime: new Date(),
    };
    subEvent = {
      performanceId: performanceId,
      performanceDetails: performanceDetails,
      description: 'Test sub event for rendering loaded sub events.',
      ticketPrice: 1,
      venueAddress: venueAddress,
      performers: [''],
      reviewScoreAndCount: reviewScoreAndCount,
    };
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getEventByName()', () => {
    const getEventSpy = spyOn(eventService, 'getEventByName')
      .and.callThrough()
      .and.returnValue(of(event));
    const eventName = 'Test Event Name';
    component.getEventByName(eventName);
    expect(getEventSpy).toHaveBeenCalledWith(eventName);
  });

  it('should call getSubEvents()', () => {
    const getSubEventsSpy = spyOn(eventService, 'getSubEvents')
      .and.callThrough()
      .and.returnValue(of([]));
    const eventName = 'Test Event Name';
    component.getSubEvents(eventName);
    expect(getSubEventsSpy).toHaveBeenCalledWith(eventName);
  });

  it('should call getDataFromServer()', () => {
    const getStaticSearchCriteriaSpy = spyOn(
      searchService,
      'getSearchStaticCriteria'
    )
      .and.callThrough()
      .and.returnValue(of({ cities: [], genres: [] }));
    component.getDataFromServer();
    expect(getStaticSearchCriteriaSpy).toHaveBeenCalled();
  });

  it('should open dialog when addPerformance() clicked', () => {
    const openDialogSpy = spyOn(component.dialog, 'open')
      .and.callThrough()
      .and.returnValue({
        afterClosed: () => of(true),
      } as MatDialogRef<typeof component>);

    component.addPerformance();

    expect(openDialogSpy).toHaveBeenCalled();
  });

  it('should open dialog when updateSubEventDialog() isDuplicate clicked', () => {
    const openDialogSpy = spyOn(component.dialog, 'open')
      .and.callThrough()
      .and.returnValue({
        afterClosed: () => of(true),
      } as MatDialogRef<typeof component>);

    component.updateSubEventDialog(subEvent, true);

    expect(openDialogSpy).toHaveBeenCalled();
  });

  it('should open dialog when updateSubEventDialog() clicked', () => {
    const openDialogSpy = spyOn(component.dialog, 'open')
      .and.callThrough()
      .and.returnValue({
        afterClosed: () => of(true),
      } as MatDialogRef<typeof component>);

    component.updateSubEventDialog(subEvent, false);

    expect(openDialogSpy).toHaveBeenCalled();
  });

  it('should deleteSubEvent() throws error', () => {
    const deleteSubEventSpy = spyOn(
      eventService,
      'deleteSubEvent'
    ).and.callThrough();

    component.deleteSubEvent(subEvent);

    expect(deleteSubEventSpy).toHaveBeenCalledWith(subEvent);
  });

  it('should updateEvent()', () => {
    const updateEventSpy = spyOn(eventService, 'updateEvent')
      .and.callThrough()
      .and.returnValue(of(event));
    component.originalEventName = 'Test Event Name';
    component.updateEvent();
    expect(updateEventSpy).toHaveBeenCalled();
  });

  it('should updateEvent() with full information', () => {
    const updateEventSpy = spyOn(eventService, 'updateEvent')
      .and.callThrough()
      .and.returnValue(of(event));
    component.originalEventName = 'Test Event Name';
    component.updatedEventDescription = 'test';
    component.updatedEventGenre = genre;
    component.updateEvent();
    expect(updateEventSpy).toHaveBeenCalled();
  });

  it('should call discardChanges()', () => {
    const getEventSpy = spyOn(eventService, 'getEventByName')
      .and.callThrough()
      .and.returnValue(of(event));
    const eventName = 'Test Event Name';
    component.originalEventName = eventName;
    component.discardChanges();
    expect(getEventSpy).toHaveBeenCalledWith(eventName);
  });

  it('should applyFilter()', () => {
    component.applyFilter('test');
    expect(component.dataSource.filter).toBe('test');
  });

  it('should get sort()', () => {
    expect(component.sort).toBeTruthy();
  });
});
