import { Component, Inject } from '@angular/core';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EventService } from 'src/app/services/event/event.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PerformanceModel } from 'src/app/models/PerformanceModel';
import { ScoreAndCountModel } from 'src/app/models/ScoreAndCountModel';
import { PerformerModel } from 'src/app/models/PerformerModel';
const today = new Date();
const ERROR_PANEL_CLASS = 'error-snackbar';
const SUCCESS_PANEL_CLASS = 'success-snackbar';
@Component({
  selector: 'app-sub-event-update',
  templateUrl: './sub-event-update.component.html',
  styleUrls: ['./sub-event-update.component.css'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: {},
    },
  ],
})
export class SubEventUpdateComponent {
  possibleVenues: string[] = [
    'NDK',
    'Teatar Bulgarska Armia',
    'Zala Universiada',
    'Teatar Morska Gradina',
    'Stadion Vasil Levski',
  ];
  possibleStages: { [key: string]: string[] } = {
    NDK: ['Zala 1', 'Zala 2', 'Zala 3'],
    'Teatar Bulgarska Armia': ['Zala 1'],
    'Teatar Morska Gradina': ['Summer Stage'],
    'Stadion Vasil Levski': ['Stadion'],
  };
  allPerformers: PerformerModel[] = [];

  updatedInformation = {
    subEventName: '',
    description: '',
    ticketPrice: 0.0,
    venue: '',
    stage: '',
    startDateTime: today,
    time: '',
    date: today,
    performers: '',
  };
  subEvent = {} as SubEventModel;
  isDuplicate: boolean;

  constructor(
    private readonly eventService: EventService,
    readonly snackBar: MatSnackBar,
    private readonly dialogRef: MatDialogRef<SubEventUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: [SubEventModel, boolean]
  ) {
    this.subEvent = data[0];
    this.isDuplicate = data[1];
    this.getPerformers();
    this.updatedInformation.description = this.subEvent.description ?? '';
    this.updatedInformation.ticketPrice = this.subEvent.ticketPrice ?? 0.0;
    this.updatedInformation.venue = this.subEvent.performanceDetails.venue;
    this.updatedInformation.stage = this.subEvent.performanceDetails.stage;
    this.updatedInformation.startDateTime =
      this.subEvent.performanceDetails.startDateTime;
    this.updatedInformation.performers =
      this.subEvent.performers?.toString() ?? '';
  }

  subEventForm() {
    if (this.updatedInformation) {
      if (!this.isDuplicate && this.updatedInformation.subEventName === '') {
        const updatedSubEvent: SubEventModel = this.setSubEventUpdate();
        this.updateSubEvent(updatedSubEvent);
      }
      if (
        this.isDuplicate &&
        this.updatedInformation.subEventName !== '' &&
        this.updatedInformation.subEventName !==
          this.subEvent.performanceId.name
      ) {
        const updatedSubEvent: SubEventModel = this.setSubEventCreate();
        this.createSubEvent(updatedSubEvent);
      }
    }
  }
  updateSubEvent(updatedSubEvent: SubEventModel) {
    const successMessage = 'Performance was updated!';
    const errorMessage = 'Performance NOT updated!';

    this.eventService.updateSubEvent(updatedSubEvent).subscribe({
      next: _response => {
        this.showSnackBarMessage(successMessage, SUCCESS_PANEL_CLASS);
        if (
          !(
            this.updatedInformation.description &&
            this.updatedInformation.description !== this.subEvent.description
          )
        ) {
          updatedSubEvent.description = this.subEvent.description;
        }
        if (
          !(
            this.updatedInformation.ticketPrice &&
            this.updatedInformation.ticketPrice !== this.subEvent.ticketPrice
          )
        ) {
          updatedSubEvent.ticketPrice = this.subEvent.ticketPrice;
        }
        this.dialogRef.close(updatedSubEvent);
      },
      error: error => {
        const notFoundStatusCode = 409;
        if (error.status === notFoundStatusCode) {
          const duplicateError =
            'There already is a performance in the same location and with the same start time.';
          this.showSnackBarMessage(duplicateError, ERROR_PANEL_CLASS);
        } else {
          this.showSnackBarMessage(errorMessage, ERROR_PANEL_CLASS);
        }
      },
    });
  }
  createSubEvent(updatedSubEvent: SubEventModel) {
    const successMessage = 'Performance was successfully created!';
    const errorMessage =
      'Performance NOT created! Please check all the fields.';

    const performanceId = Object.assign({}, this.subEvent.performanceId);
    performanceId.name = this.updatedInformation.subEventName;
    updatedSubEvent.performanceId = performanceId;
    const reviewScoreAndCount: ScoreAndCountModel = { score: 0, count: 0 };
    updatedSubEvent.reviewScoreAndCount = reviewScoreAndCount;
    const address = {
      city: 'tempCity',
      postCode: '1000',
      street: 'tempStreet',
    };
    updatedSubEvent.venueAddress = address;
    this.eventService
      .createSubEvent(updatedSubEvent, updatedSubEvent.performanceId.eventName)
      .subscribe({
        next: _response => {
          this.showSnackBarMessage(successMessage, SUCCESS_PANEL_CLASS);
          this.dialogRef.close(updatedSubEvent);
        },
        error: _error => {
          this.showSnackBarMessage(errorMessage, ERROR_PANEL_CLASS);
        },
      });
  }

  setSubEventCreate(): SubEventModel {
    const updatedPerformanceDetails: PerformanceModel = {
      venue: this.updatedInformation.venue,
      stage: this.updatedInformation.stage,
      startDateTime: new Date(
        `${this.setDate(this.updatedInformation.date)}T${
          this.updatedInformation.time
        }:00Z`
      ),
    };
    if (this.updatedInformation.description === '') {
      this.updatedInformation.description = this.subEvent.description ?? '';
    }
    if (Number.isNaN(updatedPerformanceDetails.startDateTime.getTime())) {
      updatedPerformanceDetails.startDateTime =
        this.subEvent.performanceDetails.startDateTime;
    }
    const updatedSubEvent: SubEventModel = {
      performanceId: this.subEvent.performanceId,
      performanceDetails: updatedPerformanceDetails,
      description:
        this.updatedInformation?.description ?? this.subEvent.description,
      ticketPrice:
        this.updatedInformation?.ticketPrice ?? this.subEvent.ticketPrice,
    };
    if (this.updatedInformation.performers) {
      updatedSubEvent.performers = this.updatedInformation.performers
        .split(',')
        .map(item => item.trim());
    }
    return updatedSubEvent;
  }
  setSubEventUpdate(): SubEventModel {
    const updatedPerformanceDetails: PerformanceModel = {
      venue: this.updatedInformation.venue,
      stage: this.updatedInformation.stage,
      startDateTime: new Date(
        `${this.setDate(this.updatedInformation.date)}T${
          this.updatedInformation.time
        }:00Z`
      ),
    };
    if (Number.isNaN(updatedPerformanceDetails.startDateTime.getTime())) {
      updatedPerformanceDetails.startDateTime =
        this.subEvent.performanceDetails.startDateTime;
    }
    const updatedSubEvent: SubEventModel = {
      performanceId: this.subEvent.performanceId,
      performanceDetails: updatedPerformanceDetails,
    };
    if (this.updatedInformation.performers) {
      updatedSubEvent.performers = this.updatedInformation.performers
        .split(',')
        .map(item => item.trim());
    }
    if (
      this.updatedInformation.description &&
      this.updatedInformation.description !== this.subEvent.description
    ) {
      updatedSubEvent.description = this.updatedInformation.description;
    }
    if (
      this.updatedInformation.ticketPrice &&
      this.updatedInformation.ticketPrice !== this.subEvent.ticketPrice
    ) {
      updatedSubEvent.ticketPrice = this.updatedInformation.ticketPrice;
    }
    return updatedSubEvent;
  }

  getPerformers() {
    this.eventService.getPerformers().subscribe({
      next: response => {
        this.allPerformers = response;
      },
      error: () => {},
    });
  }

  toPerformers(name: string, event: any) {
    if (event.isUserInput) {
      if (
        this.updatedInformation.performers.length > 0 &&
        !this.updatedInformation.performers.includes(name)
      ) {
        this.updatedInformation.performers += `,${name}`;
      }
      if (this.updatedInformation.performers.length === 0) {
        this.updatedInformation.performers = name;
      }
    }
  }

  setDate(date: Date) {
    let dateSetter = `${date.getFullYear().toString()}-`;
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const delimiter = 10;
    if (month < delimiter) {
      dateSetter += `0${month.toString()}-`;
    } else {
      dateSetter += `${month.toString()}-`;
    }
    if (day < delimiter) {
      dateSetter += `0${day.toString()}`;
    } else {
      dateSetter += `${day.toString()}`;
    }
    return dateSetter;
  }

  showSnackBarMessage(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 5000,
      panelClass,
    });
  }
}
