import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubEventUpdateComponent } from './sub-event-update.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MaterialModule } from 'src/app/modules/materia/material.module';
import { EventRoutingModule } from '../../event-routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PerformanceModel } from 'src/app/models/PerformanceModel';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { PerformanceId } from 'src/app/models/PerformanceId';
import { EventService } from 'src/app/services/event/event.service';
import { of } from 'rxjs';
import { NgxMatTimepickerModule } from 'ngx-mat-timepicker';

describe('SubEventUpdateComponent', () => {
  let component: SubEventUpdateComponent;
  let fixture: ComponentFixture<SubEventUpdateComponent>;
  let eventService: EventService;
  const performanceId: PerformanceId = {
    name: 'Test Sub Event',
    eventName: 'Test Event Name',
  };
  const performanceDetails: PerformanceModel = {
    venue: 'NDK',
    stage: 'Zala 1',
    startDateTime: new Date(),
  };
  const data: SubEventModel = {
    performanceId: performanceId,
    performanceDetails: performanceDetails,
    description: 'Test sub event for rendering loaded sub events.',
    ticketPrice: 1,
    performers: ['testPerformer'],
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SubEventUpdateComponent],
      imports: [
        CommonModule,
        EventRoutingModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        MatDialogModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        MatStepperModule,
        BrowserAnimationsModule,
        MatSnackBarModule,
        NgxMatTimepickerModule,
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: [data, false] },
        {
          provide: MatDialogRef,
          useValue: {
            close: jasmine.createSpy('close'),
          },
        },
      ],
      teardown: { destroyAfterEach: false },
    }).compileComponents();
    fixture = TestBed.createComponent(SubEventUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    eventService = TestBed.inject(EventService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should updateSubEvent()', () => {
    const updateSubEventSpy = spyOn(eventService, 'updateSubEvent')
      .and.callThrough()
      .and.returnValue(of(component.subEvent));
    component.updatedInformation.description = data.description ?? '';
    component.subEventForm();
    expect(updateSubEventSpy).toHaveBeenCalled();
  });

  it('should updateSubEvent()', () => {
    const updateSubEventSpy = spyOn(eventService, 'updateSubEvent')
      .and.callThrough()
      .and.returnValue(of(component.subEvent));
    component.updatedInformation.description = 'updated';
    component.updatedInformation.ticketPrice = 20;
    component.subEventForm();
    expect(updateSubEventSpy).toHaveBeenCalled();
  });

  it('should updateSubEvent() and return error', () => {
    const updateSubEventSpy = spyOn(
      eventService,
      'updateSubEvent'
    ).and.callThrough();
    component.updatedInformation.description = '';
    component.subEventForm();
    expect(updateSubEventSpy).toHaveBeenCalled();
  });

  it('should createSubEvent() when isDuplicate is true', () => {
    const updateSubEventSpy = spyOn(
      component,
      'createSubEvent'
    ).and.callThrough();
    component.isDuplicate = true;
    component.updatedInformation.subEventName = 'Test New';
    component.updatedInformation.description = '';
    component.subEventForm();
    expect(updateSubEventSpy).toHaveBeenCalled();
  });

  it('should createSubEvent() when isDuplicate is true and return error', () => {
    const createSubEventSpy = spyOn(eventService, 'createSubEvent')
      .and.callThrough()
      .and.returnValue(of(data));
    component.createSubEvent(data);
    expect(createSubEventSpy).toHaveBeenCalled();
  });

  it('should call getPerformers()', () => {
    const getPerformersSpy = spyOn(eventService, 'getPerformers')
      .and.callThrough()
      .and.returnValue(of([]));
    component.getPerformers();
    expect(getPerformersSpy).toHaveBeenCalled();
  });

  it('should call toPerformers()', () => {
    const toPerformersSpy = spyOn(component, 'toPerformers').and.callThrough();
    const performerName = 'Test Performer Name';
    const event = {
      isUserInput: true,
    };
    component.updatedInformation.performers = '';
    component.toPerformers(performerName, event);
    expect(toPerformersSpy).toHaveBeenCalledWith(performerName, event);
  });

  it('should call toPerformers()', () => {
    const toPerformersSpy = spyOn(component, 'toPerformers').and.callThrough();
    const performerName = 'Test Performer Name';
    const event = {
      isUserInput: true,
    };
    component.toPerformers(performerName, event);
    expect(toPerformersSpy).toHaveBeenCalledWith(performerName, event);
  });

  it('should check if setDate()', () => {
    const date = new Date('2010-10-10');
    const expectedDate = '2010-10-10';
    expect(component.setDate(date)).toEqual(expectedDate);
  });
});
