import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventRoutingModule } from './event-routing';
import { MaterialModule } from '../materia/material.module';
import { EventTabsComponent } from './event-tabs/event-tabs.component';
import { EventListComponent } from './event-list/event-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SubEventsComponent } from './sub-events/sub-events.component';
import { StageComponent } from '../venue/components/stage/stage.component';
import { EventMainComponent } from './event-main/event-main.component';
import { EventSearchResultsComponent } from './event-search-results/event-search-results.component';
import { EventSearchComponent } from './event-search/event-search.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import { FilterPipe } from './filter.pipe';
import { EventCreateComponent } from './event-create/event-create.component';
import { EventUpdateComponent } from './event-update/event-update.component';
import { MatTableModule } from '@angular/material/table';
import { SubEventUpdateComponent } from './event-update/sub-event-update/sub-event-update.component';
import { SubEventCreateComponent } from './sub-event-create/sub-event-create.component';
import { CommonComponentsModule } from '../common-components/common-components.module';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxMatTimepickerModule } from 'ngx-mat-timepicker';

@NgModule({
  declarations: [
    EventTabsComponent,
    EventListComponent,
    SubEventsComponent,
    EventMainComponent,
    EventSearchResultsComponent,
    EventSearchComponent,
    FilterPipe,
    EventCreateComponent,
    EventUpdateComponent,
    SubEventUpdateComponent,
    SubEventCreateComponent,
  ],
  exports: [
    EventTabsComponent,
    EventListComponent,
    SubEventsComponent,
    EventUpdateComponent,
    FilterPipe,
  ],
  imports: [
    CommonModule,
    EventRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    StageComponent,
    FormsModule,
    MatDialogModule,
    MatTableModule,
    MatStepperModule,
    CommonComponentsModule,
    MatSortModule,
    MatCheckboxModule,
    NgxMatTimepickerModule,
  ],
})
export class EventModule {}
