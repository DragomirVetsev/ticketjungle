import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventTabsComponent } from './event-tabs.component';
import { MatTabsModule } from '@angular/material/tabs';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

describe('EventTabsComponent', () => {
  let component: EventTabsComponent;
  let fixture: ComponentFixture<EventTabsComponent>;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EventTabsComponent],
      imports: [
        HttpClientTestingModule,
        MatTabsModule,
        RouterTestingModule,
        BrowserAnimationsModule,
      ],
    });
    fixture = TestBed.createComponent(EventTabsComponent);
    httpTestingController = TestBed.inject(HttpTestingController);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
