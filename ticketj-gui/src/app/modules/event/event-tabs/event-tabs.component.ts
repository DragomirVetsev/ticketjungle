import { Component, Input } from '@angular/core';
import { SubEventModel } from 'src/app/models/SubEventModel';

@Component({
  selector: 'app-event-tabs',
  templateUrl: './event-tabs.component.html',
  styleUrls: ['./event-tabs.component.css'],
})
export class EventTabsComponent {
  @Input() subEventList!: SubEventModel[];
}
