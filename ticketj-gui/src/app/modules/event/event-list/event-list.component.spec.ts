import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { EventListComponent } from './event-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { CUSTOM_ELEMENTS_SCHEMA, TemplateRef } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { EventModol } from 'src/app/models/EventModel';
import { Router } from '@angular/router';

describe('EventListComponent', () => {
  let component: EventListComponent;
  let fixture: ComponentFixture<EventListComponent>;
  let httpTestingController: HttpTestingController;
  let templateRef: TemplateRef<EventModol>;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EventListComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatCardModule,
        MatGridListModule,
        FlexLayoutModule,
        MatIconModule,
        MatDialogModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture = TestBed.createComponent(EventListComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should open dialog when readMore()', () => {
    const openDialogSpy = spyOn(component.dialog, 'open');

    component.readMore(templateRef);

    expect(openDialogSpy).toHaveBeenCalledWith(templateRef);
  });
  it('should navigate to sub-events in event with goToEvent()', () => {
    const eventName = 'Event Name';
    const navigateSpy = spyOn(router, 'navigate');

    component.goToEvent(eventName);

    expect(navigateSpy).toHaveBeenCalledWith([
      '/events',
      eventName,
      'sub-events',
    ]);
  });
  it('should navigate to update event with updateEvent()', () => {
    const eventName = 'Event Name';
    const navigateSpy = spyOn(router, 'navigate');

    component.updateEvent(eventName);

    expect(navigateSpy).toHaveBeenCalledWith(['/events', eventName, 'update']);
  });
});
