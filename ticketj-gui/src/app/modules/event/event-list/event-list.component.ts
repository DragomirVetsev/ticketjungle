import { Component, Input, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { EventModol } from 'src/app/models/EventModel';
@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css'],
})
export class EventListComponent {
  @Input() eventList!: EventModol[];
  public isButtonPressed = false;
  isAdmin: boolean;
  username = '';
  role = '';

  constructor(readonly dialog: MatDialog, private readonly router: Router) {
    this.isAdmin = this.isAdminFunc();
    const username = localStorage.getItem('username');
    if (username) {
      this.username = username;
    }
    const role = localStorage.getItem('role');
    if (role) {
      this.role = role;
    }
  }

  goToEvent(eventName: string): void {
    this.router.navigate(['/events', eventName, 'sub-events']);
  }

  readMore(templateRef: TemplateRef<EventModol>) {
    this.dialog.open(templateRef);
  }

  updateEvent(eventName: string): void {
    this.router.navigate(['/events', eventName, 'update']);
  }

  isAdminFunc(): boolean {
    return localStorage.getItem('role') === 'ADMIN';
  }
}
