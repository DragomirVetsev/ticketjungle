import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubEventsComponent } from './sub-events.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { EventService } from 'src/app/services/event/event.service';
import { StageComponent } from '../../venue/components/stage/stage.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClient } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { FilterPipe } from '../filter.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddressModel } from 'src/app/models/AddressModel';
import { ScoreAndCountModel } from 'src/app/models/ScoreAndCountModel';
import { PerformanceModel } from 'src/app/models/PerformanceModel';
import { of } from 'rxjs';
import { TemplateRef } from '@angular/core';
import { PerformanceId } from 'src/app/models/PerformanceId';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../materia/material.module';
import { EventRoutingModule } from '../event-routing';

describe('SubEventsComponent', () => {
  let component: SubEventsComponent;
  let fixture: ComponentFixture<SubEventsComponent>;
  let eventService: EventService;
  let subEvent: SubEventModel;
  let performanceId: PerformanceId;
  let performanceDetails: PerformanceModel;
  let venueAddress: AddressModel;
  let reviewScoreAndCount: ScoreAndCountModel;
  let httpClient: HttpClient;
  let pipe = new FilterPipe();
  let templateRef: TemplateRef<SubEventModel>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SubEventsComponent, FilterPipe],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatCardModule,
        MatGridListModule,
        FlexLayoutModule,
        MatIconModule,
        StageComponent,
        MatSnackBarModule,
        FormsModule,
        MatDialogModule,
        CommonModule,
        MatProgressSpinnerModule,
        EventRoutingModule,
        MaterialModule,
        ReactiveFormsModule,
        MatTableModule,
        ReactiveFormsModule,
        MatStepperModule,
        BrowserAnimationsModule,
        MatCheckboxModule,
      ],
    });
    fixture = TestBed.createComponent(SubEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    eventService = TestBed.inject(EventService);
    httpClient = TestBed.inject(HttpClient);
    venueAddress = {
      city: 'Sofia',
      postCode: '1000',
      street: 'Test Street',
    };
    reviewScoreAndCount = {
      score: 0,
      count: 0,
    };
    performanceId = {
      name: 'Test Sub Event',
      eventName: 'Test Event Name',
    };
    performanceDetails = {
      venue: 'NDK',
      stage: 'Zala 1',
      startDateTime: new Date(),
    };
    subEvent = {
      performanceId: performanceId,
      performanceDetails: performanceDetails,
      description: 'Test sub event for rendering loaded sub events.',
      ticketPrice: 1,
      venueAddress: venueAddress,
      performers: [''],
      reviewScoreAndCount: reviewScoreAndCount,
    };
  });
  it('should call getSubEvents()', () => {
    const getSubEventsSpy = spyOn(eventService, 'getSubEvents')
      .and.callThrough()
      .and.returnValue(of([]));
    const eventName = 'Test Event Name';
    component.getSubEvents(eventName);
    expect(getSubEventsSpy).toHaveBeenCalledWith(eventName);
  });
  it('should call getStarIcons()', () => {
    const getStarIconsSpy = spyOn(component, 'getStarIcons').and.callThrough();
    const score = 1;
    component.getStarIcons(score);
    expect(getStarIconsSpy).toHaveBeenCalledWith(score);
  });
  it('should call getStarIcons() with half star', () => {
    const getStarIconsSpy = spyOn(component, 'getStarIcons').and.callThrough();
    const score = 1.5;
    component.getStarIcons(score);
    expect(getStarIconsSpy).toHaveBeenCalledWith(score);
  });
  it('should check if isSubEventListEmpty() is true when SubEventList is empty', () => {
    component.subEventList = [];
    expect(component.isSubEventListEmpty()).toBe(true);
  });
  it('should check if isSubEventListEmpty() is false when SubEventList is not empty', () => {
    component.subEventList = [subEvent];
    expect(component.isSubEventListEmpty()).toBe(false);
  });
  it('should check if isEventOver() is true when comparing with yesterday', () => {
    const date = new Date();
    date.setDate(date.getDate() - 1);
    expect(component.isEventOver(date)).toBe(true);
  });
  it('should check if isEventOver() is false when comparing with tomorrow', () => {
    const date = new Date();
    date.setDate(date.getDate() + 1);
    expect(component.isEventOver(date)).toBe(false);
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should open dialog when readMore()', () => {
    const openDialogSpy = spyOn(component.dialog, 'open');

    component.readMore(templateRef);

    expect(openDialogSpy).toHaveBeenCalledWith(templateRef);
  });
  it('pipe should filter when there are sub events', () => {
    expect(pipe.transform([subEvent], 'test')).toEqual([subEvent]);
  });
  it('pipe should filter when there are sub events and return empty list', () => {
    expect(pipe.transform([subEvent], 'badDescription')).toEqual([]);
  });
  it('pipe should return all events when filtered by nothing', () => {
    expect(pipe.transform([subEvent], '')).toEqual([subEvent]);
  });
});
