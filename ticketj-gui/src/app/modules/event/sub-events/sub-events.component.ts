import { Component, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StageModel } from 'src/app/models/StageModel';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { EventService } from 'src/app/services/event/event.service';
import { MatDialog } from '@angular/material/dialog';
import { StageDialogComponent } from '../../venue/components/stage-dialog-component/stage-dialog-component.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { finalize } from 'rxjs';
import { Location } from '@angular/common';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { PerformanceId } from 'src/app/models/PerformanceId';

@Component({
  selector: 'app-sub-events',
  templateUrl: './sub-events.component.html',
  styleUrls: ['./sub-events.component.css'],
})
export class SubEventsComponent {
  isLoading = false;
  searchText: SubEventModel['description'];
  subEventList: SubEventModel[] = [];
  eventName: string;
  addMessage = 'Added';
  stageModel = {} as StageModel;
  isButtonPressed = false;
  username = '';
  role = '';
  wishlist = new Map<string, boolean>();
  isLogged = true;
  constructor(
    private readonly eventService: EventService,
    private readonly customerService: CustomerService,
    private readonly route: ActivatedRoute,
    readonly dialog: MatDialog,
    private readonly snackBar: MatSnackBar,
    private readonly location: Location
  ) {
    this.eventName = '';
    this.searchText = '';
    this.route.paramMap.subscribe(params => {
      this.eventName = params.get('event_name') ?? '';
      this.getSubEvents(this.eventName);
    });

    const username = localStorage.getItem('username');
    const role = localStorage.getItem('role');
    if (role) {
      this.role = role;
    }
    if (username && role === 'CUSTOMER') {
      this.username = username;

      this.customerService.getWishlist(this.username).subscribe(wishlist => {
        wishlist.forEach(subEvent => {
          this.wishlist.set(subEvent.performanceId.name, true);
          console.clear();
        });
      });
    }
  }
  ngOnInit(): void {
    if (this.isLogged) {
      this.customerService.getWishlist(this.username).subscribe(wishlist => {
        wishlist.forEach(subEvent => {
          this.wishlist.set(subEvent.performanceId.name, true);
          console.clear();
        });
      });
    }
  }
  getSubEvents(eventName: string) {
    this.isLoading = true;
    this.eventService
      .getSubEvents(eventName)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          response.forEach(subEvent => {
            this.wishlist.set(subEvent.performanceId.name, false);
          });
          this.subEventList = response;
        },
        error: error => {
          const notFoundStatusCode = 404;
          if (error.status === notFoundStatusCode) {
            this.location.back();
            this.snackBar.open(
              `${error.error.details.entity} with ID: '${error.error.details.entityId}' is not found!`,
              'Close',
              {
                duration: 10000,
              }
            );
          }
        },
      });
  }

  readMore(templateRef: TemplateRef<SubEventModel>) {
    this.dialog.open(templateRef);
  }

  getStageModel(event: string, subEvent: string) {
    if (!this.isButtonPressed) {
      this.isButtonPressed = true;
      this.eventService.getSubEventSeats(event, subEvent).subscribe({
        next: response => {
          const dialogRef = this.dialog.open(StageDialogComponent, {
            width: '50%',
            data: {
              event: event,
              subEvent: subEvent,
              stageModel: response,
            },
          });

          dialogRef.afterClosed().subscribe(() => {
            this.isButtonPressed = false;
          });
        },
      });
    }
  }

  getStarIcons(score: number): string {
    const lowerBound = 0.25;
    const upperBound = 0.9;
    const maxScore = 5.0;
    const filledStars = Math.min(Math.round(score), maxScore);
    if (
      score - Math.floor(score) >= lowerBound &&
      score - Math.floor(score) < upperBound
    ) {
      const emptyStars = Math.max(maxScore - filledStars, 0);
      return `${'★'.repeat(filledStars - 1)}✬${'☆'.repeat(emptyStars)}`;
    } else {
      const emptyStars = Math.max(maxScore - filledStars, 0);
      return '★'.repeat(filledStars) + '☆'.repeat(emptyStars);
    }
  }

  isSubEventListEmpty() {
    if (this.subEventList.length > 0) {
      return false;
    }
    return true;
  }

  isEventOver(startTime: Date) {
    const today = new Date();
    if (startTime > today) {
      return false;
    }
    return true;
  }

  addToWishlist(performanceId: PerformanceId) {
    this.customerService.addToWishlist(this.username, performanceId).subscribe({
      error: () => console.clear(),
    });
    this.wishlist.set(performanceId.name, true);
  }
  removeFromWishlist(performanceId: PerformanceId) {
    this.customerService
      .removeFromWishlist(this.username, performanceId)
      .subscribe({
        error: () => console.clear(),
      });
    this.wishlist.set(performanceId.name, false);
  }

  openSnackBar(message: string) {
    const duration = 1500;
    this.snackBar.open(message, '', {
      duration,
    });
  }

  isItInTheWishlist(performanceId: PerformanceId): boolean {
    const result = this.wishlist.get(performanceId.name);
    if (result !== undefined) {
      return result;
    }
    return false;
  }
}
