import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { EventService } from 'src/app/services/event/event.service';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { PerformanceId } from 'src/app/models/PerformanceId';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-sub-event-create',
  templateUrl: './sub-event-create.component.html',
  styleUrls: ['./sub-event-create.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: {},
    },
  ],
})
export class SubEventCreateComponent implements OnInit {
  startDateControl = new FormControl();
  startTimeControl = new FormControl();
  dateIsInPast = false;
  @ViewChild('createSubEventForm')
  createSubEventForm!: TemplateRef<SubEventModel>;

  eventNames: string[] = [];
  performers: string[] = [];
  performanceId: PerformanceId = {
    name: '',
    eventName: '',
  };
  subEvent: SubEventModel = {
    performanceId: this.performanceId,
    description: '',
    ticketPrice: 0,
    performanceDetails: {
      venue: '',
      stage: '',
      startDateTime: new Date(),
    },
    performers: [],
    venueAddress: {
      city: 'City example',
      postCode: '1111',
      street: 'Street example',
    },
    reviewScoreAndCount: {
      score: 0,
      count: 0,
    },
  };
  possibleVenues: string[] = [
    'NDK',
    'Teatar Bulgarska Armia',
    'Zala Universiada',
    'Teatar Morska Gradina',
    'Stadion Vasil Levski',
  ];
  possibleStages: { [key: string]: string[] } = {
    NDK: ['Zala 1', 'Zala 2', 'Zala 3'],
    'Teatar Bulgarska Armia': ['Zala 1'],
    'Teatar Morska Gradina': ['Summer Stage'],
    'Stadion Vasil Levski': ['Stadion'],
  };

  createdInformation = {
    description: '',
    ticketPrice: 0.0,
    venue: '',
    stage: '',
    startTime: '',
    date: '',
    time: '',
  };
  constructor(
    private readonly eventService: EventService,
    private readonly snackBar: MatSnackBar,
    private readonly dialogRef: MatDialogRef<SubEventCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string
  ) {
    this.subEvent.performanceId.eventName = data ?? '';
  }

  ngOnInit() {
    this.loadEventNames();
    const currentDate = new Date();

    this.startTimeControl.setValue(
      currentDate.toLocaleTimeString('en-US', {
        hour: '2-digit',
        minute: '2-digit',
      })
    );
    const ISO_DATE_LENGTH = 16;
    this.startDateControl.setValue(
      currentDate.toISOString().slice(0, ISO_DATE_LENGTH)
    );
  }
  loadEventNames() {
    this.eventService.getEvents().subscribe(events => {
      this.eventNames = events.map(event => event.name);
    });
  }

  performersInput = '';

  createSubEvent() {
    const ERROR_PANEL_CLASS = 'error-snackbar';
    const HTTP_STATUS_CONFLICT = 409;

    const isFieldsValid = this.areFieldsValid();
    this.updatePerformers();

    if (isFieldsValid) {
      const enteredDateStr = this.createdInformation.date;
      const enteredTime = this.createdInformation.time;

      if (enteredDateStr && enteredTime) {
        const enteredDate = new Date(enteredDateStr);
        const selectedDateTime = new Date(
          enteredDate.getFullYear(),
          enteredDate.getMonth(),
          enteredDate.getDate(),
          parseInt(enteredTime.split(':')[0], 10),
          parseInt(enteredTime.split(':')[1], 10)
        );

        if (
          !isNaN(selectedDateTime.getTime()) &&
          selectedDateTime > new Date()
        ) {
          this.subEvent.performanceDetails.startDateTime = selectedDateTime;

          this.eventService
            .createSubEvent(
              this.subEvent,
              this.subEvent.performanceId.eventName
            )
            .subscribe({
              next: _response => {
                this.eventNames.push(this.subEvent.performanceId.eventName);
                this.showSuccessMessage(
                  'Performance was successfully created!',
                  'success-snackbar'
                );
                this.dialogRef.close(this.subEvent);
              },
              error: error => {
                if (error.status === HTTP_STATUS_CONFLICT) {
                  this.showErrorMessage(
                    'This performance already exists',
                    ERROR_PANEL_CLASS
                  );
                } else {
                  this.showErrorMessage(
                    'Performance NOT created!',
                    ERROR_PANEL_CLASS
                  );
                }
              },
            });
        } else {
          this.showErrorMessage(
            'Please select a future date and time.',
            ERROR_PANEL_CLASS
          );
        }
      } else {
        this.showErrorMessage(
          'Please enter valid date and time.',
          ERROR_PANEL_CLASS
        );
      }
    } else {
      this.showErrorMessage(
        'Please fill in all required fields',
        ERROR_PANEL_CLASS
      );
    }
  }

  private areFieldsValid(): boolean {
    const isPerformanceIdValid = !!(
      this.subEvent.performanceId?.eventName &&
      this.subEvent.performanceId?.name
    );

    const isDescriptionValid = !!(
      this.subEvent.description && this.subEvent.ticketPrice
    );

    const isPerformanceDetailsValid = !!(
      this.subEvent.performanceDetails?.venue &&
      this.subEvent.performanceDetails?.stage &&
      this.subEvent.performanceDetails?.startDateTime
    );

    return (
      isPerformanceIdValid && isDescriptionValid && isPerformanceDetailsValid
    );
  }

  private updatePerformers() {
    if (this.performersInput) {
      this.subEvent.performers = this.performersInput
        .split(',')
        .map(item => item.trim());
    } else {
      this.subEvent.performers = ['Not added.'];
    }
  }

  private showErrorMessage(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 5000,
      panelClass,
    });
  }

  private showSuccessMessage(message: string, panelClass: string) {
    this.snackBar.open(message, 'Close', {
      duration: 3000,
      panelClass,
    });
  }
  getCurrentDateTime(): string {
    const PAD_LENGTH = 2;
    const now = new Date();
    const year = now.getFullYear();
    const month = (now.getMonth() + 1).toString().padStart(PAD_LENGTH, '0');
    const day = now.getDate().toString().padStart(PAD_LENGTH, '0');
    const hours = now.getHours().toString().padStart(PAD_LENGTH, '0');
    const minutes = now.getMinutes().toString().padStart(PAD_LENGTH, '0');

    return `${year}-${month}-${day}T${hours}:${minutes}`;
  }
}
