import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { SubEventCreateComponent } from './sub-event-create.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MaterialModule } from '../../materia/material.module';
import { EventRoutingModule } from '../event-routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { EventService } from 'src/app/services/event/event.service';
import { of } from 'rxjs';
import { EventModol } from 'src/app/models/EventModel';
import { SubEventModel } from 'src/app/models/SubEventModel';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('SubEventCreateComponent', () => {
  let component: SubEventCreateComponent;
  let fixture: ComponentFixture<SubEventCreateComponent>;
  let mockEventService: jasmine.SpyObj<EventService>;
  let mockSnackBar: jasmine.SpyObj<MatSnackBar>;
  let createdSubEvent: SubEventModel;
  let dummyEvents: EventModol[];
  const mockDateTimeService = jasmine.createSpyObj('DateTimeService', [
    'getCurrentDateTime',
  ]);
  mockDateTimeService.getCurrentDateTime.and.returnValue(
    new Date('2023-09-10T14:30:00')
  );

  beforeEach(() => {
    mockEventService = jasmine.createSpyObj('EventService', [
      'getEvents',
      'createSubEvent',
    ]);
    mockSnackBar = jasmine.createSpyObj('MatSnackBar', ['open']);
    mockEventService.getEvents.and.returnValue(of(dummyEvents));
    mockEventService.createSubEvent.and.returnValue(of(createdSubEvent));

    TestBed.configureTestingModule({
      declarations: [SubEventCreateComponent],
      imports: [
        CommonModule,
        EventRoutingModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        MatDialogModule,
        MatTableModule,
        FormsModule,
        ReactiveFormsModule,
        MatStepperModule,
        BrowserAnimationsModule,
        MatSnackBarModule,
        MatCheckboxModule,
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: createdSubEvent },
        { provide: EventService, useValue: mockEventService },
        { provide: MatSnackBar, useValue: mockSnackBar },
        {
          provide: MatDialogRef,
          useValue: {
            close: jasmine.createSpy('close'),
          },
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(SubEventCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load event names', () => {
    const dummyEvents: EventModol[] = [
      {
        name: 'Event 1',
        description: 'Description 1',
        genre: { value: 'Genre 1' },
      },
      {
        name: 'Event 2',
        description: 'Description 2',
        genre: { value: 'Genre 2' },
      },
      {
        name: 'Event 3',
        description: 'Description 3',
        genre: { value: 'Genre 3' },
      },
    ];
    mockEventService.getEvents.and.returnValue(of(dummyEvents));

    component.loadEventNames();

    expect(component.eventNames).toEqual(['Event 1', 'Event 2', 'Event 3']);
  });

  it('should set performers to "Not added." if no input', () => {
    component.performersInput = '';
    component.createSubEvent();

    expect(component.subEvent.performers).toEqual(['Not added.']);
  });

  it('should set performers from input', () => {
    component.performersInput = 'Performer 1, Performer 2, Performer 3';
    component.createSubEvent();

    expect(component.subEvent.performers).toEqual([
      'Performer 1',
      'Performer 2',
      'Performer 3',
    ]);
  });

  it('should show error snackbar if required fields are missing', () => {
    component.createSubEvent();

    expect(mockSnackBar.open).toHaveBeenCalledWith(
      'Please fill in all required fields',
      'Close',
      jasmine.any(Object)
    );
  });

  it('should show error message for empty fields', () => {
    component.createdInformation.startTime = '10:00';

    component.createSubEvent();

    expect(mockSnackBar.open).toHaveBeenCalledWith(
      'Please fill in all required fields',
      'Close',
      jasmine.any(Object)
    );
  });

  it('should show error message for past date and time', () => {
    component.subEvent.performanceId.eventName = 'Test Event';
    component.subEvent.performanceId.name = 'Test Performance';
    component.subEvent.description = 'Test Description';
    component.subEvent.ticketPrice = 50;
    component.subEvent.performanceDetails.venue = 'Test Venue';
    component.subEvent.performanceDetails.stage = 'Test Stage';
    component.subEvent.performanceDetails.startDateTime = new Date();

    component.createSubEvent();

    expect(mockSnackBar.open).toHaveBeenCalledWith(
      'Please enter valid date and time.',
      'Close',
      jasmine.any(Object)
    );
  });
});
