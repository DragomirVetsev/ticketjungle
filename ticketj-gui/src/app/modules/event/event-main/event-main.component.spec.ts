import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventMainComponent } from './event-main.component';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { RouterTestingModule } from '@angular/router/testing';
import { EventListComponent } from '../event-list/event-list.component';
import { EventService } from 'src/app/services/event/event.service';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MAT_MOMENT_DATE_FORMATS,
} from '@angular/material-moment-adapter';
import {
  MAT_DATE_LOCALE,
  DateAdapter,
  MAT_DATE_FORMATS,
} from '@angular/material/core';
import { SearchEventService } from 'src/app/services/search-event/search-event.service';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { FilterPipe } from '../filter.pipe';
import { of } from 'rxjs';

describe('EventMainComponent', () => {
  let component: EventMainComponent;
  let fixture: ComponentFixture<EventMainComponent>;
  let httpTestingController: HttpTestingController;
  let eventService: EventService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [EventMainComponent, EventListComponent, FilterPipe],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatCardModule,
        MatGridListModule,
        FlexLayoutModule,
        MatIconModule,
        FormsModule,
        MatDialogModule,
      ],
      providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'bg-BG' },
        {
          provide: DateAdapter,
          useClass: MomentDateAdapter,
          deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    eventService = TestBed.inject(EventService);
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture = TestBed.createComponent(EventMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call getEvents', () => {
    const getEventsSpy = spyOn(eventService, 'getEvents')
      .and.callThrough()
      .and.returnValue(of([]));
    component.ngOnInit();
    expect(getEventsSpy).toHaveBeenCalled;
  });
});
