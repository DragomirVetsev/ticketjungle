import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs';
import { EventModol } from 'src/app/models/EventModel';
import { EventService } from 'src/app/services/event/event.service';
@Component({
  selector: 'app-event-main',
  templateUrl: './event-main.component.html',
  styleUrls: ['./event-main.component.css'],
})
export class EventMainComponent implements OnInit {
  isLoading = true;
  eventList: EventModol[] = [];
  searchText: EventModol['name'];
  constructor(private readonly eventService: EventService) {
    this.searchText = '';
  }
  ngOnInit() {
    this.eventService
      .getEvents()
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(response => {
        this.eventList = Array.from(Object.values(response));
      });
  }
}
