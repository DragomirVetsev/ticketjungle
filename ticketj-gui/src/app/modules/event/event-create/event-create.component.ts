import { HttpClient } from '@angular/common/http';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import {} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  EventModol,
  Genre,
  Genres,
  getAllGenres,
} from 'src/app/models/EventModel';
import { EventService } from 'src/app/services/event/event.service';

@Component({
  selector: 'app-event-create',
  templateUrl: './event-create.component.html',
  styleUrls: ['./event-create.component.css'],
})
export class EventCreateComponent {
  addPerformanceClicked = false;
  showAddPerformanceDialog = false;
  @ViewChild('createEventForm') createEventForm!: TemplateRef<EventModol>;
  name = '';
  description = '';
  genre: Genre = Genres[0];
  genreOptions: string[] = getAllGenres();

  constructor(
    private readonly eventService: EventService,
    private readonly dialog: MatDialog,
    private readonly snackBar: MatSnackBar,
    private readonly httpClient: HttpClient
  ) {}

  createEvent() {
    const eventsUrl = this.eventService.getEventsUrl();
    const isFieldsValid = this.name && this.description && this.genre;
    const successMessage = 'Event was created!';
    const errorMessage = 'Event NOT created!';
    const ERROR_SNACKBAR_CONFIG = {
      duration: 3000,
      panelClass: 'error-snackbar',
    };
    this.addPerformanceClicked = false;
    if (isFieldsValid) {
      const newEvent: EventModol = {
        name: this.name,
        description: this.description,
        genre: this.genre,
      };
      this.httpClient.post<EventModol>(eventsUrl, newEvent).subscribe({
        next: _response => {
          this.snackBar.open(successMessage, 'Close', {
            duration: 3000,
            panelClass: 'success-snackbar',
          });
        },
        error: error => {
          const HTTP_STATUS_CONFLICT = 409;
          if (error.status === HTTP_STATUS_CONFLICT) {
            this.snackBar.open(
              'This event already exists.',
              'Close',
              ERROR_SNACKBAR_CONFIG
            );
          } else {
            this.snackBar.open(errorMessage, 'Close', ERROR_SNACKBAR_CONFIG);
          }
        },
      });
    } else {
      this.snackBar.open(errorMessage, 'Close', ERROR_SNACKBAR_CONFIG);
    }
  }
  openCreateDialog() {
    this.dialog.open(this.createEventForm);
  }

  toggleAddPerformanceDialog() {
    this.addPerformanceClicked = true;
  }
}
