import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogModule,
} from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventCreateComponent } from './event-create.component';
import { EventService } from 'src/app/services/event/event.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of } from 'rxjs';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { Genre } from 'src/app/models/EventModel';
import { CommonModule } from '@angular/common';
import { EventRoutingModule } from '../event-routing';
import { MaterialModule } from '../../materia/material.module';
import { StageComponent } from '../../venue/components/stage/stage.component';
import { MatTableModule } from '@angular/material/table';
import { MatStepperModule } from '@angular/material/stepper';
import { CommonComponentsModule } from '../../common-components/common-components.module';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxMatTimepickerModule } from 'ngx-mat-timepicker';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('EventCreateComponent', () => {
  let component: EventCreateComponent;
  let fixture: ComponentFixture<EventCreateComponent>;
  const mockSnackBar = jasmine.createSpyObj('MatSnackBar', ['open']);
  let mockDialog: jasmine.SpyObj<MatDialog>;
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    mockDialog = jasmine.createSpyObj('MatDialog', ['open']);
    await TestBed.configureTestingModule({
      declarations: [EventCreateComponent],
      providers: [
        { provide: MatSnackBar, useValue: mockSnackBar },
        { provide: MatDialog, useValue: mockDialog },
        { provide: MAT_DIALOG_DATA, useValue: 'your-test-data-here' },
      ],
      imports: [
        CommonModule,
        EventRoutingModule,
        MaterialModule,
        ReactiveFormsModule,
        StageComponent,
        FormsModule,
        MatDialogModule,
        MatTableModule,
        MatStepperModule,
        CommonComponentsModule,
        MatSortModule,
        MatCheckboxModule,
        NgxMatTimepickerModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
      ],
    }).compileComponents();
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show error snackbar if required fields are missing', () => {
    (mockSnackBar.open as jasmine.Spy).calls.reset();

    component.createEvent();

    expect(mockSnackBar.open).toHaveBeenCalledWith(
      'Event NOT created!',
      'Close',
      jasmine.any(Object)
    );
  });

  it('should open create dialog', () => {
    const dialogRefSpyObj = jasmine.createSpyObj(
      { afterClosed: of('dialog closed') },
      ['afterClosed']
    );

    mockDialog.open.and.returnValue(dialogRefSpyObj);

    component.openCreateDialog();
    expect(mockDialog.open).toHaveBeenCalledWith(component.createEventForm);
  });

  it('should toggle addPerformanceClicked', () => {
    expect(component.addPerformanceClicked).toBe(false);
    component.toggleAddPerformanceDialog();
    expect(component.addPerformanceClicked).toBe(true);
  });

  it('should send a POST request when createEvent is called with valid data', fakeAsync(() => {
    const eventService = TestBed.inject(EventService);
    const eventsUrl = eventService['eventsUrl'];

    component.name = 'Test Event';
    component.description = 'Test Event Description';
    const testGenre: Genre = { value: 'Test Genre' };
    component.genre = testGenre;

    component.createEvent();

    const req = httpTestingController.expectOne(eventsUrl);
    expect(req.request.method).toEqual('POST');
    req.flush({});

    tick();

    expect(mockSnackBar.open).toHaveBeenCalledWith(
      'Event was created!',
      'Close',
      jasmine.any(Object)
    );
  }));
  afterEach(() => {
    httpTestingController.verify();
  });
});
