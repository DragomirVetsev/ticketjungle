import { Component, OnInit } from '@angular/core';
import { EventModol } from 'src/app/models/EventModel';
import { StaticSearchCriteria } from 'src/app/models/StaticSearchCriteria';
import { EventService } from 'src/app/services/event/event.service';
import { SearchEventService } from 'src/app/services/search-event/search-event.service';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MAT_MOMENT_DATE_FORMATS,
} from '@angular/material-moment-adapter';
import {
  MAT_DATE_LOCALE,
  DateAdapter,
  MAT_DATE_FORMATS,
} from '@angular/material/core';

import { Moment } from 'moment';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-event-search',
  templateUrl: './event-search.component.html',
  styleUrls: ['./event-search.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'bg-BG' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class EventSearchComponent implements OnInit {
  eventList: EventModol[] = [];
  staticSearchData: StaticSearchCriteria = { cities: [''], genres: [''] };
  clearDateFlag!: boolean;
  eventListTitle!: string;

  constructor(
    private readonly eventService: EventService,
    private readonly searchService: SearchEventService,
    public dialog: MatDialog,
    public formBuilder: FormBuilder,
    private readonly router: Router
  ) {}

  range: FormGroup = this.formBuilder.group(
    {
      start: new FormControl<Moment | null>(null),
      end: new FormControl<Moment | null>(null),
      searchCity: new FormControl(null),
      searchGenre: new FormControl(null),
      searchEventName: '',
    },
    { validator: this.atLeastOneFildRequired }
  );

  atLeastOneFildRequired(group: FormGroup): {
    [s: string]: boolean;
  } | null {
    if (group) {
      if (
        group.controls['end'].value ||
        group.controls['start'].value ||
        group.controls['searchCity'].value ||
        group.controls['searchGenre'].value ||
        group.controls['searchEventName'].value
      ) {
        return null;
      }
    }
    return { error: true };
  }

  ngOnInit() {
    this.eventService.getEvents().subscribe(response => {
      this.eventList = response;
    });

    this.getDataFromServer();
    this.eventListTitle = 'All Available Events';
  }

  onDatePicketRaiseFlag() {
    this.clearDateFlag = true;
  }

  getDataFromServer(): void {
    this.searchService.getSearchStaticCriteria().subscribe(response => {
      this.staticSearchData = response;
    });
  }

  clearDate() {
    this.range.reset();
    this.range.controls['start'].reset();
    this.range.controls['end'].reset();
    this.clearDateFlag = false;
  }

  search() {
    this.router.navigate(['/events/search'], {
      queryParams: {
        from: this.range.controls['start'].value?.utc().format(),
        to: this.range.controls['end'].value?.utc().format(),
        city: this.range.controls['searchCity'].value,
        genre: this.range.controls['searchGenre'].value,
        name: this.range.controls['searchEventName'].value,
      },
    });
    this.dialog.closeAll();
  }
}
