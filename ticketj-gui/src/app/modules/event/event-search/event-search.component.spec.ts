import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventSearchComponent } from './event-search.component';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { EventService } from 'src/app/services/event/event.service';
import { SearchEventService } from 'src/app/services/search-event/search-event.service';
import { CUSTOM_ELEMENTS_SCHEMA, forwardRef } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MAT_MOMENT_DATE_FORMATS,
} from '@angular/material-moment-adapter';
import {
  MAT_DATE_LOCALE,
  DateAdapter,
  MAT_DATE_FORMATS,
} from '@angular/material/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from '../../materia/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';

describe('EventSearchComponent', () => {
  let component: EventSearchComponent;
  let fixture: ComponentFixture<EventSearchComponent>;
  let httpTestingController: HttpTestingController;
  let eventService: EventService;
  let searchService: SearchEventService;
  let router: Router;
  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [EventSearchComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        MaterialModule,
        NoopAnimationsModule,
      ],
      providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'bg-BG' },
        {
          provide: DateAdapter,
          useClass: MomentDateAdapter,
          deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    searchService = TestBed.inject(SearchEventService);
    eventService = TestBed.inject(EventService);
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture = TestBed.createComponent(EventSearchComponent);
    router = TestBed.inject(Router) as any;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should clear date', () => {
    component.clearDate();
    expect(component.clearDateFlag).toEqual(false);
  });
  it('should put down picker flag', () => {
    component.onDatePicketRaiseFlag();
    expect(component.clearDateFlag).toEqual(true);
  });

  it('should navigate with correct queryParams and close dialog', () => {
    const router = TestBed.inject(Router);
    const dialog = TestBed.inject(MatDialog);
    let spyDialog = spyOn(dialog, 'closeAll');
    const queryParams = {
      from: '2023-08-15T00:00:00Z',
      to: '2023-08-16T00:00:00Z',
      city: 'Sofia',
      genre: 'Pop',
      name: 'Cool',
    };

    component.range.controls['start'].setValue(moment('2023-08-15T00:00:00Z'));
    component.range.controls['end'].setValue(moment('2023-08-16T00:00:00Z'));
    component.range.controls['searchCity'].setValue('Sofia');
    component.range.controls['searchGenre'].setValue('Pop');
    component.range.controls['searchEventName'].setValue('Cool');
    let spy = spyOn(router, 'navigate');
    component.search();

    expect(spy).toHaveBeenCalledWith(['/events/search'], {
      queryParams,
    });

    expect(spyDialog).toHaveBeenCalled();
  });

  it('should call getCriteria()', () => {
    const getCriteriaSpy = spyOn(
      searchService,
      'getSearchStaticCriteria'
    ).and.callThrough();
    component.getDataFromServer();
    expect(getCriteriaSpy).toHaveBeenCalled();
  });
});
