import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubEventsComponent } from './sub-events/sub-events.component';
import { EventMainComponent } from './event-main/event-main.component';
import { EventSearchResultsComponent } from './event-search-results/event-search-results.component';
import { EventUpdateComponent } from './event-update/event-update.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: EventMainComponent,
  },
  {
    path: ':event_name/sub-events',
    component: SubEventsComponent,
  },
  {
    path: '**',
    redirectTo: 'events',
  },
  {
    path: 'search',
    component: EventSearchResultsComponent,
  },
  {
    path: ':event_name/update',
    component: EventUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventRoutingModule {}
