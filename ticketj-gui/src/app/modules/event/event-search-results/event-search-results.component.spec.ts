import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventSearchResultsComponent } from './event-search-results.component';
import { EventListComponent } from '../event-list/event-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from '../../materia/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { SearchEventService } from 'src/app/services/search-event/search-event.service';
import { EventModol } from 'src/app/models/EventModel';
import { of, throwError } from 'rxjs';

describe('EventSearchResultsComponent', () => {
  let component: EventSearchResultsComponent;
  let fixture: ComponentFixture<EventSearchResultsComponent>;
  let snackBar: MatSnackBar;
  let mockActivatedRoute: ActivatedRoute;
  let mockSearchService: jasmine.SpyObj<SearchEventService>;
  let mockSnackBar: jasmine.SpyObj<MatSnackBar>;
  beforeEach(async () => {
    mockSearchService = jasmine.createSpyObj('SearchService', [
      'searchForEvents1',
    ]);
    mockSnackBar = jasmine.createSpyObj('MatSnackBar', ['open']);

    mockActivatedRoute = new ActivatedRoute();
    TestBed.configureTestingModule({
      declarations: [EventSearchResultsComponent, EventListComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FlexLayoutModule,
        MaterialModule,
        NoopAnimationsModule,
        MatSnackBarModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: SearchEventService, useValue: mockSearchService },
        { provide: MatSnackBar, useValue: mockSnackBar },
      ],
    }).compileComponents();
    snackBar = TestBed.inject(MatSnackBar);
    fixture = TestBed.createComponent(EventSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  const event1: EventModol = {
    name: 'Burgas Fest',
    description: 'Only the best now in Burgas',
    genre: { value: 'Pop' },
  };
  const event2: EventModol = {
    name: 'GRAFA TOUR',
    description: 'The new tour of Grafa 2023',
    genre: { value: 'Pop' },
  };
  const event3: EventModol = {
    name: 'Mercury World Tour',
    description:
      'The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.',
    genre: { value: 'Pop' },
  };
  const mockData: EventModol[] = [event1, event2, event3];

  it('should set loadingFlag to true and eventList to an empty array while loading', () => {
    const mockData1: EventModol[] = [];
    mockSearchService.searchForEvents1.and.returnValue(of(mockData1));
    component.paramsMap = new Map();

    component.onDisplay();

    expect(component.loadingFlag).toBeFalse();
    expect(component.eventList).toEqual([]);
  });

  it('should set loadingFlag to false after loading and populate eventList', () => {
    mockSearchService.searchForEvents1.and.returnValue(of(mockData));
    component.paramsMap = new Map();

    component.onDisplay();

    expect(component.loadingFlag).toBeFalse();
    expect(component.eventList).toEqual(mockData);
  });

  it('should set eventListTitle based on data length', () => {
    mockSearchService.searchForEvents1.and.returnValue(of(mockData));
    component.paramsMap = new Map();

    component.onDisplay();

    expect(component.eventListTitle).toBe('Your search results');

    mockSearchService.searchForEvents1.and.returnValue(of([]));
    component.onDisplay();

    expect(component.eventListTitle).toBe('No events were found');
  });

  it('should handle error with proper status code', () => {
    const mockError = { status: 404 };
    mockSearchService.searchForEvents1.and.returnValue(
      throwError(() => mockError)
    );
    component.paramsMap = new Map();

    component.onDisplay();

    expect(component.errorSearchMassage).toMatch(/error.*search/i);
    expect(component.errorSearchFlag).toBeTrue();
    expect(mockSnackBar.open).toHaveBeenCalledWith(
      'Search params are wrong!',
      'Close',
      {
        duration: 10000,
      }
    );
  });

  it('should set searchText based on paramsMap', () => {
    mockSearchService.searchForEvents1.and.returnValue(of(mockData));
    component.onDisplay();
    expect(component.searchText).toBe('');
    component.paramsMap.set('name', 'Test Event');

    component.onDisplay();

    expect(component.searchText).toBe('Test Event');
  });
});
