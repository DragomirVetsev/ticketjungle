import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs';
import { EventModol } from 'src/app/models/EventModel';
import { SearchEventService } from 'src/app/services/search-event/search-event.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-event-search-results',
  templateUrl: './event-search-results.component.html',
  styleUrls: ['./event-search-results.component.css'],
})
export class EventSearchResultsComponent implements OnInit {
  loadingFlag = false;
  eventList: EventModol[] = [];
  searchText: EventModol['name'];
  order!: string;
  paramsMap: Map<string, string | null> = new Map<string, string>();
  eventListTitle = '';
  errorSearchFlag = false;
  errorSearchMassage = '';

  constructor(
    private readonly route: ActivatedRoute,
    private readonly searchService: SearchEventService,
    private readonly snackBar: MatSnackBar
  ) {
    this.searchText = '';
  }
  ngOnInit() {
    this.onSearch();
  }

  onSearch() {
    this.route.queryParamMap.subscribe(params => {
      this.paramsMap = new Map<string, string>();
      params.keys.forEach(key => {
        this.paramsMap.set(key, params.get(key));
      });
      this.onDisplay();
    });
  }

  onDisplay() {
    this.loadingFlag = true;
    this.eventList = [];
    this.searchService
      .searchForEvents1(this.paramsMap)
      .pipe(
        finalize(() => {
          this.loadingFlag = false;
        })
      )
      .subscribe({
        next: data => {
          this.eventList = data;
          this.errorSearchMassage = '';
          if (data.length !== 0) {
            this.eventListTitle = 'Your search results';
          } else {
            this.eventListTitle = 'No events were found';
          }
        },
        error: error => {
          const notFoundStatusCode = 404;

          if (error.status === notFoundStatusCode) {
            this.errorSearchMassage =
              'Their was an error whit the sercht! Plase comferm that your search paramears are correct!';
            this.errorSearchFlag = true;
            this.snackBar.open(
              'Search params are wrong!',

              'Close',

              {
                duration: 10000,
              }
            );
          }
        },
      });

    if (
      this.paramsMap.get('name') === '' ||
      this.paramsMap.get('name') === undefined
    ) {
      this.searchText = '';
    } else {
      this.searchText = `${this.paramsMap.get('name')}`;
    }
  }
}
