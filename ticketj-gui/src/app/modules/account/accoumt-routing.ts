import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountAuthenticationComponent } from './account-authentication/account-authentication.component';

const routes: Routes = [
  {
    path: '',
    component: AccountAuthenticationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
