import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountAuthenticationComponent } from './account-authentication/account-authentication.component';
import { AccountRoutingModule } from './accoumt-routing';
import { MaterialModule } from '../materia/material.module';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [AccountAuthenticationComponent],
  imports: [
    CommonModule,
    AccountRoutingModule,
    MaterialModule,
    FormsModule,
    MatIconModule,
  ],
  exports: [AccountAuthenticationComponent],
})
export class AccountModule {}
