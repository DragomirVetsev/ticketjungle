import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserCredentials } from 'src/app/models/UserCredentials';
import { LoginService } from 'src/app/services/account/login.service';
import {
  CustomerWithCredentials,
  PASSWORD_VALIDATOTORS,
  UNRESCRICTED_CITY_VALIDATOTORS,
  USERNAME_VALIDATOTORS,
} from 'src/app/models/CustomerWithCredentials';
import { CustomerService } from 'src/app/services/customer/customer.service';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MAT_MOMENT_DATE_FORMATS,
} from '@angular/material-moment-adapter';
import {
  MAT_DATE_LOCALE,
  DateAdapter,
  MAT_DATE_FORMATS,
} from '@angular/material/core';
import jwtDecode from 'jwt-decode';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  NAME_VALIDATOTORS,
  POST_CODE_VALIDATORS,
  PHONE_CODE_VALIDATORS,
  PHONE_NUMBER_VALIDATORS,
  STREET_VALIDATORS,
  EMAIL_VALIDATORS,
} from 'src/app/models/OfficeModel';
import { HttpStatusCode } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Codes } from 'src/app/models/UserModel';

@Component({
  selector: 'app-account-authentication',
  templateUrl: './account-authentication.component.html',
  styleUrls: ['./account-authentication.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'bg-BG' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class AccountAuthenticationComponent {
  dialog: any;
  logoFlag = true;
  selectedTabIndex = 0;
  passwordComfirm!: string;
  hide = true;
  PhoneCodes = Codes;
  visible = true;
  changetype = true;
  disSignUp = false;
  user: UserCredentials = {
    username: '',
    password: '',
  };
  newClient: CustomerWithCredentials = {
    credentials: {
      username: '',
      password: '',
    },
    personName: { givenName: '', familyName: '' },
    email: '',
    phoneNumber: {
      code: '',
      number: '',
    },
    address: { city: '', postCode: '', street: '' },
    birthday: null,
  };
  comfirmPassword!: string;
  customerForm: FormGroup;
  lgInForm: FormGroup;
  errorSnack = 'error-snackbar';

  constructor(
    private readonly loginService: LoginService,
    private readonly route: Router,
    private readonly dialogRef: MatDialog,
    private readonly service: CustomerService,
    private readonly fb: FormBuilder,
    private readonly snackBar: MatSnackBar
  ) {
    this.customerForm = this.initializeForm();
    this.lgInForm = this.initlgInForm();
  }

  signIn() {
    this.mapLogUser();
    this.loginService.onLogin(this.user).subscribe({
      next: _response => {
        localStorage.setItem('jwtToken', _response.jwtToken);
        localStorage.setItem('username', _response.userInfo.username);
        localStorage.setItem('role', _response.userInfo.role);
        localStorage.setItem(
          'givenName',
          _response.userInfo.fullName.givenName
        );
        localStorage.setItem(
          'familyName',
          _response.userInfo.fullName.familyName
        );
        this.dialogRef.closeAll();
        this.route.navigateByUrl('/');
        this.loginService.autoLogout(this.getExpirationDate());
      },
      error: error => {
        if (error.status === HttpStatusCode.BadRequest) {
          this.snackBar.open(`Wrong Username or Password!`, 'Close', {
            duration: 10000,
            panelClass: this.errorSnack,
          });
        }
      },
    });
  }

  getExpirationDate(): number {
    const MILLISECONDS_PER_SECOND = 1000;
    const token = localStorage.getItem('jwtToken');
    if (token) {
      const decodedToken: any = jwtDecode(token);
      const expirationDateInSeconds = decodedToken.exp;
      const currentDate = Date.now();
      const expirationDate = new Date(
        expirationDateInSeconds * MILLISECONDS_PER_SECOND
      );
      return Number(expirationDate) - currentDate;
    } else {
      return 0;
    }
  }
  initlgInForm(): FormGroup {
    return this.fb.group({
      logusername: [this.user.username],
      logpassword: [this.user.password],
    });
  }
  initializeForm(): FormGroup {
    return this.fb.group(
      {
        username: [this.newClient.credentials.username, USERNAME_VALIDATOTORS],
        password: [this.newClient.credentials.password, PASSWORD_VALIDATOTORS],
        confirmPassword: [],
        givenName: [this.newClient.personName.givenName, NAME_VALIDATOTORS],
        familyName: [this.newClient.personName.familyName, NAME_VALIDATOTORS],
        email: [this.newClient.email, EMAIL_VALIDATORS],
        street: [this.newClient.address.street, STREET_VALIDATORS],
        city: [this.newClient.address.city, UNRESCRICTED_CITY_VALIDATOTORS],
        postCode: [this.newClient.address.postCode, POST_CODE_VALIDATORS],
        phoneCode: [this.newClient.phoneNumber.code, PHONE_CODE_VALIDATORS],
        number: [this.newClient.phoneNumber.number, PHONE_NUMBER_VALIDATORS],
        bday: [this.newClient.birthday],
      },
      {
        validator: this.confirmedValidator('password', 'confirmPassword'),
      }
    );
  }

  puCustomer() {
    this.mapCustomerPropertie();
    console.log(this.newClient);
    this.service.putCustomer(this.newClient).subscribe({
      next: _response => {
        this.snackBar.open(
          `You have successfully created a new account`,
          'Close',
          {
            duration: 15000,
            panelClass: this.errorSnack,
          }
        );
      },
      error: error => {
        if (error.status === HttpStatusCode.Conflict) {
          this.snackBar.open(`User already exists`, 'Close', {
            duration: 15000,
            panelClass: this.errorSnack,
          });
        }
        if (error.status === HttpStatusCode.BadRequest) {
          this.snackBar.open(`Invalid user details`, 'Close', {
            duration: 15000,
            panelClass: this.errorSnack,
          });
        }
      },
    });
  }

  mapCustomerPropertie() {
    this.newClient.credentials.username = this.customerForm.value.username;
    this.newClient.credentials.password = this.customerForm.value.password;
    this.newClient.personName.givenName = this.customerForm.value.givenName;
    this.newClient.personName.familyName = this.customerForm.value.familyName;
    this.newClient.email = this.customerForm.value.email;
    this.newClient.address.city = this.customerForm.value.city;
    this.newClient.address.postCode = this.customerForm.value.postCode;
    this.newClient.address.street = this.customerForm.value.street;
    this.newClient.phoneNumber.code = this.customerForm.value.phoneCode;
    this.newClient.phoneNumber.number = this.customerForm.value.number;
  }

  mapLogUser() {
    this.user.username = this.lgInForm.value.logusername;
    this.user.password = this.lgInForm.value.logpassword;
  }

  confirmedValidator(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (
        matchingControl.errors &&
        !matchingControl.errors['confirmedValidator']
      ) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }
}
