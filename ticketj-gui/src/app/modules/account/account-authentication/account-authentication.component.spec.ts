import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountAuthenticationComponent } from './account-authentication.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MAT_MOMENT_DATE_FORMATS,
} from '@angular/material-moment-adapter';
import {
  MAT_DATE_LOCALE,
  DateAdapter,
  MAT_DATE_FORMATS,
} from '@angular/material/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from '../../materia/material.module';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/account/login.service';
import { MatDialog } from '@angular/material/dialog';
import { of } from 'rxjs';
import { UserCredentials } from 'src/app/models/UserCredentials';
import { CustomerWithCredentials } from 'src/app/models/CustomerWithCredentials';
import { Moment } from 'moment';
import * as moment from 'moment';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { JwtUserModel } from 'src/app/models/JwtUserModel';
import { UserInfo } from 'src/app/models/UserInfo';
import { PersonNameModel } from 'src/app/models/CustomerModel';

describe('AccountAuthenticationComponent', () => {
  let component: AccountAuthenticationComponent;
  let fixture: ComponentFixture<AccountAuthenticationComponent>;
  let customerService: CustomerService;
  let loginService: LoginService;
  let router: Router;
  let loginServiceMock: jasmine.SpyObj<LoginService>;
  let customerServiceMock: jasmine.SpyObj<CustomerService>;
  let dialogMock: jasmine.SpyObj<MatDialog>;
  let routerMock: jasmine.SpyObj<Router>;

  beforeEach(async () => {
    loginServiceMock = jasmine.createSpyObj('LoginService', [
      'onLogin',
      'autoLogout',
    ]);
    customerServiceMock = jasmine.createSpyObj('CustomerService', [
      'putCustomer',
    ]);
    dialogMock = jasmine.createSpyObj('MatDialog', ['closeAll']);
    routerMock = jasmine.createSpyObj('Router', ['navigateByUrl']);

    TestBed.configureTestingModule({
      declarations: [AccountAuthenticationComponent],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FlexLayoutModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        NoopAnimationsModule,
        MatSnackBarModule,
      ],
      providers: [
        { provide: LoginService, useValue: loginServiceMock },
        { provide: CustomerService, useValue: customerServiceMock },
        { provide: MatDialog, useValue: dialogMock },
        { provide: Router, useValue: routerMock },
        { provide: MAT_DATE_LOCALE, useValue: 'bg-BG' },
        {
          provide: DateAdapter,
          useClass: MomentDateAdapter,
          deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
        },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    customerService = TestBed.inject(CustomerService);
    loginService = TestBed.inject(LoginService);
    fixture = TestBed.createComponent(AccountAuthenticationComponent);
    router = TestBed.inject(Router) as any;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call login service, set jwtToken in localStorage, close dialog, navigate to "/", and call autoLogout', () => {
    const mockUserCredentials: UserCredentials = {
      username: 'mockUsername',
      password: 'mockPassword',
    };
    const expectedUser: UserCredentials = {
      username: '',
      password: '',
    };
    const expectedPersonNameModel: PersonNameModel = {
      givenName: 'mockGivenName',
      familyName: 'mockFamilyName',
    };

    const expectedUserInfo: UserInfo = {
      username: 'mockUsername',
      fullName: expectedPersonNameModel,
      role: 'mockPassword',
    };
    const mockResponse: JwtUserModel = {
      userInfo: expectedUserInfo,
      jwtToken: 'mockJWT',
    };

    loginServiceMock.onLogin.and.returnValue(of(mockResponse));

    spyOn(component, 'getExpirationDate').and.returnValue(0);
    // spyOn(component, 'autoLogout');

    component.signIn();

    expect(loginServiceMock.onLogin).toHaveBeenCalledWith(expectedUser);
    expect(localStorage.getItem('jwtToken')).toBe('mockJWT');
    expect(dialogMock.closeAll).toHaveBeenCalled();
    expect(routerMock.navigateByUrl).toHaveBeenCalledWith('/');
  });

  it('should call customer service, and register new client, and call autoLogout', () => {
    const mockNewClient: CustomerWithCredentials = {
      credentials: {
        username: '',
        password: '',
      },
      personName: { givenName: '', familyName: '' },
      email: '',
      phoneNumber: {
        code: '',
        number: '',
      },
      address: { city: '', postCode: '', street: '' },
      birthday: new Date().toISOString(),
    };
    const expectedUser: UserCredentials = {
      username: '',
      password: '',
    };
    const mockResponse: any = {
      username: '',

      personName: { givenName: '', familyName: '' },
      email: '',
      phoneNumber: {
        code: '',
        number: '',
      },
      address: { city: '', postCode: '', street: '' },
      birthday: new Date().toISOString(),
    };
    const expectedBirthday: Moment = moment(new Date());

    customerServiceMock.putCustomer.and.returnValue(of(mockResponse));

    component.puCustomer();

    expect(customerServiceMock.putCustomer).toHaveBeenCalledWith(
      component.newClient
    );
  });

  it('should return 0 when no token is present in localStorage', () => {
    spyOn(localStorage, 'getItem').and.returnValue(null);

    const expirationTime = component.getExpirationDate();

    expect(localStorage.getItem).toHaveBeenCalledWith('jwtToken');
    expect(expirationTime).toBe(0);
  });
});
