import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { finalize } from 'rxjs';
import { UserModel } from 'src/app/models/UserModel';
import { rowsAnimation } from 'src/app/models/animations';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  animations: [rowsAnimation],
})
export class UserListComponent implements OnInit {
  userList: UserModel[] = [];
  dataSource = new MatTableDataSource<UserModel>();
  isLoading = true;
  displayedColumns = [
    'username',
    'name',
    'email',
    'number',
    'addressCity',
    'addressPostCode',
    'addressStreet',
    'role',
    'profile',
  ];
  @ViewChild(MatSort) set matSort(sort: MatSort) {
    this.dataSource.sort = sort;
  }
  @ViewChild(MatPaginator) set matPaginator(paginator: MatPaginator) {
    this.dataSource.paginator = paginator;
  }
  constructor(
    private readonly userService: UserService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.userService
      .getUsers()
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe(response => {
        this.userList = response;
        this.dataSource = new MatTableDataSource<UserModel>(this.userList);

        this.dataSource.sortingDataAccessor = (item, property) => {
          switch (property) {
            case 'addressCity':
              return item.contactInfo?.address?.city?.toLowerCase() ?? '';
            case 'name':
              return `${item.personName.givenName} ${item.personName.familyName}`;
            case 'username':
              return item.username.toLowerCase();
            default:
              return this.getFieldValue(item, property);
          }
        };
      });
  }

  getFieldValue(item: UserModel, field: string): string | number {
    const value = item[field];

    if (typeof value === 'object' && value !== null) {
      if ('city' in value && 'postCode' in value && 'street' in value) {
        return `${value.city} ${value.postCode} ${value.street}`;
      } else if ('code' in value && 'number' in value) {
        return `${value.code}${value.number}`;
      } else {
        return 'N/A';
      }
    }
    return String(value);
  }

  seeProfile(user: UserModel) {
    this.router.navigate(['/customer-profile', user.username]);
  }

  goBack() {
    window.history.back();
  }
}
