import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from 'src/app/services/user/user.service';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { of } from 'rxjs';
import { CommonComponentsModule } from '../../common-components/common-components.module';
import { MatCardModule } from '@angular/material/card';
import { UserListComponent } from './user-list.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserModel } from 'src/app/models/UserModel';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;
  let userService: UserService;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserListComponent],
      imports: [
        HttpClientTestingModule,
        MatTableModule,
        MatCardModule,
        MatPaginatorModule,
        CommonComponentsModule,
        MatIconModule,
        FlexLayoutModule,
        RouterTestingModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    userService = TestBed.inject(UserService);
    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getUsers()', () => {
    const getUsersSpy = spyOn(userService, 'getUsers').and.returnValue(of([]));
    component.ngOnInit();
    expect(getUsersSpy).toHaveBeenCalled();
  });

  it('should return the correct field value for string property', () => {
    const item: UserModel = {
      username: 'john',
      personName: { givenName: 'John', familyName: 'Doe' },
      email: 'john@example.com',
      contactInfo: {
        address: { city: 'New York', postCode: '12345', street: '123 Main St' },
      },
      role: 'USER',
    };
    const field = 'username';
    const result = component.getFieldValue(item, field);
    expect(result).toBe('john');
  });

  it('should navigate to the profile page', () => {
    const user: UserModel = {
      username: 'bobo',
      personName: {
        givenName: 'Boris',
        familyName: 'Borisov',
      },
      email: 'bobo@fde.fe',
      contactInfo: {
        phone: {
          code: '+359',
          number: '877380301',
        },
        address: {
          city: 'Sofia',
          postCode: '1000',
          street: 'ul.Stefan Bazov 8',
        },
      },
      role: 'CUSTOMER',
    };

    const navigateSpy = spyOn(router, 'navigate');
    component.seeProfile(user);
    expect(navigateSpy).toHaveBeenCalledWith(['/customer-profile', 'bobo']);
  });

  it('should go back in history', () => {
    const windowSpy = spyOn(window.history, 'back');
    component.goBack();
    expect(windowSpy).toHaveBeenCalled();
  });

  it('should sort by username in lowercase', () => {
    const item: UserModel = {
      username: 'johndoe',
      personName: { givenName: 'John', familyName: 'Doe' },
      email: 'john@example.com',
      contactInfo: {
        phone: { code: '+359', number: '123456789' },
        address: { city: 'New York', postCode: '12345', street: '123 Main St' },
      },
      role: 'USER',
    };

    const result = component.dataSource.sortingDataAccessor(item, 'username');
    expect(result).toBe('johndoe');
  });
});
