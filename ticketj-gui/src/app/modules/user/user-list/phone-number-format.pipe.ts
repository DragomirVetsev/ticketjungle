import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'phoneNumberFormat' })
export class PhoneNumberFormatPipe implements PipeTransform {
  transform(value: string): string {
    if (!value) {
      return value;
    }

    return value.replace(/\s/g, '').replace(/(\d{3})/g, '$1 ');
  }
}
