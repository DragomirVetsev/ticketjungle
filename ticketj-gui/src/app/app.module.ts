import { LOCALE_ID, NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { EventModule } from './modules/event/event.module';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { HeaderModule } from './modules/header/header.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FooterModule } from './modules/footer/footer.module';
import { UserListComponent } from './modules/user/user-list/user-list.component';
import { MaterialModule } from './modules/materia/material.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { PhoneNumberFormatPipe } from './modules/user/user-list/phone-number-format.pipe';
import localeEn from '@angular/common/locales/en';
import { registerLocaleData } from '@angular/common';
import localeBg from '@angular/common/locales/bg';
import { CommonComponentsModule } from './modules/common-components/common-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AccountModule } from './modules/account/account.module';
import { JwtInterceptorService } from './services/interceptor/jwt-interceptor.service';
import { OfficesModule } from './modules/offices/offices.module';
import { MatNativeDateModule } from '@angular/material/core';
import { ErrorInterceptorInterceptor } from './services/interceptor/error-interceptor/error-interceptor';
import { MatCheckboxModule } from '@angular/material/checkbox';

registerLocaleData(localeBg);
registerLocaleData(localeEn);

@NgModule({
  declarations: [AppComponent, UserListComponent, PhoneNumberFormatPipe],
  providers: [
    { provide: LOCALE_ID, useValue: navigator.language },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    EventModule,
    OfficesModule,
    BrowserAnimationsModule,
    MatCardModule,
    HeaderModule,
    FooterModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MaterialModule,
    MatPaginatorModule,
    MatSortModule,
    CommonComponentsModule,
    MatSnackBarModule,
    MatTooltipModule,
    AccountModule,
    MatNativeDateModule,
    MatCheckboxModule,
  ],
})
export class AppModule {}
