const { interval } = require('rxjs');
const internal = require('stream');
const baseConfig = require('./karma.conf.js');

module.exports = function (config) {
  // Load base config
  baseConfig(config);

  config.set({
    plugins: [
      require('karma-jasmine'),
      require('karma-coverage'),
      require('karma-chrome-launcher'),
      require('karma-spec-reporter'),
      require('karma-sonarqube-unit-reporter'),
      require('@angular-devkit/build-angular/plugins/karma'),
    ],
    sonarQubeUnitReporter: {
      sonarQubeVersion: 'LATEST',
      outputFile: './target/karma-reports/ut_report.xml',
      overrideTestDescription: true,
      testPaths: ['./src'],
      prependTestFileName: 'ticketj-gui',
      testFilePattern: '.spec.ts',
      useBrowserName: false,
    },
    client: {
      jasmine: {
        timeoutInterval: 20000,
      },
    },
    reporters: ['spec', 'progress', 'sonarqubeUnit', 'coverage'],
    coverageReporter: {
      dir: require('path').join(__dirname, './target/coverage-reports/'),
      subdir: '.',
      reporters: [{ type: 'lcov' }],
    },
    preprocessors: {
      'src/**/*.js': ['coverage'],
      'src/**/*.ts': ['coverage'],
    },
    autoWatch: false,
    singleRun: true,
    browsers: ['ChromeHeadless'],
    customLaunchers: {
      ChromeHeadless: {
        base: 'Chrome',
        flags: [
          '--no-sandbox',
          '--headless',
          '--disable-gpu',
          '--remote-debugging-port=9222',
        ],
      },
    },
    captureTimeout: 210000,
    browserDisconnectTolerance: 3,
    browserDisconnectTimeout: 210000,
    browserNoActivityTimeout: 210000,
  });
};
