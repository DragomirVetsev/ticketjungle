package com.dxc.ghp.ticketj.jwt.service;

import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.function.Function;

import com.dxc.ghp.ticketj.dto.UserInfoDTO;
import com.dxc.ghp.ticketj.dto.UserJwtDTO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

/**
 * Service for creation and parsing JWT token.
 */
@SuppressWarnings("nls")
public final class JwtService {
    private static final String SECRET = "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c"
        + "5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e";
    private static final Key SIGN_KEY = Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET));
    private static final int JWT_EXPIRATION_TIME_IN_MINUTES = 30;
    /**
     * Used to set the user role in the JWT Claims
     */
    public static final String USER_ROLE = "ROLE";

    private JwtService() {
        // No initialization.
    }

    /**
     * @param user
     *            is the user who what`s to obtain a JWT token.
     * @return user with issued JWT token.
     */
    public static UserJwtDTO generateUserWithToken(final UserInfoDTO user) {
        final String jwtToken = Jwts
            .builder()
            .claim(USER_ROLE, user.role())
            .setSubject(user.username())
            .setIssuer("Ticket Jungle")
            .setIssuedAt(Date.from(Instant.now()))
            .setExpiration(Date.from(Instant.now().plusMillis(1000 * 60 * JWT_EXPIRATION_TIME_IN_MINUTES)))
            .signWith(SIGN_KEY)
            .compact();

        return new UserJwtDTO(user, jwtToken);
    }

    /**
     * @param token
     *            is existing JWT token.
     * @return extracted claims
     */
    public static Claims parse(final String token) {
        // @formatter:off       
        return Jwts.parserBuilder()
            .setSigningKey(SIGN_KEY)
            .build()
            .parseClaimsJws(token)
            .getBody();       
        // @formatter:on
    }

    /**
     * @param token
     *            is existing JWT token.
     * @return if the token is expired.
     */
    public static boolean isTokenExpired(final String token) {
        return extractExpirationDate(token).before(Date.from(Instant.now()));
    }

    /**
     * @param token
     *            is existing JWT token.
     * @return the extracted username
     */
    public static String extractUsername(final String token) {
        return extractClaim(token, Claims::getSubject);
    }

    /**
     * @param token
     *            is existing JWT token.
     * @return the extracted role.
     */
    public static String extractRole(final String token) {
        return parse(token).get(USER_ROLE).toString();
    }

    /**
     * @param token
     *            is existing JWT token.
     * @return the extracted issuer
     */
    public static String extractIssuer(final String token) {
        return extractClaim(token, Claims::getIssuer);
    }

    private static Date extractExpirationDate(final String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private static <T> T extractClaim(final String token, final Function<Claims, T> claimsResolver) {
        final Claims claims = parse(token);
        return claimsResolver.apply(claims);
    }
}
