package comd.dxc.ghp.ticketj.jwt.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.jwt.service.JwtService;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.SignatureException;

/**
 * Test class for {@code JwtService}
 */
@SuppressWarnings({
    "nls", "static-method"
})
public class JwtServiceTest {

    /**
     * Testing extracted issuer from the JWT token is as expected.
     */
    @Test
    void testTokenExtractCorrectIssuer() {
        final String tokenWithIssuer = "eyJhbGciOiJIUzUxMiJ9.eyJSb2xlIjoiQWRtaW4iLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImlzcyI6IlRpY2tldCBKdW5nbGUiLCJleHAiOjMyNTI0MTIxMzI3LCJpYXQiOjE2OTI5NzI1Mjd9.VgzisvUy6g8EPvT2sVGl3Pk8v_xiMD85M7HOZRIDVg4u3FlhuZnfe6Avg7QdMxLwbs5JW-1emgoP8Ptt1ydiog";
        final String actualIssuer = JwtService.extractIssuer(tokenWithIssuer);
        final String expectedIssuer = "Ticket Jungle";
        assertEquals(expectedIssuer, actualIssuer);
    }

    /**
     * Testing, extracted role for ADMIN from the JWT token is as expected.
     */
    @Test
    void testTokenExtractedRoleForAdminIsCorrect() {
        final String tokenWithRoleAdmin = "eyJhbGciOiJIUzUxMiJ9.eyJST0xFIjoiQURNSU4iLCJzdWIiOiJUaWNrZXRKIFVzZXIiLCJJc3N1ZXIiOiJJc3N1ZXIiLCJleHAiOjMyNTI0MDI5NDM4LCJpYXQiOjE2OTI5NjcwMzh9.WsYi7i8M2dgbjdlnNkhETy9rMTSPNYE49HlOOYcOO5SXVIkkH9eyCdOsQ-VWBZTmVkl6rEeewpp51v8iUF7TRg";
        final String actualRole = JwtService.extractRole(tokenWithRoleAdmin);
        final String expectedRole = "ADMIN";
        assertEquals(expectedRole, actualRole);
    }

    /**
     * Testing extracted role for CUSTOMER from the JWT token is as expected.
     */
    @Test
    void testTokenExtractedRoleForCustomerIsCorrect() {
        final String tokenWithRoleAdmin = "eyJhbGciOiJIUzUxMiJ9.eyJST0xFIjoiQ1VTVE9NRVIiLCJzdWIiOiJUaWNrZXRKIFVzZXIiLCJJc3N1ZXIiOiJJc3N1ZXIiLCJleHAiOjMyNTI0MDI5NDM4LCJpYXQiOjE2OTI5NjcwMzh9.TFeq7NleBS5WU_1OR9H69Y9KRRRtj1-lqgaqnE28vIoVlvhLOyJVDnDIfblMjRdgMuMiMIDNkV6_8kGxVYiDNQ";
        final String actualRole = JwtService.extractRole(tokenWithRoleAdmin);
        final String expectedRole = "CUSTOMER";
        assertEquals(expectedRole, actualRole);
    }

    /**
     * Testing, extracted subject username from the JWT token is as expected.
     */
    @Test
    void testTokenContainsCorrectSubject() {
        final String tokenSubject = "eyJhbGciOiJIUzUxMiJ9.eyJSb2xlIjoiQWRtaW4iLCJzdWIiOiJUaWNrZXRKIFVzZXIiLCJJc3N1ZXIiOiJJc3N1ZXIiLCJleHAiOjMyNTI0MDI5NDM4LCJpYXQiOjE2OTI5NjcwMzh9.yHi6vN_LVcC8iaOSpSZ5agW51P8fuSl47QP8Xiyhe1od0v68BR6Dj7010DPDBqQXgErAmvswnCFa9AVZ0XXy9g";
        final String actualSubject = JwtService.extractUsername(tokenSubject);
        final String expectedSubject = "TicketJ User";
        assertEquals(expectedSubject, actualSubject);
    }

    /**
     * Testing, JWT token is valid when the expiration date is not passed.
     */
    @Test
    void testTokenIsValidWhenNotExpired() {
        final String expiredToken = "eyJhbGciOiJIUzUxMiJ9.eyJSb2xlIjoiQWRtaW4iLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImlzcyI6IlRpY2tldCBKdW5nbGUiLCJleHAiOjMyNTI0MTIxMzI3LCJpYXQiOjE2OTI5NzI1Mjd9.VgzisvUy6g8EPvT2sVGl3Pk8v_xiMD85M7HOZRIDVg4u3FlhuZnfe6Avg7QdMxLwbs5JW-1emgoP8Ptt1ydiog";
        assertFalse(JwtService.isTokenExpired(expiredToken));
    }

    /**
     * Testing, when the token is expired throws {@code ExpiredJwtException}
     */
    @Test
    void testWhenTokenIsExpiredThrows() {
        final String expiredToken = "eyJhbGciOiJIUzUxMiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MTY5Mjg4MDYzOCwiaWF0IjoxNjkyOTY3MDM4fQ.2xs6aEj4BDRmUrDvwjrUYIpbpbUXB5YPCyY6i6xhzvuKwbPcc1A7IzraYkKvE3twHaCpnKIQpkDs-xksOKZGjA";
        assertThrows(ExpiredJwtException.class, () -> JwtService.isTokenExpired(expiredToken));
    }

    /**
     * Testing, when the token is malformed throws {@code MalformedJwtException}
     */
    @Test
    void testWhenTokenIsMalformedThrows() {
        final String expiredToken = "12345";
        assertThrows(MalformedJwtException.class, () -> JwtService.parse(expiredToken));
    }

    /**
     * Testing, when the token is sign with different secret key throws
     * {@code SignatureException}
     */
    @Test
    void testWhenTokenIsSignWithWrongSignatureThrows() {
        final String tokenWithWrongSignature = "eyJhbGciOiJIUzUxMiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MzI1MjQwMjk0MzgsImlhdCI6MTY5Mjk2NzAzOH0.1QySWV9II8hLSgoHWpPF6grIZGTJq909AhjA-7NC3vfmmgY7z_knZxFLDShKcuCFHwxG4IKeVCM-mnJ92fyccA";
        assertThrows(SignatureException.class, () -> JwtService.parse(tokenWithWrongSignature));
    }
}
