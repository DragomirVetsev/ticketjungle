package com.dxc.ghp.ticketj.persistence.jpa.repositories.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeWorkerRepository;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;

/**
 * Testing the {@link OfficeWorkerRepository} repository.
 */
class OfficeWorkerRepositoryJPATest extends AbstractJPATest {

    /**
     * Tests the OfficeWorkerRepository method {@code findAllAvailableWorkers()}.
     * Method does not change the data base.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml", orderBy = "officeName")
    void findAllAvailableWorkersReturnsCorrectDataTest() {
        final List<OfficeWorker> workers = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final OfficeWorkerRepository officeWorkerRepository = provider.find(OfficeWorkerRepository.class);
            return officeWorkerRepository.findAllAvailableWorkers();
        });
        assertThat(workers)
            .isNotNull()
            .hasSize(1)
            .extracting(OfficeWorker::getUsername, OfficeWorker::getWorkingPlace)
            .containsExactly(tuple("notAssigned", null));
    }

    /**
     * Tests {@link OfficeWorkerRepository#findWorker(String) findWorker} method and
     * asserts that the office worker exists in dataSet "office.yml".
     *
     * @param username
     *            The worker id used to locate worker. Must not be null.
     * @param officeName
     *            The office id used to locate office. Must not be null.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @ParameterizedTest
    @MethodSource("officeWorkerTestData")
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml")
    void findWorkerShouldReturnWorkerWhenExists(final String username, final String officeName) {
        final Optional<OfficeWorker> actual = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final OfficeWorkerRepository officeWorkerRepository = provider.find(OfficeWorkerRepository.class);
                return officeWorkerRepository.findWorker(username);
            });

        actual
            .ifPresentOrElse(worker -> assertThat(worker)
                .extracting(OfficeWorker::getUsername, office -> office.getWorkingPlace().getOfficeName())
                .containsExactly(username, officeName), () -> fail("Office worker not found"));
    }

    /**
     * Tests {@link OfficeWorkerRepository#findWorker(String) findWorker} method and
     * asserts that the office worker exists in dataSet "office.yml".
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml")
    void findWorkerShouldReturnWorkerWhenOfficeIsNull() {
        final String workerUsername = "notAssigned";
        final Optional<OfficeWorker> actual = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final OfficeWorkerRepository officeWorkerRepository = provider.find(OfficeWorkerRepository.class);
                return officeWorkerRepository.findWorker(workerUsername);
            });

        actual
            .ifPresentOrElse(worker -> assertThat(worker)
                .extracting(OfficeWorker::getUsername, office -> office.getWorkingPlace())
                .containsExactly(workerUsername, null), () -> fail("Office worker not found"));
    }

    /**
     * Tests {@link OfficeWorkerRepository#findWorker(String) findWorker} method and
     * asserts that the office worker does not exist in dataSet "office.yml".
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml")
    void findWorkerShouldNotReturnWorkerWhenDoesNotExist() {
        final Optional<OfficeWorker> actual = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final OfficeWorkerRepository officeWorkerRepository = provider.find(OfficeWorkerRepository.class);
                return officeWorkerRepository.findWorker("Worker101");
            });

        actual.ifPresent(worker -> fail(String.format("Office worker '' should not exist", worker.getUsername())));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> officeWorkerTestData() {
        return Stream.of(arguments("bobo", "TicketJ 105"), arguments("TicketHunter", "TicketJ 100"));
    }

}
