package com.dxc.ghp.ticketj.persistence.jpa.repositories.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.PaymentType;
import com.dxc.ghp.ticketj.persistence.entities.Performer;
import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;
import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;
import com.dxc.ghp.ticketj.persistence.entities.Venue;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;

/**
 * Testing the City repository.
 */
final class ReferentialReposistoryJPATest extends AbstractJPATest {
    /**
     * Tests the Event repository method {@code findAllEvents()}. Method dose not
     * change the data base.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("city.yml")
    @ExpectedDataSet("city.yml")
    void findCitiesShouldFindAllCities() {
        final List<City> foundCities = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository eventRepository = provider.find(ReferentialRepository.class);
            return eventRepository.findAllCities();
        });
        assertThat(foundCities)
            .isNotNull()
            .hasSize(3)
            .extracting(City::getCityName)
            .containsExactly("Burgas", "Pleven", "Pomorie");
    }

    /**
     * Tests the Event repository method {@code findAllEvents()}. Method dose not
     * change the data base.
     */
    @SuppressWarnings({
        "static-method", "nls"
    })
    @Test
    @DataSet("venue.yml")
    @ExpectedDataSet("venue.yml")
    void venueContainsDatasetTest() {
        final List<Venue> foundVenues = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository eventRepository = provider.find(ReferentialRepository.class);
            return eventRepository.findAllVenues();
        });
        assertThat(foundVenues)
            .isNotNull()
            .hasSize(3)
            .extracting(Venue::getName, venue -> venue.getAddress().postCode(), venue -> venue.getAddress().street(),
                        venue -> venue.getAddress().city().getCityName())
            .contains(tuple("N. O. Masalitinov", "4000", "Aleksandur I-vi, 38", "Plovdiv"),
                      tuple("NDK", "1000", "Ploshtad Bulgaria, 1", "Sofia"),
                      tuple("Stoyan Bachvarov Dramatic Theatre", "9000", "Stoqn Buchvarov, 1", "Varna"));
    }

    /**
     * Tests the Event repository method {@code findAllEvents()}. Method dose not
     * change the data base.
     */
    @SuppressWarnings({
        "static-method", "nls"
    })
    @Test
    @DataSet("genre.yml")
    @ExpectedDataSet("genre.yml")
    void genreContainsDatasetTest() {
        final List<Genre> foundVenues = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository genreRepository = provider.find(ReferentialRepository.class);
            return genreRepository.findAllGenres();
        });
        assertThat(foundVenues)
            .isNotNull()
            .hasSize(3)
            .extracting(Genre::getType)
            .contains("Brum And Base", "Jazz Rap", "Rock and Roll");
    }

    /**
     * Tests {@link ReferentialRepository#findGenre(String) findGenre} method and
     * asserts that the returned {@link Genre} object exists in dataSet "genre.yml".
     *
     * @param genreType
     *            The type of genre passed in
     *            {@link ReferentialRepository#findGenre(String) findGenre} method.
     *            Must not be null.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @ParameterizedTest
    @MethodSource("genreTestData")
    @DataSet("genre.yml")
    @ExpectedDataSet(value = "genre.yml", orderBy = "genre_type")
    void findGenreShouldReturnGenreWhenExists(final String genreType) {
        final Optional<Genre> actualGenre = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final ReferentialRepository genreRepository = provider.find(ReferentialRepository.class);
                return genreRepository.findGenre(genreType);
            });

        actualGenre
            .ifPresentOrElse(genre -> assertThat(genre).extracting(Genre::getType).isEqualTo(genreType),
                             () -> fail("Genre not found"));
    }

    /**
     * Tests {@link ReferentialRepository#findDeliveryType(String) findDeliveryType}
     * method and asserts that the returned {@link DeliveryType} object exists in
     * dataSet "deliveryType.yml".
     *
     * @param deliveryType
     *            The type of delivery passed in
     *            {@link ReferentialRepository#findDeliveryType(String)
     *            findDeliveryType} method. Must not be null.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @ParameterizedTest
    @MethodSource("DeliveryTypeTestData")
    @DataSet("deliveryType.yml")
    @ExpectedDataSet(value = "deliveryType.yml", orderBy = "delivery_type")
    void findDeliveryTypeShouldReturnDeliveryTypeWhenExists(final String deliveryType) {
        final Optional<DeliveryType> foundDeliveryType = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
                return referentialRepository.findDeliveryType(deliveryType);
            }, DeliveryType.class, deliveryType);
        foundDeliveryType
            .ifPresentOrElse(delivery -> assertThat(delivery).extracting(DeliveryType::getName).isEqualTo(deliveryType),
                             () -> fail("Delivery type not found!"));
    }

    /**
     * Tests {@link ReferentialRepository#findPurchaseStatus(String)
     * findPurchaseStatus} method and asserts that the returned
     * {@link PurchaseStatus} object exists in dataSet "purchaseStatus.yml".
     *
     * @param purchaseStatus
     *            The type of purchase passed in
     *            {@link ReferentialRepository#findPurchaseStatus(String)
     *            findPurchaseStatus} method. Must not be null.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @ParameterizedTest
    @MethodSource("PurchaseStatusTestData")
    @DataSet("purchaseStatus.yml")
    @ExpectedDataSet(value = "purchaseStatus.yml", orderBy = "purchase_status")
    void findPurchaseStatusShouldReturnPurchaseStatusWhenExists(final String purchaseStatus) {
        final Optional<PurchaseStatus> foundPurchaseStatus = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
                return referentialRepository.findPurchaseStatus(purchaseStatus);
            }, PurchaseStatus.class, purchaseStatus);
        foundPurchaseStatus
            .ifPresentOrElse(purchase -> assertThat(purchase)
                .extracting(PurchaseStatus::getName)
                .isEqualTo(purchaseStatus), () -> fail("Purchase status not found!"));
    }

    /**
     * Tests {@link ReferentialRepository#findPaymentType(String) findPaymentType}
     * method and asserts that the returned {@link PaymentType} object exists in
     * dataSet "paymentType.yml".
     *
     * @param paymentType
     *            The type of payment passed in
     *            {@link ReferentialRepository#findPaymentType(String)
     *            findPaymentType} method. Must not be null.
     */

    @SuppressWarnings({
        "nls", "static-method"
    })
    @ParameterizedTest
    @MethodSource("paymentTypeTestData")
    @DataSet("paymentType.yml")
    @ExpectedDataSet(value = "paymentType.yml", orderBy = "payment_type")
    void findPaymentTypeShouldReturnPaymentTypeWhenExists(final String paymentType) {
        final Optional<PaymentType> foundPaymentType = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
                return referentialRepository.findPaymentType(paymentType);
            });

        foundPaymentType
            .ifPresentOrElse(payment -> assertThat(payment).extracting(PaymentType::getName).isEqualTo(paymentType),
                             () -> fail("Payment type not found!"));
    }

    /**
     * Tests {@link ReferentialRepository#findPaymentType(String) findPaymentType}
     * method and asserts that it returns empty optional, when the searched payment
     * type does not exist in paymentType.yml
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("paymentType.yml")
    @ExpectedDataSet(value = "paymentType.yml", orderBy = "payment_type")
    void findPaymentTypeShouldReturnEmptyOptionalForNonExistingType() {
        final Optional<PaymentType> foundPaymentType = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
                return referentialRepository.findPaymentType("NonExistingPaymentType");
            });

        assertTrue(foundPaymentType.isEmpty());
    }

    /**
     * Tests {@link ReferentialRepository#findAllPhoneCodes() findAllPhoneCodes} and
     * asserts that the method returns all phone codes ordered by
     * {@code phone_code}.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("phoneCodes.yml")
    @ExpectedDataSet(value = "phoneCodes.yml", orderBy = "phone_code")
    void findAllPhoneCodesShouldReturnAll() {
        final List<PhoneCode> actualPhoneCodes = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
                return referentialRepository.findAllPhoneCodes();
            });

        assertThat(actualPhoneCodes).isNotEmpty().extracting(PhoneCode::getValue).contains("+30", "+39", "+359", "+49");
    }

    /**
     * Tests {@link ReferentialRepository#findPhoneCode(String) findPhoneCode}
     * method and asserts that the returned {@link PhoneCode} object exists in
     * dataSet "phoneCodes.yml".
     *
     * @param phoneCode
     *            The code passed in
     *            {@link ReferentialRepository#findPhoneCode(String) findPhoneCode}
     *            method. Must not be null.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @ParameterizedTest
    @MethodSource("phoneCodeTestData")
    @DataSet("phoneCodes.yml")
    @ExpectedDataSet(value = "phoneCodes.yml", orderBy = "phone_code")
    void findPhoneCodeShouldReturnPhoneCodeWhenExists(final String phoneCode) {
        final Optional<PhoneCode> actualGenre = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
                return referentialRepository.findPhoneCode(phoneCode);
            });

        actualGenre
            .ifPresentOrElse(genre -> assertThat(genre).extracting(PhoneCode::getValue).isEqualTo(phoneCode),
                             () -> fail("Phone code not found"));
    }

    /**
     * Tests {@link ReferentialRepository#findAllPerformers()} and asserts that the
     * method returns all performers ordered by their name.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("performers.yml")
    @ExpectedDataSet(value = "performers.yml", orderBy = "performer_name")
    void findAllPerformersShouldReturnAll() {
        final List<Performer> actualPerformers = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
                return referentialRepository.findAllPerformers();
            });

        assertThat(actualPerformers)
            .isNotNull()
            .hasSize(3)
            .extracting(Performer::getName, Performer::getDescription)
            .containsExactly(tuple("Grafa", "Grafa description."), tuple("Lubo Kirov", "Lubo Kirov description."),
                             tuple("Rolling Stones", "Rolling Stones description."));
    }

    /**
     * Tests {@link ReferentialRepository#findAllPerformers()} with an empty dataset
     * returns empty list.
     */
    @SuppressWarnings({
        "static-method"
    })
    @Test
    void findAllPerformersEmpty() {
        final List<Performer> actualPerformers = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
                return referentialRepository.findAllPerformers();
            });

        assertThat(actualPerformers).isEmpty();
    }

    /**
     * Tests the Referential repository method
     * {@link ReferentialRepository#findPerformer(String)}. Method does not change
     * the database.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("performers.yml")
    void findExistingPerformer() {
        final String performerName = "Grafa";

        final Optional<Performer> foundPerformer = transactionHandler.executeInTransaction(provider -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            return referentialRepository.findPerformer(performerName);
        });

        foundPerformer
            .ifPresentOrElse(performer -> assertThat(performer)
                .isNotNull()
                .extracting(Performer::getName, Performer::getDescription)
                .containsExactly("Grafa", "Grafa description."), () -> fail("Performer not found"));
    }

    /**
     * Tests the Referential repository method
     * {@link ReferentialRepository#findPerformer(String)} when searching for non
     * existing performer if it returns empty optional. Method does not change the
     * database.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    void findMissingPerformerReturnsEmptyOptional() {
        final String invalidPerformer = "invalidPerformer";

        final Optional<Performer> foundPerformer = transactionHandler.executeInTransaction(provider -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            return referentialRepository.findPerformer(invalidPerformer);
        });

        assertThat(foundPerformer).isNotNull().isEmpty();
    }

    /**
     * Tests the Referential repository method
     * {@link ReferentialRepository#createPerformer(String, String)}. Method changes
     * the data base.
     */

    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @ExpectedDataSet(value = "newPerformer.yml")
    void createPerformerShouldCreateNewEntity() {
        final Performer createdPerformer = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            return referentialRepository.createPerformer("Test Performer name", "Test Performer description.");
        });
        assertThat(createdPerformer)
            .extracting(Performer::getName, Performer::getDescription)
            .containsExactly("Test Performer name", "Test Performer description.");
    }

    @SuppressWarnings("nls")
    private static Stream<String> genreTestData() {
        return Stream.of("Brum And Base", "Jazz Rap", "Rock and Roll");
    }

    @SuppressWarnings("nls")
    private static Stream<String> paymentTypeTestData() {
        return Stream.of("BANK TRANSFER", "CASH", "DEBIT/CREDIT CARD");
    }

    @SuppressWarnings("nls")
    private static Stream<String> DeliveryTypeTestData() {
        return Stream.of("OFFICE_DELIVERY", "HOME_DELIVERY", "EMAIL_DELIVERY");
    }

    @SuppressWarnings("nls")
    private static Stream<String> PurchaseStatusTestData() {
        return Stream.of("COMPLETED", "AWAITING_PAYMENT");
    }

    @SuppressWarnings("nls")
    private static Stream<String> phoneCodeTestData() {
        return Stream.of("+30", "+39", "+359", "+49");
    }
}
