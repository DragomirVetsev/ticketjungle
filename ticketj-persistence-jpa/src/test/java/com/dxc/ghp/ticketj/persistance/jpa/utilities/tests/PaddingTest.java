package com.dxc.ghp.ticketj.persistance.jpa.utilities.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.dxc.ghp.ticketj.persistance.jpa.utilities.Padding;

/**
 * The class contains parameterized unit tests for the {@link Padding} class.
 */
class PaddingTest {

    /**
     * Tests the normal behavior of the {@link Padding#padRight(String, int)} method
     * with valid input.
     *
     * @param textToPad
     *            the input String to be padded.
     * @param maxLen
     *            the desired maximum length of the padded text.
     * @param paddedText
     *            the expected padded text.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @CsvSource({
        "'hello', 6,'hello '", "'ciao', 10, 'ciao      '", "'a', 3, 'a  '",
    })
    void testPadRightNormalArgument(final String textToPad, final int maxLen, final String paddedText) {
        final String result = Padding.padRight(textToPad, maxLen);
        assertEquals(paddedText, result);
    }

    /**
     * Tests the normal behavior of the {@link Padding#padRight(String, int)} method
     * with zero or negative lengths should return the input string unchanged.
     *
     * @param textToPad
     *            the input String to be padded.
     * @param maxLen
     *            the desired maximum length of the padded text.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @CsvSource({
        "'hello', 0", "'ciao', 0", "'a', -1",
    })
    void testPadRightZeroOrNegativeLengthShouldReturnInputString(final String textToPad, final int maxLen) {
        final String result = Padding.padRight(textToPad, maxLen);
        assertEquals(textToPad, result);
    }

    /**
     * Tests the normal behavior of the {@link Padding#padRight(String, int)} method
     * with input text's length greater than the maximum length, should return the
     * input string unchanged.
     *
     * @param textToPad
     *            the input String to be padded.
     * @param maxLen
     *            the desired maximum length of the padded text.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @CsvSource({
        "'hello', 3", "'ciao', 1", "'abcdefgh', 5",
    })
    void testPadRightInputStringLengthGreaterMaxLength(final String textToPad, final int maxLen) {
        final String result = Padding.padRight(textToPad, maxLen);
        assertEquals(textToPad, result);
    }

    /**
     * Tests the normal behavior of the {@link Padding#padRight(String, int)} method
     * with empty input text, should return the input string unchanged.
     *
     * @param textToPad
     *            the input String to be padded.
     * @param maxLen
     *            the desired maximum length of the padded text.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @CsvSource({
        "'', 0", "'', 1", "'', -1", "'', 5"
    })
    void testPadRightInputStringIsEmpty(final String textToPad, final int maxLen) {
        final String result = Padding.padRight(textToPad, maxLen);
        assertEquals(textToPad, result);
    }

}
