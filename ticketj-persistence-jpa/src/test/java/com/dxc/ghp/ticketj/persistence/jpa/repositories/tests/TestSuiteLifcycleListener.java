package com.dxc.ghp.ticketj.persistence.jpa.repositories.tests;

import java.util.logging.Logger;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import com.dxc.ghp.ticketj.persistence.jpa.layers.PersistenceLayer;

/**
 * Can execute code before each test class or executes code once after all tests
 * in every test class.
 */
public class TestSuiteLifcycleListener implements BeforeAllCallback, ExtensionContext.Store.CloseableResource {
    private static final Logger LOGGER = Logger.getLogger(TestSuiteLifcycleListener.class.getName());
    private final PersistenceLayer persistenceLayer;

    /**
     * Constructs TestSuiteSetupTeardownListener with specified
     * {@link PersistenceLayer}
     *
     * @param persistenceLayer
     *            the {@link PersistenceLayer} object that should be released after
     *            all tests
     */
    public TestSuiteLifcycleListener(final PersistenceLayer persistenceLayer) {
        this.persistenceLayer = persistenceLayer;
    }

    @Override
    public void beforeAll(final ExtensionContext context) {
        LOGGER.fine("Class tests start..."); //$NON-NLS-1$

        context
            .getRoot()
            .getStore(ExtensionContext.Namespace.GLOBAL)
            .getOrComputeIfAbsent(this.getClass().getName(), key -> this, TestSuiteLifcycleListener.class);
    }

    @SuppressWarnings("nls")
    @Override
    public void close() {
        persistenceLayer.release();
        LOGGER.info("Persistance layer released...");

    }

}
