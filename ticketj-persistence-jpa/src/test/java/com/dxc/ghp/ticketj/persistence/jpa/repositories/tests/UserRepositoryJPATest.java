package com.dxc.ghp.ticketj.persistence.jpa.repositories.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.persistence.entities.User;
import com.dxc.ghp.ticketj.persistence.repositories.UserRepository;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;

/**
 * Testing the User repository.
 */
final class UserRepositoryJPATest extends AbstractJPATest {

    /**
     * Tests the User repository method {@code findAllUsers()}. Method does not
     * change the data base.
     */

    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("user.yml")
    @ExpectedDataSet(value = "user.yml", orderBy = "username")
    void findAllUsersReturnsCorrectDataTest() {
        final List<User> foundUser = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final UserRepository userRepository = provider.find(UserRepository.class);
            return userRepository.findAllUsers();
        });
        assertThat(foundUser)
            .isNotNull()
            .hasSize(2)
            .extracting(User::getUsername, user -> user.getPersonName().givenName(),
                        user -> user.getPersonName().familyName(), User::getEmail, User::getPassword,
                        user -> user.getPhone().number(), user -> user.getPhone().code().getValue(),
                        user -> user.getAddress().street(), user -> user.getAddress().city(),
                        user -> user.getAddress().postCode(), User::getRole)
            .containsExactly(tuple("TicketHunter", "George", "Stefanov", "Stefanov@dxc.com", "8888888", "987654321",
                                   "+359", "Iskar str9", "Sofia", "2000", UserRole.CUSTOMER),
                             tuple("theWhathcer", "Petur", "Ivanov", "ivanoc@dxc.com", "87878787", "123456789", "+359",
                                   "Dunav str18", "Svishtov", "1000", UserRole.CUSTOMER));
    }

    /**
     * Testing with empty user list.
     */
    @SuppressWarnings({
        "static-method"
    })
    @Test
    void findAllUsersEmptyDataTest() {
        final List<User> foundUsers = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final UserRepository userRepository = provider.find(UserRepository.class);
            return userRepository.findAllUsers();
        });

        assertThat(foundUsers).isEmpty();
    }
}
