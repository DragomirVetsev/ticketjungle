package com.dxc.ghp.ticketj.persistence.jpa.repositories.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Office;
import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;
import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeRepository;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;
import com.dxc.ghp.ticketj.persistence.types.Phone;
import com.dxc.ghp.ticketj.persistence.types.RestrictedAddress;
import com.dxc.ghp.ticketj.persistence.types.UniqueOfficeLocation;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;

/**
 * Testing the Office repository.
 */
final class OfficeRepositoryJPATest extends AbstractJPATest {

    /**
     * Tests the Office repository method {@code findAllOffices()}. Method does not
     * change the data base.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml", orderBy = "officeName")
    void findAllOfficesReturnsCorrectDataTest() {
        transactionHandler.<Void>executeInTransaction((final ServiceProvider provider) -> {
            final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
            final List<Office> foundOffice = officeRepository.findAllOffices();

            assertThat(foundOffice)
                .isNotNull()
                .hasSize(3)
                .extracting(Office::getOfficeName, office -> office.getAddress().street(),
                            office -> office.getAddress().postCode(),
                            office -> office.getAddress().city().getCityName(), Office::getEmail,
                            office -> office.getPhone().code().getValue(), office -> office.getPhone().number(),
                            office -> office.getWorkingPeriod().startTime(),
                            office -> office.getWorkingPeriod().endTime(),
                            office -> office.getWorkers().stream().map(OfficeWorker::getUsername).toList())
                .containsExactly(tuple("TicketJ 100", "ul. Todor Kableshkov, 8", "1000", "Sofia",
                                       "office_100@ticketj.com", "+359", "889764321", LocalTime.parse("09:00"),
                                       LocalTime.parse("18:00"), List.of("TicketHunter")),
                                 tuple("TicketJ 105", "ul. General Gurko", "4000", "Plovdiv", "office_105@ticketj.com",
                                       "+359", "889745721", LocalTime.parse("09:00"), LocalTime.parse("18:00"),
                                       List.of("bobo")),
                                 tuple("TicketJ 83", "ul. Antim I, 30", "8000", "Burgas", "office_83@ticketj.com",
                                       "+359", "889583721", LocalTime.parse("09:00"), LocalTime.parse("18:00"),
                                       List.of()));
            return null;
        });
    }

    /**
     * Tests the Office repository method {@code findAllOffices()}. Method does not
     * change the data base.
     */
    @SuppressWarnings({
        "static-method"
    })
    @Test
    void findAllOfficesEmptyDataTest() {
        final List<Office> foundOffices = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
            return officeRepository.findAllOffices();
        });

        assertThat(foundOffices).isEmpty();
    }

    /**
     * Tests the Office repository method {@code createOffice()}. Method changes the
     * data base.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("office.yml")
    @ExpectedDataSet(value = "withNewOffice.yml")
    void createOfficeShouldCreateNewEntity() {
        transactionHandler.<Void>executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final City city = referentialRepository.findCity("Sofia").get();
            final RestrictedAddress officeAddress = new RestrictedAddress("test street", city, "1839");
            final PhoneCode code = referentialRepository.findPhoneCode("+359").get();
            final Phone officePhone = new Phone(code, "222444567");
            final Period<LocalTime> workingHours = new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00"));

            final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
            final Office createdOffice = officeRepository
                .createOffice("TestTicketJ 777", officeAddress, officePhone, "test_email@ticketj.com", workingHours);

            assertThat(createdOffice)
                .extracting(Office::getOfficeName, office -> office.getAddress().street(),
                            office -> office.getAddress().postCode(),
                            office -> office.getAddress().city().getCityName(), Office::getEmail,
                            office -> office.getPhone().code().getValue(), office -> office.getPhone().number(),
                            office -> office.getWorkingPeriod().startTime(),
                            office -> office.getWorkingPeriod().endTime(),
                            office -> office.getWorkers().stream().map(OfficeWorker::getUsername).toList())
                .containsExactly("TestTicketJ 777", "test street", "1839", "Sofia", "test_email@ticketj.com", "+359",
                                 "222444567", LocalTime.parse("09:00"), LocalTime.parse("18:00"), List.of());
            return null;
        });
    }

    /**
     * Tests {@link OfficeRepository#findOffice(String) findOffice} method and
     * asserts that the returned {@link Office} object exists in dataSet
     * "office.yml".
     *
     * @param officeName
     *            The code passed in {@link OfficeRepository#findOffice(String)
     *            findOffice} method. Must not be null.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @ParameterizedTest
    @MethodSource("officeNameTestData")
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml", orderBy = "office_name")
    void findOfficeShouldReturnExistingEntity(final String officeName) {
        final Optional<Office> actualOffice = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
                return officeRepository.findOffice(officeName);
            });

        actualOffice
            .ifPresentOrElse(office -> assertThat(office).extracting(Office::getOfficeName).isEqualTo(officeName),
                             () -> fail("Office not found"));
    }

    /**
     * Tests {@link OfficeRepository#emailExists(String) emailExists} method and
     * asserts that the returned boolean is true since email exists in dataSet
     * "office.yml".
     *
     * @param email
     *            The code passed in {@link OfficeRepository#emailExists(String)
     *            emailExists} method. Must not be null.
     */
    @SuppressWarnings({
        "static-method", "boxing"
    })
    @ParameterizedTest
    @MethodSource("officeEmailTestData")
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml", orderBy = "office_name")
    void emailExistsShouldReturnTrue(final String email) {
        final boolean actual = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
            return officeRepository.emailExists(email);
        });

        assertThat(actual).isTrue();
    }

    /**
     * Tests {@link OfficeRepository#emailExists(String) emailExists} method and
     * asserts that the returned boolean is false since email does not exist in
     * dataSet "office.yml".
     *
     * @param email
     *            The code passed in {@link OfficeRepository#emailExists(String)
     *            emailExists} method. Must not be null.
     */
    @SuppressWarnings({
        "static-method", "boxing"
    })
    @ParameterizedTest
    @MethodSource("officeEmailInvalidTestData")
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml", orderBy = "office_name")
    void emailExistsShouldReturnFalse(final String email) {
        final boolean actual = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
            return officeRepository.emailExists(email);
        });

        assertThat(actual).isFalse();
    }

    /**
     * Tests {@link OfficeRepository#phoneExists(Phone) phoneExists} method and
     * asserts that the returned boolean is true since the phone exists in dataSet
     * "office.yml".
     *
     * @param code
     *            The phone code used to create {@link Phone} object. Must not be
     *            null.
     * @param number
     *            The phone number used to create {@link Phone} object. Must not be
     *            null.
     */
    @SuppressWarnings({
        "static-method", "boxing"
    })
    @ParameterizedTest
    @MethodSource("officePhoneTestData")
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml", orderBy = "office_name")
    void phoneExistsShouldReturnTrue(final String code, final String number) {
        final boolean actual = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepostory = provider.find(ReferentialRepository.class);
            final PhoneCode phoneCode = referentialRepostory.findPhoneCode(code).get();
            final Phone testPhone = new Phone(phoneCode, number);

            final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
            return officeRepository.phoneExists(testPhone);
        });

        assertThat(actual).isTrue();
    }

    /**
     * Tests {@link OfficeRepository#phoneExists(Phone) phoneExists} method and
     * asserts that the returned boolean is false since the phone does not exist in
     * dataSet "office.yml".
     *
     * @param code
     *            The phone code used to create {@link Phone} object. Must not be
     *            null.
     * @param number
     *            The phone number used to create {@link Phone} object. Must not be
     *            null.
     */
    @SuppressWarnings({
        "static-method", "boxing"
    })
    @ParameterizedTest
    @MethodSource("officePhoneInvalidTestData")
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml", orderBy = "office_name")
    void phoneExistsShouldReturnFalse(final String code, final String number) {
        final boolean actual = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepostory = provider.find(ReferentialRepository.class);
            final PhoneCode phoneCode = referentialRepostory.findPhoneCode(code).get();
            final Phone testPhone = new Phone(phoneCode, number);

            final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
            return officeRepository.phoneExists(testPhone);
        });

        assertThat(actual).isFalse();
    }

    /**
     * Tests {@link OfficeRepository#addressExists(UniqueOfficeLocation)
     * addressExists} method and asserts that the returned boolean is true since the
     * address exists in dataSet "office.yml".
     *
     * @param city
     *            The city used to create {@link UniqueOfficeLocation} object. Must
     *            not be null.
     * @param street
     *            The street used to create {@link UniqueOfficeLocation} object.
     *            Must not be null.
     */
    @SuppressWarnings({
        "static-method", "boxing"
    })
    @ParameterizedTest
    @MethodSource("officeAddressTestData")
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml", orderBy = "office_name")
    void addressExistsShouldReturnTrue(final String city, final String street) {
        final boolean actual = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepostory = provider.find(ReferentialRepository.class);
            final City phoneCode = referentialRepostory.findCity(city).get();
            final UniqueOfficeLocation testAddress = new UniqueOfficeLocation(phoneCode, street);

            final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
            return officeRepository.addressExists(testAddress);
        });

        assertThat(actual).isTrue();
    }

    /**
     * Tests {@link OfficeRepository#addressExists(UniqueOfficeLocation)
     * addressExists} method and asserts that the returned boolean is false since
     * the address does not exist in dataSet "office.yml".
     *
     * @param city
     *            The city used to create {@link UniqueOfficeLocation} object. Must
     *            not be null.
     * @param street
     *            The street used to create {@link UniqueOfficeLocation} object.
     *            Must not be null.
     */
    @SuppressWarnings({
        "static-method", "boxing"
    })
    @ParameterizedTest
    @MethodSource("officeAddressInvalidTestData")
    @DataSet("office.yml")
    @ExpectedDataSet(value = "office.yml", orderBy = "office_name")
    void addressExistsShouldReturnFalse(final String city, final String street) {
        final boolean actual = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepostory = provider.find(ReferentialRepository.class);
            final City phoneCode = referentialRepostory.findCity(city).get();
            final UniqueOfficeLocation testAddress = new UniqueOfficeLocation(phoneCode, street);

            final OfficeRepository officeRepository = provider.find(OfficeRepository.class);
            return officeRepository.addressExists(testAddress);
        });

        assertThat(actual).isFalse();
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> officePhoneInvalidTestData() {
        return Stream
            .of(arguments("+359", "889760321"), arguments("+359", "189745721"), arguments("+359", "822583721"));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> officePhoneTestData() {
        return Stream
            .of(arguments("+359", "889764321"), arguments("+359", "889745721"), arguments("+359", "889583721"));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> officeAddressInvalidTestData() {
        return Stream
            .of(arguments("Sofia", "ul. Todoreni Kukli, 8"), arguments("Sofia", "ul. General Gurko"),
                arguments("Plovdiv", "ul. 57 I, 30"));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> officeAddressTestData() {
        return Stream
            .of(arguments("Sofia", "ul. Todor Kableshkov, 8"), arguments("Plovdiv", "ul. General Gurko"),
                arguments("Burgas", "ul. Antim I, 30"));
    }

    @SuppressWarnings("nls")
    private static Stream<String> officeEmailInvalidTestData() {
        return Stream.of("office_555@ticketj.com", "office_hq@ticketj.com", "office_new@ticketj.com");
    }

    @SuppressWarnings("nls")
    private static Stream<String> officeEmailTestData() {
        return Stream.of("office_100@ticketj.com", "office_105@ticketj.com", "office_83@ticketj.com");
    }

    @SuppressWarnings("nls")
    private static Stream<String> officeNameTestData() {
        return Stream.of("TicketJ 100", "TicketJ 105", "TicketJ 83");
    }

}
