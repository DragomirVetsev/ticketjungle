/**
 *
 */
package com.dxc.ghp.ticketj.persistence.jpa.repositories.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.repositories.EventRepository;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;

/**
 * Testing the Event repository.
 */
final class EventRepositoryJPATest extends AbstractJPATest {

    /**
     * Tests the Event repository method {@code findAllEvents()}. Method does not
     * change the data base.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("eventsWithGeners.yml")
    @ExpectedDataSet("eventsWithGeners.yml")
    void eventContainsEventsWithGenresTest() {
        final List<Event> foundEvents = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository.findAllEvents();
        });
        assertThat(foundEvents)
            .isNotNull()
            .hasSize(3)
            .extracting(Event::getName, Event::getDescription, event -> event.getGenre().getType())
            .containsExactly(tuple("Coca-cola Fest", "Your summer music in your city", "Pop"),
                             tuple("Coca-cola Fest3", "Your summer music in your city", "Pop"),
                             tuple("Grafa consert", "Consert whit the Graf", "Rock"));
    }

    /**
     * Testing whit e empty data set.
     */
    @SuppressWarnings({
        "static-method"
    })
    @Test
    void findAllEventsEmptyData() {
        final List<Event> foundEvents = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository.findAllEvents();
        });

        assertThat(foundEvents).isEmpty();
    }

    /**
     * Tests the Event repository method {@link EventRepository#findByName(String)}.
     * Method does not change the database.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("eventsWithGeners.yml")
    @ExpectedDataSet("eventsWithGeners.yml")
    void findExistingEvent() {
        final String eventName = "Coca-cola Fest";

        final Optional<Event> foundEvent = transactionHandler.executeInTransaction(provider -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository.findByName(eventName);
        });

        foundEvent
            .ifPresentOrElse(event -> assertThat(event)
                .isNotNull()
                .extracting(Event::getName, Event::getDescription, eventLambda -> eventLambda.getGenre().getType())
                .containsExactly("Coca-cola Fest", "Your summer music in your city", "Pop"),
                             () -> fail("Event not found"));
    }

    /**
     * Tests the Event repository method {@link EventRepository#findByName(String)}
     * when searching for non existing event if it returns empty optional. Method
     * does not change the database.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    void findMissingEventReturnsEmptyOptional() {
        final String invalidEvent = "invalidEvent";

        final Optional<Event> foundEvent = transactionHandler.executeInTransaction(provider -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository.findByName(invalidEvent);
        });

        assertThat(foundEvent).isNotNull().isEmpty();
    }

    /**
     * Tests {@link EventRepository#findSubEvent(PerformanceId)} method for existing
     * SubEvent. Method does not change the data base.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("subEventNotEmpty.yml")
    @ExpectedDataSet("subEventNotEmpty.yml")
    void findExistingSubEvent() {
        final String venueName = "NDKa";
        final String stageName = "Zala 11";
        final Instant startTime = Instant.parse("2023-07-20T20:00:00Z");
        final String subEventName = "My Satisfaction Sofia";
        final String eventName = "ROLLING STONES TOURS";
        final PerformanceId performance = new PerformanceId(subEventName, eventName);

        final Optional<SubEvent> foundSubEvent = transactionHandler.executeInTransaction((provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository.findSubEvent(performance);
        });

        foundSubEvent
            .ifPresentOrElse(subEvent -> assertThat(subEvent)
                .isNotNull()
                .extracting(subEventLambda -> subEvent.getEvent().getName(), SubEvent::getName,
                            SubEvent::getDescription, subEventLambda -> subEvent.getStage().getVenue().getName(),
                            subEventLambda -> subEvent.getStage().getName(), SubEvent::getStartTime,
                            SubEvent::getTicketPrice)
                .contains(eventName, subEventName, "The world tour comes to Bulgaria", venueName, stageName, startTime,
                          Double.valueOf(8.45)), () -> fail("Sub Event not found"));
    }

    /**
     * Tests {@link EventRepository#findSubEvent(PerformanceId)} method for not
     * existing SubEvent. Method does not change the data base.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    void findNonExistingSubEventEmpty() {
        final PerformanceId performance = new PerformanceId("InvalidSubEventName", "InvalidEventName");

        final Optional<SubEvent> foundSubEvent = transactionHandler.executeInTransaction((provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository.findSubEvent(performance);
        });

        assertThat(foundSubEvent).isNotNull().isEmpty();
    }

    /**
     * Tests the Event repository method
     * {@link EventRepository#findAllSubEventsOrderedByTime()}. Method does not
     * change the database.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("findAllSubEventsOrderedDataset/subEventOrderedByTimeNotEmpty.yml")
    @ExpectedDataSet("findAllSubEventsOrderedDataset/subEventOrderedByTimeNotEmptyExpected.yml")
    void findAllSubEventsOrderedByTime() {
        final List<SubEvent> foundSubEvents = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final EventRepository eventRepository = provider.find(EventRepository.class);
                return eventRepository.findAllSubEventsOrderedByTime();
            });

        assertThat(foundSubEvents)
            .isNotNull()
            .hasSize(3)
            .extracting(SubEvent::getName, SubEvent::getDescription, subEvent -> subEvent.getEvent().getName(),
                        subEvent -> subEvent.getStage().getVenue().getName(), subEvent -> subEvent.getStage().getName(),
                        SubEvent::getTicketPrice, SubEvent::getStartTime)
            .containsExactly(tuple("My Satisfaction Sofia3", "The world tour comes to Bulgaria", "ROLLING STONES TOURS",
                                   "NDKa", "Zala 11", Double.valueOf(8.45), Instant.parse("2023-07-30T20:00:00Z")),
                             tuple("My Satisfaction Sofia2", "The world tour comes to Bulgaria", "ROLLING STONES TOURS",
                                   "NDKa", "Zala 11", Double.valueOf(8.45), Instant.parse("2023-07-25T20:00:00Z")),
                             tuple("My Satisfaction Sofia1", "The world tour comes to Bulgaria", "ROLLING STONES TOURS",
                                   "NDKa", "Zala 11", Double.valueOf(8.45), Instant.parse("2023-07-20T20:00:00Z")));
    }

    /**
     * Tests the Event repository method
     * {@link EventRepository#findAllSubEventsOrderedByTime()} when searching in
     * empty database. Method does not change the database.
     */
    @SuppressWarnings({
        "static-method"
    })
    @Test
    void findAllSubEventsOrderedByTimeEmpty() {
        final List<SubEvent> foundSubEvents = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final EventRepository eventRepository = provider.find(EventRepository.class);
                return eventRepository.findAllSubEventsOrderedByTime();
            });

        assertThat(foundSubEvents).isEmpty();
    }

    /**
     * Tests the Event repository method
     * {@link EventRepository#findAllSubEventsOrderedByPopularity()}. Method does
     * not change the database.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("findAllSubEventsOrderedDataset/subEventOrderedByPopularityNotEmpty.yml")
    @ExpectedDataSet("findAllSubEventsOrderedDataset/subEventOrderedByPopularityNotEmptyExpected.yml")
    void findAllSubEventsOrderedByPopularity() {
        final List<SubEvent> foundSubEvents = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final EventRepository eventRepository = provider.find(EventRepository.class);
                return eventRepository.findAllSubEventsOrderedByPopularity();
            });

        assertThat(foundSubEvents)
            .isNotNull()
            .hasSize(2)
            .extracting(SubEvent::getName, SubEvent::getDescription, subEvent -> subEvent.getEvent().getName(),
                        subEvent -> subEvent.getStage().getVenue().getName(), subEvent -> subEvent.getStage().getName(),
                        SubEvent::getTicketPrice, SubEvent::getStartTime)
            .containsExactly(tuple("My Satisfaction Sofia1", "The world tour comes to Bulgaria", "ROLLING STONES TOURS",
                                   "NDKa", "Zala 11", Double.valueOf(8.45), Instant.parse("2023-07-20T20:00:00Z")),
                             tuple("My Satisfaction Sofia2", "The world tour comes to Bulgaria", "ROLLING STONES TOURS",
                                   "NDKa", "Zala 11", Double.valueOf(8.45), Instant.parse("2023-07-25T20:00:00Z")));
    }

    /**
     * Tests the Event repository method
     * {@link EventRepository#findAllSubEventsOrderedByPopularity()} when searching
     * in empty database. Method does not change the database.
     */
    @SuppressWarnings({
        "static-method"
    })
    @Test
    void findAllSubEventsOrderedByPopularityEmpty() {
        final List<SubEvent> foundSubEvents = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final EventRepository eventRepository = provider.find(EventRepository.class);
                return eventRepository.findAllSubEventsOrderedByPopularity();
            });

        assertThat(foundSubEvents).isEmpty();
    }

    /**
     * Tests the Event repository method {@code createEvent()}. Method changes the
     * data base.
     */

    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("eventsWithGeners.yml")
    @ExpectedDataSet(value = "newEvent.yml")
    void createEventShouldCreateNewEntity() {
        final Event createdEvent = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final Genre genre = referentialRepository.findGenre("Pop").get();
            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository.createEvent("Test Event name", "Test Event description", genre);
        });
        assertThat(createdEvent)
            .extracting(Event::getName, Event::getDescription, event -> event.getGenre().getType())
            .contains("Test Event name", "Test Event description", "Pop");
    }

    /**
     * Tests the Sub Event repository method {@code createSubEvent()}. Method
     * changes the data base.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("subEventNotEmpty.yml")
    @ExpectedDataSet(value = "newSubEvent.yml")
    void createSubEventShouldCreateNewEntity() {
        final SubEvent createdSubEvent = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final Event event = referentialRepository.findEvent("ROLLING STONES TOURS").get();
            final Stage stage = findStageOrThrow("Zala 11", "NDKa", referentialRepository);
            final List<String> performers = new ArrayList<>();
            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository
                .createSubEvent("My Satisfaction", event, "The world tour comes", 10.00,
                                Instant.parse("2023-10-10T10:00:00Z"), stage, performers);
        });
        assertThat(createdSubEvent)
            .extracting(SubEvent::getName, subEvent -> subEvent.getEvent().getName(), SubEvent::getDescription,
                        SubEvent::getTicketPrice, SubEvent::getStartTime, subEvent -> subEvent.getStage().getName(),
                        SubEvent::getPerformers)
            .contains("My Satisfaction", "ROLLING STONES TOURS", "The world tour comes", Double.valueOf(10.00),
                      Instant.parse("2023-10-10T10:00:00Z"), "Zala 11");

    }

    /**
     * Tests {@link EventRepository#findEvent(String) findEvent} method and asserts
     * that the returned {@link Event} object exists in dataSet
     * "eventsWithGeners.yml".
     *
     * @param eventName
     *            The code passed in {@link EventRepository#findEvent(String)
     *            findEvent} method. Must not be null.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @ParameterizedTest
    @MethodSource("eventTestData")
    @DataSet("eventsWithGeners.yml")
    @ExpectedDataSet(value = "eventsWithGeners.yml")
    void findEventShouldReturnExistingEntity(final String eventName) {
        final Optional<Event> actualEvent = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final EventRepository eventRepository = provider.find(EventRepository.class);
                return eventRepository.findEvent(eventName);
            });

        actualEvent
            .ifPresentOrElse(event -> assertThat(event).extracting(Event::getName).isEqualTo(eventName),
                             () -> fail("Event not found"));
    }

    /**
     * Tests the Event repository method
     * {@link EventRepository#deleteSubEvent(SubEvent)}. Method changes the data
     * base.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("subEventNotEmpty.yml")
    @ExpectedDataSet("deletedSubEvent.yml")
    void deleteEventShouldDeleteEntity() {
        final String subEventName = "My Satisfaction Sofia";
        final String eventName = "ROLLING STONES TOURS";
        final String venueName = "NDKa";
        final String stageName = "Zala 11";
        final Instant startTime = Instant.parse("2023-07-20T20:00:00Z");
        final PerformanceId performance = new PerformanceId(subEventName, eventName);

        final SubEvent deletedSubEvent = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final SubEvent subEvent = eventRepository
                .findSubEvent(performance)
                .orElseGet(() -> fail("Sub-event not found"));

            eventRepository.deleteSubEvent(subEvent);
            return subEvent;
        });

        assertThat(deletedSubEvent)
            .extracting(SubEvent::getName, deleted -> deleted.getEvent().getName(), SubEvent::getDescription,
                        deleted -> deleted.getStage().getVenue().getName(), deleted -> deleted.getStage().getName(),
                        SubEvent::getStartTime, SubEvent::getTicketPrice)
            .containsExactly("My Satisfaction Sofia", "ROLLING STONES TOURS", "The world tour comes to Bulgaria",
                             venueName, stageName, startTime, Double.valueOf(8.45));
    }

    @SuppressWarnings("nls")
    private static Stream<String> eventTestData() {
        return Stream.of("Coca-cola Fest", "Coca-cola Fest3", "Grafa consert");
    }

    @SuppressWarnings("nls")
    private static Stage findStageOrThrow(final String stage, final String venue,
                                          final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findStage(stage, venue)
            .orElseThrow(() -> new EntityNotFoundException(SubEvent.class,
                String.format("Stage '%s' with Venue '%s' not found.", stage, venue)));
    }

}
