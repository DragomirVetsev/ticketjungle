package com.dxc.ghp.ticketj.persistence.jpa.repositories.tests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.types.DeliveryTypeEnum;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;
import com.dxc.ghp.ticketj.persistence.entities.Order;
import com.dxc.ghp.ticketj.persistence.entities.PaymentType;
import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;
import com.dxc.ghp.ticketj.persistence.entities.Review;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.repositories.CustomerRepository;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;

/**
 * Testing the {@link CustomerRepository}
 */
public final class CustomerRepositoryJPATest extends AbstractJPATest {

    /**
     * Tests that the {@link CustomerRepository} method - findCustomer() returns
     * correct data.
     */
    @SuppressWarnings({
        "static-method", "nls"
    })
    @Test
    @DataSet("customer.yml")
    @ExpectedDataSet(value = "customer.yml", orderBy = "username")
    void findCustomerWithBirthday() {
        final Optional<Customer> foundCustomer = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
                return customerRepository.findCustomer("bobo");
            });

        foundCustomer
            .ifPresentOrElse(customerToCheck -> assertThat(customerToCheck)
                .extracting(Customer::getUsername, customer -> customer.getPersonName().givenName(),
                            customer -> customer.getPersonName().familyName(), Customer::getEmail,
                            customer -> customer.getAddress().city(), customer -> customer.getAddress().street(),
                            customer -> customer.getAddress().postCode(),
                            customer -> customer.getPhone().code().getValue(), customer -> customer.getPhone().number(),
                            Customer::getRole, Customer::getBirthday)
                .contains("bobo", "Boris", "Spaski", "Spaski@dxc.com", "Peterburg", "Volga str22", "3322", "+39",
                          "987654321", UserRole.CUSTOMER, LocalDate.of(1945, 1, 15)), () -> fail("Customer not found"));
    }

    /**
     * Tests that the {@link CustomerRepository} method - findCustomer() returns
     * correct data.
     */
    @SuppressWarnings({
        "static-method", "nls"
    })
    @Test
    @DataSet("customer.yml")
    @ExpectedDataSet(value = "customer.yml", orderBy = "username")
    void findCustomerWithoutBirthday() {
        final Optional<Customer> foundCustomer = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
                return customerRepository.findCustomer("TicketHunter");
            });

        foundCustomer
            .ifPresentOrElse(customerToCheck -> assertThat(customerToCheck)
                .extracting(Customer::getUsername, customer -> customer.getPersonName().givenName(),
                            customer -> customer.getPersonName().familyName(), Customer::getEmail,
                            customer -> customer.getAddress().city(), customer -> customer.getAddress().street(),
                            customer -> customer.getAddress().postCode(),
                            customer -> customer.getPhone().code().getValue(), customer -> customer.getPhone().number(),
                            Customer::getRole, Customer::getBirthday)
                .contains("TicketHunter", "George", "Stefanov", "Stefanov@dxc.com", "Sofia", "Iskar str9", "2000",
                          "+359", "987654321", UserRole.CUSTOMER, null), () -> fail("Customer not found"));
    }

    /**
     * Tests the Customer wish list method - findWishlist().Method returns correct
     * data.
     */

    @SuppressWarnings({
        "static-method", "nls"
    })
    @Test
    @DataSet("wishlist.yml")
    @ExpectedDataSet(value = "wishlist.yml")
    void findCustomerWithWishlist() {
        transactionHandler.<Void>executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Optional<Customer> foundCustomer = customerRepository.findCustomer("bobo");

            foundCustomer
                .ifPresentOrElse(customer -> assertThat(customer.getWishlist())
                    .isNotEmpty()
                    .hasSize(2)
                    .extracting(SubEvent::getName, subEvent -> subEvent.getStage().getVenue().getName(),
                                subEvent -> subEvent.getStage().getName(), SubEvent::getDescription,
                                SubEvent::getTicketPrice, SubEvent::getStartTime)
                    .containsExactly(tuple("My Satisfaction Sofia", "NDKa", "Zala 11",
                                           "The world tour comes to Bulgaria", Double.valueOf(8.45),
                                           Instant.parse("2023-07-20T20:00:00Z")),
                                     tuple("Summer Fest Tour", "Sea Theater", "Zala 5",
                                           "The world summer tour has come to Bulgaria", Double.valueOf(12.15),
                                           Instant.parse("2023-12-12T16:00:00Z"))),
                                 () -> fail("Customer Not Found"));
            return null;

        });
    }

    /**
     * Tests the Customer findWishlist() method, when customer has no events added
     * to its wish list, returns empty {@code List<SubEvents>}.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet(value = "wishlist.yml")
    @ExpectedDataSet(value = "wishlist.yml")
    void findCustomerWithEmptyWishlist() {
        transactionHandler.<Void>executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Optional<Customer> foundCustomer = customerRepository.findCustomer("giliev");

            foundCustomer
                .ifPresentOrElse(customer -> assertThat(customer.getWishlist()).isEmpty(),
                                 () -> fail("Customer Not Found!"));
            return null;
        });
    }

    /**
     * Tests
     * {@link CustomerRepository#findSubEventFromWishlist(String, PerformanceId)} by
     * passing a valid data. Expecting the result {@code Optional<SubEvent>)} to be
     * {@code present}, not {@code null} and to contains exactly the data that is
     * compared to.
     */
    @SuppressWarnings({
        "static-method", "nls"
    })
    @Test
    @DataSet(value = "customerWishlistSubEventNotEmpty.yml")
    @ExpectedDataSet(value = "customerWishlistSubEventNotEmpty.yml")
    void findSubEventFromWishlistReturnsExpectedDataSet() {
        final String username = "bobo";
        final String eventName = "ROLLING STONES TOURS";
        final String subEventName = "My Satisfaction Sofia";
        final PerformanceId performanceId = new PerformanceId(subEventName, eventName);

        final Optional<SubEvent> foundSubEvent = transactionHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final CustomerRepository customerRepository = provider.find(CustomerRepository.class);

                return customerRepository.findSubEventFromWishlist(username, performanceId);
            });

        foundSubEvent
            .ifPresentOrElse(subEvent -> assertThat(subEvent)
                .isNotNull()
                .extracting(SubEvent::getId, SubEvent::getName, event -> event.getEvent().getName(),
                            event -> event.getEvent().getDescription(), event -> event.getEvent().getGenre().getType(),
                            SubEvent::getDescription, SubEvent::getTicketPrice, SubEvent::getStartTime,
                            stage -> stage.getStage().getName(), stage -> stage.getStage().getVenue().getName())
                .containsExactly(new PerformanceId("My Satisfaction Sofia", "ROLLING STONES TOURS"),
                                 "My Satisfaction Sofia", "ROLLING STONES TOURS", "WORLD TOUR SATISFACTIONS", "Pop",
                                 "The world tour comes to Bulgaria", Double.valueOf(8.45),
                                 Instant.parse("2023-07-20T20:00:00Z"), "Zala 11", "NDKa"),
                             () -> fail("Customer Not Found"));
    }

    /**
     * Test case to verify that the "findCustomer" method in the CustomerRepository
     * associated with List of Reviews of a customer. The test uses a test data set
     * ("reviews.yml") that contains data for the "bobo" customer, and it ensures
     * that the returned list of reviews has the expected size (1) and contains the
     * correct review data.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet("reviews.yml")
    @ExpectedDataSet(value = "reviews.yml")
    void findReviewNotEmptyReturnsExpectedDataSet() {
        final List<Review> reviews = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Optional<Customer> customer = customerRepository.findCustomer("bobo");

            return customer.get().getReviews().stream().toList();
        });
        assertThat(reviews)
            .isNotNull()
            .hasSize(2)
            .extracting(review -> review.getSubEvent().getName(), review -> review.getSubEvent().getEvent().getName(),
                        review -> review.getSubEvent().getStage().getVenue().getName(),
                        review -> review.getSubEvent().getStage().getName(), Review::getComment, Review::getScore)
            .contains(tuple("My Satisfaction Sofia", "ROLLING STONES TOUR", "NDK", "Zala 1", "Amazing",
                            Integer.valueOf(5)),
                      tuple("Mercury - Sofia", "Mercury World Tour", "NDK", "Zala 1", "TOP TOP TOP",
                            Integer.valueOf(5)));
    }

    /**
     * Test case to verify that the "findCustomer" method in the CustomerRepository
     * associated with List of Reviews of a customer. The test uses a test data set
     * ("reviews.yml") that contains data for the "giliev" customer, and it ensures
     * that the returned list of reviews that is empty.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    @DataSet(value = "reviews.yml")
    @ExpectedDataSet(value = "reviews.yml")
    void findReviewEmptyReturnsExpectedDataSet() {
        final List<Review> reviews = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Optional<Customer> customer = customerRepository.findCustomer("giliev");

            return customer.get().getReviews().stream().toList();
        });
        assertThat(reviews).isEmpty();
    }

    /**
     * Test case to verify that the CustomerRepository method
     * createOrderWithoutDeliveryAddress() will create order without delivery
     * address and returns correct data.
     */
    @SuppressWarnings({
        "static-method", "nls"
    })
    @Test
    @DataSet(value = "createOrderWithTicketsBefore.yml")
    @ExpectedDataSet(value = "createOrderWithTicketsAfter.yml", orderBy = "CREATION_DATE")
    void createOrderWithoutDeliveryAddressShouldCreate() {
        final Order createdOrder = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final PurchaseStatus purchaseStatus = referentialRepository
                .findPurchaseStatus("AWAITING_PAYMENT")
                .orElseGet(() -> fail("Purchase Status Not Found!"));
            final Customer customer = customerRepository
                .findCustomer("bobo")
                .orElseGet(() -> fail("Customer Not Found!"));
            final DeliveryType deliveryType = referentialRepository
                .findDeliveryType("EMAIL_DELIVERY")
                .orElseGet(() -> fail("Delivery type Not Found!"));
            final PaymentType paymentType = referentialRepository
                .findPaymentType("DEBIT/CREDIT CARD")
                .orElseGet(() -> fail("Payment type Not Found!"));

            final Instant date = Instant.parse("2022-02-20T20:00:00Z");

            return customerRepository
                .createOrderWithoutDeliveryAddress(purchaseStatus, customer, deliveryType, paymentType, date);
        });

        assertThat(createdOrder)
            .extracting(order -> order.getPurchaseStatus().getName(), order -> order.getCustomer().getUsername(),
                        order -> order.getDeliveryType().getName(), order -> order.getPaymentType().getName())
            .contains("AWAITING_PAYMENT", "bobo", "EMAIL_DELIVERY", "DEBIT/CREDIT CARD");
    }

    /**
     * Test case to verify that the CustomerRepository method
     * createOrderWithDeliveryAddress() will create order with delivery address and
     * returns correct data..
     */
    @SuppressWarnings({
        "static-method", "nls"
    })
    @Disabled
    @Test
    @DataSet(value = "createOrderWithDeliveryAddressBefore.yml")
    @ExpectedDataSet(value = "createOrderWithDeliveryAddressAfter.yml", orderBy = "CREATION_DATE")
    void createOrderWithDeliveryAddressSholudCreate() {
        final Order createdOrder = transactionHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final PurchaseStatus purchaseStatus = referentialRepository
                .findPurchaseStatus("AWAITING_PAYMENT")
                .orElseGet(() -> fail("Purchase Status Not Found!"));
            final Customer customer = customerRepository
                .findCustomer("bobo")
                .orElseGet(() -> fail("Customer Not Found!"));
            final DeliveryType deliveryType = referentialRepository
                .findDeliveryType(DeliveryTypeEnum.HOME_DELIVERY.name())
                .orElseGet(() -> fail("Delivery type Not Found!"));
            final PaymentType paymentType = referentialRepository
                .findPaymentType("BANK TRANSFER")
                .orElseGet(() -> fail("Payment type Not Found!"));
            final UnrestrictedAddress unrestrictedAddress = new UnrestrictedAddress("Rakovska", "Sofia", "1000");
            final Instant date = Instant.parse("2022-02-20T20:00:00Z");
            // TODO with delivery address.
            return customerRepository
                .createOrderWithDeliveryAddress(purchaseStatus, customer, deliveryType, paymentType, date,
                                                unrestrictedAddress);
        });

        assertThat(createdOrder)
            .extracting(order -> order.getPurchaseStatus().getName(), order -> order.getCustomer().getUsername(),
                        order -> order.getDeliveryType().getName(), order -> order.getPaymentType().getName(),
                        order -> order.getDeliveryAddress().city(), order -> order.getDeliveryAddress().street(),
                        order -> order.getDeliveryAddress().postCode())
            .contains("AWAITING_PAYMENT", "bobo", "EMAIL_DELIVERY", "DEBIT/CREDIT CARD", "Sofia", "Rakovska", "1000");
    }
}
