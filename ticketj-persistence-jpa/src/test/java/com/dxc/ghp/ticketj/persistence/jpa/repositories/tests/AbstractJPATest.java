package com.dxc.ghp.ticketj.persistence.jpa.repositories.tests;

import java.util.Map;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;

import com.dxc.ghp.ticketj.persistence.jpa.layers.PersistenceLayer;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.junit5.DBUnitExtension;

/**
 * Base class for all JPA repositories tests.
 */
@ExtendWith(DBUnitExtension.class)
@DataSet("cleanDB.yml")
abstract class AbstractJPATest {

    @SuppressWarnings("nls")
    private static final PersistenceLayer persistenceLayer = new PersistenceLayer(
        Map.of("jakarta.persistence.sql-load-script-source", "META-INF/oracle-scripts/emptyInsert.sql"));

    /**
     * Creates {@link TestSuiteLifcycleListener} object responsible for
     * releasing the {@link PersistenceLayer} object after all tests are finished.
     */
    @RegisterExtension
    static final TestSuiteLifcycleListener testSuiteSetupTeardownListener = new TestSuiteLifcycleListener(
        persistenceLayer);

    /**
     * Includes the {@link TransactionHandler} available for service testing
     * purposes.
     */
    protected static final TransactionHandler transactionHandler = persistenceLayer.createTransactionHandler();
}
