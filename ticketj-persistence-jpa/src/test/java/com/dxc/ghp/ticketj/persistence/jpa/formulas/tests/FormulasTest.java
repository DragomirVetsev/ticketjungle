package com.dxc.ghp.ticketj.persistence.jpa.formulas.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.testng.Assert;

import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;
import com.dxc.ghp.ticketj.persistence.jpa.formulas.Formulas;

/**
 * Contains all tests for {@link Formulas}.
 */
final class FormulasTest {

    /**
     * Tests the SubEvent entity methods for calculating the review score and count
     * with only non null Integers.
     */
    @SuppressWarnings("static-method")
    @Test
    void reviewScoreAndCountCalculateTest() {
        final List<Integer> reviewScoreList = List
            .of(Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3), Integer.valueOf(4), Integer.valueOf(5));

        final ReviewScoreAndCount expectedReviewScoreAndCount = new ReviewScoreAndCount(3, 5);
        assertEquals(Formulas.reviewScoreAndCountCalculate(reviewScoreList), expectedReviewScoreAndCount);
    }

    /**
     * Tests the method for calculating the review score and count when there are
     * null Integers.
     */
    @SuppressWarnings("static-method")
    @Test
    void reviewScoreAndCountCalculateWithNullTest() {
        final List<Integer> reviewScoreList = List
            .of(null, Integer.valueOf(1), Integer.valueOf(2), null, Integer.valueOf(3), Integer.valueOf(4),
                Integer.valueOf(5));

        final ReviewScoreAndCount expectedReviewScoreAndCount = new ReviewScoreAndCount(3, 5);
        assertEquals(Formulas.reviewScoreAndCountCalculate(reviewScoreList), expectedReviewScoreAndCount);
    }

    /**
     * Tests the method for calculating the review score and count when there are
     * null Integers and score returns a Real number.
     */
    @SuppressWarnings("static-method")
    @Test
    void reviewScoreAndCountCalculateReturnsRealNumberTest() {
        final List<Integer> reviewScoreList = List
            .of(null, Integer.valueOf(4), Integer.valueOf(2), null, Integer.valueOf(4));

        final ReviewScoreAndCount expectedReviewScoreAndCount = new ReviewScoreAndCount(3.33, 3);
        assertEquals(Formulas.reviewScoreAndCountCalculate(reviewScoreList), expectedReviewScoreAndCount);
    }

    /**
     * Tests the method for calculating the review score and count when there are no
     * reviews.
     */
    @SuppressWarnings("static-method")
    @Test
    void reviewScoreAndCountCalculateEmptyTest() {
        final ReviewScoreAndCount expectedReviewScoreAndCount = new ReviewScoreAndCount(0, 0);
        assertEquals(Formulas.reviewScoreAndCountCalculate(new ArrayList<>()), expectedReviewScoreAndCount);
    }

    /**
     * Testing method for {@link Formulas#calculateDiscountedPrice(double, double)
     * calculateDiscountedPrice}.
     *
     * @param grossPrice
     *            The total price. Must be {@code >= 0}!
     * @param discount
     *            The discount percentage. Must be {@code >= 0 && <= 100}.
     * @param expectedDiscountedPrice
     *            The expected discounted price.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("calculatePriceTestData")
    void calculateDiscountedPriceTest(final double grossPrice, final double discount,
                                      final double expectedDiscountedPrice) {
        final double actualDiscountPrice = Formulas.calculateDiscountedPrice(grossPrice, discount);

        Assert.assertEquals(actualDiscountPrice, expectedDiscountedPrice);
    }

    @SuppressWarnings("boxing")
    private static Stream<Arguments> calculatePriceTestData() {
        return Stream
            .of(arguments(0, 0, 0), arguments(0, 20, 0), arguments(100, 25.50, 74.50), arguments(100, 100, 0),
                arguments(100, 0, 100), arguments(Integer.MAX_VALUE, 30, 1_503_238_552.90));
    }

}
