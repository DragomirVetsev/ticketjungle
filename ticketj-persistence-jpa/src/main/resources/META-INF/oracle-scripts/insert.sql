INSERT INTO PERFORMER (performer_name, description)
VALUES
  ('Grafa', 'The bulgarian singer');

INSERT INTO PERFORMER (performer_name, description)
VALUES
  ('Teatralna grupa', 'bulgarian teather group');

INSERT INTO PERFORMER (performer_name, description)
VALUES
  ('Rolling Stones', 'The best band of the World');
  
  INSERT INTO PERFORMER (performer_name, description)
VALUES
  ('Imagine Dragons', 'The American pop rock band based in Las Vegas, Nevada, consisting of lead singer Dan Reynolds, guitarist Wayne Sermon, bassist Ben McKee and drummer Daniel Platzman');

  INSERT INTO PERFORMER (performer_name, description)
VALUES
  ('Лили Иванова', 'Примата на българската естрада“. Обявена е за "Най-големия български поп изпълнител за всички времена" със специална награда от БГ радио през 2023 г.');
  
   INSERT INTO PERFORMER (performer_name, description)
VALUES
  ('СИГНАЛ', 'Българската рок група, създадена в София през юни 1978 година от Йордан Караджов, Христо Ламбрев, Румен Спасов и Георги Кокаланов.');
  
     INSERT INTO PERFORMER (performer_name, description)
VALUES
  ('Леонид Йовчев', 'Завършва НАТФИЗ „Кр. Сарафов" през 2007 г. „актьорско майсторство за драматичен театър" в класа на проф. Димитрина Гюров.');
  
     INSERT INTO PERFORMER (performer_name, description)
VALUES
  ('Мариус Куркински', 'Български театрален и кино актьор, режисьор и музикален изпълнител, особено известен с монологичните си театрални постановки');
  
INSERT INTO PHONE_CODE (phone_code)
VALUES
  ('+359');

INSERT INTO PHONE_CODE (phone_code)
VALUES
  ('+40');
  
INSERT INTO CITY (city_name)
VALUES
  ('Sofia');
  
INSERT INTO CITY (city_name)
VALUES
  ('Plovdiv');
  
INSERT INTO CITY (city_name)
VALUES
  ('Burgas');
  
INSERT INTO CITY (city_name)
VALUES
  ('Varna');
  
INSERT INTO CITY (city_name)
VALUES
  ('Montana');
  
INSERT INTO CITY (city_name)
VALUES
  ('Stara Zagora');

INSERT INTO CITY (city_name)
VALUES
  ('Veliko Turnovo');

INSERT INTO OFFICE (office_name, address_street, address_city, address_post_code, phone_code, phone_number, email, opening_time, closing_time)
VALUES
  ('TicketJ 100', 'ul. Todor Kableshkov, 8', 'Sofia', '1000', '+359', 884563489, 'office_100@ticketj.com', '09:00', '18:00');

INSERT INTO OFFICE (office_name, address_street, address_city, address_post_code, phone_code, phone_number, email, opening_time, closing_time)
VALUES
  ('TicketJ 105', 'ul. General Gurko', 'Plovdiv', '4000', '+359', 897645310, 'office_105@ticketj.com', '09:00', '18:00');

INSERT INTO OFFICE (office_name, address_street, address_city, address_post_code, phone_code, phone_number, email, opening_time, closing_time)
VALUES
  ('TicketJ 34', 'ул. Поп Харитон', 'Montana', '3400', '+359', 877124502, 'office_34@ticketj.com', '09:00', '18:00');

INSERT INTO OFFICE (office_name, address_street, address_city, address_post_code, phone_code, phone_number, email, opening_time, closing_time)
VALUES
  ('TicketJ 33', 'ул. Мария Луиза', 'Varna', '9000', '+359', 895348693, 'office_33@ticketj.com', '09:00', '18:00');
  
INSERT INTO OFFICE (office_name, address_street, address_city, address_post_code, phone_code, phone_number, email, opening_time, closing_time)
VALUES
  ('TicketJ 83', 'ul. Antim I, 30', 'Burgas', '8000', '+359', 888234567, 'office_83@ticketj.com', '09:00', '18:00');  
  
INSERT INTO OFFICE (office_name, address_street, address_city, address_post_code, phone_code, phone_number, email, opening_time, closing_time)
VALUES
  ('TicketJ 22', 'ул. Георги Райчев', 'Stara Zagora', '6004', '+359', 897123475, 'office_22@ticketj.com', '09:00', '18:00');
  
INSERT INTO OFFICE (office_name, address_street, address_city, address_post_code, phone_code, phone_number, email, opening_time, closing_time)
VALUES
  ('TicketJ 111', 'ul. Geo Milev', 'Sofia', '1000', '+359', 898125279, 'office_111@ticketj.com', '09:00', '18:00');

INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('NDK', 'Ploshtad Bulgaria 1', 'Sofia', '1000');

INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Teatar Bulgarska Armia', 'ulitsa Rakovski 98', 'Sofia', '1000');

INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Zala Universiada', 'ulitsa Shipka 10', 'Plovdiv', '2100');

INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Teatar Morska Gradina', 'ulitsa Vasil Levski 21', 'Burgas', '3100');

INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Stadion Vasil Levski', 'blvd. Levski 100', 'Sofia', '1000');

INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Zala 1', 'NDK');

INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Zala 2', 'NDK');

INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Zala 3', 'NDK');

INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Zala 1', 'Teatar Bulgarska Armia');

INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Summer Stage', 'Teatar Morska Gradina');

INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Stadion', 'Stadion Vasil Levski');
  
INSERT INTO SECTOR (sector_name, venue_name, stage_name)
VALUES 
  ('Sector 1', 'NDK', 'Zala 2');

INSERT INTO SECTOR (sector_name, venue_name, stage_name)
VALUES 
  ('Sector 2', 'NDK', 'Zala 2');

INSERT INTO SECTOR (sector_name, venue_name, stage_name)
VALUES 
  ('Sector 3', 'NDK', 'Zala 2');
 
INSERT INTO LINE (line_number, venue_name, stage_name, sector_name)
VALUES (1, 'NDK', 'Zala 2', 'Sector 1');

INSERT INTO LINE (line_number, venue_name, stage_name, sector_name)
VALUES (2, 'NDK', 'Zala 2', 'Sector 1');

INSERT INTO LINE (line_number, venue_name, stage_name, sector_name)
VALUES (3, 'NDK', 'Zala 2', 'Sector 1');

INSERT INTO SEAT (seat_number, venue_name, stage_name, sector_name, line_number)
VALUES (1, 'NDK', 'Zala 2', 'Sector 1', 1);

INSERT INTO SEAT (seat_number, venue_name, stage_name, sector_name, line_number)
VALUES (2, 'NDK', 'Zala 2', 'Sector 1', 1);

INSERT INTO SEAT (seat_number, venue_name, stage_name, sector_name, line_number)
VALUES (3, 'NDK', 'Zala 2', 'Sector 1', 1);

INSERT INTO SEAT (seat_number, venue_name, stage_name, sector_name, line_number)
VALUES (4, 'NDK', 'Zala 2', 'Sector 1', 2);

INSERT INTO SEAT (seat_number, venue_name, stage_name, sector_name, line_number)
VALUES (5, 'NDK', 'Zala 2', 'Sector 1', 2);

INSERT INTO SEAT (seat_number, venue_name, stage_name, sector_name, line_number)
VALUES (6, 'NDK', 'Zala 2', 'Sector 1', 2);

INSERT INTO SEAT (seat_number, venue_name, stage_name, sector_name, line_number)
VALUES (7, 'NDK', 'Zala 2', 'Sector 1', 3);

INSERT INTO SEAT (seat_number, venue_name, stage_name, sector_name, line_number)
VALUES (8, 'NDK', 'Zala 2', 'Sector 1', 3);

INSERT INTO SEAT (seat_number, venue_name, stage_name, sector_name, line_number)
VALUES (9, 'NDK', 'Zala 2', 'Sector 1', 3);

------------------------------NDK Zala 1----------------------------------------
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('NDK', 'Zala 1', 'Sector 1');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 1', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 1, 5);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 1', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 2, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 2, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 2, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 2, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 1', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 3, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 3, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 3, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 3, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 3, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 1', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 4, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 4, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 4, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 4, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 4, 20);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 1', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 5, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 5, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 5, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 5, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 1', 5, 25);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('NDK', 'Zala 1', 'Sector 2');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 2', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 1, 26);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 1, 27);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 1, 28);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 1, 29);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 1, 30);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 2', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 2, 31);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 2, 32);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 2, 33);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 2, 34);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 2, 35);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 2', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 3, 36);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 3, 37);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 3, 38);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 3, 39);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 3, 40);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 2', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 4, 41);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 4, 42);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 4, 43);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 4, 44);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 4, 45);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 2', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 5, 46);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 5, 47);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 5, 48);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 5, 49);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 2', 5, 50);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('NDK', 'Zala 1', 'Sector 3');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 3', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 1, 51);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 1, 52);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 1, 53);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 1, 54);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 1, 55);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 3', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 2, 56);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 2, 57);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 2, 58);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 2, 59);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 2, 60);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 3', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 3, 61);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 3, 62);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 3, 63);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 3, 64);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 3, 65);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 3', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 4, 66);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 4, 67);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 4, 68);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 4, 69);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 4, 70);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 3', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 5, 71);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 5, 72);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 5, 73);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 5, 74);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 3', 5, 75);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('NDK', 'Zala 1', 'Sector 4');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 4', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 1, 76);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 1, 77);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 1, 78);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 1, 79);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 1, 80);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 4', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 2, 81);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 2, 82);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 2, 83);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 2, 84);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 2, 85);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 4', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 3, 86);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 3, 87);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 3, 88);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 3, 89);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 3, 90);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 4', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 4, 91);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 4, 92);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 4, 93);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 4, 94);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 4, 95);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 1', 'Sector 4', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 5, 96);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 5, 97);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 5, 98);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 5, 99);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 1','Sector 4', 5, 100);


-----------------------NDK Zala 3--------------------------------------
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('NDK', 'Zala 3', 'Sector 1');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 1', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 1, 5);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 1', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 2, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 2, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 2, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 2, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 1', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 3, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 3, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 3, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 3, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 3, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 1', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 4, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 4, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 4, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 4, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 1', 4, 20);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('NDK', 'Zala 3', 'Sector 2');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 2', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 1, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 1, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 1, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 1, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 1, 25);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 2', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 2, 26);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 2, 27);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 2, 28);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 2, 29);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 2, 30);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 2', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 3, 31);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 3, 32);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 3, 33);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 3, 34);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 3, 35);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 2', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 4, 36);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 4, 37);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 4, 38);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 4, 39);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 2', 4, 40);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('NDK', 'Zala 3', 'Sector 3');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 3', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 1, 41);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 1, 42);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 1, 43);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 1, 44);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 1, 45);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 3', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 2, 46);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 2, 47);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 2, 48);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 2, 49);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 2, 50);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 3', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 3, 51);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 3, 52);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 3, 53);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 3, 54);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 3, 55);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 3', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 4, 56);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 4, 57);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 4, 58);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 4, 59);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 3', 4, 60);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('NDK', 'Zala 3', 'Sector 4');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 4', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 1, 61);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 1, 62);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 1, 63);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 1, 64);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 1, 65);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 4', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 2, 66);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 2, 67);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 2, 68);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 2, 69);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 2, 70);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 4', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 3, 71);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 3, 72);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 3, 73);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 3, 74);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 3, 75);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 4', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 4, 76);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 4, 77);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 4, 78);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 4, 79);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 4', 4, 80);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('NDK', 'Zala 3', 'Sector 5');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 5', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 1, 81);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 1, 82);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 1, 83);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 1, 84);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 1, 85);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 5', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 2, 86);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 2, 87);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 2, 88);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 2, 89);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 2, 90);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 5', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 3, 91);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 3, 92);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 3, 93);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 3, 94);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 3, 95);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('NDK', 'Zala 3', 'Sector 5', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 4, 96);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 4, 97);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 4, 98);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 4, 99);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('NDK', 'Zala 3','Sector 5', 4, 100);

------------------------Драматичен театър - Стоян Бъчваров---------------------------------
INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Драматичен театър - Стоян Бъчваров', 'пл. „Независимост“ 1, Център', 'Varna', '9000');
    
INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Главна зала', 'Драматичен театър - Стоян Бъчваров');
    
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 1');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 1', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 1, 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 1, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 1, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 1, 8);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 1', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 2, 10);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 2, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 2, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 2, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 2, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 2, 15);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 2, 16);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 1', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 3, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 3, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 3, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 3, 20);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 3, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 3, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 3, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 3, 24);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 1', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 4, 25);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 4, 26);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 4, 27);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 4, 28);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 4, 29);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 4, 30);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 4, 31);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 4, 32);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 1', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 5, 33);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 5, 34);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 5, 35);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 5, 36);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 5, 37);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 5, 38);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 5, 39);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 5, 40);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 1', 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 6, 41);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 6, 42);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 6, 43);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 6, 44);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 6, 45);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 6, 46);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 6, 47);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 1', 6, 48);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 2');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 2', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 1, 49);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 1, 50);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 1, 51);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 1, 52);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 1, 53);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 2', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 2, 54);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 2, 55);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 2, 56);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 2, 57);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 2, 58);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 2', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 3, 59);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 3, 60);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 3, 61);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 3, 62);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 2', 3, 63);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 3');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 3', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 1, 64);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 1, 65);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 1, 66);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 1, 67);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 1, 68);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 3', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 2, 69);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 2, 70);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 2, 71);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 2, 72);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 2, 73);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 3', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 3, 74);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 3, 75);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 3, 76);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 3, 77);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 3', 3, 78);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 4');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 4', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 1, 79);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 1, 80);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 1, 81);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 1, 82);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 1, 83);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 1, 84);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 4', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 2, 85);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 2, 86);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 2, 87);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 2, 88);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 2, 89);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 2, 90);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 4', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 3, 91);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 3, 92);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 3, 93);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 3, 94);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 3, 95);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 4', 3, 96);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 5');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 5', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 1, 97);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 1, 98);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 1, 99);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 1, 100);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 1, 101);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 5', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 2, 102);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 2, 103);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 2, 104);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 2, 105);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 2, 106);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 5', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 3, 107);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 3, 108);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 3, 109);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 3, 110);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 5', 3, 111);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 6');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 6', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 1, 112);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 1, 113);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 1, 114);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 1, 115);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 1, 116);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 6', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 2, 117);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 2, 118);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 2, 119);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 2, 120);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 2, 121);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала', 'Сектор 6', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 3, 122);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 3, 123);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 3, 124);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 3, 125);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Драматичен театър - Стоян Бъчваров', 'Главна зала','Сектор 6', 3, 126);

--------------------------Народен театър Иван Вазов ---------------------------
INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Народен театър "Иван Вазов"', 'ул. „Дякон Игнатий“ №5', 'Sofia', '1000');
    
INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Голяма сцена', 'Народен театър "Иван Вазов"');

INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 1, 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 1, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 1, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 1, 8);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 2, 10);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 2, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 2, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 2, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 2, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 2, 15);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 2, 16);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 3, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 3, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 3, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 3, 20);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 3, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 3, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 3, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 3, 24);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 4, 25);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 4, 26);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 4, 27);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 4, 28);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 4, 29);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 4, 30);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 4, 31);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 4, 32);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 5, 33);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 5, 34);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 5, 35);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 5, 36);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 5, 37);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 5, 38);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 5, 39);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 5, 40);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1', 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 6, 41);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 6, 42);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 6, 43);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 6, 44);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 6, 45);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 6, 46);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 6, 47);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 6, 48);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1', 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 7, 49);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 7, 50);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 7, 51);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 7, 52);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 7, 53);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 7, 54);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 7, 55);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 7, 56);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1', 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 8, 57);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 8, 58);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 8, 59);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 8, 60);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 8, 61);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 8, 62);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 8, 63);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 8, 64);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 1', 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 9, 65);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 9, 66);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 9, 67);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 9, 68);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 9, 69);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 9, 70);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 9, 71);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 1', 9, 72);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 2');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 2', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 1, 73);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 1, 74);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 1, 75);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 1, 76);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 1, 77);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 2', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 2, 78);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 2, 79);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 2, 80);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 2, 81);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 2, 82);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 2', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 3, 83);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 3, 84);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 3, 85);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 3, 86);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 2', 3, 87);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 3');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 3', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 1, 88);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 1, 89);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 1, 90);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 1, 91);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 1, 92);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 1, 93);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 3', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 2, 94);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 2, 95);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 2, 96);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 2, 97);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 2, 98);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 2, 99);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 3', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 3, 100);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 3, 101);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 3, 102);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 3, 103);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 3, 104);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 3', 3, 105);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 4');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 4', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 1, 106);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 1, 107);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 1, 108);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 1, 109);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 1, 110);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 4', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 2, 111);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 2, 112);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 2, 113);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 2, 114);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 2, 115);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 4', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 3, 116);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 3, 117);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 3, 118);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 3, 119);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 4', 3, 120);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 5');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 5', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 1, 121);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 1, 122);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 1, 123);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 1, 124);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 5', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 2, 125);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 2, 126);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 2, 127);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 2, 128);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 5', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 3, 129);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 3, 130);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 3, 131);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 5', 3, 132);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 6');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 6', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 1, 133);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 1, 134);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 1, 135);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 1, 136);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 1, 137);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 6', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 2, 138);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 2, 139);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 2, 140);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 2, 141);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 2, 142);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 6', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 3, 143);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 3, 144);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 3, 145);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 3, 146);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 6', 3, 147);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 7');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 7', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 1, 148);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 1, 149);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 1, 150);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 1, 151);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 7', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 2, 152);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 2, 153);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 2, 154);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 2, 155);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена', 'Сектор 7', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 3, 156);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 3, 157);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 3, 158);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Народен театър "Иван Вазов"', 'Голяма сцена','Сектор 7', 3, 159);

----------------------------Летен театър - Бургас----------------------
INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Летен театър', 'бул. Демокрация 94, Бургас Център', 'Burgas', '8000');
    
INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Сцена', 'Летен театър');
  
  INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Летен театър', 'Сцена', 'Сектор 1');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 1', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 1, 5);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 1', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 2, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 2, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 2, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 2, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 1', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 3, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 3, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 3, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 3, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 3, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 1', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 4, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 4, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 4, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 4, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 4, 20);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 1', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 5, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 5, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 5, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 5, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 5, 25);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 1', 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 6, 26);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 6, 27);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 6, 28);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 6, 29);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 6, 30);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 1', 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 7, 31);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 7, 32);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 7, 33);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 7, 34);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 7, 35);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 1', 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 8, 36);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 8, 37);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 8, 38);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 8, 39);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 1', 8, 40);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Летен театър', 'Сцена', 'Сектор 2');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 2', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 1, 5);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 2', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 2, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 2, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 2, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 2, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 2', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 3, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 3, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 3, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 3, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 3, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 2', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 4, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 4, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 4, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 4, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 4, 20);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 2', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 5, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 5, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 5, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 5, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 5, 25);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 2', 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 6, 26);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 6, 27);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 6, 28);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 6, 29);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 6, 30);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 2', 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 7, 31);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 7, 32);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 7, 33);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 7, 34);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 7, 35);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 2', 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 8, 36);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 8, 37);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 8, 38);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 8, 39);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 2', 8, 40);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Летен театър', 'Сцена', 'Сектор 3');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 3', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 1, 5);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 3', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 2, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 2, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 2, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 2, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 3', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 3, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 3, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 3, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 3, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 3, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 3', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 4, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 4, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 4, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 4, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 4, 20);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 3', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 5, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 5, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 5, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 5, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 5, 25);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 3', 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 6, 26);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 6, 27);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 6, 28);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 6, 29);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 6, 30);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 3', 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 7, 31);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 7, 32);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 7, 33);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 7, 34);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 7, 35);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Летен театър', 'Сцена', 'Сектор 3', 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 8, 36);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 8, 37);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 8, 38);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 8, 39);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Летен театър', 'Сцена','Сектор 3', 8, 40);


--------------------------Дом на киното - София-----------------------------
INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Дом на киното', 'ул. Екзарх Йосиф 37', 'Sofia', '1000');
    
INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Прожекционна зала', 'Дом на киното');
  

INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Дом на киното', 'Прожекционна зала', 'Основен сектор');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Дом на киното', 'Прожекционна зала', 'Основен сектор', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 1, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Дом на киното', 'Прожекционна зала', 'Основен сектор', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 15);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 2, 20);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Дом на киното', 'Прожекционна зала', 'Основен сектор', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 25);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 26);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 27);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 28);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 29);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 3, 30);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Дом на киното', 'Прожекционна зала', 'Основен сектор', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 31);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 32);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 33);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 34);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 35);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 36);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 37);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 38);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 39);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 4, 40);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Дом на киното', 'Прожекционна зала', 'Основен сектор', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 41);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 42);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 43);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 44);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 45);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 46);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 47);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 48);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 49);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 5, 50);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Дом на киното', 'Прожекционна зала', 'Основен сектор', 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 51);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 52);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 53);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 54);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 55);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 56);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 57);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 58);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 59);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Дом на киното', 'Прожекционна зала','Основен сектор', 6, 60);

----------------------Фестивален и конгресен център-----------------------------------
INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Фестивален и конгресен център', 'Бул. "Сливница" № 2', 'Varna', '9000');
    
INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Зала 1', 'Фестивален и конгресен център');
  
  INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Фестивален и конгресен център', 'Зала 1', 'Основен сектор');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Фестивален и конгресен център', 'Зала 1', 'Основен сектор', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 10);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 1, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Фестивален и конгресен център', 'Зала 1', 'Основен сектор', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 20);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 25);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 26);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 27);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 28);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 29);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 2, 30);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Фестивален и конгресен център', 'Зала 1', 'Основен сектор', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 31);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 32);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 33);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 34);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 35);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 36);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 37);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 38);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 39);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 40);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 41);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 42);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 43);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 44);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 3, 45);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Фестивален и конгресен център', 'Зала 1', 'Основен сектор', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 46);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 47);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 48);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 49);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 50);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 51);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 52);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 53);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 54);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 55);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 56);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 57);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 58);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 59);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 4, 60);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Фестивален и конгресен център', 'Зала 1', 'Основен сектор', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 61);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 62);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 63);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 64);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 65);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 66);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 67);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 68);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 69);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 70);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 71);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 72);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 73);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 74);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 5, 75);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Фестивален и конгресен център', 'Зала 1', 'Основен сектор', 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 76);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 77);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 78);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 79);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 80);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 81);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 82);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 83);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 84);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 85);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 86);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 87);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 88);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 89);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 6, 90);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Фестивален и конгресен център', 'Зала 1', 'Основен сектор', 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 91);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 92);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 93);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 94);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 95);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 96);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 97);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 98);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 99);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 100);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 101);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 102);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 103);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 104);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Фестивален и конгресен център', 'Зала 1','Основен сектор', 7, 105);

--------------------------Античен театър - Пловдив----------------------------------
INSERT INTO VENUE (venue_name, address_street, address_city, address_post_code)
VALUES
    ('Античен театър', 'ул. Цар Ивайло 4', 'Plovdiv', '4000');
    
INSERT INTO STAGE (stage_name, venue_name)
VALUES
  ('Сцена', 'Античен театър');
  
  INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Античен театър', 'Сцена', 'Сектор 1');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 1', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 1, 5);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 1', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 2, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 2, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 2, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 2, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 1', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 3, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 3, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 3, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 3, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 3, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 1', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 4, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 4, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 4, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 4, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 4, 20);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 1', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 5, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 5, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 5, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 5, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 1', 5, 25);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Античен театър', 'Сцена', 'Сектор 2');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 2', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 1, 5);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 2', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 2, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 2, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 2, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 2, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 2', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 3, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 3, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 3, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 3, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 3, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 2', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 4, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 4, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 4, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 4, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 4, 20);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 2', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 5, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 5, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 5, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 5, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 2', 5, 25);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Античен театър', 'Сцена', 'Сектор 3');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 3', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 1, 5);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 3', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 2, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 2, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 2, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 2, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 3', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 3, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 3, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 3, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 3, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 3, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 3', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 4, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 4, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 4, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 4, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 4, 20);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 3', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 5, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 5, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 5, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 5, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 3', 5, 25);
INSERT INTO SECTOR(venue_name, stage_name, sector_name)
VALUES('Античен театър', 'Сцена', 'Сектор 4');
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 4', 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 1, 1);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 1, 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 1, 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 1, 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 1, 5);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 4', 2);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 2, 6);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 2, 7);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 2, 8);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 2, 9);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 2, 10);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 4', 3);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 3, 11);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 3, 12);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 3, 13);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 3, 14);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 3, 15);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 4', 4);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 4, 16);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 4, 17);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 4, 18);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 4, 19);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 4, 20);
INSERT INTO LINE(venue_name, stage_name, sector_name, line_number)
VALUES('Античен театър', 'Сцена', 'Сектор 4', 5);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 5, 21);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 5, 22);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 5, 23);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 5, 24);
INSERT INTO SEAT(venue_name, stage_name, sector_name, line_number, seat_number)
VALUES('Античен театър', 'Сцена','Сектор 4', 5, 25);

INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('aIvanov', 'Anton', 'Ivanov', 'ivanoc@dxc.com', '87878787', '877380303', '+359', 'ul. Knyaz Boris 1', 'Varna', '9000', 'OFFICE_WORKER');
  
  INSERT INTO OFFICE_WORKER (username, office_name)
  VALUES('aIvanov', null);
  
  INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('ticketHunter', 'George', 'Stefanov', 'Stefanov@dxc.com', '8888888', '987654322' , '+359', 'ul. Stefan Dimitrov 7', 'Sofia', '1000', 'CUSTOMER');
  
  INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('bobo', 'Boris', 'Spaski', 'Spaski@dxc.com', '111111' ,'987654321', '+40', 'ul. Aleko Konstantinov', 'Plovdiv', '4000', 'CUSTOMER');
  
  INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('giliev', 'Georgi', 'Iliev', 'georgi.iliev3@dxc.com', '011235', '887565543', '+40', 'ul. Boris Drangov 32', 'Burgas', '8000', 'CUSTOMER');
  
  INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('Алиса', 'Ани', 'Ангелова', 'ani.angel@abv.bg', '123456789', '889565542', '+359', 'ул. Чайка 17', 'Варна', '9000', 'CUSTOMER');
  
INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('spongebob', 'Boris', 'Borisov', 'boris123.@abv.bg', '123456789', '899565542', '+359', 'Rila str. 28', 'Sofia', '1000', 'CUSTOMER');
  
INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('хитър-петър', 'Петър', 'Здравков', 'zdravkov90.@abv.bg', '123456789', '899445544', '+359', 'G.Rakovski str. 35', 'Sofia', '1000', 'CUSTOMER');
  
  INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('Mario', 'Marina', 'Petrova', 'marina90.@abv.bg', '123456789', '899565542', '+359', 'Stambolijski str. 2', 'Сmolyan', '4700', 'OFFICE_WORKER');

  INSERT INTO OFFICE_WORKER (username, office_name)
  VALUES('Mario', 'TicketJ 100');
  
    INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('Neo', 'Ivan', 'Ivanov', 'ivan.ivanov.@dxc.com', '123456789', '898989898', '+359', 'Teatralna str. 15', 'Plovdiv', '4000', 'ADMIN');
  
INSERT INTO SYSTEM_ADMIN (admin_username)
VALUES ('Neo');

  INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('user123', 'Мария', 'Иванова', 'maria.ivanova@example.bg', '987654321', '878123456', '+359', 'ул. Пирин 10', 'Благоевград', '2700', 'CUSTOMER');

INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('john.doe', 'Иван', 'Петров', 'john.doe@example.bg', '123456789', '888111222', '+359', 'бул. Витоша 1', 'София', '1000', 'CUSTOMER');

  INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('jane_doe', 'Иванa', 'Петровa', 'jane.doe@example.bg', '165456789', '888115642', '+359', 'бул. Витоша 1', 'София', '1000', 'OFFICE_WORKER');

  INSERT INTO OFFICE_WORKER (username, office_name)
  VALUES('jane_doe', null);
  
INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('sa60', 'Александър', 'Стоянов', 'bobi87@example.bg', '987654321', '889222333', '+359', 'бул. 6-ти септември 15', 'Пловдив', '4000', 'CUSTOMER');

  INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('sandov', 'Александър', 'Иванов', 'sandov@example.bg', '987004321', '889227630', '+359', 'бул. 5-ти октомври 9', 'Пазарджик', '4400', 'OFFICE_WORKER');
  
     INSERT INTO OFFICE_WORKER (username, office_name)
  VALUES('sandov', null); 
  
INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('laura', 'Лаура', 'Маринова', 'laura@example.bg', '123456789', '878333444', '+359', 'ул. Княз Борис 12', 'Варна', '9000', 'CUSTOMER');

  INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('renter', 'Иван', 'Петканов', 'renter@example.bg', '186456789', '878333444', '+359', 'ул. 876', 'Варна', '9000', 'OFFICE_WORKER');

   INSERT INTO OFFICE_WORKER (username, office_name)
  VALUES('renter', 'TicketJ 105'); 
  
INSERT INTO SYSTEM_USER (username, first_name, last_name, email, password, phone_number, phone_code, address_street, address_city, address_post_code, role)
VALUES
  ('peter.pan', 'Петър', 'Ангелов', 'peter.pan@example.bg', '987654321', '888444555', '+359', 'ул. Александровска 8', 'Бургас', '8000', 'CUSTOMER');

  INSERT INTO CUSTOMER (username, birthday)
VALUES
  ('user123', TO_DATE('1980-05-15', 'YYYY-MM-DD'));
  
INSERT INTO CUSTOMER (username, birthday)
VALUES
  ('john.doe', TO_DATE('1985-09-30', 'YYYY-MM-DD'));
  
INSERT INTO CUSTOMER (username, birthday)
VALUES
  ('sa60', TO_DATE('1990-02-20', 'YYYY-MM-DD'));

INSERT INTO CUSTOMER (username, birthday)
VALUES
  ('laura', TO_DATE('1982-11-10', 'YYYY-MM-DD'));

INSERT INTO CUSTOMER (username, birthday)
VALUES
  ('peter.pan', TO_DATE('1988-07-25', 'YYYY-MM-DD'));
  
INSERT INTO CUSTOMER (username, birthday)
VALUES
  ('Алиса', TO_DATE('1995-03-21', 'YYYY-MM-DD'));
  
INSERT INTO CUSTOMER (username, birthday)
VALUES
  ('spongebob' , TO_DATE('2000-12-04', 'YYYY-MM-DD'));
  
INSERT INTO CUSTOMER (username, birthday)
VALUES
  ('хитър-петър', null);
  
  INSERT INTO CUSTOMER (username, birthday)
VALUES ('bobo', TO_DATE('1945-01-15', 'YYYY-MM-DD'));

INSERT INTO CUSTOMER (username, birthday)
VALUES ('giliev', TO_DATE('1995-05-20', 'YYYY-MM-DD'));

INSERT INTO CUSTOMER (username, birthday)
VALUES ('ticketHunter', null);

  INSERT INTO DISCOUNT (name, percentage)
VALUES ('NONE', 0.00);

  INSERT INTO DISCOUNT (name, percentage)
VALUES ('KID', 20.00);

  INSERT INTO DISCOUNT (name, percentage)
VALUES ('STUDENT', 10.00);

  INSERT INTO DISCOUNT (name, percentage)
VALUES ('ELDERLY', 15.00);

INSERT INTO GENRE(genre_type)
VALUES ('Rock');

INSERT INTO GENRE(genre_type)
VALUES ('Pop');

INSERT INTO GENRE(genre_type)
VALUES ('Theater');

INSERT INTO GENRE(genre_type)
VALUES ('Tragedy');

INSERT INTO GENRE(genre_type)
VALUES ('Tehno');


--------- EVENTS ---------------------
INSERT INTO EVENT (name, description, genre_type)
  VALUES ('GRAFA TOUR', 'The new tour of Grafa 2023', 'Pop');
  
INSERT INTO EVENT (name, description, genre_type)
  VALUES ('Leep Year Event', 'Leap year event 2020', 'Pop');

INSERT INTO EVENT (name, description, genre_type)
  VALUES ('ROLLING STONES TOUR', 'WORLD TOUR SATISFACTION', 'Rock');
  
INSERT INTO EVENT (name, description, genre_type)
  VALUES ('Mercury World Tour', 'The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.', 'Pop');
    
INSERT INTO EVENT (name, description, genre_type)
  VALUES ('Burgas Fest', 'Only the best now in Burgas', 'Pop'); 

INSERT INTO EVENT (name, description, genre_type)
  VALUES ('Театрални вечери във Варна', 'Заповядайте на забравими театрални вечери в град Варна', 'Theater');

INSERT INTO EVENT (name, description, genre_type)
  VALUES ('Cool den - tour', 'Cool den - around Bulgaria', 'Rock');

INSERT INTO EVENT (name, description, genre_type)
  VALUES ('Турнето на Лили Иванова', 'Коледния подарък на Лили за публиката и!', 'Pop');
  
INSERT INTO EVENT (name, description, genre_type)
  VALUES ('Хамлет в Народния Театър', 'Да бъдем или да не бъдем, това се пита', 'Tragedy');

INSERT INTO EVENT (name, description, genre_type)
  VALUES ('СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ', 'Юбилейни концерти на група Сигнал за 45-та годишнина', 'Rock');
  
INSERT INTO EVENT (name, description, genre_type)
  VALUES ('UTRELAND', 'Waiting for everything', 'Tehno');

--------- SUB-EVENTS ---------------------
  
--------- Leep Year Event ----------------
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Leap year sub event', 'Leep Year Event', 'Дом на киното', 'Прожекционна зала',
          'Every 4 years their is a leap year', 20.00,  TO_DATE('2020-02-29 22:00:00', 'YYYY-MM-DD HH24:MI:SS'));

--------- Cool den - tour ----------------  
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('София- Дом на киното - In The Palace', 'Cool den - tour', 'Дом на киното', 'Прожекционна зала',
          'Започва от месния ни град София в най- доброто мясно, чакаме ви там!', 20.00, trunc(SYSDATE)+30 + INTERVAL '22:00' HOUR TO MINUTE);  

INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Бургас- Greenburg fest-/Papa Beer/', 'Cool den - tour', 'Драматичен театър - Стоян Бъчваров','Главна зала', 
          'Здравей приятели от топъл Бургас! Заповядайте на нашия септеврийски концерт!', 20.00, trunc(SYSDATE)+36 + INTERVAL '22:00' HOUR TO MINUTE);

--------- Театрални вечери във Варна ----------------
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Оперета', 'Театрални вечери във Варна', 'Драматичен театър - Стоян Бъчваров', 'Главна зала',
          'Опера с гости от чужбина и учсници от децка градина Юнак', 20.00, trunc(SYSDATE)-43 + INTERVAL '20:00' HOUR TO MINUTE);

INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Ромео и Жулиета', 'Театрални вечери във Варна', 'Драматичен театър - Стоян Бъчваров', 'Главна зала',
          'Изпълненеи от трупа 107 София', 30.00, trunc(SYSDATE)-42 + INTERVAL '20:00' HOUR TO MINUTE);

INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Само за жени', 'Театрални вечери във Варна', 'Драматичен театър - Стоян Бъчваров', 'Главна зала',
          'Премиера за варненските фенове на бългаския театър', 20.00, trunc(SYSDATE)-41 + INTERVAL '20:00' HOUR TO MINUTE);

INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Под игото', 'Театрални вечери във Варна', 'Фестивален и конгресен център', 'Зала 1',
          'Невороятната трупа Пенчо Славейко ще изпълняват за повторен път във Варна.', 20.00, trunc(SYSDATE)-40 + INTERVAL '20:00' HOUR TO MINUTE);

INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Красавицата и Звяра', 'Театрални вечери във Варна', 'Фестивален и конгресен център', 'Зала 1',
          'За млади и стари месната трупа Васил Левски ще изпулнява чудесна приказка от Шарл Перо', 10.00, trunc(SYSDATE)-39 + INTERVAL '20:00' HOUR TO MINUTE);

--------- Burgas Fest ----------------
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Frenz ferdinad', 'Burgas Fest', 'Летен театър', 'Сцена',
          'Noting but the thieves hayes & y ', 40.00, trunc(SYSDATE)-19 + INTERVAL '20:00' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Years&Years', 'Burgas Fest', 'Летен театър', 'Сцена',
          'Osker and the worlf', 40.00, trunc(SYSDATE)-12 + INTERVAL '20:00' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Rosin Merphy', 'Burgas Fest', 'Летен театър', 'Сцена',
          'Phuture rock', 40.00, trunc(SYSDATE)-5 + INTERVAL '20:00' HOUR TO MINUTE);

---- ГРАФА ----         
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Графа в София - 1', 'GRAFA TOUR', 'NDK', 'Zala 3',
          'Най-мащабното събитие в кариерата на българския топ изпълнител. София готова ли си?', 70.00, trunc(SYSDATE)-26 + INTERVAL '19:30' HOUR TO MINUTE);

INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Графа в София - 2', 'GRAFA TOUR', 'NDK', 'Zala 3',
          'Най-мащабното събитие в кариерата на българския топ изпълнител. София готова ли си?', 70.00, trunc(SYSDATE)-25 + INTERVAL '19:30' HOUR TO MINUTE);
  
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Графа в София - 3', 'GRAFA TOUR', 'NDK', 'Zala 1',
          'Най-мащабното събитие в кариерата на българския топ изпълнител. София готова ли си?', 70.00, trunc(SYSDATE)+39 + INTERVAL '19:30' HOUR TO MINUTE);
  
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Графа в Пловдив', 'GRAFA TOUR', 'Античен театър', 'Сцена',
          'Най-мащабното събитие в кариерата на българския топ изпълнител. Пловдив готов ли си?', 60.00, trunc(SYSDATE)+41 + INTERVAL '19:30' HOUR TO MINUTE);
  
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Графа в Бургас', 'GRAFA TOUR', 'Летен театър', 'Сцена',
          'Най-мащабното събитие в кариерата на българския топ изпълнител. Бургас готов ли си?', 60.00, trunc(SYSDATE)+55 + INTERVAL '20:30' HOUR TO MINUTE);

---- Rolling Stones -------    
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 'NDK', 'Zala 1',
          'The world tour comes to Bulgaria', 100.00, trunc(SYSDATE)-27 + INTERVAL '20:00' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 'NDK', 'Zala 1',
          'The world tour comes to Bulgaria', 100.00, trunc(SYSDATE)-26 + INTERVAL '20:00' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 'Античен театър', 'Сцена',
          'The world tour comes to Bulgaria', 100.00, trunc(SYSDATE)+30 + INTERVAL '20:00' HOUR TO MINUTE);

---- Imagine of Dragons -----               
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Mercury - Sofia', 'Mercury World Tour', 'NDK', 'Zala 3',
          'The world tour comes to Bulgaria', 100.00, trunc(SYSDATE)-17 + INTERVAL '19:30' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 'Античен театър', 'Сцена',
          'The world tour comes to Bulgaria', 100.00, trunc(SYSDATE)+34 + INTERVAL '19:30' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Mercury - Burgas', 'Mercury World Tour', 'Летен театър ', 'Сцена',
          'The world tour comes to Bulgaria', 100.00, trunc(SYSDATE)+46 + INTERVAL '19:30' HOUR TO MINUTE);

          INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Mercury - Burgas 2', 'Mercury World Tour', 'Летен театър ', 'Сцена',
          'The world tour comes to Bulgaria', 100.00, trunc(SYSDATE)+48 + INTERVAL '19:30' HOUR TO MINUTE);

---- Lili Ivanova -----
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Просто Лили - НДК', 'Турнето на Лили Иванова', 'NDK', 'Zala 3',
          'Коледния концерт на Лили', 90.00, trunc(SYSDATE)+120 + INTERVAL '20:15' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Всичко Лили - НДК', 'Турнето на Лили Иванова', 'NDK', 'Zala 3',
          'Коледния концерт на Лили', 90.00, trunc(SYSDATE)+128 + INTERVAL '20:00' HOUR TO MINUTE);         

----- Hamlet -----
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 'Народен театър "Иван Вазов"', 'Голяма сцена',
          'Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата', 40.00, trunc(SYSDATE)-83 + INTERVAL '19:30' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Постановка на Хамлет - 2', 'Хамлет в Народния Театър', 'Народен театър "Иван Вазов"', 'Голяма сцена',
          'Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата', 40.00, trunc(SYSDATE)-52 + INTERVAL '19:30' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Постановка на Хамлет - 3', 'Хамлет в Народния Театър', 'Народен театър "Иван Вазов"', 'Голяма сцена',
          'Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата', 40.00, trunc(SYSDATE)-22 + INTERVAL '19:30' HOUR TO MINUTE);

INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Постановка на Хамлет - 4', 'Хамлет в Народния Театър', 'Народен театър "Иван Вазов"', 'Голяма сцена',
          'Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата', 40.00, trunc(SYSDATE)+40 + INTERVAL '19:30' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Постановка на Хамлет - 5', 'Хамлет в Народния Театър', 'Народен театър "Иван Вазов"', 'Голяма сцена',
          'Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата', 40.00, trunc(SYSDATE)+70 + INTERVAL '19:30' HOUR TO MINUTE);
          
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('Постановка на Хамлет - 6', 'Хамлет в Народния Театър', 'Народен театър "Иван Вазов"', 'Голяма сцена',
          'Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата', 40.00, trunc(SYSDATE)+101 + INTERVAL '19:30' HOUR TO MINUTE);

------ SIGNAL ---------
INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ БУРГАС', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ', 'Летен театър', 'Сцена',
          'Първи от трите юбилейни концерти на група Сигнал за 45-та годишнина', 70.00, trunc(SYSDATE)-56 + INTERVAL '20:30' HOUR TO MINUTE);       

INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ СОФИЯ', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ', 'NDK', 'Zala 3',
          'Втори от трите юбилейни концерти на група Сигнал за 45-та годишнина', 70.00, trunc(SYSDATE)+44 + INTERVAL '20:30' HOUR TO MINUTE);       

INSERT INTO SUB_EVENT (sub_event_name, event_name, venue_name_id, stage_name_id, description, ticket_price, start_datetime)
  VALUES ('СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ ПЛОВДИВ', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ', 'Античен театър', 'Сцена',
          'Последен юбилейни концерти на група Сигнал за 45-та годишнина', 70.00, trunc(SYSDATE)+69 + INTERVAL '20:30' HOUR TO MINUTE);       

------ WISH LIST ---------       
INSERT INTO WISHLIST(sub_event_name, event_name, username)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 'bobo');
          
INSERT INTO WISHLIST(sub_event_name, event_name, username)
VALUES ('Years&Years', 'Burgas Fest', 'bobo');

INSERT INTO WISHLIST(sub_event_name, event_name, username)
VALUES ('Бургас- Greenburg fest-/Papa Beer/', 'Cool den - tour', 'bobo');

INSERT INTO WISHLIST(sub_event_name, event_name, username)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 'bobo');

INSERT INTO WISHLIST(sub_event_name, event_name, username)
VALUES ('СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ ПЛОВДИВ', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ', 'bobo');

INSERT INTO WISHLIST(sub_event_name, event_name, username)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 'bobo');

------ REVIEWS --------- 
INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('giliev', 'My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 'Amazing', 5);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('bobo', 'My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 'Невероятен концерт беше!!! Тези два часа не ми стигнаха! Жалко малко за опашките на входа, едвам влезнах преди началото. Но това ще остане най-якия концерт на живота ми!!!', 4);
     
INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('bobo', 'Mercury - Sofia', 'Mercury World Tour', 'WOW!!!', 5);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('bobo', 'Графа в София - 1', 'GRAFA TOUR', null, 1);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('bobo', 'Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 'Невероятна пиеса!', null);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('bobo', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ БУРГАС', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ', 'Още 45 така!!!', 4);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('bobo', 'Графа в София - 2', 'GRAFA TOUR', 'Искаме още!!!', null);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('bobo', 'Ромео и Жулиета', 'Театрални вечери във Варна', null, 1);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('Алиса', 'Ромео и Жулиета', 'Театрални вечери във Варна', null, 5);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('spongebob', 'Ромео и Жулиета', 'Театрални вечери във Варна', null, 5);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('хитър-петър', 'Ромео и Жулиета', 'Театрални вечери във Варна', null, 1);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('user123', 'Ромео и Жулиета', 'Театрални вечери във Варна', 'Много приятна постановка', null);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('john.doe', 'Ромео и Жулиета', 'Театрални вечери във Варна', null, 4);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('sa60', 'Ромео и Жулиета', 'Театрални вечери във Варна', null, 4);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('laura', 'Ромео и Жулиета', 'Театрални вечери във Варна', null, 2);

INSERT INTO REVIEW (username, sub_event_name, event_name, review_comment, review_score)
VALUES ('peter.pan', 'Ромео и Жулиета', 'Театрални вечери във Варна', null, 4);

-------------- PERFORMER_SUB_EVENT ----------------------

INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Grafa', 'Графа в София - 1', 'GRAFA TOUR');
  
  INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Grafa', 'Графа в София - 2', 'GRAFA TOUR');
  
  INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Grafa', 'Графа в София - 3', 'GRAFA TOUR');
  
  INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Grafa', 'Графа в Пловдив', 'GRAFA TOUR');
  
  INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Grafa', 'Графа в Бургас', 'GRAFA TOUR');
  
  INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Rolling Stones', 'My Satisfaction Sofia - 1', 'ROLLING STONES TOUR');
  
  INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Rolling Stones', 'My Satisfaction Sofia - 2', 'ROLLING STONES TOUR');
  
  INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Rolling Stones', 'My Satisfaction Plovdiv', 'ROLLING STONES TOUR');
  
  INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Imagine Dragons', 'Mercury - Sofia', 'Mercury World Tour');
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Imagine Dragons', 'Mercury - Plovdiv', 'Mercury World Tour');
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Imagine Dragons', 'Mercury - Burgas', 'Mercury World Tour');
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Imagine Dragons', 'Mercury - Burgas 2', 'Mercury World Tour');
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Лили Иванова', 'Просто Лили - НДК', 'Турнето на Лили Иванова');
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Лили Иванова', 'Всичко Лили - НДК', 'Турнето на Лили Иванова');  
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Леонид Йовчев', 'Постановка на Хамлет - 1', 'Хамлет в Народния Театър');
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Леонид Йовчев', 'Постановка на Хамлет - 2', 'Хамлет в Народния Театър');  
  
      INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Леонид Йовчев', 'Постановка на Хамлет - 3', 'Хамлет в Народния Театър');  
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Леонид Йовчев', 'Постановка на Хамлет - 4', 'Хамлет в Народния Театър');  
  
      INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Леонид Йовчев', 'Постановка на Хамлет - 5', 'Хамлет в Народния Театър');  
  
      INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Леонид Йовчев', 'Постановка на Хамлет - 6', 'Хамлет в Народния Театър');  
  
      INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Мариус Куркински', 'Постановка на Хамлет - 1', 'Хамлет в Народния Театър');  
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Мариус Куркински', 'Постановка на Хамлет - 2', 'Хамлет в Народния Театър');  
  
      INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Мариус Куркински', 'Постановка на Хамлет - 3', 'Хамлет в Народния Театър');  
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Мариус Куркински', 'Постановка на Хамлет - 4', 'Хамлет в Народния Театър');  
  
      INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Мариус Куркински', 'Постановка на Хамлет - 5', 'Хамлет в Народния Театър');  
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('Мариус Куркински', 'Постановка на Хамлет - 6', 'Хамлет в Народния Театър');  
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('СИГНАЛ', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ БУРГАС', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ'); 
  
      INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('СИГНАЛ', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ СОФИЯ', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ'); 
  
    INSERT INTO PERFORMER_SUB_EVENT (performer_name, sub_event_name, event_name)
VALUES
  ('СИГНАЛ', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ ПЛОВДИВ', 'СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ'); 

  INSERT INTO PAYMENT_TYPE(payment_type)
VALUES ('BANK TRANSFER');  

  INSERT INTO PAYMENT_TYPE(payment_type)
VALUES ('CASH'); 

  INSERT INTO PAYMENT_TYPE(payment_type)
VALUES ('DEBIT/CREDIT CARD'); 

  INSERT INTO DELIVERY_TYPE(delivery_type)
VALUES ('OFFICE_DELIVERY');  

  INSERT INTO DELIVERY_TYPE(delivery_type)
VALUES ('HOME_DELIVERY');

  INSERT INTO DELIVERY_TYPE(delivery_type)
VALUES ('EMAIL_DELIVERY');

  INSERT INTO PURCHASE_STATUS(purchase_status)
VALUES ('COMPLETED');

  INSERT INTO PURCHASE_STATUS(purchase_status)
VALUES ('AWAITING_PAYMENT'); 

  INSERT INTO PURCHASE_ORDER(purchase_status, username, delivery_type, payment_type, creation_date, address_street, address_city, address_post_code)
VALUES ('COMPLETED', 'bobo', 'EMAIL_DELIVERY', 'CASH', TO_DATE('2023-07-14 20:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'street 58', 'Sofia', '1938');

  INSERT INTO PURCHASE_ORDER(purchase_status, username, delivery_type, payment_type, creation_date, address_street, address_city, address_post_code)
VALUES ('COMPLETED', 'bobo', 'EMAIL_DELIVERY', 'CASH', TO_DATE('2023-07-19 12:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'street 58', 'Sofia', '1938');

  INSERT INTO PURCHASE_ORDER(purchase_status, username, delivery_type, payment_type, creation_date, address_street, address_city, address_post_code)
VALUES ('COMPLETED', 'giliev', 'HOME_DELIVERY', 'BANK TRANSFER', TO_DATE('2023-07-18 15:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'street 22', 'Sofia', '1900');

  INSERT INTO PURCHASE_ORDER(purchase_status, username, delivery_type, payment_type, creation_date, address_street, address_city, address_post_code)
VALUES ('COMPLETED', 'giliev', 'OFFICE_DELIVERY', 'BANK TRANSFER', TO_DATE('2023-07-05 08:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'street 22', 'Sofia', '1900');

  INSERT INTO PURCHASE_ORDER(purchase_status, username, delivery_type, payment_type, creation_date, address_street, address_city, address_post_code)
VALUES ('COMPLETED', 'giliev', 'OFFICE_DELIVERY', 'BANK TRANSFER', TO_DATE('2023-07-08 09:00:00', 'YYYY-MM-DD HH24:MI:SS'), 'street 35', 'Varna', '9000');

  INSERT INTO SEAT_STATUS(seat_status)
VALUES ('ACQUIRED');  
  
  INSERT INTO SEAT_STATUS(seat_status)
VALUES ('AVAILABLE');

  INSERT INTO SEAT_STATUS(seat_status)
VALUES ('RESERVED');

INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, order_id, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 1, 1, 'Sector 1', 'Zala 1', 'NDK', 'ACQUIRED', 1, 'STUDENT'); 

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 2, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 3, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 4, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 5, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 6, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 7, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 8, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 9, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 10, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 11, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 12, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 13, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 14, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 15, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 16, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 17, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 18, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 19, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 20, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 21, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 22, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 23, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 24, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 25, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 26, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 27, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 28, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 29, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 30, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 31, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 32, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, order_id, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 33, 2, 'Sector 2', 'Zala 1', 'NDK', 'ACQUIRED', 2, 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, order_id, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 34, 2, 'Sector 2', 'Zala 1', 'NDK', 'ACQUIRED', 3, 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, order_id, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 35, 2, 'Sector 2', 'Zala 1', 'NDK', 'ACQUIRED', 4, 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, order_id, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 36, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 3, 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 37, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 38, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 39, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 40, 3, 'Sector 2', 'Zala 1', 'NDK', 'RESERVED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 41, 4, 'Sector 2', 'Zala 1', 'NDK', 'RESERVED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 42, 4, 'Sector 2', 'Zala 1', 'NDK', 'RESERVED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 43, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 44, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 45, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 46, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 47, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 48, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 49, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 50, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 51, 1, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 52, 1, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 53, 1, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 54, 1, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 55, 1, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 56, 2, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 57, 2, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 58, 2, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 59, 2, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 60, 2, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 61, 3, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 62, 3, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 63, 3, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, order_id, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 64, 3, 'Sector 3', 'Zala 1', 'NDK', 'ACQUIRED', 5, 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 65, 3, 'Sector 3', 'Zala 1', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 66, 4, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 67, 4, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 68, 4, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 69, 4, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 70, 4, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 71, 5, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 72, 5, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 73, 5, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 74, 5, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 75, 5, 'Sector 3', 'Zala 1', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 76, 1, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 77, 1, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 78, 1, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 79, 1, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 80, 1, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 81, 2, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 82, 2, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 83, 2, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 84, 2, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 85, 2, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 86, 3, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 87, 3, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 88, 3, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 89, 3, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 90, 3, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 91, 4, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 92, 4, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 93, 4, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 94, 4, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 95, 4, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 96, 5, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 97, 5, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 98, 5, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 99, 5, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 100, 5, 'Sector 4', 'Zala 1', 'NDK', 'ACQUIRED', 'STUDENT');


  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, order_id, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 1, 1, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 1, 'STUDENT'); 

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 2, 1, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 3, 1, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 4, 1, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 5, 1, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 6, 2, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 7, 2, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 8, 2, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 9, 2, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 10, 2, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 11, 3, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 12, 3, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 13, 3, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 14, 3, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 15, 3, 'Sector 1', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 16, 4, 'Sector 1', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 17, 4, 'Sector 1', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 18, 4, 'Sector 1', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 19, 4, 'Sector 1', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 20, 4, 'Sector 1', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 21, 1, 'Sector 2', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 22, 1, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 23, 1, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 24, 1, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 25, 1, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 26, 2, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 27, 2, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 28, 2, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 29, 2, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 30, 2, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 31, 3, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 32, 3, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 33, 3, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 34, 3, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 35, 3, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 36, 4, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 37, 4, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 38, 4, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 39, 4, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 40, 4, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 41, 1, 'Sector 3', 'Zala 3', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 42, 1, 'Sector 3', 'Zala 3', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 43, 1, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 44, 1, 'Sector 3', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 45, 1, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 46, 2, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 47, 2, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 48, 2, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 49, 2, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 50, 2, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 51, 3, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 52, 3, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 53, 3, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 54, 3, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 55, 3, 'Sector 3', 'Zala 3', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 56, 4, 'Sector 3', 'Zala 3', 'NDK', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 57, 4, 'Sector 3', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 58, 4, 'Sector 3', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 59, 4, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 60, 4, 'Sector 3', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 61, 1, 'Sector 4', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 62, 1, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 63, 1, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 64, 1, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 65, 1, 'Sector 4', 'Zala 3', 'NDK', 'RESERVED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 66, 2, 'Sector 4', 'Zala 3', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 67, 2, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 68, 2, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 69, 2, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 70, 2, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 71, 3, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 72, 3, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 73, 3, 'Sector 4', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 74, 3, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 75, 3, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 76, 4, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 77, 4, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 78, 4, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 79, 4, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 80, 4, 'Sector 4', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 81, 1, 'Sector 5', 'Zala 3', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 82, 1, 'Sector 5', 'Zala 3', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 83, 1, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 84, 1, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 85, 1, 'Sector 5', 'Zala 3', 'NDK', 'RESERVED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 86, 2, 'Sector 5', 'Zala 3', 'NDK', 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 87, 2, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 88, 2, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 89, 2, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 90, 2, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 91, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 92, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 93, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 94, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 95, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 96, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 97, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 98, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 99, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Sofia', 'Mercury World Tour', 100, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');



--------------------------------------------Графа - 2023-07-21 19:30:00------------------------------------


  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 1, 1, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 2, 1, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 3, 1, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 4, 1, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 5, 1, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 6, 2, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 7, 2, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 8, 2, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 9, 2, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 10, 2, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 11, 3, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 12, 3, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 13, 3, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 14, 3, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 15, 3, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 16, 4, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 17, 4, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 18, 4, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 19, 4, 'Sector 1', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 20, 4, 'Sector 1', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 21, 1, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 22, 1, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 23, 1, 'Sector 2', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 24, 1, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 25, 1, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 26, 2, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 27, 2, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 28, 2, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 29, 2, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 30, 2, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 31, 3, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 32, 3, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 33, 3, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 34, 3, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 35, 3, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 36, 4, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 37, 4, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 38, 4, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 39, 4, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 40, 4, 'Sector 2', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 41, 1, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 42, 1, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 43, 1, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 44, 1, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 45, 1, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 46, 2, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 47, 2, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 48, 2, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 49, 2, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 50, 2, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 51, 3, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 52, 3, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 53, 3, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 54, 3, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 55, 3, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 56, 4, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 57, 4, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 58, 4, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 59, 4, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 60, 4, 'Sector 3', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 61, 1, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 62, 1, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 63, 1, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 64, 1, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 65, 1, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 66, 2, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 67, 2, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 68, 2, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 69, 2, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 70, 2, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 71, 3, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 72, 3, 'Sector 4', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 73, 3, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 74, 3, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 75, 3, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 76, 4, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 77, 4, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 78, 4, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 79, 4, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 80, 4, 'Sector 4', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 81, 1, 'Sector 5', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 82, 1, 'Sector 5', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 83, 1, 'Sector 5', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 84, 1, 'Sector 5', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 85, 1, 'Sector 5', 'Zala 3', 'NDK', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 86, 2, 'Sector 5', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 87, 2, 'Sector 5', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 88, 2, 'Sector 5', 'Zala 3', 'NDK', 'ACQUIRED', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 89, 2, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 90, 2, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 91, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 92, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 93, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 94, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 95, 3, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 96, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 97, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 98, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 99, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 2', 'GRAFA TOUR', 100, 4, 'Sector 5', 'Zala 3', 'NDK', 'AVAILABLE', 'STUDENT');  
------------------------------------------------------------------------------------------------------------------------------------------



---------------------------------------------Графа - 2023-09-24 - 19:30:00--------------------------
  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 1, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 2, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 3, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 4, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 5, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 6, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 7, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 8, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 9, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 10, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 11, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 12, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 13, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 14, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 15, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 16, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 17, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 18, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 19, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 20, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 21, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 22, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 23, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 24, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 25, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 26, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 27, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 28, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 29, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 30, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 31, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 32, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 33, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 34, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 35, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 36, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 37, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 38, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 39, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 40, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 41, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 42, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 43, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 44, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 45, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 46, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 47, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 48, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 49, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 50, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 51, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 52, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 53, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 54, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 55, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 56, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 57, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 58, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 59, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 60, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 61, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 62, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 63, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 64, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 65, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 66, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 67, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 68, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 69, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 70, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 71, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 72, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 73, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 74, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 75, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 76, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 77, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 78, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 79, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 80, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 81, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 82, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 83, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 84, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 85, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 86, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 87, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 88, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 89, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 90, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 91, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 92, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 93, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 94, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 95, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 96, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 97, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 98, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 99, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в София - 3', 'GRAFA TOUR', 100, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');
--------------------------------------------------------------------------------------------------------------------

----------------------------Графа 2023-09-26 19:30:00--------------------------------------------------
  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, order_id, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 1, 1, 'Сектор 1', 'Сцена', 'Античен театър', 3, 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, order_id, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 2, 1, 'Сектор 1', 'Сцена', 'Античен театър', 3,  'RESERVED', 'KID');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, order_id, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 3, 1, 'Сектор 1', 'Сцена', 'Античен театър', 3, 'RESERVED', 'ELDERLY');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 4, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 5, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 6, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 7, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 8, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 9, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 10, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 11, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 12, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 13, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 14, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 15, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 16, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 17, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 18, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 19, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 20, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 21, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 22, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 23, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 24, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 25, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 1, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 2, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 3, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 4, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 5, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 6, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 7, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 8, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 9, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 10, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 11, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 12, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 13, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 14, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 15, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 16, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 17, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 18, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 19, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 20, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 21, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 22, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 23, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 24, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 25, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 1, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 2, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 3, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 4, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 5, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 6, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 7, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 8, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 9, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 10, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 11, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 12, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 13, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 14, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 15, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 16, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 17, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 18, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 19, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 20, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 21, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 22, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 23, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 24, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 25, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 1, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 2, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 3, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 4, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 5, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 6, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 7, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 8, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 9, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 10, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 11, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 12, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 13, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 14, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 15, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 16, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 17, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 18, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 19, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 20, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 21, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 22, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 23, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 24, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Пловдив', 'GRAFA TOUR', 25, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');


----------------------------------------------Графа - 2023-10-10 - 20:30:00 ----------------------------------
  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, order_id, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 1, 1, 'Сектор 1', 'Сцена', 'Летен театър', 4, 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, order_id, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 2, 1, 'Сектор 1', 'Сцена', 'Летен театър', 4, 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, order_id, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 3, 1, 'Сектор 1', 'Сцена', 'Летен театър', 4, 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 4, 1, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 5, 1, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 6, 2, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 7, 2, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 8, 2, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 9, 2, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 10, 2, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 11, 3, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 12, 3, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 13, 3, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 14, 3, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 15, 3, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 16, 4, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 17, 4, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 18, 4, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 19, 4, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 20, 4, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 21, 5, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 22, 5, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 23, 5, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 24, 5, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 25, 5, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 26, 6, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 27, 6, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 28, 6, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 29, 6, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 30, 6, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 31, 7, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 32, 7, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 33, 7, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 34, 7, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 35, 7, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 36, 8, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 37, 8, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 38, 8, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 39, 8, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 40, 8, 'Сектор 1', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 1, 1, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 2, 1, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 3, 1, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 4, 1, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 5, 1, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 6, 2, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 7, 2, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 8, 2, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 9, 2, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 10, 2, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 11, 3, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 12, 3, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 13, 3, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 14, 3, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 15, 3, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 16, 4, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 17, 4, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 18, 4, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 19, 4, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 20, 4, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 21, 5, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 22, 5, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 23, 5, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 24, 5, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 25, 5, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 26, 6, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 27, 6, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 28, 6, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 29, 6, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 30, 6, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 31, 7, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 32, 7, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 33, 7, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 34, 7, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 35, 7, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 36, 8, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 37, 8, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 38, 8, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 39, 8, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 40, 8, 'Сектор 2', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 1, 1, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 2, 1, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 3, 1, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 4, 1, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 5, 1, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 6, 2, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 7, 2, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 8, 2, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 9, 2, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 10, 2, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 11, 3, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 12, 3, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 13, 3, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 14, 3, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 15, 3, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 16, 4, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 17, 4, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 18, 4, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 19, 4, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 20, 4, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 21, 5, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 22, 5, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 23, 5, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 24, 5, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 25, 5, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 26, 6, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 27, 6, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 28, 6, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 29, 6, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 30, 6, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 31, 7, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 32, 7, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 33, 7, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 34, 7, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 35, 7, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 36, 8, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 37, 8, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 38, 8, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 39, 8, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Графа в Бургас', 'GRAFA TOUR', 40, 8, 'Сектор 3', 'Сцена', 'Летен театър', 'AVAILABLE', 'NONE');


----------------------------------Rolling stones - 2023-07-21 - 20:00:00 ---------------------------------
  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 1, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 2, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 3, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 4, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 5, 1, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 6, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 7, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 8, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 9, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 10, 2, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 11, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 12, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 13, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 14, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 15, 3, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 16, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 17, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 18, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 19, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 20, 4, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 21, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 22, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 23, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 24, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 25, 5, 'Sector 1', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 26, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 27, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 28, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 29, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 30, 1, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 31, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 32, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 33, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 34, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 35, 2, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 36, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 37, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 38, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 39, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 40, 3, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 41, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 42, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 43, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 44, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 45, 4, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 46, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 47, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 48, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 49, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 50, 5, 'Sector 2', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 51, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 52, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 53, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 54, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 55, 1, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 56, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 57, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 58, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 59, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 60, 2, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 61, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 62, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 63, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 64, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 65, 3, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 66, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 67, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 68, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 69, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 70, 4, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 71, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 72, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 73, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 74, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 75, 5, 'Sector 3', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 76, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 77, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 78, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 79, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 80, 1, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 81, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 82, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 83, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 84, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 85, 2, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 86, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 87, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 88, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 89, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 90, 3, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 91, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 92, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 93, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 94, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 95, 4, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 96, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 97, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 98, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 99, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Sofia - 2', 'ROLLING STONES TOUR', 100, 5, 'Sector 4', 'Zala 1', 'NDK', 'AVAILABLE', 'NONE');


----------------------------------Rolling stones - 2023-09-15 - 20:00:00---------------------------------
  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 1, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 2, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 3, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 4, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 5, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 6, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 7, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 8, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 9, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 10, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 11, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 12, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 13, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 14, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 15, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 16, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 17, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 18, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 19, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 20, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 21, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 22, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 23, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 24, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 25, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 1, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 2, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 3, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 4, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 5, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 6, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 7, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 8, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 9, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 10, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 11, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 12, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 13, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 14, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 15, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 16, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 17, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 18, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 19, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 20, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 21, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 22, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 23, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 24, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 25, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 1, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 2, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 3, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 4, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 5, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 6, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 7, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 8, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 9, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 10, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 11, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 12, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 13, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 14, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 15, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 16, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 17, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 18, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 19, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 20, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 21, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 22, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 23, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 24, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 25, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 1, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 2, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 3, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 4, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 5, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 6, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 7, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 8, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 9, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 10, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 11, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 12, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 13, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 14, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 15, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 16, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 17, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 18, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 19, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 20, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 21, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 22, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 23, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 24, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('My Satisfaction Plovdiv', 'ROLLING STONES TOUR', 25, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

-------------------------------------Imagine Dragons - 2023-09-19 - 19:30:00---------------------------------

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 1, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 2, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 3, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 4, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 5, 1, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 6, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 7, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 8, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 9, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 10, 2, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 11, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 12, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 13, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 14, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 15, 3, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 16, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 17, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 18, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 19, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 20, 4, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 21, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 22, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 23, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 24, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 25, 5, 'Сектор 1', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 1, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 2, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 3, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 4, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 5, 1, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 6, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 7, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 8, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 9, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 10, 2, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 11, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 12, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 13, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 14, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 15, 3, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 16, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 17, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 18, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 19, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 20, 4, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 21, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 22, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 23, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 24, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 25, 5, 'Сектор 2', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 1, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 2, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 3, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 4, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 5, 1, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 6, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 7, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 8, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 9, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 10, 2, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 11, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 12, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 13, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 14, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 15, 3, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 16, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 17, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 18, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 19, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 20, 4, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 21, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 22, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 23, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 24, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 25, 5, 'Сектор 3', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 1, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 2, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 3, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 4, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 5, 1, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 6, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 7, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 8, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 9, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 10, 2, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 11, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 12, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 13, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 14, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 15, 3, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 16, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 17, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 18, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 19, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 20, 4, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 21, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 22, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 23, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 24, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Mercury - Plovdiv', 'Mercury World Tour', 25, 5, 'Сектор 4', 'Сцена', 'Античен театър', 'AVAILABLE', 'NONE');

----------------------------------Хамлет - 2023-05-25 - 19:30:00--------------------------------------

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 1, 1, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 2, 1, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 3, 1, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 4, 1, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 5, 1, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 6, 1, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 7, 1, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 8, 1, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 9, 2, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 10, 2, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 11, 2, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 12, 2, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 13, 2, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 14, 2, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 15, 2, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 16, 2, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 17, 3, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 18, 3, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 19, 3, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 20, 3, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 21, 3, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 22, 3, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 23, 3, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 24, 3, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 25, 4, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 26, 4, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 27, 4, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 28, 4, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 29, 4, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 30, 4, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 31, 4, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 32, 4, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 33, 5, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 34, 5, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 35, 5, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 36, 5, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 37, 5, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 38, 5, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 39, 5, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 40, 5, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 41, 6, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 42, 6, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 43, 6, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 44, 6, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 45, 6, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 46, 6, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 47, 6, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 48, 6, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 49, 7, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 50, 7, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 51, 7, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 52, 7, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 53, 7, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 54, 7, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 55, 7, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 56, 7, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 57, 8, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 58, 8, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 59, 8, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 60, 8, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 61, 8, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 62, 8, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 63, 8, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 64, 8, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 65, 9, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 66, 9, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 67, 9, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 68, 9, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 69, 9, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 70, 9, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 71, 9, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 72, 9, 'Сектор 1', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 73, 1, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 74, 1, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 75, 1, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 76, 1, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 77, 1, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 78, 2, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 79, 2, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 80, 2, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 81, 2, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 82, 2, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 83, 3, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 84, 3, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 85, 3, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 86, 3, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 87, 3, 'Сектор 2', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 88, 1, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 89, 1, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 90, 1, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 91, 1, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 92, 1, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 93, 1, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 94, 2, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 95, 2, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 96, 2, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 97, 2, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 98, 2, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 99, 2, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 100, 3, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 101, 3, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 102, 3, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 103, 3, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 104, 3, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 105, 3, 'Сектор 3', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 106, 1, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 107, 1, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 108, 1, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 109, 1, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 110, 1, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 111, 2, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 112, 2, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 113, 2, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 114, 2, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 115, 2, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 116, 3, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 117, 3, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 118, 3, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 119, 3, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 120, 3, 'Сектор 4', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 121, 1, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 122, 1, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 123, 1, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 124, 1, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 125, 2, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 126, 2, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 127, 2, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 128, 2, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 129, 3, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 130, 3, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 131, 3, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 132, 3, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 133, 1, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 134, 1, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 135, 1, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 136, 1, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 137, 1, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 138, 2, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 139, 2, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 140, 2, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 141, 2, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 142, 2, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 143, 3, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 144, 3, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 145, 3, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 146, 3, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'ACQUIRED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 147, 3, 'Сектор 6', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 148, 1, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 149, 1, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 150, 1, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 151, 1, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 152, 2, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 153, 2, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 154, 2, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 155, 2, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 156, 3, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 157, 3, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'AVAILABLE', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 158, 3, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

  INSERT INTO TICKET(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, seat_status, discount_name)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 159, 3, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'RESERVED', 'NONE');

INSERT INTO CART_ITEM(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, username)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 132, 3, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'sa60');

INSERT INTO CART_ITEM(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, username)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 131, 3, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'sa60');

INSERT INTO CART_ITEM(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, username)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 130, 3, 'Сектор 5', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'sa60');

INSERT INTO CART_ITEM(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, username)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 158, 3, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'sa60');

INSERT INTO CART_ITEM(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, username)
VALUES ('Постановка на Хамлет - 1', 'Хамлет в Народния Театър', 159, 3, 'Сектор 7', 'Голяма сцена', 'Народен театър "Иван Вазов"', 'sa60');

INSERT INTO CART_ITEM(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, username)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 59, 2, 'Sector 3', 'Zala 1', 'NDK', 'sa60');

INSERT INTO CART_ITEM(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, username)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 66, 4, 'Sector 3', 'Zala 1', 'NDK', 'sa60');

INSERT INTO CART_ITEM(sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, username)
VALUES ('My Satisfaction Sofia - 1', 'ROLLING STONES TOUR', 70, 4, 'Sector 3', 'Zala 1', 'NDK', 'sa60');
