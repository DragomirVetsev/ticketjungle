CREATE TABLE PERFORMER (
    performer_name CHAR(30 CHAR),
    description VARCHAR(500 CHAR) NOT NULL,
    CONSTRAINT performer_name_pk PRIMARY KEY (performer_name)
);

CREATE TABLE PHONE_CODE(
    phone_code CHAR(6 CHAR),
    CONSTRAINT phone_code_pk PRIMARY KEY (phone_code)
);

CREATE TABLE CITY( 
    city_name CHAR (30 CHAR),
    CONSTRAINT city_pk PRIMARY KEY (city_name)
);

CREATE TABLE OFFICE( 
    office_name CHAR (30 CHAR),    
    address_street VARCHAR (50 CHAR) NOT NULL,
    address_city CHAR (30 CHAR) NOT NULL,
    address_post_code VARCHAR(10 CHAR) NOT NULL,
    phone_code CHAR(6 CHAR) NOT NULL,
    phone_number VARCHAR(9 CHAR) NOT NULL,
    email VARCHAR(100 CHAR) NOT NULL UNIQUE,
    opening_time VARCHAR(5 CHAR) NOT NULL,
    closing_time VARCHAR(5 CHAR) NOT NULL,
    CONSTRAINT office_name_pk PRIMARY KEY (office_name),
    CONSTRAINT office_phone_uk UNIQUE (phone_code, phone_number),
    CONSTRAINT office_address_uk UNIQUE (address_city, address_street),
    CONSTRAINT address_office_city_fk FOREIGN KEY (address_city) REFERENCES CITY (city_name),
    CONSTRAINT office_phone_code_fk FOREIGN KEY (phone_code) REFERENCES PHONE_CODE(phone_code)
);

CREATE TABLE VENUE( 
    venue_name CHAR(50 CHAR),
    address_street VARCHAR(50 CHAR) NOT NULL,
    address_city CHAR(30 CHAR) NOT NULL,
    address_post_code VARCHAR(10 CHAR) NOT NULL,
    CONSTRAINT venue_name_pk PRIMARY KEY (venue_name),
    CONSTRAINT address_venue_city_fk FOREIGN KEY (address_city) REFERENCES CITY (city_name)
);

CREATE TABLE STAGE(
    stage_name CHAR(30 CHAR),
    venue_name CHAR(50 CHAR),
    CONSTRAINT stage_pk PRIMARY KEY (stage_name, venue_name),  
    CONSTRAINT stage_venue_fk FOREIGN KEY (venue_name) REFERENCES VENUE (venue_name)
);

CREATE TABLE SECTOR(
    sector_name CHAR(30 CHAR),
    venue_name CHAR(50 CHAR),
    stage_name CHAR(30 CHAR),
    CONSTRAINT sector_pk PRIMARY KEY (sector_name, stage_name, venue_name),
    CONSTRAINT sector_fk FOREIGN KEY (venue_name, stage_name) REFERENCES STAGE (venue_name, stage_name)
);

CREATE TABLE LINE(
    line_number NUMBER(3),
    venue_name CHAR(50 CHAR),
    stage_name CHAR(30 CHAR),
    sector_name CHAR(30 CHAR),
    CONSTRAINT line_pk PRIMARY KEY (line_number, sector_name, stage_name, venue_name),   
    CONSTRAINT line_sector_fk FOREIGN KEY (venue_name, stage_name, sector_name) REFERENCES SECTOR (venue_name, stage_name, sector_name)
);

CREATE TABLE SEAT(
    seat_number NUMBER(3),
    venue_name CHAR(50 CHAR),
    stage_name CHAR(30 CHAR),
    sector_name CHAR(30 CHAR),
    line_number NUMBER(3),
    CONSTRAINT seat_pk PRIMARY KEY (seat_number, line_number, sector_name, stage_name, venue_name),
    CONSTRAINT seat_line_fk FOREIGN KEY (venue_name, stage_name, sector_name, line_number) REFERENCES LINE (venue_name, stage_name, sector_name, line_number)
);

CREATE TABLE SYSTEM_USER (
    username CHAR(30 CHAR),
    first_name VARCHAR(30 CHAR) NOT NULL,
    last_name VARCHAR(30 CHAR) NOT NULL,
    email VARCHAR(100 CHAR) NOT NULL UNIQUE,
    password VARCHAR(100 CHAR) NOT NULL,
    phone_number VARCHAR(9 CHAR) NOT NULL,
    phone_code CHAR(6 CHAR) NOT NULL,
    address_street varchar (30 CHAR) NOT NULL,
    address_city varchar (30 CHAR) NOT NULL,
    address_post_code varchar (10 CHAR) NOT NULL,
    role varchar (30 CHAR) NOT NULL,
    CONSTRAINT user_pk PRIMARY KEY (username),
    CONSTRAINT phone_code_fk FOREIGN KEY (phone_code) REFERENCES PHONE_CODE(phone_code)
);

CREATE TABLE CUSTOMER (
    username CHAR(30 CHAR),
    birthday DATE,
    CONSTRAINT customer_pk PRIMARY KEY (username),
    CONSTRAINT customer_user_fk FOREIGN KEY (username) REFERENCES SYSTEM_USER(username)
);

CREATE TABLE OFFICE_WORKER (
    username CHAR(30 CHAR),
    office_name CHAR(30 CHAR),
    CONSTRAINT office_worker_pk PRIMARY KEY (username),
    CONSTRAINT worker_user_fk FOREIGN KEY (username) REFERENCES SYSTEM_USER(username),
    CONSTRAINT worker_office_fk FOREIGN KEY (office_name) REFERENCES OFFICE(office_name)
);

CREATE TABLE SYSTEM_ADMIN(
    admin_username CHAR(30 CHAR),
    CONSTRAINT system_admin_pk PRIMARY KEY (admin_username),
    CONSTRAINT admin_user_fk FOREIGN KEY (admin_username) REFERENCES SYSTEM_USER(username)
);

CREATE TABLE DISCOUNT (
    name CHAR(30 CHAR),
    percentage NUMBER(5,2) NOT NULL,
    CONSTRAINT discount_pk PRIMARY KEY (name)
);

CREATE TABLE GENRE (
    genre_type CHAR(50 CHAR),
    CONSTRAINT genre_pk PRIMARY KEY (genre_type)
);

CREATE TABLE EVENT(
    name CHAR(100 CHAR),
    description VARCHAR(500 CHAR) NOT NULL,
    genre_type CHAR(50 CHAR) NOT NULL,
    CONSTRAINT event_pk PRIMARY KEY (name),
    CONSTRAINT event_genre_fk FOREIGN KEY (genre_type) REFERENCES GENRE(genre_type)
);

CREATE TABLE SUB_EVENT(
    sub_event_name CHAR(100 CHAR) NOT NULL,
    event_name CHAR(100 CHAR) NOT NULL,
    venue_name_id CHAR(50 CHAR),
    stage_name_id CHAR(30 CHAR),
    description VARCHAR(500 CHAR) NOT NULL,
    ticket_price DECIMAL(8, 2) NOT NULL,
    start_datetime DATE,
    
    CONSTRAINT sub_event_pk PRIMARY KEY (sub_event_name, event_name),
    CONSTRAINT sub_event_uk UNIQUE (start_datetime, venue_name_id, stage_name_id),
    CONSTRAINT event_sub_event_fk FOREIGN KEY (event_name) REFERENCES EVENT(name),
    CONSTRAINT sub_event_stage_fk FOREIGN KEY (venue_name_id, stage_name_id) REFERENCES STAGE(venue_name, stage_name)
);

CREATE TABLE WISHLIST (
    sub_event_name CHAR(100 CHAR),
    event_name CHAR(100 CHAR),
    username CHAR(30 CHAR),
    CONSTRAINT wishlist_pk PRIMARY KEY (sub_event_name, event_name, username),
    CONSTRAINT wishlist_sub_event_fk FOREIGN KEY (sub_event_name, event_name) 
    REFERENCES SUB_EVENT(sub_event_name, event_name),
    CONSTRAINT wishlist_customer_fk FOREIGN KEY (username) REFERENCES CUSTOMER(username)
);
CREATE TABLE REVIEW (
    username CHAR(30 CHAR) NOT NULL,
    sub_event_name CHAR(100 CHAR),
    event_name CHAR(100 CHAR),
    review_comment VARCHAR(500 CHAR),
    review_score NUMBER (1,0) CHECK (review_score >= 1 AND review_score <= 5),
    CONSTRAINT review_pk PRIMARY KEY (username, sub_event_name, event_name),
    CONSTRAINT customer_review_fk FOREIGN KEY (username) REFERENCES CUSTOMER(username),
    CONSTRAINT review_sub_event_fk FOREIGN KEY (sub_event_name, event_name) REFERENCES SUB_EVENT(sub_event_name, event_name),
    CONSTRAINT review_score_xor_comment CHECK ((review_score IS NOT NULL AND review_comment IS NULL) OR (review_score IS NULL AND review_comment IS NOT NULL) OR (review_score IS NOT NULL AND review_comment IS NOT NULL))
);

CREATE TABLE PERFORMER_SUB_EVENT (
    performer_name CHAR(30 CHAR),
    sub_event_name CHAR(100 CHAR),
    event_name CHAR(100 CHAR),
    CONSTRAINT performer_sub_event_pk PRIMARY KEY (performer_name, sub_event_name, event_name),
    CONSTRAINT sub_event_performer_fk FOREIGN KEY (performer_name) REFERENCES PERFORMER (performer_name),
    CONSTRAINT performer_sub_event_fk FOREIGN KEY (sub_event_name, event_name) REFERENCES SUB_EVENT (sub_event_name, event_name)
);

CREATE TABLE PAYMENT_TYPE (
    payment_type CHAR(30 CHAR),
    CONSTRAINT payment_type_pk PRIMARY KEY (payment_type)
);

CREATE TABLE DELIVERY_TYPE (
    delivery_type CHAR(30 CHAR),
    CONSTRAINT delivery_type_pk PRIMARY KEY (delivery_type)
);

CREATE TABLE PURCHASE_STATUS (
    purchase_status CHAR(30 CHAR),
    CONSTRAINT purchase_status_pk PRIMARY KEY (purchase_status)
);

CREATE TABLE PURCHASE_ORDER(
    order_id NUMBER(6) GENERATED ALWAYS AS IDENTITY,
    purchase_status CHAR(30 CHAR) NOT NULL,
    username CHAR(30 CHAR) NOT NULL,
    delivery_type CHAR(30 CHAR) NOT NULL,
    payment_type CHAR(30 CHAR) NOT NULL,
    creation_date DATE NOT NULL,
    address_street VARCHAR(30 CHAR),
    address_city VARCHAR(30 CHAR),
    address_post_code VARCHAR(10 CHAR),
    CONSTRAINT purchase_order_pk PRIMARY KEY(order_id),
    CONSTRAINT order_customer_fk FOREIGN KEY(username) REFERENCES CUSTOMER(username),
    CONSTRAINT payment_order_fk FOREIGN KEY(payment_type) REFERENCES PAYMENT_TYPE(payment_type),
    CONSTRAINT delivery_order_fk FOREIGN KEY(delivery_type) REFERENCES DELIVERY_TYPE(delivery_type),
    CONSTRAINT purchase_order_fk FOREIGN KEY(purchase_status) REFERENCES PURCHASE_STATUS(purchase_status)
);

CREATE TABLE SEAT_STATUS (
    seat_status CHAR(20 CHAR),
    CONSTRAINT seat_status_pk PRIMARY KEY (seat_status)
);

CREATE TABLE TICKET (
    sub_event_name CHAR(100 CHAR),
    event_name CHAR(100 CHAR),
    seat_number NUMBER(3),
    line_number NUMBER(3),
    sector_name CHAR(30 CHAR),
    stage_name CHAR(30 CHAR),
    venue_name CHAR(50 CHAR),
    seat_status CHAR(20 CHAR) NOT NULL,
    order_id NUMBER(6),
    discount_name CHAR(30 CHAR) NOT NULL,
    PRIMARY KEY (sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name),
    CONSTRAINT ticket_discount_fk FOREIGN KEY (discount_name) REFERENCES DISCOUNT(name),
    CONSTRAINT ticket_purchase_order_fk FOREIGN KEY (order_id) REFERENCES PURCHASE_ORDER(order_id),
    CONSTRAINT ticket_sub_event_fk FOREIGN KEY (sub_event_name, event_name) REFERENCES SUB_EVENT(sub_event_name, event_name),
    CONSTRAINT ticket_seat_fk FOREIGN KEY (seat_number, line_number, sector_name, stage_name, venue_name) 
        REFERENCES SEAT(seat_number, line_number, sector_name, stage_name, venue_name),
    CONSTRAINT ticket_seat_status_fk FOREIGN KEY(seat_status) REFERENCES SEAT_STATUS(seat_status)
);

CREATE TABLE CART_ITEM (
    sub_event_name CHAR(100 CHAR),
    event_name CHAR(100 CHAR),
    seat_number NUMBER(3),
    line_number NUMBER(3),
    sector_name CHAR(30 CHAR),
    stage_name CHAR(30 CHAR),
    venue_name CHAR(50 CHAR),
    username CHAR(30 CHAR),
    PRIMARY KEY (sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name, username),
    CONSTRAINT cart_ticket_fk FOREIGN KEY (sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name) REFERENCES TICKET (sub_event_name, event_name, seat_number, line_number, sector_name, stage_name, venue_name),
    CONSTRAINT cart_customer_fk FOREIGN KEY (username) REFERENCES CUSTOMER (username)
);