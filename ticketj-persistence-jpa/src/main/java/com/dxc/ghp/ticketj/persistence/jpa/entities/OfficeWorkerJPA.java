package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.persistence.entities.Office;
import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;
import com.dxc.ghp.ticketj.persistence.types.Phone;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * JPA implementation of Office worker entity.
 */
@Entity
@Table(name = "OFFICE_WORKER")
@NamedQuery(name = "officeWorker.findAvailable",
    query = "SELECT worker FROM OfficeWorkerJPA AS worker WHERE worker.workingPlace IS NULL order by worker.username")
public class OfficeWorkerJPA extends UserJPA implements OfficeWorker {

    @ManyToOne(targetEntity = OfficeJPA.class)
    @JoinColumn(name = "office_name")
    private Office workingPlace;

    /**
     * Initializes OfficeWorkerJPA entity with specified characteristics.
     *
     * @param credentials
     *            The customer credentials. Must not be null.
     * @param personName
     *            The PersonName of the worker. Must not be null.
     * @param email
     *            The email of the worker. Must not be null.
     * @param phone
     *            The phone of the worker. Must not be null.
     * @param address
     *            The address of the worker. Must not be null.
     * @param role
     *            The role of the worker. Must not be null.
     * @param workingPlace
     *            The employment place of worker. Can be null.
     */
    public OfficeWorkerJPA(final Credentials credentials, final PersonName personName, final String email,
                           final Phone phone, final UnrestrictedAddress address, final UserRole role,
                           final Office workingPlace) {
        super(credentials, personName, email, phone, address, role);

        this.workingPlace = workingPlace;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected OfficeWorkerJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public Office getWorkingPlace() {
        return workingPlace;
    }

    @Override
    public void setWorkingPlace(final Office office) {
        workingPlace = office;
    }
}
