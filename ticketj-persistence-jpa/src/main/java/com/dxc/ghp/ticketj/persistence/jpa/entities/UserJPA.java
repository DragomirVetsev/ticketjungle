package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;
import com.dxc.ghp.ticketj.persistence.entities.User;
import com.dxc.ghp.ticketj.persistence.types.Phone;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * JPA implementation of User entity.
 */
@Entity
@Table(name = "SYSTEM_USER")
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({
    @NamedQuery(name = "user.findAll", query = "SELECT user FROM UserJPA AS user order by user.username"),
    @NamedQuery(name = "user.findByCredentials",
        query = "SELECT user FROM UserJPA AS user WHERE user.username = :username AND user.password = :password")
})
public class UserJPA implements User {

    /**
     * The maximum size of the User, username.
     */
    public static final int USERNAME_SIZE = 30;

    @Id
    @Column(name = "username")
    private String username;
    private String password;

    @Embedded
    @AttributeOverride(name = "givenName", column = @Column(name = "first_name"))
    @AttributeOverride(name = "familyName", column = @Column(name = "last_name"))
    private PersonNameJPA personName;

    private String email;

    @ManyToOne(targetEntity = PhoneCodeJPA.class)
    @JoinColumn(name = "phone_code")
    private PhoneCode phoneCode;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Embedded
    @AttributeOverride(name = "street", column = @Column(name = "address_street"))
    @AttributeOverride(name = "postCode", column = @Column(name = "address_post_code"))
    private AddressJPA address;

    @Column(name = "address_city")
    private String city;

    @Enumerated(EnumType.STRING)
    @Column
    private UserRole role;

    /**
     * Initializes UserJpa entity with specified characteristics.
     *
     * @param credentials
     *            The user credentials. Must not be null.
     * @param personName
     *            The PersonName of the user. Must not be null.
     * @param email
     *            The email of the user. Must not be null.
     * @param phone
     *            The phone of the user. Must not be null.
     * @param address
     *            The address of the user. Must not be null.
     * @param role
     *            The role of the user. Must not be null.
     */
    @SuppressWarnings("nls")
    public UserJPA(final Credentials credentials, final PersonName personName, final String email, final Phone phone,
                   final UnrestrictedAddress address, final UserRole role) {
        assert credentials != null : "User credentials should not be null!";

        assert personName != null : "PersonName name should not be null!";
        assert personName.givenName() != null : "Given name city should not be null!";
        assert personName.familyName() != null : "Family name city should not be null!";

        assert email != null : "Email should not be null!";
        assert phone.code() != null : "Phone code should not be null!";
        assert phone.number() != null : "Phone number should not be null!";
        assert address != null : "Address should not be null";
        assert role != null : "Role should not be null";

        this.username = credentials.username();
        this.password = credentials.password();
        this.personName = new PersonNameJPA(personName.givenName(), personName.familyName());
        this.email = email;
        this.phoneNumber = phone.number();
        this.phoneCode = phone.code();
        this.address = new AddressJPA(address.street(), address.postCode());
        this.city = address.city();
        this.role = role;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected UserJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getUsername() {
        return username.stripTrailing();
    }

    @Override
    public PersonName getPersonName() {
        return new PersonName(personName.getGivenName(), personName.getFamilyName());
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public Phone getPhone() {
        return new Phone(phoneCode, phoneNumber);
    }

    @Override
    public UnrestrictedAddress getAddress() {
        return new UnrestrictedAddress(address.getStreet(), city.stripTrailing(), address.getPostCode());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public UserRole getRole() {
        return role;
    }

    @Override
    public void changeAddress(final UnrestrictedAddress newAddress) {
        assertAddress(newAddress);

        this.address = new AddressJPA(newAddress.street(), newAddress.postCode());
        this.city = newAddress.city();
    }

    @Override
    public void changePhone(final Phone newPhone) {
        assertPhone(newPhone);

        phoneNumber = newPhone.number();
        phoneCode = newPhone.code();
    }

    @SuppressWarnings("nls")
    @Override
    public void changePassword(final String newPassword) {
        assert newPassword != null : "Password of user must not be null";

        password = newPassword;
    }

    @SuppressWarnings("nls")
    private static void assertAddress(final UnrestrictedAddress address) {
        assert address != null : "Address should not be empty!";

        assert address.postCode() != null : "The post code of the venue's address must not be null!";
        assert address.street() != null : "The street of the venue's address must not be null!";
        assert address.city() != null : "The city of the venue's address must not be null!";
    }

    @SuppressWarnings("nls")
    private static void assertPhone(final Phone phone) {
        assert phone != null : "Phone must not be null!";

        assert phone.code() != null : "Phone code must not be null!";
        assert phone.number() != null : "Phone number must not be null!";
    }
}
