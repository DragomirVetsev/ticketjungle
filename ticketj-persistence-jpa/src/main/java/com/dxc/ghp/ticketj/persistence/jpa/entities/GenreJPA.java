package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.util.Objects;

import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.persistence.entities.Genre;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * JPA implementation of {@link Genre} entity.
 */
@Entity
@Table(name = "GENRE")
@NamedQuery(name = "genre.findAll", query = "SELECT genre FROM GenreJPA AS genre")
public class GenreJPA implements Genre {

    @Id
    @Column(name = "genre_type", length = Constants.GENRE_MAX_LENGTH)
    private String type;

    /**
     * Construct a GenreJPA entity with with the specified characteristics.
     *
     * @param genreType
     *            The genre type of the genre entity. Must not be null.
     */
    @SuppressWarnings("nls")
    public GenreJPA(final String genreType) {
        assert genreType != null && !genreType.isEmpty() : "Genre type must not be null or empty";

        this.type = genreType;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * used by application code.
     */
    protected GenreJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getType() {
        return type.stripTrailing();
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    @Override
    public boolean equals(final Object obj) {
        return (obj instanceof final GenreJPA that) && Objects.equals(type, that.type);
    }

}
