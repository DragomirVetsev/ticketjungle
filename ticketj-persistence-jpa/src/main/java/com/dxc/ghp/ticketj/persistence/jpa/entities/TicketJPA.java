package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.CartItem;
import com.dxc.ghp.ticketj.persistence.entities.Discount;
import com.dxc.ghp.ticketj.persistence.entities.Order;
import com.dxc.ghp.ticketj.persistence.entities.Seat;
import com.dxc.ghp.ticketj.persistence.entities.SeatStatus;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.entities.Ticket;
import com.dxc.ghp.ticketj.persistence.jpa.formulas.Formulas;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.PostLoad;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

/**
 * JPA implementation of {@link Ticket}.
 */
@Entity
@Table(name = "TICKET")
@NamedQuery(name = "ticket.findTicketWithTicketId", query = "SELECT ticket FROM TicketJPA ticket "
    + "WHERE ticket.id.subEventId = :subEventId AND ticket.id.seatId = :seatId")
public class TicketJPA implements Ticket {

    @EmbeddedId
    private TicketId id;

    // When the entity's EmbeddedId consists of other EmbeddedIds, interface field
    // mapping does not work. Until another solution is found, concrete JPA
    // implementation will be used together with a down cast in the
    // constructor.
    @MapsId("subEventId")
    @ManyToOne(targetEntity = SubEventJPA.class)
    @JoinColumn(name = "sub_event_name", referencedColumnName = "sub_event_name")
    @JoinColumn(name = "event_name", referencedColumnName = "event_name")
    private SubEventJPA subEvent;

    // When the entity's EmbeddedId consists of other EmbeddedIds, interface field
    // mapping does not work. Until another solution is found, concrete JPA
    // implementation will be used together with a down cast in the
    // constructor.
    @MapsId("seatId")
    @ManyToOne(targetEntity = SeatJPA.class)
    @JoinColumn(name = "seat_number", referencedColumnName = "seat_number")
    @JoinColumn(name = "line_number", referencedColumnName = "line_number")
    @JoinColumn(name = "sector_name", referencedColumnName = "sector_name")
    @JoinColumn(name = "stage_name", referencedColumnName = "stage_name")
    @JoinColumn(name = "venue_name", referencedColumnName = "venue_name")
    private SeatJPA seat;

    @Transient
    private CartItem cartItem;

    @ManyToOne(targetEntity = SeatStatusJPA.class)
    @JoinColumn(name = "seat_status")
    private SeatStatus seatStatus;

    @ManyToOne(targetEntity = OrderJPA.class)
    @JoinColumn(name = "order_id")
    private Order order;

    @ManyToOne(targetEntity = DiscountJPA.class)
    private Discount discount;

    @Transient
    private double ticketPrice;

    /**
     * Construct Ticket JPA with all of its characteristics.
     *
     * @param subEvent
     *            The sub-event of the Ticket. Cannot be null.
     * @param order
     *            The order of Ticket. Can be null.
     * @param seat
     *            The seat of ticket. Must not be null;
     * @param seatStatus
     *            The seat status of ticket. Must not be null.
     * @param discount
     *            The discount of ticket. Must not be null.
     * @param cartItem
     *            the cart item, when the ticket is reserved. Can be null.
     */
    @SuppressWarnings("nls")
    public TicketJPA(final SubEvent subEvent, final Seat seat, final Order order, final SeatStatus seatStatus,
                     final Discount discount, final CartItem cartItem) {
        assert seat != null : "Seat must not be null!";
        assert seatStatus != null : "Seat status must not be null!";
        assert discount != null : "Discount must not be null!";
        assert subEvent != null : "Sub-event must not be null!";
        assert seat instanceof SeatJPA : "Seat must be an instance of SeatJPA";
        assert subEvent instanceof SubEventJPA : "Sub event must be an instance of SubEventJPA";

        this.subEvent = SubEventJPA.class.cast(subEvent);
        this.seat = SeatJPA.class.cast(seat);
        this.id = new TicketId(this.seat.getId(),
            new SubEventId(subEvent.getId().name(), subEvent.getId().eventName()));
        this.order = order;
        this.seatStatus = seatStatus;
        this.discount = discount;
        this.cartItem = cartItem;
        calculateAndSetTicketPrice();
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * used by application code.
     */
    protected TicketJPA() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * @return the ticket id.
     */
    public TicketId getId() {
        return id;
    }

    @Override
    public Seat getSeat() {
        return seat;
    }

    @Override
    public SeatStatus getSeatStatus() {
        return seatStatus;
    }

    @Override
    public void setSeatStatus(final SeatStatus seatStatus) {
        this.seatStatus = seatStatus;
    }

    @Override
    public Order getOrder() {
        return order;
    }

    @Override
    public Discount getDiscount() {
        return discount;
    }

    @Override
    public SubEvent getSubEvent() {
        return subEvent;
    }

    @Override
    public double getTicketPrice() {
        return ticketPrice;
    }

    @PostLoad
    private void calculateAndSetTicketPrice() {
        ticketPrice = ticketPriceCalculation();
    }

    private double ticketPriceCalculation() {
        return Formulas.calculateDiscountedPrice(subEvent.getTicketPrice(), discount.getPercentage());
    }

    @Override
    public CartItem getCartItem() {
        return cartItem;
    }

    @Override
    public void changeSeatStatus(final SeatStatus newSeatStatus) {
        // TODO refactor getStatus(). getName?

        this.seatStatus = newSeatStatus;

    }

    @Override
    public void setOrder(final Order order) {
        this.order = order;

    }

    @Override
    public void setDiscount(final Discount discount) {
        this.discount = discount;

    }

}
