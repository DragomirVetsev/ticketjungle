package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.Review;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

/**
 * JPA implementation of Review entity.
 */

@Entity
@Table(name = "REVIEW")
public class ReviewJPA implements Review {

    @EmbeddedId
    private ReviewId id;

    @MapsId("username")
    @ManyToOne(targetEntity = CustomerJPA.class)
    @JoinColumn(name = "username", insertable = false, updatable = false)
    private Customer customer;

    // When the entity's EmbeddedId consists of other EmbeddedIds, interface field
    // mapping does not work. Until another solution is found, concrete JPA
    // implementation will be used together with a down cast in the
    // constructor.
    @MapsId("subEventId")
    @ManyToOne(targetEntity = SubEventJPA.class)
    @JoinColumn(name = "sub_event_name", referencedColumnName = "sub_event_name")
    @JoinColumn(name = "event_name", referencedColumnName = "event_name")
    private SubEventJPA subEvent;

    @Column(name = "review_comment")
    private String comment;

    @Column(name = "review_score")
    private Integer score;

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected ReviewJPA() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Initialize a ReviewJPA entity with the specified characteristics.
     *
     * @param customer
     *            is who want to see his list of Reviews. Can not be null.
     * @param subEvent
     *            the specific subEvent of the review. Can not be null.
     * @param comment
     *            that customer give to specific event. Can be null if the customer
     *            does not comment the event, but give a score.
     * @param score
     *            that customer give to specific event. Can be null if the customer
     *            does not score the event, but give a comment.
     */
    @SuppressWarnings("nls")
    public ReviewJPA(final Customer customer, final SubEvent subEvent, final String comment, final Integer score) {
        assert customer != null : "The username of Customer should not be empty!";
        assert subEvent != null : "The subEvent should not be empty!";
        assert score != null && comment != null : "The review must have score, comment or both.";

        this.id = new ReviewId(customer.getUsername(),
            new SubEventId(subEvent.getName(), subEvent.getEvent().getName()));
        this.customer = customer;
        this.subEvent = SubEventJPA.class.cast(subEvent);
        this.score = score;
        this.comment = comment;
    }

    @Override
    public Customer getCustomer() {
        return customer;
    }

    @Override
    public SubEvent getSubEvent() {
        return subEvent;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public Integer getScore() {
        return score;
    }

}
