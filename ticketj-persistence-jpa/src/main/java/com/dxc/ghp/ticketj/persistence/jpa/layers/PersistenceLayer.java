package com.dxc.ghp.ticketj.persistence.jpa.layers;

import java.util.Map;

import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistry;
import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistryImpl;
import com.dxc.ghp.ticketj.persistence.jpa.repositories.CustomerRepositoryJPA;
import com.dxc.ghp.ticketj.persistence.jpa.repositories.EventRepositoryJPA;
import com.dxc.ghp.ticketj.persistence.jpa.repositories.OfficeRepositoryJPA;
import com.dxc.ghp.ticketj.persistence.jpa.repositories.OfficeWorkerRepositoryJPA;
import com.dxc.ghp.ticketj.persistence.jpa.repositories.ReferentialRepositoryJPA;
import com.dxc.ghp.ticketj.persistence.jpa.repositories.UserRepositoryJPA;
import com.dxc.ghp.ticketj.persistence.repositories.CustomerRepository;
import com.dxc.ghp.ticketj.persistence.repositories.EventRepository;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeRepository;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeWorkerRepository;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;
import com.dxc.ghp.ticketj.persistence.repositories.UserRepository;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.jpa.TransactionHandlerJPA;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

/**
 * Manages transaction-related components by initializing EntityManagerFactory
 * and ServiceRegistry. Provides access to repositories through the
 * PersistenceLayer.
 */
public final class PersistenceLayer {

    private final EntityManagerFactory entityManagerFactory;
    private final ServiceRegistry<EntityManager> serviceRegistry;

    /**
     * Constructs a PersistenceLayer with default properties.
     */
    public PersistenceLayer() {
        this(Map.of());
    }

    /**
     * Constructs a PersistenceLayer with the specified entity manager properties.
     *
     * @param entityManagerProperties
     *            The properties to configure the EntityManagerFactory.
     */
    @SuppressWarnings("nls")
    public PersistenceLayer(final Map<String, String> entityManagerProperties) {
        entityManagerFactory = Persistence.createEntityManagerFactory("ticketj-jpa-hibernate", entityManagerProperties);
        serviceRegistry = serviceRegister();
    }

    /**
     * Releases all resources associated with the PersistenceLayer by closing for
     * example EntityManagerFactory, if it is open.
     */
    public void release() {
        if (entityManagerFactory != null && entityManagerFactory.isOpen()) {
            entityManagerFactory.close();
        }
    }

    /**
     * Creates a new TransactionHandler using the initialized Persistence unit
     * properties.
     *
     * @return A new TransactionHandler instance.
     */
    public TransactionHandler createTransactionHandler() {
        return new TransactionHandlerJPA(entityManagerFactory, serviceRegistry);
    }

    @SuppressWarnings("static-method")
    private ServiceRegistry<EntityManager> serviceRegister() {
        final ServiceRegistry<EntityManager> repositoryRegistry = new ServiceRegistryImpl<>();
        repositoryRegistry.register(EventRepository.class, EventRepositoryJPA::new);
        repositoryRegistry.register(OfficeRepository.class, OfficeRepositoryJPA::new);
        repositoryRegistry.register(OfficeWorkerRepository.class, OfficeWorkerRepositoryJPA::new);
        repositoryRegistry.register(UserRepository.class, UserRepositoryJPA::new);
        repositoryRegistry.register(CustomerRepository.class, CustomerRepositoryJPA::new);
        repositoryRegistry.register(ReferentialRepository.class, ReferentialRepositoryJPA::new);

        return repositoryRegistry;
    }
}
