package com.dxc.ghp.ticketj.persistence.jpa.entities;

import jakarta.persistence.Embeddable;

/**
 * Embedded person name for JPA entities
 */
@Embeddable
public class PersonNameJPA {
    private String givenName;
    private String familyName;

    /**
     * Constructs embedded PersonName for JPA entities
     *
     * @param givenName
     *            the give name of the person. Must not be null.
     * @param familyName
     *            the family name of the person. Must not be null.
     */
    @SuppressWarnings("nls")
    public PersonNameJPA(final String givenName, final String familyName) {
        assert givenName != null : "Given name must not be null";
        assert familyName != null : "Family name must not be null";

        this.givenName = givenName;
        this.familyName = familyName;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * used by application code.
     */
    protected PersonNameJPA() {
        // Needed by JPA.
    }

    /**
     * Retrieves given name of person.
     *
     * @return The given name of person. Cannot be null.
     */
    public String getGivenName() {
        return givenName;
    }

    /**
     * Retrieves family name of the person.
     *
     * @return The family name of the person.
     */
    public String getFamilyName() {
        return familyName;
    }
}
