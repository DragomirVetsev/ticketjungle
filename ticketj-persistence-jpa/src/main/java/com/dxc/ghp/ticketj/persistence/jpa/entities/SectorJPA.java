package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dxc.ghp.ticketj.persistence.entities.Row;
import com.dxc.ghp.ticketj.persistence.entities.Sector;
import com.dxc.ghp.ticketj.persistence.entities.Stage;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * JPA implementation of Sector entity.
 */

@Entity
@Table(name = "SECTOR")
public class SectorJPA implements Sector {

    @EmbeddedId
    private SectorStageId id;

    // When the entity's EmbeddedId consists of other EmbeddedIds, interface field
    // mapping does not work. Until another solution is found, concrete JPA
    // implementation will be used together with a down cast in the
    // constructor.
    @MapsId("stageVenueId")
    @ManyToOne(targetEntity = StageJPA.class)
    @JoinColumn(name = "venue_name", referencedColumnName = "venue_name")
    @JoinColumn(name = "stage_name", referencedColumnName = "stage_name")
    private StageJPA stage;

    @OneToMany(targetEntity = RowJPA.class, mappedBy = "sector")
    private List<Row> rows;

    /**
     * Initialize a SectorJPA entity with the specified characteristics.
     *
     * @param sectorName
     *            The name of the Sector. Must not be null.
     * @param stage
     *            The {@link Stage} to which the sector belongs. Must not be null.
     */
    @SuppressWarnings("nls")
    public SectorJPA(final String sectorName, final Stage stage) {
        assert sectorName != null : "Name must not be null!";
        assert stage != null : "Stage must not be null!";

        id = new SectorStageId(sectorName, new StageVenueId(stage.getName(), stage.getVenue().getName()));
        this.stage = StageJPA.class.cast(stage);
        rows = new ArrayList<>();
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected SectorJPA() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves the Sector's ID.
     *
     * @return The ID of the Sector. Cannot be null.
     */
    public SectorStageId getId() {
        return id;
    }

    @Override
    public String getName() {
        return id.getSectorName().stripTrailing();
    }

    @Override
    public Stage getStage() {
        return stage;
    }

    @Override
    public List<Row> getRows() {
        return Collections.unmodifiableList(rows);
    }
}
