package com.dxc.ghp.ticketj.persistence.jpa.converters;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.logging.Logger;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

/**
 * Converts Instant into Timestamp and the opposite.
 */
@Converter(autoApply = true)
public final class InstantConverter implements AttributeConverter<Instant, Timestamp> {
    private static final Logger LOGGER = Logger.getLogger(InstantConverter.class.getName());

    @SuppressWarnings("nls")
    @Override
    public Timestamp convertToDatabaseColumn(final Instant instant) {
        LOGGER.fine(() -> String.format("Converting instant %s to timestamp.", instant.toString()));
        return instant != null ? Timestamp.from(instant) : null;
    }

    @SuppressWarnings("nls")
    @Override
    public Instant convertToEntityAttribute(final Timestamp timestamp) {
        LOGGER.fine(() -> String.format("Converting timpstamp %s to instant.", timestamp.toString()));
        return timestamp != null ? timestamp.toInstant() : null;
    }

}
