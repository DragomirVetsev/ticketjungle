package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Performer;
import com.dxc.ghp.ticketj.persistence.entities.Review;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.entities.Ticket;
import com.dxc.ghp.ticketj.persistence.jpa.formulas.Formulas;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.PostLoad;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

/**
 * JPA implementation of SubEvent entity.
 */
@Entity
@Table(name = "SUB_EVENT")
@NamedQuery(name = "subEvent.findAllOrderByTime",
    query = "SELECT subEvent FROM SubEventJPA subEvent ORDER BY subEvent.startTime DESC")
@NamedQuery(name = "subEvent.findAllOrderByPopularity",
    query = "SELECT subEvent FROM SubEventJPA subEvent JOIN subEvent.tickets ticket "
        + "WHERE ticket.seatStatus.status = 'ACQUIRED            ' GROUP BY subEvent ORDER BY COUNT(ticket) DESC")
@NamedQuery(name = "subEvent.findByDetails",
    query = "SELECT subEvent FROM SubEventJPA subEvent JOIN subEvent.stage stage "
        + "where subEvent.startTime = :startTime "
        + "AND stage.id.stageName = :stageName AND stage.id.venueName = :venueName")
@NamedQuery(name = "subEvent.findSubEventFromWishlist",
    query = "SELECT se FROM SubEventJPA se JOIN se.customers c WHERE c.username = :username AND se.id = :subEventId")
public class SubEventJPA implements SubEvent {

    @EmbeddedId
    private SubEventId id;

    @MapsId("eventName")
    @ManyToOne(targetEntity = EventJPA.class)
    @JoinColumn(name = "event_name")
    private Event event;

    private String description;

    @Column(name = "ticket_price")
    private double ticketPrice;

    @Column(name = "start_datetime")
    private Instant startTime;

    // When the entity's EmbeddedId consists of other EmbeddedIds, interface field
    // mapping does not work. Until another solution is found, concrete JPA
    // implementation will be used together with a down cast in the
    // constructor.
    @ManyToOne(targetEntity = StageJPA.class)
    @JoinColumn(name = "stage_name_id", referencedColumnName = "stage_name")
    @JoinColumn(name = "venue_name_id", referencedColumnName = "venue_name")
    private StageJPA stage;

    @OneToMany(targetEntity = ReviewJPA.class, mappedBy = "subEvent")
    private List<Review> reviews;

    @OrderBy("sector_name ASC, line_number ASC, seat_number ASC")
    @OneToMany(targetEntity = TicketJPA.class, mappedBy = "subEvent")
    private List<Ticket> tickets;

    @ManyToMany(targetEntity = PerformerJPA.class)
    @JoinTable(name = "PERFORMER_SUB_EVENT", joinColumns = {
        @JoinColumn(name = "sub_event_name", referencedColumnName = "sub_event_name"),
        @JoinColumn(name = "event_name", referencedColumnName = "event_name")
    }, inverseJoinColumns = {
        @JoinColumn(name = "performer_name", referencedColumnName = "performer_name")
    })
    private List<Performer> performers;

    @ManyToMany(mappedBy = "wishlist")
    private List<CustomerJPA> customers;

    @Transient
    private ReviewScoreAndCount reviewScoreAndCount;

    /**
     * Initialize a SubEventJPA entity with the specified characteristics.
     *
     * @param name
     *            of the sub-event. Must not be null.
     * @param event
     *            is the name of the Event which contains this sub-event. Must not
     *            be null.
     * @param stage
     *            The {@link Stage} where the sub-event is held. Must not be null.
     * @param description
     *            of the sub-event. Must not be null.
     * @param ticketPrice
     *            of the sub-event. Must not be negative.
     * @param startTime
     *            of the sub-event. Must not be null.
     * @param performerNames
     *            of the sub-event.
     */
    @SuppressWarnings("nls")
    public SubEventJPA(final String name, final Event event, final String description, final double ticketPrice,
                       final Instant startTime, final Stage stage, final List<String> performerNames) {
        assert name != null : "Sub-event name should not be empty!";
        assert event != null : "Event name should not be empty!";
        assert description != null : "Description should not be empty!";
        assert ticketPrice >= 0 : "Ticket price should not be negative!";
        assert startTime != null : "Start date and time should not be empty!";
        assert stage != null : "Stage should not be empty!";

        this.id = new SubEventId(name, event.getName());
        this.event = event;
        this.description = description;
        this.ticketPrice = ticketPrice;
        this.startTime = startTime;
        this.stage = StageJPA.class.cast(stage);
        this.reviews = new ArrayList<>();
        this.tickets = new ArrayList<>();
        this.performers = performerNames != null
            ? performerNames
                .stream()
                .map(performerName -> new PerformerJPA(performerName, description))
                .collect(Collectors.toList())
            : new ArrayList<>();

        this.customers = new ArrayList<>();
        calculateThenSetReviewScoreAndCount();
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected SubEventJPA() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves the ID of the sub-event.
     *
     * @return the ID of the sub-event. Cannot be null.
     */
    @Override
    public PerformanceId getId() {
        return new PerformanceId(id.getName(), id.getEventName());
    }

    @Override
    public String getName() {
        return id.getName();
    }

    @Override
    public Event getEvent() {
        return event;
    }

    @Override
    public String getDescription() {
        return description.stripTrailing();
    }

    @Override
    public double getTicketPrice() {
        return ticketPrice;
    }

    @Override
    public Instant getStartTime() {
        return startTime;
    }

    @Override
    public Stage getStage() {
        return stage;
    }

    @Override
    public List<Review> getReviews() {
        return Collections.unmodifiableList(reviews);
    }

    @Override
    public List<Ticket> getTickets() {
        return Collections.unmodifiableList(tickets);
    }

    @Override
    public List<Performer> getPerformers() {
        return Collections.unmodifiableList(performers);
    }

    @Override
    public List<Customer> getCustomers() {
        return Collections.unmodifiableList(customers);
    }

    @Override
    public ReviewScoreAndCount getReviewScoreAndCount() {
        return reviewScoreAndCount;
    }

    @PostLoad
    private void calculateThenSetReviewScoreAndCount() {
        reviewScoreAndCount = reviewScoreAndCountCalculate();
    }

    private ReviewScoreAndCount reviewScoreAndCountCalculate() {
        return Formulas.reviewScoreAndCountCalculate(reviews.stream().map(Review::getScore).toList());
    }

    @Override
    public void setTicketPrice(final double ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    @Override
    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = StageJPA.class.cast(stage);
    }

    @Override
    public void setStartTime(final Instant startTime) {
        this.startTime = startTime;
    }

    @Override
    public void setPerformers(final List<Performer> performers) {
        this.performers = List.copyOf(performers);
    }
}
