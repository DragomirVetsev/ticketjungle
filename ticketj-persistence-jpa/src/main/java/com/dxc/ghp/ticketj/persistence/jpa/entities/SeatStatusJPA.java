package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.SeatStatus;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * JPA implementation of {@link SeatStatus}.
 */
@Entity
@Table(name = "SEAT_STATUS")
@NamedQuery(name = "seatstatus.findAvailableStatus",
    query = "SELECT seatstatus FROM SeatStatusJPA seatstatus WHERE seatstatus.id = :name")
public class SeatStatusJPA implements SeatStatus {
    /**
     * The maximum size of seat status name.
     */
    public static final int MAX_SEAT_STATUS_SIZE = 20;

    @Id
    @Column(name = "seat_status")
    private String status;

    /**
     * Constructs SeatStatusJPA entity with the given characteristics.
     *
     * @param status
     *            The status of the seat. Must not be null or empty.
     */
    @SuppressWarnings("nls")
    public SeatStatusJPA(final String status) {
        assert status != null && !status.isEmpty() : "Status must not be null or empty!";

        this.status = status;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * used by application code.
     */
    protected SeatStatusJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getStatus() {
        return status.stripTrailing();
    }

    @Override
    public void setStatus(final String status) {
        this.status = status;
    }

}
