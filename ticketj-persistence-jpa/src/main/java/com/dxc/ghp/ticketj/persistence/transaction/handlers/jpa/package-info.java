/**
 * Contains all JPA implementations of transaction handlers.
 */
package com.dxc.ghp.ticketj.persistence.transaction.handlers.jpa;
