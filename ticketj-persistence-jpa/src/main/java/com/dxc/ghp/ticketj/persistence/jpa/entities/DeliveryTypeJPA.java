package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * JPA implementation of {@link DeliveryType}.
 */
@Entity
@Table(name = "DELIVERY_TYPE")
public class DeliveryTypeJPA implements DeliveryType {
    /**
     * The maximum size of delivery type name.
     */
    public static final int MAX_DELIVERY_TYPE_SIZE = 30;

    @Id
    @Column(name = "delivery_type")
    private String name;

    /**
     * Construct a DeliveryTypeJPA entity with with the specified characteristics.
     *
     * @param name
     *            The delivery type of the genre entity. Must not be null or empty.
     */
    @SuppressWarnings("nls")
    public DeliveryTypeJPA(final String name) {
        assert name != null && !name.isEmpty() : "Name of delivery type must not be null or empty!";

        this.name = name;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * used by application code.
     */
    protected DeliveryTypeJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getName() {
        return name.stripTrailing();
    }

}
