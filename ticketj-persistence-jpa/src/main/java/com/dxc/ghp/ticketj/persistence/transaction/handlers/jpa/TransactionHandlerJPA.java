package com.dxc.ghp.ticketj.persistence.transaction.handlers.jpa;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;

import com.dxc.ghp.ticketj.misc.exceptions.EntityExistsException;
import com.dxc.ghp.ticketj.misc.exceptions.InvalidCredentialsException;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProviderImpl;
import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistry;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.persistence.types.Database;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceException;

/**
 * JPA implementation of {@link TransactionHandler}.
 */
public final class TransactionHandlerJPA implements TransactionHandler {

    private static final Logger LOGGER = Logger.getLogger(TransactionHandlerJPA.class.getName());

    private final EntityManagerFactory entityManagerFactory;
    private final ServiceRegistry<EntityManager> serviceRegistry;
    private final Database currentDatabase;

    /**
     * Constructs a new JPA {@link TransactionHandler}. Injects
     * {@link EntityManagerFactory}.
     *
     * @param entityManagerFactory
     *            used to create an instance of {@link EntityManager} per request.
     *            Class does not become owner of the Entity manager factory and will
     *            not close it. Must not be null.
     * @param serviceRegistry
     *            used to create an instance of {@link ServiceRegistry} per request.
     *            Must not be null.
     */
    @SuppressWarnings("nls")
    public TransactionHandlerJPA(final EntityManagerFactory entityManagerFactory,
                                 final ServiceRegistry<EntityManager> serviceRegistry) {
        assert entityManagerFactory != null : "Entity Manager Factory must not be null!";
        assert serviceRegistry != null : "Service Registry must not be null!";

        this.entityManagerFactory = entityManagerFactory;
        this.serviceRegistry = serviceRegistry;
        this.currentDatabase = extractDatabase(entityManagerFactory);
    }

    @SuppressWarnings("nls")
    @Override
    public <R> R executeInTransaction(final Function<ServiceProvider, R> function) {
        try (EntityManager manager = entityManagerFactory.createEntityManager()) {
            final EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            LOGGER.fine("Transaction started!");
            try {
                final ServiceProvider serviceProvider = new ServiceProviderImpl<>(manager, serviceRegistry);
                final R result = function.apply(serviceProvider);
                transaction.commit();
                LOGGER.fine("Transaction commited successfully!");
                return result;
            } finally {
                if (transaction.isActive()) {
                    LOGGER.warning("Transaction is about to be rolled back!");
                    transaction.rollback();
                }
            }
        }
    }

    @SuppressWarnings("nls")
    @Override
    public <EntityInfo, R> R executeInTransaction(final Function<ServiceProvider, R> function,
                                                  final Class<?> entityClass, final EntityInfo additionalInfo) {
        assert entityClass != null : "Entity class must not be null";
        assert additionalInfo != null : "Aditional info must not be null";

        try {
            return executeInTransaction(function);
        } catch (final PersistenceException e) {
            checkForUniqueConstraintViolation(e, entityClass, additionalInfo);
            checkForInvalidCredentials(e, entityClass, additionalInfo);
            throw e;
        }
    }

    private <EntityInfo> void checkForUniqueConstraintViolation(final PersistenceException e,
                                                                final Class<?> entityClass, final EntityInfo entityId) {
        for (Throwable cause = e; cause != null; cause = cause.getCause()) {
            if (primaryKeyConstraintViolation(cause)) {
                throw new EntityExistsException(entityClass, entityId, e);
            }
        }
    }

    private boolean primaryKeyConstraintViolation(final Throwable cause) {
        return cause instanceof final SQLIntegrityConstraintViolationException integrityException
            && extractPrimaryViolationSqlStateCode().equals(integrityException.getSQLState())
            && extractPrimaryViolationErrorCode() == integrityException.getErrorCode();
    }

    @SuppressWarnings("static-method")
    private <EntityInfo> void checkForInvalidCredentials(final PersistenceException e, final Class<?> entityClass,
                                                         final EntityInfo message) {
        for (Throwable cause = e; cause != null; cause = cause.getCause()) {
            if (cause instanceof NoResultException) {
                throw new InvalidCredentialsException(entityClass, message, e);
            }
        }
    }

    @SuppressWarnings("nls")
    private String extractPrimaryViolationSqlStateCode() {
        return switch (currentDatabase) {
            case ORACLE -> "23000";
            case HSQLDB -> "23505";
        };
    }

    private int extractPrimaryViolationErrorCode() {
        return switch (currentDatabase) {
            case ORACLE -> 1;
            case HSQLDB -> -104;
        };
    }

    @SuppressWarnings("nls")
    private static Database extractDatabase(final EntityManagerFactory entityManagerFactory) {
        final Object currentDatabase = entityManagerFactory.getProperties().get("database.in.use");
        return Database.valueOf(currentDatabase.toString());
    }

    @SuppressWarnings("nls")
    @Override
    public void executeInTransaction(final Consumer<ServiceProvider> function) {
        try (EntityManager manager = entityManagerFactory.createEntityManager()) {
            final EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            LOGGER.fine("Transaction started!");
            try {
                final ServiceProvider serviceProvider = new ServiceProviderImpl<>(manager, serviceRegistry);
                function.accept(serviceProvider);
                transaction.commit();
                LOGGER.fine("Transaction commited successfully!");
            } finally {
                if (transaction.isActive()) {
                    LOGGER.warning("Transaction is about to be rolled back!");
                    transaction.rollback();
                }
            }
        }

    }
}
