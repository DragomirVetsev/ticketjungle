package com.dxc.ghp.ticketj.persistence.jpa.repositories;

import java.util.List;

import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.persistance.jpa.utilities.Padding;
import com.dxc.ghp.ticketj.persistence.entities.User;
import com.dxc.ghp.ticketj.persistence.jpa.entities.UserJPA;
import com.dxc.ghp.ticketj.persistence.repositories.UserRepository;

import jakarta.persistence.EntityManager;

/**
 * JPA implementation of {@link UserRepository User persistent repository}.
 */
public final class UserRepositoryJPA extends BaseRepositoryJPA implements UserRepository {

    /**
     * Constructs JPA implementation of User Repository. Injects
     * {@link EntityManager}.
     *
     * @param entityManager
     *            used to execute all persistence operations. Must not be null.
     */
    public UserRepositoryJPA(final EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("nls")
    @Override
    public List<User> findAllUsers() {
        return entityManager.createNamedQuery("user.findAll", User.class).getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public User findUserByCredentials(final Credentials credentials) {
        final String paddedUsername = Padding.padRight(credentials.username(), UserJPA.USERNAME_SIZE);
        final String password = credentials.password();

        return entityManager
            .createNamedQuery("user.findByCredentials", UserJPA.class)
            .setParameter("username", paddedUsername)
            .setParameter("password", password)
            .getSingleResult();
    }
}
