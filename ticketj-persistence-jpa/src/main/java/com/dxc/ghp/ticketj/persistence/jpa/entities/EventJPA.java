package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;

/**
 * JPA implementation of Event entity.
 */
@Entity
@Table(name = "EVENT")
@NamedQuery(name = "event.findAll", query = "SELECT event FROM EventJPA AS event order by event.name")
public class EventJPA implements Event {

    @Id
    private String name;

    private String description;

    @ManyToOne(targetEntity = GenreJPA.class)
    @JoinColumn(name = "genre_type")
    private Genre genre;

    @OrderBy("start_datetime DESC")
    @OneToMany(targetEntity = SubEventJPA.class, mappedBy = "event")
    private List<SubEvent> subEvents;

    /**
     * Initialize a EventJPA entity with the specified characteristics.
     *
     * @param name
     *            The name of the event. Must not be null or empty.
     * @param description
     *            The description of the event. Must not be null or empty.
     * @param genre
     *            The genre of the event. Must not be null.
     */
    @SuppressWarnings("nls")
    public EventJPA(final String name, final String description, final Genre genre) {
        assert name != null && !name.isEmpty() : "Name should not be null or empty!";
        assert description != null && !description.isEmpty() : "Description should not be null or empty!";
        assert genre != null : "Genre should not be empty!";

        this.name = name;
        this.description = description;
        this.genre = genre;
        this.subEvents = new ArrayList<>();
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected EventJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getName() {
        return name.stripTrailing();
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public Genre getGenre() {
        return genre;
    }

    @Override
    public List<SubEvent> getSubEvents() {
        return Collections.unmodifiableList(subEvents);
    }

    @Override
    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public void setGenre(final Genre genre) {
        this.genre = genre;
    }
}
