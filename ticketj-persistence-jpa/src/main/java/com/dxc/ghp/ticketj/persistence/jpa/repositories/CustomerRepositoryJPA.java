package com.dxc.ghp.ticketj.persistence.jpa.repositories;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.persistance.jpa.utilities.Padding;
import com.dxc.ghp.ticketj.persistence.entities.CartItem;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;
import com.dxc.ghp.ticketj.persistence.entities.Order;
import com.dxc.ghp.ticketj.persistence.entities.PaymentType;
import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.jpa.entities.CustomerJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.OrderJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.SubEventId;
import com.dxc.ghp.ticketj.persistence.repositories.CustomerRepository;
import com.dxc.ghp.ticketj.persistence.types.Phone;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

/**
 * JPA implementation of {@link CustomerRepository Customer persistent
 * repository}.
 */
@SuppressWarnings("nls")
public final class CustomerRepositoryJPA extends BaseRepositoryJPA implements CustomerRepository {

    /**
     * Constructs JPA implementation of Customer Repository. Injects
     * {@link EntityManager}.
     *
     * @param entityManager
     *            used to execute all persistence operations. Must not be null.
     */
    public CustomerRepositoryJPA(final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public Optional<Customer> findCustomer(final String username) {
        assert username != null : "Username must not be null!";

        final String paddedUsername = Padding.padRight(username, Constants.USERNAME_MAX_LENGTH);
        return Optional.ofNullable(entityManager.find(CustomerJPA.class, paddedUsername));
    }

    @Override
    public Order createOrderWithDeliveryAddress(final PurchaseStatus purchaseStatus, final Customer customer,
                                                final DeliveryType deliveryType, final PaymentType paymentType,
                                                final Instant date, final UnrestrictedAddress deliveryAddress) {
        assert purchaseStatus != null : "purchaseStatus must not be null!";
        assert customer != null : "customer must not be null!";
        assert deliveryType != null : "deliveryType must not be null!";
        assert paymentType != null : "paymentType must not be null!";
        assert date != null : "date must not be null!";
        assert deliveryAddress != null : "deliveryAddress must not be null!";

        final Order order = new OrderJPA(purchaseStatus, customer, deliveryType, paymentType, date, deliveryAddress);
        entityManager.persist(order);
        return order;
    }

    @Override
    public Order createOrderWithoutDeliveryAddress(final PurchaseStatus purchaseStatus, final Customer customer,
                                                   final DeliveryType deliveryType, final PaymentType paymentType,
                                                   final Instant date) {

        assert purchaseStatus != null : "Purchase status must not be null!";
        assert customer != null : "Customer must not be null!";
        assert deliveryType != null : "Delivery type must not be null!";
        assert paymentType != null : "Payment type must not be null!";
        assert date != null : "Date must not be null!";
        final Order order = new OrderJPA(purchaseStatus, customer, deliveryType, paymentType, date);
        entityManager.persist(order);
        return order;
    }

    @Override
    public Customer createCustomer(final Credentials cridentials, final PersonName personName, final String email,
                                   final Phone phone, final UnrestrictedAddress address, final LocalDate birthday) {
        assert cridentials != null : "Customer`s cridentials must not be null!";
        assert cridentials.username() != null : "Customer`s username must not be null!";
        assert cridentials.password() != null : "Customer`s password must not be null!";
        assert personName != null : "Customer`s Personal name must not be null!";
        assert personName.givenName() != null : "Customer`s given name must not be null!";
        assert personName.familyName() != null : "Customer`s family name must not be null!";
        assert email != null : "Customer`s email must not be null!";
        assert phone != null : "Customer`s phone must not be null!";
        assert phone.code() != null : "Customer`s phone code must not be null!";
        assert phone.number() != null : "Customer`s phone number must not be null!";
        assert address != null : "Customer`s address must not be null!";
        assert address.city() != null : "Customer`s address city must not be null!";
        assert address.street() != null : "Customer`s address street must not be null!";
        assert address.postCode() != null : "Customer`s address post code must not be null!";
        assert birthday != null : "Customer`s birthday must not be null!";

        final Customer customer = new CustomerJPA(cridentials, personName, email, phone, address, UserRole.CUSTOMER,
            birthday);

        entityManager.persist(customer);

        return customer;
    }

    @Override
    public Optional<SubEvent> findSubEventFromWishlist(final String username, final PerformanceId performanceId) {
        final String paddedUsername = Padding.padRight(username, Constants.USERNAME_MAX_LENGTH);
        final String paddedSubEventName = Padding.padRight(performanceId.name(), Constants.SUB_EVENT_NAME_MAX_LENGTH);
        final String paddedEventName = Padding.padRight(performanceId.eventName(), Constants.EVENT_MAX_LENGTH);
        final SubEventId subEventId = new SubEventId(paddedSubEventName, paddedEventName);

        final TypedQuery<SubEvent> query = entityManager
            .createNamedQuery("subEvent.findSubEventFromWishlist", SubEvent.class);
        query.setParameter("username", paddedUsername);
        query.setParameter("subEventId", subEventId);
        return query.getResultList().stream().findFirst();
    }

    @Override
    public void removeCartIthem(final CartItem cartItem) {
        assert cartItem != null : "cartItem must not be null!";
        entityManager.remove(cartItem);
    }

}
