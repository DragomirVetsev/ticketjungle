package com.dxc.ghp.ticketj.persistence.jpa.converters;

import java.time.LocalTime;
import java.util.logging.Logger;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

/**
 * Converts LocalTime into String and the opposite.
 */
@Converter
public final class LocalTimeConverter implements AttributeConverter<LocalTime, String> {
    private static final Logger LOGGER = Logger.getLogger(LocalTimeConverter.class.getName());

    @SuppressWarnings("nls")
    @Override
    public String convertToDatabaseColumn(final LocalTime attribute) {
        LOGGER.fine(() -> String.format("Converting localTime %s to string.", attribute.toString()));

        return attribute != null ? attribute.toString() : null;
    }

    @SuppressWarnings("nls")
    @Override
    public LocalTime convertToEntityAttribute(final String dbData) {
        LOGGER.fine(() -> String.format("Converting string %s to localTime.", dbData));

        return dbData != null ? LocalTime.parse(dbData) : null;
    }

}
