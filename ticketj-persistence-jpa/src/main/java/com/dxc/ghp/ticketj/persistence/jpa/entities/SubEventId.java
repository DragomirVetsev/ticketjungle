package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.io.Serializable;
import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.SubEvent;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;

/**
 * Embedded id for {@link SubEvent}
 */
@Embeddable
public class SubEventId implements Serializable {
    private static final long serialVersionUID = 966618123734191305L;

    @Column(name = "sub_event_name")
    private String name;

    @JoinColumn(name = "event_name")
    private String eventName;

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    public SubEventId() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Constructs an object representing the composite key for {@link SubEvent}.
     *
     * @param name
     *            is the sub event name. Must not be null.
     * @param eventName
     *            is the name of the event containing the sub event. Must not be
     *            null.
     */
    @SuppressWarnings("nls")
    public SubEventId(final String name, final String eventName) {
        assert name != null : "Sub-event name should not be null.";
        assert eventName != null : "Event name should not be null.";

        this.name = name;
        this.eventName = eventName;
    }

    /**
     * Retrieves sub-event name.
     *
     * @return The sub-event name. Cannot be null.
     */
    public String getName() {
        return name.stripTrailing();
    }

    /**
     * Retrieves the event name containing the sub-event.
     *
     * @return The event name containing the sub-event. Cannot be null.
     */
    public String getEventName() {
        return eventName.stripTrailing();
    }

    @Override
    public boolean equals(final Object other) {
        return (other instanceof final SubEventId that)
            && Objects.equals(name, that.name)
            && Objects.equals(eventName, that.eventName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, eventName);
    }
}
