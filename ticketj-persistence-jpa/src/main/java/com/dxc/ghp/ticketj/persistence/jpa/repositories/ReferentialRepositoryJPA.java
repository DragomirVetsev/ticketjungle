/**
 *
 */
package com.dxc.ghp.ticketj.persistence.jpa.repositories;

import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.persistance.jpa.utilities.Padding;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;
import com.dxc.ghp.ticketj.persistence.entities.Discount;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.PaymentType;
import com.dxc.ghp.ticketj.persistence.entities.Performer;
import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;
import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;
import com.dxc.ghp.ticketj.persistence.entities.SeatStatus;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.Venue;
import com.dxc.ghp.ticketj.persistence.jpa.entities.CityJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.DeliveryTypeJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.DiscountJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.EventJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.GenreJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.PaymentTypeJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.PerformerJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.PhoneCodeJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.PurchaseStatusJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.SeatStatusJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.StageJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.StageVenueId;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;

import jakarta.persistence.EntityManager;

/**
 *
 */
public class ReferentialRepositoryJPA extends BaseRepositoryJPA implements ReferentialRepository {

    /**
     * Constructs JPA implementation of Venue, Genre, City Repository. Injects
     * {@link EntityManager}.
     *
     * @param entityManager
     *            used to execute all persistence operations. Must not be null.
     */
    public ReferentialRepositoryJPA(final EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("nls")
    @Override
    public List<Venue> findAllVenues() {
        return entityManager.createNamedQuery("venue.findAll", Venue.class).getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public List<Genre> findAllGenres() {
        return entityManager.createNamedQuery("genre.findAll", Genre.class).getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<Event> findEvent(final String eventName) {
        assert eventName != null : "Event name must not be null!";

        final String paddedText = Padding.padRight(eventName, Constants.EVENT_NAME_MAX_LENGTH);

        return Optional.ofNullable(entityManager.find(EventJPA.class, paddedText));
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<Stage> findStage(final String stage, final String venue) {
        assert stage != null : "Stage name must not be null!";
        assert venue != null : "Venue name must not be null!";

        return Optional
            .ofNullable(entityManager
                .find(StageJPA.class, new StageVenueId(Padding.padRight(stage, Constants.STAGE_MAX_LENGTH),
                    Padding.padRight(venue, Constants.VENUE_MAX_LENGTH))));
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<Genre> findGenre(final String genreType) {
        assert genreType != null : "Genre Type must not be null!";

        final String paddedText = Padding.padRight(genreType, Constants.GENRE_MAX_LENGTH);

        return Optional.ofNullable(entityManager.find(GenreJPA.class, paddedText));
    }

    @SuppressWarnings("nls")
    @Override
    public List<City> findAllCities() {
        return entityManager.createNamedQuery("city.findAll", City.class).getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<City> findCity(final String cityName) {
        assert cityName != null : "City Name must not be null!";

        return Optional
            .ofNullable(entityManager.find(CityJPA.class, Padding.padRight(cityName, Constants.USERNAME_MAX_LENGTH)));
    }

    @SuppressWarnings("nls")
    @Override
    public List<PhoneCode> findAllPhoneCodes() {
        return entityManager.createNamedQuery("phoneCode.findAll", PhoneCode.class).getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<PhoneCode> findPhoneCode(final String phoneCode) {
        assert phoneCode != null : "Phone code must not be null!";

        final String paddedText = Padding.padRight(phoneCode, PhoneCodeJPA.MAX_PHONE_CODE_SIZE);

        return Optional.ofNullable(entityManager.find(PhoneCodeJPA.class, paddedText));
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<PurchaseStatus> findPurchaseStatus(final String purchaseStatus) {
        assert purchaseStatus != null : "purchaseStatus must not be null!";

        final String paddedPurchaseStatus = Padding
            .padRight(purchaseStatus, PurchaseStatusJPA.MAX_PURCHASE_STATUS_SIZE);

        return Optional.ofNullable(entityManager.find(PurchaseStatusJPA.class, paddedPurchaseStatus));
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<DeliveryType> findDeliveryType(final String deliveryType) {
        assert deliveryType != null : "deliveryType must not be null!";

        final String paddedDeliveryType = Padding.padRight(deliveryType, DeliveryTypeJPA.MAX_DELIVERY_TYPE_SIZE);
        return Optional.ofNullable(entityManager.find(DeliveryTypeJPA.class, paddedDeliveryType));
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<PaymentType> findPaymentType(final String paymentType) {
        assert paymentType != null : "paymentType must not be null!";

        final String paddedPaymentType = Padding.padRight(paymentType, PaymentTypeJPA.MAX_PAYMENT_TYPE_SIZE);
        return Optional.ofNullable(entityManager.find(PaymentTypeJPA.class, paddedPaymentType));
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<SeatStatus> findSeatStatus(final String seatStatus) {
        assert seatStatus != null : "seatStatus must not be null!";

        final String paddedSeatStatus = Padding.padRight(seatStatus, SeatStatusJPA.MAX_SEAT_STATUS_SIZE);
        return Optional.ofNullable(entityManager.find(SeatStatusJPA.class, paddedSeatStatus));
    }

    @SuppressWarnings("nls")
    @Override
    public Performer createPerformer(final String name, final String description) {
        assert name != null : "Performer's name must not be null!";
        assert description != null : "Performer's description must not be null!";

        final Performer performer = new PerformerJPA(name, description);
        entityManager.persist(performer);
        return performer;
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<Performer> findPerformer(final String performerName) {
        assert performerName != null : "Performer Name must not be null!";

        return Optional
            .ofNullable(entityManager
                .find(PerformerJPA.class, Padding.padRight(performerName, Constants.PERFORMER_MAX_LENGTH)));
    }

    @SuppressWarnings("nls")
    @Override
    public List<Performer> findAllPerformers() {
        return entityManager.createNamedQuery("performer.findAll", Performer.class).getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<Discount> findDiscount(final String discountName) {
        assert discountName != null : "discountName must not be null!";

        final String paddedDiscountName = Padding.padRight(discountName, DiscountJPA.MAX_DISCOUNT_NAME_SIZE);
        return Optional.ofNullable(entityManager.find(DiscountJPA.class, paddedDiscountName));
    }
}
