/**
 * Contains utility classes used by Persistence JPA.
 */
package com.dxc.ghp.ticketj.persistance.jpa.utilities;
