package com.dxc.ghp.ticketj.persistence.jpa.formulas;

import java.util.Collection;

import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;

/**
 * Utility class that contains formulas for calculations
 */
public final class Formulas {

    private static final int GROSS_PRICE_PERCENTAGE = 100;

    private Formulas() {
        // Empty constructor to prevent instantiation of the class.
    }

    /**
     * Calculates a list of Integer's average and their non null count.
     *
     * @param reviews
     *            is the list of Integers we calculate for.
     * @return {@ReviewScoreAndCount} containing the calculated score and count of
     *         the list of reviews.
     */
    public static ReviewScoreAndCount reviewScoreAndCountCalculate(final Collection<Integer> reviews) {
        if (reviews.isEmpty()) {
            return new ReviewScoreAndCount(0, 0);
        }

        double sum = 0;
        int count = 0;
        for (final Integer arg : reviews) {
            if (arg != null) {
                sum = sum + arg.intValue();
                count++;
            }
        }
        if (count != 0) {
            return new ReviewScoreAndCount(sum / count, count);
        }
        return new ReviewScoreAndCount(0, 0);
    }

    /**
     * Used to calculate price after discount.
     *
     * @param grossPrice
     *            The total price. Must be {@code >= 0}!
     * @param discount
     *            The discount percentage. Must be {@code >= 0 && <= 100}.
     * @return The price after discount. Cannot be {@code < 0}.
     */
    @SuppressWarnings("nls")
    public static double calculateDiscountedPrice(final double grossPrice, final double discount) {
        assert grossPrice >= 0 : "Gross Price must not be a negative number!";
        assert discount >= 0 && discount <= GROSS_PRICE_PERCENTAGE : "Discount must be in range [0, 100]!";

        return grossPrice * (GROSS_PRICE_PERCENTAGE - discount) / GROSS_PRICE_PERCENTAGE;
    }
}
