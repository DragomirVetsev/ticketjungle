package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.io.Serializable;
import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.Review;

import jakarta.persistence.Embeddable;

/**
 * Embedded id for {@link Review}.
 */
@Embeddable
public class ReviewId implements Serializable {

    private static final long serialVersionUID = 6381952649743440402L;

    private String username;

    private SubEventId subEventId;

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    public ReviewId() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * @param username
     *            of customer who want to see his reviews. Must not be null.
     * @param subEventId
     *            is the subEvent Id. Must not be null.
     */
    @SuppressWarnings("nls")
    public ReviewId(final String username, final SubEventId subEventId) {
        assert username != null : "Username should not be null.";
        assert subEventId != null : "Sub Event id should not be null.";

        this.username = username;
        this.subEventId = subEventId;

    }

    /**
     * Retrieves the Customer's username.
     *
     * @return the username of the customer. Can not be null.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Retrieves the SubEvent ID.
     *
     * @return the SubEvent ID. Can not be null.
     */
    public SubEventId getSubEventId() {
        return subEventId;
    }

    @Override
    public boolean equals(final Object other) {
        return (other instanceof final ReviewId that)
            && Objects.equals(username, that.username)
            && Objects.equals(subEventId, that.subEventId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, subEventId);
    }

}
