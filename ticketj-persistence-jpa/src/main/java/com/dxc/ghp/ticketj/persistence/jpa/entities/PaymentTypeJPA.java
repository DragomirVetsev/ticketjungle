package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.PaymentType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * JPA implementation of {@link PaymentType}.
 */
@Entity
@Table(name = "PAYMENT_TYPE")
public class PaymentTypeJPA implements PaymentType {
    /**
     * The maximum size of payment type name.
     */
    public static final int MAX_PAYMENT_TYPE_SIZE = 30;
    @Id
    @Column(name = "payment_type")
    private String name;

    /**
     * Constructs PaymentTypeJPA with all its characteristics.
     *
     * @param name
     *            The name of the Payment entity. Must not be null or empty.
     */
    @SuppressWarnings("nls")
    public PaymentTypeJPA(final String name) {
        assert name != null && !name.isEmpty() : "Name of payment type must not be null or empty!";

        this.name = name;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * used by application code.
     */
    protected PaymentTypeJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getName() {
        return name.stripTrailing();
    }

}
