package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.io.Serializable;
import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.Stage;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

/**
 * Embedded id for {@link Stage}.
 */
@Embeddable
public class StageVenueId implements Serializable {

    private static final long serialVersionUID = -8844916265285005490L;

    @Column(name = "stage_name")
    private String stageName;
    @Column(name = "venue_name")
    private String venueName;

    /**
     * Constructs object representing composite key for {@link Stage}.
     *
     * @param stageName
     *            The name of the stage. Must not be null.
     * @param venueName
     *            The name of the venue. Must not be null.
     */
    @SuppressWarnings("nls")
    public StageVenueId(final String stageName, final String venueName) {
        assert stageName != null : "Stage's name must not be null.";
        assert venueName != null : "Venue's name must not be null.";

        this.venueName = venueName;
        this.stageName = stageName;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    public StageVenueId() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves Stage's name.
     *
     * @return Stage's name.
     */
    public String getStageName() {
        return stageName;
    }

    /**
     * Retrieves Venue's name.
     *
     * @return Venue's name.
     */
    public String getVenueName() {
        return venueName;
    }

    @Override
    public boolean equals(final Object other) {
        return (other instanceof final StageVenueId that)
            && Objects.equals(venueName, that.venueName)
            && Objects.equals(stageName, that.stageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(venueName, stageName);
    }
}
