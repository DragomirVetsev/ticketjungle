/**
 *
 */
package com.dxc.ghp.ticketj.persistence.jpa.repositories;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.jpa.entities.EventJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.StageJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.SubEventJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.VenueJPA;

import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

/**
 * Class used for building criteria query for searching events in
 * {@link EventRepositoryJPA}.
 */
final class EventSearchBuilder {
    private EventSearchBuilder() {
    }

    /**
     * @param period
     *            The {@link Instant} start and end to witch the search will be
     *            performed.
     * @param city
     *            Wear the Event will be held.
     * @param genre
     *            Of the Event
     * @param entityManager
     *            Of the current repository.
     * @return Criteria query used for searching events.
     */
    @SuppressWarnings("nls")
    public static CriteriaQuery<Event> buildQuery(final Period<Instant> period, final City city, final Genre genre,
                                                  final EntityManager entityManager) {
        final CriteriaBuilder eventCriteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Event> eventCriteria = eventCriteriaBuilder.createQuery(Event.class);
        final Root<EventJPA> event = eventCriteria.from(EventJPA.class);
        final Join<EventJPA, SubEventJPA> subEvent = event.join("subEvents");
        final Join<SubEventJPA, StageJPA> stage = subEvent.join("stage");
        final Join<StageJPA, VenueJPA> venue = stage.join("venue");

        final Predicate[] whereEventPredicates = wherePredicates(period, city, genre, eventCriteriaBuilder, event,
                                                                 subEvent, venue);

        return eventCriteria
            .select(event)
            .where(whereEventPredicates)
            .orderBy(eventCriteriaBuilder.asc(event.get("name")));
    }

    private static Predicate[] wherePredicates(final Period<Instant> period, final City city, final Genre genre,
                                               final CriteriaBuilder eventCriteriaBuilder, final Root<EventJPA> event,
                                               final Join<EventJPA, SubEventJPA> joinSubEvent,
                                               final Join<StageJPA, VenueJPA> joinVenue) {
        final List<Predicate> whereEventPredicates = new ArrayList<>();
        addPeriodPredicate(whereEventPredicates, period, eventCriteriaBuilder, joinSubEvent);
        addCityPredicates(whereEventPredicates, city, eventCriteriaBuilder, joinVenue);
        addGenrePredicate(whereEventPredicates, genre, eventCriteriaBuilder, event);
        return whereEventPredicates.toArray(Predicate[]::new);
    }

    @SuppressWarnings("nls")
    private static void addGenrePredicate(final Collection<Predicate> predicates, final Genre genre,
                                          final CriteriaBuilder eventCriteriaBuilder, final Root<EventJPA> event) {
        if (genre != null) {
            predicates.add(eventCriteriaBuilder.equal(event.get("genre"), genre));
        }
    }

    @SuppressWarnings("nls")
    private static void addCityPredicates(final Collection<Predicate> predicates, final City city,
                                          final CriteriaBuilder eventCriteriaBuilder,
                                          final Join<StageJPA, VenueJPA> joinVenue) {
        if (city != null) {
            predicates.add(eventCriteriaBuilder.equal(joinVenue.get("city"), city));
        }
    }

    @SuppressWarnings("nls")
    private static void addPeriodPredicate(final Collection<Predicate> predicates, final Period<Instant> period,
                                           final CriteriaBuilder eventCriteriaBuilder,
                                           final Join<EventJPA, SubEventJPA> subEvent) {
        if (period.startTime() == null && period.endTime() == null) {
            return;
        }
        final var startTime = subEvent.<Instant>get("startTime");
        if (period.startTime() != null && period.endTime() != null) {
            predicates.add(eventCriteriaBuilder.between(startTime, period.startTime(), period.endTime()));
        } else if (period.startTime() != null) {
            predicates.add(eventCriteriaBuilder.greaterThanOrEqualTo(startTime, period.startTime()));
        } else if (period.endTime() != null) {
            predicates.add(eventCriteriaBuilder.lessThanOrEqualTo(startTime, period.endTime()));
        } else {
            assert false;
        }
    }
}
