package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.Row;
import com.dxc.ghp.ticketj.persistence.entities.Seat;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

/**
 * JPA implementation of Seat entity.
 */
@Entity
@Table(name = "SEAT")
public class SeatJPA implements Seat {

    @EmbeddedId
    private SeatRowId id;

    // When the entity's EmbeddedId consists of other EmbeddedIds, interface field
    // mapping does not work. Until another solution is found, concrete JPA
    // implementation will be used together with a down cast in the
    // constructor.
    @MapsId("rowSectorId")
    @ManyToOne(targetEntity = RowJPA.class)
    @JoinColumn(name = "sector_name", referencedColumnName = "sector_name")
    @JoinColumn(name = "venue_name", referencedColumnName = "venue_name")
    @JoinColumn(name = "stage_name", referencedColumnName = "stage_name")
    @JoinColumn(name = "line_number", referencedColumnName = "line_number")
    private RowJPA row;

    /**
     * Initialize a SeatJPA entity with the specified characteristics.
     *
     * @param number
     *            The number of the seat. Must not be below 0.
     * @param row
     *            The {@link Row} to which the seat belongs. Must not be null.
     */
    @SuppressWarnings("nls")
    public SeatJPA(final int number, final Row row) {
        assert number > 0 : "Seat's number must be above 0.";
        assert row != null : "Row should not be empty!";

        id = new SeatRowId(number, new RowSectorId(row.getNumber(), new SectorStageId(row.getSector().getName(),
            new StageVenueId(row.getSector().getStage().getName(), row.getSector().getStage().getVenue().getName()))));
        this.row = RowJPA.class.cast(row);
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected SeatJPA() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves the Seat's ID.
     *
     * @return The ID of the Seat. Cannot be null.
     */
    public SeatRowId getId() {
        return id;
    }

    @Override
    public int getNumber() {
        return id.getSeatNumber();
    }

    @Override
    public Row getRow() {
        return row;
    }
}
