package com.dxc.ghp.ticketj.persistence.jpa.repositories;

import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.persistance.jpa.utilities.Padding;
import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;
import com.dxc.ghp.ticketj.persistence.jpa.entities.OfficeWorkerJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.UserJPA;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeWorkerRepository;

import jakarta.persistence.EntityManager;

/**
 * JPA implementation of {@link OfficeWorkerRepository Office worker persistent
 * repository}.
 */
public final class OfficeWorkerRepositoryJPA extends BaseRepositoryJPA implements OfficeWorkerRepository {

    /**
     * Constructs JPA implementation of Office Worker Repository. Injects
     * {@link EntityManager}.
     *
     * @param entityManager
     *            used to execute all persistence operations. Must not be null.
     */
    public OfficeWorkerRepositoryJPA(final EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("nls")
    @Override
    public List<OfficeWorker> findAllAvailableWorkers() {
        return entityManager.createNamedQuery("officeWorker.findAvailable", OfficeWorker.class).getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<OfficeWorker> findWorker(final String username) {
        assert username != null : "Username must not be null";

        final String paddedUsername = Padding.padRight(username, UserJPA.USERNAME_SIZE);

        return Optional.ofNullable(entityManager.find(OfficeWorkerJPA.class, paddedUsername));
    }

}
