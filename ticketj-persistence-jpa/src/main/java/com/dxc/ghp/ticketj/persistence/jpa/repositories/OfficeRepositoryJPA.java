package com.dxc.ghp.ticketj.persistence.jpa.repositories;

import java.time.LocalTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.persistance.jpa.utilities.Padding;
import com.dxc.ghp.ticketj.persistence.entities.Office;
import com.dxc.ghp.ticketj.persistence.jpa.entities.OfficeJPA;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeRepository;
import com.dxc.ghp.ticketj.persistence.types.Phone;
import com.dxc.ghp.ticketj.persistence.types.RestrictedAddress;
import com.dxc.ghp.ticketj.persistence.types.UniqueOfficeLocation;

import jakarta.persistence.EntityManager;

/**
 * JPA implementation of {@link OfficeRepository Office persistent repository}.
 */
public final class OfficeRepositoryJPA extends BaseRepositoryJPA implements OfficeRepository {
    /**
     * Constructs JPA implementation of Event Repository. Injects
     * {@link EntityManager}.
     *
     * @param entityManager
     *            used to execute all persistence operations. Must not be null.
     */
    public OfficeRepositoryJPA(final EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("nls")
    @Override
    public List<Office> findAllOffices() {
        return entityManager.createNamedQuery("office.findAll", Office.class).getResultList();
    }

    @Override
    public Office createOffice(final String officeName, final RestrictedAddress address, final Phone phone,
                               final String email, final Period<LocalTime> workingHours) {
        final Office office = new OfficeJPA(officeName, address, phone, email, workingHours);
        entityManager.persist(office);
        return office;
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<Office> findOffice(final String officeId) {
        assert officeId != null : "Office id must not be null!";

        final String paddedUsername = Padding.padRight(officeId, OfficeJPA.MAX_OFFICE_NAME_LENGTH);

        return Optional.ofNullable(entityManager.find(OfficeJPA.class, paddedUsername));
    }

    @SuppressWarnings("nls")
    @Override
    public boolean emailExists(final String email) {
        assert email != null : "Email must not be null!";

        final List<Office> resultList = entityManager
            .createNamedQuery("office.findByEmail", Office.class)
            .setParameter("email", email)
            .getResultList();

        return !resultList.isEmpty();
    }

    @SuppressWarnings("nls")
    @Override
    public boolean phoneExists(final Phone phone) {
        assert phone != null : "Phone must not be null!";

        final List<Office> resultList = entityManager
            .createNamedQuery("office.findByPhone", Office.class)
            .setParameter("number", phone.number())
            .setParameter("code", phone.code())
            .getResultList();

        return !resultList.isEmpty();
    }

    @SuppressWarnings("nls")
    @Override
    public boolean addressExists(final UniqueOfficeLocation officeAddress) {
        assert officeAddress != null : "Office address mut not be null!";

        final List<Office> resultList = entityManager
            .createNamedQuery("office.findByAddress", Office.class)
            .setParameter("street", officeAddress.street())
            .setParameter("city", officeAddress.city())
            .getResultList();

        return extractResult(resultList);
    }

    private static boolean extractResult(final Collection<Office> resultList) {

        return !resultList.isEmpty();

    }
}
