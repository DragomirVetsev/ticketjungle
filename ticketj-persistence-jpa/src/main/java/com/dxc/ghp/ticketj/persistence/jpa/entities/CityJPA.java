package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.City;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * JPA implementation of City entity.
 */
@Entity
@Table(name = "CITY")
@NamedQuery(name = "city.findAll", query = "SELECT city FROM CityJPA AS city order by city.name")
public class CityJPA implements City {
    /**
     * The maximum size of the City type.
     */
    public static final int MAX_CITY_LENGTH = 30;

    @Id
    @Column(name = "city_name")
    private String name;

    /**
     * Initialize a CityJPA entity with the specified characteristics.
     *
     * @param addressCity
     *            The name of the City. Must not be null.
     */
    @SuppressWarnings("nls")
    public CityJPA(final String addressCity) {
        assert addressCity != null : "City must not be empty!";
        this.name = addressCity;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected CityJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getCityName() {
        return name.stripTrailing();
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public boolean equals(final Object obj) {
        return (obj instanceof final CityJPA that) && Objects.equals(name, that.name);
    }

}
