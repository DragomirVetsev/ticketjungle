package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;
import com.dxc.ghp.ticketj.persistence.entities.Order;
import com.dxc.ghp.ticketj.persistence.entities.PaymentType;
import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;
import com.dxc.ghp.ticketj.persistence.entities.Ticket;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;

/**
 * JPA implementation of {@link Order}.
 */
@Entity
@Table(name = "PURCHASE_ORDER")
public class OrderJPA implements Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private int id;

    @ManyToOne(targetEntity = PurchaseStatusJPA.class)
    @JoinColumn(name = "purchase_status")
    private PurchaseStatus purchaseStatus;

    @ManyToOne(targetEntity = CustomerJPA.class)
    @JoinColumn(name = "username")
    private Customer customer;

    @ManyToOne(targetEntity = DeliveryTypeJPA.class)
    @JoinColumn(name = "delivery_type")
    private DeliveryType deliveryType;

    @ManyToOne(targetEntity = PaymentTypeJPA.class)
    @JoinColumn(name = "payment_type")
    private PaymentType paymentType;

    @Column(name = "creation_date")
    private Instant creationDate;

    @Embedded
    @AttributeOverride(name = "street", column = @Column(name = "address_street"))
    @AttributeOverride(name = "postCode", column = @Column(name = "address_post_code"))
    private AddressJPA deliveryAddress;

    @Column(name = "address_city")
    private String city;

    @OrderBy("venue_name ASC, stage_name ASC, sector_name ASC, line_number ASC, seat_number ASC "
        + "sub_event_start_time ASC")
    @OneToMany(mappedBy = "order", targetEntity = TicketJPA.class)
    private List<Ticket> tickets;

    /**
     * Constructs OrderJPA with delivery address {@code id} is
     * {@literal auto-generated}).
     *
     * @param purchaseStatus
     *            The purchase status of the order. Must not be null!
     * @param customer
     *            The customer of the order. Must not be null!
     * @param deliveryType
     *            The delivery type of the order. Must not be null!
     * @param paymentType
     *            The payment type of the order. Must not be null!
     * @param date
     *            The date of the order. Must not be null!
     * @param deliveryAddress
     *            The delivery address of order. Must not be null!
     */
    @SuppressWarnings("nls")
    public OrderJPA(final PurchaseStatus purchaseStatus, final Customer customer, final DeliveryType deliveryType,
                    final PaymentType paymentType, final Instant date, final UnrestrictedAddress deliveryAddress) {
        this(purchaseStatus, customer, deliveryType, paymentType, date);

        assert deliveryAddress != null : "Delivery Address must not be null!";

        this.deliveryAddress = new AddressJPA(deliveryAddress.street(), deliveryAddress.postCode());
        this.city = deliveryAddress.city();
        tickets = new ArrayList<>();
    }

    /**
     * Constructs OrderJPA without delivery address {@code id} is
     * {@literal auto-generated}).
     *
     * @param purchaseStatus
     *            The purchase status of the order. Must not be null!
     * @param customer
     *            The customer of the order. Must not be null!
     * @param deliveryType
     *            The delivery type of the order. Must not be null!
     * @param paymentType
     *            The payment type of the order. Must not be null!
     * @param date
     *            The date of the order. Must not be null!
     */
    @SuppressWarnings("nls")
    public OrderJPA(final PurchaseStatus purchaseStatus, final Customer customer, final DeliveryType deliveryType,
                    final PaymentType paymentType, final Instant date) {
        assert purchaseStatus != null : "Purchase status must not be null!";
        assert customer != null : "Customer must not be null!";
        assert deliveryType != null : "Delivery type must not be null!";
        assert paymentType != null : "Payment type status must not be null!";

        this.purchaseStatus = purchaseStatus;
        this.customer = customer;
        this.deliveryType = deliveryType;
        this.paymentType = paymentType;
        creationDate = date;
        tickets = new ArrayList<>();
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected OrderJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public PurchaseStatus getPurchaseStatus() {
        return purchaseStatus;
    }

    @Override
    public Customer getCustomer() {
        return customer;
    }

    @Override
    public DeliveryType getDeliveryType() {
        return deliveryType;
    }

    @Override
    public PaymentType getPaymentType() {
        return paymentType;
    }

    @Override
    public Instant getCreatedDate() {
        return creationDate;
    }

    @Override
    public UnrestrictedAddress getDeliveryAddress() {
        return (deliveryAddress != null)
            ? new UnrestrictedAddress(deliveryAddress.getStreet(), city.stripTrailing(), deliveryAddress.getPostCode())
            : null;
    }

    @Override
    public List<Ticket> getTickets() {
        return Collections.unmodifiableList(tickets);
    }
}
