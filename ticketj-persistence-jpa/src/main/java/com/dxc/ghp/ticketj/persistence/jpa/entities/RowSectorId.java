package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.io.Serializable;
import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.Row;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

/**
 * Embedded id for {@link Row}
 */
@Embeddable
public class RowSectorId implements Serializable {

    private static final long serialVersionUID = -1891069191877084119L;

    @Column(name = "line_number")
    private int rowNumber;

    private SectorStageId sectorStageId;

    /**
     * Constructs object representing composite key for {@link Row}.
     *
     * @param rowNumber
     *            The number of the row. Must not be null.
     * @param sectorStageId
     *            The Sector's id. Must not be null.
     */
    @SuppressWarnings("nls")
    public RowSectorId(final int rowNumber, final SectorStageId sectorStageId) {
        assert rowNumber > 0 : "Row's number must be above 0.";
        assert sectorStageId != null : "Sector's id must not be null.";

        this.rowNumber = rowNumber;
        this.sectorStageId = sectorStageId;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    public RowSectorId() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves the number of the row.
     *
     * @return The number of the row. Cannot be below 0.
     */
    public int getLineNumber() {
        return rowNumber;
    }

    /**
     * Retrieves the Row's id.
     *
     * @return The Row's id. Cannot be null.
     */
    public SectorStageId getSectorStageId() {
        return sectorStageId;
    }

    @Override
    public boolean equals(final Object other) {
        return (other instanceof final RowSectorId that)
            && (rowNumber == that.rowNumber)
            && Objects.equals(sectorStageId, that.sectorStageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Integer.valueOf(rowNumber), sectorStageId);
    }

}
