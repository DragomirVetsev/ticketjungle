package com.dxc.ghp.ticketj.persistence.jpa.entities;

import jakarta.persistence.Embeddable;

/**
 * Embedded address for JPA entities.
 */
@Embeddable
public class AddressJPA {

    private String street;
    private String postCode;

    /**
     * Constructs embedded Address for JPA entities.
     *
     * @param street
     *            The name of the street. Must not be null.
     * @param postCode
     *            The post code. Must not be null.
     */
    @SuppressWarnings("nls")
    public AddressJPA(final String street, final String postCode) {
        assert street != null : "Street must not be empty!";
        assert postCode != null : "Post code must not be empty!";

        this.street = street;
        this.postCode = postCode;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected AddressJPA() {
        // Needed by JPA.
    }

    /**
     * Retrieves street of address.
     *
     * @return The street of address. Cannot be null.
     */
    public String getStreet() {
        return street;
    }

    /**
     * Retrieves post code of address.
     *
     * @return The post code of address. Cannot be null.
     */
    public String getPostCode() {
        return postCode;
    }

}
