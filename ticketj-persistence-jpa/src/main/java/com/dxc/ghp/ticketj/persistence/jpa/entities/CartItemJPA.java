package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.CartItem;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.Ticket;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

/*** JPA implementation of Cart entity. */

@Entity
@Table(name = "CART_ITEM")
public class CartItemJPA implements CartItem {

    @EmbeddedId
    private CartItemId id;

    // When the entity's EmbeddedId consists of other EmbeddedIds, interface
    // field
    // mapping does not work. Until another solution is found, concrete JPA
    // implementation will be used together with a down cast in the
    // constructor.
    @MapsId("ticketId")
    @OneToOne(targetEntity = TicketJPA.class)
    @JoinColumn(name = "sub_event_name", referencedColumnName = "sub_event_name")
    @JoinColumn(name = "event_name", referencedColumnName = "event_name")
    @JoinColumn(name = "seat_number", referencedColumnName = "seat_number")
    @JoinColumn(name = "line_number", referencedColumnName = "line_number")
    @JoinColumn(name = "sector_name", referencedColumnName = "sector_name")
    @JoinColumn(name = "stage_name", referencedColumnName = "stage_name")
    @JoinColumn(name = "venue_name", referencedColumnName = "venue_name")
    private TicketJPA ticket;

    @MapsId("username")
    @ManyToOne(targetEntity = CustomerJPA.class)
    @JoinColumn(name = "username", referencedColumnName = "username", insertable = false, updatable = false)
    private Customer customer;

    /**
     * Initialize a CartJPA entity with the specified characteristics.
     *
     * @param customer
     *            is who want to see his list of reserved Tickets. Can not be null.
     * @param ticket
     *            the reserved ticket. Can not be null.
     */
    @SuppressWarnings("nls")
    public CartItemJPA(final Ticket ticket, final Customer customer) {
        assert customer != null : "The username of Customer should not be null!";
        assert ticket != null : "The ticket should not be null!";

        this.ticket = TicketJPA.class.cast(ticket);
        this.customer = customer;
        this.id = new CartItemId(this.ticket.getId(), this.customer.getUsername());
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected CartItemJPA() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * @return cartitem id.
     */
    public CartItemId getId() {
        return id;
    }

    @Override
    public Ticket getTicket() {
        return ticket;
    }

    @Override
    public Customer getCustomer() {
        return customer;
    }

}
