package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;

import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

/**
 * Embedded phone for JPA entities.
 */
@Embeddable
public class PhoneJPA {
    private String phoneNumber;

    @ManyToOne(targetEntity = PhoneCodeJPA.class)
    @JoinColumn(name = "phone_code")
    private PhoneCode phoneCode;

    /**
     * Constructs embedded Phone for JPA entities.
     *
     * @param phoneNumber
     *            The phone number. Must not be null or empty.
     * @param phoneCode
     *            The phone code. Must not be null.
     */
    @SuppressWarnings("nls")
    public PhoneJPA(final String phoneNumber, final PhoneCode phoneCode) {
        assert phoneNumber != null && !phoneNumber.isEmpty() : "Phone number must not be null or empty!";
        assert phoneCode != null : "Phone code must not be null!";

        this.phoneNumber = phoneNumber;
        this.phoneCode = phoneCode;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected PhoneJPA() {
        // Needed by JPA.
    }

    /**
     * Retrieves the number of the phone.
     *
     * @return The phone number. Cannot be null.
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Retrieves the code of the phone.
     *
     * @return The phone code. Cannot be null.
     */
    public PhoneCode getPhoneCode() {
        return phoneCode;
    }

}
