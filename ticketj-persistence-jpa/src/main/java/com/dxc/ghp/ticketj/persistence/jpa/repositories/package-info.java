/**
 * Contains all JPA Implementations of persistent repositories.
 */
package com.dxc.ghp.ticketj.persistence.jpa.repositories;
