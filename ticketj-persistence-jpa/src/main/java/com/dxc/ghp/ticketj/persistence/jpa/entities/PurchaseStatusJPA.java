package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * JPA implementation of {@link PurchaseStatus}.
 */
@Entity
@Table(name = "PURCHASE_STATUS")
public class PurchaseStatusJPA implements PurchaseStatus {

    /**
     * The maximum size purchase status name.
     */
    public static final int MAX_PURCHASE_STATUS_SIZE = 30;

    @Id
    @Column(name = "purchase_status")
    private String name;

    /**
     * Constructs PurchaseStatusJPA with all its characteristics.
     *
     * @param name
     *            The name of the Purchase Status, must not be null or empty.
     */
    @SuppressWarnings("nls")
    public PurchaseStatusJPA(final String name) {
        assert name != null && !name.isEmpty() : "Name of purchase status must not be null or empty!";

        this.name = name;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * used by application code.
     */
    protected PurchaseStatusJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getName() {
        return name.stripTrailing();
    }

}
