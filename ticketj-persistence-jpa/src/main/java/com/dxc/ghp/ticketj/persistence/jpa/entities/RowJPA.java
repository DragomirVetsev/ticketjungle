package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dxc.ghp.ticketj.persistence.entities.Row;
import com.dxc.ghp.ticketj.persistence.entities.Seat;
import com.dxc.ghp.ticketj.persistence.entities.Sector;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * JPA implementation of Row entity.
 */
@Entity
@Table(name = "LINE")
public class RowJPA implements Row {

    @EmbeddedId
    private RowSectorId id;

    // When the entity's EmbeddedId consists of other EmbeddedIds, interface field
    // mapping does not work. Until another solution is found, concrete JPA
    // implementation will be used together with a down cast in the
    // constructor.
    @MapsId("sectorStageId")
    @ManyToOne(targetEntity = SectorJPA.class)
    @JoinColumn(name = "sector_name", referencedColumnName = "sector_name")
    @JoinColumn(name = "venue_name", referencedColumnName = "venue_name")
    @JoinColumn(name = "stage_name", referencedColumnName = "stage_name")
    private SectorJPA sector;

    @OneToMany(targetEntity = SeatJPA.class, mappedBy = "row")
    private List<Seat> seats;

    /**
     * Initialize a LineJPA with the specified characteristics.
     *
     * @param number
     *            The number of the row. Must not be below 0;
     * @param sector
     *            The {@link Sector} to which the row belongs. Must not be null.
     */
    @SuppressWarnings("nls")
    public RowJPA(final int number, final Sector sector) {
        assert number > 0 : "Row's number must be above 0.";
        assert sector != null : "Sector must not be empty!";

        id = new RowSectorId(number, new SectorStageId(sector.getName(),
            new StageVenueId(sector.getStage().getName(), sector.getStage().getVenue().getName())));
        this.sector = SectorJPA.class.cast(sector);
        seats = new ArrayList<>();
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected RowJPA() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves Row's ID.
     *
     * @return The ID of the Row. Cannot be null.
     */
    public RowSectorId getId() {
        return id;
    }

    @Override
    public int getNumber() {
        return id.getLineNumber();
    }

    @Override
    public Sector getSector() {
        return sector;
    }

    @Override
    public List<Seat> getSeats() {
        return Collections.unmodifiableList(seats);
    }
}
