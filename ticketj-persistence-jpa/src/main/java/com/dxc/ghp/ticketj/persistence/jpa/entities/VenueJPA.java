package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.Venue;
import com.dxc.ghp.ticketj.persistence.types.RestrictedAddress;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * JPA implementation of Venue entity.
 */
@Entity
@Table(name = "VENUE")
@NamedQuery(name = "venue.findAll", query = "SELECT venue FROM VenueJPA AS venue ORDER BY venue.name")
public class VenueJPA implements Venue {

    @Id
    @Column(name = "venue_name")
    private String name;

    @Embedded
    @AttributeOverride(name = "street", column = @Column(name = "address_street"))
    @AttributeOverride(name = "postCode", column = @Column(name = "address_post_code"))
    private AddressJPA address;

    @OneToMany(targetEntity = StageJPA.class, mappedBy = "venue")
    private List<Stage> stages;

    @ManyToOne(targetEntity = CityJPA.class)
    @JoinColumn(name = "address_city")
    private City city;

    /**
     * Initialize a VenueJPA with the specified characteristics.
     *
     * @param name
     *            The name of the venue. Must not be null.
     * @param address
     *            The {@link UnrestrictedAddress} of the Venue. Must not be null.
     * @param city
     *            The {@link City} of the Venue. Must not be null.
     */
    @SuppressWarnings("nls")
    public VenueJPA(final String name, final UnrestrictedAddress address, final City city) {
        assert name != null : "Name of venue must not be empty!";
        assert name != null : "Address of venue must not be empty!";
        assert city != null : "The city of the venue's address must not be null!";
        assert address.postCode() != null : "The post code of the venue's address must not be null!";
        assert address.street() != null : "The street of the venue's address must not be null!";
        this.name = name;
        this.address = new AddressJPA(address.street(), address.postCode());
        this.city = city;
        stages = new ArrayList<>();
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected VenueJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getName() {
        return name.stripTrailing();
    }

    @Override
    public RestrictedAddress getAddress() {
        return new RestrictedAddress(address.getStreet(), city, address.getPostCode());
    }

    @Override
    public List<Stage> getStages() {
        return Collections.unmodifiableList(stages);
    }
}
