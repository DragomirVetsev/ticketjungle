package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.io.Serializable;
import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.Ticket;

import jakarta.persistence.Embeddable;

/**
 * Embedded id of {@link Ticket}.
 */
@Embeddable
public class TicketId implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7273881322897162249L;
    private SeatRowId seatId;
    private SubEventId subEventId;

    /**
     * Constructs TicketId with all its characteristics.
     *
     * @param seatId
     *            The seat it. Must not be null.
     * @param subEventId
     *            The sub-event id. Must not be null.
     */
    @SuppressWarnings("nls")
    public TicketId(final SeatRowId seatId, final SubEventId subEventId) {
        assert seatId != null : "Seat id must not be null!";
        assert subEventId != null : "Sub-event id must not be null";

        this.seatId = seatId;
        this.subEventId = subEventId;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    public TicketId() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves the seat id of the ticket id.
     *
     * @return The seat id, cannot be null.
     */
    public SeatRowId getSeatId() {
        return seatId;
    }

    /**
     * Retrieves the sub-event id of the ticket id.
     *
     * @return The sub-event id, cannot be null.
     */
    public SubEventId getSubEventId() {
        return subEventId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(seatId, subEventId);
    }

    @Override
    public boolean equals(final Object obj) {
        return (obj instanceof final TicketId that)
            && Objects.equals(seatId, that.seatId)
            && Objects.equals(subEventId, that.subEventId);
    }

}
