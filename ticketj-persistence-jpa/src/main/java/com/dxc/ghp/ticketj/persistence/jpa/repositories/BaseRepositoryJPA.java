/**
 *
 */
package com.dxc.ghp.ticketj.persistence.jpa.repositories;

import jakarta.persistence.EntityManager;

/**
 * A base for all repository for common interactions.
 */
public abstract class BaseRepositoryJPA {
    /**
     * Used to execute all persistence operations. Is not null.
     */
    protected final EntityManager entityManager;

    /**
     * Constructs JPA implementation of Event Repository. Injects
     * {@link EntityManager}.
     *
     * @param entityManager
     *            used to execute all persistence operations. Must not be null.
     */
    @SuppressWarnings("nls")
    protected BaseRepositoryJPA(final EntityManager entityManager) {
        assert entityManager != null : "Entity Manager must not be null!";

        this.entityManager = entityManager;
    }
}
