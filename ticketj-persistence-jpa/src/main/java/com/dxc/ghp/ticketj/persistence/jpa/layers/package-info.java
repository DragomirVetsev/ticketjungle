/**
 * Contains Persistence Layer abstraction.
 */
package com.dxc.ghp.ticketj.persistence.jpa.layers;
