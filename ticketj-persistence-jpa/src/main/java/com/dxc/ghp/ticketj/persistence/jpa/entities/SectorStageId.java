package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.io.Serializable;
import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.Sector;
import com.dxc.ghp.ticketj.persistence.entities.Stage;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

/**
 * Embedded id for {@link Sector}.
 */
@Embeddable
public class SectorStageId implements Serializable {

    private static final long serialVersionUID = -7800093831424448589L;

    @Column(name = "sector_name")
    private String sectorName;

    private StageVenueId stageVenueId;

    /**
     * Constructs object representing composite key for {@link Stage}.
     *
     * @param sectorName
     *            The name of the sector. Must not be null.
     * @param stageVenueId
     *            The Stage's id. Must not be null.
     */
    @SuppressWarnings("nls")
    public SectorStageId(final String sectorName, final StageVenueId stageVenueId) {
        assert sectorName != null : "Sector's name must not be null.";
        assert stageVenueId != null : "Stage's id must not be null.";

        this.sectorName = sectorName;
        this.stageVenueId = stageVenueId;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    public SectorStageId() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves sector's name.
     *
     * @return The name of the sector. Cannot be null.
     */
    public String getSectorName() {
        return sectorName;
    }

    /**
     * Retrieves Stage's id.
     *
     * @return The Stage's id. Cannot be null.
     */
    public StageVenueId getStageVenueId() {
        return stageVenueId;
    }

    @Override
    public boolean equals(final Object other) {
        return (other instanceof final SectorStageId that)
            && Objects.equals(sectorName, that.sectorName)
            && Objects.equals(stageVenueId, that.stageVenueId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sectorName, stageVenueId);
    }
}
