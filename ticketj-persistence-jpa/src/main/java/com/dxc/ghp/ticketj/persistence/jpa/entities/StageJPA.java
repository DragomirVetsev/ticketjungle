package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dxc.ghp.ticketj.persistence.entities.Sector;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.Venue;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * JPA implementation of Stage entity.
 */
@Entity
@Table(name = "STAGE")
public class StageJPA implements Stage {

    @EmbeddedId
    private StageVenueId id;

    @MapsId("venueName")
    @ManyToOne(targetEntity = VenueJPA.class)
    @JoinColumn(name = "venue_name")
    private Venue venue;

    @OneToMany(targetEntity = SectorJPA.class, mappedBy = "stage")
    private List<Sector> sectors;

    /**
     * Initialize a StageJPA entity with the specified characteristics.
     *
     * @param stageName
     *            The name of the stage. Must not be null.
     * @param venue
     *            The {@link Venue} that the stage belongs to. Must not be null.
     */
    @SuppressWarnings("nls")
    public StageJPA(final String stageName, final Venue venue) {
        assert stageName != null : "Stage's name must not be empty!";
        assert venue != null : "Venue must not be null!";

        id = new StageVenueId(stageName, venue.getName());
        this.venue = VenueJPA.class.cast(venue);
        sectors = new ArrayList<>();
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected StageJPA() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves the Stage's ID.
     *
     * @return The ID of the Stage. Cannot be null.
     */
    public StageVenueId getId() {
        return id;
    }

    @Override
    public String getName() {
        return id.getStageName().stripTrailing();
    }

    @Override
    public Venue getVenue() {
        return venue;
    }

    @Override
    public List<Sector> getSectors() {
        return Collections.unmodifiableList(sectors);
    }

}
