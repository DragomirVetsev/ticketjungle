package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * JPA implementation of {@link PhoneCode} entity.
 */
@Entity
@Table(name = "PHONE_CODE")
@NamedQuery(name = "phoneCode.findAll",
    query = "SELECT phoneCode FROM PhoneCodeJPA AS phoneCode order by phoneCode.code")
public class PhoneCodeJPA implements PhoneCode {

    /**
     * Max Phone code size
     */
    public static final int MAX_PHONE_CODE_SIZE = 6;

    @Id
    @Column(name = "phone_code")
    private String code;

    /**
     * @param phoneCode
     *            The phone code of the {@link PhoneCode} entity. Must not be null.
     */
    @SuppressWarnings("nls")
    public PhoneCodeJPA(final String phoneCode) {
        assert phoneCode != null : "Phone code must not be null";

        code = phoneCode;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * used by application code.
     */
    protected PhoneCodeJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getValue() {
        return code.stripTrailing();
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public boolean equals(final Object obj) {
        return (obj instanceof final PhoneCodeJPA that) && Objects.equals(getValue(), that.getValue());
    }
}
