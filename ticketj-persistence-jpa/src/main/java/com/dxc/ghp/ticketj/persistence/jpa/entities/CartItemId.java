package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.io.Serializable;
import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.CartItem;

import jakarta.persistence.Embeddable;

/**
 * Embedded id of {@link CartItem}.
 */
@Embeddable
public class CartItemId implements Serializable {

    private static final long serialVersionUID = 1000238249517346405L;
    private TicketId ticketId;
    private String username;

    /**
     * Constructs CartItemId with all its characteristics.
     *
     * @param ticketId
     *            the ticket ID. Must not be null.
     * @param username
     *            of the customer. Must not be null.
     */
    @SuppressWarnings("nls")
    public CartItemId(final TicketId ticketId, final String username) {
        assert ticketId != null : "Ticket id must not be null!";
        assert username != null : "Username must not be null";

        this.ticketId = ticketId;
        this.username = username;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected CartItemId() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves the ticket id of the cart item id.
     *
     * @return The ticket id, cannot be null.
     */
    public TicketId getTicketId() {
        return ticketId;
    }

    /**
     * @return username of the customer.
     */
    public String getUsername() {
        return username;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticketId, username);
    }

    @Override
    public boolean equals(final Object obj) {
        return (obj instanceof final CartItemId that)
            && Objects.equals(ticketId, that.ticketId)
            && Objects.equals(username, that.username);
    }

}
