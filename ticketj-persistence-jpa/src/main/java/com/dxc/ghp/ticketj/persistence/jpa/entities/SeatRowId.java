package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.io.Serializable;
import java.util.Objects;

import com.dxc.ghp.ticketj.persistence.entities.Row;
import com.dxc.ghp.ticketj.persistence.entities.Seat;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

/**
 * Embedded id for {@link Seat}
 */
@Embeddable
public class SeatRowId implements Serializable {

    private static final long serialVersionUID = -3577348890441197902L;

    @Column(name = "seat_number")
    private int seatNumber;

    private RowSectorId rowSectorId;

    /**
     * Constructs object representing composite key for {@link Seat}.
     *
     * @param seatNumber
     *            The number of the seat. Must not be null.
     * @param lineSectorId
     *            The Row's id. Must not be null.
     */
    @SuppressWarnings("nls")
    public SeatRowId(final int seatNumber, final RowSectorId lineSectorId) {
        assert seatNumber > 0 : "Seat's number must be above 0!";
        assert lineSectorId != null : "Line's id must not be null!";

        this.seatNumber = seatNumber;
        this.rowSectorId = lineSectorId;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    public SeatRowId() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Retrieves the {@link Seat}'s number.
     *
     * @return The number of the seat. Cannot be below 0.
     */
    public int getSeatNumber() {
        return seatNumber;
    }

    /**
     * Retrieves the {@link Row}'s id.
     *
     * @return The Row's id. Cannot be null.
     */
    public RowSectorId getLineSectorId() {
        return rowSectorId;
    }

    @Override
    public boolean equals(final Object other) {
        return (other instanceof final SeatRowId that)
            && (seatNumber == that.seatNumber)
            && Objects.equals(rowSectorId, that.rowSectorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Integer.valueOf(seatNumber), rowSectorId);
    }
}
