/**
 * Package containing the implementation of converters used to convert entity
 * attribute into database representation.
 */
package com.dxc.ghp.ticketj.persistence.jpa.converters;
