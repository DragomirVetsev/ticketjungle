package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Office;
import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;
import com.dxc.ghp.ticketj.persistence.jpa.converters.LocalTimeConverter;
import com.dxc.ghp.ticketj.persistence.types.Phone;
import com.dxc.ghp.ticketj.persistence.types.RestrictedAddress;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

/**
 * JPA implementation of Office entity.
 */

@Entity
@Table(name = "OFFICE")
@NamedQuery(name = "office.findAll", query = "SELECT office FROM OfficeJPA AS office order by office.officeName")
@NamedQuery(name = "office.findByEmail", query = "SELECT office FROM OfficeJPA AS office where office.email = :email")
@NamedQuery(name = "office.findByPhone",
    query = "SELECT office FROM OfficeJPA AS office where office.phone.phoneNumber = :number "
        + "AND office.phone.phoneCode = :code")
@NamedQuery(name = "office.findByAddress",
    query = "SELECT office FROM OfficeJPA AS office where office.address.street = :street AND office.city = :city")
public class OfficeJPA implements Office {

    /**
     * Max office name length.
     */
    public static final int MAX_OFFICE_NAME_LENGTH = 30;

    @Id
    @Column(name = "office_name")
    private String officeName;

    @Embedded
    @AttributeOverride(name = "street", column = @Column(name = "address_street"))
    @AttributeOverride(name = "postCode", column = @Column(name = "address_post_code"))
    private AddressJPA address;

    @ManyToOne(targetEntity = CityJPA.class)
    @JoinColumn(name = "address_city")
    private City city;

    @Embedded
    @AttributeOverride(name = "phoneNumber", column = @Column(name = "phone_number"))
    @AttributeOverride(name = "phoneCode", column = @Column(name = "phone_code"))
    private PhoneJPA phone;

    @Column(name = "email")
    private String email;

    @Column(name = "opening_time")
    @Convert(converter = LocalTimeConverter.class)
    private LocalTime openingTime;

    @Column(name = "closing_time")
    @Convert(converter = LocalTimeConverter.class)
    private LocalTime closingTime;

    @OneToMany(targetEntity = OfficeWorkerJPA.class, mappedBy = "workingPlace")
    private List<OfficeWorker> employees;

    /**
     * Initialize a OfficeJPA entity with the specified characteristics.
     *
     * @param officeName
     *            The name of the office. Must not be null.
     * @param address
     *            The address of the office. Must not be null.
     * @param phone
     *            The office phone. Must not be null.
     * @param email
     *            The office email. Must not be null.
     * @param workingHours
     *            The office working hours. Must not be null.
     */
    @SuppressWarnings("nls")
    public OfficeJPA(final String officeName, final RestrictedAddress address, final Phone phone, final String email,
                     final Period<LocalTime> workingHours) {
        assert officeName != null : "Office name should not be empty!";
        assertAddress(address);
        assertPhone(phone);
        assertEmail(email);
        assertWorkingHours(workingHours);

        this.officeName = officeName;
        this.address = new AddressJPA(address.street(), address.postCode());
        this.city = address.city();
        this.phone = new PhoneJPA(phone.number(), phone.code());
        this.email = email;
        this.openingTime = workingHours.startTime();
        this.closingTime = workingHours.endTime();
        this.employees = new ArrayList<>();
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected OfficeJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getOfficeName() {
        return officeName.stripTrailing();
    }

    @Override
    public RestrictedAddress getAddress() {
        return new RestrictedAddress(address.getStreet(), city, address.getPostCode());
    }

    @Override
    public Phone getPhone() {
        return new Phone(phone.getPhoneCode(), phone.getPhoneNumber());
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public Period<LocalTime> getWorkingPeriod() {
        return new Period<>(openingTime, closingTime);
    }

    @Override
    public List<OfficeWorker> getWorkers() {
        return Collections.unmodifiableList(employees);
    }

    @Override
    public void changeAddress(final RestrictedAddress newAddress) {
        assertAddress(newAddress);

        address = new AddressJPA(newAddress.street(), newAddress.postCode());
        city = newAddress.city();
    }

    @Override
    public void changePhone(final Phone newPhone) {
        assertPhone(newPhone);

        phone = new PhoneJPA(newPhone.number(), newPhone.code());
    }

    @Override
    public void changeEmail(final String newEmail) {
        assertEmail(newEmail);

        email = newEmail;
    }

    @Override
    public void changeWorkingPeriod(final Period<LocalTime> workingPeriod) {
        assertWorkingHours(workingPeriod);

        openingTime = workingPeriod.startTime();
        closingTime = workingPeriod.endTime();
    }

    @SuppressWarnings("nls")
    @Override
    public void changeEmployees(final List<OfficeWorker> newEmployees) {
        assert newEmployees != null : "Employees must not be null";

        employees = List.copyOf(newEmployees);
    }

    @SuppressWarnings("nls")
    private static void assertAddress(final RestrictedAddress address) {
        assert address != null : "Address should not be empty!";
        assert address.postCode() != null : "The post code of the venue's address must not be null!";
        assert address.street() != null : "The street of the venue's address must not be null!";
        assert address.city() != null : "The city of the venue's address must not be null!";
    }

    @SuppressWarnings("nls")
    private static void assertPhone(final Phone phone) {
        assert phone != null : "Phone must not be null!";
        assert phone.code() != null : "Phone code must not be null!";
        assert phone.number() != null : "Phone number must not be null!";
    }

    @SuppressWarnings("nls")
    private static void assertEmail(final String email) {
        assert email != null && !email.isEmpty() : "Email must not be null or empty!";
    }

    @SuppressWarnings("nls")
    private static void assertWorkingHours(final Period<LocalTime> workingHours) {
        assert workingHours != null : "Working hours must not be null!";
        assert workingHours.startTime() != null : "Opening time must not be null!";
        assert workingHours.endTime() != null : "Closing time must not be null!";
    }
}
