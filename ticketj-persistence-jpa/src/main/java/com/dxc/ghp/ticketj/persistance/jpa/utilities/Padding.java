package com.dxc.ghp.ticketj.persistance.jpa.utilities;

/**
 * A utility class for text padding operations.
 */
public final class Padding {

    private Padding() {
        // Prevent instantiation of the class.
    }

    /**
     * Pads the given text with spaces on the right to ensure it reaches the
     * specified maximum length. If the input text's length is greater than or equal
     * to the maximum length or the maximum length is equal or less than zero, the
     * original text is returned unchanged.
     *
     * @param textToPad
     *            is the text to be padded. Must not be null.
     * @param maxLen
     *            is the desired maximum length of the resulting padded text. If it
     *            is equal or less than zero or the length of textPad is more than
     *            maxLen, the text to be padded do not change.
     * @return The padded text or the same text.
     */
    @SuppressWarnings("nls")
    public static String padRight(final String textToPad, final int maxLen) {
        assert textToPad != null : "The text to be padded must not be null!";

        if (maxLen <= 0 || textToPad.length() >= maxLen || textToPad.isEmpty()) {
            return textToPad;
        }
        return String.format("%-" + maxLen + "s", textToPad);
    }
}
