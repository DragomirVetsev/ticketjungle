package com.dxc.ghp.ticketj.persistence.jpa.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.persistence.entities.CartItem;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.Order;
import com.dxc.ghp.ticketj.persistence.entities.Review;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.types.Phone;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;

/**
 * JPA implementation of Customer entity.
 */
@Entity
@Table(name = "CUSTOMER")
@NamedQuery(name = "customer.findAll",
    query = "SELECT customer FROM CustomerJPA AS customer order by customer.username")
public class CustomerJPA extends UserJPA implements Customer {

    /**
     * The maximum size of the Customer username.
     */

    @OneToMany(targetEntity = ReviewJPA.class, mappedBy = "customer")
    @OrderBy("username ASC, sub_event_name ASC, review_score DESC")
    private List<Review> reviews;

    @Column(name = "birthday")
    private LocalDate birthday;

    @OneToMany(mappedBy = "customer", targetEntity = OrderJPA.class)
    @OrderBy("order_id ASC")
    private List<Order> orders;

    @OneToMany(mappedBy = "customer", targetEntity = CartItemJPA.class)

    @OrderBy("sub_event_name ASC, event_name ASC, "
        + "seat_number ASC, line_number ASC, sector_name ASC, stage_name ASC, venue_name ASC")
    private List<CartItem> cartItems;

    @ManyToMany(targetEntity = SubEventJPA.class)
    @JoinTable(name = "WISHLIST", joinColumns = {
        @JoinColumn(name = "username", referencedColumnName = "username")
    }, inverseJoinColumns = {
        @JoinColumn(name = "sub_event_name", referencedColumnName = "sub_event_name"),
        @JoinColumn(name = "event_name", referencedColumnName = "event_name")
    })
    @OrderBy("sub_event_name ASC, event_name ASC")
    private Set<SubEvent> wishlist;

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected CustomerJPA() {
        // Required by JPA. Must not be called by application code.
    }

    /**
     * Initializes CustomerJPA entity with specified characteristics.
     *
     * @param credentials
     *            The customer credentials. Must not be null.
     * @param personName
     *            The PersonName of the customer. Must not be null.
     * @param email
     *            The email of the customer. Must not be null.
     * @param phone
     *            The phone of the customer. Must not be null.
     * @param address
     *            The address of the customer. Must not be null.
     * @param role
     *            The role of the customer. Must not be null.
     * @param birthday
     *            The birthday date of the customer. Must not be null.
     */
    public CustomerJPA(final Credentials credentials, final PersonName personName, final String email,
                       final Phone phone, final UnrestrictedAddress address, final UserRole role,
                       final LocalDate birthday) {
        super(credentials, personName, email, phone, address, role);
        this.birthday = birthday;
        orders = new ArrayList<>();
        reviews = new ArrayList<>();
        wishlist = new HashSet<>();
        cartItems = new ArrayList<>();
    }

    @Override
    public LocalDate getBirthday() {
        return birthday;
    }

    @Override
    public List<Review> getReviews() {
        return Collections.unmodifiableList(reviews);
    }

    @Override
    public List<Order> getOrders() {
        return Collections.unmodifiableList(orders);
    }

    @Override
    public Set<SubEvent> getWishlist() {
        return Collections.unmodifiableSet(wishlist);
    }

    @Override
    public List<CartItem> getCartItems() {
        return Collections.unmodifiableList(cartItems);
    }

    @Override
    public boolean addToWishList(final SubEvent subEvent) {
        return wishlist.add(subEvent);
    }

    @Override
    public boolean removeFromWishlist(final SubEvent subEvent) {
        return wishlist.remove(subEvent);
    }

    @Override
    public boolean removeFromWishlist(final Set<SubEvent> foundSubEvents) {
        return wishlist.removeAll(foundSubEvents);
    }

}
