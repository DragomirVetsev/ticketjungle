package com.dxc.ghp.ticketj.persistence.jpa.repositories;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.persistance.jpa.utilities.Padding;
import com.dxc.ghp.ticketj.persistence.entities.CartItem;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.entities.Ticket;
import com.dxc.ghp.ticketj.persistence.jpa.entities.CartItemJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.EventJPA;
import com.dxc.ghp.ticketj.persistence.jpa.entities.SubEventId;
import com.dxc.ghp.ticketj.persistence.jpa.entities.SubEventJPA;
import com.dxc.ghp.ticketj.persistence.repositories.EventRepository;

import jakarta.persistence.EntityManager;

/**
 * JPA implementation of {@link EventRepository Event persistent repository}.
 */
public final class EventRepositoryJPA extends BaseRepositoryJPA implements EventRepository {
    /**
     * Constructs JPA implementation of Event Repository. Injects
     * {@link EntityManager}.
     *
     * @param entityManager
     *            used to execute all persistence operations. Must not be null.
     */
    public EventRepositoryJPA(final EntityManager entityManager) {
        super(entityManager);
    }

    @SuppressWarnings("nls")
    @Override
    public Event createEvent(final String name, final String description, final Genre genre) {
        assert name != null : "Event's name must not be null!";
        assert description != null : "Event's description must not be null!";
        assert genre != null : "Event's genre must not be null!";

        final Event event = new EventJPA(name, description, genre);
        entityManager.persist(event);
        return event;
    }

    @SuppressWarnings("nls")
    @Override
    public SubEvent createSubEvent(final String name, final Event event, final String description,
                                   final double ticketPrice, final Instant startTime, final Stage stage,
                                   final List<String> performers) {
        assert name != null : "Sub-event name should not be empty!";
        assert event != null : "Event name should not be empty!";
        assert description != null : "Description should not be empty!";
        assert ticketPrice >= 0 : "Ticket price should not be negative!";
        assert startTime != null : "Start date and time should not be empty!";
        assert stage != null : "Stage should not be empty!";

        final SubEvent subEvent = new SubEventJPA(name, event, description, ticketPrice, startTime, stage, performers);
        entityManager.persist(subEvent);
        return subEvent;
    }

    @Override
    public List<Event> findByCriteria(final Period<Instant> period, final City city, final Genre genre) {
        return entityManager
            .createQuery(EventSearchBuilder.buildQuery(period, city, genre, entityManager))
            .getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public List<Event> findAllEvents() {
        return entityManager.createNamedQuery("event.findAll", Event.class).getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<Event> findByName(final String name) {
        assert name != null : "Name must not be null!";

        return Optional
            .ofNullable(entityManager.find(EventJPA.class, Padding.padRight(name, Constants.EVENT_MAX_LENGTH)));
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<SubEvent> findSubEvent(final PerformanceId performance) {
        assert performance != null : "Performance must not be null!";
        assert performance.name() != null : "Sub-event's name must not be null!";
        assert performance.eventName() != null : "Sub-event's containing event name must not be null!";

        return Optional
            .ofNullable(entityManager
                .find(SubEventJPA.class,
                      new SubEventId(Padding.padRight(performance.name(), Constants.SUB_EVENT_NAME_MAX_LENGTH),
                          Padding.padRight(performance.eventName(), Constants.EVENT_MAX_LENGTH))));
    }

    @SuppressWarnings("nls")
    @Override
    public List<SubEvent> findAllSubEventsOrderedByTime() {
        final int limit = 4;
        return entityManager
            .createNamedQuery("subEvent.findAllOrderByTime", SubEvent.class)
            .setMaxResults(limit)
            .getResultList();

    }

    @SuppressWarnings("nls")
    @Override
    public List<SubEvent> findAllSubEventsOrderedByPopularity() {
        final int limit = 3;
        return entityManager
            .createNamedQuery("subEvent.findAllOrderByPopularity", SubEvent.class)
            .setMaxResults(limit)
            .getResultList();
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<SubEvent> findSubEventByDetails(final PerformanceDetails performanceDetails) {
        assert performanceDetails != null : "performanceDetails must not be null!";

        final List<SubEvent> foundSubEvent = entityManager
            .createNamedQuery("subEvent.findByDetails", SubEvent.class)
            .setParameter("startTime", performanceDetails.startDateTime())
            .setParameter("stageName", Padding.padRight(performanceDetails.stage(), Constants.STAGE_MAX_LENGTH))
            .setParameter("venueName", Padding.padRight(performanceDetails.venue(), Constants.VENUE_MAX_LENGTH))
            .getResultList();

        if (!foundSubEvent.isEmpty()) {
            return Optional.ofNullable(foundSubEvent.get(0));
        }
        return Optional.empty();
    }

    @SuppressWarnings("nls")
    @Override
    public Optional<Event> findEvent(final String eventId) {
        assert eventId != null : "Event id must not be null!";

        final String paddedUsername = Padding.padRight(eventId, Constants.EVENT_MAX_LENGTH);

        return Optional.ofNullable(entityManager.find(EventJPA.class, paddedUsername));
    }

    @SuppressWarnings("nls")
    @Override
    public void deleteSubEvent(final SubEvent subEventToDelete) {
        assert subEventToDelete != null : "Performance must not be null!";

        entityManager.remove(subEventToDelete);
    }

    @SuppressWarnings("nls")
    @Override
    public void createCartItem(final Customer customer, final Ticket ticket) {
        assert customer != null : "Customer must be not null";
        assert ticket != null : "Ticket must be not null";

        final CartItem cartItem = new CartItemJPA(ticket, customer);

        entityManager.persist(cartItem);
    }
}
