package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.Discount;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * JPA implementation of {@link Discount} entity.
 */
@Entity
@Table(name = "DISCOUNT")
public class DiscountJPA implements Discount {
    private static final double ONE_HUNDRED_PERCENT = 100.0;
    /**
     * The max size for discount name
     */
    public static final int MAX_DISCOUNT_NAME_SIZE = 30;
    @Id
    private String name;
    private double percentage;

    /**
     * Constructs DiscountJPA entity with the given characteristics.
     *
     * @param name
     *            The name of the discount. Must not be null or empty.
     * @param percentage
     *            The percentage of the discount. Must not be negative.
     */
    @SuppressWarnings("nls")
    public DiscountJPA(final String name, final double percentage) {
        assert name != null && !name.isEmpty() : "Name must not be null or empty!";
        assert percentage >= 0.0 && percentage <= ONE_HUNDRED_PERCENT : "Percentage must be in range [0, 100]!";

        this.name = name;
        this.percentage = percentage;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * used by application code.
     */
    protected DiscountJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getName() {
        return name.stripTrailing();
    }

    @Override
    public double getPercentage() {
        return percentage;
    }

}
