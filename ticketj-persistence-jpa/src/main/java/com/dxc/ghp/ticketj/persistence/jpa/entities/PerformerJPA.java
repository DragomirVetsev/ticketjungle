package com.dxc.ghp.ticketj.persistence.jpa.entities;

import com.dxc.ghp.ticketj.persistence.entities.Performer;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * JPA implementation of Performer entity.
 */
@Entity
@Table(name = "PERFORMER")
@NamedQuery(name = "performer.findAll",
    query = "SELECT performer FROM PerformerJPA AS performer order by performer.name")
public class PerformerJPA implements Performer {

    @Id
    @Column(name = "performer_name")
    private String name;

    private String description;

    /**
     * Constructs PerformerJPA with all its characteristics.
     *
     * @param name
     *            The name of the performer. Must not be null.
     * @param description
     *            The description of the performer. Must not be null.
     */
    @SuppressWarnings("nls")
    public PerformerJPA(final String name, final String description) {
        assert name != null : "Purchase status must not be null!";
        assert description != null : "Customer must not be null!";

        this.name = name;
        this.description = description;
    }

    /**
     * Constructor without arguments to be used by JPA implementation. Must not be
     * called by application code.
     */
    protected PerformerJPA() {
        // Required by JPA. Must not be called by application code.
    }

    @Override
    public String getName() {
        return name.stripTrailing();
    }

    @Override
    public String getDescription() {
        return description.stripTrailing();
    }
}
