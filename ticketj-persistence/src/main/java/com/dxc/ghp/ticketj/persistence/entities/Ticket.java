package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Ticket entity.
 */
public interface Ticket {

    /**
     * Retrieves the seat of the ticket.
     *
     * @return The seat of the ticket, cannot be null.
     */
    Seat getSeat();

    /**
     * Retrieves the seat status of the ticket.
     *
     * @return The seat status of the ticket, cannot be null.
     */
    SeatStatus getSeatStatus();

    /**
     * Set the status of the Seat.
     *
     * @param seatStatus
     *            the new status to set.
     */
    void setSeatStatus(SeatStatus seatStatus);

    /**
     * Retrieves the order of the ticket.
     *
     * @return The order of the ticket, can be null.
     */
    Order getOrder();

    /**
     * Retrieves the discount of the ticket.
     *
     * @return the discount of the ticket, cannot be null.
     */
    Discount getDiscount();

    /**
     * Retrieves the sub-event of the ticket.
     *
     * @return the sub-event of the ticket, cannot be null.
     */
    SubEvent getSubEvent();

    /**
     * Retrieves the cart item of the reserved ticket.
     *
     * @return the cart item of the ticket. Cannot be null.
     */
    CartItem getCartItem();

    /**
     * Retrieves ticket price after discount.
     *
     * @return the ticket price after discount, cannot be negative.
     */
    double getTicketPrice();

    /**
     * Changes the seat status of the {@Ticket} entity.
     *
     * @param seatStatus
     *            the {@link SeatStatus} to be changed to.
     */
    void changeSeatStatus(SeatStatus seatStatus);

    /**
     * Sets {@Order} for the {@Ticket} entity.
     *
     * @param order
     *            the {@link Order} entity to be set.
     */
    void setOrder(Order order);

    /**
     * Sets {@Discount} for the {@Ticket} entity.
     *
     * @param discount
     *            the {@link Discount} entity to be set.
     */
    void setDiscount(Discount discount);
}
