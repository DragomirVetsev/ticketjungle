package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Phone code entity.
 */
public interface PhoneCode {

    /**
     * Retrieves the number phone code.
     *
     * @return The phone code, Cannot be null.
     */
    String getValue();

}
