package com.dxc.ghp.ticketj.persistence.entities;

/**
 * PurchaseStatus entity.
 */
public interface PurchaseStatus {
    /**
     * Retrieves name of purchase status.
     *
     * @return the name of purchase status, cannot be null.
     */
    String getName();
}
