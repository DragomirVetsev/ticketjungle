package com.dxc.ghp.ticketj.persistence.repositories;

import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;
import com.dxc.ghp.ticketj.persistence.entities.Discount;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.PaymentType;
import com.dxc.ghp.ticketj.persistence.entities.Performer;
import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;
import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;
import com.dxc.ghp.ticketj.persistence.entities.SeatStatus;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.Venue;

/**
 * Contains all referential related persistence services.
 */
public interface ReferentialRepository {

    /**
     * Finds and returns list with all Ticket Jungle event Venues.
     *
     * @return Optional list of {@link Venue}. Cannot be null. Empty list means no
     *         venues.
     */
    List<Venue> findAllVenues();

    /**
     * Finds and returns list with all Ticket Jungle event genres.
     *
     * @return List of {@link Genre}. Cannot be null. Empty list means no genres.
     */
    List<Genre> findAllGenres();

    /**
     * Finds and returns event.
     *
     * @param eventName
     *            of the event. Must not be null.
     * @return {@link Optional} of {@link Event} or empty optional if entity is not
     *         found.
     */
    Optional<Event> findEvent(String eventName);

    /**
     * Finds and returns stages.
     *
     * @param stage
     *            where the event is going to be. Must not be null.
     * @param venue
     *            where the event is going to be. Must not be null.
     * @return {@link Optional} of {@link Stage} or empty optional if entity is not
     *         found.
     */
    Optional<Stage> findStage(String stage, String venue);

    /**
     * Find an event genre (ex. drama, action).
     *
     * @param type
     *            of the genre. Must not be null.
     * @return {@link Optional} of {@link Genre} or empty optional if entity is not
     *         found.
     */
    Optional<Genre> findGenre(String type);

    /**
     * Finds and returns all Cities.
     *
     * @return List of {@link City}. Cannot be null. Empty list means no cities.
     */
    List<City> findAllCities();

    /**
     * Finds and returns a {@link City} by its name.
     *
     * @param cityName
     *            The name of the city. Must not be null.
     * @return {@link Optional} of {@link City} or empty optional if entity is not
     *         found.
     */
    Optional<City> findCity(String cityName);

    /**
     * Finds and returns all Phone Codes.
     *
     * @return List of {@link PhoneCode}. Cannot be null. Empty list means no phone
     *         codes.
     */
    List<PhoneCode> findAllPhoneCodes();

    /**
     * Finds and returns a {@link PhoneCode} by its name.
     *
     * @param phoneCode
     *            The phone code. Must not be null.
     * @return {@link Optional} of {@link PhoneCode} or empty optional if entity is
     *         not found.
     */
    Optional<PhoneCode> findPhoneCode(final String phoneCode);

    /**
     * Finds and returns optioanl {@link PurchaseStatus} by its name.
     *
     * @param purchaseStatus
     *            the name of the purchase status to be searched. Must not be null.
     * @return {@link Optional} of {@link PurchaseStatus} or empty optional if
     *         entity is not found.
     */
    Optional<PurchaseStatus> findPurchaseStatus(String purchaseStatus);

    /**
     * Finds and returns optional of {@link DeliveryType}.
     *
     * @param deliveryType
     *            the name of delivery type to be searched. Must not be null.
     * @return {@link Optional} of {@link DeliveryType} or empty optional if entity
     *         is not found.
     */
    Optional<DeliveryType> findDeliveryType(String deliveryType);

    /**
     * Finds and returns optional of {@link PaymentType}.
     *
     * @param paymentType
     *            the name of the payment type to be searched. Must not be null.
     * @return {@link Optional} of {@link PaymentType} or empty optional if entity
     *         is not found.
     */
    Optional<PaymentType> findPaymentType(String paymentType);

    /**
     * Finds and returns optional of {@link SeatStatus}.
     *
     * @param seatStatus
     *            the name of the payment type to be searched. Must not be null.
     * @return {@link Optional} of {@link SeatStatus} or empty optional if entity is
     *         not found.
     */
    Optional<SeatStatus> findSeatStatus(String seatStatus);

    /**
     * Creates a performer.
     *
     * @param name
     *            the name of the performer. Must not be null.
     * @param description
     *            is the description of the performer. Must not be null.
     * @return the {@link Performer} object after being persisted into the
     *         repository.
     */
    Performer createPerformer(final String name, final String description);

    /**
     * Finds and returns optional of {@link Performer}.
     *
     * @param performerName
     *            is the name of the performer. Must not be null.
     * @return {@link Optional} of {@link Performer} or empty optional if entity is
     *         not found.
     */
    Optional<Performer> findPerformer(final String performerName);

    /**
     * Finds and returns all {@link Performer}.
     *
     * @return List of {@link Performer}. Cannot be null. Empty list means no
     *         performers.
     */
    List<Performer> findAllPerformers();

    /**
     * Finds and returns {@link Discount}.
     *
     * @param discountName
     *            primary key of the entity, represents the discount name.
     * @return the found {@link Discount}.
     */
    Optional<Discount> findDiscount(String discountName);
}
