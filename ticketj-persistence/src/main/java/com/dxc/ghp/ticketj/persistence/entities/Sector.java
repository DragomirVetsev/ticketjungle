package com.dxc.ghp.ticketj.persistence.entities;

import java.util.List;

/**
 * Sector entity.
 */
public interface Sector {
    /**
     * Retrieves the name of the sector.
     *
     * @return The name of the sector. Cannot be null.
     */
    String getName();

    /**
     * Retrieves the stage to which the sector belongs.
     *
     * @return The {@link Stage} to which the sector belongs. Cannot be null.
     */
    Stage getStage();

    /**
     * Retrieves the rows in the Sector.
     *
     * @return The list of {@link Row} in the Sector. Cannot be null. Empty if there
     *         are no rows.
     */
    List<Row> getRows();
}
