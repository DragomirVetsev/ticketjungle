package com.dxc.ghp.ticketj.persistence.transaction.handlers;

import java.util.function.Consumer;
import java.util.function.Function;

import com.dxc.ghp.ticketj.misc.exceptions.EntityExistsException;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;

/**
 * Executes business functions in a transaction.
 */
public interface TransactionHandler {

    /**
     * Method used to execute code within a single transaction. Can throw any
     * exception leaving the business function.
     *
     * @param <R>
     *            is the return type of the function that we execute in a single
     *            transaction.
     * @param function
     *            containing the business code that needs to executed within a
     *            single transaction. Must not be null.
     * @return the result of the {@code function}. Can be null.
     * @throws PersistenceException
     *             can occur during persistence operation. (Encapsulation is broken.
     *             Should not be thrown. Should throw appropriate business
     *             exceptions.)
     */
    <R> R executeInTransaction(Function<ServiceProvider, R> function);

    /**
     * Method used to execute code within a single transaction. Can throw any
     * exception leaving the business function.
     *
     * @param <R>
     *            is the return type of the function that we execute in a single
     *            transaction.
     * @param function
     *            containing the business code that needs to executed within a
     *            single transaction. Must not be null.
     * @param <Id>
     *            The type of the entity id.
     * @param entityClass
     *            The entity class. Must not be null.
     * @param entityId
     *            The entity id. Must not be null.
     * @return the result of the {@code function}. Must not be null.
     * @throws PersistenceException
     *             can occur during persistence operation. (Encapsulation is broken.
     *             Should not be thrown. Should throw appropriate business
     *             exceptions.)
     * @throws EntityExistsException
     *             can occur when persisting an entity whose id already exists in
     *             database.
     */
    <Id, R> R executeInTransaction(Function<ServiceProvider, R> function, Class<?> entityClass, Id entityId);

    /**
     * @param function
     *            containing the business code that needs to executed within a
     *            single transaction. Must not be null.
     */
    void executeInTransaction(Consumer<ServiceProvider> function);
}
