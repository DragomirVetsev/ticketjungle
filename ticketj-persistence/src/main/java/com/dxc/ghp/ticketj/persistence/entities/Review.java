package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Review entity.
 */
public interface Review {

    /**
     * @return the Customer who wants to see the list of his reviews. Can not be
     *         null.
     */
    Customer getCustomer();

    /**
     * @return the SubEvent that have a review. Can not be null.
     */
    SubEvent getSubEvent();

    /**
     * @return the comment that customer give to specific event. Can be null if the
     *         customer does not comment the event, but give a score.
     */
    String getComment();

    /**
     * @return the score that customer give to specific event. Can be null if the
     *         customer does not score the event, but give a comment.
     */
    Integer getScore();

}
