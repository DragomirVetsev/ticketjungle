package com.dxc.ghp.ticketj.persistence.repositories;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;

import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.persistence.entities.CartItem;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;
import com.dxc.ghp.ticketj.persistence.entities.Order;
import com.dxc.ghp.ticketj.persistence.entities.PaymentType;
import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.types.Phone;

/**
 * Contains all customer related persistence services.
 */
public interface CustomerRepository {

    /**
     * Find Customer.
     *
     * @param username
     *            of the customer. Must not be null.
     * @return {@link Optional} of {@link Customer} or empty optional if entity is
     *         not found.
     */
    Optional<Customer> findCustomer(String username);

    /**
     * Creates purchase order with delivery address.
     *
     * @param purchaseStatus
     *            The status of purchase order. Must not be null.
     * @param customer
     *            The customer for whom is the order. Must not be null.
     * @param paymentType
     *            The payment type of the order. Must not be null.
     * @param deliveryType
     *            The delivery type of the order. Must not be null.
     * @param date
     *            the date of the order. Must not be null.
     * @param deliveryAddress
     *            can be null, must be presented if the deliveryType equals courier.
     * @return The created order.
     */

    Order createOrderWithDeliveryAddress(PurchaseStatus purchaseStatus, Customer customer, DeliveryType deliveryType,
                                         PaymentType paymentType, Instant date, UnrestrictedAddress deliveryAddress);

    /**
     * Creates purchase order without delivery address.
     *
     * @param purchaseStatus
     *            The status of purchase order. Must not be null.
     * @param customer
     *            The customer for whom is the order. Must not be null.
     * @param deliveryType
     *            The delivery type of the order. Must not be null.
     * @param paymentType
     *            The payment type of the order. Must not be null.
     * @param date
     *            the date of the order. Must not be null.
     * @return The created order.
     */

    Order createOrderWithoutDeliveryAddress(PurchaseStatus purchaseStatus, Customer customer, DeliveryType deliveryType,
                                            PaymentType paymentType, Instant date);

    /**
     * @param credentials
     *            The credential of user are his {@code username} and
     *            {@code password}.
     * @param personName
     *            The given name of individual.
     * @param email
     *            The email of the user of the profile.
     * @param phone
     *            Phone number and phone code of the user.
     * @param address
     *            The address where the user is register.
     * @param birthday
     *            The date of berth of the user.
     * @return The newly carted user.
     */
    Customer createCustomer(Credentials credentials, PersonName personName, String email, Phone phone,
                            UnrestrictedAddress address, LocalDate birthday);

    /**
     * Finds {@code SubEvent} from customer's wishlist.
     *
     * @param username
     *            The unique identifier for customer.
     * @param performanceId
     *            The unique identifier for sub-event.
     * @return {@code Optional} of {@code SubEvent} containing the result of the
     *         search. Empty {@code Optional} if there is no such sub-event in
     *         customer's wishlist.
     * @see Optional
     */
    Optional<SubEvent> findSubEventFromWishlist(String username, PerformanceId performanceId);

    /**
     * Removes {@link CartItem} entity from the database.
     *
     * @param cartItem
     *            the CartItem that should be removed;
     */
    void removeCartIthem(CartItem cartItem);

}
