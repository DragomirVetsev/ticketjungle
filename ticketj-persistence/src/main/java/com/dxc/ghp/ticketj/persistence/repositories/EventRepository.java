package com.dxc.ghp.ticketj.persistence.repositories;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.entities.Ticket;

/**
 * Contains all event related persistence services.
 */
public interface EventRepository {

    /**
     * Finds and returns all Ticket Jungle events.
     *
     * @return List of {@link Event}. Cannot be null. Empty list means no events.
     */
    List<Event> findAllEvents();

    /**
     * Creates an event.
     *
     * @param name
     *            of the event. Must not be null;
     * @param description
     *            of the event. Must not be null.
     * @param genre
     *            of the event. Must not be null.
     * @return the {@link Event} object after being persisted into the repository.
     */
    Event createEvent(String name, String description, Genre genre);

    /**
     * Creates an event.
     *
     * @param name
     *            of the sub-event. Must not be null;
     * @param event
     *            of the sub-event. Must not be null.
     * @param description
     *            of the sub-event. Must not be null.
     * @param ticketPrice
     *            of the sub-event. Must not be null.
     * @param startTime
     *            of the sub-event. Must not be null.
     * @param stage
     *            of the sub-event. Must not be null.
     * @param performers
     *            of the sub-event. Must not be null.
     * @return the {@link SubEvent} object after being persisted into the
     *         repository.
     */
    SubEvent createSubEvent(final String name, final Event event, final String description, final double ticketPrice,
                            final Instant startTime, final Stage stage, List<String> performers);

    /**
     * Finds and returns all Ticket Jungle events.
     *
     * @param period
     *            The {@link Instant} start and end to witch the search will be
     *            performed.
     * @param city
     *            Wear the Event will be held.
     * @param genre
     *            Of the Event
     * @return @return List of {@link Event}. Cannot be null. Empty list means no
     *         events.
     */
    List<Event> findByCriteria(Period<Instant> period, City city, Genre genre);

    /**
     * Finds and returns a Ticket Jungle event by name.
     *
     * @param name
     *            is the name of the Event we are searching for. Must not be null
     * @return Finds and returns an {@code Optional<}{@link Event}{@code >} by name.
     * @see Optional
     */
    Optional<Event> findByName(String name);

    /**
     * Finds and returns a Ticket Jungle sub-event by id.
     *
     * @param performance
     *            containing the id of the sub-event.
     * @return Finds and returns an {@code Optional<}{@link SubEvent}{@code >}.
     * @see Optional
     */
    Optional<SubEvent> findSubEvent(final PerformanceId performance);

    /**
     * Finds and returns all Ticket Jungle sub-events ordered by date time.
     *
     * @return List of {@link SubEvent}. Cannot be null. Empty list means no events.
     */
    List<SubEvent> findAllSubEventsOrderedByTime();

    /**
     * Finds and returns all Ticket Jungle sub-events ordered by popularity.
     *
     * @return List of {@link SubEvent}. Cannot be null. Empty list means no events.
     */
    List<SubEvent> findAllSubEventsOrderedByPopularity();

    /**
     * Finds and returns all sub-events by their unique details (start time, venue
     * and stage names).
     *
     * @param performanceDetails
     *            The unique sub-event details. Must not be null.
     * @return {@link Optional} of {@link SubEvent} or empty optional if entity is
     *         not found.
     */
    Optional<SubEvent> findSubEventByDetails(final PerformanceDetails performanceDetails);

    /**
     * Find Event.
     *
     * @param eventId
     *            The event unique identifier. Must not be null.
     * @return {@link Optional} of {@link Event} or empty optional if entity is not
     *         found.
     */
    Optional<Event> findEvent(final String eventId);

    /**
     * Deletes an existing sub-event entity (does not delete if there already are
     * relationships containing this entity).
     *
     * @param subEventToDelete
     *            is the entity we are deleting. Must not be null.
     * @throws IllegalArgumentException
     *             if the instance is not an entity or is a detached entity.
     * @throws EntityNotFoundException
     *             if the entity is not found.
     */
    void deleteSubEvent(SubEvent subEventToDelete);

    /**
     * @param customer
     *            the user that create his cart Items
     * @param ticket
     *            the reserved ticket
     */
    void createCartItem(Customer customer, Ticket ticket);
}
