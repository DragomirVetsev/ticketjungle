package com.dxc.ghp.ticketj.persistence.repositories;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Office;
import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;
import com.dxc.ghp.ticketj.persistence.types.Phone;
import com.dxc.ghp.ticketj.persistence.types.RestrictedAddress;
import com.dxc.ghp.ticketj.persistence.types.UniqueOfficeLocation;

/**
 * Contains all office related persistence services.
 */
public interface OfficeRepository {

    /**
     * Finds and returns list with all Ticket Jungle offices.
     *
     * @return List of {@link Office}. Cannot be null. Empty list means no events.
     */
    List<Office> findAllOffices();

    /**
     * Creates a new {@link Office} entity.
     *
     * @param officeName
     *            The office name. Cannot be null.
     * @param address
     *            The complete office address. Cannot be null.
     * @param phone
     *            The office phone. Cannot be null.
     * @param email
     *            The office email. Cannot be null.
     * @param workingHours
     *            The office working hours. Cannot be null.
     * @return The {@link Office} object after being persisted into the repository.
     * @throws PersistenceException
     *             When a persistence operation fails.
     */
    Office createOffice(final String officeName, final RestrictedAddress address, final Phone phone, final String email,
                        final Period<LocalTime> workingHours);

    /**
     * Finds Office via office's unique identifier.
     *
     * @param officeName
     *            The office unique identifier. Must not be null.
     * @return {@link Optional} of {@link Office} or empty optional if entity is not
     *         found.
     */
    Optional<Office> findOffice(String officeName);

    /**
     * Checks if there is an Office with the given email.
     *
     * @param email
     *            The unique email. Must not be null.
     * @return True if there is an Office entity with the passed email, or False
     *         otherwise.
     */
    boolean emailExists(String email);

    /**
     * Checks if there is an Office with the given phone (unique office phone
     * consists of unique combination between {@link PhoneCode} and phone number).
     *
     * @param phone
     *            The unique phone. Must not be null.
     * @return True if there is an Office entity with the passed phone, or False
     *         otherwise.
     */
    boolean phoneExists(Phone phone);

    /**
     * Checks if there is an Office with the given address. (unique office address
     * consists of unique combination between {@link City} and street).
     *
     * @param officeAddress
     *            The office unique location. Must not be null.
     * @return True if there is an Office entity with the passed address, or False
     *         otherwise.
     */
    boolean addressExists(UniqueOfficeLocation officeAddress);
}
