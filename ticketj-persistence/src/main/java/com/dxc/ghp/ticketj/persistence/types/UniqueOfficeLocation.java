package com.dxc.ghp.ticketj.persistence.types;

import com.dxc.ghp.ticketj.persistence.entities.City;

/**
 * Contains unique Office location information.
 *
 * @param city
 *            The office's city.
 * @param street
 *            The office's street.
 */
public record UniqueOfficeLocation(City city, String street) {

}
