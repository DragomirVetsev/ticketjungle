/**
 *
 */
package com.dxc.ghp.ticketj.persistence.entities;

import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.persistence.types.Phone;

/**
 * User entity.
 */
public interface User {
    /**
     * Retrieves the username of the user.
     *
     * @return User's username. Cannot be null.
     */
    String getUsername();

    /**
     * Retrieves the {@link PersonName} of the user.
     *
     * @return User's {@link PersonName}. Cannot be null.
     */
    PersonName getPersonName();

    /**
     * Retrieves the email of the user.
     *
     * @return User's email. Cannot be null.
     */
    String getEmail();

    /**
     * Retireves the {@link Phone} of the user.
     *
     * @return User's {@link Phone}. Cannot be null.
     */
    Phone getPhone();

    /**
     * Retrieves the {@link UnrestrictedAddress} of the user.
     *
     * @return User's {@Address}.
     */
    UnrestrictedAddress getAddress();

    /**
     * Retrieves the password of the user.
     *
     * @return User's password. Cannot be null.
     */
    String getPassword();

    /**
     * Retrieves the role of the user.
     *
     * @return User's role. Cannot be null.
     */
    UserRole getRole();

    /**
     * Changes the Users address.
     *
     * @param address
     *            The new address of the user. Must not be null.
     */
    void changeAddress(UnrestrictedAddress address);

    /**
     * Changes the User Phone.
     *
     * @param phone
     *            The new phone for the user. Must not be null.
     */
    void changePhone(Phone phone);

    /**
     * Changes the User password
     *
     * @param password
     *            The new password for the user. Must not be null.
     */
    void changePassword(String password);
}
