/**
 *
 */
package com.dxc.ghp.ticketj.persistence.entities;

/**
 * City entity.
 */
public interface City {

    /**
     * Retrieves the city name.
     *
     * @return The city name, cannot be null.
     */
    String getCityName();
}
