package com.dxc.ghp.ticketj.persistence.repositories;

import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;

/**
 * Contains all office worker related persistence services.
 */
public interface OfficeWorkerRepository {
    /**
     * Finds and returns list with all available office workers.
     *
     * @return List of {@link OfficeWorker}. Cannot be null. Empty list means no
     *         events.
     */
    List<OfficeWorker> findAllAvailableWorkers();

    /**
     * Finds Office worker via worker's unique identifier.
     *
     * @param username
     *            The office worker unique identifier. Must not be null.
     * @return {@link Optional} of {@link OfficeWorker} or empty optional if entity
     *         is not found.
     */
    Optional<OfficeWorker> findWorker(String username);
}
