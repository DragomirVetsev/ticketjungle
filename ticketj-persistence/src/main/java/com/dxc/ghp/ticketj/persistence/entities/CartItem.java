package com.dxc.ghp.ticketj.persistence.entities;

/**
*
*/
public interface CartItem {

    /**
     * @return the reserved ticket. Can not be null.
     */
    Ticket getTicket();

    /**
     * @return the customer. Can not be null.
     */
    Customer getCustomer();

}
