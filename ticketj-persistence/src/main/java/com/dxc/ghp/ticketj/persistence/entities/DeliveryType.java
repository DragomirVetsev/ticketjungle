package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Delivery type entity.
 */
public interface DeliveryType {
    /**
     * Retrieve delivery name of delivery type.
     *
     * @return The delivery name, cannot be null.
     */
    String getName();
}
