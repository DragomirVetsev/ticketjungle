package com.dxc.ghp.ticketj.persistence.types;

/**
 * Contains all databases.
 */
public enum Database {
    /**
     * Oracle
     */
    ORACLE,
    /**
     * Hsqldb
     */
    HSQLDB;

}
