package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Discount entity.
 */
public interface Discount {

    /**
     * Retrieves name of discount.
     *
     * @return The name of discount, cannot be null.
     */
    String getName();

    /**
     * Retrieves percentage of discount.
     *
     * @return The percentage of discount. Must be {@code >= 0.0 && <= 100.0}.
     */
    double getPercentage();

}
