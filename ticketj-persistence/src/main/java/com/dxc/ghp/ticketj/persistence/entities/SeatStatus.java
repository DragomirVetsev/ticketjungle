package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Seat Status entity.
 */
public interface SeatStatus {
    /**
     * Retrieves the status of seat.
     *
     * @return The status of the seat, cannot be null.
     */
    String getStatus();

    /**
     * @param status
     *            is the new status name.
     */
    void setStatus(String status);
}
