package com.dxc.ghp.ticketj.persistence.repositories;

import java.util.List;

import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.persistence.entities.User;

/**
 * Contains all user related persistence services.
 */
public interface UserRepository {

    /**
     * Finds and returns list with all Ticket Jungle users.
     *
     * @return List of {@link User}. Cannot be null. Empty list means no users.
     */
    List<User> findAllUsers();

    /**
     * @param credentials
     *            Contains user`s useername and password. Can be null.
     * @return {@code User} object with credentials.
     * @throws PersistenceExcepton
     *             if user with the given credentials is not found.
     */
    User findUserByCredentials(Credentials credentials);
}
