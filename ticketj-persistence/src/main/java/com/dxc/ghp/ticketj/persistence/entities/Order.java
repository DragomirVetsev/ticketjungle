package com.dxc.ghp.ticketj.persistence.entities;

import java.time.Instant;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;

/**
 * Order entity.
 */
public interface Order {
    /**
     * Retrieves id of order.
     *
     * @return the id of the order.
     */
    int getId();

    /**
     * Retrieves purchase status of order.
     *
     * @return The purchase status, cannot be null.
     */
    PurchaseStatus getPurchaseStatus();

    /**
     * Retrieves customer of order.
     *
     * @return The customer, cannot be null.
     */
    Customer getCustomer();

    /**
     * Retrieves delivery type of order.
     *
     * @return The delivery type, cannot be null.
     */
    DeliveryType getDeliveryType();

    /**
     * Retrieves payment type of order.
     *
     * @return The payment type, cannot be null.
     */
    PaymentType getPaymentType();

    /**
     * Retrieves the date of creation of order.
     *
     * @return The date of creation. cannot be null.
     */
    Instant getCreatedDate();

    /**
     * Retrieves the delivery address of the order.
     *
     * @return the delivery address, cannot be null.
     */
    UnrestrictedAddress getDeliveryAddress();

    /**
     * Retrieves all tickets of the order.
     *
     * @return all order tickets, cannot be null.
     */
    List<Ticket> getTickets();
}
