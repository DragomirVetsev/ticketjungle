package com.dxc.ghp.ticketj.persistence.entities;

import java.util.List;

/**
 * Event entity.
 */
public interface Event {
    /**
     * Retrieves the name of the event.
     *
     * @return The name of the event. Cannot be null.
     */
    String getName();

    /**
     * Retrieves the description of the event.
     *
     * @return The description of the event. Cannot be null.
     */
    String getDescription();

    /**
     * Retrieves the genre of the event.
     *
     * @return The genre of the event. Cannot be null.
     */
    Genre getGenre();

    /**
     * Retrieves the sub-events of the event.
     *
     * @return List of the sub-events of the event. Cannot be null.
     */
    List<SubEvent> getSubEvents();

    /**
     * Sets the description of the event.
     *
     * @param description
     *            is the new description.
     */
    void setDescription(String description);

    /**
     * Sets the genre of the event.
     *
     * @param genre
     *            is the new genre.
     */
    void setGenre(Genre genre);
}
