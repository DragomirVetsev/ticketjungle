package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Purchase type entity.
 */
public interface PaymentType {
    /**
     * Retrieves the name of payment type.
     *
     * @return The name of the payment type, cannot be null.
     */
    String getName();
}
