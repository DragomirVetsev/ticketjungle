package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Genre entity.
 */
public interface Genre {

    /**
     * Retrieves the type of genre.
     *
     * @return The type of genre. Cannot be null.
     */
    String getType();
}
