package com.dxc.ghp.ticketj.persistence.entities;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * Customer entity
 */
public interface Customer extends User {
    /**
     * Retrieves the birthday of the customer.
     *
     * @return Customer's birthday. Cannot be null.
     */
    LocalDate getBirthday();

    /**
     * Retrieves the reviews list of the customer.
     *
     * @return List of the reviews of the customer. Cannot be null.
     */
    List<Review> getReviews();

    /**
     * Retrieves all customer orders.
     *
     * @return List of customer orders, cannot be null.
     */
    List<Order> getOrders();

    /**
     * Retrieves all sub-events from the customer wish list.
     *
     * @return list of sub-events. Cannot be null.
     */
    Set<SubEvent> getWishlist();

    /**
     * Retrieves the cart items (reserved tickets) of the customer.
     *
     * @return the list of the reserved ticket by customer. Cannot be null.
     */
    List<CartItem> getCartItems();

    /**
     * Adds a {@code SubEvent} to customer's wishlist.
     *
     * @param subEvent
     *            The {@code SubEvent} that will be added to customer's wishlist.
     *            Must not be null.
     * @return {@code true} if the {@code SubEvent} has been added. {@code false} if
     *         the {@code SubEvent} hasn't been added.
     */
    boolean addToWishList(SubEvent subEvent);

    /**
     * Removes a {@code SubEvent} from customer's wishlist.
     *
     * @param subEvent
     *            The {@code SubEvent} that will be removed to customer's wishlist.
     *            Must not be null.
     * @return {@code true} if the {@code SubEvent} has been removed. {@code false}
     *         if the {@code SubEvent} hasn't been removed.
     */
    boolean removeFromWishlist(SubEvent subEvent);

    /**
     * Removes one or many {@code SubEvent}'s from customer wishlist.
     *
     * @param foundSubEvents
     *            The {@code SubEvent}s that will be removed from customer's
     *            wishlist.
     * @return {@code true} if one, some or all of the foundSubEvents have been
     *         successfully removed from the wishlist. Returns {@code false} if the
     *         customer's wishlist was not modified.
     */
    boolean removeFromWishlist(Set<SubEvent> foundSubEvents);

}
