package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Seat entity.
 */
public interface Seat {

    /**
     * Retrieves the number of the seat.
     *
     * @return The number of the seat. Cannot be below 0.
     */
    int getNumber();

    /**
     * Retrieves the row to which the seat belongs.
     *
     * @return The {@link Row} to which the seat belongs. Cannot be null.
     */
    Row getRow();
}
