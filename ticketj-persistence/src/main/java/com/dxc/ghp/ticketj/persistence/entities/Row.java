package com.dxc.ghp.ticketj.persistence.entities;

import java.util.List;

/**
 * Row entity.
 */
public interface Row {
    /**
     * Retrieves the number of the row.
     *
     * @return The number of the row in the sector. Cannot be below 0.
     */
    int getNumber();

    /**
     * Retrieves the sector to which the Row belongs.
     *
     * @return The {@Sector} to which the Row belongs. Cannot be null.
     */
    Sector getSector();

    /**
     * Retrieves the seats in the Row.
     *
     * @return The list of {@link Seat} in the Row.
     */
    List<Seat> getSeats();
}
