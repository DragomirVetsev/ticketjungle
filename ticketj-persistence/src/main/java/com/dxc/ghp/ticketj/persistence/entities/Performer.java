package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Performer entity
 */
public interface Performer {

    /**
     * Retrieves the name of the performer.
     *
     * @return the name of the performer.
     */
    String getName();

    /**
     * Retrieves the description of the performer.
     *
     * @return the description of the performer.
     */
    String getDescription();
}
