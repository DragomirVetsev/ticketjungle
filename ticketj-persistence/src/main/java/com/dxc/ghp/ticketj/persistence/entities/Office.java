package com.dxc.ghp.ticketj.persistence.entities;

import java.time.LocalTime;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.persistence.types.Phone;
import com.dxc.ghp.ticketj.persistence.types.RestrictedAddress;

/**
 * Office entity.
 */
public interface Office {

    /**
     * Retrieves the name of the office.
     *
     * @return The name of the office, cannot be null.
     */
    String getOfficeName();

    /**
     * Retrieves the address of the office.
     *
     * @return The address of the office, cannot be null.
     */
    RestrictedAddress getAddress();

    /**
     * Retrieves the office phone.
     *
     * @return The office phone, cannot be null.
     */
    Phone getPhone();

    /**
     * Retrieves the office email.
     *
     * @return The office email, cannot be null.
     */
    String getEmail();

    /**
     * Retrieves the working hours of the office.
     *
     * @return The working hours, cannot be null.
     */
    Period<LocalTime> getWorkingPeriod();

    /**
     * Retrieves all employees.
     *
     * @return The List of all employees, cannot be null. Empty list means that the
     *         office has no employees.
     */
    List<OfficeWorker> getWorkers();

    /**
     * Changes the Office address.
     *
     * @param address
     *            The new office address. Must not be null.
     */
    void changeAddress(RestrictedAddress address);

    /**
     * Changes the Office Phone.
     *
     * @param phone
     *            The new office phone. Must not be null.
     */
    void changePhone(Phone phone);

    /**
     * Changes the Office email.
     *
     * @param email
     *            The new office email. Must not be null.
     */
    void changeEmail(String email);

    /**
     * Changes the Office working hours.
     *
     * @param workingPeriod
     *            The new office working hours. Must not be null.
     */
    void changeWorkingPeriod(Period<LocalTime> workingPeriod);

    /**
     * Changes all Office workers.
     *
     * @param employees
     *            The new office workers. Must not be null.
     */
    void changeEmployees(List<OfficeWorker> employees);
}
