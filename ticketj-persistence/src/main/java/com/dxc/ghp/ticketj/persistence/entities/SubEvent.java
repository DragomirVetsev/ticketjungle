package com.dxc.ghp.ticketj.persistence.entities;

import java.time.Instant;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;

/**
 * SubEvent entity.
 */
public interface SubEvent {

    /**
     * Retrieves the unique identifier of the sub-event.
     *
     * @return The ID of the sub-event.
     */
    PerformanceId getId();

    /**
     * Retrieves the name of the sub-event.
     *
     * @return The name of the sub-event. Cannot be null.
     */
    String getName();

    /**
     * Retrieves the Event which contains this sub-event.
     *
     * @return The event which contains the SubEvent. Cannot be null.
     */
    Event getEvent();

    /**
     * Retrieves the description of the sub-event.
     *
     * @return The description of the sub-event. Cannot be null.
     */
    String getDescription();

    /**
     * Retrieves the ticket price of the sub-event.
     *
     * @return The ticket price of the sub-event.
     */
    double getTicketPrice();

    /**
     * Retrieves the start date and time of the sub-event.
     *
     * @return The start date and time of the sub-event. Cannot be null.
     */
    Instant getStartTime();

    /**
     * Retrieves the Stage where the sub-event is held.
     *
     * @return The the Stage of the sub-event. Cannot be null.
     */
    Stage getStage();

    /**
     * Retrieves the reviews list of the sub-event.
     *
     * @return List of the reviews of the sub-event. Cannot be null.
     */
    List<Review> getReviews();

    /**
     * Retrieves the tickets of the sub-event.
     *
     * @return List of the tickets for the sub-event. Cannot be null.
     */
    List<Ticket> getTickets();

    /**
     * Retrieves the performers of the sub-event.
     *
     * @return List of the performers for the sub-event. Cannot be null.
     */
    List<Performer> getPerformers();

    /**
     * Retrieves the customers who have the sub-event in their wish list.
     *
     * @return List of the customers who have the sub-event in their wish list for
     *         the sub-event. Cannot be null.
     */
    List<Customer> getCustomers();

    /**
     * Retrieves the average score and count of people who have scored the
     * sub-event.
     *
     * @return a {@link ReviewScoreAndCount} of the average score and count of
     *         people who have scored the sub-event. Cannot be null.
     */
    ReviewScoreAndCount getReviewScoreAndCount();

    /**
     * Sets the price of the tickets of the sub-event.
     *
     * @param ticketPrice
     *            is the new ticketPrice.
     */
    void setTicketPrice(double ticketPrice);

    /**
     * Sets the description of the sub-event.
     *
     * @param description
     *            is the new description.
     */
    void setDescription(String description);

    /**
     * Sets the stage of the sub-event.
     *
     * @param stage
     *            is the new stage.
     */
    void setStage(Stage stage);

    /**
     * Sets the start date and time of the sub-event.
     *
     * @param startTime
     *            is the new start date and time.
     */
    void setStartTime(Instant startTime);

    /**
     * Sets the list of performers of the sub-event.
     *
     * @param performers
     *            is the new start date and time.
     */
    void setPerformers(final List<Performer> performers);
}
