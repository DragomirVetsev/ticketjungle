package com.dxc.ghp.ticketj.persistence.entities;

import java.util.List;

/**
 * Stage entity
 */
public interface Stage {

    /**
     * Retrieves the name of the stage.
     *
     * @return The name of the stage. Cannot be null.
     */
    String getName();

    /**
     * Retrieves the venue to which the scene belongs.
     *
     * @return The {@link Venue} to which the scene belongs. Cannot be null.
     */
    Venue getVenue();

    /**
     * Retrieves the sectors in the Stage.
     *
     * @return The list of {@link Sector} around the Stage.
     */
    List<Sector> getSectors();
}
