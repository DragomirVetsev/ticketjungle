package com.dxc.ghp.ticketj.persistence.entities;

import java.util.List;

import com.dxc.ghp.ticketj.persistence.types.RestrictedAddress;

/**
 * Venue entity.
 */
public interface Venue {

    /**
     * Retrieves the name of the Venue. It is also the ID of the Venue.
     *
     * @return The name of the Venue. Cannot be null.
     */
    String getName();

    /**
     * Retrieves the address of the Venue.
     *
     * @return The {@link RestrictedAddress} of the Venue. Cannot be null.
     */
    RestrictedAddress getAddress();

    /**
     * Retrieves the stages of this Venue.
     *
     * @return The list of {@link Stage} in Venue. Cannot be null. Empty if there
     *         are no stages.
     */
    List<Stage> getStages();

}
