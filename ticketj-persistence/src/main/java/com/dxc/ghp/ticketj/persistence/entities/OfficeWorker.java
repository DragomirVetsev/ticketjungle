package com.dxc.ghp.ticketj.persistence.entities;

/**
 * Office worker Entity.
 */
public interface OfficeWorker extends User {
    /**
     * Retrieves the working place of worker.
     *
     * @return The Office of employment, can be null.
     */
    Office getWorkingPlace();

    /**
     * Changes the working place of the worker.
     *
     * @param office
     *            The new working place of worker. Can be null.
     */
    void setWorkingPlace(Office office);
}
