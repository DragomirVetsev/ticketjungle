package com.dxc.ghp.ticketj.persistence.types;

import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;

/*
 * Persistence record holding {@link PhoneCode} and number.
 * @param code The phone code entity.
 * @param number The phone number.
 */
/**
 * @param code
 *            of the phone.
 * @param number
 *            of the phone.
 */
public record Phone(PhoneCode code, String number) {

}
