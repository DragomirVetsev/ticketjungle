/**
 *
 */
package com.dxc.ghp.ticketj.persistence.types;

import com.dxc.ghp.ticketj.persistence.entities.City;

/**
 * @param street
 *            Of the restricted address.
 * @param city
 *            Of the restricted address.
 * @param postCode
 *            Of the restricted address.
 */
public record RestrictedAddress(String street, City city, String postCode) {

}
