package com.dxc.ghp.ticketj.misc.service.registry.tests;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistry;
import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistryImpl;

/**
 * Testing the ServiceRegistry.
 */
final class ServiceRegistryImplTest {

    /**
     * Test if registering a correct service does not throw.
     */
    @SuppressWarnings("static-method")
    @Test
    void testRegisterDoesNotThrowWithCorrectData() {
        final ServiceRegistry<String> registry = new ServiceRegistryImpl<>();

        assertDoesNotThrow(() -> registry.register(String.class, String::new));
    }

    /**
     * Test if the type of the found object is correct.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    void testFindServiceConstructorPositive() {
        final ServiceRegistry<String> registry = new ServiceRegistryImpl<>();
        registry.register(String.class, String::new);
        final Function<String, ?> constructor = registry.findServiceConstructor(String.class);

        assertEquals(String.class, constructor.apply("testString").getClass());
    }

    /**
     * Test if returned function is null when looking for unregistered service.
     */
    @SuppressWarnings("static-method")
    @Test
    void testFindServiceConstructorNegative() {
        final ServiceRegistry<String> registry = new ServiceRegistryImpl<>();
        final Object function = registry.findServiceConstructor(String.class);

        assertNull(function);
    }
}
