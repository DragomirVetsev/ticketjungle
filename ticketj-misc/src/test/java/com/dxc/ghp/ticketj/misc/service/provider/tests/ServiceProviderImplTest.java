package com.dxc.ghp.ticketj.misc.service.provider.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProviderImpl;
import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistry;
import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistryImpl;

/**
 * Testing the ServiceProvider.
 */
final class ServiceProviderImplTest {

    private ServiceRegistry<String> serviceRegistry;
    private ServiceProvider serviceProvider;

    @SuppressWarnings("nls")
    @BeforeEach
    private void initEach() {
        final String testString = "testString";
        serviceRegistry = new ServiceRegistryImpl<>();
        serviceProvider = new ServiceProviderImpl<>(testString, serviceRegistry);
    }

    /**
     * Test if type of the found object is correct.
     */
    @Test
    void testFindPositive() {
        serviceRegistry.register(String.class, String::new);

        final String registeredService = serviceProvider.find(String.class);

        assertEquals(String.class, registeredService.getClass());
    }

    /**
     * Test if throws ServiceMissingException when using find for unregistered
     * service.
     */
    @SuppressWarnings({
        "nls"
    })
    @Test
    void testFindNegativeThrowsServiceMissingException() {
        final ServiceMissingException serviceMissingException = throwsServiceMissingException(serviceProvider);
        assertEquals("String", serviceMissingException.getMissingService());
    }

    /**
     * Test if throws ServiceMissingException when using find and register method
     * returns null.
     */
    @SuppressWarnings({
        "nls"
    })
    @Test
    private void testFindNegativeWhenServiceFactoryReturnsNullThrows() {
        serviceRegistry.register(String.class, (final String returnNull) -> null);

        final ServiceMissingException serviceMissingException = throwsServiceMissingException(serviceProvider);
        assertEquals("String", serviceMissingException.getMissingService());
    }

    /**
     * Test if throws when using find and register method throws.
     */
    @Test
    void testFindNegativeWhenServiceFactoryThrows() {
        serviceRegistry.register(String.class, (final String returnThrows) -> {
            throw new RuntimeException();
        });

        assertThrows(RuntimeException.class, () -> serviceProvider.find(String.class));
    }

    private static ServiceMissingException throwsServiceMissingException(final ServiceProvider serviceProvider) {
        return assertThrows(ServiceMissingException.class, () -> serviceProvider.find(String.class));
    }
}
