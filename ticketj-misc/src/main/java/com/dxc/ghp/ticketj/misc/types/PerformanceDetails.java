package com.dxc.ghp.ticketj.misc.types;

import java.io.Serializable;
import java.time.Instant;

/**
 * Record containing place and time of sub-event.
 *
 * @param venue
 *            The venue of sub-event.
 * @param stage
 *            The stage of sub-event.
 * @param startDateTime
 *            The starting time of sub-event.
 */

public record PerformanceDetails(String venue, String stage, Instant startDateTime) implements Serializable {

}
