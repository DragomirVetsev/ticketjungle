package com.dxc.ghp.ticketj.misc.service.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Stores services.
 *
 * @param <ConstructorArgument>
 *            is used for the service factory.
 */
public final class ServiceRegistryImpl<ConstructorArgument> implements ServiceRegistry<ConstructorArgument> {

    private final Map<String, Function<ConstructorArgument, ?>> services;

    /**
     * Empty constructor to initialize services.
     */
    public ServiceRegistryImpl() {
        services = new HashMap<>();
    }

    @SuppressWarnings("nls")
    @Override
    public <Service> void register(final Class<Service> service,
                                   final Function<ConstructorArgument, Service> serviceFactory) {
        assert service != null : "service must not be null!";
        assert serviceFactory != null : "serviceFactory must not be null!";

        services.put(service.getName(), serviceFactory);
    }

    @SuppressWarnings("nls")
    @Override
    public <Service> Function<ConstructorArgument, ?> findServiceConstructor(final Class<Service> service) {
        assert service != null : "service must not be null!";

        return services.get(service.getName());
    }

}
