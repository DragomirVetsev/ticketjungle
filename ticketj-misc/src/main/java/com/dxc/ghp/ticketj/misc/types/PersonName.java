/**
 *
 */
package com.dxc.ghp.ticketj.misc.types;

/**
 * Contains the customer names
 *
 * @param givenName
 *            Person's given name.
 * @param familyName
 *            Person's family name.
 */
public record PersonName(String givenName, String familyName) {

}
