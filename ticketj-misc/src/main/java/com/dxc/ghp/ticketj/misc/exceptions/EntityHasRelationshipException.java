package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Custom exception used when trying to delete an entity which already has
 * relationships.
 */
public final class EntityHasRelationshipException extends EntityException {
    private static final long serialVersionUID = 2158793504609919405L;

    /**
     * Construct a custom exception when changing or deleting entity that already
     * has relationships containing its class and primary id.
     *
     * @param <ID>
     *            The entity id type.
     * @param entity
     *            The entity class, must not be null.
     * @param entityId
     *            The entity primary id, must not be null.
     */
    @SuppressWarnings("nls")
    public <ID> EntityHasRelationshipException(final Class<?> entity, final ID entityId) {
        super("Entity %s with ID: %s already has relationships!", entity, entityId, null);
    }

}
