package com.dxc.ghp.ticketj.misc.exceptions;

import com.dxc.ghp.ticketj.misc.types.EntityFieldType;

/**
 * Record used to pass information about entity when error with duplicate field
 * occurs. Field must not be the primary identifier of the entity.
 *
 * @param <Field>
 *            The field value type.
 * @param entity
 *            The entity name.
 * @param fieldType
 *            The field type.
 * @param fieldValue
 *            The field value.
 */
public record DuplicateFieldInfo<Field>(String entity, EntityFieldType fieldType, Field fieldValue)
    implements ErrorInfo {

}
