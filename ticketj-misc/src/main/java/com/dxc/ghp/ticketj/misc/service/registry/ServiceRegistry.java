package com.dxc.ghp.ticketj.misc.service.registry;

import java.util.function.Function;

/**
 * Functionality for storing services.
 *
 * @param <ConstructorArgument>
 *            is used for the service factory.
 */
public interface ServiceRegistry<ConstructorArgument> {

    /**
     * Registers a pair of service and service factory method.
     *
     * @param <Service>
     *            is the type of the service.
     * @param service
     *            is the class of the service. Must not be null.
     * @param serviceFactory
     *            is the factory method of the service. Must not be null.
     */
    <Service> void register(Class<Service> service, Function<ConstructorArgument, Service> serviceFactory);

    /**
     * Finds a method used to make a service.
     *
     * @param <Service>
     *            is the type of the wanted service.
     * @param service
     *            is the class of the wanted service. Must not be null.
     * @return function, that returns a new Service object, or null if serviceClass
     *         does not exist.
     */
    <Service> Function<ConstructorArgument, ?> findServiceConstructor(Class<Service> service);

}
