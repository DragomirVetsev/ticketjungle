package com.dxc.ghp.ticketj.misc.types;

/**
 * Contains the constraint constants for domain objects.
 */
public final class Constants {

    /**
     * Maximum allowed length for the event-name.
     */
    public static final int EVENT_NAME_MAX_LENGTH = 100;
    /**
     * Maximum allowed length for the given name of a person.
     */
    public static final int GIVEN_NAME_MAX_LENGTH = 30;
    /**
     * Maximum allowed length for the family name of a person.
     */
    public static final int FAMILY_NAME_MAX_LENGTH = 30;
    /**
     * The regular expression pattern containing the allowed symbols for a persons
     * name. Allows: Only english letters and blank spaces.
     */
    @SuppressWarnings("nls")
    public static final String PERSON_NAME_PATTERN = "[a-zA-Z ]+";
    /**
     * Maximum allowed length for the user-name.
     */
    public static final int USERNAME_MAX_LENGTH = 30;
    /**
     * Minimum allowed length for the user-name.
     */
    public static final int USERNAME_MIN_LENGTH = 2;
    /**
     * The regular expression pattern containing the allowed symbols for email.
     * Allows:[English Upper and lower case letters, symbols ~!@#$%^&*()_+ and
     * numbers with a @ in the middle and a . something at the end]
     */
    @SuppressWarnings("nls")
    public static final String EMAIL_PATTERN = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
    /**
     * Maximum allowed length for the email.
     */
    public static final int EMAIL_MAX_LENGTH = 30;
    /**
     * Minimum allowed length for the email.
     */
    public static final int EMAIL_MIN_LENGTH = 2;
    /**
     * The regular expression pattern containing the allowed symbols for email.
     * Allows:[Matches only positive numbers, but the first digit must not be 0]
     */
    @SuppressWarnings("nls")
    public static final String PHONE_NUMBER_PATTERN = "^[1-9]\\d*$";
    /**
     * The regular expression pattern containing the allowed symbols for email.
     * Allows:[Which can be 1 to 3 digits or 1 to 4 digits for the area code an
     * optional separator after the area code "-" for matching 1 to 4 digits for the
     * local exchange code and the number in the code are from 1 to 9]
     */
    @SuppressWarnings("nls")
    public static final String PHONE_CODE_PATTERN = "^\\+\\d*[1-9]\\d*$";

    /**
     * Maximum allowed length for the phone number.
     */
    public static final int PHONE_NUMBER_MAX_LENGTH = 9;
    /**
     * Minimum allowed length for the phone number.
     */
    public static final int PHONE_NUMBER_MIN_LENGTH = 8;
    /**
     * Maximum allowed length for the phone code.
     */
    public static final int PHONE_CODE_MAX_LENGTH = 6;
    /**
     * Minimum allowed length for the phone code.
     */
    public static final int PHONE_CODE_MIN_LENGTH = 3;
    /**
     * The regular expression pattern containing the allowed symbols for user-name.
     * Allows:[English Upper and lower case letters, symbols ~!@#$%^&*()_+ and
     * numbers]
     */
    @SuppressWarnings("nls")
    public static final String USERNAME_PATTERN = "[a-zA-Z~!@#$%^&*()_+0-9]+";

    /**
     * The regular expression pattern containing the allowed symbols for password.
     * The password must have at least: One upper case letter, one lower case
     * letter, one symbol and one number
     */
    @SuppressWarnings("nls")
    public static final String PASSWORD_PATTERN = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()_+])"
        + "[A-Za-z0-9!@#$%^&*()_+]+$";
    /**
     * Maximum allowed length for the email.
     */
    public static final int PASSWORD_MAX_LENGTH = 30;
    /**
     * Minimum allowed length for the email.
     */
    public static final int PASSWORD_MIN_LENGTH = 10;
    /**
     * Maximum allowed length for city.
     */
    public static final int CITY_MAX_LENGTH = 30;

    /**
     * Minimum allowed length for city.
     */
    public static final int CITY_MIN_LENGTH = 2;

    /**
     * The regular expression pattern containing the allowed symbols for city's
     * name. Allows: Only english letters and blank spaces.
     */
    @SuppressWarnings("nls")
    public static final String CITY_PATTERN = "[a-zA-Z ]+";

    /**
     * Maximum allowed length for genre.
     */
    public static final int GENRE_MAX_LENGTH = 50;
    /**
     * Minimum allowed length for genre.
     */
    public static final int GENRE_MIN_LENGTH = 2;

    /**
     * The regular expression pattern containing the allowed symbols for genre.
     * Allows: Only english letters and blank spaces.
     */
    @SuppressWarnings("nls")
    public static final String GENRE_PATTERN = "[a-zA-Z ]+";

    /**
     * The maximum size of the Event name.
     */
    public static final int EVENT_MAX_LENGTH = 100;

    /**
     * The maximum size of the Event name.
     */
    public static final int EVENT_MIN_LENGTH = 5;

    /**
     * The regular expression pattern containing the allowed symbols for event's
     * name.Allows: Different language letters, blank spaces, numbers and
     * symbols("'.!?:-).
     */
    @SuppressWarnings({
        "nls"
    })
    public static final String EVENT_NAME_PATTERN = "^[\\p{L} 0-9\"'\\.!\\?:\\-–&/]+$";

    /**
     * The maximum size of event's description.
     */
    public static final int EVENT_DESCRIPTION_MAX_LENGTH = 500;

    /**
     * The minimum size of event's description.
     */
    public static final int EVENT_DESCRIPTION_MIN_LENGTH = 8;

    /**
     * The maximum size of sub-event's name.
     */
    public static final int SUB_EVENT_NAME_MAX_LENGTH = 100;

    /**
     * The minimum size of sub-event's name.
     */
    public static final int SUB_EVENT_NAME_MIN_LENGTH = 5;

    /**
     * The regular expression pattern containing the allowed symbols for event's
     * name. * name. Allows: Different language letters, blank spaces, numbers and
     * symbols("'.!?:-).
     */
    @SuppressWarnings({
        "nls"
    })
    public static final String SUB_EVENT_NAME_PATTERN = "^[\\p{L} 0-9\"'\\.!\\?:\\-–&/]+$";

    /**
     * The maximum length of venue's name.
     */
    public static final int VENUE_MAX_LENGTH = 50;

    /**
     * The minimum length of venue's name.
     */
    public static final int VENUE_MIN_LENGTH = 2;
    /**
     * The regular expression pattern containing the allowed symbols for venue's
     * name. * name. Allows: Different language letters, blank spaces, numbers and
     * symbols("'.-).
     */
    @SuppressWarnings("nls")
    public static final String VENUE_PATTERN = "^[\\p{L} 0-9\"'\\.!\\?:\\-–]+$";

    /**
     * The maximum length of stage's name.
     */
    public static final int STAGE_MAX_LENGTH = 30;

    /**
     * The minimum length of stage's name.
     */
    public static final int STAGE_MIN_LENGTH = 5;

    /**
     * The regular expression pattern containing the allowed symbols for stage's
     * name. Allows: Different language letters, blank spaces, numbers and
     * symbols("'.-).
     */
    @SuppressWarnings("nls")
    public static final String STAGE_PATTERN = "^[\\p{L} 0-9\"'\\.!\\?:\\-–]+$";

    /**
     * The maximum length of sector's name.
     */
    public static final int SECTOR_MAX_LENGTH = 30;

    /**
     * The maximum length of seat status's name.
     */
    public static final int SEAT_STATUS_MAX_LENGTH = 20;

    /**
     * The minimum allowed street's name length.
     */
    public static final int STREET_NAME_MIN_LENGTH = 5;
    /**
     * The maximum allowed street's name length.
     */
    public static final int STREET_NAME_MAX_LENGTH = 50;

    /**
     * The pattern for the street. Allows: Different language letters, blank spaces,
     * numbers and symbols("'.-).
     */
    @SuppressWarnings("nls")
    public static final String STREET_PATTERN = "^[\\p{L} 0-9\"'\\.№,\\-]+$";

    /**
     * The minimum allowed postcode length.
     */
    public static final int POSTCODE_MIN_LENGTH = 3;
    /**
     * The maximum allowed postcode length.
     */
    public static final int POSTCODE_MAX_LENGTH = 10;

    /**
     * The pattern for postcode. Allows: Upper case english letters, numbers and
     * symbol -
     */
    @SuppressWarnings("nls")
    public static final String POSTCODE_PATTERN = "^[A-Z0-9-]+$";

    /**
     * The maximum allowed performer's name length.
     */
    public static final int PERFORMER_MAX_LENGTH = 30;

    /**
     * The minimum allowed performer's name length.
     */
    public static final int PERFORMER_MIN_LENGTH = 2;

    /**
     * The pattern for performer's name. Allows any language letter, numbers, and
     * symbols ('\"\\-~!@№$%€§^&*()_+.:;)
     */
    @SuppressWarnings("nls")
    public static final String PERFORMER_PATTERN = "[\\p{L}\\p{M}\\p{N}\\s'\"\\-~!@№$%€§^&*()_+.:;]+";

    /**
     * The maximum allowed review score decimal digits.
     */
    public static final int REVIEW_SCORE_ALLOWED_DECIMALS = 2;

    /**
     * The minimum allowed review score value.
     */
    public static final double REVIEW_SCORE_MIN = 0.00;

    /**
     * The maximum allowed review score value.
     */
    public static final double REVIEW_SCORE_MAX = 5.00;
    /**
     * The minimum allowed office's name length.
     */
    public static final int OFFICE_MIN_LENGTH = 5;
    /**
     * The maximum allowed office's name length.
     */
    public static final int OFFICE_MAX_LENGTH = 30;
    /**
     * The pattern for office name.
     */
    @SuppressWarnings("nls")
    public static final String OFFICE_NAME_PATTERN = "^[\\p{L} 0-9\"'\\.№,\\-]+$";

    /**
     * The pattern for status of available tickets.
     */
    @SuppressWarnings("nls")
    public static final String AVAILABLE_STATUS_TICKET = "AVAILABLE";

    /**
     * The pattern for status of reserved tickets.
     */
    @SuppressWarnings("nls")
    public static final String RESERVED_STATUS_TICKET = "RESERVED";

    /**
     * The minimum allowed payment name length.
     */
    public static final int PAYMENT_MIN_LENGTH = 5;
    /**
     * The maximum allowed payment name length.
     */
    public static final int PAYMENT_MAX_LENGTH = 30;
    /**
     * The pattern for payment name.
     */
    @SuppressWarnings("nls")
    public static final String PAYMENT_NAME_PATTERN = "[a-zA-Z _/]+";
    /**
     * The minimum allowed delivery name length.
     */
    public static final int DELIVERY_MIN_LENGTH = 5;
    /**
     * The maximum allowed delivery name length.
     */
    public static final int DELIVERY_MAX_LENGTH = 30;
    /**
     * The pattern for delivery name.
     */
    @SuppressWarnings("nls")
    public static final String DELIVERY_NAME_PATTERN = "[a-zA-Z _/]+";

    private Constants() {
        // Class for static constants. No instances.
    }
}
