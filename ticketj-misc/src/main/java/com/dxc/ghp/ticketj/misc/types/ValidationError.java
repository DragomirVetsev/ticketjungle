package com.dxc.ghp.ticketj.misc.types;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;

/**
 * @param <Value>
 *            The type of the value.
 * @param field
 *            The name of the problematic field.
 * @param value
 *            The incorrect value.
 * @param error
 *            The error that occurred during validating the given field.
 */
public record ValidationError<Value>(String field, Value value, String error) implements ErrorInfo {
}
