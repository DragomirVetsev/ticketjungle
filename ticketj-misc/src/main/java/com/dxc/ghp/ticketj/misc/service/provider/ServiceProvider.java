package com.dxc.ghp.ticketj.misc.service.provider;

import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;

/**
 * Functionality for retrieving registered service by its type. A new service
 * could be constructed every time we search.
 */
public interface ServiceProvider {
    /**
     * Retrieves registered application services by type.
     *
     * @param <Service>
     *            is the type of the wanted service.
     * @param service
     *            is the class of the wanted service. Must not be null.
     * @return an instance of the wanted service. Cannot be null.
     * @throws ServiceMissingException
     *             when the service cannot be found, and any exception from the
     *             method used to make the service.
     */
    <Service> Service find(final Class<Service> service);
}
