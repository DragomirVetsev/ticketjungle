package com.dxc.ghp.ticketj.misc.exceptions;

import java.io.Serializable;

/**
 * Used for error data transfer to client.
 */
public interface ErrorInfo extends Serializable {
    // Marker interface. No methods.
}
