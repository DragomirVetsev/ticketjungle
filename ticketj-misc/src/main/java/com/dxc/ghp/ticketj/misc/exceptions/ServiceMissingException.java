package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Custom exception when service is missing.
 */
public final class ServiceMissingException extends RuntimeException {

    private static final long serialVersionUID = -3422567182695972362L;
    private final String missingService;

    /**
     * Constructor which takes the name of the missing service.
     *
     * @param service
     *            The name of the missing service.
     */
    @SuppressWarnings("nls")
    public ServiceMissingException(final String service) {
        super(String.format("Service %s is missing.", service));
        missingService = service;
    }

    /**
     * Get method to return the name of the missing service.
     *
     * @return The name value of the missing service is being returned.
     */
    public String getMissingService() {
        return missingService;
    }
}
