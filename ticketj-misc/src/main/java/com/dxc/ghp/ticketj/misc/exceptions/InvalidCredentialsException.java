package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Custom exception used when the user send invalid username or password.
 */
public final class InvalidCredentialsException extends RuntimeException {

    private static final long serialVersionUID = -3065762913754768561L;

    private final ErrorInfo credentialsErrorInfo;

    /**
     * Constructs an exception when user send invalid credentials.
     * 
     * @param entity
     *            The entity class, must not be null.
     * @param message
     *            The explanation message.
     * @param cause
     *            The cause of the exception constructed.
     */
    public <ExceptionInfo> InvalidCredentialsException(final Class<?> entity, final ExceptionInfo message,
                                                       final Throwable cause) {
        super(message.toString(), cause);
        credentialsErrorInfo = new InvalidCredentialsInfo<>(entity.getSimpleName(), message);
    }

    /**
     * Displays short informative message.
     * 
     * @return Informative message about the cause of the exception.
     */
    public ErrorInfo getCredentialsErrorInfo() {
        return credentialsErrorInfo;
    }
}
