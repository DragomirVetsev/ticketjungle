package com.dxc.ghp.ticketj.misc.exceptions;

import com.dxc.ghp.ticketj.misc.types.EntityFieldType;

/**
 * Custom exception thrown when an entity field has an already existing value
 * (value of this field must be unique).
 */
public final class DuplicateFieldException extends RuntimeException {

    private static final long serialVersionUID = 6769896592405950692L;
    private final ErrorInfo entityErrorInfo;

    /**
     * Construct a custom exception for duplicate field within its class, field type
     * and field value.
     *
     * @param <Field>
     *            The field value type. Must not be null
     * @param entity
     *            The entity class. Must not be null.
     * @param field
     *            The entity field type. Must not be null.
     * @param fieldValue
     *            The field value. Must not be null.
     */
    public <Field> DuplicateFieldException(final Class<?> entity, final EntityFieldType field, final Field fieldValue) {
        super(constructMessage(entity, field, fieldValue));
        entityErrorInfo = new DuplicateFieldInfo<>(entity.getSimpleName(), field, fieldValue);
    }

    /**
     * Retrieves the duplicate entity record.
     *
     * @return The duplicate entity record, cannot be null.
     */
    public ErrorInfo getEntityErrorInfo() {
        return entityErrorInfo;
    }

    @SuppressWarnings("nls")
    private static <Field> String constructMessage(final Class<?> entity, final EntityFieldType field,
                                                   final Field fieldValue) {
        assert entity != null : "Entity must not be null!";
        assert field != null : "Field type must not be null!";
        assert fieldValue != null : "Field value must not be null!";

        return String
            .format("Entity %s with field type %s and value : %s already exists!", entity.getSimpleName(), field,
                    fieldValue.toString());
    }

}
