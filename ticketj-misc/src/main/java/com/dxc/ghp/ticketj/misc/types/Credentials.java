package com.dxc.ghp.ticketj.misc.types;

/**
 * Contains user credentials
 *
 * @param username
 *            the username of the user.
 * @param password
 *            the password of the user.
 */
public record Credentials(String username, String password) {

}
