package com.dxc.ghp.ticketj.misc.exceptions;

import com.dxc.ghp.ticketj.misc.types.EntityFieldType;

/**
 * Custom exception thrown when an entity is not found.
 */
public final class CustomerTicketNotAvailableException extends RuntimeException {

    private static final long serialVersionUID = -6188077912607285603L;
    private final ErrorInfo entityErrorInfo;

    /**
     * @param <Status>
     *            d
     * @param entity
     *            d
     * @param status
     *            d
     * @param fieldValue
     *            d
     */
    public <Status> CustomerTicketNotAvailableException(final Class<?> entity, final EntityFieldType status,
                                                        final Status fieldValue) {
        super(constructMessage(entity, status, fieldValue));
        entityErrorInfo = new DuplicateFieldInfo<>(entity.getSimpleName(), status, fieldValue);
    }

    /**
     * @return d
     */
    public ErrorInfo getEntityErrorInfo() {
        return entityErrorInfo;
    }

    @SuppressWarnings("nls")
    private static <Status> String constructMessage(final Class<?> entity, final EntityFieldType status,
                                                    final Status fieldValue) {
        assert entity != null : "Entity must not be null!";
        assert status != null : "Field type must not be null!";
        assert fieldValue != null : "Field value must not be null!";

        return String
            .format("Entity %s with field type %s is already %s!", entity.getSimpleName(), status,
                    fieldValue.toString());
    }

}
