package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Base class for all entity-related exceptions.
 */
public abstract class EntityException extends RuntimeException {

    private static final long serialVersionUID = 3760884145720742565L;
    private final ErrorInfo entityErrorInfo;

    /**
     * Construct a custom exception for duplicate entity with its class and primary
     * id.
     *
     * @param <Id>
     *            The entity id type.
     * @param entity
     *            The entity class, must not be null.
     * @param entityId
     *            The entity primary id, must not be null.
     * @param message
     *            The explanation message.
     * @param cause
     *            The cause of the exception constructed.
     */
    protected <Id> EntityException(final String message, final Class<?> entity, final Id entityId,
                                   final Throwable cause) {
        super(contructErrorMessage(message, entity, entityId), cause);
        entityErrorInfo = new EntityErrorInfo<>(entity.getSimpleName(), entityId);
    }

    /**
     * @param <Id>
     *            The generic type for the ID.
     * @param entity
     *            The problematic entity class.
     * @param entityId
     *            The ID for the problematic entity.
     * @param message
     *            The explanation message.
     * @return The combined message.
     */
    @SuppressWarnings("nls")
    private static <Id> String contructErrorMessage(final String message, final Class<?> entity, final Id entityId) {
        assert entity != null : "Entity must not be null!";
        assert entityId != null : "Entity id must not be null!";

        return String.format(message, entity, entityId.toString());
    }

    /**
     * Retrieves the duplicate entity record.
     *
     * @return The duplicate entity record, cannot be null.
     */
    public ErrorInfo getEntityErrorInfo() {
        return entityErrorInfo;
    }

}
