package com.dxc.ghp.ticketj.misc.service.provider;

import java.util.function.Function;

import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistry;

/**
 * Provider for services.
 *
 * @param <ConstructorArgument>
 *            is used for the service factory.
 */
public final class ServiceProviderImpl<ConstructorArgument> implements ServiceProvider {

    private final ConstructorArgument constructorArgument;
    private final ServiceRegistry<ConstructorArgument> registry;

    /**
     * @param constructorArgument
     *            the service constructor argument. Must not be null.
     * @param registry
     *            contains registered services. Must not be null.
     */
    @SuppressWarnings("nls")
    public ServiceProviderImpl(final ConstructorArgument constructorArgument,
                               final ServiceRegistry<ConstructorArgument> registry) {
        assert constructorArgument != null : "constructorArgument must not be null!";
        assert registry != null : "registry must not be null!";

        this.constructorArgument = constructorArgument;
        this.registry = registry;
    }

    @SuppressWarnings("nls")
    @Override
    public <Service> Service find(final Class<Service> service) {
        assert service != null : "service must not be null!";

        final Function<ConstructorArgument, ?> constructor = registry.findServiceConstructor(service);
        if (constructor != null) {
            final Object registeredService = constructor.apply(constructorArgument);
            if (service.isInstance(registeredService)) {
                return service.cast(registeredService);
            }
        }
        throw new ServiceMissingException(service.getSimpleName());
    }

}
