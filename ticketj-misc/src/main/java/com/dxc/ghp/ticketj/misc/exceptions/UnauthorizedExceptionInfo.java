package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Record used to pass information about entity when error occurs.
 * 
 * @param message
 *            is informative text about the the occurred error.
 */
public record UnauthorizedExceptionInfo(String message) implements ErrorInfo {

}
