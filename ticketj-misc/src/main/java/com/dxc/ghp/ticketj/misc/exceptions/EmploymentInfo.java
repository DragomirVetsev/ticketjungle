package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Contains worker's employment information (place and username).
 *
 * @param officeName
 *            The office unique id where the worker is employed.
 * @param workerUsername
 *            The worker unique id.
 */
public record EmploymentInfo(String officeName, String workerUsername) implements ErrorInfo {

}
