package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Record used to pass information about entity when error occurs.
 * 
 * @param <ExceptionInfo>
 *            Information about the error.
 * @param entity
 *            The entity name. Must not be null.
 * @param message
 *            Short message about the error. Must not be null.
 */
public record InvalidCredentialsInfo<ExceptionInfo>(String entity, ExceptionInfo message) implements ErrorInfo {

}
