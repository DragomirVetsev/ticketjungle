package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Used to pass information to client about back-end error.
 *
 * @param <ErrorDetails>
 *            error details type.
 * @param type
 *            The type of error.
 * @param details
 *            The details of error.
 */
public record Error<ErrorDetails>(ErrorType type, ErrorDetails details) {

}
