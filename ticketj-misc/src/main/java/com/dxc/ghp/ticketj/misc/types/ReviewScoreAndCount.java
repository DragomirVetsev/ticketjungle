package com.dxc.ghp.ticketj.misc.types;

/**
 * Contains a sub-event's score and the count of people who have given a score
 * to it.
 *
 * @param score
 *            is the sub-event's review average score.
 * @param count
 *            is the count of the sub-event's scores.
 */
public record ReviewScoreAndCount(double score, int count) {

}
