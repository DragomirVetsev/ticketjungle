package com.dxc.ghp.ticketj.misc.types;

/**
 * Enum representing different payment types for purchase orders in the system.
 */
@SuppressWarnings("nls")
public enum PaymentTypeEnum {
    /**
     * Represents a bank transfer payment order.
     */
    BANK_TRANSFER("BANK TRANSFER"),
    /**
     * Represents awaiting payment for purchase order.
     */
    CARD("DEBIT/CREDIT CARD"),
    /**
     * Represents awaiting payment purchase order.
     */
    CASH("CASH");

    private final String value;

    PaymentTypeEnum(final String value) {
        this.value = value;
    }

    /**
     * Returns the value connected to the Enum.
     *
     * @return the value of the Enum.
     */
    public String value() {
        return value;
    }
}
