package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Custom exception when authenticated user is trying to access resource with
 * required role different than his.
 */
public final class ForbiddenAccessException extends RuntimeException {

    private static final long serialVersionUID = 1738248234077181904L;
    private final ErrorInfo errorInfo;

    /**
     * Construct a custom exception for unauthorized users.
     */
    @SuppressWarnings("nls")
    public ForbiddenAccessException() {
        super();
        errorInfo = new ForbiddenAccessExceptionInfo("User not allowed!");
    }

    /**
     * @return informative message to clarify the reason of the error.
     */
    public ErrorInfo getErrorInfo() {
        return errorInfo;
    }
}
