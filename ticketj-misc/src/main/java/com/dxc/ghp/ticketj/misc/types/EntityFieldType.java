package com.dxc.ghp.ticketj.misc.types;

/**
 * Contains all field types. Used to pass the field name when necessary.
 */
public enum EntityFieldType {

    /**
     * Field type - Username.
     */
    USERNAME,
    /**
     * Field type - Password for cridentials.
     */
    PASSWORD,
    /**
     * Field type - Credentials containing username and password.
     */
    CREDENTIALS,
    /**
     * Field type - persons given name and family name.
     */
    PERSON_NAME,
    /**
     * Field type - Person's given name.
     */
    PERSON_GIVEN_NAME,
    /**
     * Field type - Person's family name.
     */
    PERSON_FAMALY_NAME,
    /**
     * Field type - Email.
     */
    EMAIL,
    /**
     * Field type - Phone.
     */
    PHONE,
    /**
     * Field type - Only the phone code.
     */
    PHONE_CODE,
    /**
     * Field type - Phone number with out the code.
     */
    PHONE_NUMBER,
    /**
     * Field type - Address.
     */
    ADDRESS,

    /**
     * Field type - list of performers'names;
     */
    PERFORMERS_NAMES,

    /**
     * Field type - review score.
     */
    REVIEW_SCORE,

    /**
     * Field type - review count.
     */
    REVIEW_COUNT,

    /**
     * The name of the field city.
     */

    CITY,
    /**
     * The event field name.
     */
    EVENT,

    /**
     * The sub-event field name.
     */
    SUB_EVENT,

    /**
     * The name of description name.
     */
    DESCRIPTION,

    /**
     * The name of the venue field.
     */
    VENUE,

    /**
     * The name of the stage field.
     */
    STAGE,

    /**
     * Common field name used in test classes.
     */
    TEST,

    /**
     * The end time field name.
     */
    END_TIME,

    /**
     * The start time field name.
     */
    START_TIME,

    /**
     * The ticket price field name.
     */
    TICKET_PRICE,

    /**
     * The street field name.
     */
    STREET,

    /**
     * The postcode field name.
     */
    POST_CODE,

    /**
     * The name of the field genre.
     */
    GENRE,
    /**
     * Field type - performer.
     */
    PERFORMER,
    /**
     * The name field type.
     */
    NAME,
    /**
     * The sub-event details field.
     */
    SUB_EVENT_DETAILS,
    /**
     * The period field type.
     */
    TIME_PERIOD,
    /**
     * Office workers.
     */
    WORKERS,
    /**
     * Payment type.
     */
    PAYMENT_TYPE,
    /**
     * Delivery type.
     */
    DELIVERY_TYPE;
}
