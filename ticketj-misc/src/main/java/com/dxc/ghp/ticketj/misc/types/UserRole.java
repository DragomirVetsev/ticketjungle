package com.dxc.ghp.ticketj.misc.types;

/**
 * Enum representing different roles for users in the system.
 */
public enum UserRole {

    /**
     * Represents a regular customer role
     */
    CUSTOMER,

    /**
     * Represents an admin role with higher privileges.
     */
    ADMIN,

    /**
     * Represents an office worker role.
     */
    OFFICE_WORKER;
}
