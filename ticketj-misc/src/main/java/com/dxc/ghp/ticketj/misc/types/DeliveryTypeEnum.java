package com.dxc.ghp.ticketj.misc.types;

/**
 * Enum representing different delivery types for purchase orders in the system.
 */
public enum DeliveryTypeEnum {

    /**
     * Represents a home delivery type.
     */
    HOME_DELIVERY,

    /**
     * Represents an office delivery type.
     */
    OFFICE_DELIVERY,

    /**
     * Represents an email delivery type.
     */
    EMAIL_DELIVERY;
}
