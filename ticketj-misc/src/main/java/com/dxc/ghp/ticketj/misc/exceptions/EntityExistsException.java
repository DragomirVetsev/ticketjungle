package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Custom exception when entity is duplicate.
 */
public final class EntityExistsException extends EntityException {

    private static final long serialVersionUID = -2167750791358010736L;

    /**
     * Construct a custom exception for duplicate entity with its class and primary
     * id.
     *
     * @param <ID>
     *            The entity id type.
     * @param entity
     *            The entity class, must not be null.
     * @param entityId
     *            The entity primary id, must not be null.
     * @param cause
     *            The cause of the exception constructed.
     */
    @SuppressWarnings("nls")
    public <ID> EntityExistsException(final Class<?> entity, final ID entityId, final Throwable cause) {
        super("Entity %s with ID: %s already exists!", entity, entityId, cause);
    }
}
