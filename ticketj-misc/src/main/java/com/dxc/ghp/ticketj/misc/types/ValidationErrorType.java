package com.dxc.ghp.ticketj.misc.types;

/**
 * Contains validation error type.
 */
public enum ValidationErrorType {

    /**
     * Null validation type.
     */
    @SuppressWarnings("nls")
    NULL("Null value"),
    /**
     * Null validation type.
     */
    @SuppressWarnings("nls")
    IDENTICAL("The filds are identical"),
    /**
     * Null values validation type.
     */
    @SuppressWarnings("nls")
    HAS_NULL_VALUES("Has null values"),
    /**
     * Invalid length type.
     */
    @SuppressWarnings("nls")
    INVALID_LENGTH("Invalid length"),
    /**
     * Invalid symbols type.
     */
    @SuppressWarnings("nls")
    INVALID_SYMBOLS("Invalid symbols"),

    /**
     * Negative integer type.
     */
    @SuppressWarnings("nls")
    NEGATIVE_INTEGER("Negative integer"),

    /**
     * Negative double type.
     */
    @SuppressWarnings("nls")
    NEGATIVE_DOUBLE("Negative double"),

    /**
     * Incorrect order of dates.
     */
    @SuppressWarnings("nls")
    INCORRECT_DATE_TIME_ORDER("Incorrect DateTime order"),

    /**
     * Incorrect number of decimal numbers.
     */
    @SuppressWarnings("nls")
    INVALID_NUMBER_OF_DECIMALS("Invalid number of decimals after decimal point"),

    /**
     * Out of range.
     */
    @SuppressWarnings("nls")
    OUT_OF_RANGE("Out of range");

    /**
     * The message for the given enumeration.
     */
    public final String errorType;

    ValidationErrorType(final String errorType) {
        this.errorType = errorType;
    }

}
