package com.dxc.ghp.ticketj.misc.types;

import java.time.temporal.Temporal;

/**
 * @param <Time>
 *            The type which sets the range of time.
 * @param startTime
 *            The start time of a given period creating a custom period in.
 * @param endTime
 *            The end time of a given period.
 */
public record Period<Time extends Temporal>(Time startTime, Time endTime) {
}
