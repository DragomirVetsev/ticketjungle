package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Custom exception thrown when an entity is not found.
 */
public final class EntityNotFoundException extends EntityException {
    private static final long serialVersionUID = 2582915186215879276L;

    /**
     * Constructs exception with missing entity name and id.
     *
     * @param <ID>
     *            The entity id type.
     * @param entity
     *            The entity class, must not be null.
     * @param entityId
     *            The entity primary id, must not be null.
     */
    @SuppressWarnings("nls")
    public <ID> EntityNotFoundException(final Class<?> entity, final ID entityId) {
        super("Entity %s with ID: %s is not found!", entity, entityId, null);
    }
}
