package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Contains all Error types.
 */
public enum ErrorType {
    /**
     * Service missing error.
     */
    MISSING_SERVICE,
    /**
     * Entity not found error.
     */
    ENTITY_NOT_FOUND,
    /**
     * Entity already exists error.
     */
    ENTITY_EXISTS,
    /**
     * Entity field value already exist (when value must be unique).
     */
    DUPLICATE_FIELD,

    /**
     * Entity doesn't belong to other entity.
     */
    EVENT_MISMATCH,

    /**
     * Validation error.
     */
    VALIDATION_ERROR,

    /**
     * Unknown error.
     */
    UNKNOWN_ERROR,

    /**
     * Invalid credentials error.
     */
    INVALID_CREDENTIALS,

    /**
     * Unauthorized user error.
     */
    UNAUTHORIZED,
    /**
     * Forbidden access to method source.
     */
    FORBIDDEN,
    /**
     * Entity has relationships and cannot be changed.
     */
    HAS_RELATIONSHIP,

    /**
     * Office worker is not available for assigning to an office since he/she is
     * already assigned.
     */
    ALREADY_EMPOYED;

}
