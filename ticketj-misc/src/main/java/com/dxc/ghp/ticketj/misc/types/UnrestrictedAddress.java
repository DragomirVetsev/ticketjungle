/**
 *
 */
package com.dxc.ghp.ticketj.misc.types;

/**
 * Contains address.
 *
 * @param street
 *            the address street.
 * @param city
 *            the city name.
 * @param postCode
 *            the address post code.
 */
public record UnrestrictedAddress(String street, String city, String postCode) {
}
