package com.dxc.ghp.ticketj.misc.types;

import java.io.Serializable;

/**
 * Record containing the name of the sub-event and the name of the event.
 *
 * @param name
 *            The name of sub-event.
 * @param eventName
 *            The name of the event containing the sub-event.
 */
public record PerformanceId(String name, String eventName) implements Serializable {

}
