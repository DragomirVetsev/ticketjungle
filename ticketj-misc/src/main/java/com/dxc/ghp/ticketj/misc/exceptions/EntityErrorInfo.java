package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Record used to pass information about entity when error occurs.
 *
 * @param <Id>
 *            The type of entity id.
 * @param entity
 *            The entity name. Must not be null.
 * @param entityId
 *            Entity's unique id. Must not be null.
 */
public record EntityErrorInfo<Id>(String entity, Id entityId) implements ErrorInfo {
}
