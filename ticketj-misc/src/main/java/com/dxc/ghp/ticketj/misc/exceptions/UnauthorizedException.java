package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Custom exception when unauthorized user is trying to access authenticated
 * URL.
 */
public final class UnauthorizedException extends RuntimeException {

    private static final long serialVersionUID = -8622616309900140717L;

    private final ErrorInfo errorInfo;

    /**
     * Construct a custom exception for unauthorized users.
     * 
     * @param message
     *            is informative text about the the occurred error.
     * @param cause
     *            The cause of the exception constructed.
     */
    public UnauthorizedException(final String message, final Throwable cause) {
        super(message, cause);
        errorInfo = new UnauthorizedExceptionInfo(message);
    }

    /**
     * Construct a custom exception for unauthorized users.
     */
    @SuppressWarnings("nls")
    public UnauthorizedException() {
        super();
        errorInfo = new UnauthorizedExceptionInfo("Unauthorized User!");
    }

    /**
     * @return informative message to clarify the reason of the error.
     */
    public ErrorInfo getErrorInfo() {
        return errorInfo;
    }
}
