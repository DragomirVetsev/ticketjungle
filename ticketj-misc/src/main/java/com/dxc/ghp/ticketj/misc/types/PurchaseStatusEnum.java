package com.dxc.ghp.ticketj.misc.types;

/**
 * Enum representing different statuses for purchase orders in the system.
 */
public enum PurchaseStatusEnum {

    /**
     * Represents a completed purchase order.
     */
    COMPLETED,

    /**
     * Represents awaiting payment purchase order.
     */
    AWAITING_PAYMENT,
}
