package com.dxc.ghp.ticketj.misc.types;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;

/**
 * @param <FirstValue>
 *            The type of the first value.
 * @param <SecondValue>
 *            The type of the second value.
 * @param firstValueName
 *            The name of the first value.
 * @param firstValue
 *            The first value.
 * @param secondValueName
 *            The name of the second value.
 * @param secondValue
 *            The second value.
 * @param error
 *            The error that occurred during validating the given fields.
 */
public record BiValidationError<FirstValue, SecondValue>(String firstValueName, FirstValue firstValue,
                                                         String secondValueName, SecondValue secondValue, String error)
    implements ErrorInfo {

}
