package com.dxc.ghp.ticketj.misc.exceptions;

/**
 * Custom exception thrown when assigning an office worker to an office when the
 * worker is already assigned to a different office.
 */
public final class WorkerUnavailableException extends RuntimeException {

    private static final long serialVersionUID = 4956843473878297109L;
    private final ErrorInfo entityErrorInfo;

    /**
     * Construct a custom exception for office worker already being employed by a
     * different office.
     *
     * @param officeName
     *            The office where the worker is already employed.
     * @param workerUsername
     *            The worker's username.
     */
    public WorkerUnavailableException(final String officeName, final String workerUsername) {
        super(constructMessage(officeName, workerUsername));

        entityErrorInfo = new EmploymentInfo(officeName, workerUsername);
    }

    @SuppressWarnings("nls")
    private static String constructMessage(final String officeName, final String workerUsername) {
        assert officeName != null : "Office name must not be null!";
        assert workerUsername != null : "Office worker must not be null!";

        return String
            .format("Worker with username '%s' is already assigned to Office '%s'", workerUsername, officeName);
    }

    /**
     * Retrieves worker's employment information.
     *
     * @return The worker's employment information. Cannot be null.
     */
    public ErrorInfo getEmploymentInfo() {
        return entityErrorInfo;
    }
}
