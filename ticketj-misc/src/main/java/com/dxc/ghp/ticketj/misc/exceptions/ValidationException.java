package com.dxc.ghp.ticketj.misc.exceptions;

import java.util.Collection;
import java.util.Collections;

/**
 * Validation exception containing the list of errors after validation.
 */
public final class ValidationException extends RuntimeException {

    private static final long serialVersionUID = 3736047340632633545L;
    private final Collection<ErrorInfo> errors;

    /**
     * Constructs validation exception containing errors and their causes.
     *
     * @param errors
     *            The list with collected errors.
     */
    public ValidationException(final Collection<ErrorInfo> errors) {
        super(constructMessage(errors));
        this.errors = Collections.unmodifiableCollection(errors);
    }

    @SuppressWarnings("nls")
    private static String constructMessage(final Collection<ErrorInfo> errors) {
        assert errors != null : "Errors must not be null!";

        return errors
            .stream()
            .map(Object::toString)
            .reduce("Failed validations: ", (acc, comb) -> acc + System.lineSeparator() + comb);
    }

    /**
     * Retrieves the list of the breaches occurred during validation.
     *
     * @return The list of errors.
     */
    public Collection<ErrorInfo> getErrors() {
        return Collections.unmodifiableCollection(errors);
    }

}
