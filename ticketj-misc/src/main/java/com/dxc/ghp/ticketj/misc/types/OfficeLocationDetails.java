package com.dxc.ghp.ticketj.misc.types;

/**
 * Contains only city and street.
 *
 * @param city
 *            The office city.
 * @param street
 *            The office street.
 */
public record OfficeLocationDetails(String city, String street) {

}
