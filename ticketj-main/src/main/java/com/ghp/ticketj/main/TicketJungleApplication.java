package com.ghp.ticketj.main;

import java.util.logging.Logger;

import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.persistence.jpa.layers.PersistenceLayer;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.service.impl.layers.ServiceLayer;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

/**
 * Ticket Jungle initialization module.
 */
@WebListener
public final class TicketJungleApplication implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(TicketJungleApplication.class.getName());
    private PersistenceLayer persistenceLayer;

    @SuppressWarnings("nls")
    @Override
    public void contextInitialized(final ServletContextEvent event) {
        LOGGER.info("Ticket Jungle initialization starting...");

        persistenceLayer = new PersistenceLayer();
        final TransactionHandler transactionHandler = persistenceLayer.createTransactionHandler();

        final ServletContext context = event.getServletContext();

        context.setAttribute(ServiceProvider.class.getName(), ServiceLayer.createServiceProvider(transactionHandler));

        LOGGER.info("Ticket Jungle initialization completed successfully.");
    }

    @SuppressWarnings("nls")
    @Override
    public void contextDestroyed(final ServletContextEvent event) {
        LOGGER.info("Ticket Jungle stoping...");

        if (persistenceLayer != null) {
            persistenceLayer.release();
        }

        LOGGER.info("Ticket Jungle stopped successfully.");
    }
}
