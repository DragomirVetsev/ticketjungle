/**
 * Contains all Ticket Jungle Data Transfer Objects.
 */
package com.dxc.ghp.ticketj.dto;
