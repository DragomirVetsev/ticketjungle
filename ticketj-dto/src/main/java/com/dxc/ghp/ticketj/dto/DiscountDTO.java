package com.dxc.ghp.ticketj.dto;

/**
 * Discount data transfer object.
 *
 * @param name
 *            The name of discount (ex. 'KID', 'STUDENT').
 * @param percentage
 *            The percentage of the discount.
 */
public record DiscountDTO(String name, double percentage) {

}
