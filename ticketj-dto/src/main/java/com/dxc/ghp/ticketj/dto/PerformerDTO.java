package com.dxc.ghp.ticketj.dto;

/**
 * Performer data transfer object.
 *
 * @param name
 *            of the performer. Must not be null.
 * @param description
 *            of the performer. Must not be null.
 */
public record PerformerDTO(String name, String description) {

}
