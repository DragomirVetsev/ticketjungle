package com.dxc.ghp.ticketj.dto;

import java.util.List;

import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;

/**
 * Sub-event data transfer object.
 *
 * @param performanceId
 *            is the id of the sub-event containing the name of the sub-event
 *            and the name of the Event. Must not be null
 * @param performanceDetails
 *            the details about the venue, stage, and start time.
 * @param description
 *            of the sub-event. Must not be null.
 * @param ticketPrice
 *            of the sub-event. Must not be negative.
 * @param venueAddress
 *            the address of the venue. Must not be null.
 * @param performers
 *            is a list of all performers. Must not be null.
 * @param reviewScoreAndCount
 *            {@link ReviewScoreAndCount} is the average score and review count.
 *            Must not be negative. Must not be null.
 */
public record SubEventDTO(PerformanceId performanceId, PerformanceDetails performanceDetails, String description,
                          double ticketPrice, UnrestrictedAddress venueAddress, List<String> performers,
                          ReviewScoreAndCount reviewScoreAndCount) {
}
