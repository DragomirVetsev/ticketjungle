package com.dxc.ghp.ticketj.dto;

import java.util.List;

/**
 * Pair of ordered list of sub-events by time and popularity.
 *
 * @param orderedByTime
 *            is the list of sub-events ordered by time.
 * @param orderedByPopularity
 *            is the list of sub-events ordered by popularity.
 */
public record SubEventOrderedPairDTO(List<SubEventDTO> orderedByTime, List<SubEventDTO> orderedByPopularity) {

}
