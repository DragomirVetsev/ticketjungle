package com.dxc.ghp.ticketj.dto;

import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;

/**
 * Ticket data transfer object.
 *
 * @param orderId
 *            The id of the purchase order.
 * @param performanceId
 *            is the id of the sub-event which contains the main event name and
 *            the name of the {@literal sub-event}.
 * @param subEventDescription
 *            The description of the {@literal sub-event}.
 * @param performanceDetails
 *            The performance of the {@literal sub-event}. Contains venue, stage
 *            and startTime.
 * @param sector
 *            The sector of the row.
 * @param row
 *            The row of the seat.
 * @param seat
 *            The ticket seat. Contains seat number and status.
 * @param price
 *            The price of the ticket.
 * @param discount
 *            The ticket discount.
 */
public record TicketDTO(int orderId, PerformanceId performanceId, String subEventDescription,
                        PerformanceDetails performanceDetails, String sector, int row, SeatDTO seat, double price,
                        DiscountDTO discount) {

}
