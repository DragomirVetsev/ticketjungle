package com.dxc.ghp.ticketj.dto;

import java.util.List;

/**
 * Sector data transfer object.
 *
 * @param name
 *            The name of the sector.
 * @param rows
 *            The rows in the sector.
 */
public record SectorDTO(String name, List<RowDTO> rows) {
}
