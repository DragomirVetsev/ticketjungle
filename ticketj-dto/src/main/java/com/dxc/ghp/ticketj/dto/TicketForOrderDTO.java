package com.dxc.ghp.ticketj.dto;

import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;

/**
 * Transfer object for ticket for ordering.
 *
 * @param event
 *            name of the {@literal event}
 * @param subEvent
 *            The name of the {@literal sub-event}.
 * @param performance
 *            record contains place and time of sub-event.
 * @param sector
 *            The sector of the row.
 * @param row
 *            The row of the seat.
 * @param seatNumber
 *            The ticket seat number.
 * @param discount
 *            The discount for the ticket.
 */
public record TicketForOrderDTO(String event, String subEvent, PerformanceDetails performance, String sector, int row,
                                int seatNumber, String discount) {

}
