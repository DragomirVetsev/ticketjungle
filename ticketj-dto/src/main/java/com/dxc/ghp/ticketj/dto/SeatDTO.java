package com.dxc.ghp.ticketj.dto;

/**
 * Seat data transfer object.
 *
 * @param number
 *            The number of the seat.
 * @param status
 *            The status of the seat.
 */
public record SeatDTO(int number, String status) {
}
