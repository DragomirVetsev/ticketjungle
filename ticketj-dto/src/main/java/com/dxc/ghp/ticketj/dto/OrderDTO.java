package com.dxc.ghp.ticketj.dto;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;

/**
 * Order data transfer object.
 *
 * @param purchaseStatus
 *            The status of the purchase order.
 * @param username
 *            The username of the purchase order.
 * @param personName
 *            The {@link PersonName} of the purchase order
 * @param paymentType
 *            The payment type of the purchase order.
 * @param creationDate
 *            The creation day of the purchase order.
 * @param deliveryType
 *            The delivery type of the purchase order.
 * @param unrestrictedAddress
 *            The address of the purchase order.
 * @param tickets
 *            Set of tickets connected to the purchase order.
 */
public record OrderDTO(String purchaseStatus, String username, PersonName personName, String paymentType,
                       Instant creationDate, String deliveryType, UnrestrictedAddress unrestrictedAddress,
                       List<TicketDTO> tickets) {
    /**
     * Constructor for the record so we can assign a copy of tickets. That way the
     * ticket in the record can't be changed after initial set.
     *
     * @param purchaseStatus
     *            The status of the purchase order.
     * @param username
     *            The username of the purchase order.
     * @param personName
     *            The {@link PersonName} of the purchase order
     * @param paymentType
     *            The payment type of the purchase order.
     * @param creationDate
     *            The creation day of the purchase order.
     * @param deliveryType
     *            The delivery type of the purchase order.
     * @param unrestrictedAddress
     *            The address of the purchase order.
     * @param tickets
     *            Set of tickets connected to the purchase order.
     */
    public OrderDTO(final String purchaseStatus, final String username, final PersonName personName,
                    final String paymentType, final Instant creationDate, final String deliveryType,
                    final UnrestrictedAddress unrestrictedAddress, final List<TicketDTO> tickets) {

        this.purchaseStatus = purchaseStatus;
        this.username = username;
        this.personName = personName;
        this.paymentType = paymentType;
        this.creationDate = creationDate;
        this.deliveryType = deliveryType;
        this.unrestrictedAddress = unrestrictedAddress;
        this.tickets = List.copyOf(tickets);
    }

    /**
     * Retrieves the tickets connected to the order.
     *
     * @return the tickets connected for the order. Cannot be null.
     */
    @Override
    public List<TicketDTO> tickets() {
        return Collections.unmodifiableList(tickets);
    }
}
