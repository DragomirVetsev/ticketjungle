package com.dxc.ghp.ticketj.dto;

import java.util.ArrayList;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;

/**
 * Purchase order data transfer object. The status of the purchase order.
 *
 * @param deliveryType
 *            The delivery type of the purchase order.
 * @param deliveryAddress
 *            The delivery address of the purchase order.
 * @param paymentType
 *            The payment type of the purchase order.
 * @param ticketsForOrderDTOs
 *            The list of tickets associated with the purchase order
 */
public record OrderWithTicketsDTO(String paymentType, String deliveryType, UnrestrictedAddress deliveryAddress,
                                  List<TicketForOrderDTO> ticketsForOrderDTOs) {

    /**
     * Used to return copy of ticketDTOs.
     *
     * @return copy of ticketDTOs.
     */
    public List<TicketForOrderDTO> ticketDTOs() {
        return new ArrayList<>(ticketsForOrderDTOs);
    }

}
