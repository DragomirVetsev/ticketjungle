package com.dxc.ghp.ticketj.dto;

import java.util.List;

import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;

/**
 * Stage data transfer object.
 *
 * @param venueName
 *            The name of the venue.
 * @param stageName
 *            The name of the stage.
 * @param address
 *            The address of the venue.
 * @param sectors
 *            The sectors in around the stage.
 */
public record StageDTO(String venueName, UnrestrictedAddress address, String stageName, List<SectorDTO> sectors) {
}
