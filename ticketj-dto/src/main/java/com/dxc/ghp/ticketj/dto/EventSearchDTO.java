/**
 *
 */
package com.dxc.ghp.ticketj.dto;

import java.time.Instant;

import com.dxc.ghp.ticketj.misc.types.Period;

/**
 * Criteria data transfer object used for searching events.
 *
 * @param city
 *            The location wear the Event is held.
 * @param genre
 *            The type of the Event that is held.
 * @param duration
 *            Of the event from start date type{@link Instant} and end date of
 *            type{@link Instant}.
 */
public record EventSearchDTO(String city, String genre, Period<Instant> duration) {
}
