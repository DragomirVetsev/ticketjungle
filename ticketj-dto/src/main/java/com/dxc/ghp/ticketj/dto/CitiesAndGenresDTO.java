package com.dxc.ghp.ticketj.dto;

import java.util.List;

/**
 * Data transfer object used for transporting parameters which are going to used
 * to search events by criteria.
 *
 * @param genres
 *            Off all existing Events. If the set is empty then their are no
 *            Genres found.
 * @param cities
 *            Off all existing Venues. If the set is empty then their are no
 *            cities found.
 */
public record CitiesAndGenresDTO(List<String> genres, List<String> cities) {
}
