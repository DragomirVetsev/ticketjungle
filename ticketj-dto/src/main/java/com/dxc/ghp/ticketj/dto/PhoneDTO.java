package com.dxc.ghp.ticketj.dto;

/**
 * Phone number details
 *
 * @param code
 *            the phone code
 * @param number
 *            the phone number
 */
public record PhoneDTO(String code, String number) {
}
