package com.dxc.ghp.ticketj.dto;

/**
 * @param userInfo
 *            contains the username and the role of a customer.
 * @param jwtToken
 *            user`s JWT authentication token.
 */
public record UserJwtDTO(UserInfoDTO userInfo, String jwtToken) {
}
