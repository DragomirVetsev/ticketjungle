package com.dxc.ghp.ticketj.dto;

import java.time.Instant;

/**
 * Represents a Review Data Transfer Object (DTO) for one review of a Sub Event.
 *
 * @param subEventName
 *            regarding the specific review.
 * @param eventName
 *            the associated main event.
 * @param venue
 *            where the Sub event played.
 * @param stage
 *            where the Sub event played.
 * @param startTime
 *            when the Sub event start.
 * @param comment
 *            provided by the customer in the review. Can be null if the
 *            customer does not comment the event, but give a score.
 * @param score
 *            given by the customer in the review (on a scale of 1 to 5
 *            "stars"). Can be null if the customer does not score the event,
 *            but give a comment.
 */
public record ReviewDTO(String subEventName, String eventName, String venue, String stage, Instant startTime,
                        String comment, Integer score) {
}
