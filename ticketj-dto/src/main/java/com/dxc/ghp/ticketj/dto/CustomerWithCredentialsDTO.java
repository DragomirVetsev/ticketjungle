package com.dxc.ghp.ticketj.dto;

import java.time.LocalDate;

import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;

/**
 * @param credentials
 *            The credentials are the customers {@code username} and
 *            {@code password} as a {@link Credentials} {@code record}.
 * @param personName
 *            Are the customers given name and family name as a
 *            {@link PersonName} {@code record}..
 * @param email
 *            It the email address of the customer.
 * @param phoneNumber
 *            Is the customers phone code and number as a {@link PhoneDTO}
 *            {@code record}..
 * @param address
 *            Is the customers city, street and post code as a
 *            {@link UnrestrictedAddress} {@code record}.
 * @param birthday
 *            Is the date of birth of the customer,
 */
public record CustomerWithCredentialsDTO(Credentials credentials, PersonName personName, String email,
                                         PhoneDTO phoneNumber, UnrestrictedAddress address, LocalDate birthday) {

}
