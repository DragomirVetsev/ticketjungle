package com.dxc.ghp.ticketj.dto;

import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;

/**
 * Reserved Ticket data transfer object.
 *
 * @param event
 *            name of the {@literal event}
 * @param subEvent
 *            The name of the {@literal sub-event}.
 * @param performance
 *            record contains place and time of sub-event.
 * @param sector
 *            The sector of the row.
 * @param row
 *            The row of the seat.
 * @param seat
 *            The ticket seat. Contains seat number and status.
 * @param price
 *            The price of the ticket.
 */
public record ReservedTicketDTO(String event, String subEvent, PerformanceDetails performance, String sector, int row,
                                SeatDTO seat, double price) {

}
