package com.dxc.ghp.ticketj.dto;

import java.time.LocalTime;
import java.util.List;

import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;

/**
 * Office data transfer object.
 *
 * @param officeName
 *            The name of the office, must not be null.
 * @param officeAddress
 *            office's {@link UnrestrictedAddress} details.
 * @param phone
 *            The Office phone.
 * @param email
 *            The Office email.
 * @param workingHours
 *            The Office working hours.
 * @param workers
 *            List of all Office workers.
 */
public record OfficeDTO(String officeName, UnrestrictedAddress officeAddress, PhoneDTO phone, String email,
                        Period<LocalTime> workingHours, List<String> workers) {
}
