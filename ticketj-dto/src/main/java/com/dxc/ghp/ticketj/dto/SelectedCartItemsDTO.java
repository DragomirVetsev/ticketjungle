package com.dxc.ghp.ticketj.dto;

import java.util.List;

/**
 * Selected Cart Items data transfer object.
 *
 * @param reservedTicket
 *            the reserved ticket related to cart item. Empty list corresponds
 *            to not selected cart items or empty shopping cart of Customer.
 */
public record SelectedCartItemsDTO(List<ReservedTicketDTO> reservedTicket) {

}
