package com.dxc.ghp.ticketj.dto;

import java.util.Objects;

/**
 * Event data transfer object.
 */
public final class EventDTO {

    private final String name;
    private final String description;
    private final String genre;

    /**
     * Initialize a EventDto with the specified characteristics.
     *
     * @param name
     *            The name of the event. Must not be null.
     * @param description
     *            The description of the event. Must not be null.
     * @param genre
     *            The genre of the event. Must not be null.
     */

    public EventDTO(final String name, final String description, final String genre) {
        this.name = name;
        this.description = description;
        this.genre = genre;
    }

    /**
     * Retrieves the name of the event.
     *
     * @return The name of the event. Cannot be null.
     */
    public String getName() {
        return name;
    }

    /**
     * Retrieves the description of the event.
     *
     * @return The description of the event. Cannot be null.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Retrieves the genre of the event.
     *
     * @return The genre of the event. Cannot be null.
     */
    public String getGenre() {
        return genre;
    }

    @SuppressWarnings("nls")
    @Override
    public String toString() {
        return "EventDTO{" + "name='" + name + '\'' + ", description='" + description + '\'' + ", genre='" + genre
            + '\'' + '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, genre);
    }

    @Override
    public boolean equals(final Object other) {
        return (other instanceof final EventDTO that)
            && Objects.equals(name, that.name)
            && Objects.equals(description, that.description)
            && Objects.equals(genre, that.genre);
    }
}
