package com.dxc.ghp.ticketj.dto;

import com.dxc.ghp.ticketj.misc.types.PerformanceId;

/**
 * Transfer object for ticket for ordering.
 *
 * @param username
 *            unique of the customer who want to reserved tickets.
 * @param performance
 *            record contains event and subevent name.
 * @param venue
 *            the venue of the stage
 * @param stage
 *            the stage of the sector
 * @param sector
 *            The sector of the row.
 * @param row
 *            The row of the seat.
 * @param seat
 *            The ticket seat number and Status.
 */
public record CartItemDTO(String username, PerformanceId performance, String venue, String stage, String sector,
                          int row, SeatDTO seat) {

}
