package com.dxc.ghp.ticketj.dto;

import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;

/**
 * @param username
 *            user's username
 * @param personName
 *            user's name object {@link PersonName}.
 * @param email
 *            user's email
 * @param phoneNumber
 *            user's {@link PhoneDTO} object.
 * @param address
 *            user's {@link UnrestrictedAddress} details.
 * @param role
 *            user's role details.
 */
public record UserDTO(String username, PersonName personName, String email, PhoneDTO phoneNumber,
                      UnrestrictedAddress address, UserRole role) {

}
