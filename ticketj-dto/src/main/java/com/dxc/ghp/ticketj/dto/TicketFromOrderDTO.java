/**
 *
 */
package com.dxc.ghp.ticketj.dto;

/**
 * @param ticketDto
 *            record with ticket details.
 * @param priceWithoutDiscount
 *            the price of the ticket before any discounts.
 */
public record TicketFromOrderDTO(TicketDTO ticketDto, double priceWithoutDiscount) {

}
