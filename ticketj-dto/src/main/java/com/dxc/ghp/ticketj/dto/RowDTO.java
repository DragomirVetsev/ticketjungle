package com.dxc.ghp.ticketj.dto;

import java.util.List;

/**
 * Row data transfer object.
 *
 * @param number
 *            The number of the row.
 * @param seats
 *            The seats in the row.
 */
public record RowDTO(int number, List<SeatDTO> seats) {
}
