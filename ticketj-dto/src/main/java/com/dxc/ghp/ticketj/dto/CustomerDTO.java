package com.dxc.ghp.ticketj.dto;

import java.time.LocalDate;

import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;

/**
 * Customer data transfer object
 *
 * @param username
 *            customer's username.
 * @param personName
 *            customer's name object {@link PersonName}.
 * @param address
 *            customer's {@link UnrestrictedAddress} details.
 * @param email
 *            customer's email.
 * @param phone
 *            customer's {@link PhoneDTO} object.
 * @param birthday
 *            customer's birthday.
 */
public record CustomerDTO(String username, PersonName personName, String email, PhoneDTO phone,
                          UnrestrictedAddress address, LocalDate birthday) {

}
