package com.dxc.ghp.ticketj.dto;

import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UserRole;

/**
 * @param username
 *            user`s username.
 * @param fullName
 *            is first name and last name of the user.
 * @param role
 *            user`s role.
 */
public record UserInfoDTO(String username, PersonName fullName, UserRole role) {

}
