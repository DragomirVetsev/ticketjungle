package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.CredentialsValidator;

/**
 * Test class for {@link CredentialsValidator}.
 */
public class CredentialsValidatorTest {

    /**
     * Testing {@link CredentialsValidator#validate}
     *
     * @param credentials
     *            The {@code Credentials} that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testValidData")
    void validateShouldNotAddErrors(final Credentials credentials) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new CredentialsValidator(Credentials.class.getSimpleName(), credentials,
            actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * @param credentials
     *            The {@code Credentials} that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final Credentials credentials, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new CredentialsValidator(Credentials.class.getSimpleName(), credentials,
            actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testValidData() {
        final String validUsername = "ValidUserName";
        final String validPassword = "IvanPasss123!";
        final String validUsername1 = "Bobcho123";
        final String validPassword1 = "KokoJumbo123!!!!!!!";

        return Stream
            .of(arguments(new Credentials(validUsername, validPassword),
                          new Credentials(validUsername1, validPassword1)));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidUsername = "ValidUserNadifyghiudАСДАСДАСДСhfigshdhfgiushdfiughisudhfgiuhdiufhe^%$#!@%2431265371267TR67ETYUESGFYUGSEYTame";
        final String invalidPassword = "IvanPasss121278R376556$r%^$&%АСД^&^2R56V1R2587%^v!&)(_)@&#&6^&$%&%!$@(*#v&$v!%@^#3!";
        final String invalidUsername1 = null;
        final String invalidPassword1 = null;

        return Stream
            .of(arguments(new Credentials(invalidUsername, invalidPassword),
                          List
                              .of(new ValidationError<>(EntityFieldType.USERNAME.toString(), invalidUsername,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.USERNAME.toString(), invalidUsername,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.PASSWORD.toString(), invalidPassword,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PASSWORD.toString(), invalidPassword,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(new Credentials(invalidUsername1, invalidPassword1),
                          List
                              .of(new ValidationError<>(EntityFieldType.USERNAME.toString(), invalidUsername1,
                                  ValidationErrorType.NULL.toString()),
                                  new ValidationError<>(EntityFieldType.PASSWORD.toString(), invalidPassword1,
                                      ValidationErrorType.NULL.toString()))));
    }
}
