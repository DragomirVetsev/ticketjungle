package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.IntegerPositiveValidator;

/**
 * Test class for {@link IntegerPositiveValidator}.
 */
class IntegerPositiveValidatorTest {

    /**
     * Testing {@link IntegerPositiveValidator#validate()} with positive values.
     * Expecting no errors to be collected.
     *
     * @param value
     *            The {@code integer} value that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @ValueSource(ints = {
        0, 1, 2, 100, Integer.MAX_VALUE
    })
    void validateShouldNotAddErrors(final int value) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new IntegerPositiveValidator(EntityFieldType.TEST.toString(), value, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link IntegerPositiveValidator#validate()} with negative values.
     * Expecting errors to be collected.
     *
     * @param value
     *            The {@code integer} value that will be validated.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("integerPositiveInvalidData")
    void validateShouldAddErrors(final int value, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new IntegerPositiveValidator(EntityFieldType.TEST.toString(), value, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    private static Stream<Arguments> integerPositiveInvalidData() {
        return Stream
            .of(arguments(Integer.valueOf(-1),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Integer.valueOf(-1),
                                  ValidationErrorType.NEGATIVE_INTEGER.toString()))),
                arguments(Integer.valueOf(-5),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Integer.valueOf(-5),
                                  ValidationErrorType.NEGATIVE_INTEGER.toString()))),
                arguments(Integer.valueOf(Integer.MIN_VALUE), List
                    .of(new ValidationError<>(EntityFieldType.TEST.toString(), Integer.valueOf(Integer.MIN_VALUE),
                        ValidationErrorType.NEGATIVE_INTEGER.toString()))),
                arguments(Integer.valueOf(-325),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Integer.valueOf(-325),
                                  ValidationErrorType.NEGATIVE_INTEGER.toString()))));
    }
}
