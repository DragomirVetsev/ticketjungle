package com.dxc.ghp.ticketj.services.validation.composite.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.composite.DoubleFieldValidator;

/**
 * Test class for {@link DoubleFieldValidator}.
 */
final class DoubleFieldValidatorTest {

    /**
     * Testing {@link DoubleFieldValidator#validate()} by passing valid double
     * values. Expecting no errors to be collected.
     *
     * @param value
     *            The {@code double} value that will be validated.
     * @param digitsAfterPoint
     *            The number of digits after the decimal point.
     * @param min
     *            The minimum allowed value.
     * @param max
     *            The maximum allowed value.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("validData")
    void validateShouldNotAddErrors(final double value, final int digitsAfterPoint, final double min,
                                    final double max) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new DoubleFieldValidator(EntityFieldType.TEST.toString(), value, digitsAfterPoint,
            min, max, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link DoubleFieldValidator#validate()} by passing invalid double
     * values. Expecting no errors to be collected.
     *
     * @param value
     *            The {@code double} value that will be validated.
     * @param digitsAfterPoint
     *            The number of digits after the decimal point.
     * @param min
     *            The minimum allowed value.
     * @param max
     *            The maximum allowed value.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidData")
    void validateShouldAddErrors(final double value, final int digitsAfterPoint, final double min, final double max,
                                 final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new DoubleFieldValidator(EntityFieldType.TEST.toString(), value, digitsAfterPoint,
            min, max, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    private static Stream<Arguments> validData() {
        return Stream
            .of(arguments(Double.valueOf(22.000), Integer.valueOf(1), Double.valueOf(3.0000001),
                          Double.valueOf(22.0001)));
    }

    private static Stream<Arguments> invalidData() {
        return Stream
            .of(arguments(Double.valueOf(-22.002), Integer.valueOf(1), Double.valueOf(3.0000001),
                          Double.valueOf(21.999),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(-22.002),
                                  ValidationErrorType.NEGATIVE_DOUBLE.toString()),
                                  new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(-22.002),
                                      ValidationErrorType.INVALID_NUMBER_OF_DECIMALS.toString()),
                                  new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(-22.002),
                                      ValidationErrorType.OUT_OF_RANGE.toString()))));
    }
}
