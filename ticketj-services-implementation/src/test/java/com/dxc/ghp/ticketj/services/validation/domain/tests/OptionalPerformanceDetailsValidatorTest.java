package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.OptionalPerformanceDetailsValidator;

/**
 * Test class for {@link OptionalPerformanceDetailsValidator}.
 */
final class OptionalPerformanceDetailsValidatorTest {

    /**
     * Testing {@link OptionalPerformanceDetailsValidator#validate()} by passing
     * valid data. Expecting no errors to be collected.
     *
     * @param performanceDetails
     *            The {@link PerformanceDetails} to be tested.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("validData")
    void validateShouldNotAddErrors(final PerformanceDetails performanceDetails) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new OptionalPerformanceDetailsValidator(PerformanceDetails.class.getSimpleName(),
            performanceDetails, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> validData() {
        return Stream
            .of(arguments(new PerformanceDetails("Valid venue name", "Valid stage name",
                Instant.parse("2023-07-24T19:30:00Z"))),
                arguments(new PerformanceDetails(null, "Зала 1", Instant.parse("2023-07-24T19:30:00Z"))),
                arguments(new PerformanceDetails("NDK", null, Instant.parse("2023-07-24T19:30:00Z"))),
                arguments(new PerformanceDetails("Летен театър - Варна", "Голяма сцена", null)),
                arguments(new PerformanceDetails("Народен театър-\"Иван Вазов\"", "Valid stage name",
                    Instant.parse("2023-07-24T19:30:00Z"))));
    }
}
