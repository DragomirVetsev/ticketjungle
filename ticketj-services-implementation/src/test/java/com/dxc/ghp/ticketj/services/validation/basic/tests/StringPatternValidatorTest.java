/**
 *
 */
package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.StringPatternValidator;

/**
 * Testing class for {@link StringPatternValidator}.
 */
class StringPatternValidatorTest {

    @SuppressWarnings("nls")
    private static final String onlyNumbersPattern = "\\d+";
    @SuppressWarnings("nls")
    private static final String englishLettersAndSpacesPattern = "[a-zA-Z ]+";
    @SuppressWarnings("nls")
    private static final String anyLanguageLettersAndSymbolsPattern = "[a-zA-Z~!@#$%^&*()_+0-9]+";
    @SuppressWarnings("nls")
    private static final String englishLettersNumbersAndSymbolsPattern = "^[\\p{L} 0-9\"'\\.]+$";

    /**
     * Testing {@link StringPatternValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param value
     *            The {@code String} value that will be validated.
     * @param pattern
     *            The pattern that will be used to validate the {@code value}.
     */
    @SuppressWarnings({
        "static-method",
    })
    @ParameterizedTest
    @MethodSource("stringVerificationData")
    void validateShouldNotAddErrors(final String value, final String pattern) {
        final List<ErrorInfo> errorInfo = new ArrayList<>();
        final Validator validator = new StringPatternValidator(EntityFieldType.TEST.toString(), value, pattern,
            errorInfo);

        validator.validate();
        assertTrue(errorInfo.isEmpty());
    }

    /**
     * Testing {@link StringPatternValidator#validate()} by passing invalid data.
     * Expecting errors to be collected.
     *
     * @param value
     *            The {@code String} value that will be validated.
     * @param pattern
     *            The pattern that will be used to validate the {@code value}.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings({
        "static-method",
    })
    @ParameterizedTest
    @MethodSource("stringVerificationInvalidData")
    void validateShouldAddErrors(final String value, final String pattern, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new StringPatternValidator(EntityFieldType.TEST.toString(), value, pattern,
            actualErrors);
        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings({
        "nls"
    })
    private static Stream<Arguments> stringVerificationData() {
        final String onlyNumbersValue = "12323123120909663540908239864512634";

        final String englishLettersAndSpacesValue = "This is the value";

        final String anyLanguageLettersAndSymbolsValue = "tsdjsndS12";
        final String englishLettersNumbersAndSymbolsValue = "Ndk Зала '1'";
        return Stream
            .of(arguments(onlyNumbersValue, onlyNumbersPattern),
                arguments(englishLettersAndSpacesValue, englishLettersAndSpacesPattern),
                arguments(anyLanguageLettersAndSymbolsValue, anyLanguageLettersAndSymbolsPattern),
                arguments(englishLettersNumbersAndSymbolsValue, englishLettersNumbersAndSymbolsPattern));
    }

    @SuppressWarnings({
        "nls"
    })
    private static Stream<Arguments> stringVerificationInvalidData() {
        final String invalidNumbersValue = "1232312312090sdaf9663540908239864512634";

        final String invalidEnglishLettersAndSpacesValue = "This is the vсд2@alue";
        return Stream
            .of(arguments(invalidNumbersValue, onlyNumbersPattern,
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), invalidNumbersValue,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(invalidEnglishLettersAndSpacesValue, englishLettersAndSpacesPattern, List
                    .of(new ValidationError<>(EntityFieldType.TEST.toString(), invalidEnglishLettersAndSpacesValue,
                        ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
