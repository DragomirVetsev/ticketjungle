package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.CustomerDTO;
import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.CustomerDtoValidator;

/**
 * Test class for {@link CustomerDtoValidator}.
 */
final class CustomerDtoValidatorTest {

    /**
     * Testing {@link CustomerDtoValidator#validate}
     *
     * @param customerDto
     *            The {@code CustomerDTO} that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testValidData")
    void validateShouldNotAddErrors(final CustomerDTO customerDto) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new CustomerDtoValidator(CustomerDTO.class.getSimpleName(), customerDto,
            actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * @param customerDto
     *            The {@code CustomerDTO} that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final CustomerDTO customerDto, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new CustomerDtoValidator(CustomerDTO.class.getSimpleName(), customerDto,
            actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testValidData() {
        final String validUsername = "ValidUserName";
        final String givenName = "Ivan";
        final String familyName = "Ivanov";
        final PersonName validPersonName = new PersonName(givenName, familyName);
        final String validEmail = "Ivan@gmail.com";
        final PhoneDTO validPhone = new PhoneDTO("+359", "878576423");
        final UnrestrictedAddress validAddress = new UnrestrictedAddress("Car osvoboditel", "Varna", "9000");
        final LocalDate validBurthday = LocalDate.now();

        return Stream
            .of(arguments(new CustomerDTO(validUsername, validPersonName, validEmail, validPhone, validAddress,
                validBurthday)));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidSymbolsVenueName = "The best venue?|>|?}Plus it has more than allowed characterssssssssss"
            + "sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
        final String familyName = "Ivanov";
        final PersonName validPersonName = new PersonName(invalidSymbolsVenueName, familyName);
        final String invalidEmail = "Ivan@gm!@!@#ail";
        final String invalidPhoneCode = "-087";
        final String invaldPhoneNumber = "078576423";
        final PhoneDTO validPhone = new PhoneDTO(invalidPhoneCode, invaldPhoneNumber);
        final UnrestrictedAddress validAddress = new UnrestrictedAddress("Car osvoboditel", "Varna", "9000");
        final LocalDate validBurthday = LocalDate.now();

        return Stream
            .of(arguments(new CustomerDTO(invalidSymbolsVenueName, validPersonName, invalidEmail, validPhone,
                validAddress, validBurthday),
                          List
                              .of(new ValidationError<>(EntityFieldType.USERNAME.toString(), invalidSymbolsVenueName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.USERNAME.toString(), invalidSymbolsVenueName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.PERSON_GIVEN_NAME.toString(),
                                      invalidSymbolsVenueName, ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PERSON_GIVEN_NAME.toString(),
                                      invalidSymbolsVenueName, ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.EMAIL.toString(), invalidEmail,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.PHONE_CODE.toString(), invalidPhoneCode,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.PHONE_NUMBER.toString(), invaldPhoneNumber,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
