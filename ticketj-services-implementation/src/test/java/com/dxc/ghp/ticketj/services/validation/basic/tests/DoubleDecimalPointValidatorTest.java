package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.DoubleDecimalPointValidator;

/**
 * Test class for {@link DoubleDecimalPointValidator}
 */
class DoubleDecimalPointValidatorTest {

    /**
     * Testing {@link DoubleDecimalPointValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param value
     *            The value that will be validated.
     * @param allowedNumberDecimals
     *            The maximum allowed decimal digits.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("validData")
    void validateShouldNotAddErrors(final double value, final int allowedNumberDecimals) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new DoubleDecimalPointValidator(EntityFieldType.TEST.toString(), value,
            allowedNumberDecimals, actualErrors);

        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link DoubleDecimalPointValidator#validate()} by passing invalid
     * data. Expecting errors to be collected.
     *
     * @param fieldName
     *            The name of the {@code double} value that will be validated.
     * @param value
     *            The value that will be validated.
     * @param allowedNumberDecimals
     *            The maximum allowed decimal digits.
     * @param expectedErrors
     *            The list with errors expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidData")
    void validateShouldAddErrors(final String fieldName, final double value, final int allowedNumberDecimals,
                                 final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new DoubleDecimalPointValidator(fieldName, value, allowedNumberDecimals,
            actualErrors);

        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    private static Stream<Arguments> validData() {
        return Stream
            .of(arguments(Double.valueOf(2.001), Integer.valueOf(3)),
                arguments(Double.valueOf(12.123451398), Integer.valueOf(10)),
                arguments(Double.valueOf(2.1), Integer.valueOf(1)));
    }

    private static Stream<Arguments> invalidData() {
        return Stream
            .of(arguments(EntityFieldType.TEST.toString(), Double.valueOf(1.0001), Integer.valueOf(3),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(1.0001),
                                  ValidationErrorType.INVALID_NUMBER_OF_DECIMALS.toString()))),
                arguments(EntityFieldType.TEST.toString(), Double.valueOf(121.12345678), Integer.valueOf(7),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(121.12345678),
                                  ValidationErrorType.INVALID_NUMBER_OF_DECIMALS.toString()))),
                arguments(EntityFieldType.TEST.toString(), Double.valueOf(421.5942), Integer.valueOf(3),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(421.5942),
                                  ValidationErrorType.INVALID_NUMBER_OF_DECIMALS.toString()))),
                arguments(EntityFieldType.TEST.toString(), Double.valueOf(421.592), Integer.valueOf(2),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(421.592),
                                  ValidationErrorType.INVALID_NUMBER_OF_DECIMALS.toString()))));
    }
}
