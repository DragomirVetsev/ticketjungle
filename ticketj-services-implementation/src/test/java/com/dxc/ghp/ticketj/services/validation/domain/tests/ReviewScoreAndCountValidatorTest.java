package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.ReviewScoreAndCountValidator;

/**
 * Test class for {@link ReviewScoreAndCountValidator}
 */
final class ReviewScoreAndCountValidatorTest {

    /**
     * Testing {@link ReviewScoreAndCountValidator#validate()} by passing valid
     * {@ReviewScoreAndCount} data. Expecting no errors to be collected.
     *
     * @param reviewScoreAndCount
     *            The {@code ReviewScoreAndCount} to be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("validData")
    void validateShouldNotAddErrors(final ReviewScoreAndCount reviewScoreAndCount) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new ReviewScoreAndCountValidator(ReviewScoreAndCount.class.getSimpleName(),
            reviewScoreAndCount, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    private static Stream<Arguments> validData() {
        return Stream
            .of(arguments(new ReviewScoreAndCount(5.00, 3)), arguments(new ReviewScoreAndCount(1.1, 23)),
                arguments(new ReviewScoreAndCount(4.91, 512)), arguments(new ReviewScoreAndCount(3.99, 3)),
                arguments(new ReviewScoreAndCount(4.2, 3)), arguments(new ReviewScoreAndCount(1.0, 3)));
    }

    /**
     * Testing {@link ReviewScoreAndCountValidator#validate()} by passing invalid
     * {@ReviewScoreAndCount} data. Expecting errors to be collected.
     *
     * @param reviewScoreAndCount
     *            The {@code ReviewScoreAndCount} to be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidData")
    void validateShouldAddErrors(final ReviewScoreAndCount reviewScoreAndCount, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new ReviewScoreAndCountValidator(ReviewScoreAndCount.class.getSimpleName(),
            reviewScoreAndCount, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    private static Stream<Arguments> invalidData() {
        return Stream
            .of(arguments(new ReviewScoreAndCount(-5.1, 3),
                          List
                              .of(new ValidationError<>(EntityFieldType.REVIEW_SCORE.toString(), Double.valueOf(-5.1),
                                  ValidationErrorType.NEGATIVE_DOUBLE.toString()),
                                  new ValidationError<>(EntityFieldType.REVIEW_SCORE.toString(), Double.valueOf(-5.1),
                                      ValidationErrorType.OUT_OF_RANGE.toString()))),
                arguments(new ReviewScoreAndCount(-5.00, -2),
                          List
                              .of(new ValidationError<>(EntityFieldType.REVIEW_SCORE.toString(), Double.valueOf(-5.00),
                                  ValidationErrorType.NEGATIVE_DOUBLE.toString()),
                                  new ValidationError<>(EntityFieldType.REVIEW_SCORE.toString(), Double.valueOf(-5.00),
                                      ValidationErrorType.OUT_OF_RANGE.toString()),
                                  new ValidationError<>(EntityFieldType.REVIEW_COUNT.toString(), Integer.valueOf(-2),
                                      ValidationErrorType.NEGATIVE_INTEGER.toString()))));
    }
}
