package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.EventNameValidator;

/**
 * Test class for {@link EventNameValidator}.
 */
final class EventNameValidatorTest {

    /**
     * Testing {@link EventNameValidator#validate} by passing valid data. Expecting
     * no errors to be collected.
     *
     * @param eventName
     *            The event'name to be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @ValueSource(strings = {
        "July morning", "Жътва на открито", "Сигнал D G", "Домът на Foster за въображаеми приятели"
    })
    void validateShouldNotAddErrors(final String eventName) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new EventNameValidator(EntityFieldType.EVENT.toString(), eventName, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link EventNameValidator#validate} by passing invalid data.
     * Expecting to collect errors.
     *
     * @param eventName
     *            The event'name to be validated.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final String eventName, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new EventNameValidator(EntityFieldType.EVENT.toString(), eventName, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidSymbolsName = "Test|{:>s";
        final String tooLongName = "Toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
            + "ooooooooooooooooooLongName";
        final String tooLongAndInvalidSymbolsName = "Toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
            + "oooooooooooooooInvalidName>{?@#$%^&*";
        return Stream
            .of(arguments(invalidSymbolsName,
                          List
                              .of(new ValidationError<>(EntityFieldType.EVENT.toString(), invalidSymbolsName,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(tooLongName,
                          List
                              .of(new ValidationError<>(EntityFieldType.EVENT.toString(), tooLongName,
                                  ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(tooLongAndInvalidSymbolsName,
                          List
                              .of(new ValidationError<>(EntityFieldType.EVENT.toString(), tooLongAndInvalidSymbolsName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.EVENT.toString(), tooLongAndInvalidSymbolsName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(null,
                          List
                              .of(new ValidationError<>(EntityFieldType.EVENT.toString(), null,
                                  ValidationErrorType.NULL.toString()))));
    }
}
