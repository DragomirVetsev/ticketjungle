package com.dxc.ghp.ticketj.services.validation.tests;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.misc.exceptions.ValidationException;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.ValidationEnforcer;
import com.dxc.ghp.ticketj.services.validation.ValidatorConstructor;
import com.dxc.ghp.ticketj.services.validation.domain.EventNameValidator;

/**
 * Test class for {@link ValidationEnforcer}.
 */
class ValidationEnforcerTest {

    /**
     * Testing
     * {@link ValidationEnforcer#verifyMandatoryField(String, Object, ValidatorConstructor)}
     * with data for {@link EventNameValidator}. Expecting no exception to be
     * thrown.
     */
    @SuppressWarnings({
        "static-method", "nls"
    })
    @Test
    // @formatter:off
    void validateMandatoryFieldShouldNotThrow() {
        final String validEvent = "No brainer party!";
        assertDoesNotThrow(()->ValidationEnforcer.verifyMandatoryField(EntityFieldType.EVENT.toString(),
                                                                       validEvent, EventNameValidator::new));
    }

    // @formatter:on

    /**
     * Testing
     * {@link ValidationEnforcer#verifyMandatoryField(String, Object, ValidatorConstructor)}
     * with data for {@link EventNameValidator}. Expecting
     * {@code ValidationException} to be thrown.
     *
     * @see ValidationException
     */
    @SuppressWarnings({
        "static-method", "nls"
    })
    @Test
    // @formatter:off
    void validateMandatoryFieldShouldThrow() {
        final String invalidEvent = "No brainer party!#$%^&*||?+=()";
        assertThrows(ValidationException.class,
                     ()->ValidationEnforcer.verifyMandatoryField(EntityFieldType.EVENT.toString(),
                                                                       invalidEvent, EventNameValidator::new));
    }
    // @formatter:on

    /**
     * Testing
     * {@link ValidationEnforcer#verifyMandatoryField(String, Object, ValidatorConstructor)}
     * with null value for {@link EventNameValidator}. Expecting
     * {@code ValidationException} to be thrown.
     *
     * @see ValidationException
     */
    @SuppressWarnings("static-method")
    @Test
    // @formatter:off
    void validateMandatoryFieldWithNullShouldThrow() {
        assertThrows(ValidationException.class,
                     ()->ValidationEnforcer.verifyMandatoryField(EntityFieldType.EVENT.toString(),
                                                                       null, EventNameValidator::new));
    }
    // @formatter:on
}
