package com.dxc.ghp.ticketj.services.impl.utilities.tests;

import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.OffsetTime;
import java.time.temporal.Temporal;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.DiscountDTO;
import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.dto.SeatDTO;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.services.impl.utilities.ServicesCommon;

/**
 * Test for all {@link ServicesCommon} methods.
 */
final class ServicesCommonTest {

    /**
     * Tests {@link ServicesCommon#notNull(Object) notNull} with non-null data.
     * Should return true.
     *
     * @param <Attribute>
     *            The type of the parameter passed for testing.
     * @param attribute
     *            The parameter passed for testing.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("notNullTestData")
    <Attribute> void notNullShouldReturnTrue(final Attribute attribute) {
        Assertions.assertTrue(ServicesCommon.notNull(attribute));
    }

    /**
     * Tests {@link ServicesCommon#notNull(Object) notNull} with null data. Should
     * return false.
     */
    @SuppressWarnings("static-method")
    @Test
    void notNullShouldReturnFalse() {
        Assertions.assertFalse(ServicesCommon.notNull(null));
    }

    /**
     * Tests {@link ServicesCommon#requiresModifying(UnrestrictedAddress)
     * requiresModifying} with non-null address and at least one non-null address
     * attribute.
     *
     * @param testData
     *            The address passed for testing.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("notNullAddressTestData")
    void requiresModifyingAddressShouldReturnTrue(final UnrestrictedAddress testData) {
        Assertions.assertTrue(ServicesCommon.requiresModifying(testData));
    }

    /**
     * Tests {@link ServicesCommon#requiresModifying(UnrestrictedAddress)
     * requiresModifying} with either address is null or all of its attributes are
     * null.
     *
     * @param testData
     *            The address passed for testing.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("nullAddressTestData")
    void requiresModifyingAddressShouldReturnFalse(final UnrestrictedAddress testData) {
        Assertions.assertFalse(ServicesCommon.requiresModifying(testData));
    }

    /**
     * Tests {@link ServicesCommon#requiresModifying(PhoneDTO) requiresModifying}
     * with non-null phone and at least one non-null phone attribute.
     *
     * @param testData
     *            The phone passed for testing.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("notNullPhoneTestData")
    void requiresModifyingPhoneShouldReturnTrue(final PhoneDTO testData) {
        Assertions.assertTrue(ServicesCommon.requiresModifying(testData));
    }

    /**
     * Tests {@link ServicesCommon#requiresModifying(PhoneDTO) requiresModifying}
     * with either phone is null or all of its attributes are null.
     *
     * @param testData
     *            The phone passed for testing.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("nullPhoneTestData")
    void requiresModifyingPhoneShouldReturnFalse(final PhoneDTO testData) {
        Assertions.assertFalse(ServicesCommon.requiresModifying(testData));
    }

    /**
     * Tests {@link ServicesCommon#requiresModifying(Period) requiresModifying} with
     * non-null period and at least one non-null period attribute.
     *
     * @param testData
     *            The period passed for testing.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("notNullPeriodTestData")
    void requiresModifyingPeriodShouldReturnTrue(final Period<Temporal> testData) {
        Assertions.assertTrue(ServicesCommon.requiresModifying(testData));
    }

    /**
     * Tests {@link ServicesCommon#requiresModifying(Period) requiresModifying} with
     * either period is null or all of its attributes are null.
     *
     * @param testData
     *            The period passed for testing.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("nullPeriodTestData")
    void requiresModifyingPeriodShouldReturnFalse(final Period<Temporal> testData) {
        Assertions.assertFalse(ServicesCommon.requiresModifying(testData));
    }

    private static Stream<Arguments> notNullTestData() {
        return Stream
            .of(arguments(new UnrestrictedAddress(null, null, null)), arguments(new PhoneDTO(null, null)),
                arguments(new DiscountDTO(null, 0)), arguments(new SeatDTO(0, null)));
    }

    @SuppressWarnings("nls")
    private static Stream<UnrestrictedAddress> notNullAddressTestData() {
        return Stream
            .of(new UnrestrictedAddress("street", null, null), new UnrestrictedAddress(null, "city", null),
                new UnrestrictedAddress(null, null, "postCode"), new UnrestrictedAddress(null, "city", "postCode"),
                new UnrestrictedAddress("street", "city", "postCode"));
    }

    private static Stream<UnrestrictedAddress> nullAddressTestData() {
        return Stream.of(new UnrestrictedAddress(null, null, null), null);
    }

    @SuppressWarnings("nls")
    private static Stream<PhoneDTO> notNullPhoneTestData() {
        return Stream
            .of(new PhoneDTO("+234", null), new PhoneDTO(null, "567543245"), new PhoneDTO("+234", "567543245"));
    }

    private static Stream<PhoneDTO> nullPhoneTestData() {
        return Stream.of(new PhoneDTO(null, null), null);
    }

    @SuppressWarnings("nls")
    private static Stream<Period<Temporal>> notNullPeriodTestData() {
        return Stream
            .of(new Period<>(null, OffsetTime.parse("12:00Z")), new Period<>(OffsetTime.parse("09:00Z"), null),
                new Period<>(OffsetTime.parse("09:00Z"), OffsetTime.parse("12:00Z")));
    }

    private static Stream<Period<Temporal>> nullPeriodTestData() {
        return Stream.of(new Period<>(null, null), null);
    }
}
