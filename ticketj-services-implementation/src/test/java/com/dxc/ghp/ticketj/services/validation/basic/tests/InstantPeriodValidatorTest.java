package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.BiValidationError;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.InstantPeriodValidator;

/**
 * Test class for {@link InstantPeriodValidator}.
 */
class InstantPeriodValidatorTest {

    /**
     * Testing {@link InstantPeriodValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param period
     *            The {@link Period} value that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("periodValidatorData")
    void validateShouldNotAddErrors(final Period<Instant> period) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new InstantPeriodValidator(period, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link InstantPeriodValidator#validate()} by passing invalid data.
     * Expecting no errors to be collected.
     *
     * @param period
     *            The {@Period} value that will be validated.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("periodValidatorInvalidData")
    void validateShouldAddErrors(final Period<Instant> period, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new InstantPeriodValidator(period, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> periodValidatorData() {
        final Instant pastDate = Instant.parse("2022-07-22T19:30:00Z");
        final Instant futureDate = Instant.parse("2023-07-24T19:30:00Z");
        return Stream
            .of(arguments(new Period<>(pastDate, futureDate)), arguments(new Period<>(pastDate, pastDate)),
                arguments(new Period<>(futureDate, futureDate)));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> periodValidatorInvalidData() {
        final Instant pastDate = Instant.parse("2022-07-22T19:30:00Z");
        final Instant futureDate = Instant.parse("2023-07-24T19:30:00Z");
        final Instant now = Instant.now();
        return Stream
            .of(arguments(new Period<>(futureDate, pastDate),
                          List
                              .of(new BiValidationError<>(EntityFieldType.START_TIME.toString(), futureDate,
                                  EntityFieldType.END_TIME.toString(), pastDate,
                                  ValidationErrorType.INCORRECT_DATE_TIME_ORDER.toString()))),
                arguments(new Period<>(now, pastDate),
                          List
                              .of(new BiValidationError<>(EntityFieldType.START_TIME.toString(), now,
                                  EntityFieldType.END_TIME.toString(), pastDate,
                                  ValidationErrorType.INCORRECT_DATE_TIME_ORDER.toString()))));
    }
}
