package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.PerformanceDetailsValidator;

/**
 * Test class for {@link PerformanceDetailsValidator}.
 */
final class PerformanceDetailsValidatorTest {

    /**
     * Testing {@link PerformanceDetailsValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param performanceDetails
     *            The {@code PerformanceDetails} to be tested.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("validData")
    void validateShouldNotAddErrors(final PerformanceDetails performanceDetails) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new PerformanceDetailsValidator(PerformanceDetails.class.getSimpleName(),
            performanceDetails, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link PerformanceDetailsValidator#validate()} by passing invalid
     * data. Expecting to collect errors.
     *
     * @param fieldName
     *            The name of the {@code performanceDetails}.
     * @param performanceDetails
     *            The details of the sub-event to be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidData")
    void testValidateShouldProduceErrors(final String fieldName, final PerformanceDetails performanceDetails,
                                         final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new PerformanceDetailsValidator(fieldName, performanceDetails, actualErrors);

        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> validData() {
        return Stream
            .of(arguments(new PerformanceDetails("Valid venue name", "Valid stage name",
                Instant.parse("2023-07-24T19:30:00Z"))),
                arguments(new PerformanceDetails("НДК", "Зала 1", Instant.parse("2023-07-24T19:30:00Z"))),
                arguments(new PerformanceDetails("NDK", "Zala 1", Instant.parse("2023-07-24T19:30:00Z"))),
                arguments(new PerformanceDetails("Летен театър - Варна", "Голяма сцена",
                    Instant.parse("2023-07-24T19:30:00Z"))),
                arguments(new PerformanceDetails("Народен театър-\"Иван Вазов\"", "Valid stage name",
                    Instant.parse("2023-07-24T19:30:00Z"))));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> invalidData() {
        final String performanceOne = "PerformanceOne";
        final String performanceTwo = "PerformanceTwo";
        final String performanceThree = "PerformanceThree";

        final String tooLongVenueName = "venue2еееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееее";
        final String invalidSymbolsVenueName = "venueжбнк:>}{@и";
        final String tooLongAndInvalidVenueName = "venue3йнсйднй./.................................."
            + "................................";
        final String invalidSymbolsStageName = "invalidStage2ухедунун(*§";

        return Stream
            .of(arguments(performanceOne,
                          new PerformanceDetails(invalidSymbolsVenueName, "stage",
                              Instant.parse("2023-07-22T19:30:00Z")),
                          List
                              .of(new ValidationError<>(EntityFieldType.VENUE.toString(), invalidSymbolsVenueName,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(performanceTwo,
                          new PerformanceDetails(tooLongVenueName, invalidSymbolsStageName,
                              Instant.parse("2023-12-22T19:30:00Z")),
                          List
                              .of(new ValidationError<>(EntityFieldType.VENUE.toString(), tooLongVenueName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.STAGE.toString(), invalidSymbolsStageName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(performanceThree,
                          new PerformanceDetails(tooLongAndInvalidVenueName, invalidSymbolsStageName,
                              Instant.parse("2022-01-01T19:30:00Z")),
                          List
                              .of(new ValidationError<>(EntityFieldType.VENUE.toString(), tooLongAndInvalidVenueName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.VENUE.toString(), tooLongAndInvalidVenueName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.STAGE.toString(), invalidSymbolsStageName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
