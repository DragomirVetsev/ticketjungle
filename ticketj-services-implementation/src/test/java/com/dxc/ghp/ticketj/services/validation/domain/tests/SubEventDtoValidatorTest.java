package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.SubEventDtoValidator;

/**
 * Test class for {@link SubEventDtoValidator}.
 */
final class SubEventDtoValidatorTest {

    /**
     * Testing {@link SubEventDtoValidator#validate} bypassi
     *
     * @param subEventDto
     *            The {@code SubEventDTO} that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testValidData")
    void validateShouldNotAddErrors(final SubEventDTO subEventDto) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new SubEventDtoValidator(SubEventDTO.class.getSimpleName(), subEventDto,
            actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * @param subEventDto
     *            The {@code SubEventDTO} that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final SubEventDTO subEventDto, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new SubEventDtoValidator(SubEventDTO.class.getSimpleName(), subEventDto,
            actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testValidData() {

        final String name = "Sub-event name";
        final String eventName = "Valid Event name";
        final String stage = "Stage";
        final String venue = "Venue";
        final Instant time = Instant.parse("2023-07-22T19:30:00Z");
        final String description = "Valid description";
        final String street = "Valid street";
        final String city = "Valid city";
        final String postCode = "1111";
        final double price = 10.00;
        final double score = 1.00;
        final int count = 1;
        final List<String> performers = List.of("Performer1", "Performer2");

        return Stream
            .of(arguments(new SubEventDTO(new PerformanceId(name, eventName),
                new PerformanceDetails(venue, stage, time), description, price,
                new UnrestrictedAddress(street, city, postCode), performers, new ReviewScoreAndCount(score, count))));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidName = "min";
        final String invalidEventName = "";
        final String invalidDescription = "invalid";
        final String invalidStage = "Stage2ухедунун(*§";
        final String invalidVenue = null;
        final Instant time = Instant.parse("2023-07-22T19:30:00Z");
        final double invalidPrice = -10.00;
        final String invalidStreet = "v";
        final String invalidCity = "c";
        final String invalidPostCode = "11";
        final List<String> performers = null;
        final double score = -1.0;
        final int count = -1;

        return Stream
            .of(arguments(new SubEventDTO(new PerformanceId(invalidName, invalidEventName),
                new PerformanceDetails(invalidVenue, invalidStage, time), invalidDescription, invalidPrice,
                new UnrestrictedAddress(invalidStreet, invalidCity, invalidPostCode), performers,
                new ReviewScoreAndCount(score, count)),
                          List
                              .of(new ValidationError<>(EntityFieldType.SUB_EVENT.toString(), invalidName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.EVENT.toString(), invalidEventName,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.EVENT.toString(), invalidEventName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.VENUE.toString(), invalidVenue,
                                      ValidationErrorType.NULL.toString()),
                                  new ValidationError<>(EntityFieldType.STAGE.toString(), invalidStage,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.DESCRIPTION.toString(), invalidDescription,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.TICKET_PRICE.toString(),
                                      Double.valueOf(invalidPrice), ValidationErrorType.NEGATIVE_DOUBLE.toString()),
                                  new ValidationError<>(EntityFieldType.STREET.toString(), invalidStreet,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.CITY.toString(), invalidCity,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.POST_CODE.toString(), invalidPostCode,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PERFORMERS_NAMES.toString(), performers,
                                      ValidationErrorType.NULL.toString()),
                                  new ValidationError<>(EntityFieldType.REVIEW_SCORE.toString(), Double.valueOf(score),
                                      ValidationErrorType.NEGATIVE_DOUBLE.toString()),
                                  new ValidationError<>(EntityFieldType.REVIEW_SCORE.toString(), Double.valueOf(score),
                                      ValidationErrorType.OUT_OF_RANGE.toString()),
                                  new ValidationError<>(EntityFieldType.REVIEW_COUNT.toString(), Integer.valueOf(count),
                                      ValidationErrorType.NEGATIVE_INTEGER.toString()))));
    }
}
