package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.StringCompareValidator;
import com.dxc.ghp.ticketj.services.validation.basic.StringPatternValidator;

/**
 * Test class for {@link StringCompareValidator}.
 */
public class StringCompareValidatorTest {
    /**
     * Testing {@link StringPatternValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param subjectA
     *            The String field that will be compared.
     * @param subjectB
     *            The String field that will be compared to the {@code subjectA}.
     */
    @SuppressWarnings({
        "static-method",
    })
    @ParameterizedTest
    @MethodSource("stringVerificationData")
    void validateShouldNotAddErrors(final String subjectA, final String subjectB) {
        final List<ErrorInfo> errorInfo = new ArrayList<>();
        final Validator validator = new StringCompareValidator(EntityFieldType.TEST.toString(), subjectA, subjectB,
            errorInfo);

        validator.validate();
        assertTrue(errorInfo.isEmpty());
    }

    /**
     * Testing {@link StringPatternValidator#validate()} by passing invalid data.
     * Expecting errors to be collected.
     *
     * @param subjectA
     *            The String field that will be compared.
     * @param subjectB
     *            The String field that will be compared to the {@code subjectA}.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings({
        "static-method",
    })
    @ParameterizedTest
    @MethodSource("stringVerificationInvalidData")
    void validateShouldAddErrors(final String subjectA, final String subjectB, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new StringCompareValidator(EntityFieldType.TEST.toString(), subjectA, subjectB,
            actualErrors);
        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings({
        "nls"
    })
    private static Stream<Arguments> stringVerificationData() {
        final String subjectA = "StringToCompere";
        final String subjectB = "StringToCompereIsDiferent";
        final String subjectA1 = "StringToCompereWithNumber123";
        final String subjectB2 = "StringToCompereIsDiferentNumber123";
        return Stream.of(arguments(subjectA, subjectB), arguments(subjectA1, subjectB2));
    }

    @SuppressWarnings({
        "nls"
    })
    private static Stream<Arguments> stringVerificationInvalidData() {
        final String subjectA = "StringToCompere";
        final String subjectB = "StringToCompere";
        final String subjectA1 = "StringToCompereWithNumber123";
        final String subjectB2 = "StringToCompereWithNumber123";

        return Stream
            .of(arguments(subjectA, subjectB,
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), subjectA,
                                  ValidationErrorType.IDENTICAL.toString()))),
                arguments(subjectA1, subjectB2,
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), subjectA1,
                                  ValidationErrorType.IDENTICAL.toString()))));
    }
}
