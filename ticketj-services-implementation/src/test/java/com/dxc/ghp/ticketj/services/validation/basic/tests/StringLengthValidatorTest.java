/**
 *
 */
package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.StringLengthValidator;

/**
 * Test class for {@link StringLengthValidator}.
 */
final class StringLengthValidatorTest {

    /**
     * Testing {@link StringLengthValidator#validate()} by passing valid data.
     * Expecting no errors.
     *
     * @param value
     *            The {@code String} value that will be validated.
     * @param min
     *            The minimum number of characters the {@code value} must have.
     * @param max
     *            The maximum number of characters the {@code value} must have.
     */
    @SuppressWarnings({
        "static-method"
    })
    @ParameterizedTest
    @MethodSource("stringFieldVerificationData")
    void validateShouldNotAddErrors(final String value, final int min, final int max) {
        final List<ErrorInfo> errors = new ArrayList<>();
        final Validator validator = new StringLengthValidator(EntityFieldType.TEST.toString(), value, min, max, errors);

        validator.validate();

        assertTrue(errors.isEmpty());
    }

    /**
     * Testing {@link StringLengthValidator#validate()} by passing invalid data.
     * Expecting errors to be collected.
     *
     * @param value
     *            The {@code String} value that will be validated.
     * @param min
     *            The minimum number of characters the {@code value} must have.
     * @param max
     *            The maximum number of characters the {@code value} must have.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings({
        "static-method"
    })
    @ParameterizedTest
    @MethodSource("stringFieldVerificationInvalidData")
    void validateShouldAddErrors(final String value, final int min, final int max,
                                 final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new StringLengthValidator(EntityFieldType.TEST.toString(), value, min, max,
            actualErrors);

        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings({
        "nls"
    })
    private static Stream<Arguments> stringFieldVerificationData() {
        final String stringValue = "String field of whit spacesand spce!al S#mbols!";

        return Stream
            .of(arguments(stringValue, Integer.valueOf(1), Integer.valueOf(100)),
                arguments(" ", Integer.valueOf(1), Integer.valueOf(1)),
                arguments(stringValue, Integer.valueOf(1), Integer.valueOf(Integer.MAX_VALUE)));
    }

    @SuppressWarnings({
        "nls"
    })
    private static Stream<Arguments> stringFieldVerificationInvalidData() {
        final String zeroCharactersValue = "";
        final String fiveCharactersValue = "value";

        return Stream
            .of(arguments(zeroCharactersValue, Integer.valueOf(1), Integer.valueOf(100),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), zeroCharactersValue,
                                  ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(fiveCharactersValue, Integer.valueOf(1), Integer.valueOf(1),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), fiveCharactersValue,
                                  ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(zeroCharactersValue, Integer.valueOf(1), Integer.valueOf(Integer.MAX_VALUE),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), zeroCharactersValue,
                                  ValidationErrorType.INVALID_LENGTH.toString()))));
    }

}
