package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.PersonNameValidator;

/**
 * Test class for {@link PersonName}.
 */
final class PersonNameValidatorTest {
    /**
     * Testing {@link PersonNameValidator#validate} bypassi
     *
     * @param personName
     *            The {@code PersonName} that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testValidData")
    void validateShouldNotAddErrors(final PersonName personName) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new PersonNameValidator(PersonName.class.getSimpleName(), personName, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * @param personName
     *            The {@code PersonName} that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final PersonName personName, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new PersonNameValidator(PersonName.class.getSimpleName(), personName, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testValidData() {

        final String validGivenName = "Ivan";
        final String validFamilyName = "Zvazdev";
        final String validGivenName1 = "Ceca";
        final String validFamilyName1 = "Bojikova";
        final String validGivenName2 = "Amsarqn";
        final String validFamilyName2 = "Bulgarov";
        return Stream
            .of(arguments(new PersonName(validGivenName, validFamilyName)),
                arguments(new PersonName(validGivenName1, validFamilyName1)),
                arguments(new PersonName(validGivenName2, validFamilyName2)));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidSymbolsVenueName = "The best NAME?|>|?}Plus it has more than allowed characterssssssssss"
            + "sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
        final String invalidLengthFamilyName = "This will be one of the best events ever!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";

        return Stream
            .of(arguments(new PersonName(invalidSymbolsVenueName, invalidLengthFamilyName),
                          List
                              .of(new ValidationError<>(EntityFieldType.PERSON_GIVEN_NAME.toString(),
                                  invalidSymbolsVenueName, ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PERSON_GIVEN_NAME.toString(),
                                      invalidSymbolsVenueName, ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.PERSON_FAMALY_NAME.toString(),
                                      invalidLengthFamilyName, ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PERSON_FAMALY_NAME.toString(),
                                      invalidLengthFamilyName, ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }

}
