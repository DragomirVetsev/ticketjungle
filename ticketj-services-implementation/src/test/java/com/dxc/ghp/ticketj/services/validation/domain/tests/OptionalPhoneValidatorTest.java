package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.OptionalPhoneValidator;

/**
 * Test class for {@link OptionalPhoneValidator}.
 */
final class OptionalPhoneValidatorTest {

    /**
     * Testing {@link OptionalPhoneValidator#validate validate} by passing valid
     * data.
     *
     * @param phone
     *            The {@link PhoneDTO} that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testValidData")
    void validateShouldNotAddErrors(final PhoneDTO phone) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new OptionalPhoneValidator(PhoneDTO.class.getSimpleName(), phone, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * @param phone
     *            The {@link PhoneDTO} that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final PhoneDTO phone, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new OptionalPhoneValidator(PhoneDTO.class.getSimpleName(), phone, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testValidData() {
        final String validPhoneNumber = "57883947";
        final String validPhoneCode = "+12";

        return Stream
            .of(arguments(new PhoneDTO(null, validPhoneNumber)), arguments(new PhoneDTO(validPhoneCode, null)),
                arguments(new PhoneDTO(null, null)));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidPhoneNumber = "57883941735№@$%75762357862374657862534756782934576234756";

        return Stream
            .of(arguments(new PhoneDTO(null, invalidPhoneNumber),
                          List
                              .of(new ValidationError<>(EntityFieldType.PHONE_NUMBER.toString(), invalidPhoneNumber,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PHONE_NUMBER.toString(), invalidPhoneNumber,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }

}
