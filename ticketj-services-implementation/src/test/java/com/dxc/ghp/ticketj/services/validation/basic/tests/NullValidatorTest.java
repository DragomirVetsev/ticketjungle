package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.services.validation.basic.NullValidator;

/**
 * Test class {@link NullValidator}.
 */
class NullValidatorTest {

    /**
     * Testing {@link NullValidator#validate()} and expecting nothing.
     */
    @SuppressWarnings("static-method")
    @Test
    void validateShouldNotThrow() {
        final NullValidator validator = new NullValidator();
        assertDoesNotThrow(() -> validator.validate());
    }

}
