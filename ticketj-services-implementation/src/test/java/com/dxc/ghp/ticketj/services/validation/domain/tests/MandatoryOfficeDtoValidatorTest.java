package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.OfficeDTO;
import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.BiValidationError;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.MandatoryOfficeDtoValidator;

/**
 * Test class for {@link MandatoryOfficeDtoValidator}.
 */
final class MandatoryOfficeDtoValidatorTest {

    /**
     * Testing {@link MandatoryOfficeDtoValidator#validate} with valid data.
     *
     * @param officeDto
     *            The {@link OfficeDTO} that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testValidData")
    void validateShouldNotAddErrors(final OfficeDTO officeDto) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new MandatoryOfficeDtoValidator(OfficeDTO.class.getSimpleName(), officeDto,
            actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link MandatoryOfficeDtoValidator#validate} with invalid data.
     *
     * @param officeDto
     *            The {@link OfficeDTO} that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final OfficeDTO officeDto, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new MandatoryOfficeDtoValidator(OfficeDTO.class.getSimpleName(), officeDto,
            actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testValidData() {
        return Stream
            .of(arguments(new OfficeDTO("TicketJ 000",
                new UnrestrictedAddress("Mladost, bl. 234, str. 12", "Sofia", "1000"),
                new PhoneDTO("+359", "886756432"), "office_000@ticketj.com",
                new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of())),
                arguments(new OfficeDTO("Ticket Jungle HQ", new UnrestrictedAddress("Vrajdebna, ul 7", "Sofia", "1839"),
                    new PhoneDTO("+359", "888333495"), "office_hq@ticketj.com",
                    new Period<>(LocalTime.parse("10:00"), LocalTime.parse("20:00")), List.of())));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidNameLength = "Tick";
        final String invalidStreetLength = "Mankato Mississippi 96522 Roseville NH 11523, Past Bridge";
        final String invalidCity = "S";
        final String invalidPostCode = "!@#il";
        final String invalidPhoneCode = "-087";
        final String invaldPhoneNumber = "078576423";
        final String invalidEmail = "office_000ticketj.com";
        final Period<LocalTime> invalidWorkingHours = new Period<>(LocalTime.parse("18:00"), LocalTime.parse("15:00"));
        return Stream
            .of(arguments(new OfficeDTO(invalidNameLength,
                new UnrestrictedAddress(invalidStreetLength, invalidCity, invalidPostCode),
                new PhoneDTO(invalidPhoneCode, invaldPhoneNumber), invalidEmail, invalidWorkingHours, List.of()),
                          List
                              .of(new ValidationError<>(EntityFieldType.NAME.toString(), invalidNameLength,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new BiValidationError<>(EntityFieldType.START_TIME.toString(),
                                      invalidWorkingHours.startTime(), EntityFieldType.END_TIME.toString(),
                                      invalidWorkingHours.endTime(),
                                      ValidationErrorType.INCORRECT_DATE_TIME_ORDER.toString()),
                                  new ValidationError<>(EntityFieldType.PHONE_CODE.toString(), invalidPhoneCode,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.PHONE_NUMBER.toString(), invaldPhoneNumber,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.EMAIL.toString(), invalidEmail,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.STREET.toString(), invalidStreetLength,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.CITY.toString(), invalidCity,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.POST_CODE.toString(), invalidPostCode,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
