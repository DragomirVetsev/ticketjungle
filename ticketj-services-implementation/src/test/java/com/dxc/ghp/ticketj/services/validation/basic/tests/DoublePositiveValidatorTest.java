package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.DoublePositiveValidator;

/**
 * Test class for {@link DoublePositiveValidator}.
 */
class DoublePositiveValidatorTest {

    /**
     * Testing {@link DoublePositiveValidator#validate()} with positive double
     * values. Expecting no errors to be collected.
     *
     * @param value
     *            The {@code double} value that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @ValueSource(doubles = {
        0, 1.2, 4.5, 20.6, 0.00001
    })
    void validateShouldNotAddErrors(final double value) {
        final List<ErrorInfo> errors = new ArrayList<>();
        final Validator validator = new DoublePositiveValidator(EntityFieldType.TEST.toString(), value, errors);

        validator.validate();
        assertTrue(errors.isEmpty());
    }

    /**
     * Testing {@link DoublePositiveValidator#validate()} with negative double
     * values. Expecting errors to be collected.
     *
     * @param value
     *            The {@code double} value that will be validated.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("doublePositiveValidationInvalidaData")
    void validateShouldAddErrors(final double value, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new DoublePositiveValidator(EntityFieldType.TEST.toString(), value, actualErrors);
        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    private static Stream<Arguments> doublePositiveValidationInvalidaData() {

        return Stream
            .of(arguments(Double.valueOf(-1.2),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(-1.2),
                                  ValidationErrorType.NEGATIVE_DOUBLE.toString()))),
                arguments(Double.valueOf(-20.2),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(-20.2),
                                  ValidationErrorType.NEGATIVE_DOUBLE.toString()))),
                arguments(Double.valueOf(-0.000000001),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(-0.000000001),
                                  ValidationErrorType.NEGATIVE_DOUBLE.toString()))),
                arguments(Double.valueOf(-100.2),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(-100.2),
                                  ValidationErrorType.NEGATIVE_DOUBLE.toString()))),
                arguments(Double.valueOf(-15.4),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(-15.4),
                                  ValidationErrorType.NEGATIVE_DOUBLE.toString()))));
    }
}
