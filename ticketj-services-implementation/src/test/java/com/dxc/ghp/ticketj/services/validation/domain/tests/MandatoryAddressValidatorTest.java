package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.MandatoryAddressValidator;

/**
 * Test class for {@link MandatoryAddressValidator}.
 */
final class MandatoryAddressValidatorTest {

    /**
     * Testing {@link MandatoryAddressValidator#validate() validate} by passing
     * valid data. Expecting no errors to be collected.
     *
     * @param address
     *            The {@link UnrestrictedAddress} that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("validData")
    void validateShouldNotAddErrors(final UnrestrictedAddress address) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new MandatoryAddressValidator(EntityFieldType.ADDRESS.toString(), address,
            actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link MandatoryAddressValidator#validate() validate} by passing
     * invalid data. Expecting errors to be collected.
     *
     * @param address
     *            The {@link UnrestrictedAddress} that will be validated.
     * @param expectedErrors
     *            The list with errors expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidData")
    void validateShouldAddErrors(final UnrestrictedAddress address, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new MandatoryAddressValidator(EntityFieldType.ADDRESS.toString(), address,
            actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> validData() {
        return Stream
            .of(arguments(new UnrestrictedAddress("Street", "City", "124-234A")),
                arguments(new UnrestrictedAddress("Улица Ивановски № 40", "Varna", "1B24-4A")),
                arguments(new UnrestrictedAddress("Street № 23B", "Burgas", "124-234A")),
                arguments(new UnrestrictedAddress("Peter Stalinski 23", "Montana", "D3400-24A")),
                arguments(new UnrestrictedAddress("Проф. Улекси", "Ruse", "9344-24A")));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> invalidData() {
        return Stream
            .of(arguments(new UnrestrictedAddress("Street@#$%^&*()", "City", "124-234A"),
                          List
                              .of(new ValidationError<>(EntityFieldType.STREET.toString(), "Street@#$%^&*()",
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(new UnrestrictedAddress("Улица Ивановски № 40", "VarnaЕЦЕЦЕсдс", "1B24-4A"),
                          List
                              .of(new ValidationError<>(EntityFieldType.CITY.toString(), "VarnaЕЦЕЦЕсдс",
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(new UnrestrictedAddress("Улица Ивановски № 40", null, "921ASM344-234A"),
                          List
                              .of(new ValidationError<>(EntityFieldType.CITY.toString(), null,
                                  ValidationErrorType.NULL.toString()),
                                  new ValidationError<>(EntityFieldType.POST_CODE.toString(), "921ASM344-234A",
                                      ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(new UnrestrictedAddress(
                    "Проф.111111111111111111111111111111111111111111111111111111111 Улекси", "Ruse", "921ASM344-234A"),
                          List
                              .of(new ValidationError<>(EntityFieldType.STREET.toString(),
                                  "Проф.111111111111111111111111111111111111111111111111111111111 Улекси",
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.POST_CODE.toString(), "921ASM344-234A",
                                      ValidationErrorType.INVALID_LENGTH.toString()))));
    }
}
