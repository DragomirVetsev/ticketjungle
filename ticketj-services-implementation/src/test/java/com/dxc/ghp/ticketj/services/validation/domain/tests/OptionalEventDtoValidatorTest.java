package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.EventDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.OptionalEventDtoValidator;

/**
 * Test class for {@link OptionalEventDtoValidator}.
 */
final class OptionalEventDtoValidatorTest {

    /**
     * Testing {@link OptionalEventDtoValidator#validate} by passing valid data.
     *
     * @param eventDto
     *            The {@link EventDTO} that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testValidData")
    void validateShouldNotAddErrors(final EventDTO eventDto) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new OptionalEventDtoValidator(EventDTO.class.getSimpleName(), eventDto,
            actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * @param eventDto
     *            The {@code EventDTO} that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final EventDTO eventDto, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new OptionalEventDtoValidator(EventDTO.class.getSimpleName(), eventDto,
            actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testValidData() {

        final String validName = "The best venue";
        final String validDescription = "This will be one of the best events ever!";
        final String validGenre = "Pop";
        final String validNameTwo = "Дядовата ръкавичка";
        final String validDescriptionTwo = "Ще можеш ли да се побереш и ти вътре?? Ела и разбери!";
        final String validGenreTwo = "Theatre";
        final String validNameThree = "Дядовата ръкавичка: Part 2";
        final String validDescriptionThree = "Even more space in it!?";
        final String validGenreThree = "Theatre";
        return Stream
            .of(arguments(new EventDTO(validName, validDescription, validGenre)),
                arguments(new EventDTO(validName, null, validGenreTwo)), arguments(new EventDTO(validName, null, null)),
                arguments(new EventDTO(validName, validDescriptionTwo, null)),
                arguments(new EventDTO(validNameTwo, validDescriptionTwo, validGenreTwo)),
                arguments(new EventDTO(validNameThree, validDescriptionThree, validGenreThree)));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidSymbolsEventName = "The best venue?|>|?}Plus it has more than allowed characterssssssssss"
            + "sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
        final String invalidLengthDescription = "This will be one of the best events ever!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        final String invalidSymbolsGenre = "PopХБНЙКЛ";
        return Stream
            .of(arguments(new EventDTO(invalidSymbolsEventName, invalidLengthDescription, invalidSymbolsGenre),
                          List
                              .of(new ValidationError<>(EntityFieldType.EVENT.toString(), invalidSymbolsEventName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.EVENT.toString(), invalidSymbolsEventName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.DESCRIPTION.toString(),
                                      invalidLengthDescription, ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.GENRE.toString(), invalidSymbolsGenre,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
