package com.dxc.ghp.ticketj.services.validation.composite.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.DoublePositiveValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Test class for {@link MandatoryFieldValidator}
 */
final class MandatoryFieldValidatorTest {

    /**
     * Testing {@link MandatoryFieldValidator#validate()} by passing not null
     * values. Expecting no errors to be collected.
     *
     * @param valueName
     *            The name of the value.
     * @param value
     *            The value to be validated.
     * @param validator
     *            The {@link Validator} that will be executed if {@code value} is
     *            not null.
     * @param actualErrors
     *            The list with errors that will be collected during the validation.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */

    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("mandatoryValidatorData")
    void validateShouldNotAddErrors(final String valueName, final Object value, final Supplier<Validator> validator,
                                    final List<ErrorInfo> actualErrors, final List<ErrorInfo> expectedErrors) {
        final Validator mandatoryFieldValidator = new MandatoryFieldValidator(valueName, value, validator,
            actualErrors);
        mandatoryFieldValidator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    /**
     * Testing {@link MandatoryFieldValidator#validate()} by passing null values.
     * Expecting errors to be collected.
     *
     * @param valueName
     *            The name of the value.
     * @param value
     *            The value to be validated.
     * @param validator
     *            The {@link Validator} that will be executed if {@code value} is
     *            not null.
     * @param actualErrors
     *            The list with errors that will be collected during the validation.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("mandatoryValidatorInvalidData")
    void validateShouldAddErrors(final String valueName, final Object value, final Supplier<Validator> validator,
                                 final List<ErrorInfo> actualErrors, final List<ErrorInfo> expectedErrors) {
        final Validator mandatoryFieldValidator = new MandatoryFieldValidator(valueName, value, validator,
            actualErrors);
        mandatoryFieldValidator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> mandatoryValidatorData() {

        final String stringName = "StringValue";
        final String stringValue = "testableValueWithTooManyCharactersToBeAllowed";
        final int stringMin = 1;
        final int stringMax = 20;
        final String pattern = "[a-zA-Z ]+";
        final List<ErrorInfo> stringValidatorErrors = new ArrayList<>();
        final Supplier<Validator> stringValidator = () -> new StringFieldValidator(stringName, stringValue, stringMin,
            stringMax, pattern, stringValidatorErrors);

        final String doubleName = "DoubleNumber";
        final double doubleValue = 9.0;
        final List<ErrorInfo> doublePositiveValidatorErrors = new ArrayList<>();
        final Supplier<Validator> doublePositiveValidator = () -> new DoublePositiveValidator(doubleName, doubleValue,
            doublePositiveValidatorErrors);
        return Stream
            .of(arguments(stringName, stringValue, stringValidator, stringValidatorErrors, List
                .of(new ValidationError<>(stringName, stringValue, ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(doubleName, Double.valueOf(doubleValue), doublePositiveValidator,
                          doublePositiveValidatorErrors, List.of()));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> mandatoryValidatorInvalidData() {
        final String stringName = "ValueString";
        final String stringValue = null;
        final int stringMin = 2;
        final int stringMax = 15;
        final String pattern = "[a-zA-Z ]+";
        final List<ErrorInfo> stringValidatorErrors = new ArrayList<>();
        final Supplier<Validator> stringValidator = () -> new StringFieldValidator(stringName, stringValue, stringMin,
            stringMax, pattern, stringValidatorErrors);

        final String doubleName = "DoubleNumber";
        final double doubleValue = 9.0;
        final List<ErrorInfo> doublePositiveValidatorErrors = new ArrayList<>();
        final Supplier<Validator> doublePositiveValidator = () -> new DoublePositiveValidator(doubleName, doubleValue,
            doublePositiveValidatorErrors);
        return Stream
            .of(arguments(stringName, null, stringValidator, stringValidatorErrors,
                          List.of(new ValidationError<>(stringName, null, ValidationErrorType.NULL.toString()))),
                arguments(doubleName, null, doublePositiveValidator, doublePositiveValidatorErrors,
                          List.of(new ValidationError<>(doubleName, null, ValidationErrorType.NULL.toString()))));
    }
}
