package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.OfficeNameValidator;

/**
 * Test class for {@link OfficeNameValidator}.
 */
final class OfficeNameValidatorTest {

    /**
     * Testing {@link OfficeNameValidator#validate} by passing valid data. Expecting
     * no errors to be collected.
     *
     * @param officeName
     *            The office name to be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @ValueSource(strings = {
        "TicketJ 1", "Tickt", "'Ticket Jungle' - Headquarters", "TicketJ - 150"
    })
    void validateShouldNotAddErrors(final String officeName) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new OfficeNameValidator(EntityFieldType.NAME.toString(), officeName, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link OfficeNameValidator#validate} by passing invalid data.
     * Expecting to collect errors.
     *
     * @param officeName
     *            The office name to be validated.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final String officeName, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new OfficeNameValidator(EntityFieldType.NAME.toString(), officeName, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidSymbolsName = "Test|{:>s";
        final String tooLongName = "Toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
            + "ooooooooooooooooooLongName";
        final String tooLongAndInvalidSymbolsName = "Toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
            + "oooooooooooooooInvalidName>{?@#$%^&*";
        return Stream
            .of(arguments(invalidSymbolsName,
                          List
                              .of(new ValidationError<>(EntityFieldType.NAME.toString(), invalidSymbolsName,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(tooLongName,
                          List
                              .of(new ValidationError<>(EntityFieldType.NAME.toString(), tooLongName,
                                  ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(tooLongAndInvalidSymbolsName,
                          List
                              .of(new ValidationError<>(EntityFieldType.NAME.toString(), tooLongAndInvalidSymbolsName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.NAME.toString(), tooLongAndInvalidSymbolsName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }

}
