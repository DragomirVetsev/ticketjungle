package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.PerformanceIdValidator;

/**
 * Test class for {@link PerformanceIdValidatorTest}.
 */
final class PerformanceIdValidatorTest {

    /**
     * Testing {@link PerformanceIdValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param performanceId
     *            The {@code PerformanceId} to be tested.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("validData")
    void validateShouldNotAddErrors(final PerformanceId performanceId) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new PerformanceIdValidator(PerformanceId.class.getSimpleName(), performanceId,
            actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link PerformanceIdValidator#validate()} by passing invalid data.
     * Expecting to collect errors.
     *
     * @param fieldName
     *            The name of the {@code performanceId}.
     * @param performanceId
     *            The ID of the sub-event to be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidData")
    void validateShouldAddErrors(final String fieldName, final PerformanceId performanceId,
                                 final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new PerformanceIdValidator(fieldName, performanceId, actualErrors);

        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> validData() {
        return Stream
            .of(arguments(new PerformanceId("Valid name", "Valid event name")),
                arguments(new PerformanceId("Grafa concert", "GRAFA")),
                arguments(new PerformanceId("Златно ключе", "Коледен концерт")),
                arguments(new PerformanceId("Дивна", "Коледен концерт")),
                arguments(new PerformanceId("Миро и приятели", "Summer festival in Varna")));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> invalidData() {
        final String performanceOne = "PerformanceOne";
        final String performanceTwo = "PerformanceTwo";

        final String shortName = "v";
        final String invalidName = "venueжбнк:>}{@и";
        final String invalidEventName = "nlidStage2ухедунун(*§";

        return Stream
            .of(arguments(performanceOne, new PerformanceId(invalidName, "Valid event name"),
                          List
                              .of(new ValidationError<>(EntityFieldType.SUB_EVENT.toString(), invalidName,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(performanceTwo, new PerformanceId(shortName, invalidEventName),
                          List
                              .of(new ValidationError<>(EntityFieldType.EVENT.toString(), invalidEventName,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.SUB_EVENT.toString(), shortName,
                                      ValidationErrorType.INVALID_LENGTH.toString()))));
    }

}
