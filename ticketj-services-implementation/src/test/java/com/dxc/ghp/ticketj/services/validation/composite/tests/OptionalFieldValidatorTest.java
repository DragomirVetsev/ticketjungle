/**
 *
 */
package com.dxc.ghp.ticketj.services.validation.composite.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.composite.OptionalFieldValidator;
import com.dxc.ghp.ticketj.services.validation.domain.EventNameValidator;
import com.dxc.ghp.ticketj.services.validation.domain.UsernameValidator;

/**
 * Test class for {@link OptionalFieldValidator}.
 */
final class OptionalFieldValidatorTest {

    /**
     * Testing {@link OptionalFieldValidator#validate()} by passing not null value.
     * Expecting not to collect any errors.
     *
     * @param <ValueType>
     *            The type of the value that will be validated.
     * @param value
     *            The {@code String} value that will be validated.
     * @param validator
     *            The {@link Validator} that will be executed if the {@code value}
     *            is not null.
     * @param actualErrors
     *            The list with errors that will be collected during the validation.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings({
        "static-method"
    })
    @ParameterizedTest
    @MethodSource("optionalVerificationData")
    <ValueType> void validateShouldNotAddErrors(final ValueType value, final Supplier<Validator> validator,
                                                final List<ErrorInfo> actualErrors,
                                                final List<ErrorInfo> expectedErrors) {

        final Validator optionalValidator = new OptionalFieldValidator<>(value, validator);

        optionalValidator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    /**
     * Testing {@link OptionalFieldValidator} with null values. Expecting to collect
     * no errors.
     *
     * @param <ValueType>
     *            The type of the value that will be validated.
     * @param value
     *            The {@code String} value that will be validated.
     * @param validator
     *            The {@link Validator} that will be executed if the {@code value}
     *            is not null.
     * @param actualErrors
     *            The list with errors that will be collected during the validation.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("optionalVerificationInvalidData")
    <ValueType> void validateShouldAddErrors(final ValueType value, final Supplier<Validator> validator,
                                             final List<ErrorInfo> actualErrors, final List<ErrorInfo> expectedErrors) {
        final Validator optionalValidator = new OptionalFieldValidator<>(value, validator);

        optionalValidator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings({
        "nls"
    })
    private static Stream<Arguments> optionalVerificationData() {
        final String usernameFieldName = "Username";
        final String usernameFieldValue = "UsernameTest234@d'>dddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
        final List<ErrorInfo> usernameValidatorErrors = new ArrayList<>();
        final Supplier<Validator> usernameValidator = () -> new UsernameValidator(usernameFieldName, usernameFieldValue,
            usernameValidatorErrors);

        final String eventFieldName = "EventName";
        final String eventFieldValue = "Event name of tomorrow";
        final List<ErrorInfo> eventValidatorErrors = List.of();
        final Supplier<Validator> eventNameValidator = () -> new EventNameValidator(eventFieldName, eventFieldValue,
            eventValidatorErrors);

        return Stream
            .of(arguments(usernameFieldValue, usernameValidator, usernameValidatorErrors,
                          List
                              .of(new ValidationError<>(usernameFieldName, usernameFieldValue,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(usernameFieldName, usernameFieldValue,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(eventFieldValue, eventNameValidator, eventValidatorErrors, List.of()));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> optionalVerificationInvalidData() {
        final String usernameFieldName = "Username";
        final String usernameFieldValue = "UsernameTest234@d'>dddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
        final List<ErrorInfo> usernameValidatorErrors = new ArrayList<>();
        final Supplier<Validator> usernameValidator = () -> new UsernameValidator(usernameFieldName, usernameFieldValue,
            usernameValidatorErrors);

        final String eventFieldName = "EventName";
        final String nullValue = null;
        final List<ErrorInfo> eventValidatorErrors = new ArrayList<>();
        final Supplier<Validator> eventNameValidator = () -> new EventNameValidator(eventFieldName, nullValue,
            eventValidatorErrors);

        return Stream
            .of(arguments(usernameFieldValue, usernameValidator, usernameValidatorErrors,
                          List
                              .of(new ValidationError<>(usernameFieldName, usernameFieldValue,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(usernameFieldName, usernameFieldValue,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(nullValue, eventNameValidator, eventValidatorErrors, new ArrayList<>()));
    }
}
