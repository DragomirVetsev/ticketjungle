package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.NotNullListValuesValidator;
import com.dxc.ghp.ticketj.services.validation.domain.PerformerNameValidator;

/**
 * Test class for {@link NotNullListValuesValidator}
 */
class NotNullListValuesValidatorTest {

    /**
     * Testing {@link NotNullListValuesValidator#validate()} using
     * {@code PerformerNameValidator} and passing valid data for it. Expecting no
     * errors to be collected.
     *
     * @param performersNames
     *            The list with valid performers names that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("validData")
    void validateShouldNotAddErrors(final List<String> performersNames) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new NotNullListValuesValidator<>(EntityFieldType.PERFORMER.toString(),
            performersNames, PerformerNameValidator::new, actualErrors);

        validator.validate();
        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link NotNullListValuesValidator#validate()} using
     * {@code PerformerNameValidator} and passing invalid data for it. Expecting
     * errors to be collected.
     *
     * @param performersNames
     *            The list with valid performers names that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidData")
    void validateShouldAddErrors(final List<String> performersNames, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new NotNullListValuesValidator<>(EntityFieldType.PERFORMER.toString(),
            performersNames, PerformerNameValidator::new, actualErrors);

        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> validData() {
        return Stream
            .of(arguments(List.of("IvanTheDemonБГ12", "ХристоХристосков@", "ЛошМС5 \"678\" №%€§*('")),
                arguments(List.of("HristoХристосков", "GenadiiVlahow", "Градски23 \"234\" №%€§*('")),
                arguments(List.of("Mrsсс", "ХристоХристосков@", "Камарата 211 \"678\" §*('")));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> invalidData() {
        final String invalidSymbolsName = "TheWrongOne|";
        final String invalidLengthName = "Toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
            + "ooooooooooooooooooooooooooooooooLongName";
        final String invalidLengthAndSymbols = "Toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo\"\r\n"
            + "            + \"ooooooooooooooooooooooooooooooooLongName|";

        return Stream
            .of(arguments(List.of(invalidSymbolsName, invalidLengthName, invalidLengthAndSymbols),
                          List
                              .of(new ValidationError<>(EntityFieldType.PERFORMER.toString(), invalidSymbolsName,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.PERFORMER.toString(), invalidLengthName,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PERFORMER.toString(), invalidLengthAndSymbols,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PERFORMER.toString(), invalidLengthAndSymbols,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }

    /**
     * Testing {@link NotNullListValuesValidator#validate()} using
     * {@code PerformerNameValidator} and passing null data for it. Expecting error
     * to be collected.
     *
     * @param performersNames
     *            The list with valid performers names that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidNullData")
    void testValidateWithNullValueShouldProduceErrors(final List<String> performersNames,
                                                      final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new NotNullListValuesValidator<>(EntityFieldType.PERFORMERS_NAMES.toString(),
            performersNames, PerformerNameValidator::new, actualErrors);

        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    private static Stream<Arguments> invalidNullData() {
        final List<String> nullValues = new ArrayList<>();
        nullValues.add(null);
        nullValues.add(null);
        return Stream
            .of(arguments(nullValues,
                          List
                              .of(new ValidationError<>(EntityFieldType.PERFORMERS_NAMES.toString(), nullValues,
                                  ValidationErrorType.HAS_NULL_VALUES.toString()))));
    }
}
