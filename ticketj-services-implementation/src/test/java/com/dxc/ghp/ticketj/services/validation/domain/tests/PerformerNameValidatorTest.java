package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.PerformerNameValidator;

/**
 * Test class for {@link PerformerNameValidator}
 */
final class PerformerNameValidatorTest {

    /**
     * Testing {@link PerformerNameValidator#validate()} by passing valid names.
     * Expecting no errors to be collected.
     *
     * @param name
     *            The valid name of performer.
     */
    @SuppressWarnings({
        "static-method"
    })
    @ParameterizedTest
    @ValueSource(strings = {
        "Lady Gaga", "BTR", "Bruno Mars", "Selena Gomez"
    })
    void validateShouldNotAddErrors(final String name) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new PerformerNameValidator(EntityFieldType.PERFORMERS_NAMES.toString(), name,
            actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link PerformerNameValidator#validate} by passing invalid data.
     * Expecting to collect errors.
     *
     * @param name
     *            The performer name to be validated.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final String name, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new PerformerNameValidator(EntityFieldType.PERFORMERS_NAMES.toString(), name,
            actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String wrongSymbols = "Test|{:>s";
        final String longName = "Toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
            + "ooooooooooooooooooLongPerformerName";
        final String longAndInvalidSymbolsName = "Toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo"
            + "oooooooooooooooInvalidPerformerName>{?@#$%^&*";
        return Stream
            .of(arguments(wrongSymbols,
                          List
                              .of(new ValidationError<>(EntityFieldType.PERFORMER.toString(), wrongSymbols,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(longName,
                          List
                              .of(new ValidationError<>(EntityFieldType.PERFORMER.toString(), longName,
                                  ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(longAndInvalidSymbolsName,
                          List
                              .of(new ValidationError<>(EntityFieldType.PERFORMER.toString(), longAndInvalidSymbolsName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PERFORMER.toString(), longAndInvalidSymbolsName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
