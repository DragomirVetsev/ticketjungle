package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.DoubleRangeValidator;

/**
 * Test class for {@link DoubleRangeValidator}.
 */
class DoubleRangeValidatorTest {

    /**
     * Testing {@link DoubleRangeValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param value
     *            The value to be tested.
     * @param min
     *            The minimum allowed value.
     * @param max
     *            The maximum allowed value.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("validData")
    void validateShouldNotAddErrors(final double value, final double min, final double max) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new DoubleRangeValidator(EntityFieldType.TEST.toString(), value, min, max,
            actualErrors);

        validator.validate();
        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link DoubleRangeValidator#validate()} by passing invalid data.
     * Expecting errors to be collected.
     *
     * @param value
     *            The {@code double} value to be validated.
     * @param min
     *            The minimum allowed value.
     * @param max
     *            The maximum allowed value.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidData")
    void validateShouldAddErrors(final double value, final double min, final double max,
                                 final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new DoubleRangeValidator(EntityFieldType.TEST.toString(), value, min, max,
            actualErrors);

        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    private static Stream<Arguments> validData() {
        return Stream
            .of(arguments(Double.valueOf(100.00), Double.valueOf(99.99), Double.valueOf(100.01)),
                arguments(Double.valueOf(10.00), Double.valueOf(10.00), Double.valueOf(100.00)),
                arguments(Double.valueOf(0.01), Double.valueOf(0.00), Double.valueOf(0.02)),
                arguments(Double.valueOf(24.00), Double.valueOf(3.00), Double.valueOf(34.00)),
                arguments(Double.valueOf(594.00), Double.valueOf(323.0124), Double.valueOf(599.00)),
                arguments(Double.valueOf(0.974), Double.valueOf(0.9739), Double.valueOf(1.00)),
                arguments(Double.valueOf(2.00), Double.valueOf(1.00), Double.valueOf(3.00)));
    }

    private static Stream<Arguments> invalidData() {
        return Stream
            .of(arguments(Double.valueOf(100.00), Double.valueOf(97.99), Double.valueOf(98.99),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(100.00),
                                  ValidationErrorType.OUT_OF_RANGE.toString()))),
                arguments(Double.valueOf(20.00), Double.valueOf(99.99), Double.valueOf(100.01),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(20.00),
                                  ValidationErrorType.OUT_OF_RANGE.toString()))),
                arguments(Double.valueOf(7.00), Double.valueOf(7.99), Double.valueOf(10.01),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(7.00),
                                  ValidationErrorType.OUT_OF_RANGE.toString()))),
                arguments(Double.valueOf(1000000.00), Double.valueOf(9000.99), Double.valueOf(10000.01),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(1000000.00),
                                  ValidationErrorType.OUT_OF_RANGE.toString()))),
                arguments(Double.valueOf(200.00), Double.valueOf(8.99), Double.valueOf(100.01),
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), Double.valueOf(200.00),
                                  ValidationErrorType.OUT_OF_RANGE.toString()))));
    }
}
