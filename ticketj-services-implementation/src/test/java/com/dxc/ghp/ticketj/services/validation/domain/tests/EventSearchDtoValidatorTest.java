package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.EventSearchDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.BiValidationError;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.EventSearchDtoValidator;

/**
 * Test class for {@link EventSearchDtoValidator}.
 */
final class EventSearchDtoValidatorTest {

    /**
     * Testing {@link EventSearchDtoValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param eventSearchDto
     *            The {@code EventSearchDto} to be validated.
     */
    @SuppressWarnings({
        "static-method"
    })
    @ParameterizedTest
    @MethodSource("eventSearchDtoValidatorData")
    void validateShouldNotAddErrors(final EventSearchDTO eventSearchDto) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new EventSearchDtoValidator(EventSearchDTO.class.getSimpleName(), eventSearchDto,
            actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link EventSearchDtoValidator#validate()} by passing invalid data.
     * Expecting errors to be collected.
     *
     * @param eventSearchDto
     *            The {@code EventSearchDto} to be validated.
     * @param expectedErrors
     *            The list with errors that will collect the errors during
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("eventSearchDtoValidatorInvalidData")
    void validateShouldAddErrors(final EventSearchDTO eventSearchDto, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new EventSearchDtoValidator(EventSearchDTO.class.getSimpleName(), eventSearchDto,
            actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> eventSearchDtoValidatorData() {
        return Stream
            .of(arguments(new EventSearchDTO("Varna", "Pop",
                new Period<>(Instant.parse("2023-07-22T19:30:00Z"), Instant.parse("2023-07-24T19:30:00Z")))),
                arguments(new EventSearchDTO("Stara Zagora", "Rock", null)));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> eventSearchDtoValidatorInvalidData() {
        final String invalidSymbolsCity = "Varna$^&*";
        final String tooLongGenre = "Popppppppppppppppppppppppppppppppppppppppppppppppppppppppp";
        final Instant pastDate = Instant.parse("2022-07-22T19:30:00Z");
        final Instant futureDate = Instant.parse("2023-07-24T19:30:00Z");
        return Stream
            .of(arguments(new EventSearchDTO(invalidSymbolsCity, tooLongGenre, new Period<>(futureDate, pastDate)),
                          List
                              .of(new ValidationError<>(EntityFieldType.CITY.toString(), invalidSymbolsCity,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.GENRE.toString(), tooLongGenre,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new BiValidationError<>(EntityFieldType.START_TIME.toString(), futureDate,
                                      EntityFieldType.END_TIME.toString(), pastDate,
                                      ValidationErrorType.INCORRECT_DATE_TIME_ORDER.toString()))));
    }
}
