/**
 *
 */
package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.UsernameValidator;

/**
 * Test class for {@link UsernameValidator}.
 */
final class UsernameValidatorTest {
    /**
     * Testing {@link UsernameValidator#validate()} by passing valid data. Expected
     * no errors to be collected.
     *
     * @param username
     *            The username that will be validated.
     */
    @SuppressWarnings({
        "static-method",
    })
    @ParameterizedTest
    @MethodSource("usernameValidData")
    void validateShouldNotAddErrors(final String username) {
        final List<ErrorInfo> userNameErrorInfo = new ArrayList<>();
        final Validator validator = new UsernameValidator(EntityFieldType.USERNAME.toString(), username,
            userNameErrorInfo);

        validator.validate();

        assertTrue(userNameErrorInfo.isEmpty());
    }

    /**
     * Testing {@link UsernameValidator#validate()} by passing invalid usernames.
     * Expecting errors to be collected.
     *
     * @param username
     *            The username to be collected.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings({
        "static-method"
    })
    @ParameterizedTest
    @MethodSource("usernameInvalidData")
    void validateShouldAddErrors(final String username, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new UsernameValidator(EntityFieldType.USERNAME.toString(), username, actualErrors);

        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> usernameValidData() {
        final String usernameOne = "ValidUsername3#";
        final String usernameTwo = "missingU3sernameInDB(*";
        final String usernameThree = "We12#$@$__";

        return Stream.of(arguments(usernameOne), arguments(usernameTwo), arguments(usernameThree));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> usernameInvalidData() {
        final String usernameOne = "ValidUsernam  e3#";
        final String usernameTwo = "missingFGЦФЖГХБНЙКМЛ<InDB(*askmdlafmalkmfalkfmlakwkdmqwkmdqwlkdmasmdlkdmqlkmdwqlk";
        final String usernameThree = "boboooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo";

        return Stream
            .of(arguments(usernameOne,
                          List
                              .of(new ValidationError<>(EntityFieldType.USERNAME.toString(), usernameOne,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(usernameTwo,
                          List
                              .of(new ValidationError<>(EntityFieldType.USERNAME.toString(), usernameTwo,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.USERNAME.toString(), usernameTwo,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(usernameThree,
                          List
                              .of(new ValidationError<>(EntityFieldType.USERNAME.toString(), usernameThree,
                                  ValidationErrorType.INVALID_LENGTH.toString()))));
    }
}
