package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.SubEventNameValidator;

/**
 * Test class for {@link SubEventNameValidator}.
 */
final class SubEventNameValidatorTest {

    /**
     * Testting {@link SubEventNameValidator#validate()} by passing valid names
     * data. Expecting no errors to be collected.
     *
     * @param name
     *            The name of sub-event.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @ValueSource(strings = {
        "Sub-Event Valid", "Красна Поляна 23", "Мечо Пух: The Revenge", "D2: в 'Казабланла'"
    })
    void validateShouldNotAddErrors(final String name) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new SubEventNameValidator(EntityFieldType.SUB_EVENT.toString(), name, actualErrors);

        validator.validate();
        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link SubEventNameValidator} by passing invalid values for name.
     * Expecting errors to be collected.
     *
     * @param name
     *            The invalid name of the sub-event.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("invalidData")
    void validateShouldAddErrors(final String name, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new SubEventNameValidator(EntityFieldType.SUB_EVENT.toString(), name, actualErrors);

        validator.validate();
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> invalidData() {
        final String invalidSymbols = "~!@#$%^&*()";
        final String tooLongName = "JANSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS"
            + "SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS"
            + "SSSSSSSSSSSSS";
        final String invalidAndTooLongName = "#$%^&ismdskamdafmasfmsksksskssssssssssssssssssssssssssssssssss"
            + "sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss214ssssssss";
        return Stream
            .of(arguments(invalidSymbols,
                          List
                              .of(new ValidationError<>(EntityFieldType.SUB_EVENT.toString(), invalidSymbols,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(tooLongName,
                          List
                              .of(new ValidationError<>(EntityFieldType.SUB_EVENT.toString(), tooLongName,
                                  ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(invalidAndTooLongName,
                          List
                              .of(new ValidationError<>(EntityFieldType.SUB_EVENT.toString(), invalidAndTooLongName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.SUB_EVENT.toString(), invalidAndTooLongName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
