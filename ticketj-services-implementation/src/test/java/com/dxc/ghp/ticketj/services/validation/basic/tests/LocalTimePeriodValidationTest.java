package com.dxc.ghp.ticketj.services.validation.basic.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.BiValidationError;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.InstantPeriodValidator;
import com.dxc.ghp.ticketj.services.validation.basic.LocalTimePeriodValidator;

/**
 * Test class for {@link LocalTimePeriodValidator}.
 */
class LocalTimePeriodValidationTest {

    /**
     * Testing {@link LocalTimePeriodValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param period
     *            The {@link Period} value that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("periodValidatorData")
    void validateShouldNotAddErrors(final Period<LocalTime> period) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new LocalTimePeriodValidator(period, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link InstantPeriodValidator#validate()} by passing invalid data.
     * Expecting no errors to be collected.
     *
     * @param period
     *            The {@Period} value that will be validated.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("periodValidatorInvalidData")
    void validateShouldAddErrors(final Period<LocalTime> period, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new LocalTimePeriodValidator(period, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> periodValidatorData() {
        final LocalTime pastDate = LocalTime.parse("09:30");
        final LocalTime futureDate = LocalTime.parse("19:30");
        return Stream
            .of(arguments(new Period<>(pastDate, futureDate)), arguments(new Period<>(pastDate, pastDate)),
                arguments(new Period<>(futureDate, futureDate)));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> periodValidatorInvalidData() {
        final LocalTime pastTime = LocalTime.parse("09:30");
        final LocalTime futureTime = LocalTime.parse("19:30");
        final LocalTime between = LocalTime.parse("15:30");
        return Stream
            .of(arguments(new Period<>(futureTime, pastTime),
                          List
                              .of(new BiValidationError<>(EntityFieldType.START_TIME.toString(), futureTime,
                                  EntityFieldType.END_TIME.toString(), pastTime,
                                  ValidationErrorType.INCORRECT_DATE_TIME_ORDER.toString()))),
                arguments(new Period<>(between, pastTime),
                          List
                              .of(new BiValidationError<>(EntityFieldType.START_TIME.toString(), between,
                                  EntityFieldType.END_TIME.toString(), pastTime,
                                  ValidationErrorType.INCORRECT_DATE_TIME_ORDER.toString()))));
    }

}
