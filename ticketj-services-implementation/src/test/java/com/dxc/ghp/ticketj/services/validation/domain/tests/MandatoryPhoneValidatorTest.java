package com.dxc.ghp.ticketj.services.validation.domain.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.domain.MandatoryPhoneValidator;

/**
 * Test class for {@link MandatoryPhoneValidator}.
 */
final class MandatoryPhoneValidatorTest {
    /**
     * Testing {@link MandatoryPhoneValidator#validate validate} by passing valid
     * data.
     *
     * @param phone
     *            The {@link PhoneDTO} that will be validated.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testValidData")
    void validateShouldNotAddErrors(final PhoneDTO phone) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new MandatoryPhoneValidator(PhoneDTO.class.getSimpleName(), phone, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * @param phone
     *            The {@link PhoneDTO} that will be validated.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("testInvalidData")
    void validateShouldAddErrors(final PhoneDTO phone, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new MandatoryPhoneValidator(PhoneDTO.class.getSimpleName(), phone, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testValidData() {
        final String validPhoneCode = "+359";
        final String validPhoneNumber = "57883948";
        final String validPhoneCode1 = "+12";
        final String validPhoneNumber1 = "23784684";
        final String validPhoneCode2 = "+823";
        final String validPhoneNumber2 = "76341728";

        return Stream
            .of(arguments(new PhoneDTO(validPhoneCode, validPhoneNumber)),
                arguments(new PhoneDTO(validPhoneCode1, validPhoneNumber1)),
                arguments(new PhoneDTO(validPhoneCode2, validPhoneNumber2)));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> testInvalidData() {
        final String invalidPhoneCode = "+359234234234234@№$@$№23423423423432412412412412";
        final String invalidPhoneNumber = "57883941735№@$%75762357862374657862534756782934576234756";

        return Stream
            .of(arguments(new PhoneDTO(invalidPhoneCode, invalidPhoneNumber),
                          List
                              .of(new ValidationError<>(EntityFieldType.PHONE_CODE.toString(), invalidPhoneCode,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PHONE_CODE.toString(), invalidPhoneCode,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.PHONE_NUMBER.toString(), invalidPhoneNumber,
                                      ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.PHONE_NUMBER.toString(), invalidPhoneNumber,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
