/**
 *
 */
package com.dxc.ghp.ticketj.services.validation.composite.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.composite.EmailValidator;
import com.dxc.ghp.ticketj.services.validation.domain.UsernameValidator;

/**
 * Test class for a email.
 */
final class EmailValidatorTest {
    /**
     * Testing {@link UsernameValidator#validate()} by passing valid data. Expected
     * no errors to be collected.
     *
     * @param email
     *            The email that will be validated.
     */
    @SuppressWarnings({
        "static-method",
    })
    @ParameterizedTest
    @MethodSource("emailValidData")
    void validateShouldNotAddErrors(final String email) {
        final List<ErrorInfo> userNameErrorInfo = new ArrayList<>();
        final Validator validator = new EmailValidator(EntityFieldType.EMAIL.toString(), email, userNameErrorInfo);

        validator.validate();

        assertTrue(userNameErrorInfo.isEmpty());
    }

    /**
     * Testing {@link UsernameValidator#validate()} by passing invalid usernames.
     * Expecting errors to be collected.
     *
     * @param email
     *            The email to be collected.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings({
        "static-method"
    })
    @ParameterizedTest
    @MethodSource("usernameInvalidData")
    void validateShouldAddErrors(final String email, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new EmailValidator(EntityFieldType.EMAIL.toString(), email, actualErrors);

        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> emailValidData() {
        final String emailOne = "Ivan@gmail.com";
        final String emailTwo = "Ceco@dxc.com";
        final String emailThree = "Marta@abv.bg";

        return Stream.of(arguments(emailOne), arguments(emailTwo), arguments(emailThree));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> usernameInvalidData() {
        final String usernameOne = "IVAN@#!@#>>>C";
        final String usernameTwo = "missingFGЦФЖГХБНЙКМЛ<InDB(*askmdlafmalkmfalkfmlakwkdmqwkmdqwlkdmasmdlkdmqlkmdwqlk";
        final String usernameThree = "ivanabvcom";

        return Stream
            .of(arguments(usernameOne,
                          List
                              .of(new ValidationError<>(EntityFieldType.EMAIL.toString(), usernameOne,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(usernameTwo,
                          List
                              .of(new ValidationError<>(EntityFieldType.EMAIL.toString(), usernameTwo,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.EMAIL.toString(), usernameTwo,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(usernameThree,
                          List
                              .of(new ValidationError<>(EntityFieldType.EMAIL.toString(), usernameThree,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
