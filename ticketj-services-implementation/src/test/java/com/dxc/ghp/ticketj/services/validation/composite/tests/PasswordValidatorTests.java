package com.dxc.ghp.ticketj.services.validation.composite.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.StringPatternValidator;
import com.dxc.ghp.ticketj.services.validation.composite.PasswordValidator;

/**
 * Test class for {@link PasswordValidator}
 */
final class PasswordValidatorTests {
    /**
     * Testing {@link StringPatternValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param password
     *            The password field that will be validated.
     * @param username
     *            The username that comes with the .
     */
    @SuppressWarnings({
        "static-method",
    })
    @ParameterizedTest
    @MethodSource("stringVerificationData")
    void validateShouldNotAddErrors(final String password, final String username) {
        final List<ErrorInfo> errorInfo = new ArrayList<>();
        final Validator validator = new PasswordValidator(EntityFieldType.TEST.toString(), password, username,
            errorInfo);

        validator.validate();
        assertTrue(errorInfo.isEmpty());
    }

    /**
     * Testing {@link StringPatternValidator#validate()} by passing invalid data.
     * Expecting errors to be collected.
     *
     * @param subjectA
     *            The String field that will be compared.
     * @param subjectB
     *            The String field that will be compared to the {@code subjectA}.
     * @param expectedErrors
     *            The list with errors that will be collected after the validation.
     */
    @SuppressWarnings({
        "static-method",
    })
    @ParameterizedTest
    @MethodSource("stringVerificationInvalidData")
    void validateShouldAddErrors(final String subjectA, final String subjectB, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new PasswordValidator(EntityFieldType.TEST.toString(), subjectA, subjectB,
            actualErrors);
        validator.validate();
        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings({
        "nls"
    })
    private static Stream<Arguments> stringVerificationData() {
        final String username = "Username";
        final String password = "!123LongPasswordForProfile";
        final String username1 = "UsernameWhitNumber12";
        final String password1 = "!ASDafs3651243654";
        return Stream.of(arguments(password, username), arguments(password1, username1));
    }

    @SuppressWarnings({
        "nls"
    })
    private static Stream<Arguments> stringVerificationInvalidData() {
        final String subjectA = "StringToCompere";
        final String subjectB = "StringToCompere";
        final String subjectA1 = "StringToCompereWithNumber123";
        final String subjectB2 = "StringToCompereWithNumber123";

        return Stream
            .of(arguments(subjectA, subjectB,
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), subjectA,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.TEST.toString(), subjectA,
                                      ValidationErrorType.IDENTICAL.toString()))),
                arguments(subjectA1, subjectB2,
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), subjectA1,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.TEST.toString(), subjectA1,
                                      ValidationErrorType.IDENTICAL.toString()))));
    }
}
