package com.dxc.ghp.ticketj.services.validation.composite.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Test class for {@link StringFieldValidator}
 */
final class StringFieldValidatorTest {

    @SuppressWarnings("nls")
    private static final String onlyNumbersPattern = "\\d+";
    @SuppressWarnings("nls")
    private static final String englishLettersAndSpacesPattern = "[a-zA-Z ]+";
    @SuppressWarnings("nls")
    private static final String englishLanguageLettersAndSymbolsPattern = "[a-zA-Z~!@#$%^&*()_+0-9]+";
    @SuppressWarnings("nls")
    private static final String anyLanguageLettersNumbersAndSymbolsPattern = "^[\\p{L} 0-9\"'\\.]+$";

    /**
     * Testing {@link StringFieldValidator#validate()} by passing valid data.
     * Expecting no errors to be collected.
     *
     * @param fieldName
     *            The field name of the {@code value}.
     * @param value
     *            The {@code String} value to be validated.
     * @param min
     *            The minimum allowed characters for the {@code value}.
     * @param max
     *            The maximum allowed characters for the {@code value}.
     * @param pattern
     *            The {@code regex} that will be used to see if the {@code value}
     *            consists only of allowed characters.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("stringFieldValidatorData")
    void validateShouldNotAddErrors(final String fieldName, final String value, final int min, final int max,
                                    final String pattern) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new StringFieldValidator(fieldName, value, min, max, pattern, actualErrors);
        validator.validate();

        assertTrue(actualErrors.isEmpty());
    }

    /**
     * Testing {@link StringFieldValidator#validate()} by passing data that will
     * lead to errors. Expecting errors to be collected.
     *
     * @param fieldName
     *            The field name of the {@code value}.
     * @param value
     *            The {@code String} value to be validated.
     * @param min
     *            The minimum allowed characters for the {@code value}.
     * @param max
     *            The maximum allowed characters for the {@code value}.
     * @param pattern
     *            The {@code regex} that will be used to see if the {@code value}
     *            consists only of allowed characters.
     * @param expectedErrors
     *            The list with errors that are expected to be collected after the
     *            validation.
     */
    @SuppressWarnings("static-method")
    @ParameterizedTest
    @MethodSource("stringFieldValidatorInvalidData")
    void validateShouldAddErrors(final String fieldName, final String value, final int min, final int max,
                                 final String pattern, final List<ErrorInfo> expectedErrors) {
        final List<ErrorInfo> actualErrors = new ArrayList<>();
        final Validator validator = new StringFieldValidator(fieldName, value, min, max, pattern, actualErrors);
        validator.validate();

        assertEquals(expectedErrors, actualErrors);
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> stringFieldValidatorData() {
        final String numbersValue = "124524123132";
        final String englishLettersValue = "This is the velue bra";
        final String englishLettersPlusSymbolsValue = "Th!sIsComplexV@lue";
        final String randomLanguageValue = "Тестови Данни Plus Numbers";
        return Stream
            .of(arguments(EntityFieldType.TEST.toString(), numbersValue, Integer.valueOf(1), Integer.valueOf(30),
                          onlyNumbersPattern),
                arguments(EntityFieldType.TEST.toString(), englishLettersValue, Integer.valueOf(2), Integer.valueOf(25),
                          englishLettersAndSpacesPattern),
                arguments(EntityFieldType.TEST.toString(), englishLettersPlusSymbolsValue, Integer.valueOf(10),
                          Integer.valueOf(33), englishLanguageLettersAndSymbolsPattern),
                arguments(EntityFieldType.TEST.toString(), randomLanguageValue, Integer.valueOf(5), Integer.valueOf(40),
                          anyLanguageLettersNumbersAndSymbolsPattern));
    }

    @SuppressWarnings("nls")
    private static Stream<Arguments> stringFieldValidatorInvalidData() {
        final String invalidNumbersValue = "124524123132#4532";
        final String englishLettersValue = "ThisIsTheVelueBraaaaaaaХаосaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        final String englishLettersPlusSymbolsValue = "Th!sIsComplexV@luee eeeee";
        final String randomLanguageValue = "Тестови Данни Plus Numbers§*";
        return Stream
            .of(arguments(EntityFieldType.TEST.toString(), invalidNumbersValue, Integer.valueOf(1), Integer.valueOf(30),
                          onlyNumbersPattern,
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), invalidNumbersValue,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(EntityFieldType.TEST.toString(), englishLettersValue, Integer.valueOf(2), Integer.valueOf(25),
                          englishLettersAndSpacesPattern,
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), englishLettersValue,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(EntityFieldType.TEST.toString(), englishLettersValue,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))),

                arguments(EntityFieldType.TEST.toString(), englishLettersPlusSymbolsValue, Integer.valueOf(10),
                          Integer.valueOf(33), englishLanguageLettersAndSymbolsPattern,
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), englishLettersPlusSymbolsValue,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(EntityFieldType.TEST.toString(), randomLanguageValue, Integer.valueOf(5), Integer.valueOf(40),
                          anyLanguageLettersNumbersAndSymbolsPattern,
                          List
                              .of(new ValidationError<>(EntityFieldType.TEST.toString(), randomLanguageValue,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }
}
