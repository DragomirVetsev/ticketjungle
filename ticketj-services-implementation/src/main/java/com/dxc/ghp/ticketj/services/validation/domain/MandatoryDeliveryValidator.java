package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Mandatory validator for delivery name.
 */
public final class MandatoryDeliveryValidator extends CompositeValidator<String> {

    /**
     * Constructs a validation class for delivery type.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param deliveryName
     *            The delivery type name to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public MandatoryDeliveryValidator(final String fieldName, final String deliveryName, final List<ErrorInfo> errors) {
        super(fieldName, deliveryName,
            List
                .of(new StringFieldValidator(fieldName, deliveryName, Constants.DELIVERY_MIN_LENGTH,
                    Constants.DELIVERY_MAX_LENGTH, Constants.DELIVERY_NAME_PATTERN, errors)));
    }

}
