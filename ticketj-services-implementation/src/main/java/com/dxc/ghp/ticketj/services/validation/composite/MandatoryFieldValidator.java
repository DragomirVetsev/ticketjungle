package com.dxc.ghp.ticketj.services.validation.composite;

import java.util.List;
import java.util.function.Supplier;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.Validator;

/**
 * Validation class for validating that given reference is not null.
 */
public final class MandatoryFieldValidator implements Validator {

    private final String valueName;
    private final Object value;
    private final List<ErrorInfo> errors;
    private final Supplier<Validator> fieldValidatorFactory;

    /**
     * @param fieldName
     *            The name of the reference to be checked. Must not be null.
     * @param value
     *            The reference to be checked.
     * @param fieldValidatorFactory
     *            The {@code Supplier} containing the validator tree for the given
     *            value. The provider will only provide the constructed validator if
     *            the given value is not null. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    @SuppressWarnings("nls")
    public MandatoryFieldValidator(final String fieldName, final Object value,
                                   final Supplier<Validator> fieldValidatorFactory, final List<ErrorInfo> errors) {

        assert fieldName != null : "The name of the value must not be null.";
        assert errors != null : "The list with errors must not be null.";
        this.valueName = fieldName;
        this.value = value;
        this.errors = errors;
        this.fieldValidatorFactory = fieldValidatorFactory;
    }

    @Override
    public void validate() {
        if (value == null) {
            errors.add(new ValidationError<>(valueName, value, ValidationErrorType.NULL.name()));
        } else {
            final Validator fieldValidator = fieldValidatorFactory.get();
            fieldValidator.validate();
        }
    }
}
