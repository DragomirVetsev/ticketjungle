package com.dxc.ghp.ticketj.services.impl.utilities;

import java.time.temporal.Temporal;

import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;

/**
 * Contains all common service methods.
 */
public final class ServicesCommon {

    private ServicesCommon() {
        // Utility class. No Instances.
    }

    /**
     * Check if parameter is null.
     *
     * @param <Attribute>
     *            The type of the parameter.
     * @param attribute
     *            The parameter to be checked. Can be null.
     * @return True if it is null and false if it is not.
     */
    public static <Attribute> boolean notNull(final Attribute attribute) {
        return attribute != null;
    }

    /**
     * Check if the parameter passed has a non-null field.
     *
     * @param address
     *            The parameter whose attributes are checked. Can be null.
     * @return False if {@code address == null} or all of its attributes (city,
     *         street, postCode) are null. Return True if {@code address != null}
     *         and has at least one non-null attribute.
     */
    public static boolean requiresModifying(final UnrestrictedAddress address) {
        return notNull(address)
            && (notNull(address.city()) || notNull(address.street()) || notNull(address.postCode()));
    }

    /**
     * Check if the parameter passed has a non-null field.
     *
     * @param workingHours
     *            The parameter whose attributes are checked. Can be null.
     * @return False if {@code workingHours == null} or all of its attributes
     *         (startTime, endTime) are null. Return True if
     *         {@code workingHours != null} and has at least one non-null attribute.
     */
    public static <Time extends Temporal> boolean requiresModifying(final Period<Time> workingHours) {
        return notNull(workingHours) && (notNull(workingHours.startTime()) || notNull(workingHours.endTime()));
    }

    /**
     * Check if the parameter passed has a non-null field.
     *
     * @param phone
     *            The parameter whose attributes are checked. Can be null.
     * @return False if {@code phone == null} or all of its attributes (code,
     *         number) are null. Return True if {@code phone != null} and has at
     *         least one non-null attribute.
     */
    public static boolean requiresModifying(final PhoneDTO phone) {
        return notNull(phone) && (notNull(phone.code()) || notNull(phone.number()));
    }

    /**
     * Checks if the {@link Credentials} parameter is not null and its password is
     * not null.
     *
     * @param credentials
     *            The passed parameter. Can be null.
     * @return True if the {@code credentials} is not null and its password is not
     *         null. False if any is null.
     */
    public static boolean requiresModifying(final Credentials credentials) {
        return notNull(credentials) && notNull(credentials.password());
    }

}
