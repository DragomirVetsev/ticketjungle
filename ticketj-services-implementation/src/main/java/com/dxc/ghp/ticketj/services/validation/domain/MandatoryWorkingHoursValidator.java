package com.dxc.ghp.ticketj.services.validation.domain;

import java.time.LocalTime;
import java.time.OffsetTime;
import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.basic.LocalTimePeriodValidator;
import com.dxc.ghp.ticketj.services.validation.basic.NullValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;

/**
 * Mandatory validation class used to validate {@link Period}.
 */
public final class MandatoryWorkingHoursValidator extends CompositeValidator<Period<LocalTime>> {

    /**
     * Constructs a validation class for {@link Period}.
     *
     * @param name
     *            The name of the field. Must not be null.
     * @param workingHours
     *            The {@link OffsetTime} to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public MandatoryWorkingHoursValidator(final String name, final Period<LocalTime> workingHours,
                                          final List<ErrorInfo> errors) {
        super(name, workingHours,
            List
                .of(new MandatoryFieldValidator(EntityFieldType.START_TIME.toString(), workingHours.startTime(),
                    NullValidator::new, errors),
                    new MandatoryFieldValidator(EntityFieldType.END_TIME.toString(), workingHours.endTime(),
                        NullValidator::new, errors),
                    new LocalTimePeriodValidator(workingHours, errors)));
    }
}
