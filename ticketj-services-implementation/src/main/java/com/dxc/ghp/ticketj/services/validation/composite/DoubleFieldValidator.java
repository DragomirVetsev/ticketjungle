package com.dxc.ghp.ticketj.services.validation.composite;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.basic.DoubleDecimalPointValidator;
import com.dxc.ghp.ticketj.services.validation.basic.DoublePositiveValidator;
import com.dxc.ghp.ticketj.services.validation.basic.DoubleRangeValidator;

/**
 * Validation class for {@code double} value. This class applies three
 * validations: Positive, number of decimals allowed and is it in given range.
 */
public final class DoubleFieldValidator extends CompositeValidator<Double> {

    /**
     * @param fieldName
     *            The name of the {@code double} value.
     * @param value
     *            The value to be validated.
     * @param maxDecimalValues
     *            The maximum allowed decimal digits.
     * @param min
     *            The minimum allowed value.
     * @param max
     *            The maximum allowed value.
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    public DoubleFieldValidator(final String fieldName, final double value, final int maxDecimalValues,
                                final double min, final double max, final List<ErrorInfo> errors) {
        super(fieldName, Double.valueOf(value),
            List
                .of(new DoublePositiveValidator(fieldName, value, errors),
                    new DoubleDecimalPointValidator(fieldName, value, maxDecimalValues, errors),
                    new DoubleRangeValidator(fieldName, value, min, max, errors)));
    }
}
