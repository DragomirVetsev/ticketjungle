package com.dxc.ghp.ticketj.services.validation.basic;

import com.dxc.ghp.ticketj.services.validation.Validator;

/**
 * Null object validation class.
 */
public final class NullValidator implements Validator {

    @Override
    public void validate() {
        // Left empty for purpose of not validating anything.
    }
}
