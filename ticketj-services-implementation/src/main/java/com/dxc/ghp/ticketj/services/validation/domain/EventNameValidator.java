package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Validation class used to validate event's name.
 */
public final class EventNameValidator extends CompositeValidator<String> {

    /**
     * Constructs a validation class for {@code Event}'s name.
     *
     * @param fieldName
     *            The name of the parameter. Must not be null.
     * @param eventName
     *            The name of the event. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public EventNameValidator(final String fieldName, final String eventName, final List<ErrorInfo> errors) {

        super(fieldName, eventName,
            List
                .of(new MandatoryFieldValidator(
                    fieldName, eventName, () -> new StringFieldValidator(fieldName, eventName,
                        Constants.EVENT_MIN_LENGTH, Constants.EVENT_MAX_LENGTH, Constants.EVENT_NAME_PATTERN, errors),
                    errors)));
    }

}
