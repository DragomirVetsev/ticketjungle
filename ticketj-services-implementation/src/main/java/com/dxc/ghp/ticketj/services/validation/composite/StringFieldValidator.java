package com.dxc.ghp.ticketj.services.validation.composite;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.StringLengthValidator;
import com.dxc.ghp.ticketj.services.validation.basic.StringPatternValidator;

/**
 * Validation class containing common validations for names.
 */
public final class StringFieldValidator implements Validator {

    private final List<Validator> validators;

    /**
     * Constructs common name validation class.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param value
     *            The {@code String} value. Must not be null.
     * @param min
     *            The minimum characters for the value. Constraints[
     *            {@code min >= 0}].
     * @param max
     *            The maximum characters for the value. Constraints[
     *            {@code max >= min}].
     * @param pattern
     *            The pattern for the given value. Must not be null.
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    public StringFieldValidator(final String fieldName, final String value, final int min, final int max,
                                final String pattern, final List<ErrorInfo> errors) {
        assert value != null;
        assert min > 0;

        validators = List
            .of(new StringLengthValidator(fieldName, value, min, max, errors),
                new StringPatternValidator(fieldName, value, pattern, errors));
    }

    @Override
    public void validate() {
        validators.forEach(Validator::validate);
    }
}
