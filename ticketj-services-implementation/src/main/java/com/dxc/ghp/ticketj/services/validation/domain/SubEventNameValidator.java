package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Validation class for sub-event's name.
 */
public final class SubEventNameValidator extends CompositeValidator<String> {

    /**
     * Constructs validation class for sub-event's name.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param name
     *            The sub-event's name that will be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public SubEventNameValidator(final String fieldName, final String name, final List<ErrorInfo> errors) {
        super(fieldName, name,
            List
                .of(new MandatoryFieldValidator(fieldName, name,
                    () -> new StringFieldValidator(fieldName, name, Constants.SUB_EVENT_NAME_MIN_LENGTH,
                        Constants.SUB_EVENT_NAME_MAX_LENGTH, Constants.SUB_EVENT_NAME_PATTERN, errors),
                    errors)));
    }
}
