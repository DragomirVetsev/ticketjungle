package com.dxc.ghp.ticketj.services.validation.composite;

import java.util.function.Supplier;

import com.dxc.ghp.ticketj.services.validation.Validator;

/**
 * Validation to class for validating that given reference is either null or not
 * null.
 *
 * @param <Value>
 *            The type of the value to be validated.
 */
public final class OptionalFieldValidator<Value> implements Validator {

    private final Value value;
    private final Supplier<Validator> validator;

    /**
     * @param value
     *            The reference to be checked.
     * @param nextNalidator
     *            The validation that will be executed if the given value is not
     *            null.
     */
    @SuppressWarnings("nls")
    public OptionalFieldValidator(final Value value, final Supplier<Validator> nextNalidator) {
        assert nextNalidator != null : "The validator must not be null.";
        this.value = value;
        this.validator = nextNalidator;
    }

    @Override
    public void validate() {
        if (value != null) {
            validator.get().validate();
        }
    }

}
