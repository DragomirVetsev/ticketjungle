package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.OptionalFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Optional Validator class for {@link UnrestrictedAddress}
 */
public final class OptionalAddressValidator extends CompositeValidator<UnrestrictedAddress> {

    /**
     * Constructs validation class for {@code UnrestrictedAddress}
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param address
     *            The {@code UnrestrictedAddress} value that will be validated. Must
     *            not be null.
     * @param errors
     *            The list that will collect errors during the validation. Must not
     *            be null.
     */
    public OptionalAddressValidator(final String fieldName, final UnrestrictedAddress address,
                                    final List<ErrorInfo> errors) {
        super(fieldName, address,
            List
                .of(new OptionalFieldValidator<>(address.street(),
                    () -> new StringFieldValidator(EntityFieldType.STREET.toString(), address.street(),
                        Constants.STREET_NAME_MIN_LENGTH, Constants.STREET_NAME_MAX_LENGTH, Constants.STREET_PATTERN,
                        errors)),
                    new OptionalFieldValidator<>(address.city(),
                        () -> new StringFieldValidator(EntityFieldType.CITY.toString(), address.city(),
                            Constants.CITY_MIN_LENGTH, Constants.CITY_MAX_LENGTH, Constants.CITY_PATTERN, errors)),
                    new OptionalFieldValidator<>(address.postCode(),
                        () -> new StringFieldValidator(EntityFieldType.POST_CODE.toString(), address.postCode(),
                            Constants.POSTCODE_MIN_LENGTH, Constants.POSTCODE_MAX_LENGTH, Constants.POSTCODE_PATTERN,
                            errors))));
    }
}
