package com.dxc.ghp.ticketj.services.validation.basic;

import java.time.LocalTime;
import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Period;

/**
 * {@link Period} Validator for {@link LocalTime} values.
 */
public final class LocalTimePeriodValidator extends BasePeriodValidator<LocalTime> {

    /**
     * Constructs OffsetTimePeriodValidator. For contract
     * {@link BasePeriodValidator#BasePeriodValidator(Period, List) base
     * constructor}.
     */
    public LocalTimePeriodValidator(final Period<LocalTime> period, final List<ErrorInfo> errors) {
        super(period, errors);
    }

    @Override
    protected boolean startTimeIsAfterEndTime() {
        return value.startTime().isAfter(value.endTime());
    }
}
