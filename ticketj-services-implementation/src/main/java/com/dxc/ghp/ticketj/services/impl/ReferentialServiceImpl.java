/**
 *
 */
package com.dxc.ghp.ticketj.services.impl;

import java.util.List;
import java.util.function.Function;

import com.dxc.ghp.ticketj.dto.CitiesAndGenresDTO;
import com.dxc.ghp.ticketj.dto.PerformerDTO;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.Performer;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.services.EventService;
import com.dxc.ghp.ticketj.services.ReferentialService;
import com.dxc.ghp.ticketj.services.impl.utilities.Converter;

/**
 * {@link ReferentialService} production implementation.
 */
@SuppressWarnings("static-method")
public final class ReferentialServiceImpl extends BaseService implements ReferentialService {

    /**
     * Constructs a production implementation of {@link EventService}. For contract
     * see {@link BaseService#BaseService(TransactionHandler) base constructor}.
     */
    public ReferentialServiceImpl(final TransactionHandler transactionHandler) {
        super(transactionHandler);
    }

    @Override
    public CitiesAndGenresDTO getCitiesAndGenres() {
        return trxHandler.executeInTransaction(findCitiesAndGenres());
    }

    private Function<ServiceProvider, CitiesAndGenresDTO> findCitiesAndGenres() {
        return (final ServiceProvider provider) -> {
            final ReferentialRepository repositoryRepository = provider.find(ReferentialRepository.class);
            final List<City> foundCities = repositoryRepository.findAllCities();
            final List<Genre> foundGenres = repositoryRepository.findAllGenres();
            return new CitiesAndGenresDTO(Converter.convertList(foundGenres, Genre::getType),
                Converter.convertList(foundCities, City::getCityName));
        };
    }

    @Override
    public List<PerformerDTO> findAllPerformers() {
        return trxHandler.executeInTransaction(findPerformers());
    }

    private Function<ServiceProvider, List<PerformerDTO>> findPerformers() {
        return (final ServiceProvider provider) -> {
            final ReferentialRepository repositoryRepository = provider.find(ReferentialRepository.class);
            final List<Performer> foundPerformers = repositoryRepository.findAllPerformers();
            return Converter.convertList(foundPerformers, performers -> Converter.convertPerformersToDTO(performers));
        };
    }
}
