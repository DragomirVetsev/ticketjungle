package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.basic.DoublePositiveValidator;
import com.dxc.ghp.ticketj.services.validation.basic.NotNullListValuesValidator;
import com.dxc.ghp.ticketj.services.validation.basic.StringLengthValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;

/**
 * Validation class for {@link SubEventDTO}
 */
public final class SubEventDtoValidator extends CompositeValidator<SubEventDTO> {

    /**
     * Constructs a validation class for {@code SubEventDTO}.
     *
     * @param name
     *            The name of the field. Must not be null.
     * @param subEventDto
     *            The {@link SubEventDTO} to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public SubEventDtoValidator(final String name, final SubEventDTO subEventDto, final List<ErrorInfo> errors) {
        super(name, subEventDto, List
            .of(new MandatoryFieldValidator(EntityFieldType.SUB_EVENT.toString(), subEventDto.performanceId().name(),
                () -> new SubEventNameValidator(EntityFieldType.SUB_EVENT.toString(),
                    subEventDto.performanceId().name(), errors),
                errors),
                new MandatoryFieldValidator(EntityFieldType.EVENT.toString(), subEventDto.performanceId().eventName(),
                    () -> new EventNameValidator(EntityFieldType.EVENT.toString(),
                        subEventDto.performanceId().eventName(), errors),
                    errors),
                new MandatoryFieldValidator(PerformanceDetails.class.getSimpleName(), subEventDto.performanceDetails(),
                    () -> new PerformanceDetailsValidator(PerformanceDetails.class.getSimpleName(),
                        subEventDto.performanceDetails(), errors),
                    errors),
                new MandatoryFieldValidator(EntityFieldType.DESCRIPTION.toString(), subEventDto.description(),
                    () -> new StringLengthValidator(EntityFieldType.DESCRIPTION.toString(), subEventDto.description(),
                        Constants.EVENT_DESCRIPTION_MIN_LENGTH, Constants.EVENT_DESCRIPTION_MAX_LENGTH, errors),
                    errors),
                new MandatoryFieldValidator(EntityFieldType.TICKET_PRICE.toString(),
                    Double.valueOf(subEventDto.ticketPrice()),
                    () -> new DoublePositiveValidator(EntityFieldType.TICKET_PRICE.toString(),
                        subEventDto.ticketPrice(), errors),
                    errors),
                new MandatoryFieldValidator(UnrestrictedAddress.class.getSimpleName(), subEventDto.venueAddress(),
                    () -> new MandatoryAddressValidator(UnrestrictedAddress.class.getSimpleName(),
                        subEventDto.venueAddress(), errors),
                    errors),

                new MandatoryFieldValidator(EntityFieldType.PERFORMERS_NAMES.toString(), subEventDto.performers(),
                    () -> new NotNullListValuesValidator<>(EntityFieldType.PERFORMERS_NAMES.toString(),
                        subEventDto.performers(), PerformerNameValidator::new, errors),
                    errors),
                new MandatoryFieldValidator(ReviewScoreAndCount.class.getSimpleName(),
                    subEventDto.reviewScoreAndCount(),
                    () -> new ReviewScoreAndCountValidator(ReviewScoreAndCount.class.getSimpleName(),
                        subEventDto.reviewScoreAndCount(), errors),
                    errors)));
    }
}
