package com.dxc.ghp.ticketj.services.validation.basic;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.BasicValidator;

/**
 * Validation class for checking if given first string is the same as the second
 * string.
 */
public final class StringCompareValidator extends BasicValidator<String> {

    private final String subjectA;
    private final String subjectB;

    /**
     * Constructs validation class that will validate that two String field will be
     * compared one another.
     *
     * @param fieldName
     *            The name of the {@code String} value that will be validated. Must
     *            not be null.
     * @param subjectA
     *            The String field that will be compared.
     * @param subjectB
     *            The String field that will be compared to the {@code subjectA}.
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    @SuppressWarnings("nls")
    public StringCompareValidator(final String fieldName, final String subjectA, final String subjectB,
                                  final List<ErrorInfo> errors) {
        super(fieldName, subjectA, errors);
        assert subjectA != null : "The compery must not be null.";
        assert subjectB != null : "The comperyTo must not be null.";

        this.subjectA = subjectA;
        this.subjectB = subjectB;
    }

    @Override
    public void validate() {
        if (subjectA.equals(subjectB)) {
            errors.add(new ValidationError<>(fieldName, value, ValidationErrorType.IDENTICAL.name()));
        }
    }

}
