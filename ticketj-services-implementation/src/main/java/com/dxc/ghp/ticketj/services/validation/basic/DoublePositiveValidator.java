package com.dxc.ghp.ticketj.services.validation.basic;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.BasicValidator;

/**
 * Validation class for checking if given double is above 0.
 */
public final class DoublePositiveValidator extends BasicValidator<Double> {

    /**
     * Constructs positive double validation class.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param number
     *            The double value of the field.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public DoublePositiveValidator(final String fieldName, final double number, final List<ErrorInfo> errors) {
        super(fieldName, Double.valueOf(number), errors);
    }

    @Override
    public void validate() {
        if (value.doubleValue() < 0) {
            errors.add(new ValidationError<>(fieldName, value, ValidationErrorType.NEGATIVE_DOUBLE.toString()));
        }
    }
}
