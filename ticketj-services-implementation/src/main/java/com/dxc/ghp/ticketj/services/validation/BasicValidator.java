package com.dxc.ghp.ticketj.services.validation;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;

/**
 * Base class for all basic validation classes.
 *
 * @param <Value>
 *            The type of the value.
 */
public abstract class BasicValidator<Value> implements Validator {

    /**
     * The name of the field.
     */
    protected String fieldName;
    /**
     * The value of the field.
     */
    protected Value value;
    /**
     * The list of errors.
     */
    protected List<ErrorInfo> errors;

    /**
     * Constructs the base class for basic validation classes.
     *
     * @param fieldName
     *            The name of the value. Must not be null.
     * @param value
     *            The value to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    @SuppressWarnings("nls")
    protected BasicValidator(final String fieldName, final Value value, final List<ErrorInfo> errors) {
        assert fieldName != null : "The name of the value must not be null.";
        assert value != null : "The value must not be null.";
        assert errors != null : "The list of errors must not be null.";

        this.fieldName = fieldName;
        this.value = value;
        this.errors = errors;
    }

}
