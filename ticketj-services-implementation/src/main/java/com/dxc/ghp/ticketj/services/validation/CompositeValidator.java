package com.dxc.ghp.ticketj.services.validation;

import java.util.Collections;
import java.util.List;

/**
 * Base class for all validators containing one or more {@code Validator}
 * instances in their validation chain.
 *
 * @param <Value>
 *            The type of the value that will be validate.
 */
public abstract class CompositeValidator<Value> implements Validator {

    /**
     * The name of the value.
     */
    protected String name;

    /**
     * The value that will be validated.
     */
    protected Value value;

    /**
     * The list containing all validators that will be executed.
     */
    protected List<Validator> validators;

    /**
     * @param name
     *            The name of the value. Must not be null.
     * @param value
     *            The value to be validated. Can be null.
     * @param validators
     *            The {@code List} of validators that will be applied to the given
     *            value. Must not be null.
     */
    @SuppressWarnings("nls")
    protected CompositeValidator(final String name, final Value value, final List<Validator> validators) {
        assert name != null : "Value's name must not be null.";
        assert validators != null : "The list of validators must not be null.";

        this.name = name;
        this.value = value;
        this.validators = Collections.unmodifiableList(validators);
    }

    @Override
    public void validate() {
        validators.forEach(Validator::validate);
    }
}
