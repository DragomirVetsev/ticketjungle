package com.dxc.ghp.ticketj.services.impl.utilities;

import java.util.List;
import java.util.function.Function;

import com.dxc.ghp.ticketj.dto.DiscountDTO;
import com.dxc.ghp.ticketj.dto.EventDTO;
import com.dxc.ghp.ticketj.dto.OfficeDTO;
import com.dxc.ghp.ticketj.dto.PerformerDTO;
import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.dto.TicketDTO;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.persistence.entities.Discount;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Office;
import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;
import com.dxc.ghp.ticketj.persistence.entities.Performer;
import com.dxc.ghp.ticketj.persistence.entities.Row;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.entities.Ticket;
import com.dxc.ghp.ticketj.persistence.types.Phone;
import com.dxc.ghp.ticketj.persistence.types.RestrictedAddress;

/**
 * Utility class for converting.
 */
public final class Converter {

    private Converter() {
        // Prevent instantiation of the class.
    }

    /**
     * Maps a list of elements of type From to a list of elements of type To using
     * the provided conversion function.
     *
     * @param <From>
     *            The type of elements in the source list.
     * @param <To>
     *            The type of elements in the resulting list.
     * @param sourceList
     *            The source list to be mapped. Must not be null.
     * @param convertionFunction
     *            Function to convert elements from type From to type To. Must not
     *            be null.
     * @return A new list containing the mapped elements.
     */
    @SuppressWarnings("nls")
    public static <From, To> List<To> convertList(final List<From> sourceList,
                                                  final Function<From, To> convertionFunction) {
        assert sourceList != null : "Source list cannot be empty!";
        assert convertionFunction != null : "Convertion function cannot be empty!";

        return sourceList.stream().map(convertionFunction).toList();
    }

    /**
     * Converts {@link Office} entity to {@link OfficeDTO}.
     *
     * @param office
     *            The entity used for conversion. Must not be null.
     * @return A new data transfer object. Cannot be null.
     */
    @SuppressWarnings("nls")
    public static OfficeDTO officeToDTO(final Office office) {
        assert office != null : "Office must not be null!";

        final RestrictedAddress officeAddress = office.getAddress();
        final Phone officePhone = office.getPhone();

        return new OfficeDTO(office.getOfficeName(),
            new UnrestrictedAddress(officeAddress.street(), officeAddress.city().getCityName(),
                officeAddress.postCode()),
            new PhoneDTO(officePhone.code().getValue(), officePhone.number()), office.getEmail(),
            office.getWorkingPeriod(), convertList(office.getWorkers(), OfficeWorker::getUsername));
    }

    /**
     * Converts {@link Event} entity to {@link EventDTO}.
     *
     * @param event
     *            is the entity used for conversion. Must not be null.
     * @return A new data transfer object. Cannot be null.
     */
    @SuppressWarnings("nls")
    public static EventDTO convertEventToDTO(final Event event) {
        assert event != null : "Event must not be null!";
        return new EventDTO(event.getName(), event.getDescription(), event.getGenre().getType());
    }

    /**
     * Converts a list of {@link Performer} to a list of {@link String} containing
     * the names of the performers.
     *
     * @param subEvent
     *            is the sub-event which contains the list of performers. Must not
     *            be null.
     * @return A list of String objects. Cannot be null.
     */
    @SuppressWarnings("nls")
    public static List<String> convertPerformersToString(final SubEvent subEvent) {
        assert subEvent != null : "SubEvent must not be null!";
        return Converter.convertList(subEvent.getPerformers(), Performer::getName);
    }

    /**
     * Converts a {@link Performer} to a {@link PerformerDTO}.
     *
     * @param performer
     *            is the performer we convert. Must not be null.
     * @return A {@link PerformerDTO} object. Cannot be null.
     */
    @SuppressWarnings("nls")
    public static PerformerDTO convertPerformersToDTO(final Performer performer) {
        assert performer != null : "Performer must not be null!";
        return new PerformerDTO(performer.getName(), performer.getDescription());
    }

    /**
     * Converts {@link SubEvent} entity to {@link SubEventDTO}.
     *
     * @param subEvent
     *            is the entity used for conversion. Must not be null.
     * @return A new data transfer object. Cannot be null.
     */
    @SuppressWarnings("nls")
    public static SubEventDTO convertSubEventToDTO(final SubEvent subEvent) {
        assert subEvent != null : "SubEvent must not be null!";

        return new SubEventDTO(new PerformanceId(subEvent.getName(), subEvent.getEvent().getName()),
            new PerformanceDetails(
                subEvent.getStage().getVenue().getName(), subEvent.getStage().getName(), subEvent.getStartTime()),
            subEvent.getDescription(), subEvent.getTicketPrice(),
            new UnrestrictedAddress(subEvent.getStage().getVenue().getAddress().street(),
                subEvent.getStage().getVenue().getAddress().city().getCityName(),
                subEvent.getStage().getVenue().getAddress().postCode()),
            convertPerformersToString(subEvent), subEvent.getReviewScoreAndCount());
    }

    /**
     * Converts a {@link Ticket} to a {@link TicketDTO}.
     *
     * @param ticket
     *            is the ticket to be converted. Must not be null.
     * @return A {@link TicketDTO} object. Cannot be null;
     */
    @SuppressWarnings("nls")
    public static TicketDTO ticketToDTO(final Ticket ticket) {
        assert ticket != null : "Ticket must not be null!";

        final Stage ticketStage = ticket.getSeat().getRow().getSector().getStage();
        final Row ticketRow = ticket.getSeat().getRow();
        final SubEvent ticketSubEvent = ticket.getSubEvent();
        final PerformanceId performanceId = new PerformanceId(ticketSubEvent.getName(),
            ticketSubEvent.getEvent().getName());
        final PerformanceDetails performanceDetails = new PerformanceDetails(ticketStage.getVenue().getName(),
            ticketStage.getName(), ticketSubEvent.getStartTime());

        return new TicketDTO(ticket.getOrder().getId(), performanceId, ticketSubEvent.getDescription(),
            performanceDetails, ticketRow.getSector().getName(), ticketRow.getNumber(),
            StageDtoFactory.createSeatDto(ticket), ticket.getTicketPrice(), discountToDTO(ticket.getDiscount()));
    }

    /**
     * Converts a {@link Discount} to a {@link DiscountDTO}.
     *
     * @param discount
     *            is the discount to be converted. Must not be null.
     * @return A {@link DiscountDTO} object. Cannot be null;
     */
    @SuppressWarnings("nls")
    public static DiscountDTO discountToDTO(final Discount discount) {
        assert discount != null : "Discount must not be null!";
        return new DiscountDTO(discount.getName(), discount.getPercentage());
    }
}
