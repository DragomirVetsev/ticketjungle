package com.dxc.ghp.ticketj.services.impl;

import java.time.LocalTime;
import java.util.Collection;
import java.util.List;

import com.dxc.ghp.ticketj.dto.OfficeDTO;
import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.misc.exceptions.DuplicateFieldException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.exceptions.WorkerUnavailableException;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.OfficeLocationDetails;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Office;
import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;
import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeRepository;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeWorkerRepository;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.persistence.types.Phone;
import com.dxc.ghp.ticketj.persistence.types.RestrictedAddress;
import com.dxc.ghp.ticketj.persistence.types.UniqueOfficeLocation;
import com.dxc.ghp.ticketj.services.OfficeService;
import com.dxc.ghp.ticketj.services.impl.utilities.Converter;
import com.dxc.ghp.ticketj.services.impl.utilities.EntityFinder;
import com.dxc.ghp.ticketj.services.impl.utilities.ServicesCommon;
import com.dxc.ghp.ticketj.services.validation.ValidationEnforcer;
import com.dxc.ghp.ticketj.services.validation.domain.MandatoryOfficeDtoValidator;
import com.dxc.ghp.ticketj.services.validation.domain.OfficeNameValidator;
import com.dxc.ghp.ticketj.services.validation.domain.OptionalOfficeDtoValidator;

/**
 * {@link OfficeService} production implementation.
 */
public final class OfficeServiceImpl extends BaseService implements OfficeService {

    /**
     * Constructs a production implementation of {@link OfficeService}. For contract
     * see {@link BaseService#BaseService(TransactionHandler) base constructor}.
     */
    public OfficeServiceImpl(final TransactionHandler transactionHandler) {
        super(transactionHandler);
    }

    @Override
    public List<OfficeDTO> findAllOffices() {
        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final OfficeRepository officeRepositrory = provider.find(OfficeRepository.class);
            final List<Office> offices = officeRepositrory.findAllOffices();

            return Converter.convertList(offices, Converter::officeToDTO);
        });
    }

    @Override
    public OfficeDTO createOffice(final OfficeDTO newOffice) {
        ValidationEnforcer
            .verifyMandatoryField(OfficeDTO.class.getSimpleName(), newOffice, MandatoryOfficeDtoValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final City city = EntityFinder.findCityOrThrow(newOffice.officeAddress().city(), referentialRepository);
            final PhoneCode phoneCode = EntityFinder
                .findPhoneCodeOrThrow(newOffice.phone().code(), referentialRepository);

            final Phone officePhone = new Phone(phoneCode, newOffice.phone().number());
            final UniqueOfficeLocation officeLocation = new UniqueOfficeLocation(city,
                newOffice.officeAddress().street());

            final OfficeRepository officeRepositrory = provider.find(OfficeRepository.class);
            checkForDuplicateFields(newOffice.email(), officeRepositrory, officePhone, officeLocation);

            final Office persistedOffice = officeRepositrory
                .createOffice(newOffice.officeName(),
                              new RestrictedAddress(officeLocation.street(), officeLocation.city(),
                                  newOffice.officeAddress().postCode()),
                              officePhone, newOffice.email(), newOffice.workingHours());

            final OfficeWorkerRepository officeWorkerRepository = provider.find(OfficeWorkerRepository.class);
            modifyOfficeWorkers(newOffice.workers(), persistedOffice, officeWorkerRepository);

            return Converter.officeToDTO(persistedOffice);
        }, Office.class, newOffice.officeName());
    }

    @Override
    public OfficeDTO modifyOffice(final String officeName, final OfficeDTO officeToUpdate) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.NAME.toString(), officeName, OfficeNameValidator::new);
        ValidationEnforcer
            .verifyMandatoryField(OfficeDTO.class.getSimpleName(), officeToUpdate, OptionalOfficeDtoValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final OfficeRepository officeRepositrory = provider.find(OfficeRepository.class);
            return officeRepositrory.findOffice(officeName).map((final Office office) -> {
                modifyOfficeEmail(officeToUpdate.email(), office, officeRepositrory);
                modifyOfficeWorkingHours(officeToUpdate.workingHours(), office);

                final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
                modifyOfficeAddress(officeToUpdate.officeAddress(), referentialRepository, office, officeRepositrory);
                modifyOfficePhone(officeToUpdate.phone(), referentialRepository, office, officeRepositrory);

                final OfficeWorkerRepository officeWorkerRepository = provider.find(OfficeWorkerRepository.class);
                modifyOfficeWorkers(officeToUpdate.workers(), office, officeWorkerRepository);

                return Converter.officeToDTO(office);
            }).orElseThrow(() -> new EntityNotFoundException(Office.class, officeName));
        }, Office.class, officeName);
    }

    private static void checkForDuplicateFields(final String officeEmail, final OfficeRepository officeRepositrory,
                                                final Phone officePhone, final UniqueOfficeLocation officeLocation) {
        checkForDuplicateEmail(officeEmail, officeRepositrory);
        checkForDuplicateAddress(officeLocation, officeRepositrory);
        checkForDuplicatePhone(officePhone, officeRepositrory);
    }

    private static void modifyOfficeWorkers(final List<String> workers, final Office office,
                                            final OfficeWorkerRepository officeWorkerRepositrory) {
        if (ServicesCommon.notNull(workers)) {
            office.getWorkers().forEach((final OfficeWorker currentEmployee) -> currentEmployee.setWorkingPlace(null));
            final List<OfficeWorker> newlyEmployedWorkers = findAndEmployeeWorkers(workers, office,
                                                                                   officeWorkerRepositrory);
            office.changeEmployees(newlyEmployedWorkers);
        }
    }

    private static List<OfficeWorker> findAndEmployeeWorkers(final Collection<String> workers, final Office office,
                                                             final OfficeWorkerRepository officeWorkerRepositrory) {
        return workers.stream().map((final String candidate) -> {
            final OfficeWorker worker = EntityFinder.findWorkerOrThrow(candidate, officeWorkerRepositrory);
            if (ServicesCommon.notNull(worker.getWorkingPlace()) && !worker.getWorkingPlace().equals(office)) {
                throw new WorkerUnavailableException(worker.getWorkingPlace().getOfficeName(), worker.getUsername());
            }
            worker.setWorkingPlace(office);
            return worker;
        }).toList();
    }

    private static void modifyOfficeWorkingHours(final Period<LocalTime> workingHours, final Office officeFromRepo) {
        if (ServicesCommon.requiresModifying(workingHours)) {
            final Period<LocalTime> officeWorkingHours = officeFromRepo.getWorkingPeriod();
            final LocalTime openingHours = ServicesCommon.notNull(workingHours.startTime()) ? workingHours.startTime()
                : officeWorkingHours.startTime();
            final LocalTime closingHours = ServicesCommon.notNull(workingHours.endTime()) ? workingHours.endTime()
                : officeWorkingHours.endTime();

            officeFromRepo.changeWorkingPeriod(new Period<>(openingHours, closingHours));
        }
    }

    private static void modifyOfficeEmail(final String email, final Office officeFromRepo,
                                          final OfficeRepository officeRepositrory) {
        if (ServicesCommon.notNull(email) && !email.equals(officeFromRepo.getEmail())) {
            checkForDuplicateEmail(email, officeRepositrory);
            officeFromRepo.changeEmail(email);
        }
    }

    private static void modifyOfficePhone(final PhoneDTO phone, final ReferentialRepository referentialRepository,
                                          final Office officeFromRepo, final OfficeRepository officeRepositrory) {
        if (ServicesCommon.requiresModifying(phone)) {
            final Phone oldPhone = officeFromRepo.getPhone();
            final PhoneCode phoneCode = ServicesCommon.notNull(phone.code())
                ? EntityFinder.findPhoneCodeOrThrow(phone.code(), referentialRepository)
                : oldPhone.code();
            final String number = ServicesCommon.notNull(phone.number()) ? phone.number() : oldPhone.number();
            final Phone newPhone = new Phone(phoneCode, number);

            if (!newPhone.equals(oldPhone)) {
                checkForDuplicatePhone(newPhone, officeRepositrory);
                officeFromRepo.changePhone(newPhone);
            }
        }
    }

    private static void modifyOfficeAddress(final UnrestrictedAddress address,
                                            final ReferentialRepository referentialRepository,
                                            final Office officeFromRepo, final OfficeRepository officeRepositrory) {
        if (ServicesCommon.requiresModifying(address)) {
            final UniqueOfficeLocation oldLocation = new UniqueOfficeLocation(officeFromRepo.getAddress().city(),
                officeFromRepo.getAddress().street());
            final City city = ServicesCommon.notNull(address.city())
                ? EntityFinder.findCityOrThrow(address.city(), referentialRepository)
                : oldLocation.city();
            final String street = ServicesCommon.notNull(address.street()) ? address.street() : oldLocation.street();
            final String postCode = ServicesCommon.notNull(address.postCode()) ? address.postCode()
                : officeFromRepo.getAddress().postCode();
            final UniqueOfficeLocation newUniqueLocation = new UniqueOfficeLocation(city, street);

            if (!newUniqueLocation.equals(oldLocation)) {
                checkForDuplicateAddress(newUniqueLocation, officeRepositrory);
            }
            officeFromRepo.changeAddress(new RestrictedAddress(street, city, postCode));
        }
    }

    private static void checkForDuplicateAddress(final UniqueOfficeLocation address,
                                                 final OfficeRepository officeRepository) {
        if (officeRepository.addressExists(address)) {
            throw new DuplicateFieldException(Office.class, EntityFieldType.ADDRESS,
                new OfficeLocationDetails(address.city().getCityName(), address.street()));
        }
    }

    private static void checkForDuplicatePhone(final Phone phone, final OfficeRepository officeRepository) {
        if (officeRepository.phoneExists(phone)) {
            throw new DuplicateFieldException(Office.class, EntityFieldType.PHONE,
                new PhoneDTO(phone.code().getValue(), phone.number()));
        }
    }

    private static void checkForDuplicateEmail(final String email, final OfficeRepository officeRepositrory) {
        if (officeRepositrory.emailExists(email)) {
            throw new DuplicateFieldException(Office.class, EntityFieldType.EMAIL, email);
        }
    }
}
