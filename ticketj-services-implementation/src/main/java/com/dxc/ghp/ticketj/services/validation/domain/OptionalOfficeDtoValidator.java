package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.OfficeDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.basic.NotNullListValuesValidator;
import com.dxc.ghp.ticketj.services.validation.basic.LocalTimePeriodValidator;
import com.dxc.ghp.ticketj.services.validation.composite.EmailValidator;
import com.dxc.ghp.ticketj.services.validation.composite.OptionalFieldValidator;

/**
 * {@link OfficeDTO} validator. All fields are optional.
 */
public final class OptionalOfficeDtoValidator extends CompositeValidator<OfficeDTO> {

    /**
     * Constructs a validation class for {@code OfficeDTO}.
     *
     * @param name
     *            The name of the field. Must not be null.
     * @param officeDto
     *            The {@link OfficeDTO} to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public OptionalOfficeDtoValidator(final String name, final OfficeDTO officeDto, final List<ErrorInfo> errors) {
        super(name, officeDto,
            List
                .of(new OptionalFieldValidator<>(officeDto.workingHours(),
                    () -> new LocalTimePeriodValidator(officeDto.workingHours(), errors)),
                    new OptionalFieldValidator<>(officeDto.phone(),
                        () -> new OptionalPhoneValidator(EntityFieldType.PHONE.toString(), officeDto.phone(), errors)),
                    new OptionalFieldValidator<>(officeDto.email(),
                        () -> new EmailValidator(EntityFieldType.EMAIL.toString(), officeDto.email(), errors)),
                    new OptionalFieldValidator<>(officeDto.officeAddress(),
                        () -> new OptionalAddressValidator(EntityFieldType.ADDRESS.toString(),
                            officeDto.officeAddress(), errors)),
                    new OptionalFieldValidator<>(officeDto.workers(),
                        () -> new NotNullListValuesValidator<>(EntityFieldType.USERNAME.toString(), officeDto.workers(),
                            UsernameValidator::new, errors))));
    }
}
