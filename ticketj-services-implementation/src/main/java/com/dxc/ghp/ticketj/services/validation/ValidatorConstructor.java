package com.dxc.ghp.ticketj.services.validation;

/**
 * @param <Name>
 *            The type of the value's name.
 * @param <Value>
 *            The type of the value.
 * @param <Errors>
 *            The type of the errors.
 * @param <ResultValidator>
 *            The validation object that will be returned.
 */
@FunctionalInterface
public interface ValidatorConstructor<Name, Value, Errors, ResultValidator> {

    /**
     * Applies the function.
     *
     * @param name
     *            The name of the field.
     * @param value
     *            The value of the field.
     * @param errors
     *            The errors.
     * @return The validation object to be constructed and returned.
     */
    ResultValidator apply(Name name, Value value, Errors errors);
}
