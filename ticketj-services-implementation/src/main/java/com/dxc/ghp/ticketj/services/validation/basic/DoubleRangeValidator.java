package com.dxc.ghp.ticketj.services.validation.basic;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.BasicValidator;

/**
 * Validator class to validate that given {@code double} value is in given range
 * (inclusive).
 */
public final class DoubleRangeValidator extends BasicValidator<Double> {

    private final double min;
    private final double max;

    /**
     * Constructs validator class to validate that given {@code double} value is in
     * given range(inclusive).
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param number
     *            The double value of the field.
     * @param min
     *            The minimum allowed number.
     * @param max
     *            The maximum allowed number. {@code max >= min}
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    public DoubleRangeValidator(final String fieldName, final double number, final double min, final double max,
                                final List<ErrorInfo> errors) {
        super(fieldName, Double.valueOf(number), errors);
        this.min = min;
        this.max = max;
    }

    @Override
    public void validate() {
        if (value.doubleValue() < min || value.doubleValue() > max) {
            errors.add(new ValidationError<>(fieldName, value, ValidationErrorType.OUT_OF_RANGE.toString()));
        }

    }
}
