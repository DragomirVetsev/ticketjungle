package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Validation class for office name.
 */
public final class OfficeNameValidator extends CompositeValidator<String> {

    /**
     * Constructs office name validator.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param officeName
     *            The office name to be validated. Must not be null.
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    public OfficeNameValidator(final String fieldName, final String officeName, final List<ErrorInfo> errors) {

        super(fieldName, officeName,
            List
                .of(new StringFieldValidator(fieldName, officeName, Constants.OFFICE_MIN_LENGTH,
                    Constants.OFFICE_MAX_LENGTH, Constants.OFFICE_NAME_PATTERN, errors)));
    }
}
