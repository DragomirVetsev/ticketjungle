package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.OrderWithTicketsDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;

/**
 * {@link OrderWithTicketsDTO}
 */
public final class OrderWithTicketsDtoValidator extends CompositeValidator<OrderWithTicketsDTO> {

    /**
     * Constructs a validation class for {@code OrderWithTicketsDTO}.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param orderWithTicketsDTO
     *            The {@link OrderWithTicketsDTO} to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public OrderWithTicketsDtoValidator(final String fieldName, final OrderWithTicketsDTO orderWithTicketsDTO,
                                        final List<ErrorInfo> errors) {
        super(fieldName, orderWithTicketsDTO, List
            .of(new MandatoryFieldValidator(EntityFieldType.PAYMENT_TYPE.toString(), orderWithTicketsDTO.paymentType(),
                () -> new MandatoryPaymentValidator(EntityFieldType.PAYMENT_TYPE.toString(),
                    orderWithTicketsDTO.paymentType(), errors),
                errors),
                new MandatoryFieldValidator(EntityFieldType.DELIVERY_TYPE.toString(),
                    orderWithTicketsDTO.deliveryType(),
                    () -> new MandatoryDeliveryValidator(EntityFieldType.DELIVERY_TYPE.toString(),
                        orderWithTicketsDTO.deliveryType(), errors),
                    errors)
            // ,
            // new OptionalFieldValidator<>(orderWithTicketsDTO.deliveryAddress(),
            // () -> new OptionalAddressValidator(EntityFieldType.ADDRESS.toString(),
            // orderWithTicketsDTO.deliveryAddress(), errors))
            ));
    }

}
