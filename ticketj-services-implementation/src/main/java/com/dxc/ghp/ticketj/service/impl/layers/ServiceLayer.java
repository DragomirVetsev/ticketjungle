package com.dxc.ghp.ticketj.service.impl.layers;

import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProviderImpl;
import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistry;
import com.dxc.ghp.ticketj.misc.service.registry.ServiceRegistryImpl;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.services.CustomerService;
import com.dxc.ghp.ticketj.services.EventService;
import com.dxc.ghp.ticketj.services.OfficeService;
import com.dxc.ghp.ticketj.services.OfficeWorkerService;
import com.dxc.ghp.ticketj.services.ReferentialService;
import com.dxc.ghp.ticketj.services.UserService;
import com.dxc.ghp.ticketj.services.impl.CustomerServiceImpl;
import com.dxc.ghp.ticketj.services.impl.EventServiceImpl;
import com.dxc.ghp.ticketj.services.impl.OfficeServiceImpl;
import com.dxc.ghp.ticketj.services.impl.OfficeWorkerServiceImpl;
import com.dxc.ghp.ticketj.services.impl.ReferentialServiceImpl;
import com.dxc.ghp.ticketj.services.impl.UserServiceImpl;

/**
 * A utility class for creating and accessing services through a service
 * provider.
 */
public final class ServiceLayer {

    private ServiceLayer() {
        // A utility class that must not be instantiated.
    }

    /**
     * Creates and configures a service provider with the provided
     * TransactionHandler.
     *
     * @param trxHandler
     *            The TransactionHandler used to manage transactions for services.
     * @return A ServiceProvider instance initialized with registered services.
     */
    public static ServiceProvider createServiceProvider(final TransactionHandler trxHandler) {

        final ServiceRegistry<TransactionHandler> serviceRegistry = new ServiceRegistryImpl<>();
        serviceRegistry.register(EventService.class, EventServiceImpl::new);
        serviceRegistry.register(OfficeService.class, OfficeServiceImpl::new);
        serviceRegistry.register(OfficeWorkerService.class, OfficeWorkerServiceImpl::new);
        serviceRegistry.register(CustomerService.class, CustomerServiceImpl::new);
        serviceRegistry.register(UserService.class, UserServiceImpl::new);
        serviceRegistry.register(ReferentialService.class, ReferentialServiceImpl::new);

        return new ServiceProviderImpl<>(trxHandler, serviceRegistry);
    }
}
