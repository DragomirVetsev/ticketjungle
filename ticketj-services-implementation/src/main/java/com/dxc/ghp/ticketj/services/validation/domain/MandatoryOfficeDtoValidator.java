package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.OfficeDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.basic.NotNullListValuesValidator;
import com.dxc.ghp.ticketj.services.validation.composite.EmailValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;

/**
 * {@link OfficeDTO} validator. All fields are mandatory.
 */
public final class MandatoryOfficeDtoValidator extends CompositeValidator<OfficeDTO> {

    /**
     * Constructs a validation class for {@code OfficeDTO}.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param officeDto
     *            The {@link OfficeDTO} to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public MandatoryOfficeDtoValidator(final String fieldName, final OfficeDTO officeDto,
                                       final List<ErrorInfo> errors) {
        super(fieldName, officeDto, List
            .of(new MandatoryFieldValidator(EntityFieldType.NAME.toString(), officeDto.officeName(),
                () -> new OfficeNameValidator(EntityFieldType.NAME.toString(), officeDto.officeName(), errors), errors),
                new MandatoryFieldValidator(EntityFieldType.TIME_PERIOD.toString(), officeDto.workingHours(),
                    () -> new MandatoryWorkingHoursValidator(EntityFieldType.TIME_PERIOD.toString(),
                        officeDto.workingHours(), errors),
                    errors),
                new MandatoryFieldValidator(EntityFieldType.PHONE.toString(), officeDto.phone(),
                    () -> new MandatoryPhoneValidator(EntityFieldType.PHONE.toString(), officeDto.phone(), errors),
                    errors),
                new MandatoryFieldValidator(EntityFieldType.EMAIL.toString(), officeDto.email(),
                    () -> new EmailValidator(EntityFieldType.EMAIL.toString(), officeDto.email(), errors), errors),
                new MandatoryFieldValidator(EntityFieldType.ADDRESS.toString(), officeDto.officeAddress(),
                    () -> new MandatoryAddressValidator(EntityFieldType.ADDRESS.toString(), officeDto.officeAddress(),
                        errors),
                    errors),
                new MandatoryFieldValidator(EntityFieldType.WORKERS.toString(), officeDto.workers(),
                    () -> new NotNullListValuesValidator<>(EntityFieldType.USERNAME.toString(), officeDto.workers(),
                        UsernameValidator::new, errors),
                    errors)));
    }
}
