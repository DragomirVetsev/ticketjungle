/**
 *
 */
package com.dxc.ghp.ticketj.services.validation.composite;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.services.validation.Validator;

/**
 * Validation class for a email address
 */
public class EmailValidator implements Validator {

    private final Validator stringFieldValidator;

    /**
     * Constructs validation class for validating an email.
     *
     * @param name
     *            The name of the field. Must not be null.
     * @param email
     *            The email to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    @SuppressWarnings("nls")
    public EmailValidator(final String name, final String email, final List<ErrorInfo> errors) {
        assert name != null : "The name of the field must not be null.";
        stringFieldValidator = new StringFieldValidator(name, email, Constants.EMAIL_MIN_LENGTH,
            Constants.EMAIL_MAX_LENGTH, Constants.EMAIL_PATTERN, errors);
    }

    @Override
    public void validate() {
        stringFieldValidator.validate();
    }
}
