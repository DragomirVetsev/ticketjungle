package com.dxc.ghp.ticketj.services.validation.basic;

import java.util.List;
import java.util.regex.Pattern;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.BasicValidator;

/**
 * Validation class for checking if given string is allowed by given pattern.
 */
public final class StringPatternValidator extends BasicValidator<String> {

    private final String pattern;

    /**
     * Constructs String pattern validation class.
     *
     * @param fieldName
     *            The name of the {@code String} value that will be validated. Must
     *            not be null.
     * @param value
     *            The {@code String} value to be validated. Must not be null.
     * @param pattern
     *            The pattern that will be used to validate the given {@code value}.
     *            Must not be null.
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    @SuppressWarnings("nls")
    public StringPatternValidator(final String fieldName, final String value, final String pattern,
                                  final List<ErrorInfo> errors) {
        super(fieldName, value, errors);
        assert pattern != null : "The pattern must not be null.";

        this.pattern = pattern;
    }

    @Override
    public void validate() {
        if (!Pattern.matches(pattern, value)) {
            errors.add(new ValidationError<>(fieldName, value, ValidationErrorType.INVALID_SYMBOLS.name()));
        }
    }
}
