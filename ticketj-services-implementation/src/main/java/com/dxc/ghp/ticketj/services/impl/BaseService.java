package com.dxc.ghp.ticketj.services.impl;

import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;

/**
 * Base class for all services.
 */
public abstract class BaseService {

    /**
     * {@link TransactionHandler} used to execute business code in transactions.
     */
    protected final TransactionHandler trxHandler;

    /**
     * Constructs common state and behavior for all services.
     *
     * @param transactionHandler
     *            executes business code in transactions. Must not be null.
     */
    @SuppressWarnings("nls")
    protected BaseService(final TransactionHandler transactionHandler) {
        assert transactionHandler != null : "Transaction handler must not be null!";

        trxHandler = transactionHandler;
    }
}
