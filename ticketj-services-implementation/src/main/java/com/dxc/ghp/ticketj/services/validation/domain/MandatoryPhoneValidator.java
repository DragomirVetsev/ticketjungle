package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Mandatory validation class for {@link PhoneDTO}.
 */
public final class MandatoryPhoneValidator extends CompositeValidator<PhoneDTO> {

    /**
     * Constructs a validation class for {@code PhoneDTO}.
     *
     * @param name
     *            The name of the field. Must not be null.
     * @param phone
     *            The {@link PhoneDTO} to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public MandatoryPhoneValidator(final String name, final PhoneDTO phone, final List<ErrorInfo> errors) {
        super(name, phone,
            List
                .of(new MandatoryFieldValidator(EntityFieldType.PHONE_CODE.toString(), phone.code(),
                    () -> new StringFieldValidator(EntityFieldType.PHONE_CODE.toString(), phone.code(),
                        Constants.PHONE_CODE_MIN_LENGTH, Constants.PHONE_CODE_MAX_LENGTH, Constants.PHONE_CODE_PATTERN,
                        errors),
                    errors),
                    new MandatoryFieldValidator(EntityFieldType.PHONE_NUMBER.toString(), phone.number(),
                        () -> new StringFieldValidator(EntityFieldType.PHONE_NUMBER.toString(), phone.number(),
                            Constants.PHONE_NUMBER_MIN_LENGTH, Constants.PHONE_NUMBER_MAX_LENGTH,
                            Constants.PHONE_NUMBER_PATTERN, errors),
                        errors)));
    }
}
