package com.dxc.ghp.ticketj.services.validation.basic;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.BasicValidator;

/**
 * Validation class for checking if given string is in given range.
 */
public final class StringLengthValidator extends BasicValidator<String> {
    private final int min;
    private final int max;

    /**
     * Constructs String length validation class.
     *
     * @param fieldName
     *            The name of the {@code String} value that will be validated. Must
     *            not be null.
     * @param value
     *            The {@code String} value to be validated. Must not be null.
     * @param min
     *            Constraint for the minimum number of characters allowed.
     *            Constraints:{@code min >= 0}
     * @param max
     *            Constraint for the maximum number of characters allowed.
     *            Constraints: [{@code max >= 0}, {@code max >= min}, {@code max <=}
     *            {@link Integer#MAX_VALUE}]
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    @SuppressWarnings("nls")
    public StringLengthValidator(final String fieldName, final String value, final int min, final int max,
                                 final List<ErrorInfo> errors) {
        super(fieldName, value, errors);
        assert min >= 0 : "The minimum number of characters cannot be less than 0.";
        assert max >= min : "The maximum number must not be less than minimum number.";

        this.min = min;
        this.max = max;
    }

    @Override
    public void validate() {
        if (value.length() < min || value.length() > max) {
            errors.add(new ValidationError<>(fieldName, value, ValidationErrorType.INVALID_LENGTH.toString()));
        }
    }

}
