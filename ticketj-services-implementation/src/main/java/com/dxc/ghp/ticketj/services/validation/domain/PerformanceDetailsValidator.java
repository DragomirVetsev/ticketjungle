package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.basic.NullValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Validation class for {@link PerformanceDetails};
 */
public final class PerformanceDetailsValidator extends CompositeValidator<PerformanceDetails> {

    /**
     * Constructs a validation class for {@code PerformanceDetails}.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param performanceDetails
     *            The {@code PerformanceDetails} that will be validated. Must not be
     *            null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    @SuppressWarnings("nls")
    public PerformanceDetailsValidator(final String fieldName, final PerformanceDetails performanceDetails,
                                       final List<ErrorInfo> errors) {
        super(fieldName, performanceDetails, List
            .of(new MandatoryFieldValidator(EntityFieldType.VENUE.toString(), performanceDetails.venue(),
                () -> new StringFieldValidator(EntityFieldType.VENUE.toString(), performanceDetails.venue(),
                    Constants.VENUE_MIN_LENGTH, Constants.VENUE_MAX_LENGTH, Constants.VENUE_PATTERN, errors),
                errors),
                new MandatoryFieldValidator(EntityFieldType.STAGE.toString(), performanceDetails.stage(),
                    () -> new StringFieldValidator(EntityFieldType.STAGE.toString(), performanceDetails.stage(),
                        Constants.STAGE_MIN_LENGTH, Constants.STAGE_MAX_LENGTH, Constants.STAGE_PATTERN, errors),
                    errors),
                new MandatoryFieldValidator("startDateTime", performanceDetails.startDateTime(), NullValidator::new,
                    errors)));
    }
}
