package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.EventSearchDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.basic.InstantPeriodValidator;
import com.dxc.ghp.ticketj.services.validation.composite.OptionalFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Validation class used to validate {@link EventSearchDTO}.
 */
public final class EventSearchDtoValidator extends CompositeValidator<EventSearchDTO> {

    /**
     * Constructs a validation class for {@code EventSearchDTO}.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param eventSearchDto
     *            The criteria DTO to be validated. Must not be null.
     * @param errors
     *            The list with errors. Must not be null.
     */
    public EventSearchDtoValidator(final String fieldName, final EventSearchDTO eventSearchDto,
                                   final List<ErrorInfo> errors) {

        super(fieldName, eventSearchDto,
            List
                .of(new OptionalFieldValidator<>(eventSearchDto.city(),
                    () -> new StringFieldValidator(EntityFieldType.CITY.toString(), eventSearchDto.city(),
                        Constants.CITY_MIN_LENGTH, Constants.CITY_MAX_LENGTH, Constants.CITY_PATTERN, errors)),
                    new OptionalFieldValidator<>(eventSearchDto.genre(),
                        () -> new StringFieldValidator(EntityFieldType.GENRE.toString(), eventSearchDto.genre(),
                            Constants.GENRE_MIN_LENGTH, Constants.GENRE_MAX_LENGTH, Constants.GENRE_PATTERN, errors)),
                    new OptionalFieldValidator<>(eventSearchDto.duration(),
                        () -> new InstantPeriodValidator(eventSearchDto.duration(), errors))));

    }
}
