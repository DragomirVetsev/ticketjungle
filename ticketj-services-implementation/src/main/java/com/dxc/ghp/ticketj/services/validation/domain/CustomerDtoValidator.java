/**
 *
 */
package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.CustomerDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.EmailValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;

/**
 * Validation class for {@link CustomerDTO}.
 */
public final class CustomerDtoValidator extends CompositeValidator<CustomerDTO> {

    /**
     * Constructs a validation class for {@code CustomerDto}.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param customerDto
     *            The {@link CustomerDTO} to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public CustomerDtoValidator(final String fieldName, final CustomerDTO customerDto, final List<ErrorInfo> errors) {
        super(fieldName, customerDto, List
            .of(new MandatoryFieldValidator(EntityFieldType.USERNAME.toString(), customerDto.username(),
                () -> new UsernameValidator(EntityFieldType.USERNAME.toString(), customerDto.username(), errors),
                errors),
                new MandatoryFieldValidator(EntityFieldType.PERSON_NAME.toString(), customerDto.personName(),
                    () -> new PersonNameValidator(EntityFieldType.PERSON_NAME.toString(), customerDto.personName(),
                        errors),
                    errors),
                new MandatoryFieldValidator(EntityFieldType.EMAIL.toString(), customerDto.email(),
                    () -> new EmailValidator(EntityFieldType.EMAIL.toString(), customerDto.email(), errors), errors),
                new MandatoryFieldValidator(EntityFieldType.PHONE.toString(), customerDto.phone(),
                    () -> new MandatoryPhoneValidator(EntityFieldType.PHONE.toString(), customerDto.phone(), errors),
                    errors),
                new MandatoryFieldValidator(EntityFieldType.ADDRESS.toString(), customerDto.address(),
                    () -> new MandatoryAddressValidator(EntityFieldType.ADDRESS.toString(), customerDto.address(),
                        errors),
                    errors)));
    }
}
