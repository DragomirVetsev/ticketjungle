/**
 *
 */
package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 *
 */
public final class MandatoryPaymentValidator extends CompositeValidator<String> {

    /**
     * Constructs payment name validator.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param paymentName
     *            The payment name to be validated. Must not be null.
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    public MandatoryPaymentValidator(final String fieldName, final String paymentName, final List<ErrorInfo> errors) {

        super(fieldName, paymentName,
            List
                .of(new StringFieldValidator(fieldName, paymentName, Constants.PAYMENT_MIN_LENGTH,
                    Constants.PAYMENT_MAX_LENGTH, Constants.PAYMENT_NAME_PATTERN, errors)));
    }

}
