package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Validation class for {@code Performer}'s name.
 */
public final class PerformerNameValidator extends CompositeValidator<String> {

    /**
     * Constructs validator for {@code Performer}'s name.
     *
     * @param fieldName
     *            The name of the value. Must not be null.
     * @param value
     *            The {@code Performer}'s name that will be validated. Must not be
     *            null.
     * @param errors
     *            The list that will collect the errors during valdiation. Must not
     *            be null.
     */
    public PerformerNameValidator(final String fieldName, final String value, final List<ErrorInfo> errors) {
        super(fieldName, value,
            List
                .of(new MandatoryFieldValidator(EntityFieldType.PERFORMER.toString(), value,
                    () -> new StringFieldValidator(EntityFieldType.PERFORMER.toString(), value,
                        Constants.PERFORMER_MIN_LENGTH, Constants.PERFORMER_MAX_LENGTH, Constants.PERFORMER_PATTERN,
                        errors),
                    errors)));
    }
}
