package com.dxc.ghp.ticketj.services.validation.basic;

import java.time.Instant;
import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Period;

/**
 * {@link Period} Validator for {@link Instant} values.
 */
public final class InstantPeriodValidator extends BasePeriodValidator<Instant> {

    /**
     * Constructs InstantPeriodValidator. For contract
     * {@link BasePeriodValidator#BasePeriodValidator(Period, List) base
     * constructor}.
     */
    public InstantPeriodValidator(final Period<Instant> period, final List<ErrorInfo> errors) {
        super(period, errors);
    }

    @Override
    protected boolean startTimeIsAfterEndTime() {
        return value.startTime().isAfter(value.endTime());
    }

}
