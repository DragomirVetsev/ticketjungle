package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.EventDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.basic.StringLengthValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.OptionalFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Validation class for {@link EventDTO}
 */
public final class OptionalEventDtoValidator extends CompositeValidator<EventDTO> {

    /**
     * Constructs a validation class for {@code EventDTO}.
     *
     * @param name
     *            The name of the field. Must not be null.
     * @param eventDto
     *            The {@link EventDTO} to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public OptionalEventDtoValidator(final String name, final EventDTO eventDto, final List<ErrorInfo> errors) {
        super(name, eventDto, List
            .of(new MandatoryFieldValidator(EntityFieldType.EVENT.toString(), eventDto.getName(),
                () -> new EventNameValidator(EntityFieldType.EVENT.toString(), eventDto.getName(), errors), errors),
                new OptionalFieldValidator<>(eventDto.getDescription(),
                    () -> new StringLengthValidator(EntityFieldType.DESCRIPTION.toString(), eventDto.getDescription(),
                        Constants.EVENT_DESCRIPTION_MIN_LENGTH, Constants.EVENT_DESCRIPTION_MAX_LENGTH, errors)),
                new OptionalFieldValidator<>(eventDto.getGenre(),
                    () -> new StringFieldValidator(EntityFieldType.GENRE.toString(), eventDto.getGenre(),
                        Constants.GENRE_MIN_LENGTH, Constants.GENRE_MAX_LENGTH, Constants.GENRE_PATTERN, errors))));
    }
}
