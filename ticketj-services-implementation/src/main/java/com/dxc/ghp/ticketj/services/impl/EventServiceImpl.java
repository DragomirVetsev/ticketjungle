package com.dxc.ghp.ticketj.services.impl;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.dxc.ghp.ticketj.dto.CartItemDTO;
import com.dxc.ghp.ticketj.dto.EventDTO;
import com.dxc.ghp.ticketj.dto.EventSearchDTO;
import com.dxc.ghp.ticketj.dto.StageDTO;
import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.dto.SubEventOrderedPairDTO;
import com.dxc.ghp.ticketj.misc.exceptions.DuplicateFieldException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityHasRelationshipException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.Performer;
import com.dxc.ghp.ticketj.persistence.entities.SeatStatus;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.entities.Ticket;
import com.dxc.ghp.ticketj.persistence.repositories.CustomerRepository;
import com.dxc.ghp.ticketj.persistence.repositories.EventRepository;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.services.EventService;
import com.dxc.ghp.ticketj.services.impl.utilities.Converter;
import com.dxc.ghp.ticketj.services.impl.utilities.EntityFinder;
import com.dxc.ghp.ticketj.services.impl.utilities.ServicesCommon;
import com.dxc.ghp.ticketj.services.impl.utilities.StageDtoFactory;
import com.dxc.ghp.ticketj.services.validation.ValidationEnforcer;
import com.dxc.ghp.ticketj.services.validation.domain.EventDtoValidator;
import com.dxc.ghp.ticketj.services.validation.domain.EventNameValidator;
import com.dxc.ghp.ticketj.services.validation.domain.EventSearchDtoValidator;
import com.dxc.ghp.ticketj.services.validation.domain.OptionalEventDtoValidator;
import com.dxc.ghp.ticketj.services.validation.domain.OptionalSubEventDtoValidator;
import com.dxc.ghp.ticketj.services.validation.domain.SubEventDtoValidator;

/**
 * {@link EventService} production implementation.
 */
public final class EventServiceImpl extends BaseService implements EventService {

    /**
     * Constructs a production implementation of {@link EventService}. For contract
     * see {@link BaseService#BaseService(TransactionHandler) base constructor}.
     */
    public EventServiceImpl(final TransactionHandler transactionHandler) {
        super(transactionHandler);
    }

    @Override
    public List<EventDTO> findAllEvents() {
        final List<Event> events = trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository.findAllEvents();
        });

        return Converter.convertList(events, event -> Converter.convertEventToDTO(event));
    }

    @Override
    public EventDTO createEvent(final EventDTO eventDto) {
        ValidationEnforcer.verifyMandatoryField(EventDTO.class.getSimpleName(), eventDto, EventDtoValidator::new);

        final Event event = trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final Genre genre = EntityFinder.findGenreOrThrow(eventDto.getGenre(), referentialRepository);

            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository.createEvent(eventDto.getName(), eventDto.getDescription(), genre);
        }, Event.class, eventDto.getName());

        return Converter.convertEventToDTO(event);
    }

    @Override
    public SubEventDTO createSubEvent(final SubEventDTO newSubEvent) {
        ValidationEnforcer
            .verifyMandatoryField(SubEventDTO.class.getSimpleName(), newSubEvent, SubEventDtoValidator::new);

        final SubEvent subEvent = trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);

            final Event event = EntityFinder
                .findEventOrThrow(newSubEvent.performanceId().eventName(), referentialRepository);
            final Stage stage = EntityFinder
                .findStageOrThrow(newSubEvent.performanceDetails().stage(), newSubEvent.performanceDetails().venue(),
                                  referentialRepository);
            final List<String> performers = newSubEvent.performers();

            if (ServicesCommon.notNull(performers)) {
                getOrCreatePerformers(performers, referentialRepository);
            }

            final EventRepository eventRepository = provider.find(EventRepository.class);
            return eventRepository
                .createSubEvent(newSubEvent.performanceId().name(), event, newSubEvent.description(),
                                newSubEvent.ticketPrice(), newSubEvent.performanceDetails().startDateTime(), stage,
                                performers);
        }, SubEvent.class, newSubEvent.performanceId());

        return Converter.convertSubEventToDTO(subEvent);
    }

    @Override
    public List<EventDTO> findByCriteria(final EventSearchDTO criteriaDto) {
        ValidationEnforcer
            .verifyMandatoryField(EventSearchDTO.class.getSimpleName(), criteriaDto, EventSearchDtoValidator::new);

        final List<Event> events = trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final City cityForCriteria = findCityForCriteria(referentialRepository, criteriaDto.city());
            final Genre genreForCriteria = findGenresForCriteria(referentialRepository, criteriaDto.genre());
            return eventRepository.findByCriteria(criteriaDto.duration(), cityForCriteria, genreForCriteria);
        });
        return Converter.convertList(events, event -> Converter.convertEventToDTO(event));
    }

    private static City findCityForCriteria(final ReferentialRepository referentialRepository, final String cityName) {
        City city = null;
        if (cityName != null) {
            city = EntityFinder.findCityOrThrow(cityName, referentialRepository);
        }
        return city;
    }

    private static Genre findGenresForCriteria(final ReferentialRepository referentialRepository,
                                               final String genreName) {
        Genre genre = null;
        if (genreName != null) {
            genre = EntityFinder.findGenreOrThrow(genreName, referentialRepository);
        }
        return genre;
    }

    @Override
    public EventDTO findByName(final String name) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.EVENT.toString(), name, EventNameValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final Event foundEvent = eventRepository
                .findByName(name)
                .orElseThrow(() -> new EntityNotFoundException(Event.class, name));

            return Converter.convertEventToDTO(foundEvent);
        });
    }

    @Override
    public List<SubEventDTO> findAllSubEventsInEvent(final String eventName) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.EVENT.toString(), eventName, EventNameValidator::new);
        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final Event foundEvent = eventRepository
                .findByName(eventName)
                .orElseThrow(() -> new EntityNotFoundException(Event.class, eventName));

            return Converter
                .convertList(foundEvent.getSubEvents(), subEvent -> Converter.convertSubEventToDTO(subEvent));
        });
    }

    @Override
    public StageDTO findSubEventStageSeats(final PerformanceId performance) {

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final SubEvent foundSubEvent = eventRepository
                .findSubEvent(performance)
                .orElseThrow(() -> new EntityNotFoundException(SubEvent.class, performance));

            return StageDtoFactory.createFilledStageDto(foundSubEvent);
        });
    }

    @Override
    public SubEventOrderedPairDTO findAllSubEventsOrdered() {
        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final List<SubEvent> foundSubEventsByTime = eventRepository.findAllSubEventsOrderedByTime();
            final List<SubEvent> foundSubEventsByPopularity = eventRepository.findAllSubEventsOrderedByPopularity();

            return new SubEventOrderedPairDTO(
                Converter.convertList(foundSubEventsByTime, subEvent -> Converter.convertSubEventToDTO(subEvent)),
                Converter
                    .convertList(foundSubEventsByPopularity, subEvent -> Converter.convertSubEventToDTO(subEvent)));
        });
    }

    @Override
    public EventDTO updateEvent(final String eventName, final EventDTO eventWithUpdate) {
        ValidationEnforcer
            .verifyMandatoryField(EventDTO.class.getSimpleName(), eventWithUpdate, OptionalEventDtoValidator::new);

        final Event modifiedEvent = trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final EventRepository eventRepositrory = provider.find(EventRepository.class);

            return eventRepositrory.findByName(eventName).map((final Event event) -> {
                modifyEventDescription(eventWithUpdate.getDescription(), event);
                modifyEventGenre(eventWithUpdate.getGenre(), referentialRepository, event);

                return event;
            }).orElseThrow(() -> new EntityNotFoundException(Event.class, eventName));
        }, Event.class, eventWithUpdate.getName());

        return Converter.convertEventToDTO(modifiedEvent);
    }

    private static void modifyEventDescription(final String description, final Event repositoryEvent) {
        if (ServicesCommon.notNull(description)) {
            repositoryEvent.setDescription(description);
        }
    }

    private static void modifyEventGenre(final String genre, final ReferentialRepository referentialRepository,
                                         final Event repositoryEvent) {
        final Genre foundGenre = ServicesCommon.notNull(genre)
            ? EntityFinder.findGenreOrThrow(genre, referentialRepository)
            : repositoryEvent.getGenre();
        repositoryEvent.setGenre(foundGenre);
    }

    @Override
    public SubEventDTO updateSubEvent(final PerformanceId performance, final SubEventDTO subEventWithUpdate) {
        ValidationEnforcer
            .verifyMandatoryField(SubEventDTO.class.getSimpleName(), subEventWithUpdate,
                                  OptionalSubEventDtoValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);

            final SubEvent modifiedSubEvent = findAndModifySubEventOrThrow(performance, subEventWithUpdate,
                                                                           eventRepository, referentialRepository);
            return Converter.convertSubEventToDTO(modifiedSubEvent);
        }, SubEvent.class, performance);
    }

    private static SubEvent findAndModifySubEventOrThrow(final PerformanceId performance,
                                                         final SubEventDTO subEventWithUpdate,
                                                         final EventRepository eventRepository,
                                                         final ReferentialRepository referentialRepository) {
        return eventRepository.findSubEvent(performance).map((final SubEvent repositorySubEvent) -> {
            if (ServicesCommon.notNull(subEventWithUpdate.performanceDetails())) {
                checkForDuplicateDetails(subEventWithUpdate, repositorySubEvent, eventRepository);
            }

            modifySubEvent(subEventWithUpdate, repositorySubEvent, referentialRepository);
            return repositorySubEvent;
        }).orElseThrow(() -> new EntityNotFoundException(SubEvent.class, performance));
    }

    private static void checkForDuplicateDetails(final SubEventDTO subEventWithUpdate,
                                                 final SubEvent repositorySubEvent,
                                                 final EventRepository eventRepository) {
        final PerformanceDetails fullDetails = new PerformanceDetails(
            ServicesCommon.notNull(subEventWithUpdate.performanceDetails().venue())
                ? subEventWithUpdate.performanceDetails().venue()
                : repositorySubEvent.getStage().getVenue().getName(),
            ServicesCommon.notNull(subEventWithUpdate.performanceDetails().stage())
                ? subEventWithUpdate.performanceDetails().stage()
                : repositorySubEvent.getStage().getName(),
            ServicesCommon.notNull(subEventWithUpdate.performanceDetails().startDateTime())
                ? subEventWithUpdate.performanceDetails().startDateTime()
                : repositorySubEvent.getStartTime());

        if (eventRepository
            .findSubEventByDetails(fullDetails)
            .filter(byDetails -> !byDetails.getId().equals(subEventWithUpdate.performanceId()))
            .isPresent()) {
            throw new DuplicateFieldException(SubEvent.class, EntityFieldType.SUB_EVENT_DETAILS, fullDetails);
        }
    }

    private static void modifySubEvent(final SubEventDTO subEventWithUpdate, final SubEvent repositorySubEvent,
                                       final ReferentialRepository referentialRepository) {
        modifySubEventDescription(subEventWithUpdate.description(), repositorySubEvent);
        modifySubEventTicketPrice(subEventWithUpdate.ticketPrice(), repositorySubEvent);
        modifySubEventPerformers(subEventWithUpdate.performers(), repositorySubEvent, referentialRepository);
        if (ServicesCommon.notNull(subEventWithUpdate.performanceDetails())) {
            modifySubEventStage(subEventWithUpdate.performanceDetails().stage(),
                                subEventWithUpdate.performanceDetails().venue(), repositorySubEvent,
                                referentialRepository);
            modifySubEventStartTime(subEventWithUpdate.performanceDetails().startDateTime(), repositorySubEvent);
        }
    }

    private static void modifySubEventDescription(final String description, final SubEvent repositorySubEvent) {
        if (ServicesCommon.notNull(description)) {
            repositorySubEvent.setDescription(description);
        }
    }

    private static void modifySubEventTicketPrice(final double ticketPrice, final SubEvent repositorySubEvent) {
        if (requiresModifying(ticketPrice)) {
            repositorySubEvent.setTicketPrice(ticketPrice);
        }
    }

    private static void modifySubEventPerformers(final List<String> performerNames, final SubEvent repositorySubEvent,
                                                 final ReferentialRepository referentialRepository) {
        if (ServicesCommon.notNull(performerNames)) {
            final List<Performer> performers = getOrCreatePerformers(performerNames, referentialRepository);

            repositorySubEvent.setPerformers(performers);
        }
    }

    private static void modifySubEventStage(final String stage, final String venue, final SubEvent repositorySubEvent,
                                            final ReferentialRepository referentialRepository) {
        final Stage foundStage = ServicesCommon.notNull(stage)
            ? EntityFinder.findStageOrThrow(stage, venue, referentialRepository)
            : repositorySubEvent.getStage();
        repositorySubEvent.setStage(foundStage);
    }

    private static void modifySubEventStartTime(final Instant startTime, final SubEvent repositorySubEvent) {
        if (ServicesCommon.notNull(startTime)) {
            repositorySubEvent.setStartTime(startTime);
        }
    }

    @SuppressWarnings("nls")
    private static List<Performer> getOrCreatePerformers(final Collection<String> performerNames,
                                                         final ReferentialRepository referentialRepository) {
        final List<Performer> performers = new ArrayList<>(performerNames.size());

        for (final String performerName : performerNames) {

            final Performer performer = referentialRepository
                .findPerformer(performerName)
                .orElseGet(() -> referentialRepository.createPerformer(performerName, "Auto-generated description."));

            performers.add(performer);
        }

        return performers;
    }

    // checks if double needs to be modified because it is different from 0.0, which
    // means it is initialized.
    private static boolean requiresModifying(final double attribute) {
        final BigDecimal uninitialized = BigDecimal.valueOf(0.0);
        return BigDecimal.valueOf(attribute).compareTo(uninitialized) != 0;
    }

    @Override
    public SubEventDTO deleteSubEvent(final PerformanceId performance) {
        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final EventRepository eventRepository = provider.find(EventRepository.class);

            final SubEvent subEventToDelete = eventRepository
                .findSubEvent(performance)
                .orElseThrow(() -> new EntityNotFoundException(SubEvent.class, performance));

            checkIfSubEventHasRelationships(performance, subEventToDelete);

            eventRepository.deleteSubEvent(subEventToDelete);
            return Converter.convertSubEventToDTO(subEventToDelete);
        }, SubEvent.class, performance);
    }

    private static void checkIfSubEventHasRelationships(final PerformanceId performance,
                                                        final SubEvent subEventToDelete) {
        if (!subEventToDelete.getTickets().isEmpty() || !subEventToDelete.getReviews().isEmpty()) {
            throw new EntityHasRelationshipException(SubEvent.class, performance);
        }
    }

    @SuppressWarnings("nls")
    @Override
    public void createCartItem(final List<CartItemDTO> cartItems) {
        trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final SeatStatus reservedStatus = EntityFinder
                .findSeatStatusOrThrow(Constants.RESERVED_STATUS_TICKET, referentialRepository);

            for (final CartItemDTO cartItemDTO : cartItems) {

                boolean isSubEventTicket = false;

                final SubEvent foundSubEvent = eventRepository
                    .findSubEvent(cartItemDTO.performance())
                    .orElseThrow(() -> new EntityNotFoundException(SubEvent.class, cartItemDTO.performance()));

                final List<Ticket> tickets = foundSubEvent.getTickets();
                for (final Ticket ticketSubEvent : tickets) {
                    if (ticket_IsOfSubevent(cartItemDTO, ticketSubEvent)) {
                        isSubEventTicket = true;

                        if (ticketSubEvent
                            .getSeatStatus()
                            .getStatus()
                            .equalsIgnoreCase(Constants.AVAILABLE_STATUS_TICKET)) {
                            ticketSubEvent.changeSeatStatus(reservedStatus);
                            final Customer customer = EntityFinder
                                .findCustomerOrThrow(cartItemDTO.username(), customerRepository);
                            eventRepository.createCartItem(customer, ticketSubEvent);
                            break;
                        }
                        throw new EntityNotFoundException(Ticket.class, "The ticket isn't already reserved!");

                    }
                }
                if (!isSubEventTicket) {
                    throw new EntityNotFoundException(Ticket.class, "The ticket isn't present for this performance!");
                }
            }
        });
    }

    private static boolean ticket_IsOfSubevent(final CartItemDTO cartItemDTO, final Ticket ticketSubEvent) {
        return cartItemDTO.seat().number() == ticketSubEvent.getSeat().getNumber()
            && cartItemDTO.row() == ticketSubEvent.getSeat().getRow().getNumber()
            && cartItemDTO.sector().equals(ticketSubEvent.getSeat().getRow().getSector().getName())
            && cartItemDTO.stage().equals(ticketSubEvent.getSeat().getRow().getSector().getStage().getName())
            && cartItemDTO.venue().equals(ticketSubEvent.getSeat().getRow().getSector().getStage().getVenue().getName())
            && cartItemDTO.performance().eventName().equals(ticketSubEvent.getSubEvent().getEvent().getName())
            && cartItemDTO.performance().name().equals(ticketSubEvent.getSubEvent().getName());
    }
}
