package com.dxc.ghp.ticketj.services.validation.composite;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.basic.StringCompareValidator;

/**
 * Validation credentials password.
 */
public class PasswordValidator implements Validator {
    private final List<Validator> validators;

    /**
     * @param fieldName
     *            The name of the {@code double} value.
     * @param password
     *            The password to be validated.
     * @param username
     *            The username that comes with with the password.
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    public PasswordValidator(final String fieldName, final String password, final String username,
                             final List<ErrorInfo> errors) {
        validators = List
            .of(new StringFieldValidator(fieldName, password, Constants.PASSWORD_MIN_LENGTH,
                Constants.PASSWORD_MAX_LENGTH, Constants.PASSWORD_PATTERN, errors),
                new StringCompareValidator(fieldName, password, username, errors));
    }

    @Override
    public void validate() {
        validators.forEach(Validator::validate);
    }

}
