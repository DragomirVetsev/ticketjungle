package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;

/**
 * Validation class for {@PerformanceId}.
 */
public final class PerformanceIdValidator extends CompositeValidator<PerformanceId> {

    /**
     * Constructs validation class for {@PerformanceId}.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param performanceId
     *            The {@code PerformanceId} that will be validated. Must not be
     *            null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public PerformanceIdValidator(final String fieldName, final PerformanceId performanceId,
                                  final List<ErrorInfo> errors) {
        super(fieldName, performanceId, List
            .of(new MandatoryFieldValidator(EntityFieldType.EVENT.toString(), performanceId.eventName(),
                () -> new EventNameValidator(EntityFieldType.EVENT.toString(), performanceId.eventName(), errors),
                errors),
                new MandatoryFieldValidator(EntityFieldType.SUB_EVENT.toString(), performanceId.name(),
                    () -> new SubEventNameValidator(EntityFieldType.SUB_EVENT.toString(), performanceId.name(), errors),
                    errors)));
    }
}
