/**
 *
 */
package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Validation class for {@link PersonName}
 */
public final class PersonNameValidator extends CompositeValidator<PersonName> {

    /**
     * Constructs a validation class for {@code PersonName}.
     *
     * @param name
     *            The name of the field. Must not be null.
     * @param personalName
     *            The {@link PersonName} to be validated. Must not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public PersonNameValidator(final String name, final PersonName personalName, final List<ErrorInfo> errors) {
        super(name, personalName, List
            .of(new StringFieldValidator(EntityFieldType.PERSON_GIVEN_NAME.toString(), personalName.givenName(),
                Constants.USERNAME_MIN_LENGTH, Constants.GIVEN_NAME_MAX_LENGTH, Constants.PERSON_NAME_PATTERN, errors),
                new StringFieldValidator(EntityFieldType.PERSON_FAMALY_NAME.toString(), personalName.familyName(),
                    Constants.USERNAME_MIN_LENGTH, Constants.FAMILY_NAME_MAX_LENGTH, Constants.PERSON_NAME_PATTERN,
                    errors)));
    }
}
