package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.basic.IntegerPositiveValidator;
import com.dxc.ghp.ticketj.services.validation.composite.DoubleFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;

/**
 * Validation class for {@link ReviewScoreAndCount}
 */
public final class ReviewScoreAndCountValidator extends CompositeValidator<ReviewScoreAndCount> {

    /**
     * Constructs a validation class for {@code ReviewScoreAndCount}.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param reviewScoreAndCount
     *            The {@code ReviewScoreAndCount} value that will be validated. Must
     *            not be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public ReviewScoreAndCountValidator(final String fieldName, final ReviewScoreAndCount reviewScoreAndCount,
                                        final List<ErrorInfo> errors) {
        super(fieldName, reviewScoreAndCount,
            List
                .of(new MandatoryFieldValidator(EntityFieldType.REVIEW_SCORE.toString(),
                    Double.valueOf(reviewScoreAndCount.score()),
                    () -> new DoubleFieldValidator(EntityFieldType.REVIEW_SCORE.toString(), reviewScoreAndCount.score(),
                        Constants.REVIEW_SCORE_ALLOWED_DECIMALS, Constants.REVIEW_SCORE_MIN, Constants.REVIEW_SCORE_MAX,
                        errors),
                    errors),
                    new MandatoryFieldValidator(EntityFieldType.REVIEW_COUNT.toString(),
                        Integer.valueOf(reviewScoreAndCount.count()),
                        () -> new IntegerPositiveValidator(EntityFieldType.REVIEW_COUNT.toString(),
                            reviewScoreAndCount.count(), errors),
                        errors)));
    }
}
