package com.dxc.ghp.ticketj.services.impl;

import java.util.List;

import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeWorkerRepository;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.services.OfficeWorkerService;
import com.dxc.ghp.ticketj.services.impl.utilities.Converter;

/**
 * {@link OfficeWorkerService} production implementation.
 */
public final class OfficeWorkerServiceImpl extends BaseService implements OfficeWorkerService {

    /**
     * Constructs a production implementation of {@link OfficeWorkerService}. For
     * contract see {@link BaseService#BaseService(TransactionHandler) base
     * constructor}.
     */
    public OfficeWorkerServiceImpl(final TransactionHandler transactionHandler) {
        super(transactionHandler);
    }

    @Override
    public List<String> findAllAvailableWorkers() {
        final List<OfficeWorker> avaialbleWorkers = trxHandler
            .executeInTransaction((final ServiceProvider provider) -> {
                final OfficeWorkerRepository officeWorkerRepository = provider.find(OfficeWorkerRepository.class);
                return officeWorkerRepository.findAllAvailableWorkers();
            });
        return Converter.convertList(avaialbleWorkers, OfficeWorker::getUsername);
    }
}
