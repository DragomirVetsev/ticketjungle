package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.CustomerWithCredentialsDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;
import com.dxc.ghp.ticketj.services.validation.composite.PasswordValidator;

/**
 * Validation class for {@code Credentials}.
 */
public final class CredentialsValidator extends CompositeValidator<Credentials> {

    /**
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param redentials
     *            The {@link CustomerWithCredentialsDTO} to be validated. Must not
     *            be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public CredentialsValidator(final String fieldName, final Credentials redentials, final List<ErrorInfo> errors) {
        super(fieldName, redentials,
            List
                .of(new MandatoryFieldValidator(EntityFieldType.USERNAME.toString(), redentials.username(),
                    () -> new UsernameValidator(EntityFieldType.USERNAME.toString(), redentials.username(), errors),
                    errors),
                    new MandatoryFieldValidator(EntityFieldType.PASSWORD.toString(), redentials.password(),
                        () -> new PasswordValidator(EntityFieldType.PASSWORD.toString(), redentials.password(),
                            redentials.username(), errors),
                        errors)));
    }

}
