package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.StringFieldValidator;

/**
 * Validation class used to validate given user-name.
 */
public final class UsernameValidator extends CompositeValidator<String> {

    /**
     * Constructs the user-name validation class.
     *
     * @param name
     *            The name of the field. Must not be null.
     * @param username
     *            The user-name that will be validated. Must not be null.
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    public UsernameValidator(final String name, final String username, final List<ErrorInfo> errors) {
        super(name, username,
            List
                .of(new StringFieldValidator(name, username, Constants.USERNAME_MIN_LENGTH,
                    Constants.USERNAME_MAX_LENGTH, Constants.USERNAME_PATTERN, errors)));
    }
}
