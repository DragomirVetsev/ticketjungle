package com.dxc.ghp.ticketj.services.validation;

import java.util.ArrayList;
import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ValidationException;
import com.dxc.ghp.ticketj.services.validation.composite.MandatoryFieldValidator;

/**
 * Validation class that enforces validations.
 */
public final class ValidationEnforcer {

    private ValidationEnforcer() {
        // Utility class. No instances.
    }

    /**
     * Validation method which assures that given field is not {@code null} and then
     * enforces the validation process from the given {@code Validator}.
     *
     * @param fieldName
     *            The name of the value. Must not be null.
     * @param value
     *            The value that will be validated.
     * @param validationConstructor
     *            The constructor of the validation class. Must not be null.
     * @param <Value>
     *            The type of the value.
     * @throws ValidationException
     *             If any validation error occurs.
     */
    // @formatter:off
    @SuppressWarnings("nls")
    public static <Value> void verifyMandatoryField(final String fieldName, final Value value,
                                      final ValidatorConstructor<String,
                                                                  Value,
                                                                  List<ErrorInfo>,
                                                                  Validator> validationConstructor) {
     // @formatter:on
        assert fieldName != null : "The name of the value must not be null.";
        assert validationConstructor != null : "The constructor reference must not be null.";

        final List<ErrorInfo> errors = new ArrayList<>();
        final MandatoryFieldValidator mandatoryFieldValidator = new MandatoryFieldValidator(fieldName, value,
            () -> validationConstructor.apply(fieldName, value, errors), errors);
        mandatoryFieldValidator.validate();

        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }
}
