package com.dxc.ghp.ticketj.services.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.dxc.ghp.ticketj.dto.CustomerDTO;
import com.dxc.ghp.ticketj.dto.CustomerWithCredentialsDTO;
import com.dxc.ghp.ticketj.dto.OrderDTO;
import com.dxc.ghp.ticketj.dto.OrderWithTicketsDTO;
import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.dto.ReservedTicketDTO;
import com.dxc.ghp.ticketj.dto.ReviewDTO;
import com.dxc.ghp.ticketj.dto.SeatDTO;
import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.dto.TicketDTO;
import com.dxc.ghp.ticketj.dto.TicketForOrderDTO;
import com.dxc.ghp.ticketj.misc.exceptions.EntityExistsException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.types.Constants;
import com.dxc.ghp.ticketj.misc.types.DeliveryTypeEnum;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PaymentTypeEnum;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.PurchaseStatusEnum;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.persistence.entities.CartItem;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;
import com.dxc.ghp.ticketj.persistence.entities.Discount;
import com.dxc.ghp.ticketj.persistence.entities.Order;
import com.dxc.ghp.ticketj.persistence.entities.PaymentType;
import com.dxc.ghp.ticketj.persistence.entities.Performer;
import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;
import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;
import com.dxc.ghp.ticketj.persistence.entities.Review;
import com.dxc.ghp.ticketj.persistence.entities.Seat;
import com.dxc.ghp.ticketj.persistence.entities.SeatStatus;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.entities.Ticket;
import com.dxc.ghp.ticketj.persistence.repositories.CustomerRepository;
import com.dxc.ghp.ticketj.persistence.repositories.EventRepository;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.persistence.types.Phone;
import com.dxc.ghp.ticketj.services.CustomerService;
import com.dxc.ghp.ticketj.services.impl.utilities.Converter;
import com.dxc.ghp.ticketj.services.impl.utilities.EntityFinder;
import com.dxc.ghp.ticketj.services.impl.utilities.ServicesCommon;
import com.dxc.ghp.ticketj.services.validation.ValidationEnforcer;
import com.dxc.ghp.ticketj.services.validation.domain.MandatoryCustomerWithCredentialsDtoValidator;
import com.dxc.ghp.ticketj.services.validation.domain.OptionalCustomerWithCredentials;
import com.dxc.ghp.ticketj.services.validation.domain.OrderWithTicketsDtoValidator;
import com.dxc.ghp.ticketj.services.validation.domain.PerformanceIdValidator;
import com.dxc.ghp.ticketj.services.validation.domain.UsernameValidator;

/**
 * {@link CustomerService} implementation.
 */
public final class CustomerServiceImpl extends BaseService implements CustomerService {

    /**
     * Constructs CustomerServiceImpl. For contract
     * {@link BaseService#BaseService(TransactionHandler) base constructor}.
     */
    public CustomerServiceImpl(final TransactionHandler transactionHandler) {
        super(transactionHandler);
    }

    @Override
    public Optional<CustomerDTO> findCustomer(final String username) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.USERNAME.toString(), username, UsernameValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            return customerRepository.findCustomer(username);
        }).map(CustomerServiceImpl::customerToDTO);
    }

    @Override
    public List<TicketDTO> findTicketsOfCustomer(final String username) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.USERNAME.toString(), username, UsernameValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Customer customer = EntityFinder.findCustomerOrThrow(username, customerRepository);
            final List<Ticket> customerTickets = customer
                .getOrders()
                .stream()
                .flatMap(order -> order.getTickets().stream())
                .toList();

            return Converter.convertList(customerTickets, Converter::ticketToDTO);
        });
    }

    @Override
    public List<ReviewDTO> findReviewsOfCustomer(final String username) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.USERNAME.toString(), username, UsernameValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Customer foundCustomer = EntityFinder.findCustomerOrThrow(username, customerRepository);

            return Converter.convertList(foundCustomer.getReviews(), CustomerServiceImpl::reviewToDTO);
        });
    }

    @Override
    public List<SubEventDTO> findWishlistOfCustomer(final String username) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.USERNAME.toString(), username, UsernameValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Customer customer = EntityFinder.findCustomerOrThrow(username, customerRepository);

            return Converter.convertList(customer.getWishlist().stream().toList(), CustomerServiceImpl::wishlistToDTO);
        });
    }

    private static CustomerDTO customerToDTO(final Customer customer) {
        return new CustomerDTO(customer.getUsername(), customer.getPersonName(), customer.getEmail(),
            new PhoneDTO(customer.getPhone().code().getValue(), customer.getPhone().number()), customer.getAddress(),
            customer.getBirthday());
    }

    private static ReviewDTO reviewToDTO(final Review review) {
        final SubEvent subevent = review.getSubEvent();

        return new ReviewDTO(subevent.getName(), subevent.getEvent().getName(),
            subevent.getStage().getVenue().getName(), subevent.getStage().getName(), subevent.getStartTime(),
            review.getComment(), review.getScore());
    }

    private static SubEventDTO wishlistToDTO(final SubEvent event) {
        return new SubEventDTO(new PerformanceId(event.getName(), event.getEvent().getName()),
            new PerformanceDetails(
                event.getStage().getVenue().getName(), event.getStage().getName(), event.getStartTime()),
            event.getDescription(), event.getTicketPrice(),
            new UnrestrictedAddress(event.getStage().getVenue().getAddress().street(),
                event.getStage().getVenue().getAddress().city().getCityName(),
                event.getStage().getVenue().getAddress().postCode()),
            convertPerformersToString(event), event.getReviewScoreAndCount());
    }

    private static List<String> convertPerformersToString(final SubEvent foundSubEvent) {
        return Converter.convertList(foundSubEvent.getPerformers(), Performer::getName);
    }

    @SuppressWarnings("nls")
    @Override
    public OrderDTO createOrderWithTickets(final OrderWithTicketsDTO newOrderWithTicketsDTO, final String username) {
        // TODO newOrderWithTicketsDTO validation
        ValidationEnforcer
            .verifyMandatoryField(OrderWithTicketsDTO.class.getSimpleName(), newOrderWithTicketsDTO,
                                  OrderWithTicketsDtoValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);

            final String deliveryTypeName = newOrderWithTicketsDTO.deliveryType();
            final String paymentTypeName = newOrderWithTicketsDTO.paymentType();
            final Customer customer = EntityFinder.findCustomerOrThrow(username, customerRepository);
            final DeliveryType deliveryType = EntityFinder
                .findDeliveryTypeOrThrow(deliveryTypeName, referentialRepository);
            final PaymentType paymentType = EntityFinder.findPaymentTypeOrThrow(paymentTypeName, referentialRepository);
            final String pruchaseStatusName = purchaseStatusNameBasedOnPayment(paymentType);
            final PurchaseStatus purchaseStatus = EntityFinder
                .findPurchaseStatusOrThrow(pruchaseStatusName, referentialRepository);
            final List<TicketForOrderDTO> ticketsFromClient = newOrderWithTicketsDTO.ticketDTOs();
            final Instant date = Instant.now();

            final Order order = DeliveryTypeEnum.HOME_DELIVERY.name().equals(deliveryType.getName())
                ? customerRepository
                    .createOrderWithDeliveryAddress(purchaseStatus, customer, deliveryType, paymentType, date,
                                                    new UnrestrictedAddress(
                                                        newOrderWithTicketsDTO.deliveryAddress().street(),
                                                        newOrderWithTicketsDTO.deliveryAddress().city(),
                                                        newOrderWithTicketsDTO.deliveryAddress().postCode()))
                : customerRepository
                    .createOrderWithoutDeliveryAddress(purchaseStatus, customer, deliveryType, paymentType, date);
            final SeatStatus aquiredSeatStatus = EntityFinder.findSeatStatusOrThrow("ACQUIRED", referentialRepository);

            final List<Ticket> ticketsFromDatabase = new ArrayList<>();
            for (final TicketForOrderDTO ticketDto : ticketsFromClient) {
                boolean isInCart = false;
                for (final CartItem cartItem : customer.getCartItems()) {
                    if (ticketDTO_IsInCartItems(ticketDto, cartItem)) {
                        isInCart = true;
                        final Discount discount = EntityFinder
                            .findDiscountOrThrow(ticketDto.discount(), referentialRepository);

                        cartItem.getTicket().setDiscount(discount);
                        cartItem.getTicket().changeSeatStatus(aquiredSeatStatus);
                        cartItem.getTicket().setOrder(order);
                        ticketsFromDatabase.add(cartItem.getTicket());
                        customerRepository.removeCartIthem(cartItem);
                        break;
                    }
                }
                if (!isInCart) {
                    // TODO return cart item ID
                    throw new EntityNotFoundException(TicketForOrderDTO.class,
                        String
                            .format("Event: %s, Venue: %s, sector: %s, row: %d, seat: %d", ticketDto.subEvent(),
                                    ticketDto.performance().venue(), ticketDto.sector(), ticketDto.row(),
                                    ticketDto.seatNumber()));
                }
            }
            final List<TicketDTO> ticketDTOs = Converter.convertList(ticketsFromDatabase, Converter::ticketToDTO);
            return new OrderDTO(order.getPurchaseStatus().getName(), order.getCustomer().getUsername(),
                order.getCustomer().getPersonName(), order.getPaymentType().getName(), order.getCreatedDate(),
                order.getDeliveryType().getName(), order.getDeliveryAddress(), ticketDTOs);
        });
    }

    private static boolean ticketDTO_IsInCartItems(final TicketForOrderDTO ticketDto, final CartItem cartItem) {
        return cartItem.getTicket().getSubEvent().getEvent().getName().equals(ticketDto.event())
            && cartItem.getTicket().getSubEvent().getName().equals(ticketDto.subEvent())
            && cartItem
                .getTicket()
                .getSubEvent()
                .getStage()
                .getVenue()
                .getName()
                .equals(ticketDto.performance().venue())
            && cartItem.getTicket().getSubEvent().getStage().getName().equals(ticketDto.performance().stage())
            && cartItem.getTicket().getSeat().getRow().getSector().getName().equals(ticketDto.sector())
            && cartItem.getTicket().getSeat().getRow().getNumber() == ticketDto.row()
            && cartItem.getTicket().getSeat().getNumber() == ticketDto.seatNumber();
    }

    private static boolean reservedTicketDTO_IsInCartItems(final ReservedTicketDTO reservedTicketDTO,
                                                           final CartItem cartItem) {
        return cartItem.getTicket().getSubEvent().getEvent().getName().equals(reservedTicketDTO.event())
            && cartItem.getTicket().getSubEvent().getName().equals(reservedTicketDTO.subEvent())
            && cartItem
                .getTicket()
                .getSubEvent()
                .getStage()
                .getVenue()
                .getName()
                .equals(reservedTicketDTO.performance().venue())
            && cartItem.getTicket().getSubEvent().getStage().getName().equals(reservedTicketDTO.performance().stage())
            && cartItem.getTicket().getSeat().getRow().getSector().getName().equals(reservedTicketDTO.sector())
            && cartItem.getTicket().getSeat().getRow().getNumber() == reservedTicketDTO.row()
            && cartItem.getTicket().getSeat().getNumber() == reservedTicketDTO.seat().number();
    }

    @SuppressWarnings("nls")
    private static String purchaseStatusNameBasedOnPayment(final PaymentType paymentType) {
        if (PaymentTypeEnum.CARD.value().equals(paymentType.getName())) {
            return PurchaseStatusEnum.COMPLETED.name();
        }
        if (PaymentTypeEnum.BANK_TRANSFER.value().equals(paymentType.getName())) {
            return PurchaseStatusEnum.COMPLETED.name();
        }
        throw new IllegalArgumentException("Illegal payment type for customer order");
    }

    @Override
    public CustomerDTO registerCustomer(final CustomerWithCredentialsDTO newCustomerDTO) {
        ValidationEnforcer
            .verifyMandatoryField(CustomerWithCredentialsDTO.class.getSimpleName(), newCustomerDTO,
                                  MandatoryCustomerWithCredentialsDtoValidator::new);

        final Customer customer = trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);

            if (customerRepository.findCustomer(newCustomerDTO.credentials().username()).isPresent()) {
                throw new EntityExistsException(Customer.class, newCustomerDTO.credentials().username(), null);
            }

            return criateOrUpdateCustomer(newCustomerDTO, customerRepository, referentialRepository,
                                          newCustomerDTO.credentials().username());
        }, Customer.class, newCustomerDTO.credentials().username());
        return customerToDTO(customer);
    }

    @Override
    public CustomerDTO patchCustomer(final String username, final CustomerWithCredentialsDTO newCustomerDTO) {
        ValidationEnforcer
            .verifyMandatoryField(CustomerWithCredentialsDTO.class.getSimpleName(), newCustomerDTO,
                                  OptionalCustomerWithCredentials::new);

        final Customer customer = trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final ReferentialRepository referentialRepository = provider.find(ReferentialRepository.class);

            return criateOrUpdateCustomer(newCustomerDTO, customerRepository, referentialRepository, username);
        }, Customer.class, username);

        return customerToDTO(customer);
    }

    /**
     * Method is used for updating user data and is used both by the put and patch
     * methods.
     *
     * @param newCustomerDTO
     *            The customer for with the update or create will be performed.
     * @param customerRepository
     *            The repository needed for the customer.
     * @param referentialRepository
     *            The repository required for updating phone number.
     * @param phoneCode
     *            The phone code that was retried from the repository.
     * @return Either a updated customer or a new one.
     */

    private static Customer criateOrUpdateCustomer(final CustomerWithCredentialsDTO newCustomerDTO,
                                                   final CustomerRepository customerRepository,
                                                   final ReferentialRepository referentialRepository,
                                                   final String username) {
        final Optional<Customer> foundCustomer = customerRepository.findCustomer(username);

        if (foundCustomer.isPresent()) {
            return foundCustomer.map((final Customer foundCustomer1) -> {
                if (ServicesCommon.requiresModifying(newCustomerDTO.address())) {
                    modifyCustomerAddress(newCustomerDTO.address(), foundCustomer1);
                }
                if (ServicesCommon.requiresModifying(newCustomerDTO.phoneNumber())) {
                    modifyCustomerPhone(newCustomerDTO.phoneNumber(), referentialRepository, foundCustomer1);
                }
                if (ServicesCommon.requiresModifying(newCustomerDTO.credentials())) {
                    modifyCustomerPassword(newCustomerDTO.credentials().password(), foundCustomer1);
                }
                return foundCustomer1;
            }).orElseThrow(() -> new EntityNotFoundException(Customer.class, username));
        }

        final PhoneCode phoneCode = findPhoneCodeOrThrow(newCustomerDTO.phoneNumber().code(), referentialRepository);
        return createCustomer(newCustomerDTO, customerRepository, phoneCode);
    }

    private static void modifyCustomerPassword(final String password, final Customer foundCustomer) {
        foundCustomer.changePassword(password);
    }

    private static void modifyCustomerPhone(final PhoneDTO phone, final ReferentialRepository referentialRepository,
                                            final Customer foundCustomer) {
        final Phone foundPhone = foundCustomer.getPhone();

        final PhoneCode phoneCode = ServicesCommon.notNull(phone.code())
            ? findPhoneCodeOrThrow(phone.code(), referentialRepository)
            : foundPhone.code();
        final String number = ServicesCommon.notNull(phone.number()) ? phone.number() : foundPhone.number();
        foundCustomer.changePhone(new Phone(phoneCode, number));
    }

    private static void modifyCustomerAddress(final UnrestrictedAddress address, final Customer foundCustomer) {
        final UnrestrictedAddress customerAddress = foundCustomer.getAddress();

        final String city = ServicesCommon.notNull(address.street()) ? address.city() : customerAddress.city();
        final String street = ServicesCommon.notNull(address.street()) ? address.street() : customerAddress.street();
        final String postCode = ServicesCommon.notNull(address.postCode()) ? address.postCode()
            : customerAddress.postCode();
        foundCustomer.changeAddress(new UnrestrictedAddress(street, city, postCode));
    }

    @SuppressWarnings("nls")
    private static Customer createCustomer(final CustomerWithCredentialsDTO newCustomerDTO,
                                           final CustomerRepository customerRepository, final PhoneCode phoneCode) {
        assert newCustomerDTO != null : "New customer dto must not be null!";
        assert phoneCode != null : "phoneCode fro new customer must not be null!";
        assert customerRepository != null : "Customer repositoryy ust not be null!";

        return customerRepository
            .createCustomer(newCustomerDTO.credentials(), newCustomerDTO.personName(), newCustomerDTO.email(),
                            new Phone(phoneCode, newCustomerDTO.phoneNumber().number()), newCustomerDTO.address(),
                            newCustomerDTO.birthday());
    }

    private static PhoneCode findPhoneCodeOrThrow(final String phoneCode,
                                                  final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findPhoneCode(phoneCode)
            .orElseThrow(() -> new EntityNotFoundException(PhoneCode.class, phoneCode));
    }

    @Override
    public List<ReservedTicketDTO> findCartItemsOfCustomer(final String username) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.USERNAME.toString(), username, UsernameValidator::new);

        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Customer customer = EntityFinder.findCustomerOrThrow(username, customerRepository);
            final List<CartItem> customerCartItems = customer.getCartItems();

            return Converter.convertList(customerCartItems, CustomerServiceImpl::reservedTicketToDTO);
        });
    }

    @SuppressWarnings("nls")
    @Override
    public List<ReservedTicketDTO> removeCartItemOfCustomer(final String username,
                                                            final List<ReservedTicketDTO> reservedTickets) {
        trxHandler
            .executeInTransaction(

                                  (final ServiceProvider provider) -> {

                                      final CustomerRepository customerRepository = provider
                                          .find(CustomerRepository.class);
                                      final ReferentialRepository referentialRepository = provider
                                          .find(ReferentialRepository.class);

                                      final Customer customer = EntityFinder
                                          .findCustomerOrThrow(username, customerRepository);
                                      final SeatStatus availableStatus = EntityFinder
                                          .findSeatStatusOrThrow(Constants.AVAILABLE_STATUS_TICKET,
                                                                 referentialRepository);

                                      for (final ReservedTicketDTO reservedTicketDTO : reservedTickets) {
                                          boolean isInCart = false;

                                          for (final CartItem cartItemToDelete : customer.getCartItems()) {
                                              if (reservedTicketDTO_IsInCartItems(reservedTicketDTO,
                                                                                  cartItemToDelete)) {
                                                  isInCart = true;
                                                  cartItemToDelete.getTicket().changeSeatStatus(availableStatus);
                                                  customerRepository.removeCartIthem(cartItemToDelete);
                                                  break;
                                              }
                                          }
                                          if (!isInCart) {
                                              throw new EntityNotFoundException(CartItem.class,
                                                  "This cart item isn't present!");
                                          }

                                      }

                                  });
        return findCartItemsOfCustomer(username);

    }

    private static ReservedTicketDTO reservedTicketToDTO(final CartItem cartItem) {
        final Ticket ticket = cartItem.getTicket();
        final SubEvent subEvent = ticket.getSubEvent();
        final Seat seat = ticket.getSeat();

        return new ReservedTicketDTO(subEvent.getEvent().getName(), subEvent.getName(),
            new PerformanceDetails(subEvent.getStage().getVenue().getName(), subEvent.getStage().getName(),
                subEvent.getStartTime()),
            seat.getRow().getSector().getName(), seat.getRow().getNumber(),
            new SeatDTO(seat.getNumber(), ticket.getSeatStatus().getStatus()), ticket.getTicketPrice());

    }

    @Override
    public boolean addSubEventToWishList(final String username, final PerformanceId performanceId) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.USERNAME.toString(), username, UsernameValidator::new);
        ValidationEnforcer
            .verifyMandatoryField(EntityFieldType.SUB_EVENT.toString(), performanceId, PerformanceIdValidator::new);
        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Customer foundCustomer = EntityFinder.findCustomerOrThrow(username, customerRepository);
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final SubEvent subEvent = EntityFinder.findSubEventOrThrow(performanceId, eventRepository);

            if (!foundCustomer.getWishlist().contains(subEvent)) {
                return Boolean.valueOf(foundCustomer.addToWishList(subEvent));
            }
            return Boolean.FALSE;
        }).booleanValue();
    }

    @Override
    public boolean removeSubEventFromWishList(final String username, final PerformanceId performanceId) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.USERNAME.toString(), username, UsernameValidator::new);
        ValidationEnforcer
            .verifyMandatoryField(EntityFieldType.SUB_EVENT.toString(), performanceId, PerformanceIdValidator::new);
        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Customer foundCustomer = EntityFinder.findCustomerOrThrow(username, customerRepository);
            final EventRepository eventRepository = provider.find(EventRepository.class);
            final SubEvent subEvent = EntityFinder.findSubEventOrThrow(performanceId, eventRepository);

            if (foundCustomer.getWishlist().contains(subEvent)) {
                return Boolean.valueOf(foundCustomer.removeFromWishlist(subEvent));
            }
            return Boolean.FALSE;

        }).booleanValue();
    }

    @Override
    public boolean removeSelectedSubEventsFromWishlist(final String username, final List<PerformanceId> subEventIds) {
        ValidationEnforcer.verifyMandatoryField(EntityFieldType.USERNAME.toString(), username, UsernameValidator::new);
        return trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final CustomerRepository customerRepository = provider.find(CustomerRepository.class);
            final Customer foundCustomer = EntityFinder.findCustomerOrThrow(username, customerRepository);
            final EventRepository eventRepository = provider.find(EventRepository.class);

            return Boolean.valueOf(foundCustomer.removeFromWishlist(getSubEvents(subEventIds, eventRepository)));
        }).booleanValue();
    }

    private static Set<SubEvent> getSubEvents(final Collection<PerformanceId> subEvents,
                                              final EventRepository eventRepository) {
        final Set<SubEvent> foundSubEvents = new HashSet<>();
        subEvents
            .stream()
            .forEach(event -> foundSubEvents.add(EntityFinder.findSubEventOrThrow(event, eventRepository)));
        return foundSubEvents;
    }

}
