package com.dxc.ghp.ticketj.services.impl.utilities;

import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.persistence.entities.City;
import com.dxc.ghp.ticketj.persistence.entities.Customer;
import com.dxc.ghp.ticketj.persistence.entities.DeliveryType;
import com.dxc.ghp.ticketj.persistence.entities.Discount;
import com.dxc.ghp.ticketj.persistence.entities.Event;
import com.dxc.ghp.ticketj.persistence.entities.Genre;
import com.dxc.ghp.ticketj.persistence.entities.OfficeWorker;
import com.dxc.ghp.ticketj.persistence.entities.PaymentType;
import com.dxc.ghp.ticketj.persistence.entities.PhoneCode;
import com.dxc.ghp.ticketj.persistence.entities.PurchaseStatus;
import com.dxc.ghp.ticketj.persistence.entities.SeatStatus;
import com.dxc.ghp.ticketj.persistence.entities.Stage;
import com.dxc.ghp.ticketj.persistence.entities.SubEvent;
import com.dxc.ghp.ticketj.persistence.repositories.CustomerRepository;
import com.dxc.ghp.ticketj.persistence.repositories.EventRepository;
import com.dxc.ghp.ticketj.persistence.repositories.OfficeWorkerRepository;
import com.dxc.ghp.ticketj.persistence.repositories.ReferentialRepository;

/**
 * Contains all methods for finding specific entities.
 */
public final class EntityFinder {

    private EntityFinder() {
        // Utility class. No instances.
    }

    /**
     * Finds {@link City}.
     *
     * @param city
     *            The city unique identifier used to locate the entity. Must not be
     *            null.
     * @param referentialRepository
     *            The repository containing the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static City findCityOrThrow(final String city, final ReferentialRepository referentialRepository) {
        return referentialRepository.findCity(city).orElseThrow(() -> new EntityNotFoundException(City.class, city));
    }

    /**
     * Finds {@link OfficeWorker}.
     *
     * @param username
     *            The worker unique identifier used to locate the entity. Must not
     *            be null.
     * @param officeWorkerRepository
     *            The repository containing the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static OfficeWorker findWorkerOrThrow(final String username,
                                                 final OfficeWorkerRepository officeWorkerRepository) {
        return officeWorkerRepository
            .findWorker(username)
            .orElseThrow(() -> new EntityNotFoundException(OfficeWorker.class, username));
    }

    /**
     * Finds {@link PhoneCode}.
     *
     * @param phoneCode
     *            The phone code unique identifier used to locate the entity. Must
     *            not be null.
     * @param referentialRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static PhoneCode findPhoneCodeOrThrow(final String phoneCode,
                                                 final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findPhoneCode(phoneCode)
            .orElseThrow(() -> new EntityNotFoundException(PhoneCode.class, phoneCode));
    }

    /**
     * Finds {@link Genre}.
     *
     * @param genre
     *            The genre unique identifier used to locate the entity. Must not be
     *            null.
     * @param referentialRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static Genre findGenreOrThrow(final String genre, final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findGenre(genre)
            .orElseThrow(() -> new EntityNotFoundException(Genre.class, genre));
    }

    /**
     * Finds {@link Customer}.
     *
     * @param username
     *            The username unique identifier used to locate the entity. Must not
     *            be null.
     * @param customerRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static Customer findCustomerOrThrow(final String username, final CustomerRepository customerRepository) {
        return customerRepository
            .findCustomer(username)
            .orElseThrow(() -> new EntityNotFoundException(Customer.class, username));
    }

    /**
     * Finds {@link Event}.
     *
     * @param event
     *            is the name of the event we are looking for. Must not be null.
     * @param referentialRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static Event findEventOrThrow(final String event, final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findEvent(event)
            .orElseThrow(() -> new EntityNotFoundException(Event.class, event));
    }

    /**
     * Finds {@link Stage}.
     *
     * @param stage
     *            is the name if the stage we are looking for.
     * @param venue
     *            is the name of the venue containing the stage we are looking for.
     * @param referentialRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    @SuppressWarnings("nls")
    public static Stage findStageOrThrow(final String stage, final String venue,
                                         final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findStage(stage, venue)
            .orElseThrow(() -> new EntityNotFoundException(SubEvent.class,
                String.format("Stage '%s' with Venue '%s' not found.", stage, venue)));
    }

    /**
     * Finds {@link DeliveryType}.
     *
     * @param deliveryType
     *            The DeliveryType unique identifier used to locate the entity. Must
     *            not be null.
     * @param referentialRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static DeliveryType findDeliveryTypeOrThrow(final String deliveryType,
                                                       final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findDeliveryType(deliveryType)
            .orElseThrow(() -> new EntityNotFoundException(DeliveryType.class, deliveryType));
    }

    /**
     * Finds {@link PaymentType}.
     *
     * @param paymentType
     *            The PaymentType unique identifier used to locate the entity. Must
     *            not be null.
     * @param referentialRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static PaymentType findPaymentTypeOrThrow(final String paymentType,
                                                     final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findPaymentType(paymentType)
            .orElseThrow(() -> new EntityNotFoundException(PaymentType.class, paymentType));
    }

    /**
     * Finds {@link SeatStatus}.
     *
     * @param seatStatus
     *            The SeatStatus unique identifier used to locate the entity. Must
     *            not be null.
     * @param referentialRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static SeatStatus findSeatStatusOrThrow(final String seatStatus,
                                                   final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findSeatStatus(seatStatus)
            .orElseThrow(() -> new EntityNotFoundException(SeatStatus.class, seatStatus));
    }

    /**
     * Finds {@link PurchaseStatus}.
     *
     * @param purchaseStatus
     *            The PurchaseStatus unique identifier used to locate the entity.
     *            Must not be null.
     * @param referentialRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static PurchaseStatus findPurchaseStatusOrThrow(final String purchaseStatus,
                                                           final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findPurchaseStatus(purchaseStatus)
            .orElseThrow(() -> new EntityNotFoundException(PurchaseStatus.class, purchaseStatus));
    }

    /**
     * Finds {@link SubEvent}.
     *
     * @param performanceId
     *            The SubEvent's unique identifier used to locate the entity. Must
     *            not be null.
     * @param eventRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The {@code SubEvent} entity.
     * @throws EntityNotFoundException
     *             If the searched {@code SubEvent} doesn't exist.
     */
    public static SubEvent findSubEventOrThrow(final PerformanceId performanceId,
                                               final EventRepository eventRepository) {

        return eventRepository
            .findSubEvent(performanceId)
            .orElseThrow(() -> new EntityNotFoundException(SubEvent.class, performanceId));
    }

    /**
     * Finds {@link Discount}.
     *
     * @param discountName
     *            The Discount unique identifier used to locate the entity. Must not
     *            be null.
     * @param referentialRepository
     *            The repository used to find the entity in question. Must not be
     *            null.
     * @return The persistence entity. Cannot be null.
     * @throws EntityNotFoundException
     *             when the entity is not found.
     */
    public static Discount findDiscountOrThrow(final String discountName,
                                               final ReferentialRepository referentialRepository) {
        return referentialRepository
            .findDiscount(discountName)
            .orElseThrow(() -> new EntityNotFoundException(Discount.class, discountName));
    }
}
