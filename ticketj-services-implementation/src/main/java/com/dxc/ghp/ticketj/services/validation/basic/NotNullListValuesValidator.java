package com.dxc.ghp.ticketj.services.validation.basic;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.BasicValidator;
import com.dxc.ghp.ticketj.services.validation.Validator;
import com.dxc.ghp.ticketj.services.validation.ValidatorConstructor;

/**
 * Validation class that validates {@code List} of objects for not null values.
 *
 * @param <ValueType>
 *            The type of the value.
 */
public final class NotNullListValuesValidator<ValueType> extends BasicValidator<List<ValueType>> {

    private final ValidatorConstructor<String, ValueType, List<ErrorInfo>, Validator> validatorConstructor;

    /**
     * Constructs validation class for lists.
     *
     * @param fieldName
     *            The name of the field list.
     * @param values
     *            The list of values that will be validated.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     * @param validatorConstructor
     *            The validator constructor that will be used to construct
     *            validation object for every value of the list.
     */
    // @formatter:off
    public NotNullListValuesValidator(final String fieldName, final List<ValueType> values,
                                      final ValidatorConstructor<String,
                                                                  ValueType,
                                                                  List<ErrorInfo>,
                                                                  Validator> validatorConstructor,
                                      final List<ErrorInfo> errors) {
        super(fieldName, values, errors);
        this.validatorConstructor = validatorConstructor;
    }
    // @formatter:on

    @Override
    public void validate() {
        final boolean hasNull = value.stream().anyMatch(v -> v == null);
        if (hasNull) {
            errors.add(new ValidationError<>(fieldName, value, ValidationErrorType.HAS_NULL_VALUES.toString()));
        } else {
            value.forEach(v -> validatorConstructor.apply(fieldName, v, errors).validate());
        }
    }

}
