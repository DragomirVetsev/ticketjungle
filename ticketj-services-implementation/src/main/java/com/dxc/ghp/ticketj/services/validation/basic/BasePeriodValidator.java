package com.dxc.ghp.ticketj.services.validation.basic;

import java.time.temporal.Temporal;
import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.BiValidationError;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.BasicValidator;

/**
 * Validation class for checking if {@link Period} values are in ascending
 * order. Works properly only if both Values are PRESENT.
 *
 * @param <Time>
 *            The type of Temporal objects compared.
 */
public abstract class BasePeriodValidator<Time extends Temporal> extends BasicValidator<Period<Time>> {

    /**
     * @param period
     *            The period that will be validated.
     * @param errors
     *            The list with errors that will collect the errors during
     *            validation.
     */
    protected BasePeriodValidator(final Period<Time> period, final List<ErrorInfo> errors) {
        super(EntityFieldType.TIME_PERIOD.toString(), period, errors);

    }

    @Override
    public final void validate() {
        if (value.startTime() != null && value.endTime() != null && startTimeIsAfterEndTime()) {
            errors
                .add(new BiValidationError<>(EntityFieldType.START_TIME.toString(), value.startTime(),
                    EntityFieldType.END_TIME.toString(), value.endTime(),
                    ValidationErrorType.INCORRECT_DATE_TIME_ORDER.toString()));
        }
    }

    /**
     * Confirms that the values are not in ascending order (startTime is after
     * endTime). If returns true validation fails and vice versa.
     *
     * @return True if startTime is after endTime and false if equal or before
     *         endTime.
     */
    protected abstract boolean startTimeIsAfterEndTime();
}
