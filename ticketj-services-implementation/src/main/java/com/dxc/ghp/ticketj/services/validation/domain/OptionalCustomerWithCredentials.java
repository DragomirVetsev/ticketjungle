package com.dxc.ghp.ticketj.services.validation.domain;

import java.util.List;

import com.dxc.ghp.ticketj.dto.CustomerWithCredentialsDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.services.validation.CompositeValidator;
import com.dxc.ghp.ticketj.services.validation.composite.EmailValidator;
import com.dxc.ghp.ticketj.services.validation.composite.OptionalFieldValidator;

/**
 * Validation class for {@link CustomerWithCredentialsDTO} with optional fields
 * for every component of the record.
 */
public final class OptionalCustomerWithCredentials extends CompositeValidator<CustomerWithCredentialsDTO> {

    /**
     * Constructs a validation class for {@code CustomerWithCredentialsDTO}.
     *
     * @param fieldName
     *            The name of the field. Must not be null.
     * @param userInfo
     *            The {@link CustomerWithCredentialsDTO} to be validated. Must not
     *            be null.
     * @param errors
     *            The list that will be used to collect the errors after
     *            validations. Must not be null.
     */
    public OptionalCustomerWithCredentials(final String fieldName, final CustomerWithCredentialsDTO userInfo,
                                           final List<ErrorInfo> errors) {
        super(fieldName, userInfo, List
            .of(new OptionalFieldValidator<>(userInfo.credentials(),
                () -> new CredentialsValidator(EntityFieldType.CREDENTIALS.toString(), userInfo.credentials(), errors)),
                new OptionalFieldValidator<>(userInfo.personName(),
                    () -> new PersonNameValidator(EntityFieldType.PERSON_NAME.toString(), userInfo.personName(),
                        errors)),
                new OptionalFieldValidator<>(userInfo.email(),
                    () -> new EmailValidator(EntityFieldType.EMAIL.toString(), userInfo.email(), errors)),
                new OptionalFieldValidator<>(userInfo.phoneNumber(),
                    () -> new MandatoryPhoneValidator(EntityFieldType.PHONE.toString(), userInfo.phoneNumber(),
                        errors)),
                new OptionalFieldValidator<>(userInfo.address(),
                    () -> new MandatoryAddressValidator(EntityFieldType.ADDRESS.toString(), userInfo.address(),
                        errors))));
    }
}
