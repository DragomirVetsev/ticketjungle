package com.dxc.ghp.ticketj.services.impl;

import java.util.List;

import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.dto.UserDTO;
import com.dxc.ghp.ticketj.dto.UserInfoDTO;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;
import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.persistence.entities.User;
import com.dxc.ghp.ticketj.persistence.repositories.UserRepository;
import com.dxc.ghp.ticketj.persistence.transaction.handlers.TransactionHandler;
import com.dxc.ghp.ticketj.services.UserService;
import com.dxc.ghp.ticketj.services.impl.utilities.Converter;

/**
 * {@link UserService} production implementation.
 */
public final class UserServiceImpl extends BaseService implements UserService {

    /**
     * Constructs a production implementation of {@link UserService}. For contract
     * see {@link BaseService#BaseService(TransactionHandler) base constructor}.
     */
    public UserServiceImpl(final TransactionHandler transactionHandler) {
        super(transactionHandler);
    }

    @Override
    public List<UserDTO> findAllUsers() {
        final List<User> users = trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final UserRepository userRepositrory = provider.find(UserRepository.class);

            return userRepositrory.findAllUsers();
        });

        return Converter
            .convertList(users,
                         user -> new UserDTO(user.getUsername(), user.getPersonName(), user.getEmail(),
                             new PhoneDTO(user.getPhone().code().getValue(), user.getPhone().number()),
                             user.getAddress(), user.getRole()));
    }

    @SuppressWarnings("nls")
    @Override
    public UserInfoDTO findUserByCredentials(final Credentials credentials) {
        final String invalidCredentials = "Wrong username or password!";
        final User user = trxHandler.executeInTransaction((final ServiceProvider provider) -> {
            final UserRepository userRepository = provider.find(UserRepository.class);
            return userRepository.findUserByCredentials(credentials);
        }, User.class, invalidCredentials);

        return new UserInfoDTO(user.getUsername(), user.getPersonName(), user.getRole());
    }
}
