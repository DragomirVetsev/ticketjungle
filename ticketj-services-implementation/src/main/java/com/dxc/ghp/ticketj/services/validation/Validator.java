package com.dxc.ghp.ticketj.services.validation;

/**
 * Validation interface for validation classes.
 */
public interface Validator {

    /**
     * The method used for validation.
     */
    void validate();
}
