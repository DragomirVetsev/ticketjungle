package com.dxc.ghp.ticketj.services.validation.basic;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;
import com.dxc.ghp.ticketj.services.validation.BasicValidator;

/**
 *
 */
public final class DoubleDecimalPointValidator extends BasicValidator<Double> {

    private final int allowedDecimalPlaces;

    /**
     * @param fieldName
     *            The name of the value.
     * @param number
     *            The {@code double} value to be validated.
     * @param allowedNumberDecimals
     *            the number of allowed decimal numbers after decimal point.
     *            Constraints: {@code allowedNumberDecimals >= 0}.
     * @param errors
     *            The list of errors that will be used to collect the errors. Must
     *            not be null.
     */
    @SuppressWarnings("nls")
    public DoubleDecimalPointValidator(final String fieldName, final double number, final int allowedNumberDecimals,
                                       final List<ErrorInfo> errors) {
        super(fieldName, Double.valueOf(number), errors);
        assert allowedNumberDecimals >= 0 : "The number of decimals must not be negative.";
        this.allowedDecimalPlaces = allowedNumberDecimals;
    }

    @Override
    public void validate() {
        final String doubleText = Double.toString(value.doubleValue());
        final int indexOfPoint = doubleText.indexOf('.');
        final int actualDecimalPlaces = doubleText.length() - indexOfPoint - 1;
        if (actualDecimalPlaces > allowedDecimalPlaces) {
            errors
                .add(new ValidationError<>(fieldName, value,
                    ValidationErrorType.INVALID_NUMBER_OF_DECIMALS.toString()));
        }

    }
}
