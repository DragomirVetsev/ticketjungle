package com.dxc.ghp.ticketj.integration.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.EventDTO;
import com.dxc.ghp.ticketj.integration.tests.config.RestAssuredConfiguration;
import com.dxc.ghp.ticketj.integration.tests.requests.HTTPRequest;
import com.dxc.ghp.ticketj.integration.tests.utilities.Jwt;
import com.dxc.ghp.ticketj.integration.tests.utilities.SubEventDateCreator;
import com.dxc.ghp.ticketj.misc.exceptions.Error;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;

import io.restassured.common.mapper.TypeRef;

/**
 * Integration tests for Event Restful find by criteria service.
 */
@SuppressWarnings({
    "nls", "static-method"
})
public final class EventRestfulFindByCriteriaServiceIT extends RestAssuredConfiguration {

    private static final String SEARCH_EVENT_PATH = "/events/search";
    private final static String PAST_TIME_PARAM_MAX = SubEventDateCreator.subEventDateCreator(-365, 0, 0).toString();
    private static final UserRole ROLE_CUSTOMER = UserRole.CUSTOMER;
    private static final String VALID_JWT_FOR_CUSTOMER = Jwt.createToken("Test Username", ROLE_CUSTOMER);

    /**
     * Test, when send GET HTTP Request to "/events/criteria" returns empty array
     * because criteria is not suited the existing events with status code 200.
     *
     * @param findByCriteriaURL
     *            The URL that is filled with QueryParam`s for the search criteria.
     * @param expected
     *            The event witch the parameterized query will return after
     *            searching by criteria.
     */

    @ParameterizedTest
    @MethodSource("searchCriteriaDtoData")
    void testSearchByCriteria(final String findByCriteriaURL, final EventDTO[] expected) {
        final EventDTO[] actual = HTTPRequest
            .get(findByCriteriaURL, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, EventDTO[].class);
        Assertions.assertArrayEquals(expected, actual);
    }

    private static Stream<Arguments> searchCriteriaDtoData() {
        final String futureOneYearTimeParam = SubEventDateCreator.subEventDateCreator(365, 0, 0).toString();
        final String pastTimeParamMin = SubEventDateCreator.subEventDateCreator(-1, 0, 0).toString();
        final String pastOneMonthTimeParam = SubEventDateCreator.subEventDateCreator(-29, 0, 0).toString();
        final String todayAtMidNight = Instant.now().truncatedTo(ChronoUnit.DAYS).toString();

        final String findByWrongCriteriaURL = SEARCH_EVENT_PATH + "?from=" + futureOneYearTimeParam + "&to="
            + futureOneYearTimeParam + "&city=Plovdiv&genre=Pop";
        final String findOneByCriteriaURL = SEARCH_EVENT_PATH + "?from=" + PAST_TIME_PARAM_MAX + "&to="
            + futureOneYearTimeParam + "&city=Sofia&genre=Pop";
        final String findManyByCriteriaURL = SEARCH_EVENT_PATH + "?from=2023-07-19T19:30:00Z&to=" + pastTimeParamMin
            + "&city=Sofia";
        final String findManyByCriteriaWithNoCityAndGenreURL = SEARCH_EVENT_PATH + "?from=" + pastOneMonthTimeParam
            + "&to=" + todayAtMidNight;
        final String findManyByCriteriaWithCityAndGenre = SEARCH_EVENT_PATH + "?city=Sofia&genre=Pop";
        final String findManyByCriteriaJustWithCity = SEARCH_EVENT_PATH + "?city=Burgas";
        final String findManyByCriteriaWithCityAndToTime = SEARCH_EVENT_PATH + "?to=" + futureOneYearTimeParam
            + "&city=Burgas";
        final String findManyByCriteriaWithGenreAndToTime = SEARCH_EVENT_PATH + "?to=" + PAST_TIME_PARAM_MAX
            + "&genre=Pop";
        final String findManyByCriteriaWithGenreAndFromTime = SEARCH_EVENT_PATH + "?from=" + PAST_TIME_PARAM_MAX
            + "&genre=Pop";
        final String findManyByCriteriaWithCityAndFromTime = SEARCH_EVENT_PATH + "?from=" + PAST_TIME_PARAM_MAX
            + "&city=Varna";

        final EventDTO[] expectedEmpty = new EventDTO[0];
        final EventDTO[] expectAll = {
            new EventDTO("Burgas Fest", "Only the best now in Burgas", "Pop"),
            new EventDTO("Cool den - tour", "Cool den - around Bulgaria", "Rock"),
            new EventDTO("GRAFA TOUR", "The new tour of Grafa 2023", "Pop"),
            new EventDTO("Leep Year Event", "Leap year event 2020", "Pop"),
            new EventDTO("Mercury World Tour",
                "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.", "Pop"),
            new EventDTO("ROLLING STONES TOUR", "WORLD TOUR SATISFACTION", "Rock"),
            new EventDTO("СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ", "Юбилейни концерти на група Сигнал за 45-та годишнина",
                "Rock"),
            new EventDTO("Театрални вечери във Варна", "Заповядайте на забравими театрални вечери в град Варна",
                "Theater"),
            new EventDTO("Турнето на Лили Иванова", "Коледния подарък на Лили за публиката и!", "Pop"),
            new EventDTO("Хамлет в Народния Театър", "Да бъдем или да не бъдем, това се пита", "Tragedy")
        };
        final EventDTO[] expectOneSpecificEvent = {
            new EventDTO("GRAFA TOUR", "The new tour of Grafa 2023", "Pop"),
            new EventDTO("Mercury World Tour",
                "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.", "Pop"),
            new EventDTO("Турнето на Лили Иванова", "Коледния подарък на Лили за публиката и!", "Pop")
        };
        final EventDTO[] expectEventWithoutGenre = {
            new EventDTO("GRAFA TOUR", "The new tour of Grafa 2023", "Pop"),
            new EventDTO("Mercury World Tour",
                "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.", "Pop"),
            new EventDTO("ROLLING STONES TOUR", "WORLD TOUR SATISFACTION", "Rock"),
            new EventDTO("Хамлет в Народния Театър", "Да бъдем или да не бъдем, това се пита", "Tragedy")
        };
        final EventDTO[] expectInOneWeek = {
            new EventDTO("Burgas Fest", "Only the best now in Burgas", "Pop"),
            new EventDTO("GRAFA TOUR", "The new tour of Grafa 2023", "Pop"),
            new EventDTO("Mercury World Tour",
                "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.", "Pop"),
            new EventDTO("ROLLING STONES TOUR", "WORLD TOUR SATISFACTION", "Rock"),
            new EventDTO("Хамлет в Народния Театър", "Да бъдем или да не бъдем, това се пита", "Tragedy")
        };
        final EventDTO[] expectedFindManyByCriteriaWithCityAndGenre = {
            new EventDTO("GRAFA TOUR", "The new tour of Grafa 2023", "Pop"),
            new EventDTO("Leep Year Event", "Leap year event 2020", "Pop"),
            new EventDTO("Mercury World Tour",
                "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.", "Pop"),
            new EventDTO("Турнето на Лили Иванова", "Коледния подарък на Лили за публиката и!", "Pop")
        };
        final EventDTO[] expectFindManyByCriteriaJustWithCity = {
            new EventDTO("Burgas Fest", "Only the best now in Burgas", "Pop"),
            new EventDTO("GRAFA TOUR", "The new tour of Grafa 2023", "Pop"),
            new EventDTO("Mercury World Tour",
                "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.", "Pop"),
            new EventDTO("СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ", "Юбилейни концерти на група Сигнал за 45-та годишнина",
                "Rock"),
        };
        final EventDTO[] expectFindManyByCriteriaWithCityAndToTime = {
            new EventDTO("Burgas Fest", "Only the best now in Burgas", "Pop"),
            new EventDTO("GRAFA TOUR", "The new tour of Grafa 2023", "Pop"),
            new EventDTO("Mercury World Tour",
                "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.", "Pop"),
            new EventDTO("СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ", "Юбилейни концерти на група Сигнал за 45-та годишнина", "Rock")
        };
        final EventDTO[] expectFindManyByCriteriaWithGenreAndToTime = {
            new EventDTO("Leep Year Event", "Leap year event 2020", "Pop")
        };
        final EventDTO[] expectedFindManyByCriteriaWithGenreAndFromTime = {
            new EventDTO("Burgas Fest", "Only the best now in Burgas", "Pop"),
            new EventDTO("GRAFA TOUR", "The new tour of Grafa 2023", "Pop"),
            new EventDTO("Mercury World Tour",
                "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.", "Pop"),
            new EventDTO("Турнето на Лили Иванова", "Коледния подарък на Лили за публиката и!", "Pop")
        };
        final EventDTO[] expectedFindManyByCriteriaWithCityAndFromTime = {
            new EventDTO("Cool den - tour", "Cool den - around Bulgaria", "Rock"), new EventDTO(
                "Театрални вечери във Варна", "Заповядайте на забравими театрални вечери в град Варна", "Theater"),
        };
        return Stream
            .of(arguments(findByWrongCriteriaURL, expectedEmpty), arguments(SEARCH_EVENT_PATH, expectAll),
                arguments(findOneByCriteriaURL, expectOneSpecificEvent),
                arguments(findManyByCriteriaURL, expectEventWithoutGenre),
                arguments(findManyByCriteriaWithNoCityAndGenreURL, expectInOneWeek),
                arguments(findManyByCriteriaWithCityAndGenre, expectedFindManyByCriteriaWithCityAndGenre),
                arguments(findManyByCriteriaJustWithCity, expectFindManyByCriteriaJustWithCity),
                arguments(findManyByCriteriaWithCityAndToTime, expectFindManyByCriteriaWithCityAndToTime),
                arguments(findManyByCriteriaWithGenreAndToTime, expectFindManyByCriteriaWithGenreAndToTime),
                arguments(findManyByCriteriaWithGenreAndFromTime, expectedFindManyByCriteriaWithGenreAndFromTime),
                arguments(findManyByCriteriaWithCityAndFromTime, expectedFindManyByCriteriaWithCityAndFromTime));
    }

    /**
     * Test that when criteria is not compliant with validator.
     *
     * @param criteriaURL
     *            The search criteria permeates as a url,
     * @param validationError
     *            The expected error(`s).
     */

    @ParameterizedTest
    @MethodSource("invalidArgumentsData")
    void testFindByCriteriaEventShouldReturnValidarionError(final String criteriaURL,
                                                            final Collection<ValidationError<String>> validationError) {
        final TypeRef<Error<Collection<ValidationError<String>>>> typeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };
        final Error<Collection<ValidationError<String>>> actual = HTTPRequest
            .get(criteriaURL, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_BAD_REQUEST, typeHolder);

        final Error<Collection<ValidationError<String>>> expected = new Error<>(ErrorType.VALIDATION_ERROR,
            validationError);
        assertEquals(expected, actual);
    }

    private static Stream<Arguments> invalidArgumentsData() {
        final String cityFild = "City".toUpperCase();
        final String genreFild = "Genre".toUpperCase();
        final String invalidCityValue = "Sof231#@igdsfga";
        final String invalidGenreValue = "Pop@№";
        final String findMenyByCriteriaWithCityAndFromTime = SEARCH_EVENT_PATH + "?from=" + PAST_TIME_PARAM_MAX
            + "&city=" + invalidCityValue;
        final String findMenyByCriteriaWithGenreAndFromTime = SEARCH_EVENT_PATH + "?from=" + PAST_TIME_PARAM_MAX
            + "&genre=" + invalidGenreValue;
        final String findMenyByCriteriaWithCitiesAndGenreAndFromTime = SEARCH_EVENT_PATH + "?from="
            + PAST_TIME_PARAM_MAX + "&genre=" + invalidGenreValue + "&city=" + invalidCityValue;
        final String stringValidationError = ValidationErrorType.INVALID_SYMBOLS.toString();
        final Collection<ValidationError<String>> validationCityError = List
            .of(new ValidationError<>(cityFild, invalidCityValue, stringValidationError));
        final Collection<ValidationError<String>> validationGenreError = List
            .of(new ValidationError<>(genreFild, invalidGenreValue, stringValidationError));
        final Collection<ValidationError<String>> validationGenreAndCitiesErrors = List
            .of(new ValidationError<>(cityFild, invalidCityValue, stringValidationError),
                new ValidationError<>(genreFild, invalidGenreValue, stringValidationError));

        return Stream
            .of(arguments(findMenyByCriteriaWithCityAndFromTime, validationCityError),
                arguments(findMenyByCriteriaWithGenreAndFromTime, validationGenreError),
                arguments(findMenyByCriteriaWithCitiesAndGenreAndFromTime, validationGenreAndCitiesErrors));
    }
}
