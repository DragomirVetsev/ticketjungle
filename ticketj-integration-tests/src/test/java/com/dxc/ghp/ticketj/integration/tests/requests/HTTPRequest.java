package com.dxc.ghp.ticketj.integration.tests.requests;

import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

/**
 * Contains HTTP Requests (GET, POST, PUT, PATCH) for testing.
 */
@SuppressWarnings("nls")
public final class HTTPRequest {

    private static final String AUTHORIZATION = "Authorization";
    private static final String AUTHORIZATION_TYPE = "Bearer ";

    private HTTPRequest() {
        // Utility class, no instances
    }

    /**
     * Creates DELETE Requests.
     *
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param body
     *            The Requested body needed for a patch request.
     * @param statusCode
     *            expected HTTP response code.
     * @param type
     *            of the class to be returned.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult delete(final String url, final Object body, final String JwtToken,
                                                       final int statusCode, final Class<RequestResult> type,
                                                       final Object... pathParameters) {
        return delete(url, body, JwtToken, statusCode, pathParameters).as(type);
    }

    /**
     * Creates DELETE Requests.
     *
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param body
     *            The Requested body needed for a patch request.
     * @param statusCode
     *            expected HTTP response code.
     * @param typeRef
     *            class that allows to de-serialize a response to a container of
     *            generic type.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult delete(final String url, final Object body, final String JwtToken,
                                                       final int statusCode, final TypeRef<RequestResult> typeRef,
                                                       final Object... pathParameters) {
        return delete(url, body, JwtToken, statusCode, pathParameters).as(typeRef);
    }

    private static ResponseBodyExtractionOptions delete(final String url, final Object body, final String JwtToken,
                                                        final int statusCode, final Object... pathParameters) {
        return HTTPRequest.validateDeleteHeaders(url, body, JwtToken, statusCode, pathParameters).extract().body();
    }

    /**
     * Creates PATCH Requests.
     *
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param body
     *            The Requested body needed for a patch request.
     * @param statusCode
     *            expected HTTP response code.
     * @param type
     *            of the class to be returned.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult patch(final String url, final Object body, final String JwtToken,
                                                      final int statusCode, final Class<RequestResult> type,
                                                      final Object... pathParameters) {
        return patch(url, body, JwtToken, statusCode, pathParameters).as(type);
    }

    /**
     * Creates PATCH Requests.
     *
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param body
     *            The Requested body needed for a patch request.
     * @param statusCode
     *            expected HTTP response code.
     * @param typeRef
     *            class that allows to de-serialize a response to a container of
     *            generic type.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult patch(final String url, final Object body, final String JwtToken,
                                                      final int statusCode, final TypeRef<RequestResult> typeRef,
                                                      final Object... pathParameters) {
        return patch(url, body, JwtToken, statusCode, pathParameters).as(typeRef);
    }

    private static ResponseBodyExtractionOptions patch(final String url, final Object body, final String JwtToken,
                                                       final int statusCode, final Object... pathParameters) {
        return HTTPRequest.validatePatchHeaders(url, body, JwtToken, statusCode, pathParameters).extract().body();
    }

    /**
     * Creates a PATCH request and then asserts that the status code is equal to the
     * expected one.
     *
     * @param url
     *            is the given end point to send request to.
     * @param body
     *            The Requested body needed for a patch request.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param statusCode
     *            expected HTTP response code.
     * @param pathParameters
     *            are the path variables in the given URL.
     */
    public static void patchValidateStatus(final String url, final Object body, final String JwtToken,
                                           final int statusCode, final Object... pathParameters) {
        validatePatchStatus(url, body, JwtToken, statusCode, pathParameters);
    }

    /**
     * Creates POST Requests.
     *
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param body
     *            The Requested body needed for a post request.
     * @param statusCode
     *            expected HTTP response code.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param type
     *            of the class to be returned.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult post(final String url, final Object body, final String JwtToken,
                                                     final int statusCode, final Class<RequestResult> type,
                                                     final Object... pathParameters) {
        return post(url, body, JwtToken, statusCode, pathParameters).as(type);
    }

    /**
     * Creates POST Requests.
     *
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param body
     *            The Requested body needed for a post request.
     * @param statusCode
     *            expected HTTP response code.
     * @param typeRef
     *            class that allows to de-serialize a response to a container of
     *            generic type.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult post(final String url, final Object body, final String JwtToken,
                                                     final int statusCode, final TypeRef<RequestResult> typeRef,
                                                     final Object... pathParameters) {
        return post(url, body, JwtToken, statusCode, pathParameters).as(typeRef);
    }

    private static ResponseBodyExtractionOptions post(final String url, final Object body, final String JwtToken,
                                                      final int statusCode, final Object... pathParameters) {
        return HTTPRequest.validatePostHeaders(url, body, JwtToken, statusCode, pathParameters).extract().body();
    }

    /**
     * Creates Put Requests.
     *
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param body
     *            The Requested body needed for a post request.
     * @param statusCode
     *            expected HTTP response code.
     * @param type
     *            of the class to be returned.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult put(final String url, final Object body, final String JwtToken,
                                                    final int statusCode, final Class<RequestResult> type,
                                                    final Object... pathParameters) {
        return put(url, body, JwtToken, statusCode, pathParameters).as(type);
    }

    /**
     * Creates POST Requests.
     *
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param body
     *            The Requested body needed for a post request.
     * @param statusCode
     *            expected HTTP response code.
     * @param typeRef
     *            class that allows to de-serialize a response to a container of
     *            generic type.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult put(final String url, final Object body, final String JwtToken,
                                                    final int statusCode, final TypeRef<RequestResult> typeRef,
                                                    final Object... pathParameters) {
        return put(url, body, JwtToken, statusCode, pathParameters).as(typeRef);
    }

    private static ResponseBodyExtractionOptions put(final String url, final Object body, final String JwtToken,
                                                     final int statusCode, final Object... pathParameters) {
        return HTTPRequest.validatePutHeaders(url, body, JwtToken, statusCode, pathParameters).extract().body();
    }

    /**
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param statusCode
     *            expected HTTP response code.
     * @param type
     *            of the class to be returned.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult get(final String url, final String JwtToken, final int statusCode,
                                                    final Class<RequestResult> type, final Object... pathParameters) {
        return get(url, JwtToken, statusCode, pathParameters).as(type);
    }

    /**
     * @param <RequestResult>
     *            Is the result type.
     * @param url
     *            is the given end point to send request to.
     * @param statusCode
     *            expected HTTP response code.
     * @param typeRef
     *            class that allows to de-serialize a response to a container of
     *            generic type.
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            are the path variables in the given URL.
     * @return class of the given type.
     */
    public static <RequestResult> RequestResult get(final String url, final String JwtToken, final int statusCode,
                                                    final TypeRef<RequestResult> typeRef,
                                                    final Object... pathParameters) {

        return get(url, JwtToken, statusCode, pathParameters).as(typeRef);
    }

    private static ResponseBodyExtractionOptions get(final String url, final String JwtToken, final int statusCode,
                                                     final Object... pathParameters) {
        return HTTPRequest.validateGetHeaders(url, JwtToken, statusCode, pathParameters).extract().body();
    }

    /**
     * Method used to compare GET query's response body with a given JSON schema.
     *
     * @param url
     *            The give end point to send request to.
     * @param statusCode
     *            Expected HTTP response code.
     * @param json
     *            The name of the schema that will be used for comparison with the
     *            response body. Example: {@code testJsonSchema.json}
     * @param JwtToken
     *            is existing JWT token used for authentication and authorization.
     * @param pathParameters
     *            The path variables in the given URL.
     */
    public static void getAndCheckResponseBody(final String url, final String JwtToken, final int statusCode,
                                               final String json, final Object... pathParameters) {

        HTTPRequest
            .validateGetHeaders(url, JwtToken, statusCode, pathParameters)
            .body(JsonSchemaValidator.matchesJsonSchemaInClasspath(json));

    }

    private static ValidatableResponse validateGetHeaders(final String url, final String JwtToken, final int statusCode,
                                                          final Object... pathParameters) {
        // @formatter:off
        return RestAssured
            .given()
            .header(AUTHORIZATION, AUTHORIZATION_TYPE + JwtToken )
            .when()
                .get(url, pathParameters)
            .then()
                .assertThat()
                    .statusCode(statusCode)
                .and()
                    .contentType(ContentType.JSON)
                .and();
     // @formatter:on
    }

    private static ValidatableResponse validatePostHeaders(final String url, final Object body, final String JwtToken,
                                                           final int statusCode, final Object... pathParameters) {
     // @formatter:off
        return requestBody(body)
                .header(AUTHORIZATION, AUTHORIZATION_TYPE + JwtToken )
                .post(url, pathParameters)
            .then()
                .assertThat()
                    .statusCode(statusCode)
                .and()
                    .contentType(ContentType.JSON)
                .and();
     // @formatter:on
    }

    private static ValidatableResponse validatePutHeaders(final String url, final Object body, final String JwtToken,
                                                          final int statusCode, final Object... pathParameters) {
     // @formatter:off
        return requestBody(body)
                .header(AUTHORIZATION, AUTHORIZATION_TYPE + JwtToken )
                .put(url, pathParameters)
            .then()
                .assertThat()
                    .statusCode(statusCode)
                .and()
                    .contentType(ContentType.JSON)
                .and();
     // @formatter:on
    }

    private static ValidatableResponse validatePatchHeaders(final String url, final Object body, final String JwtToken,
                                                            final int statusCode, final Object... pathParameters) {
     // @formatter:off
        return requestBody(body)
                .header(AUTHORIZATION, AUTHORIZATION_TYPE + JwtToken )
                .patch(url, pathParameters)
            .then()
                .assertThat()
                    .statusCode(statusCode)
                .and()
                    .contentType(ContentType.JSON)
                .and();
     // @formatter:on
    }

    private static void validatePatchStatus(final String url, final Object body, final String JwtToken,
                                            final int statusCode, final Object... pathParameters) {
        requestBody(body)
            .header(AUTHORIZATION, AUTHORIZATION_TYPE + JwtToken)
            .patch(url, pathParameters)
            .then()
            .assertThat()
            .statusCode(statusCode);
    }

    private static ValidatableResponse validateDeleteHeaders(final String url, final Object body, final String JwtToken,
                                                             final int statusCode, final Object... pathParameters) {
     // @formatter:off
        return requestBody(body)
                .header(AUTHORIZATION, AUTHORIZATION_TYPE + JwtToken )
                .delete(url, pathParameters)
            .then()
                .assertThat()
                    .statusCode(statusCode)
                .and()
                    .contentType(ContentType.JSON)
                .and();
     // @formatter:on
    }

    private static RequestSpecification requestBody(final Object body) {
        return RestAssured.given().contentType(ContentType.JSON).body(body).when();
    }
}
