package com.dxc.ghp.ticketj.integration.tests;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.EventDTO;
import com.dxc.ghp.ticketj.dto.StageDTO;
import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.dto.SubEventOrderedPairDTO;
import com.dxc.ghp.ticketj.integration.tests.config.RestAssuredConfiguration;
import com.dxc.ghp.ticketj.integration.tests.requests.HTTPRequest;
import com.dxc.ghp.ticketj.integration.tests.utilities.Jwt;
import com.dxc.ghp.ticketj.integration.tests.utilities.SubEventDateCreator;
import com.dxc.ghp.ticketj.misc.exceptions.DuplicateFieldInfo;
import com.dxc.ghp.ticketj.misc.exceptions.EntityErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.Error;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;

import io.restassured.common.mapper.TypeRef;

/**
 * Integration tests for Event Restful service.
 */
@SuppressWarnings({
    "nls", "static-method"
})
public final class EventRestfulServiceIT extends RestAssuredConfiguration {

    private static final String EVENTS_PATH = "/events";
    private static final String SPECIFIC_EVENT_PATH = EVENTS_PATH + "/{event_name}";
    private static final String SUB_EVENTS_PATH = SPECIFIC_EVENT_PATH + "/sub-events";
    private static final String ALL_SUB_EVENTS_ORDERED_PATH = EVENTS_PATH + "/sub-events/ordered";
    private static final String SPECIFIC_SUB_EVENT = EVENTS_PATH + "/{event_name}/sub-events/{sub-event}";
    private static final String SEARCH_SUB_EVENT_SEAT_STATE = EVENTS_PATH + "/{event}/sub-events/{sub-event}/seats";
    private static final UserRole ROLE_CUSTOMER = UserRole.CUSTOMER;
    private static final UserRole ROLE_ADMIN = UserRole.ADMIN;
    private static String VALID_JWT_FOR_CUSTOMER = Jwt.createToken("Test Customer", ROLE_CUSTOMER);
    private static String VALID_JWT_FOR_ADMIN = Jwt.createToken("Test Admin", ROLE_ADMIN);

    /**
     * Test, when send GET HTTP Request to "/events" returns all events with status
     * code 200.
     */
    @Test
    void testFindAllEventsReturnCorrectData() {
        final EventDTO[] actual = HTTPRequest
            .get(EVENTS_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, EventDTO[].class);

        final EventDTO[] expected = {
            new EventDTO("Burgas Fest", "Only the best now in Burgas", "Pop"),
            new EventDTO("Cool den - tour", "Cool den - around Bulgaria", "Rock"),
            new EventDTO("GRAFA TOUR", "The new tour of Grafa 2023", "Pop"),
            new EventDTO("Leep Year Event", "Leap year event 2020", "Pop"),

            new EventDTO("Mercury World Tour",
                "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.", "Pop"),
            new EventDTO("ROLLING STONES TOUR", "WORLD TOUR SATISFACTION", "Rock"),
            new EventDTO("UTRELAND", "Waiting for everything", "Tehno"),
            new EventDTO("СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ", "Юбилейни концерти на група Сигнал за 45-та годишнина",
                "Rock"),
            new EventDTO("Театрални вечери във Варна", "Заповядайте на забравими театрални вечери в град Варна",
                "Theater"),
            new EventDTO("Турнето на Лили Иванова", "Коледния подарък на Лили за публиката и!", "Pop"),
            new EventDTO("Хамлет в Народния Театър", "Да бъдем или да не бъдем, това се пита", "Tragedy"),
        };
        Assertions.assertTrue(actual.length >= expected.length);
    }

    /**
     * Test for HTTP GET request with URL "/events/{name}". When invalid event name
     * is passed, response body should contain {@link Error} object and status code
     * {@link HttpStatus#SC_NOT_FOUND 404}.
     *
     * @param expectedEventDTO
     *            The expected result for EventDTO.
     */
    @ParameterizedTest
    @MethodSource("validArgumentsData")
    void findEventReturnsCorrectData(final EventDTO expectedEventDTO) {
        final EventDTO actualData = HTTPRequest
            .get(SPECIFIC_EVENT_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, EventDTO.class,
                 expectedEventDTO.getName());

        assertEquals(expectedEventDTO, actualData);
    }

    private static Stream<Arguments> validArgumentsData() {
        final String name = "ROLLING STONES TOUR";
        final String description = "WORLD TOUR SATISFACTION";
        final String genre = "Rock";

        final String name2 = "UTRELAND";
        final String description2 = "Waiting for everything";
        final String genre2 = "Tehno";

        final String name3 = "Mercury World Tour";
        final String description3 = "The Mercury World Tour is the fourth concert tour by American pop band Imagine Dragons.";
        final String genre3 = "Pop";

        return Stream
            .of(arguments(new EventDTO(name, description, genre)), arguments(new EventDTO(name2, description2, genre2)),
                arguments(new EventDTO(name3, description3, genre3)));
    }

    /**
     * Test for HTTP GET request with URL "/events/{name}". When invalid event name
     * is passed, response body should contain {@link Error} object and status code
     * {@link HttpStatus#SC_NOT_FOUND 404}.
     */
    @Test
    void findNonExistingEvent() {
        final String nonExistingEventName = "NonExistingEventName";
        final TypeRef<Error<EntityErrorInfo<String>>> errorTypeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };

        final Error<EntityErrorInfo<String>> actualData = HTTPRequest
            .get(SPECIFIC_EVENT_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_NOT_FOUND, errorTypeHolder,
                 nonExistingEventName);

        final Error<EntityErrorInfo<String>> expectedData = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("Event", nonExistingEventName));
        assertEquals(expectedData, actualData);
    }

    /**
     * Test for HTTP GET request with URL "/events/{name}". When invalid Event name
     * is passed, response body should contain {@link Error} object and status code
     * {@link HttpStatus#SC_BAD_REQUEST 409}
     *
     * @param invalidEventName
     *            The invalid name of the event.
     * @param expectedErrorCollection
     *            The collection with the collected errors after validation.
     */
    @ParameterizedTest
    @MethodSource("findEventWithInvalidEventNameData")
    void findEventFailsWhenPassingInvalidEventName(final String invalidEventName,
                                                   final Collection<ValidationError<String>> expectedErrorCollection) {
        final TypeRef<Error<Collection<ValidationError<String>>>> typeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };
        final Error<Collection<ValidationError<String>>> actual = HTTPRequest
            .get(SPECIFIC_EVENT_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_BAD_REQUEST, typeHolder, invalidEventName);
        final Error<Collection<ValidationError<String>>> expected = new Error<>(ErrorType.VALIDATION_ERROR,
            expectedErrorCollection);
        assertEquals(expected, actual);
    }

    /**
     * Test, when sending GET HTTP Request to "/events/{event-name}/sub-events"
     * returns all sub events in event with name {event_name} with status code 200.
     */
    @Test
    void findAllSubEventsInEventReturnCorrectData() {
        final String eventName = "ROLLING STONES TOUR";
        final SubEventDTO[] actual = HTTPRequest
            .get(SUB_EVENTS_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, SubEventDTO[].class, eventName);

        final SubEventDTO[] expected = {
            new SubEventDTO(new PerformanceId("My Satisfaction Plovdiv", eventName),
                new PerformanceDetails("Античен театър", "Сцена", SubEventDateCreator.subEventDateCreator(30, 20, 0)),
                "The world tour comes to Bulgaria", 100.00,
                new UnrestrictedAddress("ул. Цар Ивайло 4", "Plovdiv", "4000"), List.of("Rolling Stones"),
                new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("My Satisfaction Sofia - 2", eventName),
                new PerformanceDetails("NDK", "Zala 1", SubEventDateCreator.subEventDateCreator(-26, 20, 0)),
                "The world tour comes to Bulgaria", 100.00,
                new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"), List.of("Rolling Stones"),
                new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("My Satisfaction Sofia - 1", eventName),
                new PerformanceDetails("NDK", "Zala 1", SubEventDateCreator.subEventDateCreator(-27, 20, 0)),
                "The world tour comes to Bulgaria", 100.00,
                new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"), List.of("Rolling Stones"),
                new ReviewScoreAndCount(4.5, 2))
        };

        assertArrayEquals(expected, actual);
    }

    /**
     * Test, when sending GET HTTP Request to "/events/{event-name}/sub-events"
     * returns empty list of sub events in event with name {event_name} with status
     * code 200.
     */
    @Test
    void findAllSubEventsInEmptyEventReturnsEmptyList() {
        final String emptyEvent = "UTRELAND";

        final SubEventDTO[] actual = HTTPRequest
            .get(SUB_EVENTS_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, SubEventDTO[].class, emptyEvent);
        assertEquals(0, actual.length);
    }

    /**
     * Test for HTTP GET request with URL "/events/{name}". When invalid event name
     * is passed, response body should contain {@link Error} object and status code
     * {@link HttpStatus#SC_NOT_FOUND 404}.
     */
    @Test
    void findAllSubEventsInEventWhenNonExistingEvent() {
        final String nonExistingEventName = "NonExistingEventName";
        final TypeRef<Error<EntityErrorInfo<String>>> errorTypeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };

        final Error<EntityErrorInfo<String>> actualData = HTTPRequest
            .get(SUB_EVENTS_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_NOT_FOUND, errorTypeHolder,
                 nonExistingEventName);

        final Error<EntityErrorInfo<String>> expectedData = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("Event", nonExistingEventName));
        assertEquals(expectedData, actualData);
    }

    /**
     * Test for HTTP GET request with URL "/events/{name}/sub-events". When invalid
     * event name is passed, response body should contain {@link Error} object and
     * status code {@link HttpStatus#SC_BAD_REQUEST 409}
     *
     * @param invalidEventName
     *            The invalid name for event.
     * @param expectedErrorCollection
     *            The collection with the collected errors after validation.
     */
    @ParameterizedTest
    @MethodSource("findEventWithInvalidEventNameData")
    void findAllSubEventsInEventWhenInvalidEventName(final String invalidEventName,
                                                     final Collection<ValidationError<String>> expectedErrorCollection) {
        final TypeRef<Error<Collection<ValidationError<String>>>> typeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };
        final Error<Collection<ValidationError<String>>> actual = HTTPRequest
            .get(SUB_EVENTS_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_BAD_REQUEST, typeHolder, invalidEventName);
        final Error<Collection<ValidationError<String>>> expected = new Error<>(ErrorType.VALIDATION_ERROR,
            expectedErrorCollection);
        assertEquals(expected, actual);
    }

    /**
     * Test, when send GET HTTP Request to "/events/sub-events/{sub-event}/seats"
     * returns {@link StageDTO} with seats state for a given Sub-event.
     *
     * @param eventName
     *            The name of the Event to which the sub-event belongs.
     * @param subEventName
     *            The name of the sub-event.
     * @param jsonFile
     *            The name of the schema that will be used for comparison with the
     *            response body. Example: {@code testJsonSchema.json}
     */
    @ParameterizedTest
    @MethodSource("stageDtoData")
    void findAllSubEventSeatState(final String eventName, final String subEventName, final String jsonFile) {

        HTTPRequest
            .getAndCheckResponseBody(SEARCH_SUB_EVENT_SEAT_STATE, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, jsonFile,
                                     eventName, subEventName);
    }

    private static Stream<Arguments> stageDtoData() {
        return Stream
            .of(arguments("Mercury World Tour", "Mercury - Sofia", "mercuryWorldTour2023_07_30.json"),
                arguments("GRAFA TOUR", "Графа в София - 1", "grafaTour2023_07_22.json"),
                arguments("GRAFA TOUR", "Графа в София - 2", "grafaTour2023_09_24.json"),
                arguments("ROLLING STONES TOUR", "My Satisfaction Sofia - 1", "rollingStonesTour2023_07_20.json"),
                arguments("ROLLING STONES TOUR", "My Satisfaction Sofia - 2", "rollingStonesTour2023_07_21.json"));
    }

    /**
     * Test GET HTTP Request "/events/{event}/sub-events/{sub-event}/seats" when
     * there is no such Sub-event.
     *
     * @param eventName
     *            The name of the event.
     * @param invalidSubEventName
     *            The name of the sub-event.
     */
    @ParameterizedTest
    @MethodSource("invalidSubEventNameData")
    void findAllSubEventSeatStateInvalidSubEvent(final String eventName, final String invalidSubEventName) {
        final TypeRef<Error<EntityErrorInfo<PerformanceId>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when de-serializing a response.
        };
        final Error<EntityErrorInfo<PerformanceId>> actualError = HTTPRequest
            .get(SEARCH_SUB_EVENT_SEAT_STATE, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_NOT_FOUND, typeRef, eventName,
                 invalidSubEventName);
        final Error<EntityErrorInfo<PerformanceId>> expectedError = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("SubEvent", new PerformanceId(invalidSubEventName, eventName)));

        assertEquals(expectedError, actualError);
    }

    private static Stream<Arguments> invalidSubEventNameData() {
        return Stream.of(arguments("ROLLING STONES TOUR", "Invalid Sub Event Name"));
    }

    /**
     * Test GET HTTP Request to "/events/{event}/sub-events/{sub-event}/seats" when
     * no tickets are available.
     *
     * @param eventName
     *            The name of the Event to which the sub-event belongs.
     * @param venueName
     *            The name of the venue where the sub-event will take place.
     * @param stageName
     *            The name of the stage in the venue.
     * @param startTime
     *            The start date and time of the sub-event.
     */
    @Disabled
    @ParameterizedTest
    @MethodSource("stageDtoNoTicketsData")
    void findAllSubEventSeatStateNoTickets(final String eventName, final String venueName, final String stageName,
                                           final String startTime) {
        final String expectedResponse = "subEventSeatStateNoTickets.json";
        HTTPRequest
            .getAndCheckResponseBody(SEARCH_SUB_EVENT_SEAT_STATE, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK,
                                     expectedResponse, eventName, venueName, stageName, startTime);
    }

    private static Stream<Arguments> stageDtoNoTicketsData() {
        return Stream
            .of(arguments("GRAFA TOUR", "NDK", "Zala 3",
                          SubEventDateCreator.subEventDateCreator(-26, 19, 30).toString()));
    }

    /**
     * TicketJungle Test, when send GET HTTP Request to "/events/sub-events/ordered"
     * returns a pair of lists containing a list of all sub-events sorted by date
     * and time and a list of sub-events sorted by popularity with status code 200.
     */
    @Test
    void findAllSubEventsOrderedReturnCorrectData() {
        final String expectedResponse = "subEventsOrdered.json";
        HTTPRequest
            .getAndCheckResponseBody(ALL_SUB_EVENTS_ORDERED_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK,
                                     expectedResponse);
    }

    /**
     * TicketJungle Test, when send GET HTTP Request to "/events/sub-events/ordered"
     * returns a pair containing a list of all sub events sorted by date and time
     * with status code 200.
     */
    @Test
    void findAllSubEventsOrderedByTimeReturnCorrectData() {
        final SubEventOrderedPairDTO actual = HTTPRequest
            .get(ALL_SUB_EVENTS_ORDERED_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, SubEventOrderedPairDTO.class);

        final SubEventDTO[] expected = {

            new SubEventDTO(new PerformanceId("Всичко Лили - НДК", "Турнето на Лили Иванова"),
                new PerformanceDetails("NDK", "Zala 3", SubEventDateCreator.subEventDateCreator(128, 20, 00)),
                "Коледния концерт на Лили", 90.0, new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"),
                List.of("Лили Иванова"), new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("Просто Лили - НДК", "Турнето на Лили Иванова"),
                new PerformanceDetails("NDK", "Zala 3", SubEventDateCreator.subEventDateCreator(120, 20, 15)),
                "Коледния концерт на Лили", 90.0, new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"),
                List.of("Лили Иванова"), new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("Постановка на Хамлет - 6", "Хамлет в Народния Театър"),
                new PerformanceDetails("Народен театър \"Иван Вазов\"", "Голяма сцена",
                    SubEventDateCreator.subEventDateCreator(101, 19, 30)),
                "Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата",
                40.0, new UnrestrictedAddress("ул. „Дякон Игнатий“ №5", "Sofia", "1000"),
                List.of("Леонид Йовчев", "Мариус Куркински"), new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("Постановка на Хамлет - 5", "Хамлет в Народния Театър"),
                new PerformanceDetails("Народен театър \"Иван Вазов\"", "Голяма сцена",
                    SubEventDateCreator.subEventDateCreator(70, 19, 30)),
                "Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата",
                40.0, new UnrestrictedAddress("ул. „Дякон Игнатий“ №5", "Sofia", "1000"),
                List.of("Леонид Йовчев", "Мариус Куркински"), new ReviewScoreAndCount(0, 0)),
        };
        assertArrayEquals(expected, actual.orderedByTime().toArray());
    }

    /**
     * TicketJungle Test, when send GET HTTP Request to "/events/sub-events/ordered"
     * returns a pair containing a list of all sub events sorted by popularity with
     * status code 200.
     */
    @Test
    void findAllSubEventsOrderedByPopularityReturnCorrectData() {
        final SubEventOrderedPairDTO actual = HTTPRequest
            .get(ALL_SUB_EVENTS_ORDERED_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, SubEventOrderedPairDTO.class);

        final SubEventDTO[] expected = {
            new SubEventDTO(new PerformanceId("Постановка на Хамлет - 1", "Хамлет в Народния Театър"),
                new PerformanceDetails("Народен театър \"Иван Вазов\"", "Голяма сцена",
                    SubEventDateCreator.subEventDateCreator(-83, 19, 30)),
                "Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата",
                40.0, new UnrestrictedAddress("ул. „Дякон Игнатий“ №5", "Sofia", "1000"),
                List.of("Леонид Йовчев", "Мариус Куркински"), new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("Графа в София - 2", "GRAFA TOUR"),
                new PerformanceDetails("NDK", "Zala 3", SubEventDateCreator.subEventDateCreator(-25, 19, 30)),
                "Най-мащабното събитие в кариерата на българския топ изпълнител. София готова ли си?", 70.0,
                new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"), List.of("Grafa"),
                new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("My Satisfaction Sofia - 1", "ROLLING STONES TOUR"),
                new PerformanceDetails("NDK", "Zala 1", SubEventDateCreator.subEventDateCreator(-27, 20, 0)),
                "The world tour comes to Bulgaria", 100.0,
                new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"), List.of("Rolling Stones"),
                new ReviewScoreAndCount(4.5, 2)),
        };
        assertArrayEquals(expected, actual.orderedByPopularity().toArray());
    }

    private static Stream<Arguments> findEventWithInvalidEventNameData() {
        final String fieldName = EntityFieldType.EVENT.toString();
        final String tooShortEventName = "S";
        final String tooLongEventName = "ThisIsLongerEventNameToTestTheLengthValidationAppliedOnTheEventName"
            + "dddddddddddddddddddddddddddddddddddddd";
        final String invalidSymbolsName = "Inv@lidName;:ForEvent";
        final String tooShortAndInvalidSymbolsName = "S;";
        final String tooLongAndInvalidSymbolsName = "ThisIsLongerE(!*)ventNameToTe!~alidationAppliedOnTheEventName"
            + "AndMustBeOverSymbolsToBeInvalidBecauseOfRules";
        return Stream
            .of(arguments(tooShortEventName,
                          List
                              .of(new ValidationError<>(fieldName, tooShortEventName,
                                  ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(tooLongEventName,
                          List
                              .of(new ValidationError<>(fieldName, tooLongEventName,
                                  ValidationErrorType.INVALID_LENGTH.toString()))),
                arguments(invalidSymbolsName,
                          List
                              .of(new ValidationError<>(fieldName, invalidSymbolsName,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(tooShortAndInvalidSymbolsName,
                          List
                              .of(new ValidationError<>(fieldName, tooShortAndInvalidSymbolsName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(fieldName, tooShortAndInvalidSymbolsName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))),
                arguments(tooLongAndInvalidSymbolsName,
                          List
                              .of(new ValidationError<>(fieldName, tooLongAndInvalidSymbolsName,
                                  ValidationErrorType.INVALID_LENGTH.toString()),
                                  new ValidationError<>(fieldName, tooLongAndInvalidSymbolsName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }

    /**
     * Test, when send POST HTTP Request to "/events" returns the newly created
     * Event with status code 201.
     */
    @Test
    void createEventShouldCreateNewEvent() {
        final EventDTO expected = new EventDTO("Adele tour", "The best tour in Europe", "Pop");

        final EventDTO actual = HTTPRequest
            .post(EVENTS_PATH, expected, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CREATED, EventDTO.class);
        Assertions.assertEquals(expected, actual);

    }

    /**
     * Test, when send POST HTTP Request to "/events" returns an Error record with
     * status code 409 when event id is not unique.
     */
    @Test
    void createEventShouldReturnErrorWhenEntityExists() {
        final Error<EntityErrorInfo<String>> expected = new Error<>(ErrorType.ENTITY_EXISTS,
            new EntityErrorInfo<>("Event", "UTRELAND"));
        final TypeRef<Error<EntityErrorInfo<String>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final EventDTO duplicateEvent = new EventDTO("UTRELAND", "Test description", "Pop");
        final Error<EntityErrorInfo<String>> actual = HTTPRequest
            .post(EVENTS_PATH, duplicateEvent, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef);
        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send POST HTTP Request to "/events/{event_name}/sub-events"
     * returns the newly created sub-event with status code 201.
     */
    @Test
    void createSubEventShouldCreateNewSubEvent() {
        final String eventName = "GRAFA TOUR";
        final List<String> performers = List.of("Rolling Stones");
        final Instant time = Instant.parse("2023-07-22T19:30:00Z");
        final SubEventDTO expected = new SubEventDTO(new PerformanceId("My test tour", eventName),
            new PerformanceDetails("Античен театър", "Сцена", time), "Test description", 10.00,
            new UnrestrictedAddress("ул. Цар Ивайло 4", "Plovdiv", "4000"), performers,
            new ReviewScoreAndCount(0.00, 0));

        final SubEventDTO actual = HTTPRequest
            .post(SUB_EVENTS_PATH, expected, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CREATED, SubEventDTO.class, eventName);
        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send POST HTTP Request to "/events/{event-name}/sub-events"
     * returns an Error record with status code 409 when sub-event id is not unique.
     */
    @Test
    void createSubEventShouldReturnErrorWhenEntityExists() {
        final String eventName = "GRAFA TOUR";
        final Instant time = Instant.parse("2023-11-07T20:30:00Z");
        final List<String> performers = List.of("Grafa");
        final Error<EntityErrorInfo<PerformanceId>> expected = new Error<>(ErrorType.ENTITY_EXISTS,
            new EntityErrorInfo<>("SubEvent", new PerformanceId("Графа в Бургас", eventName)));
        final TypeRef<Error<EntityErrorInfo<PerformanceId>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final SubEventDTO duplicateEvent = new SubEventDTO(new PerformanceId("Графа в Бургас", eventName),
            new PerformanceDetails("Летен театър", "Сцена", time),
            "Най-мащабното събитие в кариерата на българския топ изпълнител. Бургас готов ли си?", 60.00,
            new UnrestrictedAddress("бул. Демокрация 94, Бургас Център", "Burgas", "8000"), performers,
            new ReviewScoreAndCount(0.00, 0));
        final Error<EntityErrorInfo<PerformanceId>> actual = HTTPRequest
            .post(SUB_EVENTS_PATH, duplicateEvent, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef, eventName);
        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send PATCH HTTP Request to "/events/{event-name}" returns a
     * modified event with status code 200.
     *
     * @param requestBody
     *            The request body containing only info to update.
     * @param expected
     *            The response body that is expected to be returned.
     */
    @ParameterizedTest
    @MethodSource("updateEventTestData")
    void updateEventShouldChangeOnlyNonNullFields(final EventDTO requestBody, final EventDTO expected) {
        final String eventName = "Adele tour";
        final EventDTO actual = HTTPRequest
            .patch(SPECIFIC_EVENT_PATH, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_OK, EventDTO.class, eventName);

        assertEquals(expected, actual);
    }

    private static Stream<Arguments> updateEventTestData() {
        final String eventName = "Adele tour";
        return Stream
            .of(arguments(new EventDTO(eventName, null, null),
                          new EventDTO(eventName, "The best tour in Europe", "Pop")),
                arguments(new EventDTO(eventName, "Updated description", null),
                          new EventDTO(eventName, "Updated description", "Pop")),
                arguments(new EventDTO(eventName, null, "Rock"),
                          new EventDTO(eventName, "Updated description", "Rock")),
                arguments(new EventDTO(eventName, "The best tour in Europe", "Pop"),
                          new EventDTO(eventName, "The best tour in Europe", "Pop")));
    }

    /**
     * Test, when send PATCH HTTP Request to "/events/{event-name}" returns not
     * found error with status code 404.
     *
     * @param requestBody
     *            The request body containing only info to update.
     */
    @ParameterizedTest
    @MethodSource("updateEventNotFoundTestData")
    void updateEventWhenNotFoundEvent(final EventDTO requestBody) {
        final String invalidEvent = "invalidEvent";
        final TypeRef<Error<EntityErrorInfo<String>>> errorTypeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };

        final Error<EntityErrorInfo<String>> actualData = HTTPRequest
            .patch(SPECIFIC_EVENT_PATH, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_NOT_FOUND, errorTypeHolder,
                   invalidEvent);

        final Error<EntityErrorInfo<String>> expectedData = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("Event", invalidEvent));
        assertEquals(expectedData, actualData);
    }

    private static Stream<Arguments> updateEventNotFoundTestData() {
        final String invalidEvent = "invalidEvent";
        return Stream.of(arguments(new EventDTO(invalidEvent, null, null)));
    }

    /**
     * Test, when send PATCH HTTP Request to "/events/{event-name}" with non
     * existing genre returns not found error with status code 404.
     *
     * @param requestBody
     *            The request body containing only info to update.
     */
    @ParameterizedTest
    @MethodSource("updateEventNotFoundGenreTestData")
    void updateEventWhenNotFoundGenre(final EventDTO requestBody) {
        final String eventName = "UTRELAND";
        final TypeRef<Error<EntityErrorInfo<String>>> errorTypeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };

        final Error<EntityErrorInfo<String>> actualData = HTTPRequest
            .patch(SPECIFIC_EVENT_PATH, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_NOT_FOUND, errorTypeHolder,
                   eventName);

        final Error<EntityErrorInfo<String>> expectedData = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("Genre", "noGenre"));
        assertEquals(expectedData, actualData);
    }

    private static Stream<Arguments> updateEventNotFoundGenreTestData() {
        final String invalidEvent = "invalidEvent";
        return Stream.of(arguments(new EventDTO(invalidEvent, null, "noGenre")));
    }

    /**
     * Test, when send PATCH HTTP Request to
     * "/events/{event_name}/sub-events/{sub-event}" returns a modified sub-event
     * with status code 200.
     *
     * @param requestBody
     *            The request body containing only info to update.
     * @param expected
     *            The response body that is expected to be returned.
     */
    @ParameterizedTest
    @MethodSource("updateSubEventTestData")
    void updateSubEventShouldChangeOnlyNonNullFields(final SubEventDTO requestBody, final SubEventDTO expected) {
        final String eventName = "ROLLING STONES TOUR";
        final String subEventName = "My Satisfaction Plovdiv";

        final SubEventDTO actual = HTTPRequest
            .patch(SPECIFIC_SUB_EVENT, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_OK, SubEventDTO.class, eventName,
                   subEventName);

        assertEquals(expected, actual);
    }

    private static Stream<Arguments> updateSubEventTestData() {
        final String subEventName = "My Satisfaction Plovdiv";
        final String eventName = "ROLLING STONES TOUR";
        final PerformanceId performanceId = new PerformanceId(subEventName, eventName);
        final String venue = "Античен театър";
        final String stage = "Сцена";
        final Instant startTime = SubEventDateCreator.subEventDateCreator(30, 20, 0);
        final Instant newStartTime = SubEventDateCreator.subEventDateCreator(30, 20, 10);
        final PerformanceDetails performanceDetails = new PerformanceDetails(venue, stage, startTime);
        final UnrestrictedAddress address = new UnrestrictedAddress("ул. Цар Ивайло 4", "Plovdiv", "4000");
        final List<String> performers = List.of("Rolling Stones");
        final ReviewScoreAndCount review = new ReviewScoreAndCount(0, 0);

        return Stream
            .of(arguments(new SubEventDTO(performanceId, performanceDetails, "The world tour comes to Bulgaria updated",
                0.00, address, null, review),
                          new SubEventDTO(performanceId, performanceDetails, "The world tour comes to Bulgaria updated",
                              100.00, address, performers, review)),
                arguments(new SubEventDTO(performanceId, performanceDetails, null, 1000.00, address, performers,
                    review),
                          new SubEventDTO(performanceId, performanceDetails, "The world tour comes to Bulgaria updated",
                              1000.00, address, performers, review)),
                arguments(new SubEventDTO(performanceId, null, "The world tour comes to Bulgaria", 100.00, address,
                    performers, review),
                          new SubEventDTO(performanceId, performanceDetails, "The world tour comes to Bulgaria", 100.00,
                              address, performers, review)),
                arguments(new SubEventDTO(performanceId, new PerformanceDetails(null, null, null),
                    "The world tour comes to Bulgaria", 100.00, address, performers, review),
                          new SubEventDTO(performanceId, performanceDetails, "The world tour comes to Bulgaria", 100.00,
                              address, performers, review)),
                arguments(new SubEventDTO(performanceId, new PerformanceDetails(null, null, newStartTime),
                    "The world tour comes to Bulgaria", 100.00, address, performers, review),
                          new SubEventDTO(performanceId, new PerformanceDetails(venue, stage, newStartTime),
                              "The world tour comes to Bulgaria", 100.00, address, performers, review)),
                arguments(new SubEventDTO(performanceId, new PerformanceDetails("NDK", "Zala 1", null),
                    "The world tour comes to Bulgaria", 100.00, address, List.of("Rolling Stones", "Lubo Kirov"),
                    review),
                          new SubEventDTO(performanceId, new PerformanceDetails("NDK", "Zala 1", newStartTime),
                              "The world tour comes to Bulgaria", 100.00,
                              new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"),
                              List.of("Rolling Stones", "Lubo Kirov"), review)),
                arguments(new SubEventDTO(performanceId, performanceDetails, "The world tour comes to Bulgaria", 100.00,
                    address, performers, review),
                          new SubEventDTO(performanceId, performanceDetails, "The world tour comes to Bulgaria", 100.00,
                              address, performers, review)));
    }

    /**
     * Test, when send PATCH HTTP Request to
     * "/events/{event_name}/sub-events/{sub-event}" with incorrect sub-event id
     * returns not found exception with status code 404.
     *
     * @param requestBody
     *            The request body containing only info to update.
     */
    @ParameterizedTest
    @MethodSource("updateSubEventNotFoundTestData")
    void updateSubEventNotFound(final SubEventDTO requestBody) {
        final String eventName = "GRAFA TOUR";
        final String subEventName = "ROLLING STONES TOUR";

        final TypeRef<Error<EntityErrorInfo<PerformanceId>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when de-serializing a response.
        };
        final Error<EntityErrorInfo<PerformanceId>> actualError = HTTPRequest
            .patch(SPECIFIC_SUB_EVENT, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_NOT_FOUND, typeRef, eventName,
                   subEventName);
        final EntityErrorInfo<PerformanceId> entityErrorInfo = new EntityErrorInfo<>("SubEvent",
            new PerformanceId(subEventName, eventName));
        final Error<EntityErrorInfo<PerformanceId>> expectedError = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            entityErrorInfo);
        assertEquals(expectedError, actualError);
    }

    private static Stream<Arguments> updateSubEventNotFoundTestData() {
        final String eventName = "Grafa TOUR";
        final String subEventName = "ROLLING STONES TOUR";
        final PerformanceDetails performance = new PerformanceDetails("Античен театър", "Сцена",
            SubEventDateCreator.subEventDateCreator(30, 20, 0));
        final UnrestrictedAddress address = new UnrestrictedAddress("ул. Цар Ивайло 4", "Plovdiv", "4000");
        final List<String> performers = List.of("Rolling Stones");
        final ReviewScoreAndCount review = new ReviewScoreAndCount(0, 0);

        return Stream
            .of(arguments(new SubEventDTO(new PerformanceId(subEventName, eventName), performance, null, 0.00, address,
                performers, review)));
    }

    /**
     * Test, when send PATCH HTTP Request to
     * "/events/{event_name}/sub-events/{sub-event}" with duplicate sub-event
     * details returns an Error record with status code 409 when performance details
     * are not unique.
     */
    @Test
    void updateSubEventDuplicateUniqueDetails() {
        final String subEventName = "My Satisfaction Sofia - 1";
        final String eventName = "ROLLING STONES TOUR";
        final PerformanceDetails conflictingDetails = new PerformanceDetails("NDK", "Zala 1",
            SubEventDateCreator.subEventDateCreator(-26, 20, 0));
        final PerformanceId performanceId = new PerformanceId(subEventName, eventName);
        final SubEventDTO duplicateSubEvent = new SubEventDTO(performanceId, conflictingDetails,
            "The world tour comes to Bulgaria", 100.00, new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"),
            List.of("Rolling Stones"), new ReviewScoreAndCount(4.50, 2));

        final Error<DuplicateFieldInfo<PerformanceDetails>> expected = new Error<>(ErrorType.DUPLICATE_FIELD,
            new DuplicateFieldInfo<>("SubEvent", EntityFieldType.SUB_EVENT_DETAILS, conflictingDetails));

        final TypeRef<Error<DuplicateFieldInfo<PerformanceDetails>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final Error<DuplicateFieldInfo<PerformanceDetails>> actual = HTTPRequest
            .patch(SPECIFIC_SUB_EVENT, duplicateSubEvent, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef,
                   eventName, subEventName);

        assertEquals(expected, actual);
    }

    /**
     * Test, when send DELETE HTTP Request to
     * "/events/{event_name}/sub-events/{sub-event}" returns deleted sub-event with
     * status code 200.
     *
     * @param requestBody
     *            The request body containing only the sub-event to delete.
     */
    @ParameterizedTest
    @MethodSource("deleteSubEventTestData")
    void deleteSubEvent(final SubEventDTO requestBody) {
        final String eventName = "Mercury World Tour";
        final String subEventName = "Mercury - Burgas 2";

        final SubEventDTO actual = HTTPRequest
            .delete(SPECIFIC_SUB_EVENT, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_OK, SubEventDTO.class,
                    eventName, subEventName);

        assertEquals(requestBody, actual);
    }

    private static Stream<Arguments> deleteSubEventTestData() {
        final String eventName = "Mercury World Tour";
        final String subEventName = "Mercury - Burgas 2";
        final PerformanceDetails performanceDetails = new PerformanceDetails("Летен театър", "Сцена",
            SubEventDateCreator.subEventDateCreator(48, 19, 30));
        final UnrestrictedAddress address = new UnrestrictedAddress("бул. Демокрация 94, Бургас Център", "Burgas",
            "8000");
        final List<String> performers = List.of("Imagine Dragons");
        final ReviewScoreAndCount review = new ReviewScoreAndCount(0, 0);

        return Stream
            .of(arguments(new SubEventDTO(new PerformanceId(subEventName, eventName), performanceDetails,
                "The world tour comes to Bulgaria", 100.00, address, performers, review)));
    }

    /**
     * Test, when send DELETE HTTP Request to
     * "/events/{event_name}/sub-events/{sub-event}" with incorrect sub-event id
     * returns not found exception with status code 404.
     *
     * @param requestBody
     *            The request body containing only info to update.
     */
    @ParameterizedTest
    @MethodSource("deleteSubEventNotFoundTestData")
    void deleteSubEventNotFound(final SubEventDTO requestBody) {
        final String eventName = "GRAFA TOUR";
        final String subEventName = "NO_SUCH_SUB_EVENT";

        final TypeRef<Error<EntityErrorInfo<PerformanceId>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when de-serializing a response.
        };
        final Error<EntityErrorInfo<PerformanceId>> actualError = HTTPRequest
            .delete(SPECIFIC_SUB_EVENT, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_NOT_FOUND, typeRef, eventName,
                    subEventName);
        final EntityErrorInfo<PerformanceId> entityErrorInfo = new EntityErrorInfo<>("SubEvent",
            new PerformanceId(subEventName, eventName));
        final Error<EntityErrorInfo<PerformanceId>> expectedError = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            entityErrorInfo);
        assertEquals(expectedError, actualError);
    }

    private static Stream<Arguments> deleteSubEventNotFoundTestData() {
        final String eventName = "Grafa TOUR";
        final String subEventName = "NO_SUCH_SUB_EVENT";
        final PerformanceDetails performanceDetails = new PerformanceDetails("Античен театър", "Сцена",
            SubEventDateCreator.subEventDateCreator(48, 19, 30));
        final UnrestrictedAddress address = new UnrestrictedAddress("ул. Цар Ивайло 4", "Plovdiv", "4000");
        final List<String> performers = List.of("");
        final ReviewScoreAndCount review = new ReviewScoreAndCount(0, 0);

        return Stream
            .of(arguments(new SubEventDTO(new PerformanceId(subEventName, eventName), performanceDetails, null, 10.00,
                address, performers, review)));
    }

    /**
     * Test, when send DELETE HTTP Request to
     * "/events/{event_name}/sub-events/{sub-event}" with a sub-event which has
     * already sold tickets, has reviews or is in a wishlist of a customer, returns
     * has_relationship error with status code 403.
     *
     * @param requestBody
     *            The request body containing only info to update.
     */
    @ParameterizedTest
    @MethodSource("deleteSubEventHasRelationshipTestData")
    void deleteSubEventHasRelationship(final SubEventDTO requestBody) {
        final String eventName = "ROLLING STONES TOUR";
        final String subEventName = "My Satisfaction Sofia - 1";

        final TypeRef<Error<EntityErrorInfo<PerformanceId>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when de-serializing a response.
        };
        final Error<EntityErrorInfo<PerformanceId>> actualError = HTTPRequest
            .delete(SPECIFIC_SUB_EVENT, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef, eventName,
                    subEventName);
        final EntityErrorInfo<PerformanceId> entityErrorInfo = new EntityErrorInfo<>("SubEvent",
            new PerformanceId(subEventName, eventName));
        final Error<EntityErrorInfo<PerformanceId>> expectedError = new Error<>(ErrorType.HAS_RELATIONSHIP,
            entityErrorInfo);
        assertEquals(expectedError, actualError);
    }

    private static Stream<Arguments> deleteSubEventHasRelationshipTestData() {
        final String eventName = "ROLLING STONES TOUR";
        final String subEventName = "My Satisfaction Sofia - 1";
        final PerformanceDetails performanceDetails = new PerformanceDetails("NDK", "Zala 1",
            SubEventDateCreator.subEventDateCreator(-27, 20, 0));
        final UnrestrictedAddress address = new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000");
        final List<String> performers = List.of("Rolling Stones");
        final ReviewScoreAndCount review = new ReviewScoreAndCount(4.50, 2);

        return Stream
            .of(arguments(new SubEventDTO(new PerformanceId(subEventName, eventName), performanceDetails,
                "The world tour comes to Bulgaria", 100.00, address, performers, review)));
    }
}
