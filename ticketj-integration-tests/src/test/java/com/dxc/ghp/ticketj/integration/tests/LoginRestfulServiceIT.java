package com.dxc.ghp.ticketj.integration.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.dto.UserJwtDTO;
import com.dxc.ghp.ticketj.integration.tests.config.RestAssuredConfiguration;
import com.dxc.ghp.ticketj.integration.tests.requests.HTTPRequest;
import com.dxc.ghp.ticketj.misc.exceptions.Error;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.exceptions.InvalidCredentialsInfo;
import com.dxc.ghp.ticketj.misc.types.Credentials;

import io.restassured.common.mapper.TypeRef;

/**
 *
 */
@SuppressWarnings({
    "nls", "static-method"
})
public final class LoginRestfulServiceIT extends RestAssuredConfiguration {

    private static final String LOGIN_PATH = "/login";

    /**
     * Testing when login with invalid username, {@code InvalidCredentialsException}
     * is thrown with HTTP Status code 404.
     */
    @Test
    void testLoginWithInvalidUsernameThrows() {
        final TypeRef<Error<InvalidCredentialsInfo<String>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final Credentials invalidCredentials = new Credentials("wrongUsername", "111111");
        final Error<InvalidCredentialsInfo<String>> expected = new Error<>(ErrorType.INVALID_CREDENTIALS,
            new InvalidCredentialsInfo<>("User", "Wrong username or password!"));
        final Error<InvalidCredentialsInfo<String>> actual = HTTPRequest
            .post(LOGIN_PATH, invalidCredentials, " ", HttpStatus.SC_BAD_REQUEST, typeRef);

        assertEquals(expected, actual);
    }

    /**
     * Testing when login with invalid password, {@code InvalidCredentialsException}
     * is thrown with HTTP Status code 404.
     */
    @Test
    void testLoginWithInvalidPasswordThrows() {
        final TypeRef<Error<InvalidCredentialsInfo<String>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final Credentials invalidCredentials = new Credentials("bobo", "wrongPassword");
        final Error<InvalidCredentialsInfo<String>> expected = new Error<>(ErrorType.INVALID_CREDENTIALS,
            new InvalidCredentialsInfo<>("User", "Wrong username or password!"));
        final Error<InvalidCredentialsInfo<String>> actual = HTTPRequest
            .post(LOGIN_PATH, invalidCredentials, " ", HttpStatus.SC_BAD_REQUEST, typeRef);

        assertEquals(expected, actual);
    }

    /**
     * Testing on login with valid credentials a JWT token is created for that user
     * with HTTP Status code 200.
     */
    @Test
    void testLoginWithValidCredentialsCreatesJwtToken() {
        final Credentials validCredentials = new Credentials("bobo", "111111");
        final UserJwtDTO expectedResult = HTTPRequest
            .post(LOGIN_PATH, validCredentials, "", HttpStatus.SC_OK, UserJwtDTO.class);
        final String JwtToken = expectedResult.jwtToken();

        assertFalse(JwtToken.isEmpty());
    }
}
