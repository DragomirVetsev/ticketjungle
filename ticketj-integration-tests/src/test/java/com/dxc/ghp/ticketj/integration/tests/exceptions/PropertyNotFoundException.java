package com.dxc.ghp.ticketj.integration.tests.exceptions;

/**
 * Custom exception thrown, when file property is missing.
 */
public final class PropertyNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -8943985604206657675L;

    /**
     * Constructor that takes the name of a specific property.
     *
     * @param missingProperty
     *            specific property that is missing in the file.
     */
    @SuppressWarnings("nls")
    public PropertyNotFoundException(final String missingProperty) {
        super(String.format("Property with '%s' name was not found!", missingProperty));
    }

    /**
     * Constructor with no parameters, called when property file cannot be loaded.
     */
    @SuppressWarnings("nls")
    public PropertyNotFoundException() {
        super("Property file was not found!");
    }
}
