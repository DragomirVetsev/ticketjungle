package com.dxc.ghp.ticketj.integration.tests.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.dxc.ghp.ticketj.integration.tests.exceptions.PropertyNotFoundException;

import io.restassured.RestAssured;

/**
 * Configuration set-up for Rest Assured library.
 */
public abstract class RestAssuredConfiguration {

    static {
        try {
            final Properties properties = loadConfiguration();
            setRestAssuredProperties(properties);
        } catch (final IOException | IllegalArgumentException | PropertyNotFoundException exception) {
            throw new ExceptionInInitializerError(exception);
        }
    }

    @SuppressWarnings("nls")
    private static Properties loadConfiguration() throws IOException {
        final String testConfigurationFilePath = "/restassured-config.txt";
        try (final InputStream stream = RestAssuredConfiguration.class.getResourceAsStream(testConfigurationFilePath)) {
            if (stream == null) {
                throw new PropertyNotFoundException();
            }
            final Properties properties = new Properties();
            properties.load(stream);
            return properties;
        }
    }

    @SuppressWarnings("nls")
    private static void setRestAssuredProperties(final Properties properties) {
        RestAssured.baseURI = findProperty(properties, "baseURI");
        RestAssured.basePath = findProperty(properties, "basePath");
        RestAssured.port = Integer.parseInt(findProperty(properties, "port"));
    }

    private static String findProperty(final Properties properties, final String propertyToFind) {
        final String foundProperty = properties.getProperty(propertyToFind);
        if (foundProperty == null) {
            throw new PropertyNotFoundException(propertyToFind);
        }
        return foundProperty;
    }
}
