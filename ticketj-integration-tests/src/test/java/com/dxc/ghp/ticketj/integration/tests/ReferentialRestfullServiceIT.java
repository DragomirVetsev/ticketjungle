/**
 *
 */
package com.dxc.ghp.ticketj.integration.tests;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.dto.CitiesAndGenresDTO;
import com.dxc.ghp.ticketj.dto.PerformerDTO;
import com.dxc.ghp.ticketj.integration.tests.config.RestAssuredConfiguration;
import com.dxc.ghp.ticketj.integration.tests.requests.HTTPRequest;
import com.dxc.ghp.ticketj.integration.tests.utilities.Jwt;
import com.dxc.ghp.ticketj.misc.types.UserRole;

/**
 * Integration tests for Referential Restful service.
 */
@SuppressWarnings({
    "static-method", "nls"
})
public final class ReferentialRestfullServiceIT extends RestAssuredConfiguration {

    private static final UserRole ROLE_CUSTOMER = UserRole.CUSTOMER;
    private static String VALID_JWT_FOR_CUSTOMER = Jwt.createToken("Test Customer", ROLE_CUSTOMER);
    private static String REFERENTIAL_PATH = "/referential";

    /**
     * Test, when send GET HTTP Request to "/referential/cities-and-genres" returns
     * all static information regarding search by criteria with status code 200.
     */

    @Test
    void testSearchByCriteriaShoudReturnNotEmpty() {
        final String finadCitiesAndGenresURL = REFERENTIAL_PATH + "/cities-and-genres";
        final CitiesAndGenresDTO actual = HTTPRequest
            .get(finadCitiesAndGenresURL, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, CitiesAndGenresDTO.class);

        final List<String> genres = List.of("Pop", "Rock", "Tehno", "Theater", "Tragedy");

        final List<String> cities = List
            .of("Burgas", "Montana", "Plovdiv", "Sofia", "Stara Zagora", "Varna", "Veliko Turnovo");
        final CitiesAndGenresDTO expected = new CitiesAndGenresDTO(genres, cities);
        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send GET HTTP Request to "/referential/performers" returns all
     * performers with status code 200.
     */
    @Test
    void testFindPerformersReturnCorrectData() {
        final PerformerDTO[] actual = HTTPRequest
            .get(REFERENTIAL_PATH + "/performers", VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, PerformerDTO[].class);

        final List<PerformerDTO> expected = new ArrayList<>();
        expected
            .addAll(List
                .of(new PerformerDTO("Grafa", "The bulgarian singer"), new PerformerDTO("Imagine Dragons",
                    "The American pop rock band based in Las Vegas, Nevada, consisting of lead singer Dan Reynolds, "
                        + "guitarist Wayne Sermon, bassist Ben McKee and drummer Daniel Platzman"),

                    new PerformerDTO("Rolling Stones", "The best band of the World"),
                    new PerformerDTO("Teatralna grupa", "bulgarian teather group"),
                    new PerformerDTO("Леонид Йовчев",
                        "Завършва НАТФИЗ „Кр. Сарафов\" през 2007 г. „актьорско майсторство за драматичен театър\" в "
                            + "класа на проф. Димитрина Гюров."),
                    new PerformerDTO("Лили Иванова",
                        "Примата на българската естрада“. Обявена е за \"Най-големия български поп изпълнител "
                            + "за всички времена\" със специална награда от БГ радио през 2023 г."),
                    new PerformerDTO("Мариус Куркински",
                        "Български театрален и кино актьор, режисьор и музикален "
                            + "изпълнител, особено известен с монологичните си театрални постановки"),
                    new PerformerDTO("СИГНАЛ", "Българската рок група, създадена в София през юни 1978 година "
                        + "от Йордан Караджов, Христо Ламбрев, Румен Спасов и Георги Кокаланов.")));

        if (actual.length == expected.size() + 1) {
            expected.add(2, new PerformerDTO("Lubo Kirov", "Auto-generated description."));
        }

        Assertions.assertArrayEquals(expected.toArray(), actual);
    }
}
