/**
 *
 */
package com.dxc.ghp.ticketj.integration.tests;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.CustomerDTO;
import com.dxc.ghp.ticketj.dto.DiscountDTO;
import com.dxc.ghp.ticketj.dto.OrderDTO;
import com.dxc.ghp.ticketj.dto.OrderWithTicketsDTO;
import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.dto.ReviewDTO;
import com.dxc.ghp.ticketj.dto.SeatDTO;
import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.dto.TicketDTO;
import com.dxc.ghp.ticketj.dto.TicketForOrderDTO;
import com.dxc.ghp.ticketj.dto.TicketFromOrderDTO;
import com.dxc.ghp.ticketj.integration.tests.config.RestAssuredConfiguration;
import com.dxc.ghp.ticketj.integration.tests.requests.HTTPRequest;
import com.dxc.ghp.ticketj.integration.tests.utilities.Jwt;
import com.dxc.ghp.ticketj.integration.tests.utilities.SubEventDateCreator;
import com.dxc.ghp.ticketj.misc.exceptions.EntityErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.exceptions.Error;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.exceptions.ForbiddenAccessExceptionInfo;
import com.dxc.ghp.ticketj.misc.exceptions.UnauthorizedExceptionInfo;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.PerformanceDetails;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.ReviewScoreAndCount;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;

import io.restassured.common.mapper.TypeRef;

/**
 * Integration tests for Customer Restful service
 */
@SuppressWarnings({
    "nls", "static-method"
})
@TestMethodOrder(OrderAnnotation.class)
public final class CustomerRestfulServiceIT extends RestAssuredConfiguration {

    private static final String CUSTOMERS_PATH = "/customers";
    private static final String SPECIFIC_CUSTOMER_PATH = CUSTOMERS_PATH + "/{username}";
    private static final String CUSTOMER_TICKETS_PATH = SPECIFIC_CUSTOMER_PATH + "/tickets";
    private static final String CUSTOMER_WISHLIST_PATH = SPECIFIC_CUSTOMER_PATH + "/wishlist";
    private static final String CUSTOMER_WISHLIST_ADD_PATH = CUSTOMER_WISHLIST_PATH + "/add";
    private static final String CUSTOMER_WISHLIST_REMOVE_PATH = CUSTOMER_WISHLIST_PATH + "/remove";
    private static final String CUSTOMER_WISHLIST_REMOVE_SELECTED_PATH = CUSTOMER_WISHLIST_REMOVE_PATH + "/selected";
    private static final String CUSTOMER_REVIEWS_PATH = SPECIFIC_CUSTOMER_PATH + "/reviews";
    private static final String PATH_FOR_ADMINS_ONLY = "/users";
    private static final String missingCustomer = "missingUsernameInDB";
    private static final String emptyListsCustomer = "ticketHunter";
    private static final UserRole ROLE_CUSTOMER = UserRole.CUSTOMER;
    private static final String ORDER_PATH = SPECIFIC_CUSTOMER_PATH + "/order";
    private static String VALID_JWT_FOR_sa60_CUSTOMER = Jwt.createToken("sa60", ROLE_CUSTOMER);

    /**
     * Test for HTTP GET request with URL "/customers/{username}" with existing
     * customer username is passed, response body should contain array of
     * {@link CustomerDTO} objects and status code {@link HttpStatus#SC_OK 200}.
     *
     * @param expectedCustomerDTO
     *            The expected result for CustomerDTO.
     */
    @ParameterizedTest
    @MethodSource("validArgumentsData")
    void testFindCustomerReturnsCorrectData(final CustomerDTO expectedCustomerDTO) {
        final String validJwtForCustomer = Jwt.createToken(expectedCustomerDTO.username(), ROLE_CUSTOMER);
        final CustomerDTO actual = HTTPRequest
            .get(SPECIFIC_CUSTOMER_PATH, validJwtForCustomer, HttpStatus.SC_OK, CustomerDTO.class,
                 expectedCustomerDTO.username());

        assertEquals(expectedCustomerDTO, actual);
    }

    /**
     * Test that when username is missing the returned data is null.
     */
    @Test
    void testFindCustomerShouldReturnNullWhenUsernameIsMissing() {
        final String validJwtForCustomer = Jwt.createToken(missingCustomer, ROLE_CUSTOMER);
        final CustomerDTO actual = HTTPRequest
            .get(SPECIFIC_CUSTOMER_PATH, validJwtForCustomer, HttpStatus.SC_OK, CustomerDTO.class, missingCustomer);

        assertNull(actual);
    }

    /**
     * Test that when user-name is not compliant with validator.
     *
     * @param path
     *            The path for the specific end-point.
     * @param username
     *            The name for witch the find will be conducted.
     * @param error
     *            The type of the violation.
     */
    @ParameterizedTest
    @MethodSource("invalidArgumentsData")
    void testFindCustomerShouldReturnValidarionError(final String path, final String username, final String error) {
        final TypeRef<Error<Collection<ValidationError<String>>>> typeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);
        final Error<Collection<ValidationError<String>>> actual = HTTPRequest
            .get(path, validJwtForCustomer, HttpStatus.SC_BAD_REQUEST, typeHolder, username);

        final Collection<ValidationError<String>> validationError = List
            .of(new ValidationError<>(EntityFieldType.USERNAME.toString(), username, error));

        final Error<Collection<ValidationError<String>>> expected = new Error<>(ErrorType.VALIDATION_ERROR,
            validationError);
        assertEquals(expected, actual);
    }

    private static Stream<Arguments> invalidArgumentsData() {
        final String usernameWithSpecialSymbols = "missingUsernameIn:\"P\"#@ES";
        final String usernameTooShort = "m";
        final String usernameTooLong = "ThisUsernameIsTooLongSoTheValidationWillNotAllowIt";

        return Stream
            .of(arguments(SPECIFIC_CUSTOMER_PATH, usernameWithSpecialSymbols,
                          ValidationErrorType.INVALID_SYMBOLS.toString()),
                arguments(SPECIFIC_CUSTOMER_PATH, usernameTooShort, ValidationErrorType.INVALID_LENGTH.toString()),
                arguments(SPECIFIC_CUSTOMER_PATH, usernameTooLong, ValidationErrorType.INVALID_LENGTH.toString()),
                arguments(CUSTOMER_TICKETS_PATH, usernameWithSpecialSymbols,
                          ValidationErrorType.INVALID_SYMBOLS.toString()),
                arguments(CUSTOMER_TICKETS_PATH, usernameTooShort, ValidationErrorType.INVALID_LENGTH.toString()),
                arguments(CUSTOMER_TICKETS_PATH, usernameTooLong, ValidationErrorType.INVALID_LENGTH.toString()),
                arguments(CUSTOMER_WISHLIST_PATH, usernameWithSpecialSymbols,
                          ValidationErrorType.INVALID_SYMBOLS.toString()),
                arguments(CUSTOMER_WISHLIST_PATH, usernameTooShort, ValidationErrorType.INVALID_LENGTH.toString()),
                arguments(CUSTOMER_WISHLIST_PATH, usernameTooLong, ValidationErrorType.INVALID_LENGTH.toString()),
                arguments(CUSTOMER_REVIEWS_PATH, usernameWithSpecialSymbols,
                          ValidationErrorType.INVALID_SYMBOLS.toString()),
                arguments(CUSTOMER_REVIEWS_PATH, usernameTooShort, ValidationErrorType.INVALID_LENGTH.toString()),
                arguments(CUSTOMER_REVIEWS_PATH, usernameTooLong, ValidationErrorType.INVALID_LENGTH.toString()));

    }

    /**
     * Test for HTTP GET request with URL "/customers/{username}/tickets" with
     * existing customer username and not empty ticket list is passed, response body
     * should contain array of {@link TicketDTO} objects and status code
     * {@link HttpStatus#SC_OK 200}.
     */
    @Test
    void testFindTicketsReturnsListWithElements() {
        final String validJwtForCustomer = Jwt.createToken("bobo", ROLE_CUSTOMER);
        final TicketDTO[] actualData = HTTPRequest
            .get(CUSTOMER_TICKETS_PATH, validJwtForCustomer, HttpStatus.SC_OK, TicketDTO[].class, "bobo");

        final DiscountDTO expectedDiscount = new DiscountDTO("STUDENT", 10);
        final TicketDTO[] expectedData = {
            new TicketDTO(1, new PerformanceId("My Satisfaction Sofia - 1", "ROLLING STONES TOUR"),
                "The world tour comes to Bulgaria",
                new PerformanceDetails("NDK", "Zala 1", SubEventDateCreator.subEventDateCreator(-27, 20, 0)),
                "Sector 1", 1, new SeatDTO(1, "ACQUIRED"), 90.0, expectedDiscount),
            new TicketDTO(1, new PerformanceId("Mercury - Sofia", "Mercury World Tour"),
                "The world tour comes to Bulgaria",
                new PerformanceDetails("NDK", "Zala 3", SubEventDateCreator.subEventDateCreator(-17, 19, 30)),
                "Sector 1", 1, new SeatDTO(1, "ACQUIRED"), 90.0, expectedDiscount),
            new TicketDTO(2, new PerformanceId("My Satisfaction Sofia - 1", "ROLLING STONES TOUR"),
                "The world tour comes to Bulgaria",
                new PerformanceDetails("NDK", "Zala 1", SubEventDateCreator.subEventDateCreator(-27, 20, 0)),
                "Sector 2", 2, new SeatDTO(33, "ACQUIRED"), 80.0, new DiscountDTO("KID", 20))
        };
        assertArrayEquals(expectedData, actualData);
    }

    /**
     * Test for HTTP GET request with URL "/customers/{username}/tickets". When
     * existing customer does not have any tickets, response body should contain
     * empty {@link TicketDTO} array and status code {@link HttpStatus#SC_OK 200}.
     */
    @Test
    void testFindTicketsReturnsEmptyList() {
        final String validJwtForCustomer = Jwt.createToken(emptyListsCustomer, ROLE_CUSTOMER);
        final TicketDTO[] actualData = HTTPRequest
            .get(CUSTOMER_TICKETS_PATH, validJwtForCustomer, HttpStatus.SC_OK, TicketDTO[].class, emptyListsCustomer);

        assertEquals(0, actualData.length);
    }

    /**
     * Test for HTTP GET request with URL "/customers/{username}/tickets". When
     * {@literal non-existing} customer username is passed, response body should
     * contain {@link Error} object and status code {@link HttpStatus#SC_NOT_FOUND
     * 404}.
     *
     * @throws EntityNotFoundException
     *             with information displayed by {@code Error} and
     *             {@code EntityErrorInfo}
     */
    @Test
    void testFindTicketsWithMissingCustomerThrows() {
        final TypeRef<Error<EntityErrorInfo<String>>> typeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };
        final String validJwtForCustomer = Jwt.createToken(missingCustomer, ROLE_CUSTOMER);
        final Error<EntityErrorInfo<String>> actualData = HTTPRequest
            .get(CUSTOMER_TICKETS_PATH, validJwtForCustomer, HttpStatus.SC_NOT_FOUND, typeHolder, missingCustomer);

        final Error<EntityErrorInfo<String>> expectedData = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("Customer", missingCustomer));
        assertEquals(expectedData, actualData);
    }

    /**
     * Test when, send GET HTTP request to "/customers/{username}/wishlist" with
     * existing customer username and not empty wish list is passed, response body
     * should contain array of {@link SubEventDTO} objects and status code
     * {@link HttpStatus#SC_OK 200}.
     */
    @Test
    void testFindWishlistReturnsListWithElements() {
        final String customerWithWishlist = "bobo";
        final String validJwtForCustomer = Jwt.createToken(customerWithWishlist, ROLE_CUSTOMER);
        final SubEventDTO[] actual = HTTPRequest
            .get(CUSTOMER_WISHLIST_PATH, validJwtForCustomer, HttpStatus.SC_OK, SubEventDTO[].class,
                 customerWithWishlist);

        final SubEventDTO[] expected = {
            new SubEventDTO(new PerformanceId("Mercury - Sofia", "Mercury World Tour"),
                new PerformanceDetails("NDK", "Zala 3", SubEventDateCreator.subEventDateCreator(-17, 19, 30)),
                "The world tour comes to Bulgaria", 100.0,
                new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"), List.of("Imagine Dragons"),
                new ReviewScoreAndCount(5, 1)),
            new SubEventDTO(new PerformanceId("My Satisfaction Sofia - 1", "ROLLING STONES TOUR"),
                new PerformanceDetails("NDK", "Zala 1", SubEventDateCreator.subEventDateCreator(-27, 20, 0)),
                "The world tour comes to Bulgaria", 100.0,
                new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"), List.of("Rolling Stones"),
                new ReviewScoreAndCount(4.5, 2)),
            new SubEventDTO(new PerformanceId("Years&Years", "Burgas Fest"),
                new PerformanceDetails("Летен театър", "Сцена", SubEventDateCreator.subEventDateCreator(-12, 20, 0)),
                "Osker and the worlf", 40.00,
                new UnrestrictedAddress("бул. Демокрация 94, Бургас Център", "Burgas", "8000"), new ArrayList<>(),
                new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("Бургас- Greenburg fest-/Papa Beer/", "Cool den - tour"),
                new PerformanceDetails("Драматичен театър - Стоян Бъчваров", "Главна зала",
                    SubEventDateCreator.subEventDateCreator(36, 22, 0)),
                "Здравей приятели от топъл Бургас! Заповядайте на нашия септеврийски концерт!", 20.00,
                new UnrestrictedAddress("пл. „Независимост“ 1, Център", "Varna", "9000"), new ArrayList<>(),
                new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("Графа в София - 3", "GRAFA TOUR"),
                new PerformanceDetails("NDK", "Zala 1", SubEventDateCreator.subEventDateCreator(39, 19, 30)),
                "Най-мащабното събитие в кариерата на българския топ изпълнител. София готова ли си?", 70.00,
                new UnrestrictedAddress("Ploshtad Bulgaria 1", "Sofia", "1000"), List.of("Grafa"),
                new ReviewScoreAndCount(0, 0)),
            new SubEventDTO(new PerformanceId("СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ ПЛОВДИВ", "СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ"),
                new PerformanceDetails("Античен театър", "Сцена", SubEventDateCreator.subEventDateCreator(69, 20, 30)),
                "Последен юбилейни концерти на група Сигнал за 45-та годишнина", 70.00,
                new UnrestrictedAddress("ул. Цар Ивайло 4", "Plovdiv", "4000"), List.of("СИГНАЛ"),
                new ReviewScoreAndCount(0, 0))
        };
        assertArrayEquals(expected, actual);
    }

    /**
     * Test when, send PATCH HTTP request to "/customers/{username}/wishlist/add"
     * with existing customer username and existing sub-event. Expecting
     * {@link HttpStatus#SC_NO_CONTENT} meaning that the sub-event was added
     * successfully.
     *
     * @param username
     *            The unique identifier of the customer to whom the wish list
     *            belongs.
     * @param performanceId
     *            The unique identifier of the sub-event that will be added to the
     *            wishlist.
     */
    @ParameterizedTest
    @MethodSource("addWishlistData")
    @Order(1)
    void testAddSubEventToWishlist(final String username, final PerformanceId performanceId) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);
        HTTPRequest
            .patchValidateStatus(CUSTOMER_WISHLIST_ADD_PATH, performanceId, validJwtForCustomer,
                                 HttpStatus.SC_NO_CONTENT, username);

    }

    /**
     * Test when, send PATCH HTTP request to "/customers/{username}/wishlist/add"
     * with existing customer username and existing sub-event. Expecting
     * {@link HttpStatus#SC_NOT_MODIFIED} meaning that the sub-event has been
     * already added in the wishlist.
     *
     * @param username
     *            The unique identifier of the customer to whom the wish list
     *            belongs.
     * @param performanceId
     *            The unique identifier of the sub-event that will be added to the
     *            wishlist.
     */
    @ParameterizedTest
    @MethodSource("addWishlistData")
    @Order(2)
    void testAddDuplicateSubEventToWishlist(final String username, final PerformanceId performanceId) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);
        HTTPRequest
            .patchValidateStatus(CUSTOMER_WISHLIST_ADD_PATH, performanceId, validJwtForCustomer,
                                 HttpStatus.SC_NOT_MODIFIED, username);

    }

    private static Stream<Arguments> addWishlistData() {
        return Stream
            .of(arguments("spongebob", new PerformanceId("Графа в София - 1", "GRAFA TOUR")),
                arguments("spongebob", new PerformanceId("Графа в София - 2", "GRAFA TOUR")),
                arguments("spongebob", new PerformanceId("Графа в София - 3", "GRAFA TOUR")),
                arguments("spongebob", new PerformanceId("Ромео и Жулиета", "Театрални вечери във Варна")));
    }

    /**
     * Test when, send PATCH HTTP request to "/customers/{username}/wishlist/remove"
     * with existing customer username and existing sub-event. Expecting
     * {@link HttpStatus#SC_NO_CONTENT} meaning that the sub-event was removed
     * successfully.
     *
     * @param username
     *            The unique identifier of the customer to whom the wish list
     *            belongs.
     * @param performanceId
     *            The unique identifier of the sub-event that will be added to the
     *            wishlist.
     */
    @ParameterizedTest
    @MethodSource("modifyWishlistData")
    @Order(3)
    void testRemoveAddedSubEventFromWishlist(final String username, final PerformanceId performanceId) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);
        HTTPRequest
            .patchValidateStatus(CUSTOMER_WISHLIST_REMOVE_PATH, performanceId, validJwtForCustomer,
                                 HttpStatus.SC_NO_CONTENT, username);

    }

    /**
     * Test when, send PATCH HTTP request to "/customers/{username}/wishlist/remove"
     * with existing customer username and existing sub-event. Expecting
     * {@link HttpStatus#SC_NOT_MODIFIED} meaning that the sub-event has already
     * been removed or not even been added in the customer's wishlist.
     *
     * @param username
     *            The unique identifier of the customer to whom the wish list
     *            belongs.
     * @param performanceId
     *            The unique identifier of the sub-event that will be removed from
     *            the wishlist.
     */
    @ParameterizedTest
    @MethodSource("modifyWishlistData")
    @Order(4)
    void testRemoveNonAddedSubEventFromWishlist(final String username, final PerformanceId performanceId) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);
        HTTPRequest
            .patchValidateStatus(CUSTOMER_WISHLIST_REMOVE_PATH, performanceId, validJwtForCustomer,
                                 HttpStatus.SC_NOT_MODIFIED, username);

    }

    private static Stream<Arguments> modifyWishlistData() {
        return Stream.of(arguments("spongebob", new PerformanceId("Ромео и Жулиета", "Театрални вечери във Варна")));
    }

    /**
     * Test when, send PATCH HTTP request to
     * "/customers/{username}/wishlist/remove/selected" with existing customer
     * username and existing sub-event. Expecting {@link HttpStatus#SC_NO_CONTENT}
     * meaning that one or many of the selected sub-events has been successfully
     * removed or not even been added in the customer's wishlist.
     *
     * @param username
     *            The unique identifier of the customer to whom the wish list
     *            belongs.
     * @param subEventIds
     *            The unique identifiers of the selected sub-event that will be
     *            removed from the wishlist.
     */
    @ParameterizedTest
    @MethodSource("removeSelectedFromWishlistData")
    @Order(5)
    void testRemoveSelectedSubEventsFromWishlist(final String username, final PerformanceId[] subEventIds) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);
        HTTPRequest
            .patchValidateStatus(CUSTOMER_WISHLIST_REMOVE_SELECTED_PATH, subEventIds, validJwtForCustomer,
                                 HttpStatus.SC_NO_CONTENT, username);
    }

    /**
     * Test when, send PATCH HTTP request to
     * "/customers/{username}/wishlist/remove/selected" with existing customer
     * username and existing sub-events. Expecting
     * {@link HttpStatus#SC_NOT_MODIFIED} meaning that the customer's wishlist is
     * not modified.
     *
     * @param username
     *            The unique identifier of the customer to whom the wish list
     *            belongs.
     * @param subEventIds
     *            The unique identifiers of the selected sub-event that will be
     *            removed from the wishlist.
     */
    @ParameterizedTest
    @MethodSource("removeSelectedFromWishlistData")
    @Order(6)
    void testRemoveSelectedNonAddedSubEventsFromWishlist(final String username, final PerformanceId[] subEventIds) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);
        HTTPRequest
            .patchValidateStatus(CUSTOMER_WISHLIST_REMOVE_SELECTED_PATH, subEventIds, validJwtForCustomer,
                                 HttpStatus.SC_NOT_MODIFIED, username);
    }

    private static Stream<Arguments> removeSelectedFromWishlistData() {
        return Stream.of(arguments("spongebob", new PerformanceId[] {
            new PerformanceId("Графа в София - 1", "GRAFA TOUR"), new PerformanceId("Графа в София - 2", "GRAFA TOUR"),
            new PerformanceId("Графа в София - 3", "GRAFA TOUR")
        }));
    }

    /**
     * Test when, send PATCH HTTP request to "/customers/{username}/wishlist/add"
     * with existing customer username and body parameters for non-existing
     * sub-event. Expecting {@link HttpStatus#SC_NOT_FOUND} and
     * {@code EntityErrorInfo} containing information about the error.
     *
     * @param username
     *            The unique identifier of the customer to whom the wish list
     *            belongs.
     * @param performanceId
     *            The unique identifier of the sub-event.
     */
    @ParameterizedTest
    @MethodSource("nonExistingSubEventData")
    void testAddNonExistingSubEventToWishlist(final String username, final PerformanceId performanceId) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);
        final TypeRef<Error<EntityErrorInfo<PerformanceId>>> typeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };
        final Error<EntityErrorInfo<PerformanceId>> actualData = HTTPRequest
            .patch(CUSTOMER_WISHLIST_ADD_PATH, performanceId, validJwtForCustomer, HttpStatus.SC_NOT_FOUND, typeHolder,
                   username);

        final Error<EntityErrorInfo<PerformanceId>> expectedData = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("SubEvent", performanceId));
        assertEquals(expectedData, actualData);
    }

    private static Stream<Arguments> nonExistingSubEventData() {
        return Stream
            .of(arguments("bobo", new PerformanceId("Non-existing sub-event name", "Non-existing event name")));
    }

    /**
     * Test when, send PATCH HTTP request to "/customers/{username}/wishlist/add"
     * with existing customer username and invalid body parameters for sub-event.
     * Expecting {@link HttpStatus#SC_BAD_REQUEST} and {@code EntityErrorInfo}
     * containing information about the error.
     *
     * @param username
     *            The unique identifier of the customer to whom the wish list
     *            belongs.
     * @param performanceId
     *            The unique identifier of the sub-event.
     * @param expectedErrors
     *            The errors that are expected to be be returned in the response.
     */
    @ParameterizedTest
    @MethodSource("invalidData")
    void testAddInvalidSubEventToWishlist(final String username, final PerformanceId performanceId,
                                          final Collection<ValidationError<String>> expectedErrors) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);

        final TypeRef<Error<Collection<ValidationError<String>>>> typeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };
        final Error<Collection<ValidationError<String>>> actualData = HTTPRequest
            .patch(CUSTOMER_WISHLIST_ADD_PATH, performanceId, validJwtForCustomer, HttpStatus.SC_BAD_REQUEST,
                   typeHolder, username);

        final Error<Collection<ValidationError<String>>> expectedData = new Error<>(ErrorType.VALIDATION_ERROR,
            expectedErrors);
        assertEquals(expectedData, actualData);
    }

    /**
     * Test when, send PATCH HTTP request to "/customers/{username}/wishlist/remove"
     * with existing customer username and invalid body parameters for sub-event.
     * Expecting {@link HttpStatus#SC_BAD_REQUEST} and {@code EntityErrorInfo}
     * containing information about the error.
     *
     * @param username
     *            The unique identifier of the customer to whom the wish list
     *            belongs.
     * @param performanceId
     *            The unique identifier of the sub-event.
     * @param expectedErrors
     *            The errors that are expected to be be returned in the response.
     */
    @ParameterizedTest
    @MethodSource("invalidData")
    void testRemoveInvalidSubEventToWishlist(final String username, final PerformanceId performanceId,
                                             final Collection<ValidationError<String>> expectedErrors) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);

        final TypeRef<Error<Collection<ValidationError<String>>>> typeHolder = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when deserializing a response.
        };
        final Error<Collection<ValidationError<String>>> actualData = HTTPRequest
            .patch(CUSTOMER_WISHLIST_REMOVE_PATH, performanceId, validJwtForCustomer, HttpStatus.SC_BAD_REQUEST,
                   typeHolder, username);

        final Error<Collection<ValidationError<String>>> expectedData = new Error<>(ErrorType.VALIDATION_ERROR,
            expectedErrors);
        assertEquals(expectedData, actualData);
    }

    private static Stream<Arguments> invalidData() {
        final String invalidSubEventName = "Invalid sub-event ?>?}{?|";
        final String invalidEventName = "Invalid event name?>?}{?|";
        return Stream
            .of(arguments("bobo", new PerformanceId(invalidSubEventName, invalidEventName),
                          List
                              .of(new ValidationError<>(EntityFieldType.EVENT.toString(), invalidEventName,
                                  ValidationErrorType.INVALID_SYMBOLS.toString()),
                                  new ValidationError<>(EntityFieldType.SUB_EVENT.toString(), invalidSubEventName,
                                      ValidationErrorType.INVALID_SYMBOLS.toString()))));
    }

    /**
     * Test for HTTP GET request with URL "/customers/{username}/tickets". When
     * existing customer does not have any wishlist, response body should contain
     * empty {@link SubEventDTO} array and status code {@link HttpStatus#SC_OK 200}.
     */
    @Test
    void testFindWishlistReturnsEmptyList() {
        final String validJwtForCustomer = Jwt.createToken(emptyListsCustomer, ROLE_CUSTOMER);
        final SubEventDTO[] actual = HTTPRequest
            .get(CUSTOMER_WISHLIST_PATH, validJwtForCustomer, HttpStatus.SC_OK, SubEventDTO[].class,
                 emptyListsCustomer);

        assertEquals(0, actual.length);
    }

    /**
     * Test for HTTP GET request with URL "/customers/{username}/wishlist". When
     * {@literal non-existing} customer username is passed, response body should
     * contain {@link Error} object and status code {@link HttpStatus#SC_NOT_FOUND
     * 404}.
     *
     * @throws EntityNotFoundException
     *             with information displayed by {@code Error} and
     *             {@code EntityErrorInfo}
     */
    @Test
    void testFindWishListWithMissingCustomerThrows() {
        final TypeRef<Error<EntityErrorInfo<String>>> errorClass = new TypeRef<>() {
            // Anonymous class. Used to safe generic type when de-serializing a response.
        };
        final String validJwtForCustomer = Jwt.createToken(missingCustomer, ROLE_CUSTOMER);
        final Error<EntityErrorInfo<String>> actual = HTTPRequest
            .get(CUSTOMER_WISHLIST_PATH, validJwtForCustomer, HttpStatus.SC_NOT_FOUND, errorClass, missingCustomer);

        final Error<EntityErrorInfo<String>> expected = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("Customer", missingCustomer));
        assertEquals(expected, actual);
    }

    /**
     * Test when, send GET HTTP request to "/customers/{username}/reviews" with
     * existing customer username and not empty review list is passed, response body
     * should contain array of {@link ReviewDTO} objects and status code
     * {@link HttpStatus#SC_OK 200}.
     */
    @Test
    void testFindReviewReturnsListWithElements() {
        final String validJwtForCustomer = Jwt.createToken("bobo", ROLE_CUSTOMER);
        final ReviewDTO[] actual = HTTPRequest
            .get(CUSTOMER_REVIEWS_PATH, validJwtForCustomer, HttpStatus.SC_OK, ReviewDTO[].class, "bobo");

        final ReviewDTO[] expected = {
            new ReviewDTO("Mercury - Sofia", "Mercury World Tour", "NDK", "Zala 3",
                SubEventDateCreator.subEventDateCreator(-17, 19, 30), "WOW!!!", Integer.valueOf(5)),
            new ReviewDTO("My Satisfaction Sofia - 1", "ROLLING STONES TOUR", "NDK", "Zala 1",
                SubEventDateCreator.subEventDateCreator(-27, 20, 0),
                "Невероятен концерт беше!!! Тези два часа не ми стигнаха! Жалко малко за опашките на входа, едвам"
                    + " влезнах преди началото. Но това ще остане най-якия концерт на живота ми!!!",
                Integer.valueOf(4)),
            new ReviewDTO("Графа в София - 1", "GRAFA TOUR", "NDK", "Zala 3",
                SubEventDateCreator.subEventDateCreator(-26, 19, 30), null, Integer.valueOf(1)),
            new ReviewDTO("Графа в София - 2", "GRAFA TOUR", "NDK", "Zala 3",
                SubEventDateCreator.subEventDateCreator(-25, 19, 30), "Искаме още!!!", null),
            new ReviewDTO("Постановка на Хамлет - 1", "Хамлет в Народния Театър", "Народен театър \"Иван Вазов\"",
                "Голяма сцена", SubEventDateCreator.subEventDateCreator(-83, 19, 30), "Невероятна пиеса!", null),
            new ReviewDTO("Ромео и Жулиета", "Театрални вечери във Варна", "Драматичен театър - Стоян Бъчваров",
                "Главна зала", SubEventDateCreator.subEventDateCreator(-42, 20, 0), null, Integer.valueOf(1)),
            new ReviewDTO("СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ БУРГАС", "СИГНАЛ – 45 ГОДИНИ НЕ СТИГАТ", "Летен театър",
                "Сцена", SubEventDateCreator.subEventDateCreator(-56, 20, 30), "Още 45 така!!!", Integer.valueOf(4))
        };
        assertArrayEquals(expected, actual);
    }

    /**
     * Test for HTTP GET request with URL "/customers/{username}/tickets". When
     * existing customer does not have any reviews, response body should contain
     * empty {@link ReviewDTO} array and status code {@link HttpStatus#SC_OK 200}.
     */
    @Test
    void testFindReviewsReturnsEmptyList() {
        final String validJwtForCustomer = Jwt.createToken(emptyListsCustomer, ROLE_CUSTOMER);
        final String findReviewsForSpecificUserURL = CUSTOMER_REVIEWS_PATH;
        final ReviewDTO[] actual = HTTPRequest
            .get(findReviewsForSpecificUserURL, validJwtForCustomer, HttpStatus.SC_OK, ReviewDTO[].class,
                 emptyListsCustomer);

        assertEquals(0, actual.length);
    }

    /**
     * Test for HTTP GET request with URL "/customers/{username}/reviews". When
     * {@literal non-existing} customer username is passed, response body should
     * contain {@link Error} object and status code {@link HttpStatus#SC_NOT_FOUND
     * 404}.
     *
     * @throws EntityNotFoundException
     *             with information displayed by {@code Error} and
     *             {@code EntityErrorInfo}
     */
    @Test
    void testFindReviewsWithMissingCustomerThrows() {
        final TypeRef<Error<EntityErrorInfo<String>>> errorClass = new TypeRef<>() {
            // Anonymous class.
        };
        final String validJwtForCustomer = Jwt.createToken(missingCustomer, ROLE_CUSTOMER);
        final Error<EntityErrorInfo<String>> actual = HTTPRequest
            .get(CUSTOMER_REVIEWS_PATH, validJwtForCustomer, HttpStatus.SC_NOT_FOUND, errorClass, missingCustomer);

        final Error<EntityErrorInfo<String>> expected = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("Customer", missingCustomer));
        assertEquals(expected, actual);
    }

    /**
     * Testing when searching user profile by URL with different username than the
     * username in JWT token throws {@code UnauthorizedException}.
     */
    @Test
    void testAccessingDifferentCustomerAcountThrows() {
        final TypeRef<Error<UnauthorizedExceptionInfo>> errorClass = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final String validJwtForCustomer = Jwt.createToken("bobo", ROLE_CUSTOMER);
        final Error<UnauthorizedExceptionInfo> actual = HTTPRequest
            .get(CUSTOMER_REVIEWS_PATH, validJwtForCustomer, HttpStatus.SC_UNAUTHORIZED, errorClass, "ticketHunter");

        final Error<UnauthorizedExceptionInfo> expected = new Error<>(ErrorType.UNAUTHORIZED,
            new UnauthorizedExceptionInfo("Unauthorized User!"));
        assertEquals(expected, actual);
    }

    /**
     * Testing when accessing URL by an user with role different than required,
     * throws {@code ForbiddenAccessException}.
     */
    @Test
    void testAccessingEndpointWithDifferentRoleThrows() {
        final TypeRef<Error<ForbiddenAccessExceptionInfo>> errorClass = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final String validJwtForCustomer = Jwt.createToken("bobo", ROLE_CUSTOMER);
        final Error<ForbiddenAccessExceptionInfo> actual = HTTPRequest
            .get(PATH_FOR_ADMINS_ONLY, validJwtForCustomer, HttpStatus.SC_FORBIDDEN, errorClass);

        final Error<ForbiddenAccessExceptionInfo> expected = new Error<>(ErrorType.FORBIDDEN,
            new ForbiddenAccessExceptionInfo("User not allowed!"));
        assertEquals(expected, actual);
    }

    private static Stream<Arguments> validArgumentsData() {
        final String username = "bobo";
        final PersonName personName = new PersonName("Boris", "Spaski");
        final UnrestrictedAddress address = new UnrestrictedAddress("ul. Aleko Konstantinov", "Plovdiv", "4000");
        final String email = "Spaski@dxc.com";
        final PhoneDTO phoneNumber = new PhoneDTO("+40", "987654321");
        final LocalDate birthDate = LocalDate.of(1945, 1, 15);

        final String username2 = "ticketHunter";
        final PersonName personName2 = new PersonName("George", "Stefanov");
        final UnrestrictedAddress address2 = new UnrestrictedAddress("ul. Stefan Dimitrov 7", "Sofia", "1000");
        final String email2 = "Stefanov@dxc.com";
        final PhoneDTO phoneNumber2 = new PhoneDTO("+359", "987654322");
        final LocalDate birthDate2 = null;

        return Stream
            .of(arguments(new CustomerDTO(username, personName, email, phoneNumber, address, birthDate)),
                arguments(new CustomerDTO(username2, personName2, email2, phoneNumber2, address2, birthDate2)));
    }

    /**
     * Test, when send POST HTTP Request to "/offices" returns the newly created
     * Office with status code 201.
     */
    @Test
    void createOrderShouldCreateNewOrder() {
        final PerformanceId performanceId = new PerformanceId("Постановка на Хамлет - 1", "Хамлет в Народния Театър");
        final String performanceDescription = "Режисьор - Антон Угринов; Превод - Сава Драгунчев; Сценография и костюми - Елис Вели; Музика - Калин Николов. Най-класическата пиеса, в най-класическия театър на столицата";
        final PerformanceDetails performanceDetails = new PerformanceDetails("Народен театър \"Иван Вазов\"",
            "Голяма сцена", SubEventDateCreator.subEventDateCreator(-83, 19, 30));
        final double ticketWithoutDiscount = 40;
        final double ticketWithDiscount = 32;

        final TicketDTO ticketDTO = new TicketDTO(6, performanceId, performanceDescription, performanceDetails,
            "Сектор 7", 3, new SeatDTO(158, "ACQUIRED"), ticketWithDiscount, new DiscountDTO("KID", 20));

        final OrderDTO expected = new OrderDTO("COMPLETED", "sa60", new PersonName("Александър", "Стоянов"),
            "BANK TRANSFER", Instant.now(), "HOME_DELIVERY", new UnrestrictedAddress("SomeStreet", "SomeCity", "1000"),
            List.of(new TicketFromOrderDTO(ticketDTO, ticketWithoutDiscount)));

        final OrderWithTicketsDTO orderWithTicketsDTO = new OrderWithTicketsDTO("BANK TRANSFER", "HOME_DELIVERY",
            new UnrestrictedAddress("SomeStreet", "SomeCity", "1000"),
            List
                .of(new TicketForOrderDTO("Хамлет в Народния Театър", "Постановка на Хамлет - 1",
                    new PerformanceDetails("Народен театър \"Иван Вазов\"", "Голяма сцена",
                        SubEventDateCreator.subEventDateCreator(-83, 19, 30)),
                    "Сектор 7", 3, 158, "KID")));

        final OrderDTO actual = HTTPRequest
            .post(ORDER_PATH, orderWithTicketsDTO, VALID_JWT_FOR_sa60_CUSTOMER, HttpStatus.SC_CREATED, OrderDTO.class,
                  "sa60");
        Assertions.assertEquals(expected.purchaseStatus(), actual.purchaseStatus());
        Assertions.assertEquals(expected.username(), actual.username());
        Assertions.assertEquals(expected.personName(), actual.personName());
        Assertions.assertEquals(expected.unrestrictedAddress(), actual.unrestrictedAddress());
        Assertions.assertEquals(expected.paymentType(), actual.paymentType());
        Assertions.assertEquals(expected.deliveryType(), actual.deliveryType());
        Assertions.assertEquals(expected.tickets(), actual.tickets());

    }
}
