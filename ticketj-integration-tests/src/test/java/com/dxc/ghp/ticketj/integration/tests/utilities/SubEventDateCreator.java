package com.dxc.ghp.ticketj.integration.tests.utilities;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * A utility class for create subevent date operations.
 */
public final class SubEventDateCreator {

    private SubEventDateCreator() {
        // Prevent instantiation of the class.
    }

    /**
     * Creates a new Instant representing a sub-event date by adding the specified
     * number of days, hours, and minutes to the current date and time.
     *
     * @param days
     *            the number of days to add.
     * @param hours
     *            the number of days to add.
     * @param minutes
     *            the number of days to add.
     * @return a new Instant representing the sub-event date.
     */
    public static Instant subEventDateCreator(final int days, final int hours, final int minutes) {
        return Instant
            .now()
            .truncatedTo(ChronoUnit.DAYS)
            .plus(days, ChronoUnit.DAYS)
            .plus(hours, ChronoUnit.HOURS)
            .plus(minutes, ChronoUnit.MINUTES);
    }

}
