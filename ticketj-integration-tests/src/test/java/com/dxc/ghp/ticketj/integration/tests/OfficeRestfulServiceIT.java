package com.dxc.ghp.ticketj.integration.tests;

import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.LocalTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.OfficeDTO;
import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.integration.tests.config.RestAssuredConfiguration;
import com.dxc.ghp.ticketj.integration.tests.requests.HTTPRequest;
import com.dxc.ghp.ticketj.integration.tests.utilities.Jwt;
import com.dxc.ghp.ticketj.misc.exceptions.DuplicateFieldInfo;
import com.dxc.ghp.ticketj.misc.exceptions.EmploymentInfo;
import com.dxc.ghp.ticketj.misc.exceptions.EntityErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.Error;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.types.EntityFieldType;
import com.dxc.ghp.ticketj.misc.types.OfficeLocationDetails;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;
import com.dxc.ghp.ticketj.misc.types.ValidationError;
import com.dxc.ghp.ticketj.misc.types.ValidationErrorType;

import io.restassured.common.mapper.TypeRef;

/**
 * Integration tests for Office Restful service.
 */
@SuppressWarnings({
    "static-method", "nls"
})
@TestMethodOrder(OrderAnnotation.class)
public final class OfficeRestfulServiceIT extends RestAssuredConfiguration {

    private static final String OFFICES_PATH = "/offices";
    private static final String SPECIFIC_OFFICE_PATH = "/offices/{officeName}";
    private static final UserRole ROLE_ADMIN = UserRole.ADMIN;
    private static String VALID_JWT_FOR_ADMIN = Jwt.createToken("Test Admin", ROLE_ADMIN);
    private static final UserRole ROLE_CUSTOMER = UserRole.CUSTOMER;
    private static String VALID_JWT_FOR_CUSTOMER = Jwt.createToken("Test Customer", ROLE_CUSTOMER);

    /**
     * Tests, when send GET HTTP Request to "/offices" returns all offices with
     * status code 200.
     */
    @Test
    @Order(1)
    void findAllOfficesReturnCorrectData() {
        final OfficeDTO[] actual = HTTPRequest
            .get(OFFICES_PATH, VALID_JWT_FOR_CUSTOMER, HttpStatus.SC_OK, OfficeDTO[].class);

        final OfficeDTO[] expected = {
            new OfficeDTO("TicketJ 100", new UnrestrictedAddress("ul. Todor Kableshkov, 8", "Sofia", "1000"),
                new PhoneDTO("+359", "884563489"), "office_100@ticketj.com",
                new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of("Mario")),
            new OfficeDTO("TicketJ 105", new UnrestrictedAddress("ul. General Gurko", "Plovdiv", "4000"),
                new PhoneDTO("+359", "897645310"), "office_105@ticketj.com",
                new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of("renter")),
            new OfficeDTO("TicketJ 111", new UnrestrictedAddress("ul. Geo Milev", "Sofia", "1000"),
                new PhoneDTO("+359", "898125279"), "office_111@ticketj.com",
                new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of()),
            new OfficeDTO("TicketJ 22", new UnrestrictedAddress("ул. Георги Райчев", "Stara Zagora", "6004"),
                new PhoneDTO("+359", "897123475"), "office_22@ticketj.com",
                new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of()),
            new OfficeDTO("TicketJ 33", new UnrestrictedAddress("ул. Мария Луиза", "Varna", "9000"),
                new PhoneDTO("+359", "895348693"), "office_33@ticketj.com",
                new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of()),
            new OfficeDTO("TicketJ 34", new UnrestrictedAddress("ул. Поп Харитон", "Montana", "3400"),
                new PhoneDTO("+359", "877124502"), "office_34@ticketj.com",
                new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of()),
            new OfficeDTO("TicketJ 83", new UnrestrictedAddress("ul. Antim I, 30", "Burgas", "8000"),
                new PhoneDTO("+359", "888234567"), "office_83@ticketj.com",
                new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of()),

        };
        Assertions.assertArrayEquals(expected, actual);
    }

    /**
     * Test, when send POST HTTP Request to "/offices" returns the newly created
     * Office with status code 201.
     */
    @Test
    void createOfficeShouldCreateNewOffice() {
        final OfficeDTO expected = new OfficeDTO("TicketJ 000",
            new UnrestrictedAddress("Mladost, bl. 234, str. 12", "Sofia", "1000"), new PhoneDTO("+359", "886756432"),
            "office_000@ticketj.com", new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of());

        final OfficeDTO actual = HTTPRequest
            .post(OFFICES_PATH, expected, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CREATED, OfficeDTO.class);
        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send POST HTTP Request to "/offices" returns an Error record with
     * status code 400.
     *
     * @param requestBody
     *            The request body. Must not be null.
     * @param expected
     *            The expected response body. Must not be null.
     */
    @ParameterizedTest
    @MethodSource("badRequestTestData")
    void createOfficeShouldReturnErrorWhenValidationFails(final OfficeDTO requestBody,
                                                          final Error<Collection<ValidationError<String>>> expected) {
        final TypeRef<Error<Collection<ValidationError<String>>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final Error<Collection<ValidationError<String>>> actual = HTTPRequest
            .post(OFFICES_PATH, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_BAD_REQUEST, typeRef);
        Assertions.assertEquals(expected, actual);
    }

    private static Stream<Arguments> badRequestTestData() {
        return Stream
            .of(arguments(new OfficeDTO("Test", new UnrestrictedAddress("Mladost, bl. 234, str. 12", "Sofia", "1000"),
                new PhoneDTO("+359", "886756432"), "office_000@ticketj.com",
                new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of()),
                          new Error<>(ErrorType.VALIDATION_ERROR,
                              List
                                  .of(new ValidationError<>(EntityFieldType.NAME.toString(), "Test",
                                      ValidationErrorType.INVALID_LENGTH.toString()))),
                          arguments(new OfficeDTO("Test",
                              new UnrestrictedAddress("Mladost, bl. 234, str. 12", "Sofia", "1000"),
                              new PhoneDTO("+359", "886756432"), "@",
                              new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of()),
                                    new Error<>(ErrorType.VALIDATION_ERROR,
                                        List
                                            .of(new ValidationError<>(EntityFieldType.NAME.toString(), "Test",
                                                ValidationErrorType.INVALID_LENGTH.toString()),
                                                new ValidationError<>(EntityFieldType.EMAIL.toString(), "@",
                                                    ValidationErrorType.INVALID_LENGTH.toString()),
                                                new ValidationError<>(EntityFieldType.EMAIL.toString(), "@",
                                                    ValidationErrorType.INVALID_SYMBOLS.toString()))))));
    }

    /**
     * Test, when send POST HTTP Request to "/offices" returns an Error record with
     * status code 409 when office id is not unique.
     */
    @Test
    void createOfficeShouldReturnErrorWhenEntityExists() {
        final Error<EntityErrorInfo<String>> expected = new Error<>(ErrorType.ENTITY_EXISTS,
            new EntityErrorInfo<>("Office", "TicketJ 100"));

        final TypeRef<Error<EntityErrorInfo<String>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO duplicateOffice = new OfficeDTO("TicketJ 100",
            new UnrestrictedAddress("Test street", "Sofia", "1000"), new PhoneDTO("+359", "284563489"),
            "test_email@email.com", new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of());
        final Error<EntityErrorInfo<String>> actual = HTTPRequest
            .post(OFFICES_PATH, duplicateOffice, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef);
        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send POST HTTP Request to "/offices" returns an Error record with
     * status code 404 when city does not exist.
     */
    @Test
    void createOfficeShouldReturnErrorWhenCityDoesNotExist() {
        final Error<EntityErrorInfo<String>> expected = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("City", "Peshtera"));

        final TypeRef<Error<EntityErrorInfo<String>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO duplicateOffice = new OfficeDTO("TicketJ 100",
            new UnrestrictedAddress("Test street", "Peshtera", "1000"), new PhoneDTO("+359", "284563489"),
            "test_email@email.com", new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of());
        final Error<EntityErrorInfo<String>> actual = HTTPRequest
            .post(OFFICES_PATH, duplicateOffice, VALID_JWT_FOR_ADMIN, HttpStatus.SC_NOT_FOUND, typeRef);
        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send POST HTTP Request to "/offices" returns an Error record with
     * status code 404 when phone code does not exist.
     */
    @Test
    void createOfficeShouldReturnErrorWhenCodeDoesNotExist() {
        final Error<EntityErrorInfo<String>> expected = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("PhoneCode", "+80"));

        final TypeRef<Error<EntityErrorInfo<String>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO duplicateOffice = new OfficeDTO("TicketJ 100",
            new UnrestrictedAddress("Test street", "Sofia", "1000"), new PhoneDTO("+80", "284563489"),
            "test_email@email.com", new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of());
        final Error<EntityErrorInfo<String>> actual = HTTPRequest
            .post(OFFICES_PATH, duplicateOffice, VALID_JWT_FOR_ADMIN, HttpStatus.SC_NOT_FOUND, typeRef);
        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send POST HTTP Request to "/offices" returns an Error record with
     * status code 409 when email is not unique.
     */
    @Test
    void createOfficeShouldReturnErrorWhenEmailExists() {
        final Error<DuplicateFieldInfo<String>> expected = new Error<>(ErrorType.DUPLICATE_FIELD,
            new DuplicateFieldInfo<>("Office", EntityFieldType.EMAIL, "office_34@ticketj.com"));

        final TypeRef<Error<DuplicateFieldInfo<String>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO duplicateOffice = new OfficeDTO("TicketJ 101",
            new UnrestrictedAddress("Test street", "Sofia", "1000"), new PhoneDTO("+359", "284563489"),
            "office_34@ticketj.com", new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of());
        final Error<DuplicateFieldInfo<String>> actual = HTTPRequest
            .post(OFFICES_PATH, duplicateOffice, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef);

        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send POST HTTP Request to "/offices" returns an Error record with
     * status code 409 when office city and street is not unique.
     */
    @Test
    void createOfficeShouldReturnErrorWhenLocationExists() {
        final Error<DuplicateFieldInfo<OfficeLocationDetails>> expected = new Error<>(ErrorType.DUPLICATE_FIELD,
            new DuplicateFieldInfo<>("Office", EntityFieldType.ADDRESS,
                new OfficeLocationDetails("Sofia", "ul. Todor Kableshkov, 8")));

        final TypeRef<Error<DuplicateFieldInfo<OfficeLocationDetails>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO duplicateOffice = new OfficeDTO("TicketJ 101",
            new UnrestrictedAddress("ul. Todor Kableshkov, 8", "Sofia", "1000"), new PhoneDTO("+359", "284563489"),
            "test_email@email.com", new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of());
        final Error<DuplicateFieldInfo<OfficeLocationDetails>> actual = HTTPRequest
            .post(OFFICES_PATH, duplicateOffice, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef);

        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send POST HTTP Request to "/offices" returns an Error record with
     * status code 409 when office phone code and phone number is not unique.
     */
    @Test
    void createOfficeShouldReturnErrorWhenPhoneExists() {
        final Error<DuplicateFieldInfo<PhoneDTO>> expected = new Error<>(ErrorType.DUPLICATE_FIELD,
            new DuplicateFieldInfo<>("Office", EntityFieldType.PHONE, new PhoneDTO("+359", "884563489")));

        final TypeRef<Error<DuplicateFieldInfo<PhoneDTO>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO duplicateOffice = new OfficeDTO("TicketJ 101",
            new UnrestrictedAddress("Test street", "Sofia", "1000"), new PhoneDTO("+359", "884563489"),
            "test_email@email.com", new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of());
        final Error<DuplicateFieldInfo<PhoneDTO>> actual = HTTPRequest
            .post(OFFICES_PATH, duplicateOffice, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef);

        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send PATCH HTTP Request to "/offices/{officeName}" returns an
     * modified office with status code 200.
     *
     * @param requestBody
     *            The request body containing only info to update.
     * @param expected
     *            The response body that is expected to be returned.
     */
    @ParameterizedTest
    @MethodSource("modifyOfficeTestData")
    void modifyOfficeShouldChangeOnlyNonNullFields(final OfficeDTO requestBody, final OfficeDTO expected) {
        final OfficeDTO actual = HTTPRequest
            .patch(SPECIFIC_OFFICE_PATH, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_OK, OfficeDTO.class,
                   "TicketJ 100");

        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send PATCH HTTP Request to "/offices/{officeName}" returns an
     * Error record with status code 409 when email is not unique.
     */
    @Test
    void modifyOfficeShouldReturnErrorWhenDuplicateEmail() {
        final Error<DuplicateFieldInfo<String>> expected = new Error<>(ErrorType.DUPLICATE_FIELD,
            new DuplicateFieldInfo<>("Office", EntityFieldType.EMAIL, "office_100@ticketj.com"));

        final TypeRef<Error<DuplicateFieldInfo<String>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO duplicateOffice = new OfficeDTO(null, null, null, "office_100@ticketj.com", null, List.of());
        final Error<DuplicateFieldInfo<String>> actual = HTTPRequest
            .patch(SPECIFIC_OFFICE_PATH, duplicateOffice, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef,
                   "TicketJ 83");

        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send PATCH HTTP Request to "/offices/{officeName}" returns an
     * Error record with status code 409 when phone is not unique.
     */
    @Test
    void modifyOfficeShouldReturnErrorWhenDuplicatePhone() {
        final Error<DuplicateFieldInfo<PhoneDTO>> expected = new Error<>(ErrorType.DUPLICATE_FIELD,
            new DuplicateFieldInfo<>("Office", EntityFieldType.PHONE, new PhoneDTO("+359", "877124502")));

        final TypeRef<Error<DuplicateFieldInfo<PhoneDTO>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO duplicateOffice = new OfficeDTO(null, null, new PhoneDTO("+359", "877124502"), null, null,
            List.of());
        final Error<DuplicateFieldInfo<PhoneDTO>> actual = HTTPRequest
            .patch(SPECIFIC_OFFICE_PATH, duplicateOffice, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef,
                   "TicketJ 83");
        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send PATCH HTTP Request to "/offices/{officeName}" returns an
     * Error record with status code 409 when street and city is not unique.
     */
    @Test
    void modifyOfficeShouldReturnErrorWhenDuplicateStreetAndCity() {
        final String duplicateStreet = "ул. Поп Харитон";
        final String duplicateCity = "Montana";
        final Error<DuplicateFieldInfo<OfficeLocationDetails>> expected = new Error<>(ErrorType.DUPLICATE_FIELD,
            new DuplicateFieldInfo<>("Office", EntityFieldType.ADDRESS,
                new OfficeLocationDetails(duplicateCity, duplicateStreet)));

        final TypeRef<Error<DuplicateFieldInfo<OfficeLocationDetails>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO duplicateOffice = new OfficeDTO(null,
            new UnrestrictedAddress(duplicateStreet, duplicateCity, "8469"), null, null, null, List.of());
        final Error<DuplicateFieldInfo<OfficeLocationDetails>> actual = HTTPRequest
            .patch(SPECIFIC_OFFICE_PATH, duplicateOffice, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef,
                   "TicketJ 83");

        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send PATCH HTTP Request to "/offices/{officeName}" returns an
     * Error record with status code 409 when trying to assign an office worker who
     * is already assigned to a different office.
     */
    @Test
    void modifyOfficeShouldReturnErrorWhenWorkerIsAlreadyAssigned() {
        final String alreadyAssigned = "Mario";
        final Error<EmploymentInfo> expected = new Error<>(ErrorType.ALREADY_EMPOYED,
            new EmploymentInfo("TicketJ 100", alreadyAssigned));

        final TypeRef<Error<EmploymentInfo>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO requestBody = new OfficeDTO(null, null, null, null, null, List.of(alreadyAssigned));
        final Error<EmploymentInfo> actual = HTTPRequest
            .patch(SPECIFIC_OFFICE_PATH, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_CONFLICT, typeRef,
                   "TicketJ 83");

        Assertions.assertEquals(expected, actual);
    }

    /**
     * Test, when send PATCH HTTP Request to "/offices/{officeName}" returns an
     * Error record with status code 409 when trying to assign an office worker who
     * is already assigned to a different office.
     */
    @Test
    void modifyOfficeShouldReturnErrorWhenWorkerIsNonExistent() {
        final String invalidWorker = "Ivan";
        final Error<EntityErrorInfo<String>> expected = new Error<>(ErrorType.ENTITY_NOT_FOUND,
            new EntityErrorInfo<>("OfficeWorker", invalidWorker));

        final TypeRef<Error<EntityErrorInfo<String>>> typeRef = new TypeRef<>() {
            // Anonymous class. Used to hold generic type.
        };
        final OfficeDTO requestBody = new OfficeDTO(null, null, null, null, null, List.of(invalidWorker));
        final Error<EntityErrorInfo<String>> actual = HTTPRequest
            .patch(SPECIFIC_OFFICE_PATH, requestBody, VALID_JWT_FOR_ADMIN, HttpStatus.SC_NOT_FOUND, typeRef,
                   "TicketJ 83");

        Assertions.assertEquals(expected, actual);
    }

    private static Stream<Arguments> modifyOfficeTestData() {
        return Stream
            .of(arguments(new OfficeDTO(null, null, null, null, null, null),
                          new OfficeDTO("TicketJ 100",
                              new UnrestrictedAddress("ul. Todor Kableshkov, 8", "Sofia", "1000"),
                              new PhoneDTO("+359", "884563489"), "office_100@ticketj.com",
                              new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of("Mario"))),
                arguments(new OfficeDTO(null, null, null, "newEmail@email.com", null, List.of("Mario")),
                          new OfficeDTO("TicketJ 100",
                              new UnrestrictedAddress("ul. Todor Kableshkov, 8", "Sofia", "1000"),
                              new PhoneDTO("+359", "884563489"), "newEmail@email.com",
                              new Period<>(LocalTime.parse("09:00"), LocalTime.parse("18:00")), List.of("Mario"))),
                arguments(new OfficeDTO(null, new UnrestrictedAddress("Test new street", null, "1234"), null, null,
                    new Period<>(null, LocalTime.parse("16:00")), null),
                          new OfficeDTO("TicketJ 100", new UnrestrictedAddress("Test new street", "Sofia", "1234"),
                              new PhoneDTO("+359", "884563489"), "newEmail@email.com",
                              new Period<>(LocalTime.parse("09:00"), LocalTime.parse("16:00")), List.of("Mario"))),
                arguments(new OfficeDTO(null, new UnrestrictedAddress(null, null, "6000"),
                    new PhoneDTO(null, "888234542"), null,
                    new Period<>(LocalTime.parse("08:30"), LocalTime.parse("16:00")), List.of()),
                          new OfficeDTO("TicketJ 100", new UnrestrictedAddress("Test new street", "Sofia", "6000"),
                              new PhoneDTO("+359", "888234542"), "newEmail@email.com",
                              new Period<>(LocalTime.parse("08:30"), LocalTime.parse("16:00")), List.of())),
                arguments(new OfficeDTO("Test Office",
                    new UnrestrictedAddress("ul. Todor Kableshkov, 8", "Plovdiv", "4000"),
                    new PhoneDTO("+40", "880063489"), "office_100@ticketj.com",
                    new Period<>(LocalTime.parse("12:30"), LocalTime.parse("15:30")), List.of("Mario")),
                          new OfficeDTO("TicketJ 100",
                              new UnrestrictedAddress("ul. Todor Kableshkov, 8", "Plovdiv", "4000"),
                              new PhoneDTO("+40", "880063489"), "office_100@ticketj.com",
                              new Period<>(LocalTime.parse("12:30"), LocalTime.parse("15:30")), List.of("Mario"))));
    }
}
