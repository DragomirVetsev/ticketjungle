package com.dxc.ghp.ticketj.integration.tests;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.dxc.ghp.ticketj.integration.tests.config.RestAssuredConfiguration;
import com.dxc.ghp.ticketj.integration.tests.requests.HTTPRequest;
import com.dxc.ghp.ticketj.integration.tests.utilities.Jwt;
import com.dxc.ghp.ticketj.misc.types.UserRole;

/**
 * Integration tests for Office Worker Restful service
 */
public final class OfficeWorkerRestfulServiceIT extends RestAssuredConfiguration {
    private static final UserRole ROLE_ADMIN = UserRole.ADMIN;
    @SuppressWarnings("nls")
    private static String VALID_JWT_FOR_ADMIN = Jwt.createToken("Test Admin", ROLE_ADMIN);

    /**
     * Test, when send GET HTTP Request to "/available-workers" returns all
     * available workers with status code 200.
     */
    @SuppressWarnings({
        "nls", "static-method"
    })
    @Test
    void findAllAvailableWorkersShouldReturnCorrentData() {
        final String[] actual = HTTPRequest
            .get("office-workers/available", VALID_JWT_FOR_ADMIN, HttpStatus.SC_OK, String[].class);

        final String[] expected = {
            "aIvanov", "jane_doe", "sandov"
        };
        Assertions.assertArrayEquals(expected, actual);
    }
}
