package com.dxc.ghp.ticketj.integration.tests.utilities;

import com.dxc.ghp.ticketj.dto.UserInfoDTO;
import com.dxc.ghp.ticketj.dto.UserJwtDTO;
import com.dxc.ghp.ticketj.jwt.service.JwtService;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UserRole;

/**
 * Service for creation of JWT token used by integration tests.
 */
public final class Jwt {

    private Jwt() {
        // Utility class, no instantiation.
    }

    /**
     * @param username
     *            is the user who what`s to obtain a JWT token.
     * @param role
     *            is authority based on user`s role
     * @return newly created JWT token.
     */
    @SuppressWarnings("nls")
    public static String createToken(final String username, final UserRole role) {
        final PersonName userFullName = new PersonName("givenName", "lastName");
        final UserInfoDTO userInfo = new UserInfoDTO(username, userFullName, role);
        final UserJwtDTO userWithToken = JwtService.generateUserWithToken(userInfo);
        return userWithToken.jwtToken();
    }

}
