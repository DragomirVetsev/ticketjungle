package com.dxc.ghp.ticketj.integration.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.time.LocalDate;
import java.util.stream.Stream;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.dxc.ghp.ticketj.dto.CustomerDTO;
import com.dxc.ghp.ticketj.dto.CustomerWithCredentialsDTO;
import com.dxc.ghp.ticketj.dto.PhoneDTO;
import com.dxc.ghp.ticketj.dto.UserDTO;
import com.dxc.ghp.ticketj.integration.tests.config.RestAssuredConfiguration;
import com.dxc.ghp.ticketj.integration.tests.requests.HTTPRequest;
import com.dxc.ghp.ticketj.integration.tests.utilities.Jwt;
import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.misc.types.PersonName;
import com.dxc.ghp.ticketj.misc.types.UnrestrictedAddress;
import com.dxc.ghp.ticketj.misc.types.UserRole;

/**
 * Integration test for User Restful service.
 */
@SuppressWarnings({
    "nls", "static-method"
})
@TestMethodOrder(OrderAnnotation.class)
public class UserRestfulServiceIT extends RestAssuredConfiguration {

    private static final UserRole ROLE_ADMIN = UserRole.ADMIN;
    private static final UserRole ROLE_CUSTOMER = UserRole.CUSTOMER;
    private static final String VALID_JWT_FOR_ADMIN = Jwt.createToken("Test Admin", ROLE_ADMIN);
    private static final String CUSTOMERS_PATH = "/customers";
    private static final String SPECIFIC_CUSTOMER_PATH = CUSTOMERS_PATH + "/{username}";
    private static final String REGISTER_CUSTOMER_PATH = CUSTOMERS_PATH + "/register";

    /**
     * Test, when send GET HTTP Request to "/users" returns all users with status
     * code 200.
     */

    @Test
    @Order(1)
    void findAllUsersReturnCorrectData() {
        final String findAllUsersURL = "/users";
        final UserDTO[] actual = HTTPRequest
            .get(findAllUsersURL, VALID_JWT_FOR_ADMIN, HttpStatus.SC_OK, UserDTO[].class);

        final UserDTO[] expected = {
            new UserDTO("Mario", new PersonName("Marina", "Petrova"), "marina90.@abv.bg",
                new PhoneDTO("+359", "899565542"), new UnrestrictedAddress("Stambolijski str. 2", "Сmolyan", "4700"),
                UserRole.OFFICE_WORKER),
            new UserDTO("Neo", new PersonName("Ivan", "Ivanov"), "ivan.ivanov.@dxc.com",
                new PhoneDTO("+359", "898989898"), new UnrestrictedAddress("Teatralna str. 15", "Plovdiv", "4000"),
                UserRole.ADMIN),
            new UserDTO("aIvanov", new PersonName("Anton", "Ivanov"), "ivanoc@dxc.com",
                new PhoneDTO("+359", "877380303"), new UnrestrictedAddress("ul. Knyaz Boris 1", "Varna", "9000"),
                UserRole.OFFICE_WORKER),
            new UserDTO("bobo", new PersonName("Boris", "Spaski"), "Spaski@dxc.com", new PhoneDTO("+40", "987654321"),
                new UnrestrictedAddress("ul. Aleko Konstantinov", "Plovdiv", "4000"), UserRole.CUSTOMER),
            new UserDTO("giliev", new PersonName("Georgi", "Iliev"), "georgi.iliev3@dxc.com",
                new PhoneDTO("+40", "887565543"), new UnrestrictedAddress("ul. Boris Drangov 32", "Burgas", "8000"),
                UserRole.CUSTOMER),
            new UserDTO("jane_doe", new PersonName("Иванa", "Петровa"), "jane.doe@example.bg",
                new PhoneDTO("+359", "888115642"), new UnrestrictedAddress("бул. Витоша 1", "София", "1000"),
                UserRole.OFFICE_WORKER),
            new UserDTO("john.doe", new PersonName("Иван", "Петров"), "john.doe@example.bg",
                new PhoneDTO("+359", "888111222"), new UnrestrictedAddress("бул. Витоша 1", "София", "1000"),
                UserRole.CUSTOMER),
            new UserDTO("laura", new PersonName("Лаура", "Маринова"), "laura@example.bg",
                new PhoneDTO("+359", "878333444"), new UnrestrictedAddress("ул. Княз Борис 12", "Варна", "9000"),
                UserRole.CUSTOMER),
            new UserDTO("peter.pan", new PersonName("Петър", "Ангелов"), "peter.pan@example.bg",
                new PhoneDTO("+359", "888444555"), new UnrestrictedAddress("ул. Александровска 8", "Бургас", "8000"),
                UserRole.CUSTOMER),
            new UserDTO("renter", new PersonName("Иван", "Петканов"), "renter@example.bg",
                new PhoneDTO("+359", "878333444"), new UnrestrictedAddress("ул. 876", "Варна", "9000"),
                UserRole.OFFICE_WORKER),
            new UserDTO("sa60", new PersonName("Александър", "Стоянов"), "bobi87@example.bg",
                new PhoneDTO("+359", "889222333"), new UnrestrictedAddress("бул. 6-ти септември 15", "Пловдив", "4000"),
                UserRole.CUSTOMER),
            new UserDTO("sandov", new PersonName("Александър", "Иванов"), "sandov@example.bg",
                new PhoneDTO("+359", "889227630"), new UnrestrictedAddress("бул. 5-ти октомври 9", "Пазарджик", "4400"),
                UserRole.OFFICE_WORKER),
            new UserDTO("spongebob", new PersonName("Boris", "Borisov"), "boris123.@abv.bg",
                new PhoneDTO("+359", "899565542"), new UnrestrictedAddress("Rila str. 28", "Sofia", "1000"),
                UserRole.CUSTOMER),
            new UserDTO("ticketHunter", new PersonName("George", "Stefanov"), "Stefanov@dxc.com",
                new PhoneDTO("+359", "987654322"), new UnrestrictedAddress("ul. Stefan Dimitrov 7", "Sofia", "1000"),
                UserRole.CUSTOMER),
            new UserDTO("user123", new PersonName("Мария", "Иванова"), "maria.ivanova@example.bg",
                new PhoneDTO("+359", "878123456"), new UnrestrictedAddress("ул. Пирин 10", "Благоевград", "2700"),
                UserRole.CUSTOMER),
            new UserDTO("Алиса", new PersonName("Ани", "Ангелова"), "ani.angel@abv.bg",
                new PhoneDTO("+359", "889565542"), new UnrestrictedAddress("ул. Чайка 17", "Варна", "9000"),
                UserRole.CUSTOMER),
            new UserDTO("хитър-петър", new PersonName("Петър", "Здравков"), "zdravkov90.@abv.bg",
                new PhoneDTO("+359", "899445544"), new UnrestrictedAddress("G.Rakovski str. 35", "Sofia", "1000"),
                UserRole.CUSTOMER),
        };
        Assertions.assertArrayEquals(expected, actual);
    }

    /**
     * Test for HTTP PUT request with URL "/customers}" with customer details are
     * passed, returns the newly created {@link CustomerDTO} object and status code
     * {@link HttpStatus#SC_CREATED 201}.
     *
     * @param expectedCustomerDTO
     *            The expected result for CustomerDTO.
     * @param putCustomerDTO
     *            The customer that will be sent to the rest service.
     */
    @ParameterizedTest
    @MethodSource("validCustomerPUTArgumentsData")
    @Order(2)
    void testPutCustomerReturnsCorrectData(final CustomerDTO expectedCustomerDTO,
                                           final CustomerWithCredentialsDTO putCustomerDTO) {
        final String validJwtForCustomer = Jwt.createToken(expectedCustomerDTO.username(), ROLE_CUSTOMER);
        final CustomerDTO actual = HTTPRequest
            .put(REGISTER_CUSTOMER_PATH, putCustomerDTO, validJwtForCustomer, HttpStatus.SC_CREATED, CustomerDTO.class);

        assertEquals(expectedCustomerDTO, actual);
    }

    private static Stream<Arguments> validCustomerPUTArgumentsData() {
        final String username = "Boika";
        final Credentials cridentials = new Credentials(username, "Boika123!!!!");
        final PersonName personName = new PersonName("Yriii", "Boika");
        final UnrestrictedAddress address = new UnrestrictedAddress("ul. Pencho Slavejkov", "Varna", "9000");
        final String email = "BoikaYri@gmail.com";
        final PhoneDTO phoneNumber = new PhoneDTO("+359", "98857475");
        final LocalDate birthDate = LocalDate.of(1995, 2, 2);

        return Stream
            .of(arguments(new CustomerDTO(username, personName, email, phoneNumber, address, birthDate),
                          new CustomerWithCredentialsDTO(cridentials, personName, email, phoneNumber, address,
                              birthDate)));
    }

    /**
     * Test for HTTP PATCH request with URL "/customers}" with customer details are
     * passed, returns the newly created {@link CustomerDTO} object and status code
     * {@link HttpStatus#SC_CREATED 201}.
     *
     * @param username
     *            This is the username from the authentication token.
     * @param expectedCustomerDTO
     *            The expected result for CustomerDTO.
     * @param putCustomerDTO
     *            The customer that will be sent to the rest service.
     */
    @ParameterizedTest
    @MethodSource("validCustomerPATCHArgumentsData")
    @Order(3)
    void testPatchCustomerReturnsCorrectData(final String username, final CustomerDTO expectedCustomerDTO,
                                             final CustomerWithCredentialsDTO putCustomerDTO) {
        final String validJwtForCustomer = Jwt.createToken(username, ROLE_CUSTOMER);
        final CustomerDTO actual = HTTPRequest
            .patch(SPECIFIC_CUSTOMER_PATH, putCustomerDTO, validJwtForCustomer, HttpStatus.SC_OK, CustomerDTO.class,
                   username);

        assertEquals(expectedCustomerDTO, actual);
    }

    private static Stream<Arguments> validCustomerPATCHArgumentsData() {
        final String username = "giliev";
        final Credentials cridentials = new Credentials(username, "011235A!!!asdaasd");
        final PersonName personName = new PersonName("Georgi", "Iliev");
        final UnrestrictedAddress address = new UnrestrictedAddress("ul. Pencho Slavejkov", "Varna", "9000");
        final String email = "georgi.iliev3@dxc.com";
        final PhoneDTO phoneNumber = new PhoneDTO("+359", "98857475");
        final LocalDate birthDate = LocalDate.of(1995, 5, 20);

        return Stream
            .of(arguments(username, new CustomerDTO(username, personName, email, phoneNumber, address, birthDate),
                          new CustomerWithCredentialsDTO(cridentials, personName, email, phoneNumber, address,
                              birthDate)));
    }
}
