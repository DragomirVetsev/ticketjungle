package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.Collection;
import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.exceptions.ValidationException;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper class for {@link ValidationException}
 */
@Provider
public final class ValidationMapper extends BaseMapper<ValidationException, Collection<ErrorInfo>> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.BAD_REQUEST;
    }

    @Override
    protected Level getLogLevel() {
        return Level.SEVERE;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.VALIDATION_ERROR;
    }

    @Override
    protected Collection<ErrorInfo> getErrorDetails(final ValidationException exception) {
        return exception.getErrors();
    }

}
