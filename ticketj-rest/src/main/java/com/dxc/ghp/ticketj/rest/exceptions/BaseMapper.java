package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.dxc.ghp.ticketj.misc.exceptions.Error;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;

/**
 * Base class for all exception mappers. Implements {@link ExceptionMapper}.
 *
 * @param <ExceptionType>
 *            The exception type.
 * @param <ErrorDetailsType>
 *            The error details type.
 */
public abstract class BaseMapper<ExceptionType extends Throwable, ErrorDetailsType>
    implements ExceptionMapper<ExceptionType> {

    private static final Logger LOGGER = Logger.getLogger(BaseMapper.class.getName());

    @SuppressWarnings("nls")
    @Override
    public final Response toResponse(final ExceptionType exception) {
        final Level logLevel = getLogLevel();
        LOGGER
            .log(logLevel, exception,
                 () -> String.format("Mapping exception in %s: ", this.getClass().getSimpleName()));

        return Response
            .status(getResponseStatus())
            .type(MediaType.APPLICATION_JSON)
            .entity(new Error<>(getErrorType(), getErrorDetails(exception)))
            .build();
    }

    /**
     * Retrieves the HTTP response status.
     *
     * @return The HTTP status. Cannot be null.
     */
    protected abstract Status getResponseStatus();

    /**
     * Retrieves the Log Level of the exception.
     *
     * @return The log level. Cannot be null.
     */
    protected abstract Level getLogLevel();

    /**
     * Retrieves the error type used to construct response body.
     *
     * @return The error type. Cannot be null.
     */
    protected abstract ErrorType getErrorType();

    /**
     * Retrieves the error details used to construct response body.
     *
     * @param exception
     *            The exception of current mapper. Used to extract the error details
     *            object. Must not be null.
     * @return The error details from the exception. Cannot be null.
     */
    protected abstract ErrorDetailsType getErrorDetails(final ExceptionType exception);

}
