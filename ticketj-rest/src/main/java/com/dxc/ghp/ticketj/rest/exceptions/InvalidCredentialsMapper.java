package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.exceptions.InvalidCredentialsException;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper class for {@link InvalidCredentialsException}.
 */
@Provider
public final class InvalidCredentialsMapper extends BaseMapper<InvalidCredentialsException, ErrorInfo> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.BAD_REQUEST;
    }

    @Override
    protected Level getLogLevel() {
        return Level.INFO;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.INVALID_CREDENTIALS;
    }

    @Override
    protected ErrorInfo getErrorDetails(final InvalidCredentialsException exception) {
        return exception.getCredentialsErrorInfo();
    }
}
