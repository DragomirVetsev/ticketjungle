/**
 *
 */
package com.dxc.ghp.ticketj.rest.services;

import java.util.List;

import com.dxc.ghp.ticketj.dto.CitiesAndGenresDTO;
import com.dxc.ghp.ticketj.dto.PerformerDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.rest.exceptions.MissingServiceMapper;
import com.dxc.ghp.ticketj.services.EventService;
import com.dxc.ghp.ticketj.services.ReferentialService;

import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Contains all referential information related Restful services.
 */
@Path("/referential")
public final class ReferentialRestfullService extends BaseRestfulService {

    /**
     * Constructs Event Restful Service. For contract see
     * {@link BaseRestfulService#BaseRestfulService(ServletContext) base
     * constructor}.
     */
    @Inject
    protected ReferentialRestfullService(final ServletContext context) {
        super(context);
    }

    /**
     * Retrieves necessary static information regarding cities and event genres that
     * will be used for search criteria for the events.
     *
     * @return Static events and cities.
     */
    @GET
    @Path("/cities-and-genres")
    @Produces(MediaType.APPLICATION_JSON)
    public CitiesAndGenresDTO findByCriteria() {
        final ReferentialService referentialService = findService(ReferentialService.class);
        return referentialService.getCitiesAndGenres();
    }

    /**
     * Retrieves every performer.
     *
     * @return List of {@link PerformerDTO} containing every performer. Cannot be
     *         null. Empty list means no performers.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @see MissingServiceMapper
     */
    @GET
    @Path("/performers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<PerformerDTO> findAllPerformers() {
        final ReferentialService referentialService = findService(ReferentialService.class);
        return referentialService.findAllPerformers();
    }
}
