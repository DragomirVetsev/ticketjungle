package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper class for {@link Throwable}
 */
@Provider
public final class ThrowableMapper extends BaseMapper<Throwable, String> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.INTERNAL_SERVER_ERROR;
    }

    @Override
    protected Level getLogLevel() {
        return Level.SEVERE;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.UNKNOWN_ERROR;
    }

    @Override
    protected String getErrorDetails(final Throwable exception) {
        return exception.getClass().getSimpleName();
    }

}
