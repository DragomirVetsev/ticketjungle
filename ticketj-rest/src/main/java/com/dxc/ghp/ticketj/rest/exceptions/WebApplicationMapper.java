package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;
import java.util.logging.Logger;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper class for {@link WebApplicationException}
 */
@Provider
public final class WebApplicationMapper implements ExceptionMapper<WebApplicationException> {

    /**
     * Logger member to register what exception occurred.
     */
    private static final Logger LOGGER = Logger.getLogger(WebApplicationMapper.class.getName());

    @SuppressWarnings("nls")
    @Override
    public Response toResponse(final WebApplicationException exception) {

        LOGGER
            .log(Level.INFO, exception,
                 () -> String.format("Mapping exception in %s: ", this.getClass().getSimpleName()));

        return exception.getResponse();
    }

}
