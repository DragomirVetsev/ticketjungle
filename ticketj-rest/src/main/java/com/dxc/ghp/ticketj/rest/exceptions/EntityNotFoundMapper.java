package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper class for {@link EntityNotFoundException}.
 */
@Provider
public final class EntityNotFoundMapper extends BaseMapper<EntityNotFoundException, ErrorInfo> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.NOT_FOUND;
    }

    @Override
    protected Level getLogLevel() {
        return Level.SEVERE;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.ENTITY_NOT_FOUND;
    }

    @Override
    protected ErrorInfo getErrorDetails(final EntityNotFoundException exception) {
        return exception.getEntityErrorInfo();
    }

}
