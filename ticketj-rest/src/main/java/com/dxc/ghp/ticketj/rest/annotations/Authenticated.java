package com.dxc.ghp.ticketj.rest.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.ws.rs.NameBinding;

/**
 * An annotation used to mark request filters and different resources with
 * specific authorization roles.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({
    TYPE, METHOD
})
@NameBinding
public @interface Authenticated {
    // Mark resources who needs to be authenticated.
}
