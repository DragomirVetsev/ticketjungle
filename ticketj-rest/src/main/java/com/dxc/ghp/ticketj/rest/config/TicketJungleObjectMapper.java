package com.dxc.ghp.ticketj.rest.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;

import jakarta.ws.rs.ext.ContextResolver;
import jakarta.ws.rs.ext.Provider;

/**
 * Jackson's ObjectMapper configuration.
 */
@Provider
public final class TicketJungleObjectMapper implements ContextResolver<ObjectMapper> {

    private final ObjectMapper mapper;

    /**
     * Create and configure Jackson's ObjectMapper instance.
     */
    public TicketJungleObjectMapper() {
        mapper = createObjectMapper();
    }

    @Override
    public ObjectMapper getContext(final Class<?> type) {
        return mapper;
    }

    private static ObjectMapper createObjectMapper() {
        return JsonMapper
            .builder()
            .findAndAddModules()
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .build();
    }

}
