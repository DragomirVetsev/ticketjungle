package com.dxc.ghp.ticketj.rest.services;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.rest.annotations.Authenticated;
import com.dxc.ghp.ticketj.rest.exceptions.MissingServiceMapper;
import com.dxc.ghp.ticketj.services.OfficeService;
import com.dxc.ghp.ticketj.services.OfficeWorkerService;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Contains all office worker related Restful Services.
 */
@Path("/office-workers")
public final class OfficeWorkerRestfulService extends BaseRestfulService {
    /**
     * Constructs Office Worker Restful Service. For contract see
     * {@link BaseRestfulService#BaseRestfulService(ServletContext) base
     * constructor}.
     */
    @Inject
    public OfficeWorkerRestfulService(final ServletContext context) {
        super(context);
    }

    /**
     * Finds and returns all Ticket Jungle not assigned to an office workers.
     *
     * @return List of all available workers' usernames. Cannot be null. Empty list
     *         means no workers or no available workers.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link OfficeService} is missing.
     * @see MissingServiceMapper
     */
    @GET
    @Path("/available")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("ADMIN")
    public List<String> findAllAvailableWorkers() {
        final OfficeWorkerService officeWorkerService = findService(OfficeWorkerService.class);
        return officeWorkerService.findAllAvailableWorkers();
    }
}
