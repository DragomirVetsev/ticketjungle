package com.dxc.ghp.ticketj.rest.services;

import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.misc.service.provider.ServiceProvider;

import jakarta.servlet.ServletContext;

/**
 * Contains common behaviour of all Restful Services.
 */
public abstract class BaseRestfulService {

    private final ServletContext context;

    /**
     * The constructor is used only by JAX-RS framework. Injects
     * {@link ServletContext}.
     *
     * @param context
     *            is the current {@link ServletContext}. Must not be null.
     */
    @SuppressWarnings("nls")
    protected BaseRestfulService(final ServletContext context) {
        assert context != null : "Servlet context must not be null!";
        this.context = context;
    }

    /**
     * Finds a specific Ticket Jungle Business Service.
     *
     * @param <Service>
     *            service type which is looked for.
     * @param serviceType
     *            the class of the service which is looked for. Must not be null.
     * @return newly created service of type {@code Service}.
     * @throws ServiceMissingException
     *             when the service is not found.
     */
    protected <Service> Service findService(final Class<Service> serviceType) {
        final Object attribute = context.getAttribute(ServiceProvider.class.getName());
        if (attribute instanceof final ServiceProvider serviceProvider) {
            return serviceProvider.find(serviceType);
        }

        throw new ServiceMissingException(serviceType.getSimpleName());
    }

}
