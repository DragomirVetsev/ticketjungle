package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.EntityHasRelationshipException;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper class for {@link EntityHasRelationshipException}.
 */
@Provider
public final class EntityHasRelationshipMapper extends BaseMapper<EntityHasRelationshipException, ErrorInfo> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.CONFLICT;
    }

    @Override
    protected Level getLogLevel() {
        return Level.SEVERE;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.HAS_RELATIONSHIP;
    }

    @Override
    protected ErrorInfo getErrorDetails(final EntityHasRelationshipException exception) {
        return exception.getEntityErrorInfo();
    }

}
