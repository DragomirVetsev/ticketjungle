package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper class for {@link ServiceMissingException}.
 */
@Provider
public final class MissingServiceMapper extends BaseMapper<ServiceMissingException, String> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.INTERNAL_SERVER_ERROR;
    }

    @Override
    protected Level getLogLevel() {
        return Level.SEVERE;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.MISSING_SERVICE;
    }

    @Override
    protected String getErrorDetails(final ServiceMissingException exception) {
        return exception.getMissingService();
    }

}
