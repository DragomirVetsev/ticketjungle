package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.DuplicateFieldException;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;

import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 *
 */
@Provider
public final class DuplicateFieldMapper extends BaseMapper<DuplicateFieldException, ErrorInfo> {

    @Override
    protected Status getResponseStatus() {
        return Status.CONFLICT;
    }

    @Override
    protected Level getLogLevel() {
        return Level.INFO;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.DUPLICATE_FIELD;
    }

    @Override
    protected ErrorInfo getErrorDetails(final DuplicateFieldException exception) {
        return exception.getEntityErrorInfo();
    }

}
