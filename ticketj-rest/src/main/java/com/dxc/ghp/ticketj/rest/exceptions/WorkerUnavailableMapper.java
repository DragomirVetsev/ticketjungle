package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.exceptions.WorkerUnavailableException;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper class for {@link WorkerUnavailableException}.
 */
@Provider
public final class WorkerUnavailableMapper extends BaseMapper<WorkerUnavailableException, ErrorInfo> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.CONFLICT;
    }

    @Override
    protected Level getLogLevel() {
        return Level.INFO;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.ALREADY_EMPOYED;
    }

    @Override
    protected ErrorInfo getErrorDetails(final WorkerUnavailableException exception) {
        return exception.getEmploymentInfo();
    }

}
