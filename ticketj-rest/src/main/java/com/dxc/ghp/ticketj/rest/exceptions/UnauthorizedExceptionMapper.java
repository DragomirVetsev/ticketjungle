package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.exceptions.UnauthorizedException;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * 
 */
@Provider
public final class UnauthorizedExceptionMapper extends BaseMapper<UnauthorizedException, ErrorInfo> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.UNAUTHORIZED;
    }

    @Override
    protected Level getLogLevel() {
        return Level.SEVERE;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.UNAUTHORIZED;
    }

    @Override
    protected ErrorInfo getErrorDetails(final UnauthorizedException exception) {
        return exception.getErrorInfo();
    }

}
