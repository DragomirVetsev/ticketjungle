/**
 * Contains all Ticket Jungle Restful exceptions.
 */
package com.dxc.ghp.ticketj.rest.exceptions;
