package com.dxc.ghp.ticketj.rest.security.filters;

import java.util.List;

import com.dxc.ghp.ticketj.misc.exceptions.ForbiddenAccessException;
import com.dxc.ghp.ticketj.misc.exceptions.UnauthorizedException;
import com.dxc.ghp.ticketj.rest.annotations.Authenticated;

import jakarta.annotation.Priority;
import jakarta.annotation.security.DenyAll;
import jakarta.annotation.security.PermitAll;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.Provider;

/**
 * Interceptor filter for authorization check.
 */
@Provider
@Priority(Priorities.AUTHORIZATION)
@Authenticated
public final class AuthorizationFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @SuppressWarnings("nls")
    @Override
    public void filter(final ContainerRequestContext requestContext) {
        if (!checkIsUserInRole(requestContext, resourceInfo)) {
            throw new ForbiddenAccessException();
        }

        final MultivaluedMap<String, String> pathParams = requestContext.getUriInfo().getPathParameters();
        final String username = requestContext.getSecurityContext().getUserPrincipal().getName();
        final boolean isUserinRole = requestContext.getSecurityContext().isUserInRole("ADMIN");
        if (pathParams.containsKey("username") && !pathParams.getFirst("username").equals(username) && !isUserinRole) {
            throw new UnauthorizedException();
        }
    }

    private static boolean checkIsUserInRole(final ContainerRequestContext requestContext,
                                             final ResourceInfo resourceinfo) {
        final boolean isDenyAllPresent = resourceinfo.getResourceMethod().isAnnotationPresent(DenyAll.class);
        final boolean isPermitAllPresent = resourceinfo.getResourceMethod().isAnnotationPresent(PermitAll.class);
        final boolean isRolesAllowedPresent = resourceinfo.getResourceMethod().isAnnotationPresent(RolesAllowed.class);
        if (isDenyAllPresent) {
            return false;
        }
        if (isRolesAllowedPresent) {
            for (final String role : findRequiredResourceRoles(resourceinfo)) {
                if (requestContext.getSecurityContext().isUserInRole(role)) {
                    return true;
                }
            }
            return false;
        }
        return isPermitAllPresent;
    }

    private static List<String> findRequiredResourceRoles(final ResourceInfo resourceinfo) {
        return List.of(resourceinfo.getResourceMethod().getAnnotation(RolesAllowed.class).value());
    }
}
