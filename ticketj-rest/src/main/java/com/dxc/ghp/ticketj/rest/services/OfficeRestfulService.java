package com.dxc.ghp.ticketj.rest.services;

import java.net.URI;
import java.util.List;

import com.dxc.ghp.ticketj.dto.OfficeDTO;
import com.dxc.ghp.ticketj.misc.exceptions.EntityExistsException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.misc.exceptions.ValidationException;
import com.dxc.ghp.ticketj.rest.annotations.Authenticated;
import com.dxc.ghp.ticketj.rest.exceptions.EntityExistsMapper;
import com.dxc.ghp.ticketj.rest.exceptions.EntityNotFoundMapper;
import com.dxc.ghp.ticketj.rest.exceptions.MissingServiceMapper;
import com.dxc.ghp.ticketj.services.OfficeService;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

/**
 * Contains all offices related Restful services.
 */
@Path("/offices")
public final class OfficeRestfulService extends BaseRestfulService {

    /**
     * Constructs Office Restful Service. For contract see
     * {@link BaseRestfulService#BaseRestfulService(ServletContext) base
     * constructor}.
     */
    @Inject
    public OfficeRestfulService(final ServletContext context) {
        super(context);
    }

    /**
     * Finds and returns all Ticket Jungle offices.
     *
     * @return List of all Ticket Jungle offices. Cannot be null. Empty list means
     *         no offices.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link OfficeService} is missing.
     * @see MissingServiceMapper
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<OfficeDTO> findAllOffices() {
        final OfficeService officeService = findService(OfficeService.class);
        return officeService.findAllOffices();
    }

    /**
     * Creates a new Office.
     *
     * @param uri
     *            Injected object, used to extract path of the newly created office
     *            entity.
     * @param officeToCreate
     *            The data transfer object passed from the client containing info
     *            for new office. Can be null.
     * @return a new office data transfer object with updated information after new
     *         office is persisted. Cannot be null.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link OfficeService} is missing.
     * @throws ValidationException
     *             when the validation within the {@code officeToCreate} object has
     *             failed.
     * @throws EntityNotFoundException
     *             when the city or phone code in the {@code officeToCreate} object
     *             does not exist.
     * @throws EntityExistsException
     *             when there is an existing office with either of the following
     *             properties: {@code officeName, email, phoneNumber}.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     * @see EntityExistsMapper
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("ADMIN")
    public Response createOffice(@Context final UriInfo uri, final OfficeDTO officeToCreate) {
        final OfficeService officeService = findService(OfficeService.class);
        final OfficeDTO createdOffice = officeService.createOffice(officeToCreate);

        final URI newOfficeUri = uri.getRequestUriBuilder().path(createdOffice.officeName()).build();
        return Response.created(newOfficeUri).entity(createdOffice).build();
    }

    /**
     * Updates an already existing office.
     *
     * @param officeToUpdate
     *            The data transfer object passed from the client containing info
     *            for new office. Can be null.
     * @param officeName
     *            is the unique office identifier to be searched by.
     * @return A new data transfer object containing updated office information.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link OfficeService} is missing.
     * @throws ValidationException
     *             when the validation within the {@code officeToCreate} object has
     *             failed.
     * @throws EntityNotFoundException
     *             when the city or phone code in the {@code officeToCreate} object
     *             does not exist.
     * @throws EntityExistsException
     *             when there is an existing office with either of the following
     *             properties: {@code email, phoneNumber}.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     * @see EntityExistsMapper
     */
    @PATCH
    @Path("/{officeName}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("ADMIN")
    public OfficeDTO modifyOffice(final OfficeDTO officeToUpdate, @PathParam("officeName") final String officeName) {
        final OfficeService officeService = findService(OfficeService.class);
        return officeService.modifyOffice(officeName, officeToUpdate);
    }
}
