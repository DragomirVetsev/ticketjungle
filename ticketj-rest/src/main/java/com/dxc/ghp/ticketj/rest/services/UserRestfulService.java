package com.dxc.ghp.ticketj.rest.services;

import java.util.List;

import com.dxc.ghp.ticketj.dto.UserDTO;
import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.rest.annotations.Authenticated;
import com.dxc.ghp.ticketj.rest.exceptions.MissingServiceMapper;
import com.dxc.ghp.ticketj.services.UserService;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Contains all user related Restful Services.
 */
@Path("/users")
public final class UserRestfulService extends BaseRestfulService {

    /**
     * Constructs User Restful Service. For contract see
     * {@link BaseRestfulService#BaseRestfulService(ServletContext) base
     * constructor}.
     */
    @Inject
    protected UserRestfulService(final ServletContext context) {
        super(context);
    }

    /**
     * Finds and returns all Ticket Jungle users.
     *
     * @return List of all Ticket Jungle users. Cannot be null. Empty list means no
     *         users.
     * @throws ServiceMissingException
     *             when the server is not properly configured or {@link UserService}
     *             is missing.
     * @see MissingServiceMapper
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("ADMIN")
    public List<UserDTO> findAllSystemUsers() {
        final UserService systemUserService = findService(UserService.class);
        return systemUserService.findAllUsers();
    }
}
