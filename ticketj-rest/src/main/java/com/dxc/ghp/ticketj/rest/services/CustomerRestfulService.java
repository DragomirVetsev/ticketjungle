package com.dxc.ghp.ticketj.rest.services;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import com.dxc.ghp.ticketj.dto.CustomerDTO;
import com.dxc.ghp.ticketj.dto.CustomerWithCredentialsDTO;
import com.dxc.ghp.ticketj.dto.OrderDTO;
import com.dxc.ghp.ticketj.dto.OrderWithTicketsDTO;
import com.dxc.ghp.ticketj.dto.ReservedTicketDTO;
import com.dxc.ghp.ticketj.dto.ReviewDTO;
import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.dto.TicketDTO;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.misc.exceptions.ValidationException;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.rest.annotations.Authenticated;
import com.dxc.ghp.ticketj.rest.exceptions.EntityNotFoundMapper;
import com.dxc.ghp.ticketj.rest.exceptions.MissingServiceMapper;
import com.dxc.ghp.ticketj.services.CustomerService;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

/**
 * Contains all customer related Restful Services.
 */
@Path("/customers")
public final class CustomerRestfulService extends BaseRestfulService {

    /**
     * Constructs Customer Restful Service. For contract see
     * {@link BaseRestfulService#BaseRestfulService(ServletContext) base
     * constructor}.
     */
    @Inject
    public CustomerRestfulService(final ServletContext context) {
        super(context);
    }

    /**
     * Finds and returns customer.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return {@link Optional} of CustomerDTO. Cannot be null. Empty Optional means
     *         no customer found.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link CustomerService} is missing.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @see MissingServiceMapper
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}")
    @Authenticated
    @RolesAllowed({
        "CUSTOMER", "ADMIN"
    })
    public Optional<CustomerDTO> findCustomer(@PathParam("username") final String username) {
        final CustomerService customerService = findService(CustomerService.class);
        return customerService.findCustomer(username);
    }

    /**
     * Update Ticket Jungle Customer phone number, witch includes .
     *
     * @param username
     *            The path username for witch the .
     * @param newCustomerDTO
     *            The customers information to be updated.
     * @return А new customer data transfer object with updated information after
     *         new customer is persisted. Cannot be null.
     */
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public CustomerDTO parchCustomer(@PathParam("username") final String username,
                                     final CustomerWithCredentialsDTO newCustomerDTO) {
        final CustomerService customerService = findService(CustomerService.class);
        return customerService.patchCustomer(username, newCustomerDTO);
    }

    /**
     * Update and if not exist create a new Ticket Jungle Customer.
     *
     * @param uri
     *            Injected object, used to extract path of the newly created
     *            customer entity.
     * @param newCustomerDTO
     *            Is the customer with all dear information and credentials.
     * @return А new customer data transfer object with updated information after
     *         new customer is persisted. Cannot be null.
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/register")
    public Response registerCustomer(@Context final UriInfo uri, final CustomerWithCredentialsDTO newCustomerDTO) {
        final CustomerService customerService = findService(CustomerService.class);
        final CustomerDTO putEventDTO = customerService.registerCustomer(newCustomerDTO);

        final URI newEventUri = uri.getRequestUriBuilder().path(putEventDTO.username()).build();
        return Response.created(newEventUri).entity(putEventDTO).build();
    }

    /**
     * Finds all Customer's tickets.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return list of tickets. Cannot be null. Empty list means this customer has
     *         no tickets.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link CustomerService} is missing.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with given username does not exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}/tickets")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public List<TicketDTO> findTicketsOfCustomer(@PathParam("username") final String username) {
        final CustomerService customerService = findService(CustomerService.class);
        return customerService.findTicketsOfCustomer(username);
    }

    /**
     * Find all Customer's reviews.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return list of reviews. Cannot be null. Empty list means this customer has
     *         no reviews.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link CustomerService} is missing.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with given username does not exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}/reviews")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public List<ReviewDTO> findReviewsOfCustomer(@PathParam("username") final String username) {
        final CustomerService customerService = findService(CustomerService.class);
        return customerService.findReviewsOfCustomer(username);
    }

    /**
     * Find Customer's Wishlist.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return list of subevents. Cannot be null. Empty list means this customer has
     *         no subevents in his Wishlist.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link CustomerService} is missing.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with given username does not exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}/wishlist")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public List<SubEventDTO> findWishlistOfCustomer(@PathParam("username") final String username) {
        final CustomerService customerService = findService(CustomerService.class);
        return customerService.findWishlistOfCustomer(username);
    }

    // SHOPPING CART ENDPOINTS:

    /**
     * Find all Customer's Cart Items.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @return Shopping Cart of Customer. Cannot be null. Empty list means this
     *         customer has no reserved tickets.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link CustomerService} is missing.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with given username does not exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}/cart")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public List<ReservedTicketDTO> findCartItemsOfCustomer(@PathParam("username") final String username) {

        final CustomerService customerService = findService(CustomerService.class);
        return customerService.findCartItemsOfCustomer(username);
    }

    /**
     * Remove the selected cart items of Customer.
     *
     * @param username
     *            is the unique customer identifier to be searched by.
     * @param reservedTickets
     *            the list of selected cart items from the customer.
     * @return the response of the completed request.
     * @throws EntityNotFoundException
     *             when the customer with given username does not exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}/cart/remove")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public List<ReservedTicketDTO> removeCartItemOfCustomer(@PathParam("username") final String username,
                                                            final List<ReservedTicketDTO> reservedTickets) {

        final CustomerService customerService = findService(CustomerService.class);
        return customerService.removeCartItemOfCustomer(username, reservedTickets);
    }

    /**
     * Creates order for the tickets given in newOrderWithTicketsDTO.
     *
     * @param uri
     *            Injected object, used to extract path of the newly created order
     *            entity.
     * @param orderWithTicketsDTO
     *            Holds the necessary information for the order creation.
     * @param username
     *            The username for whom the order will be created.
     * @return a new order with tickets data transfer object with updated
     *         information after new order is persisted. Cannot be null.
     */
    // TODO SOME throws??
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}/order")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public Response createOrderWithTickets(@Context final UriInfo uri, final OrderWithTicketsDTO orderWithTicketsDTO,
                                           @PathParam("username") final String username) {
        final CustomerService customerService = findService(CustomerService.class);
        final OrderDTO createdOrderDTO = customerService.createOrderWithTickets(orderWithTicketsDTO, username);
        final URI newOrderUri = uri.getRequestUriBuilder().path(createdOrderDTO.username()).build();
        return Response.created(newOrderUri).entity(createdOrderDTO).build();
    }

    /**
     * Adds sub-event to customer's wishlist.
     *
     * @param username
     *            The customer's unique identificator used to find the customer.
     *            Cannot be null.
     * @param performanceId
     *            The sub-event's unique identificator used to find the sub-event
     *            that will be added to customer's wishlist.
     * @return HTTP Status code:
     *         {@link jakarta.ws.rs.core.Response.Status#NO_CONTENT 204} if the
     *         sub-event was successfully added. Returns HTTP Status code:
     *         {@link jakarta.ws.rs.core.Response.Status#NOT_MODIFIED 304} if the
     *         given sub-event wasn't added to the wishlist.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link CustomerService} is missing.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with given username does not exist or sub-event
     *             with with given id doesn't exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     */
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}/wishlist/add")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public Response addSubEventToWishList(@PathParam("username") final String username,
                                          final PerformanceId performanceId) {
        final CustomerService customerService = findService(CustomerService.class);

        if (customerService.addSubEventToWishList(username, performanceId)) {
            return Response.noContent().build();
        }
        return Response.notModified().build();
    }

    /**
     * Removes sub-event from customer's wishlist.
     *
     * @param username
     *            The customer's unique identificator used to find the customer.
     *            Cannot be null.
     * @param performanceId
     *            The sub-event's unique identificator used to find the sub-event
     *            that will be removed from customer's wishlist.
     * @return HTTP Status code:
     *         {@link jakarta.ws.rs.core.Response.Status#NO_CONTENT 204} if the
     *         sub-event was successfully removed. Returns HTTP Status code:
     *         {@link jakarta.ws.rs.core.Response.Status#NOT_MODIFIED 304} if the
     *         given sub-event wasn't removed from the wishlist.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link CustomerService} is missing.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with given username does not exist or sub-event
     *             with with given id doesn't exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     */
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}/wishlist/remove")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public Response removeSubEventFromWishList(@PathParam("username") final String username,
                                               final PerformanceId performanceId) {
        final CustomerService customerService = findService(CustomerService.class);
        if (customerService.removeSubEventFromWishList(username, performanceId)) {
            return Response.noContent().build();
        }
        return Response.notModified().build();
    }

    /**
     * Removes multiple sub-events from customer's wishlist.
     *
     * @param username
     *            The customer's unique identificator used to find the customer.
     *            Cannot be null.
     * @param selectedSubEvents
     *            The IDs of the sub-events that will be removed from the customer's
     *            wishlist. Cannot be null.
     * @return HTTP Status code:
     *         {@link jakarta.ws.rs.core.Response.Status#NO_CONTENT 204} if the
     *         sub-event was successfully removed. Returns HTTP Status code:
     *         {@link jakarta.ws.rs.core.Response.Status#NOT_MODIFIED 304} if the
     *         given sub-event wasn't removed from the wishlist.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link CustomerService} is missing.
     * @throws ValidationException
     *             when the validation of the given params has failed.
     * @throws EntityNotFoundException
     *             when the customer with given username does not exist or sub-event
     *             with with given id doesn't exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     */
    @PATCH
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}/wishlist/remove/selected")
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public Response removeSelectedSubEventsFromWishlist(@PathParam("username") final String username,
                                                        final List<PerformanceId> selectedSubEvents) {
        final CustomerService customerService = findService(CustomerService.class);
        if (customerService.removeSelectedSubEventsFromWishlist(username, selectedSubEvents)) {
            return Response.noContent().build();
        }
        return Response.notModified().build();
    }
}
