package com.dxc.ghp.ticketj.rest.services;

import java.net.URI;
import java.time.Instant;
import java.util.List;

import com.dxc.ghp.ticketj.dto.CartItemDTO;
import com.dxc.ghp.ticketj.dto.EventDTO;
import com.dxc.ghp.ticketj.dto.EventSearchDTO;
import com.dxc.ghp.ticketj.dto.StageDTO;
import com.dxc.ghp.ticketj.dto.SubEventDTO;
import com.dxc.ghp.ticketj.dto.SubEventOrderedPairDTO;
import com.dxc.ghp.ticketj.misc.exceptions.DuplicateFieldException;
import com.dxc.ghp.ticketj.misc.exceptions.EntityNotFoundException;
import com.dxc.ghp.ticketj.misc.exceptions.ServiceMissingException;
import com.dxc.ghp.ticketj.misc.exceptions.ValidationException;
import com.dxc.ghp.ticketj.misc.types.PerformanceId;
import com.dxc.ghp.ticketj.misc.types.Period;
import com.dxc.ghp.ticketj.rest.annotations.Authenticated;
import com.dxc.ghp.ticketj.rest.exceptions.EntityExistsMapper;
import com.dxc.ghp.ticketj.rest.exceptions.EntityNotFoundMapper;
import com.dxc.ghp.ticketj.rest.exceptions.MissingServiceMapper;
import com.dxc.ghp.ticketj.services.EventService;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

/**
 * Contains all events related Restful services.
 */
@Path("/events")
public final class EventRestfulService extends BaseRestfulService {

    /**
     * Constructs Event Restful Service. For contract see
     * {@link BaseRestfulService#BaseRestfulService(ServletContext) base
     * constructor}.
     */
    @Inject
    public EventRestfulService(final ServletContext context) {
        super(context);
    }

    /**
     * Finds and returns Ticket Jungle events.
     *
     * @return List of all Ticket Jungle events. Cannot be null. Empty list means no
     *         events.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @see MissingServiceMapper
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<EventDTO> findAllEvents() {
        final EventService eventService = findService(EventService.class);
        return eventService.findAllEvents();
    }

    /**
     * Finds and returns Ticket Jungle events found by user criteria.
     *
     * @param from
     *            Is the the date from with the search will start and go on to the
     *            future. Can be null.
     * @param to
     *            Is the the maximum date to with the search will end. Can be null.
     * @param city
     *            Where the Event will be held. Can be null.
     * @param genre
     *            Of the Event. Can be null.
     * @return List of all Ticket Jungle events. Cannot be null. Empty list means no
     *         events.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @see MissingServiceMapper
     */
    @GET
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<EventDTO> findByCriteria(@QueryParam("from") final Instant from, @QueryParam("to") final Instant to,
                                         @QueryParam("city") final String city,
                                         @QueryParam("genre") final String genre) {
        final EventService eventService = findService(EventService.class);
        return eventService.findByCriteria(new EventSearchDTO(city, genre, new Period<>(from, to)));
    }

    /**
     * Finds and returns a Ticket Jungle event by name.
     *
     * @param name
     *            is the name of the Event we are searching for. Must not be null
     * @return Finds and returns an {@link EventDTO} by name.
     */
    @GET
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public EventDTO findByName(@PathParam("name") final String name) {
        final EventService eventService = findService(EventService.class);
        return eventService.findByName(name);
    }

    /**
     * Creates a new Ticket Jungle Event.
     *
     * @param uri
     *            Injected object, used to extract path of the newly created event
     *            entity.
     * @param newEventDTO
     *            is the data transfer object passed from the client containing info
     *            for new event. Can be null.
     * @return a new event data transfer object with updated information after new
     *         event is persisted. Cannot be null.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("ADMIN")
    public Response createEvent(@Context final UriInfo uri, final EventDTO newEventDTO) {
        final EventService eventService = findService(EventService.class);
        final EventDTO createdEventDTO = eventService.createEvent(newEventDTO);

        final URI newEventUri = uri.getRequestUriBuilder().path(createdEventDTO.getName()).build();
        return Response.created(newEventUri).entity(createdEventDTO).build();
    }

    /**
     * Creates a new sub-event.
     *
     * @param uri
     *            Injected object, used to extract path of the newly created
     *            sub-event entity.
     * @param subEventToCreate
     *            The data transfer object passed from the client containing info
     *            for new sub-event. Can be null.
     * @return a new sub-event data transfer object with updated information after
     *         new sub-event is persisted. Cannot be null.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @throws ValidationException
     *             when the validation within the {@code subEventToCreate} object
     *             has failed.
     * @see MissingServiceMapper
     */
    @POST
    @Path("/{event_name}/sub-events")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("ADMIN")
    public Response createSubEvent(@PathParam("event_name") @Context final UriInfo uri,
                                   final SubEventDTO subEventToCreate) {
        final EventService eventService = findService(EventService.class);
        final SubEventDTO createdSubEventDTO = eventService.createSubEvent(subEventToCreate);

        final URI newSubEventUri = uri.getRequestUriBuilder().path(createdSubEventDTO.performanceId().name()).build();
        return Response.created(newSubEventUri).entity(createdSubEventDTO).build();
    }

    /**
     * Finds and returns the current state of sub-event's stage seats.
     *
     * @param event
     *            is part of the unique sub-event identifier to be searched by and
     *            The name of the Event to which the sub-event belongs. Cannot be
     *            null.
     * @param subEvent
     *            is part of the unique sub-event identifier to be searched by and
     *            The name of sub-event. Cannot be null.
     * @return Sub-event's stage seats and their statuses.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @see MissingServiceMapper
     */
    @GET
    @Path("/{event}/sub-events/{sub-event-name}/seats")
    @Produces(MediaType.APPLICATION_JSON)
    public StageDTO findSubEventStageSeats(@PathParam("event") final String event,
                                           @PathParam("sub-event-name") final String subEvent) {
        final EventService eventService = findService(EventService.class);
        final PerformanceId performance = new PerformanceId(subEvent, event);
        return eventService.findSubEventStageSeats(performance);
    }

    /**
     * Finds and returns all sorted Ticket Jungle SubEvents in an Event.
     *
     * @param eventName
     *            is part of the unique sub-event identifier to be searched by and
     *            is the name of the Event which contains the SubEvents. Must not be
     *            null.
     * @return List of all Ticket Jungle events. Cannot be null. Empty list means no
     *         events.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @throws EntityNotFoundException
     *             when the event we are looking in does not exist.
     * @see MissingServiceMapper
     */
    @GET
    @Path("/{event_name}/sub-events")
    @Produces(MediaType.APPLICATION_JSON)
    public List<SubEventDTO> findAllSubEventsInEvent(@PathParam("event_name") final String eventName) {
        final EventService eventService = findService(EventService.class);
        return eventService.findAllSubEventsInEvent(eventName);
    }

    /**
     * * Finds and returns a pair of sub-event lists ordered by time and popularity.
     *
     * @return a pair of sub-event lists ordered by time and popularity. Pair cannot
     *         be null. Empty list means no events.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @see MissingServiceMapper
     */
    @GET
    @Path("/sub-events/ordered")
    @Produces(MediaType.APPLICATION_JSON)
    public SubEventOrderedPairDTO findAllSubEventsOrdered() {
        final EventService eventService = findService(EventService.class);
        return eventService.findAllSubEventsOrdered();
    }

    /**
     * Updates an already existing event.
     *
     * @param eventToUpdate
     *            The data transfer object containing info for the event. Can be
     *            null.
     * @param eventName
     *            is the unique event identifier to be searched by.
     * @return A new data transfer object containing updated event information.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @throws ValidationException
     *             when the validation within the {@code eventToUpdate} object has
     *             failed.
     * @throws EntityNotFoundException
     *             when the name in the {@code eventToUpdate} object does not exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     * @see EntityExistsMapper
     */
    @PATCH
    @Path("/{event_name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("ADMIN")
    public EventDTO updateEvent(final EventDTO eventToUpdate, @PathParam("event_name") final String eventName) {
        final EventService eventService = findService(EventService.class);
        return eventService.updateEvent(eventName, eventToUpdate);
    }

    /**
     * Updates an already existing sub-event.
     *
     * @param subEventToUpdate
     *            The data transfer object passed from the client containing info
     *            for the event. Can be null.
     * @param event
     *            is the name of the event containing the sub-event
     * @param subEvent
     *            is the name of the sub-event.
     * @return A new data transfer object containing updated sub-event information.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @throws ValidationException
     *             when the validation within the {@code subEventToUpdate} object
     *             has failed.
     * @throws EntityNotFoundException
     *             when the sub-event with this id does not exist.
     * @throws DuplicateFieldException
     *             when a sub-event already has the same start time, venue and
     *             stage.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     * @see EntityExistsMapper
     */
    @PATCH
    @Path("/{event_name}/sub-events/{sub-event-name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("ADMIN")
    public SubEventDTO updateSubEvent(final SubEventDTO subEventToUpdate, @PathParam("event_name") final String event,
                                      @PathParam("sub-event-name") final String subEvent) {
        final EventService eventService = findService(EventService.class);
        final PerformanceId performance = new PerformanceId(subEvent, event);
        return eventService.updateSubEvent(performance, subEventToUpdate);
    }

    /**
     * Deletes an existing sub-event entity.
     *
     * @param event
     *            is part of the unique sub-event identifier and is the name of the
     *            event containing the sub-event
     * @param subEvent
     *            is part of the unique sub-event identifier and is the name of the
     *            sub-event.
     * @return A data transfer object containing the deleted sub-event information.
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @throws EntityNotFoundException
     *             when the sub-event with this id does not exist.
     * @see MissingServiceMapper
     * @see EntityNotFoundMapper
     */
    @DELETE
    @Path("/{event_name}/sub-events/{sub-event-name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("ADMIN")
    public SubEventDTO deleteSubEvent(@PathParam("event_name") final String event,
                                      @PathParam("sub-event-name") final String subEvent) {
        final EventService eventService = findService(EventService.class);
        final PerformanceId performance = new PerformanceId(subEvent, event);
        return eventService.deleteSubEvent(performance);
    }

    /**
     * @param cartItems
     *            the list of Cart Items DTO
     * @throws ServiceMissingException
     *             when the server is not properly configured or
     *             {@link EventService} is missing.
     * @throws ValidationException
     *             when the validation within the {@code subEventToCreate} object
     *             has failed.
     * @see MissingServiceMapper
     */
    @POST
    @Path("/{event_name}/sub-events/addcart")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    @RolesAllowed("CUSTOMER")
    public void createCartItem(final List<CartItemDTO> cartItems) {
        final EventService eventService = findService(EventService.class);
        eventService.createCartItem(cartItems);

    }
}
