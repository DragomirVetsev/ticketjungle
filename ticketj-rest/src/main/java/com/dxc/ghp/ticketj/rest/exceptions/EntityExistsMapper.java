package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.EntityExistsException;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * Exception mapper class for {@link EntityExistsException}.
 */
@Provider
public final class EntityExistsMapper extends BaseMapper<EntityExistsException, ErrorInfo> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.CONFLICT;
    }

    @Override
    protected Level getLogLevel() {
        return Level.INFO;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.ENTITY_EXISTS;
    }

    @Override
    protected ErrorInfo getErrorDetails(final EntityExistsException exception) {
        return exception.getEntityErrorInfo();
    }

}
