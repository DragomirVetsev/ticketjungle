package com.dxc.ghp.ticketj.rest.security.filters;

import java.security.Principal;

import com.dxc.ghp.ticketj.jwt.service.JwtService;
import com.dxc.ghp.ticketj.misc.exceptions.UnauthorizedException;
import com.dxc.ghp.ticketj.rest.annotations.Authenticated;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import jakarta.annotation.Priority;
import jakarta.ws.rs.Priorities;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.SecurityContext;
import jakarta.ws.rs.ext.Provider;

/**
 * Interceptor filter for authentication check.
 */
@Provider
@Authenticated
@Priority(Priorities.AUTHENTICATION)
public final class AuthenticationFilter implements ContainerRequestFilter {

    @SuppressWarnings("nls")
    private static final String BEARER = "Bearer";

    private static record User(String username, String role) {
    }

    @Override
    public void filter(final ContainerRequestContext requestContext) {
        final String token = extractToken(requestContext);
        final User user = validateToken(token);
        modifyRequestSecurityContext(requestContext, user);
    }

    /**
     * @param requestContext
     * @return
     */
    private static String extractToken(final ContainerRequestContext requestContext) {
        final String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (authHeader == null || !authHeader.startsWith(BEARER)) {
            throw new UnauthorizedException();
        }

        final String token = authHeader.substring(BEARER.length()).trim();
        if (token.isEmpty()) {
            throw new UnauthorizedException();
        }

        return token;
    }

    /**
     * @param token
     * @return
     */
    @SuppressWarnings("nls")
    private static User validateToken(final String token) {
        try {
            final Claims claims = JwtService.parse(token);
            final String username = claims.getSubject();
            final String role = claims.get(JwtService.USER_ROLE).toString();
            return new User(username, role);
        } catch (final ExpiredJwtException exception) {
            throw new UnauthorizedException("Expired Token!", exception);
        } catch (final JwtException exception) {
            throw new UnauthorizedException("Unauthorized User!", exception);
        }
    }

    private static void modifyRequestSecurityContext(final ContainerRequestContext requestContext, final User user) {
        final SecurityContext oldSecurityContext = requestContext.getSecurityContext();
        final SecurityContext newSecurityContext = new SecurityContext() {

            @Override
            public boolean isUserInRole(final String role) {
                return user.role.equals(role);
            }

            @Override
            public boolean isSecure() {
                return oldSecurityContext.isSecure();
            }

            @Override
            public Principal getUserPrincipal() {
                return () -> user.username;
            }

            @Override
            public String getAuthenticationScheme() {
                return oldSecurityContext.getAuthenticationScheme();
            }
        };
        requestContext.setSecurityContext(newSecurityContext);
    }
}
