package com.dxc.ghp.ticketj.rest.config;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import jakarta.ws.rs.ext.ParamConverter;
import jakarta.ws.rs.ext.ParamConverterProvider;
import jakarta.ws.rs.ext.Provider;

/**
 * Missing converters for some Java 8 Date classes for use as path and query
 * parameters for rest services.
 */
@Provider
public final class Java8DateConverter implements ParamConverterProvider {

    private final Map<String, Function<CharSequence, ?>> parsers = Map
        .of(LocalDate.class.getName(), LocalDate::parse, LocalDateTime.class.getName(), LocalDateTime::parse,
            ZonedDateTime.class.getName(), ZonedDateTime::parse, Instant.class.getName(), Instant::parse);

    @Override
    public <T> ParamConverter<T> getConverter(final Class<T> rawType, final Type genericType,
                                              final Annotation[] annotations) {
        final Function<CharSequence, T> parser = (Function<CharSequence, T>) parsers.get(rawType.getName());
        return (parser == null) ? null : new Java8DateParamConverter<>(parser);
    }

    /**
     * Generic parameter converter for classes with parse method from CharSequence.
     */
    private static final class Java8DateParamConverter<T> implements ParamConverter<T> {

        private final Function<CharSequence, T> parseFunction;

        private Java8DateParamConverter(final Function<CharSequence, T> parseFunction) {
            this.parseFunction = parseFunction;
        }

        @Override
        public T fromString(final String value) {
            return Optional.ofNullable(value).map(parseFunction).orElse(null);
        }

        @Override
        // For future use?
        public String toString(final T value) {
            return Optional.ofNullable(value).map(T::toString).orElse(null);
        }
    }

}
