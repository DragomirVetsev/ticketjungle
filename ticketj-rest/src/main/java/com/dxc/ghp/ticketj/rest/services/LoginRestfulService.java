package com.dxc.ghp.ticketj.rest.services;

import com.dxc.ghp.ticketj.dto.UserInfoDTO;
import com.dxc.ghp.ticketj.dto.UserJwtDTO;
import com.dxc.ghp.ticketj.jwt.service.JwtService;
import com.dxc.ghp.ticketj.misc.types.Credentials;
import com.dxc.ghp.ticketj.services.UserService;

import jakarta.inject.Inject;
import jakarta.servlet.ServletContext;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

/**
 * Contains all login related Restful services.
 */
@Path("/login")
public final class LoginRestfulService extends BaseRestfulService {

    /**
     * Constructs Login Restful Service. For contract see
     * {@link BaseRestfulService#BaseRestfulService(ServletContext) base
     * constructor}.
     */
    @Inject
    protected LoginRestfulService(final ServletContext context) {
        super(context);
    }

    /**
     * @param credentials
     *            is the data transfer object passed from the client, containing
     *            info for his/her credentials. Can`t be null.
     * @return user credentials with generated JWT token.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserJwtDTO login(final Credentials credentials) {
        final UserService userService = findService(UserService.class);
        final UserInfoDTO userInfo = userService.findUserByCredentials(credentials);
        return JwtService.generateUserWithToken(userInfo);
    }
}
