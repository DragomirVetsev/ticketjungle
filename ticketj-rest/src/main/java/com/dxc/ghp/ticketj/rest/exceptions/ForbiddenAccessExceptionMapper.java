package com.dxc.ghp.ticketj.rest.exceptions;

import java.util.logging.Level;

import com.dxc.ghp.ticketj.misc.exceptions.ErrorInfo;
import com.dxc.ghp.ticketj.misc.exceptions.ErrorType;
import com.dxc.ghp.ticketj.misc.exceptions.ForbiddenAccessException;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;

/**
 * 
 */
@Provider
public final class ForbiddenAccessExceptionMapper extends BaseMapper<ForbiddenAccessException, ErrorInfo> {

    @Override
    protected Status getResponseStatus() {
        return Response.Status.FORBIDDEN;
    }

    @Override
    protected Level getLogLevel() {
        return Level.INFO;
    }

    @Override
    protected ErrorType getErrorType() {
        return ErrorType.FORBIDDEN;
    }

    @Override
    protected ErrorInfo getErrorDetails(final ForbiddenAccessException exception) {
        return exception.getErrorInfo();
    }

}
